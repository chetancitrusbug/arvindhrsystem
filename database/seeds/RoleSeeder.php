<?php

use Illuminate\Database\Seeder;

use App\Role;

class RoleSeeder extends Seeder
{

    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Role::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $role = Role::create(//1
            [
                'name' => 'SU',
                'label' => 'Admin'
            ]);


        $role->permissions()->sync(\App\Permission::pluck('id'));

        $arr = [

            [
                'name' => 'HRMS',
                'label' => 'Human Resource'
            ],
            [
                'name' => 'RM',
                'label' => 'Regional HR'
            ],
            [
                'name' => 'SM',
                'label' => 'Store Manger'
            ],

        ];

        foreach ($arr as $a) {
            Role::create($a);
        }


    }
}
