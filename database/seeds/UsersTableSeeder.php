<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        \DB::table('users')->delete();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $user = User::create(
            [
                'emp_name' => 'Arvind Lifestyle',
                'email' => 'arvinduniversity@gmail.com',
                'password' => bcrypt(123456),
                'emp_id' => 'super',
                'gender' => 'male',
                'date_of_joining' => '0000-00-00',
                'status' => 1

            ]
        );


        if (!$user->hasRole('SU')) {
            $user->assignRole('SU');

        }

    }
}
