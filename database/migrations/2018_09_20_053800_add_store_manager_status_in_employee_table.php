<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreManagerStatusInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("update employee set regional_status='0',hrms_status='0',regional_note='',hrms_note='',status='pending'");
        Schema::table('employee', function (Blueprint $table) {
            $table->integer('regional_approve_id')->after('user_id')->default(0);
            $table->integer('hrms_approve_id')->after('regional_approve_id')->default(0);
            $table->tinyInteger('store_status')->after('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->dropColumn('store_status');
            $table->dropColumn('hrms_approve_id');
            $table->dropColumn('regional_approve_id');
        });
    }
}
