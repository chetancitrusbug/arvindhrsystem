<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHiringReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hiring_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gap')->default(0);
            $table->integer('hired')->default(0);
            $table->integer('pending')->default(0);
            $table->integer('absconding')->default(0);
            $table->integer('resignation')->default(0);
            $table->string('monthyear');
            $table->integer('store_id');
            $table->integer('region_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hiring_reports');
    }
}
