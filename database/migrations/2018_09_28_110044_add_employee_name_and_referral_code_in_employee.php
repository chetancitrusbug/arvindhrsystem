<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeNameAndReferralCodeInEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `employee` CHANGE `source_name` `source_category_id` INT(11) NULL DEFAULT NULL");

        Schema::table('employee', function (Blueprint $table) {
            $table->integer('source_detail_id')->after('source_category_id')->nullable();
            $table->string('source_code')->after('source_detail_id')->nullable();
            $table->string('employee_name')->nullable();
            $table->string('old_employee_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `employee` CHANGE `source_category_id` `source_name` INT(11) NULL DEFAULT NULL");

        Schema::table('employee', function (Blueprint $table) {
            $table->dropColumn('source_detail_id');
            $table->dropColumn('source_code');
            $table->dropColumn('employee_name');
            $table->dropColumn('old_employee_code');
        });
    }
}
