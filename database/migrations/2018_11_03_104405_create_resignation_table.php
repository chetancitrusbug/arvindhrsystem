<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateresignationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resignation', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('employee_id');
			$table->integer('request_to')->nullable()->default(0)->comment("Request Processing Region id (Region manager can get data)");
			$table->string('current_handler_role')->nullable()->default(null)->comment("Role which is handling request");

			$table->date('last_working_date')->nullable()->default(null);
			$table->string('application_copy')->nullable()->default(null)->comment("Application request from employee");
            $table->string('agreement_copy')->nullable()->default(null)->comment("Application from employee");
            $table->string('reg_agreement_copy')->nullable()->default(null)->comment("Application from employee");
			$table->text('resignation_form')->nullable()->default(null);
			$table->enum('resignation_status',['pending','cancelled','approved','decline','documentation','complete','processing-hr','documentation-reg'])->default('pending')->comment("Non - never apply for resignation");
			$table->string('regional_note',255)->nullable()->default(null)->comment("Note by regional manager ");
            $table->string('store_note',255)->nullable()->default(null)->comment("Note by Store manager ");

            $table->integer('reason_id');
            $table->integer('sub_reason_id');

			$table->integer('request_by')->nullable()->default(0)->comment("Request by store manageer id");
            $table->integer('approve_by')->nullable()->default(0)->comment("Approve or decline by region manager id");

            $table->date('date_of_resignation');

			$table->softDeletes();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resignation');
    }
}
