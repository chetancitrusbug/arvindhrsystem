<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldsInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->string('l0_l1_status')->nullable();
            $table->string('l0_l1_certificate')->nullable();
            $table->string('uniform')->nullable();
            $table->string('id_card')->nullable();
            $table->string('highest_qualification')->nullable();
            $table->string('company')->nullable();
            $table->string('payroll_type')->nullable();
            $table->string('processstatus')->nullable();
            $table->date('date_of_leave')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->dropColumn('l0_l1_status');
            $table->dropColumn('l0_l1_certificate');
            $table->dropColumn('uniform');
            $table->dropColumn('id_card');
            $table->dropColumn('highest_qualification');
            $table->dropColumn('company');
            $table->dropColumn('payroll_type');
            $table->dropColumn('processstatus');
            $table->dropColumn('date_of_leave');
        });
    }
}
