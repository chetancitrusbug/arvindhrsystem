<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `source_category` DROP `source_name`");
        \DB::statement("ALTER TABLE `source_category` DROP `source_code`");
        \DB::statement("TRUNCATE source_category");

        Schema::create('source_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('source_category_id');
            $table->string('source_name');
            $table->string('source_code');
            $table->tinyInteger('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('source_detail');
    }
}
