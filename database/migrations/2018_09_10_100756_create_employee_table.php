<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->date('joining_date');
            $table->integer('grade_id');
            $table->string('designation');
            $table->integer('department_id');
            $table->integer('sub_department_id');
            $table->integer('business_unit_id');
            $table->string('reporting_manager_employee_code');
            $table->string('reporting_manager_name');
            $table->integer('brand_id');
            $table->integer('legal_entity_id');
            $table->string('location_code');
            $table->string('io_code');
            $table->integer('employee_classification_id');
            $table->integer('source_category_id');
            $table->double('employee_refferal_amount');
            $table->string('employee_refferal_payment_month');
            $table->double('ctc');
            $table->integer('variable_pay_type_id');
            $table->enum('soft_copy_attached',['yes','no']);
            $table->string('pod_no');
            $table->integer('city_id');
            $table->integer('state_id');
            $table->integer('region_id');
            $table->integer('store_name');
            $table->tinyInteger('status');
            $table->enum('hold_salary',['YES','NO'])->default('NO');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
