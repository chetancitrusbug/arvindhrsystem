<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_location', function (Blueprint $table) {
            $table->increments('id');
            $table->string('location_code');
            $table->string('io_code');
            $table->string('sap_code');
            $table->string('store_name');
            $table->integer('brand_id');
            $table->text('address');
            $table->integer('city_id');
            $table->integer('state_id');
            $table->integer('region_id');
            $table->tinyInteger('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_location');
    }
}
