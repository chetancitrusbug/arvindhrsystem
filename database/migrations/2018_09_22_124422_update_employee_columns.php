<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmployeeColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `employee` CHANGE `name` `name` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `joining_date` `joining_date` DATE NULL");
        \DB::statement("ALTER TABLE `family_information` CHANGE `date` `date` VARCHAR(191) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `grade_id` `grade_id` INT(11) NULL, CHANGE `designation_id` `designation_id` INT(11) NULL, CHANGE `department_id` `department_id` INT(11) NULL, CHANGE `sub_department_id` `sub_department_id` INT(11) NULL, CHANGE `business_unit_id` `business_unit_id` INT(11) NULL, CHANGE `reporting_manager_employee_code` `reporting_manager_employee_code` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `reporting_manager_name` `reporting_manager_name` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `brand_id` `brand_id` INT(11) NULL, CHANGE `legal_entity_id` `legal_entity_id` INT(11) NULL, CHANGE `location_code` `location_code` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `io_code` `io_code` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `employee_classification_id` `employee_classification_id` INT(11) NULL, CHANGE `source_name` `source_name` INT(11) NULL, CHANGE `ctc` `ctc` DOUBLE NULL, CHANGE `soft_copy_attached` `soft_copy_attached` ENUM('yes','no','na') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `city_id` `city_id` INT(11) NULL, CHANGE `state_id` `state_id` INT(11) NULL, CHANGE `region_id` `region_id` INT(11) NULL, CHANGE `store_name` `store_name` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `status` `status` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `store_status` `store_status` TINYINT(4) NULL DEFAULT '0', CHANGE `regional_status` `regional_status` TINYINT(4) NULL DEFAULT '0', CHANGE `hrms_status` `hrms_status` TINYINT(4) NULL DEFAULT '0'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `employee` CHANGE `name` `name` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `joining_date` `joining_date` DATE NULL");
        \DB::statement("ALTER TABLE `family_information` CHANGE `date` `date` VARCHAR(191) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `grade_id` `grade_id` INT(11) NULL, CHANGE `designation_id` `designation_id` INT(11) NULL, CHANGE `department_id` `department_id` INT(11) NULL, CHANGE `sub_department_id` `sub_department_id` INT(11) NULL, CHANGE `business_unit_id` `business_unit_id` INT(11) NULL, CHANGE `reporting_manager_employee_code` `reporting_manager_employee_code` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `reporting_manager_name` `reporting_manager_name` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `brand_id` `brand_id` INT(11) NULL, CHANGE `legal_entity_id` `legal_entity_id` INT(11) NULL, CHANGE `location_code` `location_code` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `io_code` `io_code` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `employee_classification_id` `employee_classification_id` INT(11) NULL, CHANGE `source_name` `source_name` INT(11) NULL, CHANGE `ctc` `ctc` DOUBLE NULL, CHANGE `soft_copy_attached` `soft_copy_attached` ENUM('yes','no','na') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `city_id` `city_id` INT(11) NULL, CHANGE `state_id` `state_id` INT(11) NULL, CHANGE `region_id` `region_id` INT(11) NULL, CHANGE `store_name` `store_name` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `status` `status` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `store_status` `store_status` TINYINT(4) NULL DEFAULT '0', CHANGE `regional_status` `regional_status` TINYINT(4) NULL DEFAULT '0', CHANGE `hrms_status` `hrms_status` TINYINT(4) NULL DEFAULT '0'");
    }
}
