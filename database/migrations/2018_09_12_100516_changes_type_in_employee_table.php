<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesTypeInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("TRUNCATE employee");
        \DB::statement("ALTER TABLE `employee` CHANGE `location_code` `location_code` INT(11) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `io_code` `io_code` INT(11) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `store_name` `store_name` INT(11) NOT NULL");
        \DB::statement("ALTER TABLE `employee` ADD `user_id` INT(11) NOT NULL AFTER `id`");
        \DB::statement("ALTER TABLE `employee` CHANGE `status` `status` VARCHAR(191) NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `employee` CHANGE `location_code` `location_code` INT(11) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `io_code` `io_code` INT(11) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `store_name` `store_name` INT(11) NOT NULL");
        \DB::statement("ALTER TABLE employee DROP COLUMN user_id");
        \DB::statement("ALTER TABLE `employee` CHANGE `status` `status` VARCHAR(191) NOT NULL");
    }
}
