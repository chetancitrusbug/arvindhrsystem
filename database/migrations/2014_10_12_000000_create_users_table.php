<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_id');
            $table->string('emp_name');
            $table->string('gender');
            $table->string('date_of_joining');
            $table->string('designation')->nullable();
            $table->string('email')->unique();
            $table->string('personal_email')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('region')->nullable();
            $table->string('state')->nullable();
            $table->string('password');
            $table->tinyInteger('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
