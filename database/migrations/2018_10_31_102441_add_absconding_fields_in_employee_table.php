<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAbscondingFieldsInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->tinyInteger('absconding_status')->default(0);
            $table->timestamp('absconding_time')->nullable();
            $table->tinyInteger('rm_absconding_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->dropColumn('absconding_status');
            $table->dropColumn('absconding_time');
            $table->dropColumn('rm_absconding_status');
        });
    }
}
