<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeCodeColumnInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->string('employee_code')->nullable();
            $table->string('appointment_letter')->nullable();
            $table->string('salary_amount')->nullable();
            $table->string('esic_team_no')->nullable();
            $table->string('id_card_issue')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->dropColumn('employee_code');
            $table->dropColumn('appointment_letter');
            $table->dropColumn('salary_amount');
            $table->dropColumn('esic_team_no');
            $table->dropColumn('id_card_issue');
        });
    }
}
