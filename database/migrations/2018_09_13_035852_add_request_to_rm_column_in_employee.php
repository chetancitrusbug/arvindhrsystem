<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestToRmColumnInEmployee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->text('regional_note')->after('store_name')->nullable();
            $table->text('hrms_note')->after('regional_note')->nullable();
            $table->tinyInteger('regional_status')->after('status')->default(0);
            $table->tinyInteger('hrms_status')->after('regional_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->dropColumn('regional_note');
            $table->dropColumn('hrms_note');
            $table->dropColumn('regional_status');
            $table->dropColumn('hrms_status');
        });
    }
}
