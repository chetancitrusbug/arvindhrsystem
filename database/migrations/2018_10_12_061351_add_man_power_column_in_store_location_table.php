<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManPowerColumnInStoreLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_location', function (Blueprint $table) {
            $table->string('budget_man_power')->after('store_name')->default(0)->nullable();
            $table->string('actual_man_power')->after('budget_man_power')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_location', function (Blueprint $table) {
            $table->dropColumn('budget_man_power');
            $table->dropColumn('actual_man_power');
        });
    }
}
