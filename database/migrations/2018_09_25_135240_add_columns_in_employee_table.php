<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->tinyInteger('upload_documents')->nullable()->default(0);
            $table->tinyInteger('store_hold')->nullable()->default(0);
            $table->tinyInteger('regional_hold')->nullable()->default(0);
            $table->tinyInteger('first_time_employment')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->dropColumn('upload_documents');
            $table->dropColumn('store_hold');
            $table->dropColumn('regional_hold');
            $table->dropColumn('first_time_employment');
        });
    }
}
