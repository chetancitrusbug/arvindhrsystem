<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnTypeInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `employee` CHANGE `employee_refferal_amount` `employee_refferal_amount` DOUBLE NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `employee_refferal_payment_month` `employee_refferal_payment_month` VARCHAR(191) NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `pod_no` `pod_no` VARCHAR(191) NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `employee` CHANGE `employee_refferal_amount` `employee_refferal_amount` DOUBLE NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `employee_refferal_payment_month` `employee_refferal_payment_month` VARCHAR(191) NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `pod_no` `pod_no` VARCHAR(191) NULL");
    }
}
