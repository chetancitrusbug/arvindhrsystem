<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeOfStoreNameColumnInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `employee` CHANGE `store_name` `store_name` VARCHAR(191) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `variable_pay_type_id` `variable_pay_type_id` INT(11) NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `employee` CHANGE `store_name` `store_name` VARCHAR(191) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `variable_pay_type_id` `variable_pay_type_id` INT(11) NULL");
    }
}
