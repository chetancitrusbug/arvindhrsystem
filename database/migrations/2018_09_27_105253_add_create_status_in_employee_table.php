<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreateStatusInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->tinyInteger('add_detail')->default(0)->nullable();
            $table->tinyInteger('add_education')->default(0)->nullable();
            $table->tinyInteger('add_experience')->default(0)->nullable();
            $table->tinyInteger('add_family_info')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->dropColumn('add_detail');
            $table->dropColumn('add_education');
            $table->dropColumn('add_experience');
            $table->dropColumn('add_family_info');
        });
    }
}
