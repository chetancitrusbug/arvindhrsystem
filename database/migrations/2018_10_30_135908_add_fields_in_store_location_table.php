<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInStoreLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_location', function (Blueprint $table) {
            $table->string('store_email')->after('store_name')->nullable();
            $table->string('store_password')->after('store_email')->nullable();
            $table->string('emp_name')->after('store_password')->nullable();
            $table->string('emp_id')->after('emp_name')->nullable();
            $table->string('mobile_no')->after('emp_id')->nullable();
            $table->string('pincode')->after('mobile_no')->nullable();
            $table->string('internal_order')->after('pincode')->nullable();
            $table->string('legal_entity_id')->after('internal_order')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_location', function (Blueprint $table) {
            $table->dropColumn('store_email');
            $table->dropColumn('store_password');
            $table->dropColumn('emp_name');
            $table->dropColumn('emp_id');
            $table->dropColumn('mobile_no');
            $table->dropColumn('pincode');
            $table->dropColumn('internal_order');
            $table->dropColumn('legal_entity_id');
        });
    }
}
