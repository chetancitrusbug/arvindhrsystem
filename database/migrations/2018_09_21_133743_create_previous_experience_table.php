<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreviousExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('previous_experience', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->integer('nature_of_employment_id');
            $table->string('company_name');
            $table->date('joining_date');
            $table->date('leaving_date');
            $table->string('designation');
            $table->string('reason_of_leaving');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('previous_experience');
    }
}
