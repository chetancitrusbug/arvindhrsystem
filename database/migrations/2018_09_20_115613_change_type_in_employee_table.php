<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("UPDATE employee set deleted_at = now()");
        \DB::statement("ALTER TABLE `employee` CHANGE `location_code` `location_code` VARCHAR(191) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `io_code` `io_code` VARCHAR(191) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `store_name` `store_name` VARCHAR(191) NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `employee` CHANGE `location_code` `location_code` VARCHAR(191) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `io_code` `io_code` VARCHAR(191) NOT NULL");
        \DB::statement("ALTER TABLE `employee` CHANGE `store_name` `store_name` VARCHAR(191) NOT NULL");
    }
}
