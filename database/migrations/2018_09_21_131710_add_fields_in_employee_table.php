<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->string('full_name')->nullable();
            $table->string('position')->nullable();
            $table->enum('blood_group',['O','A','B','AB'])->nullable();
            $table->enum('rh_factor',['positive','negative'])->nullable();
            $table->date('date_of_birth')->nullable();
            $table->enum('marital_status',['single','married'])->nullable();
            $table->enum('aadhar_status',['available','not_available'])->nullable();
            $table->string('aadhar_number')->nullable();
            $table->string('nationality')->nullable();
            $table->string('aadhar_enrolment_number')->nullable();
            $table->enum('gender',['male','female'])->nullable();
            $table->enum('pan_status',['available','not_available'])->nullable();
            $table->string('pan_number')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('esic_number')->nullable();
            $table->string('emergency_person')->nullable();
            $table->string('emergency_number')->nullable();
            $table->string('emergency_relationship')->nullable();
            $table->date('anniversary_date')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('ua_number')->nullable();
            $table->string('current_address_line_1')->nullable();
            $table->string('current_address_line_2')->nullable();
            $table->string('current_address_line_3')->nullable();
            $table->integer('current_city')->nullable();
            $table->integer('current_state')->nullable();
            $table->string('current_pincode')->nullable();
            $table->string('permanent_address_line_1')->nullable();
            $table->string('permanent_address_line_2')->nullable();
            $table->string('permanent_address_line_3')->nullable();
            $table->integer('permanent_city')->nullable();
            $table->integer('permanent_state')->nullable();
            $table->string('permanent_pincode')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->dropColumn('full_name');
            $table->dropColumn('position');
            $table->dropColumn('blood_group',['O','A','B','AB']);
            $table->dropColumn('rh_factor',['positive','negative']);
            $table->dropColumn('date_of_birth');
            $table->dropColumn('marital_status',['single','married']);
            $table->dropColumn('aadhar_status',['available','not_available']);
            $table->dropColumn('aadhar_number');
            $table->dropColumn('nationality');
            $table->dropColumn('aadhar_enrolment_number');
            $table->dropColumn('gender',['male','female']);
            $table->dropColumn('pan_status',['available','not_available']);
            $table->dropColumn('pan_number');
            $table->dropColumn('mobile');
            $table->dropColumn('email');
            $table->dropColumn('esic_number');
            $table->dropColumn('emergency_person');
            $table->dropColumn('emergency_number');
            $table->dropColumn('emergency_relationship');
            $table->dropColumn('anniversary_date');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('ua_number');
            $table->dropColumn('current_address_line_1');
            $table->dropColumn('current_address_line_2');
            $table->dropColumn('current_address_line_3');
            $table->dropColumn('current_city');
            $table->dropColumn('current_state');
            $table->dropColumn('current_pincode');
            $table->dropColumn('permanent_address_line_1');
            $table->dropColumn('permanent_address_line_2');
            $table->dropColumn('permanent_address_line_3');
            $table->dropColumn('permanent_city');
            $table->dropColumn('permanent_state');
            $table->dropColumn('permanent_pincode');
        });
    }
}
