<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->date('dol')->nullable()->default(null);
            $table->enum('process_status',['YES','NO'])->nullable()->default('NO');
            $table->enum('appointment_status',['YES','NO'])->nullable()->default('NO');
            $table->enum('id_card_status',['YES','NO'])->nullable()->default('NO');
            $table->enum('uniform_status',['YES','NO'])->nullable()->default('NO');
            $table->enum('l0l1_status',['YES','NO'])->nullable()->default('NO');
            $table->tinyinteger('l0l1_result')->nullable()->default('Null');
            $table->enum('l0l1_certificate_status',['YES','NO'])->nullable()->default('NO');
            $table->enum('background_verification',['YES','NO','N/A'])->nullable()->default('N/A');
            $table->enum('bv_report_status',['Verified','Non verified'])->nullable()->default('Non verified');
            $table->enum('is_disabled',['YES','NO'])->default('NO');
            $table->mediumText('disability_desc')->nullable()->default('');
            $table->integer('allocated_leave')->nullable()->default(0);
            $table->integer('pending_leave')->nullable()->default(0);
            $table->integer('availed_leave')->nullable()->default(0);
            $table->float('salary')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_infos');
    }
}

