<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `users` CHANGE `gender` `gender` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL");
        \DB::statement("ALTER TABLE `users` CHANGE `date_of_joining` `date_of_joining` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL, CHANGE `designation` `designation` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL");
        Schema::table('users', function (Blueprint $table) {
            $table->string('pincode')->after('state')->nullable();
            $table->string('internal_order')->after('pincode')->nullable();
            $table->string('brand_id')->after('internal_order')->nullable();
            $table->string('legal_entity_id')->after('brand_id')->nullable();
            $table->string('store_email')->after('email')->nullable();
            $table->string('store_contact')->after('mobile_no')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('pincode');
            $table->dropColumn('internal_order');
            $table->dropColumn('store_email');
            $table->dropColumn('store_contact');
            $table->dropColumn('brand_id');
            $table->dropColumn('legal_entity_id');
        });
    }
}
