<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'emp_name', 'email', 'password','emp_id','store_id','gender','date_of_joining','designation','store_email','personal_email','mobile_no','store_contact','region','state','city','pincode','internal_order','brand_id','legal_entity_id','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function legalEntity()
    {
        return $this->belongsTo('App\LegalEntity');
    }

    public function storeLocation()
    {
        return $this->belongsTo('App\StoreLocation','store_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function stateName()
    {
        return $this->belongsTo('App\State','state');
    }

    public function cityName()
    {
        return $this->belongsTo('App\City','city');
    }

    public function regionName()
    {
        return $this->belongsTo('App\Region','region');
    }
}
