<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HiringReport extends Model
{
    //
    use SoftDeletes;

    protected $table = 'hiring_reports';
    protected $fillable = ['gap','hired','pending','month-year','store_id','region_id'];
}
