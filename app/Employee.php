<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'employee';

    /**
     * The database primary key value.
     *
     * @var string
     */
    // protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','joining_date','grade_id','designation_id','department_id','sub_department_id','business_unit_id','reporting_manager_employee_code','reporting_manager_name','brand_id','legal_entity_id','location_code','io_code','employee_classification_id','source_category_id','employee_refferal_amount','employee_refferal_payment_month','ctc','variable_pay_type_id','soft_copy_attached','pod_no','city_id','state_id','region_id','store_name','status','user_id','regional_note','hrms_note','regional_status','hrms_status','full_name','position','blood_group','rh_factor','date_of_birth','marital_status','aadhar_status','aadhar_number','nationality','aadhar_enrolment_number','gender','pan_status','pan_number','mobile','email','esic_number','emergency_person','emergency_number','emergency_relationship','anniversary_date','first_name','last_name','ua_number','current_address_line_1','current_address_line_2','current_address_line_3','current_city','current_state','current_pincode','permanent_address_line_1','permanent_address_line_2','permanent_address_line_3','permanent_city','permanent_state','permanent_pincode','upload_documents','store_hold','regional_hold','first_time_employment','add_detail','add_education','add_experience','add_family_info','employee_name','old_employee_code','employee_code','store_id','location_id','bank_name','ifsc','account_no','branch_name','l0_l1_status','l0_l1_certificate','uniform','id_card','highest_qualification','company','payroll_type','processstatus','date_of_leave'];

    public function getJoiningDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d/m/Y');
        }
        return $value;
    }

    public function getDateOfBirthAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d/m/Y');
        }
        return $value;
    }

    public function getAnniversaryDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d/m/Y');
        }
        return $value;
    }

    public function setJoiningDateAttribute($value)
    {
        if($value != ""){
            $this->attributes['joining_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $value))->format('Y-m-d');
        }
    }

    public function grade()
    {
        return $this->belongsTo('App\Grade');
    }

    public function attendance()
    {
        return $this->hasMany('App\Attendance');
    }

    public function designation()
    {
        return $this->belongsTo('App\Designation');
    }

    public function subDepartment()
    {
        return $this->belongsTo('App\SubDepartment');
    }

    public function businessUnit()
    {
        return $this->belongsTo('App\BusinessUnit');
    }

    public function legalEntity()
    {
        return $this->belongsTo('App\LegalEntity');
    }

    public function previousExperience(){
        return $this->hasMany('App\PreviousExperience','employee_id','id');
    }
    public function subDepartmentEmp()
    {
        return $this->belongsTo('App\SubDepartment','sub_department_id');
    }

    public function EducationDetail()
    {
        return $this->hasMany('App\EducationDetail','employee_id','id')->orderby('education_category_id','desc');
    }

    public function resignation()
    {
        return $this->belongsTo('App\Resignation','id','employee_id');
    }

    public function FamilyInfo()
    {
        return $this->hasMany('App\FamilyInfo','employee_id','id');
    }

    public function other_infos()
    {
        return $this->belongsTo('App\OtherInfo','id','employee_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Department','department_id','id');
    }

    public function storeLocation()
    {
        return $this->belongsTo('App\StoreLocation','store_id');
    }

    public function employeeClassification()
    {
        return $this->belongsTo('App\EmployeeClassification');
    }

    public function sourceCategory()
    {
        return $this->belongsTo('App\SourceCategory','source_category_id');
    }

    public function sourceDetail()
    {
        return $this->belongsTo('App\SourceDetail','source_detail_id');
    }

    public function variablePayType()
    {
        return $this->belongsTo('App\VariablePayType');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function currentState()
    {
        return $this->belongsTo('App\State','current_state');
    }

    public function permanentState()
    {
        return $this->belongsTo('App\State','permanent_state');
    }

    public function currentCity()
    {
        return $this->belongsTo('App\City','current_city');
    }

    public function permanentCity()
    {
        return $this->belongsTo('App\City','permanent_city');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function attachment()
    {
        return $this->hasMany('App\Attachment');
    }
}
