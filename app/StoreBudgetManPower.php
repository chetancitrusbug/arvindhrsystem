<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreBudgetManPower extends Model
{
    protected $table = 'store_budget_man_powers';
    protected $fillable = ['store_id','designation_id','budget'];
}
