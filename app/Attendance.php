<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Attendance extends Model
{
    // use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attendance';

    protected $fillable = ['attendance','employee_id','day','month','year','full_date','reason'];

	public static function getAttendanceReportData(){
		$selected_date = \Carbon\Carbon::now()->startOfDay()->subDays(1)->format("Y-m-d");
		$present_per = 100;
		$actual_man_power = 0;

		$manPowerGet = \App\StoreLocation::where('store_location.status',1)->select(DB::raw('SUM(store_location.actual_man_power) as total_actual_man_power'))->first();
		$present_man_power = \App\Attendance::where('full_date', '=',$selected_date)->where('attendance','p')->count();

		if($manPowerGet){
			$actual_man_power = round($manPowerGet->total_actual_man_power);
		}
		if($actual_man_power > 0){
			$present_per =	round(($present_man_power * 100) / $actual_man_power, 2);
		}

		return ['present_per'=>$present_per,'actual_man_power'=>$actual_man_power,'present_man_power'=>$present_man_power];
	}
}
