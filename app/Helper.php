<?php

namespace App;

use Carbon\Carbon as Carbon;
use App\Employee;

class Helper
{
    public static function ymd($value='')
    {
        if($value != ''){
            return Carbon::parse($value)->format('Y-m-d');
        }
        return $value;
    }

    public static function dmy($value='')
    {
        if($value != ''){
            return Carbon::parse($value)->format('d/m/Y');
        }
        return $value;
    }

    public static function uploadEmployeeDocuments($requestData){
        $employee = Employee::find($requestData['employee_id']);

        if(isset($requestData['attach']) && !empty($requestData['attach'])){
            $name = str_replace(' ','_',$employee->full_name);
            foreach ($requestData['attach'] as $key=>$value) {
                $file = [];
                $file['key'] = $key;
                $file['employee_id'] = $employee->id;

                if(in_array($key, ['education_documents','previous_company_docs'])){
                    //remove old docs when update
                    if($requestData['upload'] == 1){
                        $oldDocs = Attachment::where('key',$key)->where('employee_id',$employee->id)->get();

                        if(count($oldDocs)){
                            foreach ($oldDocs as $k => $v) {
                                if(file_exists($v->file)){
                                    unlink($v->file); //delete previously uploaded Attachment
                                }
                            }
                        }

                        Attachment::where('key',$key)->where('employee_id',$employee->id)->delete();
                    }

                    foreach ($value as $doc_key=>$doc_value) {
                        $filename = $name.'_'.uniqid(time()) . '.' . $doc_value->getClientOriginalExtension();
                        $doc_value->move('uploads/employee/'.$key, $filename);

                        $file['file'] = 'uploads/employee/'.$key.'/'.$filename;
                        $attachment = Attachment::create($file);
                    }
                }else{
                    if($requestData['upload'] == 1){
                        //remove old docs when update
                        $oldDocs = Attachment::where('key',$key)->where('employee_id',$employee->id)->get();

                        if(count($oldDocs)){
                            foreach ($oldDocs as $k => $v) {
                                if(file_exists($v->file)){
                                    unlink($v->file); //delete previously uploaded Attachment
                                }
                            }
                        }

                        Attachment::where('key',$key)->where('employee_id',$employee->id)->delete();
                    }

                    $filename = $name.'_'.uniqid(time()) . '.' . $value->getClientOriginalExtension();
                    $value->move('uploads/employee/'.$key, $filename);
                    $file['file'] = 'uploads/employee/'.$key.'/'.$filename;
                    $attachment = Attachment::create($file);
                }
            }
        }

        if($requestData['upload'] == 1){
            $log['title'] = "Employee document update by store manager";
        }else{
            $log['title'] = "Employee document upload by store manager";
        }
        $log['emp_id'] = $employee->id;
        $log['emp_name'] = $employee->full_name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        $employee->upload_documents = 1;
        $employee->joining_date = self::ymd(str_replace('/', '-',$requestData['joining_date']));
        $employee->business_unit_id = $requestData['business_unit_id'];
        $employee->legal_entity_id = $requestData['legal_entity_id'];
        $employee->employee_classification_id = $requestData['employee_classification_id'];
        $employee->variable_pay_type_id = $requestData['variable_pay_type_id'];
        $employee->pod_no = $requestData['pod_no'];
        $employee->save();

        return true;
    }

    public static function uploadDocumentValidationRule($request,$id,$joining=''){
        $employee = Employee::select('first_time_employment')->where('id',$id)->first();

        $rules = [
            'joining_date'=>['date_format:d/m/Y','nullable',
                            function($attribute, $value, $fail) use($request,$id,$joining) {
                                if($value != '' && $joining == ''){
                                    if(Helper::ymd(str_replace('/', '-',$value)) < Helper::ymd(Carbon::now())){
                                        return $fail('The joining date must be a date after '.Helper::dmy(Carbon::now()));
                                    }
                                }
                            },
                        ],
            'pod_no'=>'nullable|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'business_unit_id'=>'required',
            'legal_entity_id'=>'required',
            'employee_classification_id'=>'required',
            'variable_pay_type_id'=>'required',
            'attach.scanned_photo' => ['nullable',function($attribute,$value,$fail) use ($request,$id){
                                            $format = ['jpg'];
                                            $status = false;
                                            if(!in_array($value->getClientOriginalExtension(), $format)){
                                                $status = true;
                                            }
                                            if($status){
                                                $fail('The education documents must be a file of type: jpg, png.');
                                            }
                                        }],
            'attach.employment_form' => 'nullable|mimes:pdf',
            'attach.form_11' => 'nullable|mimes:pdf',
            'attach.form_02' => 'nullable|mimes:pdf',
            'attach.form_f' => 'nullable|mimes:pdf',
            'attach.hiring_checklist' => 'nullable|mimes:pdf',
            'attach.inteview_assessment' => 'nullable|mimes:pdf',
            'attach.resume' => 'nullable|mimes:pdf',
            'attach.aadhar_card' => 'nullable|mimes:pdf,jpg',
            'attach.pan_card' => 'nullable|mimes:pdf,jpg',
            'attach.bank_passbook' => 'nullable|mimes:pdf,jpg',
            'attach.offer_accept' => 'nullable|mimes:pdf',
            'attach.education_documents' => ['nullable',function($attribute,$value,$fail) use ($request,$id){
                                            $format = ['pdf','jpg','png'];
                                            $status = false;
                                            foreach ($request->attach['education_documents'] as $value) {
                                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                                    $status = true;
                                                }
                                            }
                                            if($status){
                                                $fail('The education documents must be a file of type: pdf, jpg, png.');
                                            }
                                        }],

        ];
/*
        if($employee->first_time_employment == 0){
            $rules['attach.previous_company_docs'] = ['required',function($attribute,$value,$fail) use ($request,$id){
                                                            $format = ['pdf','jpg','png'];
                                                            $status = false;
                                                            foreach ($request->attach['previous_company_docs'] as $value) {
                                                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                                                    $status = true;
                                                                }
                                                            }
                                                            if($status){
                                                                $fail('The previous company documents must be a file of type: pdf, jpg, png.');
                                                            }
                                                        }];
        }
        */
        return $rules;
    }

    public static function updateDocumentValidationRule($request,$joining=''){
        return [
            'joining_date'=>['date_format:d/m/Y','nullable',
                            function($attribute, $value, $fail) use($request,$joining) {
                                if($value != '' && $joining == ''){
                                    if(Helper::ymd(str_replace('/', '-',$value)) < Helper::ymd(Carbon::now())){
                                        return $fail('The joining date must be a date after '.Helper::dmy(Carbon::now()));
                                    }
                                }
                            },
                        ],
            'pod_no'=>'nullable|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'business_unit_id'=>'required',
            'legal_entity_id'=>'required',
            'employee_classification_id'=>'required',
            'variable_pay_type_id'=>'required',
        ];
    }

    public static function validationMessage(){
        return [
                'business_unit_id.required' => 'The business unit is not defined',
                'legal_entity_id.required' => 'The legal entity is not defined',
                'employee_classification_id.required' => 'The employee classification is not defined',
                'variable_pay_type_id.required' => 'The variable pay type is not defined',
                'attach.scanned_photo.mimes' => 'The scanned photo must be a file of type: jpg.',
                'attach.scanned_photo.required' => 'The scanned photo field is required',
                'attach.employment_form.mimes' => 'The employment form attachment must be a file of type: pdf.',
                'attach.employment_form.required' => 'The employment form attachment field is required',
                'attach.form_11.required' => 'The form-11 attachment field is required',
                'attach.form_11.mimes' => 'The form-11 attachment must be a file of type: pdf.',
                'attach.form_02.required' => 'The form-02 attachment field is required',
                'attach.form_02.mimes' => 'The form-02 attachment must be a file of type: pdf',
                'attach.form_f.required' => 'The form-f attachment field is required',
                'attach.form_f.mimes' => 'The form-f attachment must be a file of type: pdf',
                'attach.hiring_checklist.required' => 'The Hiring Checklist attachment field is required',
                'attach.hiring_checklist.mimes' => 'The Hiring Checklist attachment must be a file of type: pdf',
                'attach.inteview_assessment.required' => 'The Interview Assessment attachment field is required',
                'attach.inteview_assessment.mimes' => 'The Interview Assessment must be a file of type: pdf',
                'attach.resume.required' => 'The Resume attachment field is required',
                'attach.resume.mimes' => 'The Resume attachment must be a file of type: pdf',
                'attach.education_documents.required' => 'The Education Documents attachment field is required',
                'attach.aadhar_card.required' => 'The Aadhar Card attachment field is required',
                'attach.aadhar_card.mimes' => 'The Aadhar Card attachment must be a file of type: pdf,jpg',
                'attach.pan_card.required' => 'The Pan Card attachment field is required',
                'attach.pan_card.mimes' => 'The Pan Card attachment must be a file of type: pdf,jpg',
                'attach.bank_passbook.required' => 'The Bank Passbook/Cheque attachment field is required',
                'attach.bank_passbook.mimes' => 'The Bank Passbook/Cheque attachment must be a file of type: pdf,jpg',
                'attach.offer_accept.required' => 'The offer letter acceptancey mail attachment field is required',
                'attach.offer_accept.mimes' => 'The offer letter acceptancey mail attachment must be a file of type: pdf',

            ];
    }
}
