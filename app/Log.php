<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'logs';

    // protected $fillable = ['name','status'];

    public function getCreatedAtAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d/m/Y H:i:s');
        }
        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d/m/Y H:i:s');
        }
        return $value;
    }
}
