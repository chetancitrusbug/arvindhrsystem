<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resignation extends Model
{
    use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'resignation';

    protected $primaryKey = 'id';
	
	protected $guarded = ['id'];
	
	protected $appends = ['islastday','application_url'];
	

	
	public function getIslastdayAttribute()
    {
		$res = 0;
		if($this->last_working_date == "" || !$this->last_working_date){
			$res = 0;
		}else{
			if(\Carbon\Carbon::now() >= $this->last_working_date){
				$res = 1;
			}
		}
        
		return $res;
    }
	
	public function employee()
    {
        return $this->hasOne('App\Employee', 'id', 'employee_id');
    }
	public function storemanager()
    {
        return $this->hasOne('App\User', 'id', 'request_by');
    }
	public function regionmanager()
    {
        return $this->hasOne('App\User', 'id', 'approve_by');
    }
	public function getApplicationUrlAttribute()
    {
		if($this->application_copy && \File::exists(public_path()."/uploads/resignation/".$this->application_copy)){
			$path = url("/")."/"."uploads/resignation/";
			return $path.$this->application_copy;
		}else{
			return $this->application_copy;
		}
		
        
    }
    

    
}
