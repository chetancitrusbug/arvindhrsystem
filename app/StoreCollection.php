<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreCollection extends Model
{
    use SoftDeletes;

    protected $table = 'store_collections';

    protected $fillable = ['store_id','amount','month','year'];
}
