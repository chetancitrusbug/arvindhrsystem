<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttendanceRequest extends Model
{
    // use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attendance_request';

    protected $fillable = ['attendance_id','employee_id','attendance','note','status'];

    public function getDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d/m/Y');
        }
        return $value;
    }
}
