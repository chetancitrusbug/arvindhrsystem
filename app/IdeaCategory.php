<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IdeaCategory extends Model
{
    //
    protected $table = 'idea_categories';
    protected $fillable = ['idea_name','status'];
}
