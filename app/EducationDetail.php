<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EducationDetail extends Model
{
    use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'education_detail';

    protected $fillable = ['employee_id','education_category_id','board_uni','institute','qualification','specialization','month','year'];

    public function getCategoryName(){
        return $this->belongsTo('App\EducationCategory','education_category_id','id');
    }
}
