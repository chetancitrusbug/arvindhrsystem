<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherInfo extends Model
{
    protected $table = 'other_infos';

    protected $fillable = ['employee_id','dol','process_status','appointment_status','id_card_status','uniform_status','l0l1_status','l0l1_result','l0l1_certificate_status','background_verification','bv_report_status','is_disabled','disability_desc','allocated_leave','pending_leave','availed_leave','salary'];
}
