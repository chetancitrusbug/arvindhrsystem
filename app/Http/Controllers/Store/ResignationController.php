<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\Resignation;
use App\User;
use App\Log;
use Carbon\Carbon as Carbon;
use App\Helper;
use App\ResignationReason;
use App\StoreLocation;

use Auth;

class ResignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
       // view()->share('route', 'separation');
      //  view()->share('module', 'Separation');
    }

    public function index(Request $request)
    {
		//$module = Resignation::where("employee_id",">",1)->get(); dd($module);
		//$employee = Employee::where("store_id",Auth::user()->store_id)->pluck('full_name','id')->toArray();
        $employee = Employee::where('store_id',Auth::user()->store_id)->where('rm_absconding_status','')->where('employee.hrms_status',"2")->pluck('full_name','id')->toArray();
         $reason_parent = ResignationReason::where('parent_id',0)
                                    ->where('status','Active')
                                    ->pluck('reason','id')->toArray();
        return view('storeManager.resignation.index',compact('employee','reason_parent'));
	}

    public function datatable(Request $request)
    {
        $employee = Resignation::select(['employee.*','resignation.id','last_working_date','resignation.resignation_status',
				'resignation.created_at'])
				->join('employee','employee.id','=','resignation.employee_id')->where('employee.store_id',Auth::user()->store_id)
				//->where('resignation.resignation_status',"!=","complete")
                ->orderBy('resignation.updated_at','DESC');

		if($request->has('status') && $request->status != ""){
			$employee->where('resignation.resignation_status',$request->status);
		}
		$employee = $employee->get();
        return Datatables::of($employee)
            ->make(true);
    }

	public function store(Request $request)
    {


		$this->validate($request, [
            'employee_id' => 'required',
            "last_working_date"=>"required",
            'reason_id' => "required",
            'sub_reason_id' => "required",
            'date_of_resignation' => 'required',
        ]);


		$employee = Employee::where("id",$request->employee_id)->where('employee.store_id',Auth::user()->store_id)->first();

		$result = array();

        $requestData = $request->except("document");
		$requestData['request_to'] = "region";
        $requestData['resignation_status'] = "pending";
		$requestData['request_by'] = Auth::user()->id;

		if($employee){
			if ($request->hasFile('document')) {

				$name = $this->uploadFile($request);
				if($name){
					$requestData['application_copy'] = $name;
				}
			}
			$module = Resignation::where("employee_id",$request->employee_id)->first();

			//dd($requestData);
			if($module){
				$module->update($requestData);
				$result['message'] = "Resignation request create successfully";
			}else{
				$module = Resignation::create($requestData);
                $result['message'] = "Resignation request create successfully";

            }
            $employee->hold_salary = 'YES';
            $employee->processstatus = 'Active-Resigned';
			$employee->save();
			$store = \App\StoreLocation::find($employee->store_id);
			if($store){
				$store->actual_man_power = $store->actual_man_power - 1;
				$store->save();
			}
			$result['code'] = 200;
		}else{
			$result['message'] = "Access denied.";
			$result['code'] = 400;
		}


        if($request->ajax()){
			return response()->json($result, $result['code']);
        }else{
			if($result['code'] == 200){
					Session::flash('flash_success',$result['message']);
			}else{
				Session::flash('flash_error',$result['message']);
			}

             return redirect('store/resignation');
        }


    }

	public function show($id)
    {
		$result = array();


        $item = Resignation::whereId($id)->first();

		$data = [];

		if($item && $item->employee){
			$data['employee'] = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($item->employee_id);
			$data['item'] = $item;

			if($item && $item->storemanager){
			$data['storemanager'] = $item->storemanager;
			}
			if($item && $item->regionmanager){
				$data['regionmanager'] = $item->regionmanager;
			}
			$result['code'] = 200;
		}else{
			$result['code'] = 400;

		}

		$result['data'] = $data;
		return response()->json($result, $result['code']);
    }

	public function update($id, Request $request)
    {
		$result = array();

		if($request->has('resignation_status') && $request->resignation_status == "complete"){
			$this->validate($request, [
				'document' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf',
				'rid' => 'required',
			]);
		}else{
			$this->validate($request, [
				'resignation_status' => 'required',
				'rid' => 'required',
			]);
		}



		$item = Resignation::where("resignation.id",$request->rid)->first();

		if($item && $item->employee && $item->employee->store_id == Auth::user()->store_id){

			if ($request->hasFile('document')) {

				$name = $this->uploadFile($request);
				if($name){
					$item->agreement_copy = $name;
				}
			}
			$item->resignation_status = $request->resignation_status;
			$item->save();
			
			if($request->has('resignation_status') && $request->resignation_status == "complete"){
				Session::flash('flash_success',"Employee Relieved..");
				$store = StoreLocation::find($item->employee->store_id);
				if($store){
					//$store->actual_man_power = $store->actual_man_power - 1;
					//$store->save();
				}
				$employee = $item->employee;
				$employee->delete();
			}
			if($request->has('resignation_status') && $request->resignation_status == "decline"){
				$store = StoreLocation::find($employee->store_id);
				if($store){
					$store->actual_man_power = $store->actual_man_power + 1;
					$store->save();
				}
				Session::flash('flash_success',"Resignation request declined..");
				$item->delete();
			}
		}else{
			Session::flash('flash_error',"Access denied..");
		}

		return redirect()->back();

    }

	public function resignationLetterSubmit(Request $request)
    {
		$this->validate($request, [
			'rid' => 'required',
		]);


		$array = $request->except('_token');

		foreach($array as $key => $value){
			if(!$value || $value==null){
				$array[$key] = "";
			}
		}



		$item = Resignation::where("resignation.id",$request->rid)->first();

		if ($item && $item->employee && $item->employee->store_id == Auth::user()->store_id){
			$item->resignation_form = json_encode($array);
			$item->resignation_status = "documentation";
			$item->save();
			Session::flash('flash_success',"Resignation Form submited ..");
		}else{
			Session::flash('flash_error',"Access denied..");
		}
		return redirect('store/resignation-letter/'.$request->rid);

    }

	public function resignationLetterDownload($id)
    {
		$result = array();

        $resignation = Resignation::whereId($id)->first();
		$employee = null;

        $reason_parent = ResignationReason::where('parent_id',0)
                                    ->where('status','Active')
                                    ->pluck('reason','id')->toArray();

		if($resignation && $resignation->employee){
			$employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($resignation->employee_id);

			if($resignation->resignation_status == "documentation" || $resignation->resignation_status == "complete" ){
				$fd = json_decode($resignation->resignation_form,true);
				//dd($form_data);
				return view('storeManager.resignation.print',compact('resignation','employee','fd'));
			}else{
				return view('storeManager.resignation.letter-form',compact('resignation','employee','reason_parent'));
			}
		}else{
			Session::flash('flash_error',"Access denied..");
			return redirect()->back();
		}

    }


	public function destroy($id,Request $request)
    {

        $item = Resignation::where("resignation.id",$id)->first();

        if($item && $item->employee && $item->employee->store_id == Auth::user()->store_id){
			// Employee::whereId($item->employee_id)->update(["resignation_status"=>"non"]);
            Resignation::whereId($id)->delete();
            $item->employee();
            $employee = Employee::where('id',$item->employee->id)->first();
            $employee->hold_salary = 'NO';
            $employee->processstatus = 'Active';
            $employee->save();
			$item->delete();
			$store = StoreLocation::find($employee->store_id);
			if($store){
				$store->actual_man_power = $store->actual_man_power + 1;
				$store->save();
			}
            $result['message'] = "resignation request cancelled .";
            $result['code'] = 200;

        }else{
            $result['message'] = "Access denied..";
            $result['code'] = 400;
        }

		if($request->ajax()){
			Session::flash('flash_message',$result['message']);
          //  return response()->json($result, $result['code']);
        }else{
            //Session::flash('flash_message',$result['message']);
           // return redirect('admin/module-units');
        }
    }
	public function uploadFile(Request $request)
    {
        $name = null;
        if ($request->hasFile('document')) {
            $file = $request->file('document');
            $timestamp = uniqid();
            $name = $timestamp.'.'.$file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/resignation',$name);
		}
		return $name;
    }

    public function getsubreason(Request $request,$id)
    {
        $sub_reason = array();
        $id = $request->reason_id;
        $sub_reason = ResignationReason::where('parent_id',$id)->get()->toArray();
        return response()->json($sub_reason);

    }

    public function Resignation_Update(Request $request)
    {
        $this->validate($request, [
			'rid' => 'required',
        ]);

        $resignation = \App\Resignation::find($request->rid);
        if(!empty($resignation)){
            if(isset($request->reason_id) && $request->reason_id !=  $resignation->reason_id){
                $resignation->reason_id = $request->reason_id;
            }
            if(isset($request->sub_reason_id) && $request->sub_reason_id !=  $resignation->sub_reason_id){
                $resignation->sub_reason_id = $request->sub_reason_id;
            }
            if(isset($request->date_of_resignation) && $request->date_of_resignation !=  $resignation->date_of_resignation){
                $resignation->date_of_resignation = $request->date_of_resignation;
            }
            if(isset($request->last_working_date) && $request->last_working_date !=  $resignation->last_working_date){
                $resignation->last_working_date = $request->last_working_date;
            }
            if ($request->hasFile('document')) {
				$name = $this->uploadFile($request);
				if($name){
					$resignation->application_copy = $name;
				}
			}
            $resignation->save();
            Session::flash('flash_message',"Resignation Updated");
        }
        return redirect()->back();
    }

}
