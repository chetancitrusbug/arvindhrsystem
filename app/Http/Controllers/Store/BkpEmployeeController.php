<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\User;
use App\Role;
use App\Log;
use Carbon\Carbon as Carbon;

class BkpEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.store.employee');
        $this->middleware('permission:access.store.employee.edit')->only(['edit','update']);
        $this->middleware('permission:access.store.employee.create')->only(['create', 'store']);
        $this->middleware('permission:access.store.employee.delete')->only('destroy');

        view()->share('route', 'employee');
        view()->share('module', 'Employee');
    }

    public function index(Request $request)
    {
        return view('storeManager.employee.index');
    }

    public function datatable(Request $request)
    {
        $employee = Employee::select([
                    'employee.*',
                ])
                ->orderBy('employee.id','DESC')
                ->get();

        return Datatables::of($employee)
            ->make(true);
    }

    public function logView(Request $request,$id)
    {
        view()->share('id',$id);
        return view('storeManager.employee.log');
    }

    public function log(Request $request,$id)
    {
        $logs = Log::select([
                    'logs.*',
                    'users.emp_name as user_name',
                    'roles.label as user_role',
                ])
                ->leftJoin('users',function($join){
                    $join->on('users.id','=','logs.user_id');
                })
                ->leftJoin('roles',function($join){
                    $join->on('roles.id','=','logs.user_role_id');
                })
                // ->where('user_id',auth()->id())
                ->where('logs.emp_id',$id)
                ->orderBy('logs.id','DESC')
                ->latest()->get();

        return Datatables::of($logs)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);

        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $subDepartments = $city = $states = [];
        if(old('department_id',null)){
            $subDepartments = $this->getSubDepartments($request,['department_id'=>old('department_id',null)]);
        }
        view()->share('subDepartments',$subDepartments);

        $businessUnits = BusinessUnit::where("status","1")->pluck('name','id')->toArray();
        view()->share('businessUnits',$businessUnits);

        $brands = Brand::where("status","1")->pluck('name','id')->toArray();
        view()->share('brands',$brands);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->pluck('name','id')->toArray();
        view()->share('employeeClassification',$employeeClassification);

        $sourceName = SourceCategory::where("status","1")->pluck('source_name','id')->toArray();
        view()->share('sourceName',$sourceName);

        $variablePayType = VariablePayType::where("status","1")->pluck('name','id')->toArray();
        view()->share('variablePayType',$variablePayType);

        $region = Region::where("status","1")->pluck('name','id')->toArray();
        view()->share('region',$region);

        $locationCode = StoreLocation::find(auth()->user()->store_id);
        view()->share('locationCode',$locationCode);

        if(old('region_id',null)){
            $states = $this->getStateFromRegion($request,['region_id'=>old('region_id',null)]);
        }
        view()->share('states',$states);

        if(old('state_id',null)){
            $city = $this->getCityFromState($request,['state_id'=>old('state_id',null)]);
        }
        view()->share('city',$city);

        return view('storeManager.employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['user_id'] = auth()->id();
        $requestData['status'] = 'pending';
        $requestData['joining_date'] = Carbon::parse($requestData['joining_date'])->format('Y-m-d');

        $employee = Employee::create(array_except($requestData,['attach']));

        // create log
        $log['title'] = "Employee Create";
        $log['emp_id'] = $employee->id;
        $log['emp_name'] = $request->name;
        $log['created_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        if(isset($requestData['attach']) && !empty($requestData['attach'])){
            $name = str_replace(' ','_',$request->name);
            foreach ($requestData['attach'] as $key=>$value) {
                $file = [];
                $file['key'] = $key;
                $file['employee_id'] = $employee->id;

                if(in_array($key, ['education_documents','previous_company_docs'])){
                    foreach ($value as $doc_key=>$doc_value) {
                        $filename = $name.'_'.uniqid(time()) . '.' . $doc_value->getClientOriginalExtension();
                        $doc_value->move('uploads/employee/'.$key, $filename);

                        $file['file'] = 'uploads/employee/'.$key.'/'.$filename;
                        $attachment = Attachment::create($file);
                    }
                }else{
                    $filename = $name.'_'.uniqid(time()) . '.' . $value->getClientOriginalExtension();
                    $value->move('uploads/employee/'.$key, $filename);
                    $file['file'] = 'uploads/employee/'.$key.'/'.$filename;
                    $attachment = Attachment::create($file);
                }
            }
        }

        return redirect('store/employee')->with('flash_success', 'Employee added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = Employee::with(['grade','designation','subDepartment','subDepartment.department','businessUnit','legalEntity','employeeClassification','sourceCategory','variablePayType','brand','state','city','region','attachment'])->find($id);

        return view('storeManager.employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        $employee->attach = Attachment::where('employee_id',$id)->get();
        $attach = [];
        foreach ($employee->attach as $value) {
            if(in_array($value->key, ['education_documents','previous_company_docs'])){
                $attach[$value->key][$value->id]['file'] = $value->file;
                $attach[$value->key][$value->id]['key'] = $value->key;
            }else{
                $attach[$value->key]['file'] = $value->file;
                $attach[$value->key]['key'] = $value->key;
            }
        }
        $employee->attach = $attach;

        $request = new Request();

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);

        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $subDepartments = $city = $states = [];
        if(old('department_id',$employee->department_id)){
            $subDepartments = $this->getSubDepartments($request,['department_id'=>old('department_id',$employee->department_id)]);
        }
        view()->share('subDepartments',$subDepartments);

        $sourceDetail = $this->getSourceDetail($request,old('source_name',$employee->source_name));
        $employee->source_category = isset($sourceDetail->category)?$sourceDetail->category:'';
        $employee->source_code = isset($sourceDetail->source_code)?$sourceDetail->source_code:'';

        $businessUnits = BusinessUnit::where("status","1")->pluck('name','id')->toArray();
        view()->share('businessUnits',$businessUnits);

        $brands = Brand::where("status","1")->pluck('name','id')->toArray();
        view()->share('brands',$brands);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->pluck('name','id')->toArray();
        view()->share('employeeClassification',$employeeClassification);

        $sourceName = SourceCategory::where("status","1")->pluck('source_name','id')->toArray();
        view()->share('sourceName',$sourceName);

        $variablePayType = VariablePayType::where("status","1")->pluck('name','id')->toArray();
        view()->share('variablePayType',$variablePayType);

        $region = Region::where("status","1")->pluck('name','id')->toArray();
        view()->share('region',$region);

        if(old('region_id',$employee->region_id)){
            $states = $this->getStateFromRegion($request,['region_id'=>old('region_id',$employee->region_id)]);
        }
        view()->share('states',$states);

        $locationCode = StoreLocation::where("status","1")->pluck('location_code','id')->toArray();
        view()->share('locationCode',$locationCode);

        if(old('state_id',$employee->state_id)){
            $city = $this->getCityFromState($request,['state_id'=>old('state_id',$employee->state_id)]);
        }
        view()->share('city',$city);

        return view('storeManager.employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['joining_date'] = Carbon::parse($requestData['joining_date'])->format('Y-m-d');
        $requestData['io_code'] = $requestData['store_name'] = $requestData['location_code'];

        if($request->button == 'Update & Send to RM'){
            $requestData['store_status'] = '1';
        }

        $employee = Employee::where('id',$id)->update(array_except($requestData,['attach','_method','_token','button','source_category','source_code']));

        if(isset($requestData['attach']) && !empty($requestData['attach'])){
            $name = str_replace(' ','_',$request->name);
            foreach ($requestData['attach'] as $key=>$value) {
                $file = [];
                $file['key'] = $key;
                $file['employee_id'] = $id;

                if(in_array($key, ['education_documents','previous_company_docs'])){
                    $oldDocs = Attachment::where('key',$key)->where('employee_id',$id)->get();

                    if(count($oldDocs)){
                        foreach ($oldDocs as $k => $v) {
                            if(file_exists($v->file)){
                                unlink($v->file); //delete previously uploaded Attachment
                            }
                        }
                    }

                    Attachment::where('key',$key)->where('employee_id',$id)->delete();

                    foreach ($value as $doc_key=>$doc_value) {
                        $filename = $name.'_'.uniqid(time()) . '.' . $doc_value->getClientOriginalExtension();
                        $doc_value->move('uploads/employee/'.$key, $filename);

                        $file['file'] = 'uploads/employee/'.$key.'/'.$filename;
                        $attachment = Attachment::create($file);
                    }
                }else{
                    $oldDocs = Attachment::where('key',$key)->where('employee_id',$id)->get();

                    if(count($oldDocs)){
                        foreach ($oldDocs as $k => $v) {
                            if(file_exists($v->file)){
                                unlink($v->file); //delete previously uploaded Attachment
                            }
                        }
                    }

                    Attachment::where('key',$key)->where('employee_id',$id)->delete();

                    $filename = $name.'_'.uniqid(time()) . '.' . $value->getClientOriginalExtension();
                    $value->move('uploads/employee/'.$key, $filename);
                    $file['file'] = 'uploads/employee/'.$key.'/'.$filename;
                    $attachment = Attachment::create($file);
                }
            }
        }

        // create log
        $log['title'] = "Employee Update";
        $log['emp_id'] = $id;
        $log['emp_name'] = $request->name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        if($request->button == 'Update & Send to RM'){
            return redirect('store/employee')->with('flash_success', 'Employee updated and request send to region manager!');
        }
        return redirect('store/employee')->with('flash_success', 'Employee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $employee = Employee::find($id);

        //delete certificates
        $oldDocs = Attachment::where('employee_id',$id)->get();

        if(count($oldDocs)){
            foreach ($oldDocs as $key => $value) {
                if(file_exists($value->file)){
                    unlink($value->file); //delete previously uploaded Attachment
                }
            }
        }

        Attachment::where('employee_id',$id)->delete();

        $employee->delete();

        if($request->has('from_index')){
            $message = "Employee Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee deleted!');

            return redirect('store/employee');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name' => 'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'joining_date' => 'required|date_format:d/m/Y',
            'grade_id' => 'required',
            'designation_id' => 'required',
            'department_id' => 'required',
            'sub_department_id' => 'required',
            'business_unit_id' => 'required',
            'reporting_manager_employee_code' => 'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'reporting_manager_name' => 'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'brand_id' => 'required',
            'legal_entity_id' => 'required',
            'location_code' => ['required'],
            'io_code' => ['required'],
            'employee_classification_id' => 'required',
            'source_category' => 'required',
            'source_name' => 'required',
            'source_code' => 'required',
            'employee_refferal_amount' => 'required|numeric',
            'employee_refferal_payment_month' => 'required',
            'ctc' => 'required|numeric',
            'soft_copy_attached' => 'required',
            'pod_no' => 'required|max:50',
            'state_id' => 'required',
            'city_id' => 'required',
            'region_id' => 'required',
            'store_name' => ['required','max:191'],
            'attach.scanned_photo' => 'mimes:jpg,jpeg',
            'attach.employment_form' => 'mimes:xls,xlsx,csv',
            'attach.form_11' => 'mimes:pdf',
            'attach.form_02' => 'mimes:pdf',
            'attach.form_f' => 'mimes:pdf',
            'attach.hiring_checklist' => 'mimes:pdf',
            'attach.inteview_assessment' => 'mimes:pdf',
            'attach.resume' => 'mimes:pdf',
            'attach.aadhar_card' => 'mimes:pdf,jpg,jpeg',
            'attach.pan_card' => 'mimes:pdf,jpg,jpeg',
            'attach.bank_passbook' => 'mimes:pdf,jpg,jpeg',
            'attach.offer_accept' => 'mimes:pdf',
            'attach.education_documents' => [function($attribute,$value,$fail) use ($request,$id){
                                            $format = ['pdf','jpg','jpeg','zip'];
                                            $status = false;
                                            foreach ($request->attach['education_documents'] as $value) {
                                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                                    $status = true;
                                                }
                                            }
                                            if($status){
                                                $fail('The education documents must be a file of type: pdf,jpg,jpeg,zip');
                                            }
                                        }],
            'attach.previous_company_docs' => [function($attribute,$value,$fail) use ($request,$id){
                                            $format = ['pdf','jpg','jpeg','zip'];
                                            $status = false;
                                            foreach ($request->attach['previous_company_docs'] as $value) {
                                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                                    $status = true;
                                                }
                                            }
                                            if($status){
                                                $fail('The previous company documents must be a file of type: pdf,jpg,jpeg,zip');
                                            }
                                        }],
        ];

        $rules2 = [];
        if($id == 0){
            $rules2 = [
                'attach.scanned_photo' => 'required|mimes:jpg,jpeg',
                'attach.employment_form' => 'required|mimes:xls,xlsx,csv',
                'attach.form_11' => 'required|mimes:pdf',
                'attach.form_02' => 'required|mimes:pdf',
                'attach.form_f' => 'required|mimes:pdf',
                'attach.hiring_checklist' => 'required|mimes:pdf',
                'attach.inteview_assessment' => 'required|mimes:pdf',
                'attach.resume' => 'required|mimes:pdf',
                'attach.education_documents' => ['required',function($attribute,$value,$fail) use ($request,$id){
                                            $format = ['pdf','jpg','jpeg','zip'];
                                            $status = false;
                                            foreach ($request->attach['education_documents'] as $value) {
                                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                                    $status = true;
                                                }
                                            }
                                            if($status){
                                                $fail('The education documents must be a file of type: pdf,jpg,jpeg,zip');
                                            }
                                        }],
                'attach.aadhar_card' => 'required|mimes:pdf,jpg,jpeg',
                'attach.pan_card' => 'required|mimes:pdf,jpg,jpeg',
                'attach.bank_passbook' => 'required|mimes:pdf,jpg,jpeg',
                'attach.offer_accept' => 'required|mimes:pdf',
                'attach.previous_company_docs' => ['required',function($attribute,$value,$fail) use ($request,$id){
                                            $format = ['pdf','jpg','jpeg','zip'];
                                            $status = false;
                                            foreach ($request->attach['previous_company_docs'] as $value) {
                                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                                    $status = true;
                                                }
                                            }
                                            if($status){
                                                $fail('The previous company documents must be a file of type: pdf,jpg,jpeg,zip');
                                            }
                                        }],
            ];
        }

        return $this->validate($request, array_merge($rules,$rules2),[
            'grade_id.required' => 'The grade field is required',
            'designation_id.required' => 'The designation field is required',
            'department_id.required' => 'The department field is required',
            'sub_department_id.required' => 'The sub department field is required',
            'business_unit_id.required' => 'The business unit field is required',
            'brand_id.required' => 'The brand field is required',
            'legal_entity_id.required' => 'The legal entity field is required',
            'employee_classification_id.required' => 'The employee classification field is required',
            'source_category.required' => 'The source category field is required',
            'soft_copy_attached.required' => 'The completed soft copy of Joining kit attached field is required',
            'state_id.required' => 'The state field is required',
            'city_id.required' => 'The city field is required',
            'region_id.required' => 'The region field is required',
            'attach.scanned_photo.mimes' => 'The scanned photo must be a file of type: jpg.',
            'attach.scanned_photo.required' => 'The scanned photo field is required',
            'attach.employment_form.mimes' => 'The employment form attachment must be a file of type:  xls, xlsx, csv.',
            'attach.employment_form.required' => 'The employment form attachment field is required',
            'attach.form_11.required' => 'The form-11 attachment field is required',
            'attach.form_11.mimes' => 'The form-11 attachment must be a file of type:  pdf.',
            'attach.form_02.required' => 'The form-02 attachment field is required',
            'attach.form_02.mimes' => 'The form-02 attachment must be a file of type:  pdf',
            'attach.form_f.required' => 'The form-f attachment field is required',
            'attach.form_f.mimes' => 'The form-f attachment must be a file of type:  pdf',
            'attach.hiring_checklist.required' => 'The Hiring Checklist attachment field is required',
            'attach.hiring_checklist.mimes' => 'The Hiring Checklist attachment must be a file of type:  pdf',
            'attach.inteview_assessment.required' => 'The Interview Assessment attachment field is required',
            'attach.inteview_assessment.mimes' => 'The Interview Assessment must be a file of type:  pdf',
            'attach.resume.required' => 'The Resume attachment field is required',
            'attach.resume.mimes' => 'The Resume attachment must be a file of type:  pdf',
            'attach.education_documents.required' => 'The Education Documents attachment field is required',
            'attach.aadhar_card.required' => 'The Aadhar Card attachment field is required',
            'attach.aadhar_card.mimes' => 'The Aadhar Card attachment must be a file of type:  pdf,jpg,jpeg',
            'attach.pan_card.required' => 'The Pan Card attachment field is required',
            'attach.pan_card.mimes' => 'The Pan Card attachment must be a file of type:  pdf,jpg,jpeg',
            'attach.bank_passbook.required' => 'The Bank Passbook/Cheque attachment field is required',
            'attach.bank_passbook.mimes' => 'The Bank Passbook/Cheque attachment must be a file of type:  pdf,jpg,jpeg',
            'attach.offer_accept.required' => 'The offer letter acceptancey mail attachment field is required',
            'attach.offer_accept.mimes' => 'The offer letter acceptancey mail attachment must be a file of type:  pdf',
            'attach.previous_company_docs.required' => 'The Previouse company offer letter / payslips / releiving letter attachment field is required',
        ]);
    }

    public function getSubDepartments(Request $request,$departmentId=0)
    {
        return SubDepartment::where("status","1")->where('department_id',$request->get('department_id',$departmentId))->pluck('name','id')->toArray();
    }

    public function getSourceDetail(Request $request,$name=0)
    {
        return SourceCategory::where("status","1")->where('id',$request->get('name',$name))->first();
    }

    public function getLocationDetail(Request $request,$locationCode=0)
    {
        return StoreLocation::where("status","1")->where('id',$request->get('locationCode',$locationCode))->first();
    }

    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function getStateFromRegion(Request $request,$region=0)
    {
        return State::where("status","1")->where('region_id',$request->get('region_id',$region))->pluck('name','id')->toArray();
    }

    public function sendRequestToRegionManager($id)
    {
        /*$role = Role::where("name","RM")->first();

        $rm = \DB::table('role_user')->where('role_id',$role->id)->first();

        $user = User::find($rm->user_id);*/

        $employee = Employee::find($id);
        $employee->store_status = 1;
        $employee->save();

        // create log
        $log['title'] = "Request Send to Regional HR";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        Session::flash('flash_success', 'Request send to Regional HR !');

        return redirect('store/employee');
    }

    public function showUpload()
    {
        return view('storeManager.employee.upload');
    }

    public function upload(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->get();

            $grade = array_change_key_case(Grade::pluck('id','name')->toArray(), CASE_LOWER);
            $designation = array_change_key_case(Designation::pluck('id','name')->toArray(), CASE_LOWER);
            $department = array_change_key_case(Department::pluck('id','name')->toArray(), CASE_LOWER);
            $departmentId = SubDepartment::pluck('department_id','id')->toArray();
            $subDepartment = array_change_key_case(SubDepartment::pluck('id','name')->toArray(), CASE_LOWER);
            $businessUnit = array_change_key_case(BusinessUnit::pluck('id','name')->toArray(), CASE_LOWER);
            $brand = array_change_key_case(Brand::pluck('id','name')->toArray(), CASE_LOWER);
            $legalEntity = array_change_key_case(LegalEntity::pluck('id','name')->toArray(), CASE_LOWER);
            $locationCode = array_change_key_case(StoreLocation::pluck('id','location_code')->toArray(), CASE_LOWER);
            $employeeClassification = array_change_key_case(EmployeeClassification::pluck('id','name')->toArray(), CASE_LOWER);
            $sourceCategory = array_change_key_case(SourceCategory::pluck('id','category')->toArray(), CASE_LOWER);
            $month = array_change_key_case(config('constants.month_list'), CASE_LOWER);
            $variablePayType = array_change_key_case(VariablePayType::pluck('id','name')->toArray(), CASE_LOWER);
            $joiningKit = array_change_key_case(config('constants.soft_copy_attached'), CASE_LOWER);
            $region = array_change_key_case(Region::pluck('id','name')->toArray(), CASE_LOWER);
            $state = array_change_key_case(State::pluck('id','name')->toArray(), CASE_LOWER);
            $stateId = array_change_key_case(City::pluck('state_id','id')->toArray(), CASE_LOWER);
            $city = array_change_key_case(City::pluck('id','name')->toArray(), CASE_LOWER);

            if($data->count()){
                $valLocationCode = $valSourceCategory = $valJoiningKit = $valCtc = $valAmount = $valMonth = [];

                foreach ($data as $key => $value) {
                    $arr[$key]['user_id'] = auth()->id();
                    $arr[$key]['status'] = 'pending';
                    $arr[$key]['name'] = $value->employee_full_name_as_per_aadhar_card;
                    $arr[$key]['joining_date'] = $value->date_of_joining->format('Y-m-d');
                    $arr[$key]['reporting_manager_employee_code'] = $value->reporting_manager_employee_code;
                    $arr[$key]['reporting_manager_name'] = $value->reporting_manager_name;
                    $arr[$key]['pod_no'] = $value->pod_no;

                    if(array_key_exists(caseChange($value->grade),$grade)){
                        $arr[$key]['grade_id'] = $grade[caseChange($value->grade)];
                    }else{
                        $gradeInsert['name'] = $value->grade;
                        $gradeInsert['status'] = 1;
                        $gradeCreate = Grade::create($gradeInsert);
                        $arr[$key]['grade_id'] = $gradeCreate->id;
                        $grade = array_merge([caseChange($value->grade)=>$gradeCreate->id], $grade);
                    }

                    if(array_key_exists(caseChange($value->designation),$designation)){
                        $arr[$key]['designation_id'] = $designation[caseChange($value->designation)];
                    }else{
                        $designationInsert['name'] = $value->designation;
                        $designationInsert['status'] = 1;
                        $designationCreate = Designation::create($designationInsert);
                        $arr[$key]['designation_id'] = $designationCreate->id;
                        $designation = array_merge([caseChange($value->designation)=>$designationCreate->id], $designation);
                    }

                    if(array_key_exists(caseChange($value->department),$department)){
                        $arr[$key]['department_id'] = $department[caseChange($value->department)];
                    }else{
                        $departmentInsert['name'] = $value->department;
                        $departmentInsert['status'] = 1;
                        $departmentCreate = Department::create($departmentInsert);
                        $arr[$key]['department_id'] = $departmentCreate->id;
                        $department = array_merge([caseChange($value->department)=>$departmentCreate->id], $department);
                    }

                    if(array_key_exists(caseChange($value->sub_department),$subDepartment)){
                        $arr[$key]['sub_department_id'] = $subDepartment[caseChange($value->sub_department)];
                    }else{
                        $subDepartmentInsert['department_id'] = $arr[$key]['department_id'];
                        $subDepartmentInsert['name'] = $value->sub_department;
                        $subDepartmentInsert['status'] = 1;
                        $subDepartmentCreate = SubDepartment::create($subDepartmentInsert);
                        $arr[$key]['sub_department_id'] = $subDepartmentCreate->id;
                        $subDepartment = array_merge([caseChange($value->sub_department)=>$subDepartmentCreate->id], $subDepartment);
                    }

                    if(array_key_exists(caseChange($value->business_unit),$businessUnit)){
                        $arr[$key]['business_unit_id'] = $businessUnit[caseChange($value->business_unit)];
                    }else{
                        $businessUnitInsert['name'] = $value->business_unit;
                        $businessUnitInsert['status'] = 1;
                        $businessUnitCreate = BusinessUnit::create($businessUnitInsert);
                        $arr[$key]['business_unit_id'] = $businessUnitCreate->id;
                        $businessUnit = array_merge([caseChange($value->business_unit)=>$businessUnitCreate->id], $businessUnit);
                    }

                    if(array_key_exists(caseChange($value->brand),$brand)){
                        $arr[$key]['brand_id'] = $brand[caseChange($value->brand)];
                    }else{
                        $brandInsert['name'] = $value->brand;
                        $brandInsert['status'] = 1;
                        $brandCreate = Brand::create($brandInsert);
                        $arr[$key]['brand_id'] = $brandCreate->id;
                        $brand = array_merge([caseChange($value->brand)=>$brandCreate->id], $brand);
                    }

                    if(array_key_exists(caseChange($value->legal_entity),$legalEntity)){
                        $arr[$key]['legal_entity_id'] = $legalEntity[caseChange($value->legal_entity)];
                    }else{
                        $legalEntityInsert['name'] = $value->legal_entity;
                        $legalEntityInsert['status'] = 1;
                        $legalEntityCreate = LegalEntity::create($legalEntityInsert);
                        $arr[$key]['legal_entity_id'] = $legalEntityCreate->id;
                        $legalEntity = array_merge([caseChange($value->legal_entity)=>$legalEntityCreate->id], $legalEntity);
                    }

                    //lcoation code
                    if(array_key_exists(caseChange($value->location_code),$locationCode)){
                        $arr[$key]['location_code'] = trim($value->location_code);
                        $arr[$key]['io_code'] = trim($value->cost_centre_io_code);
                        $arr[$key]['store_name'] = trim($value->store_name);
                    }else{
                        array_push($valLocationCode, $key+2);
                    }

                    if(array_key_exists(caseChange($value->employee_classification),$employeeClassification)){
                        $arr[$key]['employee_classification_id'] = $employeeClassification[caseChange($value->employee_classification)];
                    }else{
                        $employeeClassificationInsert['name'] = $value->employee_classification;
                        $employeeClassificationInsert['status'] = 1;
                        $employeeClassificationCreate = EmployeeClassification::create($employeeClassificationInsert);
                        $arr[$key]['employee_classification_id'] = $employeeClassificationCreate->id;
                        $employeeClassification = array_merge([caseChange($value->employee_classification)=>$employeeClassificationCreate->id], $employeeClassification);
                    }

                    //source category pending
                    if(array_key_exists(caseChange($value->source_category),$sourceCategory)){
                        $arr[$key]['source_name'] = $sourceCategory[caseChange($value->source_category)];
                    }else{
                        array_push($valSourceCategory, $key+2);
                    }

                    //joining kit
                    if(array_key_exists(caseChange($value->completed_soft_copy_of_joining_kit_attached),$joiningKit)){
                        $arr[$key]['soft_copy_attached'] = caseChange($value->completed_soft_copy_of_joining_kit_attached);
                    }else{
                        array_push($valJoiningKit, $key+2);
                    }

                    //ctc
                    if(is_numeric(trim($value->ctc))){
                        $arr[$key]['ctc'] = trim($value->ctc);
                    }else{
                        array_push($valCtc, $key+2);
                    }

                    if(array_key_exists(caseChange($value->variable_pay_type),$variablePayType)){
                        $arr[$key]['variable_pay_type_id'] = $variablePayType[caseChange($value->variable_pay_type)];
                    }else{
                        $variablePayTypeInsert['name'] = $value->variable_pay_type;
                        $variablePayTypeInsert['status'] = 1;
                        $variablePayTypeCreate = VariablePayType::create($variablePayTypeInsert);
                        $arr[$key]['variable_pay_type_id'] = $variablePayTypeCreate->id;
                        $variablePayType = array_merge([caseChange($value->variable_pay_type)=>$variablePayTypeCreate->id], $variablePayType);
                    }

                    if(array_key_exists(caseChange($value->region),$region) || $value->region == ''){
                        $arr[$key]['region_id'] = isset($region[caseChange($value->region)])?$region[caseChange($value->region)]:0;
                    }else{
                        $regionInsert['name'] = $value->region;
                        $regionInsert['status'] = 1;
                        $regionCreate = Region::create($regionInsert);
                        $arr[$key]['region_id'] = $regionCreate->id;
                        $region = array_merge([caseChange($value->region)=>$regionCreate->id], $region);
                    }

                    if(array_key_exists(caseChange($value->state),$state) || $value->state == ''){
                        $arr[$key]['state_id'] = isset($state[caseChange($value->state)])?$state[caseChange($value->state)]:0;
                    }else{
                        $stateInsert['region_id'] = $arr[$key]['region_id'];
                        $stateInsert['name'] = $value->state;
                        $stateInsert['status'] = 1;
                        $stateCreate = State::create($stateInsert);
                        $arr[$key]['state_id'] = $stateCreate->id;
                        $state = array_merge([caseChange($value->state)=>$stateCreate->id], $state);
                    }

                    if(array_key_exists(caseChange($value->city),$city) || $value->city == ''){
                        $arr[$key]['city_id'] = isset($city[caseChange($value->city)])?$city[caseChange($value->city)]:0;
                    }else{
                        $cityInsert['state_id'] = $arr[$key]['state_id'];
                        $cityInsert['name'] = $value->city;
                        $cityInsert['status'] = 1;
                        $cityCreate = City::create($cityInsert);
                        $arr[$key]['city_id'] = $cityCreate->id;
                        $city = array_merge([caseChange($value->city)=>$cityCreate->id], $city);
                    }
                }

                if(count($valAmount) > 0 || count($valCtc) > 0 || count($valJoiningKit) > 0 || count($valSourceCategory) > 0 || count($valLocationCode) > 0 || count($valMonth) > 0){
                    return redirect('store/show-employee-upload')
                                ->with('valAmount', implode(', ', $valAmount))
                                ->with('valCtc', implode(', ', $valCtc))
                                ->with('valJoiningKit', implode(', ', $valJoiningKit))
                                ->with('valSourceCategory', implode(', ', $valSourceCategory))
                                ->with('valLocationCode', implode(', ', $valLocationCode))
                                ->with('valMonth', implode(', ', $valMonth))
                                ->with('flash_error', 'See below errors!');
                }

                if(!empty($arr)){
                    Employee::insert($arr);
                    return redirect('store/employee')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('store/show-employee-upload')
                    ->with('flash_error', 'Request data does not have any files to import.');
    }
}
