<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\User;
use App\Role;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use Carbon\Carbon as Carbon;
use App\Helper;
use App\OtherInfo;
use PDF;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.store.employee');
        $this->middleware('permission:access.store.employee.edit')->only(['edit','update']);
        $this->middleware('permission:access.store.employee.create')->only(['create', 'store']);
        $this->middleware('permission:access.store.employee.delete')->only('destroy');

        view()->share('route', 'employee');
        view()->share('module', 'Employee');
    }

    public function index(Request $request)
    {
        return view('storeManager.employee.index');
    }

    public function datatable(Request $request)
    {
		$storeId = Auth()->user()->store_id;
		//$regionId = Auth()->user()->region;
        $employee = Employee::select([
                    'employee.*',
                    'designation.name as designation',
                    'store_location.store_name',
                    'store_location.location_code',
                ])
                ->leftJoin('designation',function($join){
                    $join->on('employee.designation_id','=','designation.id');
                })
                ->leftJoin('store_location',function($join){
                    $join->on('employee.store_id','=','store_location.id');
                })
				->leftJoin('resignation',function($join){
					  $join->on('employee.id','=','resignation.employee_id')->where('resignation.resignation_status','complete');
				})
                ->where('employee.hrms_status',"2")
                ->where('employee.store_id','=',$storeId)
        //        ->where('rm_absconding_status','!=',"2") // abscording accepted
		//		->where('employee.region_id1','=',$regionId)
                // ->where('user_id',auth()->id())
                ->orderBy('employee.id','DESC')
                ->get();

        return Datatables::of($employee)
            ->make(true);
			/*->setRowAttr([
				'style' => function($item){
					return ($item->rm_absconding_status == 2 || resignation.resignation_status == 'complete' ) ? 'background-color: #ff0000;' : '';
				}
			]); */
    }

    public function logView(Request $request,$id)
    {
        view()->share('id',$id);
        return view('storeManager.employee.log');
    }

    public function log(Request $request,$id)
    {
        $logs = Log::select([
                    'logs.*',
                    'users.emp_name as user_name',
                    'roles.label as user_role',
                ])
                ->leftJoin('users',function($join){
                    $join->on('users.id','=','logs.user_id');
                })
                ->leftJoin('roles',function($join){
                    $join->on('roles.id','=','logs.user_role_id');
                })
                // ->where('user_id',auth()->id())
                ->where('logs.emp_id',$id)
                ->orderBy('logs.id','DESC')
                ->latest()->get();

        return Datatables::of($logs)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return 'not allow to create';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($id);

        if(!$employee){
            return redirect('store/employee')->with('flash_error', 'Employee Not Found!');
        }

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as birth_date"),
                    ])->where('employee_id',$id)->get()->toArray();
        //echo "<pre>"; print_r($employee); exit();
        //dd($employee);

        $employee->otherinfo = OtherInfo::where('employee_id',$id)->first();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        // echo "<pre>"; print_r($employee); exit();
        return view('storeManager.employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        if(!$employee){
            return redirect('store/employee')->with('flash_error', 'Employee Not Found!');
        }

        $request = new Request;
        $currentCity = $permanentCity = [];
        view()->share('is_employee','1');

        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        $city = City::where("status","1")->pluck('name','id')->toArray();
        view()->share('city',$city);

        if(old('current_state',$employee->current_state)){
            $currentCity = $this->getCityFromState($request,['state_id'=>old('current_state',$employee->current_state)]);
        }
        view()->share('currentCity',$currentCity);

        if(old('permanent_state',$employee->permanent_state)){
            $permanentCity = $this->getCityFromState($request,['state_id'=>old('permanent_state',$employee->permanent_state)]);
        }
        view()->share('permanentCity',$permanentCity);

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        $locationCode = StoreLocation::where("status","1")->pluck('location_code','id')->toArray();
        view()->share('locationCode',$locationCode);

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);

        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $subDepartments = [];
        if(old('department_id',$employee->department_id)){
            $subDepartments = $this->getSubDepartments($request,['department_id'=>old('department_id',$employee->department_id)]);
        }
        view()->share('subDepartments',$subDepartments);

        $businessUnits = BusinessUnit::where("status","1")->pluck('name','id')->toArray();
        view()->share('businessUnits',$businessUnits);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->pluck('name','id')->toArray();
        view()->share('employeeClassification',$employeeClassification);

        $variablePayType = VariablePayType::where("status","1")->pluck('name','id')->toArray();
        view()->share('variablePayType',$variablePayType);

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();
        // echo "<pre>"; print_r($employee); exit();
        return view('storeManager.employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validatePersonalInfo($request);

        //$requestData = $request->only(['blood_group','rh_factor','marital_status','nationality','mobile','email','esic_number','emergency_person','emergency_number','emergency_relationship','anniversary_date','ua_number','education','family','joining_date','business_unit_id','legal_entity_id','employee_classification_id','variable_pay_type_id','grade_id','designation_id','department_id','sub_department_id','reporting_manager_name','reporting_manager_employee_code']);
		$requestData = $request->all();

        if($request->marital_status == 'married'){
			$requestData['anniversary_date'] = Helper::ymd(str_replace('/', '-', $requestData['anniversary_date']));
		}else{
			$requestData['anniversary_date'] = null;
		}


        $employee = Employee::where('id',$id)->update(array_except($requestData,['attach','_method','_token','button','education','previous','family','is_employee','same_as_current']));

        /*if(isset($requestData['education']) && !empty($requestData['education'])){
            foreach ($requestData['education'] as $value) {
                    $value['employee_id'] = $id;
                    if(isset($value['id'])){
                        \DB::table('education_detail')->where('id',$value['id'])->update(array_except($value,['education_category_id']));
                    }
            }
        }

        if(isset($requestData['family']) && !empty($requestData['family'])){
            foreach ($requestData['family'] as $value) {
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    \DB::table('family_information')->where('id',$value['id'])->update(['name'=>$value['name']]);
                }
            }
        }*/

        $employee = Employee::find($id);
        // create log
        $log['title'] = "Employee update";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        return redirect('store/employee/createEducationDetail/'.$id)->with('flash_success', 'Personal information updated!');
    }

    public function createEducationDetail(Request $request,$id=0){
        view()->share('id',$id);

        $education = EducationDetail::where('employee_id',$id)->get()->toArray();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        return view('storeManager.employee.education_create',compact('education'));
    }

    public function storeEducationDetail(Request $request,$id=0){

        $this->validateEducationDetail($request);

        $employee = Employee::find($id);
        $employee->add_education = 1;
        $employee->save();

        $requestData = $request->all();

        if(isset($requestData['education']) && !empty($requestData['education'])){
            foreach ($requestData['education'] as $key => $value) {
					$value['employee_id'] = $id;
                    if(isset($value['id'])){
                        \DB::table('education_detail')->where('id',$value['id'])->update($value);
                    }
            }
        }

        $message = 'Education detail updated!';

        return redirect('store/employee/createExperience/'.$id)->with('flash_success',$message);
    }

    public function createExperience(Request $request,$id=0){
        view()->share('id',$id);

        $employee = Employee::find($id);
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        // echo "<pre>"; print_r($employee); exit();
        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        return view('storeManager.employee.experience_create',compact('employee'));
    }

    public function storeExperience(Request $request,$id){
        $this->validateExperience($request);

        $employee = Employee::find($id);
        $employee->add_experience = 1;
        $employee->first_time_employment = $request->has('first_time_employment')? 1:0;
        $employee->save();
		$requestData = $request->all();

        if($employee->first_time_employment == 0){
            if(isset($requestData['previous']) && !empty($requestData['previous'])){
                PreviousExperience::where('employee_id',$id)->delete();

                foreach ($requestData['previous'] as $value) {
                    if($value['joining_date']){
                        $value['joining_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $value['joining_date']))->format('Y-m-d');
                    }
                    if($value['leaving_date']){
                        $value['leaving_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $value['leaving_date']))->format('Y-m-d');
                    }
                    $value['employee_id'] = $id;
					if(isset($value['id'])){
					    $value['deleted_at'] = null;
                        \DB::table('previous_experience')->where('id',$value['id'])->update($value);
                    }else{

                        PreviousExperience::insert($value);
                    }
                }
            }
        }else{
            PreviousExperience::where('employee_id',$id)->delete();
        }

        $message = 'Previous Work Experience updated!';

        return redirect('store/employee/createFamilyInfo/'.$id)->with('flash_success',$message);
    }

    public function createFamilyInfo(Request $request,$id=0){
        view()->share('id',$id);
        $employee = Employee::find($id);
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();
		//dd($employee);
        return view('storeManager.employee.family_create',compact('employee'));
    }

    public function storeFamilyInfo(Request $request,$id){
        $this->validatefamilyInfo($request);

        $employee = Employee::find($id);
        $employee->add_family_info = 1;
        $employee->save();

        $requestData = $request->all();
        // echo "<pre>"; print_r($requestData); exit();
        
        if(isset($requestData['family']) && !empty($requestData['family'])){
            foreach ($requestData['family'] as $value) {
                $value['employee_id'] = $id;
                if(isset($value['id'])){
					if($value['date'] == ''){
                        $value['date'] = '';
                        $value['month'] = '';
                        $value['year'] = '';
                    }else{
                        if($value['date'] == ''){
                            $value['date'] = '';
                            $value['month'] = '';
                            $value['year'] = '';
                        }else{
                            $value['date'] =  date('Y-m-d',strtotime(str_replace('/', '-',$value['date']))); //\Carbon\Carbon::parse($value['date'])->format('Y-m-d');
                            $date = explode('-', $value['date']);
                            $value['date'] = $date[2];
                            $value['month'] = $date[1];
                            $value['year'] = $date[0];
                        }
                    }
                    
                    \DB::table('family_information')->where('id',$value['id'])->update($value);
                }
            }
        }
        return redirect('store/employee/createOtherInfo/'.$id)->with('flash_success','Employee family detail updated successfully');
    //    return redirect('store/employee')->with('flash_success','Employee updated successfully');
    }

    public function validatePersonalInfo($request, $id = 0)
    {
        $education = 1;
        $rules = [
            'blood_group' => 'required',
            'rh_factor' => 'required',

            'marital_status' => 'required',
            'nationality' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'mobile' => ['required','numeric',function($attribute, $value, $fail) use($request,$id) {
                            if(strlen($value) != 10){
                                $fail('The mobile no must be of 10 digits');
                            }
                        }],
            'email' => 'nullable|email',
            'emergency_person' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'emergency_number' => 'required|numeric',
            'emergency_relationship' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'joining_date'=>['date_format:d/m/Y','nullable',
                            function($attribute, $value, $fail) use($request,$id) {
                                if($value != ''){
                                    if(Helper::ymd(str_replace('/', '-', $value)) <= Helper::ymd(Carbon::now())){
                                        return $fail('The joining date must be a date after '.Helper::dmy(Carbon::now()));
                                    }
                                }
                            },
                        ],
        ];
		if($request->marital_status == 'married'){
			$rules ['anniversary_date'] = 'nullable|date_format:d/m/Y';
        } 
        
        return $this->validate($request, $rules,[
            'designation_id.required' => 'The designation field is required.',
            'aadhar_number.required_if' => 'The aadhar number field is required when availability of aadhar is available.',
            'aadhar_enrolment_number.required_if' => 'The aadhar enrolment number field is required when availability of aadhar is not available.',
            'pan_number.required_if' => 'The pan number field is required when availability of PAN is available.',
        ]);
    }

    public function validateEducationDetail($request, $id = 0)
    {
        $education = 1;
        $rules = [
            'education'=>[
                        function($attribute, $value, $fail) use($request,$id) {
                            $status = true;
                            foreach ($value as $value) {
                                if($value['education_category_id'] != '' ||
                                    $value['board_uni'] != '' ||
                                    $value['institute'] != '' ||
                                    $value['qualification'] != '' ||
                                    $value['specialization'] != '' ||
                                    $value['month'] != '' ||
                                    $value['year'] != ''
                                        ){
                                    $status = false;
                                }
                            }
                            if($status){
                                $education = 0;
                                $fail('atleast one education detail is required');
                            }
                        },
                    ],
        ];

        $message = [];

        foreach($request->education as $key=>$value){
            if($key == 0){
				$rules["education.$key.board_uni"]='required|max:191';
				$rules["education.$key.institute"]='required|max:191';
				$rules["education.$key.qualification"]='required|max:191';
				$rules["education.$key.specialization"]='nullable|max:191';
			}else{
				$rules["education.$key.board_uni"]='nullable|max:191';
				$rules["education.$key.institute"]='nullable|max:191';
				$rules["education.$key.qualification"]='nullable|max:191';
				$rules["education.$key.specialization"]='nullable|max:191';
			}
			$message["education.$key.board_uni.required"]='The board / uni is required.';
            $message["education.$key.institute.required"]='The institute is required.';
            $message["education.$key.qualification.required"]='The qualification is required.';
            $message["education.$key.specialization.required"]='The qualification is required.';
            $message["education.$key.board_uni.max"]='The board / uni may not be greater than 191 characters.';
            $message["education.$key.institute.max"]='The institute may not be greater than 191 characters.';
            $message["education.$key.qualification.max"]='The qualification may not be greater than 191 characters.';
            $message["education.$key.specialization.max"]='The qualification may not be greater than 191 characters.';
        }

        return $this->validate($request, $rules, $message);
    }

    public function validateExperience($request, $id = 0)
    {
        $rules = $message = [];

        /*if(isset($request->first_time_employment)){
            foreach($request->previous as $key=>$value){
                $rules["previous.$key.company_name"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.designation"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.reason_of_leaving"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.joining_date"]='nullable|date|date_format:d/m/Y';
                $rules["previous.$key.leaving_date"]='nullable|date|date_format:d/m/Y|after:'.$value['joining_date'];

                $message["previous.$key.company_name.regex"]='The company name format is invalid';
                $message["previous.$key.company_name.max"]='The company name may not be greater than 191 characters.';
                $message["previous.$key.designation.regex"]='The designation format is invalid';
                $message["previous.$key.designation.max"]='The designation may not be greater than 191 characters.';
                $message["previous.$key.reason_of_leaving.regex"]='The reason of leaving format is invalid';
                $message["previous.$key.reason_of_leaving.max"]='The reason of leaving may not be greater than 191 characters.';
                $message["previous.$key.joining_date.date"]='The joining date is not a valid date';
                $message["previous.$key.joining_date.date_format"]='The joining date format will be d/m/Y';
                $message["previous.$key.leaving_date.date"]='The leaving date is not a valid date';
                $message["previous.$key.leaving_date.date_format"]='The leaving date format will be d/m/Y';
                $message["previous.$key.leaving_date.after"]='The leaving date must be a date after '.$value['joining_date'];
            }
        }*/

        return $this->validate($request, $rules, $message);
    }

    public function validatefamilyInfo($request, $id = 0)
    {
        $rules = $message = [];

        foreach($request->family as $key=>$value){
            $rules["family.$key.name"]='nullable|regex:/^[a-z0-9 .\-]+$/i';

            $message["family.$key.name.regex"]='The name format is invalid';
        }

        return $this->validate($request, $rules, $message);
    }
     public function validateOtherInfo($request, $id = 0)
    {
        $rules = $message = [];
        $rules = array(
            'is_disable' => 'required',
            'salary' =>'required',

        );
      /*  foreach($request->family as $key=>$value){
            $rules["family.$key.name"]='nullable|regex:/^[a-z0-9 .\-]+$/i';

            $message["family.$key.name.regex"]='The name format is invalid';
        } */

		$message = array(
            'is_disable.required' => 'Is Pankh field is required.'
        );

        return $this->validate($request, $rules, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        Employee::find($id)->delete();
        EducationDetail::where('employee_id',$id)->delete();
        PreviousExperience::where('employee_id',$id)->delete();
        FamilyInfo::where('employee_id',$id)->delete();

        if($request->has('from_index')){
            $message = "Employee Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee deleted!');

            return redirect('store/employee');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [

            'education'=>[
                        function($attribute, $value, $fail) use($request,$id) {
                            $status = true;
                            foreach ($value as $value) {
                                if($value['education_category_id'] != '' ||
                                    $value['board_uni'] != '' ||
                                    $value['institute'] != '' ||
                                    $value['qualification'] != '' ||
                                    $value['specialization'] != '' ||
                                    $value['month'] != '' ||
                                    $value['year'] != ''
                                        ){
                                    $status = false;
                                }
                            }
                            if($status){
                                $education = 0;
                                $fail('Atleast one education detail is required');
                            }
                        },
                    ],
        ];

        $message = [];
        foreach($request->education as $key=>$value){
            $rules["education.$key.board_uni"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.institute"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.qualification"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.specialization"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';

            $message["education.$key.board_uni.regex"]='The board / uni format is invalid';
            $message["education.$key.board_uni.max"]='The board / uni may not be greater than 191 characters.';
            $message["education.$key.institute.regex"]='The institute format is invalid';
            $message["education.$key.institute.max"]='The institute may not be greater than 191 characters.';
            $message["education.$key.qualification.regex"]='The qualification format is invalid';
            $message["education.$key.qualification.max"]='The qualification may not be greater than 191 characters.';
            $message["education.$key.specialization.regex"]='The specialization format is invalid';
            $message["education.$key.specialization.max"]='The qualification may not be greater than 191 characters.';
        }

        foreach($request->family as $key=>$value){
            $rules["family.$key.name"]='nullable|regex:/^[a-z0-9 .\-]+$/i';

            $message["family.$key.name.regex"]='The name format is invalid';
        }

        return $this->validate($request, $rules,array_merge([
            'aadhar_number.required_if' => 'The aadhar number field is required when availability of aadhar is available.',
            'aadhar_enrolment_number.required_if' => 'The aadhar enrolment number field is required when availability of aadhar is not available.',
            'pan_number.required_if' => 'The pan number field is required when availability of PAN is available.',
        ],$message));
    }

    public function getSubDepartments(Request $request,$departmentId=0)
    {
        return SubDepartment::where("status","1")->where('department_id',$request->get('department_id',$departmentId))->pluck('name','id')->toArray();
    }

    public function getSourceDetail(Request $request,$name=0)
    {
        return SourceCategory::where("status","1")->where('id',$request->get('name',$name))->first();
    }

    public function getLocationDetail(Request $request,$locationCode=0)
    {
        return StoreLocation::where("status","1")->where('id',$request->get('locationCode',$locationCode))->first();
    }

    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function getStateFromRegion(Request $request,$region=0)
    {
        return State::where("status","1")->where('region_id',$request->get('region_id',$region))->pluck('name','id')->toArray();
    }

    public function sendRequestToRegionManager($id)
    {
        $employee = Employee::find($id);
        $employee->store_status = 1;
        $employee->save();

        // create log
        $log['title'] = "Request Send to Regional HR";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        Session::flash('flash_success', 'Request send to Regional HR !');

        return redirect('store/hiring');
    }

    public function showUpload()
    {
        return view('storeManager.employee.upload');
    }

    public function upload(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 300);
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->get();

            $grade = array_change_key_case(Grade::pluck('id','name')->toArray(), CASE_LOWER);
            $designation = array_change_key_case(Designation::pluck('id','name')->toArray(), CASE_LOWER);
            $department = array_change_key_case(Department::pluck('id','name')->toArray(), CASE_LOWER);
            $departmentId = SubDepartment::pluck('department_id','id')->toArray();
            $subDepartment = array_change_key_case(SubDepartment::pluck('id','name')->toArray(), CASE_LOWER);
            $businessUnit = array_change_key_case(BusinessUnit::pluck('id','name')->toArray(), CASE_LOWER);
            $brand = array_change_key_case(Brand::pluck('id','name')->toArray(), CASE_LOWER);
            $legalEntity = array_change_key_case(LegalEntity::pluck('id','name')->toArray(), CASE_LOWER);
            $locationCode = array_change_key_case(StoreLocation::pluck('id','location_code')->toArray(), CASE_LOWER);
            $employeeClassification = array_change_key_case(EmployeeClassification::pluck('id','name')->toArray(), CASE_LOWER);
            $sourceCategory = array_change_key_case(SourceCategory::pluck('id','category')->toArray(), CASE_LOWER);
            $month = array_change_key_case(config('constants.month_list'), CASE_LOWER);
            $variablePayType = array_change_key_case(VariablePayType::pluck('id','name')->toArray(), CASE_LOWER);
            $joiningKit = array_change_key_case(config('constants.soft_copy_attached'), CASE_LOWER);
            $region = array_change_key_case(Region::pluck('id','name')->toArray(), CASE_LOWER);
            $stateId = array_change_key_case(City::pluck('state_id','id')->toArray(), CASE_LOWER);
            $state = array_change_key_case(State::pluck('id','name')->toArray(), CASE_LOWER);
            $city = array_change_key_case(City::pluck('id','name')->toArray(), CASE_LOWER);

            if($data->count()){
                $valLocationCode = $valSourceCategory = $valJoiningKit = $valCtc = $valAmount = $valMonth = [];

                foreach ($data as $key => $value) {
                    $arr[$key]['user_id'] = auth()->id();
                    $arr[$key]['status'] = 'pending';
                    $arr[$key]['name'] = $value->employee_full_name_as_per_aadhar_card;
                    $arr[$key]['joining_date'] = $value->date_of_joining->format('Y-m-d');
                    $arr[$key]['reporting_manager_employee_code'] = $value->reporting_manager_employee_code;
                    $arr[$key]['reporting_manager_name'] = $value->reporting_manager_name;
                    $arr[$key]['pod_no'] = $value->pod_no;

                    if(array_key_exists(caseChange($value->grade),$grade)){
                        $arr[$key]['grade_id'] = $grade[caseChange($value->grade)];
                    }else{
                        $gradeInsert['name'] = $value->grade;
                        $gradeInsert['status'] = 1;
                        $gradeCreate = Grade::create($gradeInsert);
                        $arr[$key]['grade_id'] = $gradeCreate->id;
                        $grade = array_merge([caseChange($value->grade)=>$gradeCreate->id], $grade);
                    }

                    if(array_key_exists(caseChange($value->designation),$designation)){
                        $arr[$key]['designation_id'] = $designation[caseChange($value->designation)];
                    }else{
                        $designationInsert['name'] = $value->designation;
                        $designationInsert['status'] = 1;
                        $designationCreate = Designation::create($designationInsert);
                        $arr[$key]['designation_id'] = $designationCreate->id;
                        $designation = array_merge([caseChange($value->designation)=>$designationCreate->id], $designation);
                    }

                    if(array_key_exists(caseChange($value->department),$department)){
                        $arr[$key]['department_id'] = $department[caseChange($value->department)];
                    }else{
                        $departmentInsert['name'] = $value->department;
                        $departmentInsert['status'] = 1;
                        $departmentCreate = Department::create($departmentInsert);
                        $arr[$key]['department_id'] = $departmentCreate->id;
                        $department = array_merge([caseChange($value->department)=>$departmentCreate->id], $department);
                    }

                    if(array_key_exists(caseChange($value->sub_department),$subDepartment)){
                        $arr[$key]['sub_department_id'] = $subDepartment[caseChange($value->sub_department)];
                    }else{
                        $subDepartmentInsert['department_id'] = $arr[$key]['department_id'];
                        $subDepartmentInsert['name'] = $value->sub_department;
                        $subDepartmentInsert['status'] = 1;
                        $subDepartmentCreate = SubDepartment::create($subDepartmentInsert);
                        $arr[$key]['sub_department_id'] = $subDepartmentCreate->id;
                        $subDepartment = array_merge([caseChange($value->sub_department)=>$subDepartmentCreate->id], $subDepartment);
                    }

                    if(array_key_exists(caseChange($value->business_unit),$businessUnit)){
                        $arr[$key]['business_unit_id'] = $businessUnit[caseChange($value->business_unit)];
                    }else{
                        $businessUnitInsert['name'] = $value->business_unit;
                        $businessUnitInsert['status'] = 1;
                        $businessUnitCreate = BusinessUnit::create($businessUnitInsert);
                        $arr[$key]['business_unit_id'] = $businessUnitCreate->id;
                        $businessUnit = array_merge([caseChange($value->business_unit)=>$businessUnitCreate->id], $businessUnit);
                    }

                    if(array_key_exists(caseChange($value->brand),$brand)){
                        $arr[$key]['brand_id'] = $brand[caseChange($value->brand)];
                    }else{
                        $brandInsert['name'] = $value->brand;
                        $brandInsert['status'] = 1;
                        $brandCreate = Brand::create($brandInsert);
                        $arr[$key]['brand_id'] = $brandCreate->id;
                        $brand = array_merge([caseChange($value->brand)=>$brandCreate->id], $brand);
                    }

                    if(array_key_exists(caseChange($value->legal_entity),$legalEntity)){
                        $arr[$key]['legal_entity_id'] = $legalEntity[caseChange($value->legal_entity)];
                    }else{
                        $legalEntityInsert['name'] = $value->legal_entity;
                        $legalEntityInsert['status'] = 1;
                        $legalEntityCreate = LegalEntity::create($legalEntityInsert);
                        $arr[$key]['legal_entity_id'] = $legalEntityCreate->id;
                        $legalEntity = array_merge([caseChange($value->legal_entity)=>$legalEntityCreate->id], $legalEntity);
                    }

                    //lcoation code
                    if(array_key_exists(caseChange($value->location_code),$locationCode)){
                        $arr[$key]['location_code'] = trim($value->location_code);
                        $arr[$key]['io_code'] = trim($value->cost_centre_io_code);
                        $arr[$key]['store_name'] = trim($value->store_name);
                    }else{
                        array_push($valLocationCode, $key+2);
                    }

                    if(array_key_exists(caseChange($value->employee_classification),$employeeClassification)){
                        $arr[$key]['employee_classification_id'] = $employeeClassification[caseChange($value->employee_classification)];
                    }else{
                        $employeeClassificationInsert['name'] = $value->employee_classification;
                        $employeeClassificationInsert['status'] = 1;
                        $employeeClassificationCreate = EmployeeClassification::create($employeeClassificationInsert);
                        $arr[$key]['employee_classification_id'] = $employeeClassificationCreate->id;
                        $employeeClassification = array_merge([caseChange($value->employee_classification)=>$employeeClassificationCreate->id], $employeeClassification);
                    }

                    //source category pending
                    if(array_key_exists(caseChange($value->source_category),$sourceCategory)){
                        $arr[$key]['source_name'] = $sourceCategory[caseChange($value->source_category)];
                    }else{
                        array_push($valSourceCategory, $key+2);
                    }

                    //joining kit
                    if(array_key_exists(caseChange($value->completed_soft_copy_of_joining_kit_attached),$joiningKit)){
                        $arr[$key]['soft_copy_attached'] = caseChange($value->completed_soft_copy_of_joining_kit_attached);
                    }else{
                        array_push($valJoiningKit, $key+2);
                    }

                    //ctc
                    if(is_numeric(trim($value->ctc))){
                        $arr[$key]['ctc'] = trim($value->ctc);
                    }else{
                        array_push($valCtc, $key+2);
                    }

                    if(array_key_exists(caseChange($value->variable_pay_type),$variablePayType)){
                        $arr[$key]['variable_pay_type_id'] = $variablePayType[caseChange($value->variable_pay_type)];
                    }else{
                        $variablePayTypeInsert['name'] = $value->variable_pay_type;
                        $variablePayTypeInsert['status'] = 1;
                        $variablePayTypeCreate = VariablePayType::create($variablePayTypeInsert);
                        $arr[$key]['variable_pay_type_id'] = $variablePayTypeCreate->id;
                        $variablePayType = array_merge([caseChange($value->variable_pay_type)=>$variablePayTypeCreate->id], $variablePayType);
                    }

                    if(array_key_exists(caseChange($value->region),$region) || $value->region == ''){
                        $arr[$key]['region_id'] = isset($region[caseChange($value->region)])?$region[caseChange($value->region)]:0;
                    }else{
                        $regionInsert['name'] = $value->region;
                        $regionInsert['status'] = 1;
                        $regionCreate = Region::create($regionInsert);
                        $arr[$key]['region_id'] = $regionCreate->id;
                        $region = array_merge([caseChange($value->region)=>$regionCreate->id], $region);
                    }

                    if(array_key_exists(caseChange($value->state),$state) || $value->state == ''){
                        $arr[$key]['state_id'] = isset($state[caseChange($value->state)])?$state[caseChange($value->state)]:0;
                    }else{
                        $stateInsert['region_id'] = $arr[$key]['region_id'];
                        $stateInsert['name'] = $value->state;
                        $stateInsert['status'] = 1;
                        $stateCreate = State::create($stateInsert);
                        $arr[$key]['state_id'] = $stateCreate->id;
                        $state = array_merge([caseChange($value->state)=>$stateCreate->id], $state);
                    }

                    if(array_key_exists(caseChange($value->city),$city) || $value->city == ''){
                        $arr[$key]['city_id'] = isset($city[caseChange($value->city)])?$city[caseChange($value->city)]:0;
                    }else{
                        $cityInsert['state_id'] = $arr[$key]['state_id'];
                        $cityInsert['name'] = $value->city;
                        $cityInsert['status'] = 1;
                        $cityCreate = City::create($cityInsert);
                        $arr[$key]['city_id'] = $cityCreate->id;
                        $city = array_merge([caseChange($value->city)=>$cityCreate->id], $city);
                    }
                }

                if(count($valAmount) > 0 || count($valCtc) > 0 || count($valJoiningKit) > 0 || count($valSourceCategory) > 0 || count($valLocationCode) > 0 || count($valMonth) > 0){
                    return redirect('store/show-employee-upload')
                                ->with('valAmount', implode(', ', $valAmount))
                                ->with('valCtc', implode(', ', $valCtc))
                                ->with('valJoiningKit', implode(', ', $valJoiningKit))
                                ->with('valSourceCategory', implode(', ', $valSourceCategory))
                                ->with('valLocationCode', implode(', ', $valLocationCode))
                                ->with('valMonth', implode(', ', $valMonth))
                                ->with('flash_error', 'See below errors!');
                }

                if(!empty($arr)){
                    Employee::insert($arr);
                    return redirect('store/hiring')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('store/show-employee-upload')
                    ->with('flash_error', 'Request data does not have any files to import.');
    }

    public function downloadForm($id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','grade','designation','subDepartment','subDepartment.department','brand'])->find($id);

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                         \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        $view = view('storeManager.employee.download_form', compact('employee'));

        $pdf = PDF::loadHTML($view);

        return $pdf->download('employee_detail.pdf');

    }

    public function uploadDocuments($id,$upload=1)
    {
        view()->share('id',$id);
        view()->share('upload',$upload);

        $employee = Employee::with('designation')->find($id);

        $employee->attach = Attachment::where('employee_id',$id)->get();
        $attach = [];
        foreach ($employee->attach as $value) {
            if(in_array($value->key, ['education_documents','previous_company_docs'])){
                $attach[$value->key][$value->id]['file'] = $value->file;
                $attach[$value->key][$value->id]['key'] = $value->key;
            }else{
                $attach[$value->key]['file'] = $value->file;
                $attach[$value->key]['key'] = $value->key;
            }
        }
        $employee->attach = $attach;

        $businessUnits = BusinessUnit::where("status","1")->where("name","Unlimited")->first();
        view()->share('businessUnits',$businessUnits);

        $legalEntity = LegalEntity::where("status","1")->where("name","Arvind Lifestyle Brands Limited")->first();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->where("name","Store")->first();
        view()->share('employeeClassification',$employeeClassification);

        $variablePayType = VariablePayType::where("status","1")->where("name","Incentive")->first();
        view()->share('variablePayType',$variablePayType);

        return view('storeManager.employee.upload_documents', compact('employee'));
    }

    public function saveDocuments(Request $request,$id)
    {
        $rules = Helper::uploadDocumentValidationRule($request,$id);

        $uploadRule = Helper::updateDocumentValidationRule($request);

        if($request->upload == 0){
            $this->validate($request, $rules, Helper::validationMessage());
        }else{
            $this->validate($request, $uploadRule,Helper::validationMessage());
        }

        $requestData = $request->all();

        Helper::uploadEmployeeDocuments($requestData);

        return redirect('store/employee')->with('flash_success', 'Documents Uploaded Successfully!');
    }

    public function createOtherInfo(Request $request,$id=0){
        view()->share('id',$id);
        $otherInfo = OtherInfo::where('employee_id',$id)->first();
        /* $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->first()->toArray(); */

        return view('storeManager.employee.other_info_create',compact('otherInfo'));
    }

    public function storeOtherInfo(Request $request,$id){
		
        $this->validateOtherInfo($request);

        $employeeOtherInfo = OtherInfo::where('employee_id',$id)->first();
        $employee = Employee::find($id);
        if(!$employeeOtherInfo){
            $employeeOtherInfo = new OtherInfo();
        }
        $requestData = $request->all();
       /* $employeeOtherInfo->add_other_info = 1;
        $employeeOtherInfo->save(); */
        $employeeOtherInfo->dol = $requestData['dol'];
        $employeeOtherInfo->process_status = $requestData['process_status'];
        $employeeOtherInfo->appointment_status = $requestData['appointment_latter'];
        $employeeOtherInfo->id_card_status = $requestData['id_card_status'];
        $employeeOtherInfo->uniform_status = $requestData['uniform_status'];
        $employeeOtherInfo->l0l1_status = $requestData['L0L1_status'];
        $employeeOtherInfo->l0l1_result = (isset($requestData['l0l1_result']))?$requestData['l0l1_result']:'';
        $employeeOtherInfo->l0l1_certificate_status = (isset($requestData['L0L1_certificate']))?$requestData['L0L1_certificate']:'';
        $employeeOtherInfo->background_verification = $requestData['background_verification'];
        $employeeOtherInfo->bv_report_status = $requestData['bv_report'];
        $employeeOtherInfo->is_disabled = $requestData['is_disable'];
        $employeeOtherInfo->disability_desc = (isset($requestData['disability_description']))?$requestData['disability_description']:'';
        $employeeOtherInfo->allocated_leave = $requestData['allocated_leave'];
        $employeeOtherInfo->pending_leave = $requestData['pending_leave'];
        $employeeOtherInfo->availed_leave = $requestData['availed_leave'];
        $employeeOtherInfo->salary = $requestData['salary'];
        $employeeOtherInfo->employee_id = $id;
		$employeeOtherInfo->save();
		
        $employee->id_card = $requestData['id_card_status'];
        $employee->uniform = $requestData['uniform_status'];
        $employee->l0_l1_status = $requestData['L0L1_status'];
        $employee->l0_l1_certificate = (isset($requestData['L0L1_certificate']))?$requestData['L0L1_certificate']:'';

        $employee->salary_amount = $requestData['salary'];
        $employee->ctc = $requestData['salary'];
        if($requestData['process_status'] != ''){
            $employee->status = ($requestData['process_status'] == 'YES')?'confirm':'inactive';
        }

        $employee->save();



        // echo "<pre>"; print_r($requestData); exit();
       /* if(isset($requestData['family']) && !empty($requestData['family'])){
            foreach ($requestData['family'] as $value) {
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    \DB::table('other_infos')->where('id',$value['id'])->update(['name'=>$value['name']]);
                }
            }
        } */

        return redirect('store/employee')->with('flash_success','Other Information updated successfully');
    }

    public function LeaveReportemployee(Request $request)
    {
        view()->share('module', 'Reports');
        view()->share('route', 'leave-report');
        return view('storeManager.employee.leavereportemployee');
    }
    public function LeaveReportemployeedatatable(Request $request,$store_id)
    {
        $leaveReport = Employee::selectRaw('employee.employee_code,employee.full_name as employee_name,other_infos.allocated_leave, other_infos.pending_leave,other_infos.availed_leave')
                                        ->leftjoin('other_infos','other_infos.employee_id','employee.id')
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->where('employee.store_id',$store_id)
                                        ->get();
        return Datatables::of($leaveReport)
            ->make(true);
    }
}
