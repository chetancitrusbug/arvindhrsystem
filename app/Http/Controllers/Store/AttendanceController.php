<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\Attendance;
use App\AttendanceRequest;
use Carbon\Carbon as Carbon;
use App\Helper;
use App\OtherInfo;
use mPDF;
use PDF;
use DateTime;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.attendance');
        $this->middleware('permission:access.attendance.edit')->only(['edit','update']);
        $this->middleware('permission:access.attendance.create')->only(['create', 'store']);
        $this->middleware('permission:access.attendance.delete')->only('destroy');

        view()->share('route', 'attendance');
        view()->share('module', 'Attendance');
    }

    public function index(Request $request)
    {
        $attendance = Employee::select([
                    'employee.id','employee.hold_salary',
                    \DB::raw("CONCAT(full_name,' (',employee_code,')') as full_name"),
                    'employee.employee_code','employee.rm_absconding_status','employee.absconding_time','employee.joining_date','resignation.last_working_date','resignation.date_of_resignation','resignation.deleted_at'
                ])
                ->LeftJoin('resignation','employee.id','=','resignation.employee_id')
                ->where('employee.store_id',auth()->user()->store_id)
                ->where('employee.employee_code','!=','')
                ->where('employee.hrms_status',"2")
           //     ->where('rm_absconding_status','!=',"2")
           //     ->whereNull('resignation.deleted_at')
				//->where('employee.id',3825)
                ->groupby('employee.id');

        $att = $status = [];
        /*echo "<pre>"; print_r($request->get('month'));
        echo "<pre>"; print_r($request->get('year'));
        exit();*/
        if($request->get('month') && $request->get('year')){
            view()->share('month',$request->get('month'));
            view()->share('year',$request->get('year'));

            $presents = Attendance::where('month',$request->get('month'))->where('year',$request->get('year'))->get();

            if(count($presents)){
                foreach ($presents as $value) {
                    $att[$value->employee_id][$value->day] = $value->attendance;
                    $status[$value->employee_id][$value->day] = $value->status;
                }
            }
            view()->share('presents',$att);
            view()->share('labelChange',$status);

            $monthList = ['january','march','may','july','august','september','december'];

            if(in_array($request->get('month'), $monthList)){
                view()->share('days','31');
            }else{
                if($request->get('month') == 'february'){
                    $year = $request->get('year');
                    $day = ((0 == $year % 4) and (0 != $year % 100) or (0 == $year % 400))?29:28;
                    view()->share('days',$day);
                }else{
                    view()->share('days','30');
                }
            }
        }

        // $attendance = $attendance->paginate(10);
        $attendance = $attendance->get();

        return view('storeManager.attendance.index',compact('attendance'))->with('flash_success','Attendance added successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('storeManager.attendance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $pre = Attendance::where('month',$input['month'])->where('year',$input['year'])->where('attendance',"!=","")->get();
        $data = [];
        if(count($pre)){
            foreach ($pre as $value) {
                $data[$value->employee_id][$value->day] = $value->day;
            }
        }

        if(isset($input['att']) && count($input['att'])){
            foreach ($input['att'] as $key => $value) {
                foreach ($value as $day=>$att) {

                    if($att && !in_array($day, isset($data[$key])?$data[$key]:[])){
                        $attValue = [];
                        $attValue['attendance'] = $att;
                        $attValue['employee_id'] = $key;
                        $attValue['day'] = $day;
                        $attValue['month'] = $input['month'];
                        $attValue['year'] = $input['year'];
                        $attValue['full_date'] = $input['year']."-".date("m", strtotime($input['month']))."-".$day;

                        if($attValue['attendance'] != 'p' &&  $attValue['attendance'] != 'wo' ){
                            $other_infos = Otherinfo::where('employee_id',$attValue['employee_id'])->first();
                            if($other_infos){
                                $allocated_leave = $other_infos->allocated_leave;
                                $other_infos->pending_leave = $other_infos->pending_leave - 1;
                                $other_infos->availed_leave = $other_infos->availed_leave + 1;
                                $other_infos->save();
                            }
                        }
                        Attendance::create($attValue);
                    }
                }
            }
        }

        return redirect('store/attendance?month='.$input['month'].'&year='.$input['year'])->with('flash_success', 'successfully inserted');

        // $this->validateData($request);
        config()->set('ignoreEmpty', true);
        if($request->hasFile('excel')){

            $excel = $data = [];
            $path = $request->file('excel')->getRealPath();

            \Excel::load($path, function($reader) use (&$excel) {
                $reader->formatDates(true);
                $objExcel = $reader->getExcel();

                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $employee = Employee::where('employee_code','!=','')->pluck('id','employee_code')->toArray();

                //  Loop through each row of the worksheet in turn
                for ($row = 1; $row <= $highestRow; $row++)
                {
                    //  Read a row of data into an array
                    /*$sheet->setColumnFormat(array(
                            'B' => PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY,
                        ));*/
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                            NULL, TRUE, FALSE);

                    $data = $rowData[0];

                    if($row == 1){
                        $excel['location'] = $data[0];
                        $excel['date'] = date('Y-m-d' , ($data[6] - 25569) * 86400);
                        $excel['dateMonth'] = Carbon::parse($excel['date'])->format('m-Y');
                        $month = Carbon::parse($excel['date'])->format('m');
                        $year = Carbon::parse($excel['date'])->format('Y');
                        $count=0;
                        $monthList = ['1','3','5','7','8','10','12'];

                        if(in_array($month, $monthList)){
                            $excel['endrow'] = 36;
                        }else{
                            if($month == 2){
                                $excel['endrow'] = ((0 == $year % 4) and (0 != $year % 100) or (0 == $year % 400))?34:33;
                            }else{
                                $excel['endrow'] = 35;
                            }
                        }
                    }
                    if($row > 2 && array_key_exists((int)$data[1], $employee)){
                        // echo "<pre>"; print_r($data[1]); exit();
                        $excel[$row]['employee_id'] = $employee[trim($data[1])];
                        $excel[$row]['employee_code'] = trim($data[1]);
                        $excel[$row]['joining_date'] = date('Y-m-d' , ($data[3] - 25569) * 86400);
                        $excel[$row]['designation'] = caseChange($data[4]);
                        $excel[$row]['store'] = caseChange($data[5]);

                        for($day = 6; $day<=$excel['endrow']; $day++){
                            //data['date']
                            $date = Carbon::parse(++$count.'-'.$excel['dateMonth'])->format('Y-m-d');
                            $excel[$row]['attendance'][$date] = $data[$day];
                        }
                    }
                }
            });
            echo "<pre>"; print_r($excel); exit();
        }
        return redirect('store/attendance')->with('flash_success', 'successfully inserted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $attendance = Employee::find($id);

        // echo "<pre>"; print_r($attendance); exit();
        return view('storeManager.attendance.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // return view('storeManager.attendance.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // return redirect('store/hiring/createEducationDetail/'.$id)->with('flash_success', 'Personal information updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        Attachment::where('employee_id',$id)->delete();

        if($request->has('from_index')){
            $message = "Employee Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee deleted!');

            return redirect('store/hiring');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'excel' => 'required|mimes:xls,xlsx',
        ];

        return $this->validate($request, $rules);
    }

    public function changeRequest(Request $request)
    {
        $this->validate($request,[
                'note'=>'required',
            ],[
                'note.required'=>'Please add comment'
            ]);

        $input = request()->all();

        if(isset($input['id']) && !empty($input['id'])){
            $attendance = Attendance::where('employee_id',$input['id'])
                                ->where('day',$input['day'])
                                ->where('attendance',$input['attendance'])
                                ->where('month',$input['month'])
                                ->where('year',$input['year'])
                                ->first();
            if($attendance->status == 1){
                return 'Request already sent';
            }
            $attendance->status = 1;
            $attendance->save();

            if($attendance){
                $data['attendance_id'] = $attendance->id;
                $data['employee_id'] = $input['id'];
                $data['note'] = $input['note'];
                $data['attendance'] = $input['attendance'];

                $attendanceRequest = AttendanceRequest::create($data);

                if(isset($attendanceRequest->id)){
                    return 'Attendance request sent successfully';
                }
                return 'Attendance request sent error';
            }
            return 'error in send request';
        }
        return 'Attedance not found';
    }

    public function EmployeeAttandance(Request $request)
    {
        view()->share('module', 'Reports');
        view()->share('route', 'leave-report');
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        $store_id = \Auth::user()->store_id;
        if($request->has('filter')){
            $start_date  = $request->filter['from_date'];
            $end_date = $request->filter['to_date'];
            return redirect("store/attendance-report/employee?start_date=$start_date&end_date=$end_date");
        } else if(Input::get('start_date') && Input::get('end_date') ){
            $start_date  = Input::get('start_date');
            $end_date = Input::get('end_date');
        } else {
            $start_date  = \Carbon\Carbon::now()->startOfDay()->subDays(1);
            $end_date = \Carbon\Carbon::now()->startOfDay()->subDays(1);
        }


        $attandance_data = array();
        $filter['from_date']= $start_date;
        $filter['to_date']= $end_date;

        $title = "ATTENDANCE FOR THE MONTH OF ". strtoupper(date('d F Y', strtotime($start_date))). " to ".strtoupper(date('d F Y', strtotime($end_date)));
        $title_array = array($title);
        $total_date = array("Arvind Lifestyle Brands Ltd");
        $excel_hearder = array('SR NO','Employee Code','Employee Name','DOJ','DESIGNATION','Store-Office Name','IO CODE','STORE CODE-RETAK CODE');
        $store = \App\StoreLocation::find($store_id);
        $total_days = array($store->store_name);
        $start_date = new DateTime( $start_date );
        $end_date = new DateTime( $end_date );
        $end_date = $end_date->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start_date, $interval ,$end_date);
         foreach ($period as $key => $value ){
                array_push($excel_hearder,$value->format('l')." - ".$value->format('d/m/Y'));
                array_push($total_date,$value->format('j'));
                array_push($total_days,$value->format('l'));
                array_push($title_array,$value->format('d/m/Y'));
         }
         array_push($excel_hearder,"P","W/O","C/O","L","X","Days","Remarks");
        $sr_no = 1;
        $attandance_data[] = $total_date;
        $attandance_data[] = $total_days;
        $attandance_data[] = $title_array;

        $attandance_data[] = $excel_hearder;

        $attandance_report = array();
        $employees = Employee::whereNotIn('employee.status',['pending','cancel'])->selectRaw('employee.*,store_location.store_name,store_location.io_code,store_location.sap_code')
                        ->Leftjoin('store_location','store_location.id','employee.store_id')
                         ->where('employee.store_id',$store_id)
                        ->where('store_location.status',1)
                        ->with(['designation:id,name','attendance' => function ($q) use ($start_date,$end_date)  {
                            $q->whereBetween('full_date',[$start_date,$end_date]);
                        }])
                        ->get();
        //dd($employees[0]->attendance->pluck('attendance','full_date'));
        // $attandanceemployees  = \App\Attendance::join('employee','employee.id','attendance.employee_id')
        //                             ->Leftjoin('store_location','store_location.id','employee.store_id')
        //                             ->where('store_location.region_id',Auth::user()->region)
        //                             ->where('store_location.status',1)
        //                             ->whereNotIn('employee.status',['pending','cancel'])
        //                             ->whereBetween('full_date',[$start_date,$end_date])
        //                             ->get();

        foreach($employees as $employee){
            $attandance_report = array();
            $attandance_report[$employee->id]['SR NO'] = $sr_no;
            $attandance_report[$employee->id]['Employee Code'] = $employee->employee_code;
            $attandance_report[$employee->id]['Employee Name'] = $employee->full_name;
            $attandance_report[$employee->id]['DOJ'] = $employee->joining_date;
            $attandance_report[$employee->id]['DESIGNATION'] = (isset($employee->designation) && $employee->designation->name ) ? $employee->designation->name : '';
            $attandance_report[$employee->id]['Store-Office Name'] = $employee->store_name;
            $attandance_report[$employee->id]['IO CODE'] = $employee->io_code;
            $attandance_report[$employee->id]['STORE CODE-RETAK CODE'] = $employee->sap_code;
            $x = 00; $wo = 00; $co = 00; $l = 00; $p =00; $remark = ''; $days = 00;
            $employeedates = $employee->attendance->pluck('attendance','full_date');
            $employeeAttendance = array_count_values($employeedates->toArray());
            $p = (isset($employeeAttendance['p'])) ? $employeeAttendance['p'] : 0;;
            $l =   (isset($employeeAttendance['l'])) ? $employeeAttendance['l'] : 0;
            $co =   (isset($employeeAttendance['co']))? $employeeAttendance['co'] : 0 ;
            $wo =   (isset($employeeAttendance['wo']))? $employeeAttendance['wo'] : 0 ;
            $days = $p + $wo;
            //dd($employeedates);
            foreach ($period as $key => $value ){
                
                // $attandance = [];
                // if($employee->attendance){
                //     $attandance = $employee->attendance->where('full_date',$value->format('Y-m-d'))->where('employee_id',$employee->id)->first();
                // }
                // if($attandance){
                //     ($attandance->attendance == 'p')?$p++:'';
                //     ($attandance->attendance == 'l')?$l++:'';
                //     ($attandance->attendance == 'co')?$co++:'';
                //     ($attandance->attendance == 'wo')?$wo++:'';
                //     ($attandance->attendance == 'wo' || $attandance->attendance == 'p')?$days++:'';
                //     $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = strtoupper($attandance->attendance);
                // } else {
                    $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = (isset($employeedates[$value->format('Y-m-d')])) ? $employeedates[$value->format('Y-m-d')] : '-';
                //}

            }
            $attandance_report[$employee->id]["P"] = $p;
            $attandance_report[$employee->id]["W/O"] = $wo;
            $attandance_report[$employee->id]["C/O"] = $co;
            $attandance_report[$employee->id]["L"] = $l;
            $attandance_report[$employee->id]["X"] = $x;
            $attandance_report[$employee->id]["Days"] = $days;
            $attandance_report[$employee->id]["Remarks"] = $remark;
            $sr_no++;
            $attandance_data[] = $attandance_report[$employee->id];
        } 


        return view('storeManager.employeeattendancereport',compact('attandance_data','filter'));
    }

    public function attendencecsvExport(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        $store_id = \Auth::user()->store_id;
        $excel_export = array();
        $this->validate($request, [
            'export_start_date' => 'required',
            'export_end_date' => 'required',
        ]);
        $start_date = $request->export_start_date;
        $end_date = $request->export_end_date;
        $store = \App\StoreLocation::find($store_id);

        $title = "ATTENDANCE FOR THE MONTH OF ". strtoupper(date('d F Y', strtotime($start_date))). " to ".strtoupper(date('d F Y', strtotime($end_date)));
        $title_array = array($title,'','','','','','','');
        $total_date = array("Arvind Lifestyle Brands Ltd",'','','','','','','');
        $total_days = array($store->store_name,'','','','','','','');
        $excel_hearder = array('SR NO','Employee Code','Employee Name','DOJ','DESIGNATION','Store-Office Name','IO CODE','STORE CODE-RETAK CODE');
        $start_date = new DateTime( $start_date );;
        $end_date = new DateTime( $end_date );;
        $end_date = $end_date->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start_date, $interval ,$end_date);
         foreach ($period as $key => $value ){
                array_push($excel_hearder,$value->format('l')." - ".$value->format('d/m/Y'));
                array_push($total_date,$value->format('j'));
                array_push($total_days,$value->format('l'));
                array_push($title_array,$value->format('d/m/Y'));
         }
         array_push($excel_hearder,"P","W/O","C/O","L","X","Days","Remarks");
        $sr_no = 1;
        $attandance_data[] = $total_date;
        $attandance_data[] = $total_days;
        $attandance_data[] = $title_array;
        $attandance_data[] = $excel_hearder;
        $attandance_report = array();
        $employees = Employee::whereNotIn('employee.status',['pending','cancel'])->selectRaw('employee.*,store_location.store_name,store_location.io_code,store_location.sap_code')
                        ->Leftjoin('store_location','store_location.id','employee.store_id')
                         ->where('employee.store_id',$store_id)
                        ->where('store_location.status',1)
                        ->with(['designation:id,name','attendance' => function ($q) use ($start_date,$end_date)  {
                            $q->whereBetween('full_date',[$start_date,$end_date]);
                        }])
                        ->get();
        //dd($employees[0]->attendance->pluck('attendance','full_date'));
        // $attandanceemployees  = \App\Attendance::join('employee','employee.id','attendance.employee_id')
        //                             ->Leftjoin('store_location','store_location.id','employee.store_id')
        //                             ->where('store_location.region_id',Auth::user()->region)
        //                             ->where('store_location.status',1)
        //                             ->whereNotIn('employee.status',['pending','cancel'])
        //                             ->whereBetween('full_date',[$start_date,$end_date])
        //                             ->get();

        foreach($employees as $employee){
            $attandance_report = array();
            $attandance_report[$employee->id]['SR NO'] = $sr_no;
            $attandance_report[$employee->id]['Employee Code'] = $employee->employee_code;
            $attandance_report[$employee->id]['Employee Name'] = $employee->full_name;
            $attandance_report[$employee->id]['DOJ'] = $employee->joining_date;
            $attandance_report[$employee->id]['DESIGNATION'] = (isset($employee->designation) && $employee->designation->name ) ? $employee->designation->name : '';
            $attandance_report[$employee->id]['Store-Office Name'] = $employee->store_name;
            $attandance_report[$employee->id]['IO CODE'] = $employee->io_code;
            $attandance_report[$employee->id]['STORE CODE-RETAK CODE'] = $employee->sap_code;
            $x = 00; $wo = 00; $co = 00; $l = 00; $p =00; $remark = ''; $days = 00;
            $employeedates = $employee->attendance->pluck('attendance','full_date');
            $employeeAttendance = array_count_values($employeedates->toArray());
            $p = (isset($employeeAttendance['p'])) ? $employeeAttendance['p'] : 0;;
            $l =   (isset($employeeAttendance['l'])) ? $employeeAttendance['l'] : 0;
            $co =   (isset($employeeAttendance['co']))? $employeeAttendance['co'] : 0 ;
            $wo =   (isset($employeeAttendance['wo']))? $employeeAttendance['wo'] : 0 ;
            $days = $p + $wo;
            //dd($employeedates);
            foreach ($period as $key => $value ){
                
                // $attandance = [];
                // if($employee->attendance){
                //     $attandance = $employee->attendance->where('full_date',$value->format('Y-m-d'))->where('employee_id',$employee->id)->first();
                // }
                // if($attandance){
                //     ($attandance->attendance == 'p')?$p++:'';
                //     ($attandance->attendance == 'l')?$l++:'';
                //     ($attandance->attendance == 'co')?$co++:'';
                //     ($attandance->attendance == 'wo')?$wo++:'';
                //     ($attandance->attendance == 'wo' || $attandance->attendance == 'p')?$days++:'';
                //     $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = strtoupper($attandance->attendance);
                // } else {
                    $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = (isset($employeedates[$value->format('Y-m-d')])) ? $employeedates[$value->format('Y-m-d')] : '-';
                //}

            }
            $attandance_report[$employee->id]["P"] = $p;
            $attandance_report[$employee->id]["W/O"] = $wo;
            $attandance_report[$employee->id]["C/O"] = $co;
            $attandance_report[$employee->id]["L"] = $l;
            $attandance_report[$employee->id]["X"] = $x;
            $attandance_report[$employee->id]["Days"] = $days;
            $attandance_report[$employee->id]["Remarks"] = $remark;
            $sr_no++;
            $attandance_data[] = $attandance_report[$employee->id];
        } 
        $excel_export = $attandance_data;
        \Excel::create($title,function($excel) use ($excel_export){
                $excel->setTitle('Attandace Reports');
                $excel->sheet('Attandace Reports',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                    $sheet->mergeCells('A1:H1');
                    $sheet->mergeCells('A2:H2');
                    $sheet->mergeCells('A3:H3');
                    $sheet->setAllBorders('thin');
                    $sheet->getStyle('A1')->applyFromArray(array(
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '91d3ff')
                        )
                    ));

                    $sheet->getStyle('A2')->applyFromArray(array(
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'e2cce2')
                        )
                    ));


                    $sheet->getStyle('A3')->applyFromArray(array(
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'dc80d8')
                        )
                    ));

                    $sheet->getStyle('A4:H4')->applyFromArray(array(
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '91d3ff')
                        )
                    ));
                });
            })->download('xlsx');
           return back();
    }
}
