<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\StoreLocation;
use App\Employee;
use App\OtherInfo;
use App\HiringReport;
use App\StoreCollection;
use DateTime;
class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {

        $storeId = Auth::user()->store_id;
		$store_location = StoreLocation::where('store_location.status',1)->leftJoin('region','region.id','store_location.region_id')->where('store_location.id',$storeId)->get();
		$store_location = $store_location[0];

		$manPowerGet = StoreLocation::where('store_location.status',1)->where('store_location.id',$storeId)->select(DB::raw('budget_man_power'),DB::raw('actual_man_power'),'region.name','region_id','store_name','io_code')->leftJoin('region','region.id','store_location.region_id')->get();
		if(count($manPowerGet) > 0)
			$manPowerGet = $manPowerGet[0];
		$averageCost = Employee::where('employee.processstatus','!=','Inactive')->where('store_location.status',1)->leftJoin('store_location','store_location.id','employee.store_id')->leftJoin('region','region.id','store_location.region_id')->select(DB::raw('sum(ctc) as salary'),DB::raw('count(employee.id) as total_employee'),DB::raw('( CASE WHEN SUM(ctc) / COUNT(employee.id) > 0 THEN SUM(ctc)/COUNT(employee.id) ELSE 0 END) AS avg_cost'),'region.name','region.id as region_id')->where('store_location.id',$storeId)->groupBy('store_location.id')->get()->toArray();

		if(count($averageCost) > 0)
			$averageCost = $averageCost[0];

		$hygiene['id_card'] = OtherInfo::select(DB::raw('count(*) as id_count'))->join('employee','employee.id','other_infos.employee_id')->where('other_infos.id_card_status','NO')->where('employee.store_id',$storeId)->where('employee.processstatus','!=','Inactive')->get();
		$hygiene['id_card'] = $hygiene['id_card'][0]->id_count;

		$hygiene['uniform'] = OtherInfo::select(DB::raw('count(*) as uniform_count'))->join('employee','employee.id','other_infos.employee_id')->where('other_infos.uniform_status','NO')->where('employee.store_id',$storeId)->where('employee.processstatus','!=','Inactive')->get();
		$hygiene['uniform'] = $hygiene['uniform'][0]->uniform_count;

		$hygiene['l0l1_untrained'] = OtherInfo::select(DB::raw('count(*) as l0l1_untrained_count'))->join('employee','employee.id','other_infos.employee_id')->where('employee.store_id',$storeId)->where('other_infos.l0l1_status','NO')->where('employee.processstatus','!=','Inactive')->get();
		$hygiene['l0l1_untrained'] = $hygiene['l0l1_untrained'][0]->l0l1_untrained_count;


		$hygiene['l0l1_certified'] = OtherInfo::select(DB::raw('count(*) as l0l1_certificate_count'))->join('employee','employee.id','other_infos.employee_id')->where('employee.store_id',$storeId)->where('employee.processstatus','!=','Inactive')->where('other_infos.l0l1_certificate_status','NO')->get();
		$hygiene['l0l1_certified'] = $hygiene['l0l1_certified'][0]->l0l1_certificate_count;

		$hygiene['certificate_pending'] = OtherInfo::select(DB::raw('count(*) as l0l1_untrained_count'))->join('employee','employee.id','other_infos.employee_id')->where('employee.store_id',$storeId)->where('employee.processstatus','!=','Inactive')->where('other_infos.l0l1_certificate_status','NO')->get();
		$hygiene['certificate_pending'] = $hygiene['certificate_pending'][0]->l0l1_untrained_count;

		$hygiene['appoint_letter'] = OtherInfo::select(DB::raw('count(*) as appointment_status_count'))->join('employee','employee.id','other_infos.employee_id')->where('employee.store_id',$storeId)->where('employee.processstatus','!=','Inactive')->where('other_infos.appointment_status','NO')->get();

        $hygiene['appoint_letter'] = $hygiene['appoint_letter'][0]->appointment_status_count;

        $current_year = date('Y');
        $current_month = date('m');
        if($current_month < 4){
            $current_year--;
        }
        $inmonths = array();
        for($i = 01; $i<=12;$i++){
            $next_year = $current_year+1;
            ($i < 4)?$inmonths[]= sprintf("%02d", $i)."-$next_year"  : $inmonths[]=  sprintf("%02d", $i)."-$current_year";
        }
        $hiringReport = array();
        $hiringReport = HiringReport::leftJoin('store_location','store_location.id','hiring_reports.store_id')
                        ->leftJoin('region','region.id','store_location.region_id')
                        ->whereIn('hiring_reports.monthyear',$inmonths)
                        ->where('hiring_reports.store_id',$storeId)
                        ->orderBy('hiring_reports.id')
                        ->groupBy('hiring_reports.id')
                        ->get();
        $store_name = '';

        if($hiringReport && isset($hiringReport[0])){
            $store_name = $hiringReport[0]->store_name;
        }

        $totalattritionReport = HiringReport::selectRaw('hiring_reports.*,(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,store_location.store_name,region.name')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('monthyear',[date('m-Y')])
                                ->where('hiring_reports.store_id',$storeId)
                                ->first();
        $agingReport = $this->agingregion($storeId);
        $manpowerPercentageReport = HiringReport::selectRaw('(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,SUM(store_location.actual_man_power) as total_actual_man_power')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->where('hiring_reports.store_id',$storeId)
                                ->whereIn('monthyear',[date('m-Y')])
                                ->first();
        $hiringinprocessreport = $this->hiringinprocessreport($storeId);
        $total_pending_employee = Employee::selectRaw('count(employee.id) as total_pending')
                                                ->where('employee.store_id',$storeId)
                                                ->whereNotIn('employee.status',['confirm','reject','inactive'])
                                                ->first();
        $genderwisetotalemployee = $this->genderwisetotalemployeestore($storeId);
        $hiring_employee = $this->genderwisehiringstore($storeId);
        $genderwiseattrition = $this->genderwiseattritionstore($storeId);
        $productivity_report   = $this->ColletionReportstore($storeId);
        $leaveReport = $this->leaveReport($storeId);
        $getAttendanceReportData = $this->getAttendanceReportData($storeId);
      //  return view('storeManager.dashboard',compact('manPowerGet','averageCost','hygiene','hiringReport','totalattritionReport'));
        return view('storeManager.dashboard',compact('manPowerGet','averageCost','hygiene','hiringReport','store_location','totalattritionReport','agingReport','manpowerPercentageReport','hiringinprocessreport','total_pending_employee','genderwisetotalemployee','hiring_employee','genderwiseattrition','productivity_report','leaveReport','getAttendanceReportData'));
    }
    public function agingregion($storeId)
    {
        $infant_attrition = 0;
        $baby_attrition = 0;
        $attrition = 0;
        $leaved_employee = Employee::selectRaw('resignation.last_working_date,employee.id,employee.joining_date,employee.date_of_leave')
                    ->Join('resignation','employee.id','resignation.employee_id')
                    ->leftJoin('store_location','store_location.id','employee.store_id')
                    ->leftJoin('region','region.id','store_location.region_id')
                    ->where('store_location.status',1)
                    ->where('region.status',1)
                    ->where('employee.store_id',$storeId)
                    ->whereNull('resignation.deleted_at')
                    ->where(function ($query) {
                            $query->where('resignation.last_working_date', '>=', date('Y-m-d',strtotime('1-4-'.date('Y'))))
                                ->orWhere('employee.date_of_leave', '>=', date('Y-m-d',strtotime('1-4-'.date('Y'))));
                    })->get();
        $baby_attrition = 0;
        foreach ($leaved_employee as $employee) {

            $fdate = str_replace('/', '-',$employee->joining_date);
            $tdate = (!empty($employee->date_of_leave))?$employee->date_of_leave:$employee->last_working_date;

            $datetime1 = new DateTime($fdate);
            $datetime2 = new DateTime($tdate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');

            if($days <= 30){
                $infant_attrition= $infant_attrition+1;
            } else if($days >30 && $days <=90 ){
                $baby_attrition = $baby_attrition+1;
            } else {
                $attrition = $attrition+1;
            }
        }

        $aging_report['baby_attrition'] = $baby_attrition;
        $aging_report['infant_attrition'] = $infant_attrition;
        $aging_report['attrition'] = $attrition;
        $aging_report['total'] =count($leaved_employee);
        return $aging_report;
    }
     public function hiringinprocessreport($store_id)
    {
        $store_side_pending = Employee::selectRaw('count(employee.id) as total_pending')
                                    ->where('employee.store_id',$store_id)
                                    ->whereNotIn('employee.hrms_status',[2])
                                    ->whereIn('employee.store_status',[3,0])
                                    ->whereNotIn('employee.regional_status',[2])
                                    ->whereIN('employee.status',['pending','approve'])
                                    ->first();
        $region_side_pending = Employee::selectRaw('count(employee.id) as total_pending')
                                    ->where('employee.store_id',$store_id)
                                    ->whereNotIn('employee.hrms_status',[2])
                                    ->whereIn('employee.store_status',[1])
                                    ->whereNotIn('employee.store_status',[3])
                                    ->whereNotIn('employee.regional_status',[2])
                                    ->whereIN('employee.status',['pending','approve'])
                                    ->first();
        $hrms_side_pending = Employee::selectRaw('count(employee.id) as total_pending')
                                    ->where('employee.store_id',$store_id)
                                    ->where('employee.hrms_status',1)
                                    ->whereIn('employee.regional_status',[2])
                                    ->whereIN('employee.status',['pending','approve'])
                                    ->whereNotIn('employee.hrms_status',[2,3])
                                    ->first();
        $data['store_pending'] = $store_side_pending->total_pending;
        $data['region_pending'] = $region_side_pending->total_pending;
        $data['hrms_pending'] = $hrms_side_pending->total_pending;
        return $data;
    }

    public function genderwiseattritionstore($store_id)
    {

        $report_data = array();
            $gendermale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('resignation','resignation.employee_id','employee.id')
                                    ->where('employee.status','inactive')
                                    ->where('employee.gender','male')
                                    ->where(function($query){
                                        $query->whereDate('employee.date_of_leave', '>=', date('Y-m-1'));
                                        $query->orwhereDate('resignation.last_working_date', '>=',date('Y-m-1'));
                                    })
                                    ->where('employee.store_id',$store_id)
                                    ->first();
            $genderfemale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('resignation','resignation.employee_id','employee.id')
                                    ->where('employee.status','inactive')
                                    ->where('employee.gender','female')
                                    ->where(function($query){
                                        $query->whereDate('employee.date_of_leave', '>=', date('Y-m-1'));
                                        $query->orwhereDate('resignation.last_working_date', '>=',date('Y-m-1'));
                                    })
                                    ->where('employee.store_id',$store_id)
                                    ->first();

            $report_data['male'] = $gendermale->total_employee;
            $report_data['female'] = $genderfemale->total_employee;
        return $report_data;
    }

     public function genderwisehiringstore($store_id)
    {
        $report_data = array();
            $gendermale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->where('employee.gender','male')
                                    ->whereDate('employee.joining_date', '>=', date('Y-m-1'))
                                    ->where('employee.store_id',$store_id)
                                    ->first();
            $genderfemale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->where('employee.gender','female')
                                    ->whereDate('employee.joining_date', '>=', date('Y-m-1'))
                                    ->where('employee.store_id',$store_id)
                                    ->first();
            $total_employee = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->whereDate('employee.joining_date', '>=', date('Y-m-1'))
                                    ->where('employee.store_id',$store_id)
                                    ->first();
            $report_data['total'] = $total_employee->total_employee;
            $report_data['male'] = $gendermale->total_employee;
            $report_data['female'] = $genderfemale->total_employee;


        return $report_data;
    }


    public function genderwisetotalemployeestore($store_id)
    {

        $report_data = array();
            $gendermale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->where('employee.gender','male')
                                    ->where('employee.store_id',$store_id)
                                    ->first();
            $genderfemale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->where('employee.gender','female')
                                    ->where('employee.store_id',$store_id)
                                    ->first();
            $total_employee = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->where('employee.store_id',$store_id)
                                    ->first();

            $report_data['total'] = $total_employee->total_employee;
            $report_data['male'] = $gendermale->total_employee;
            $report_data['female'] = $genderfemale->total_employee;
        return $report_data;
    }

    public function ColletionReportstore($store_id)
    {
        $collection_report = array();
        $collection_reports = StoreCollection::selectRaw('store_collections.*,SUM(store_collections.amount) as total_amount,store_location.store_name, store_location.id as store_id')
                                            ->join('store_location','store_location.id','store_collections.store_id')
                                            ->join('region','region.id','store_location.region_id')
                                            ->where('store_collections.month',date('m'))
                                            ->where('store_collections.year',date('Y'))
                                            ->where('store_collections.store_id',$store_id)
                                            ->first();
            $total_employee = Employee::selectRaw('count(employee.id) as total_employee,SUM(employee.ctc) as total_ctc')
                                        ->join('store_location','store_location.id','employee.store_id')
                                        ->join('region','region.id','store_location.region_id')
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->where('employee.store_id',$store_id)
                                        ->first();
            $collection_report = array(
                'store_id'=>$collection_reports->store_id,
                'store_name'=>$collection_reports->store_name,
                'total_amount'=>$collection_reports->total_amount,
                'total_employee'=>$total_employee->total_employee,
                'avg_ctc' =>($total_employee->total_ctc > 0  && $total_employee->total_employee >0)? round($total_employee->total_ctc/$total_employee->total_employee,2):'-',
                'per_person_income' => ($collection_reports->total_amount !=0  && $total_employee->total_employee >0)? round($collection_reports->total_amount/$total_employee->total_employee,2):'-'
            );

        return $collection_report;
    }
    public function leaveReport($storeId)
    {
        $leaveReport = Employee::selectRaw('SUM(other_infos.allocated_leave) as total_allocated_leave, SUM(other_infos.pending_leave) as total_pending_leave, SUM(other_infos.availed_leave) as availed_leave')
                                        ->leftjoin('other_infos','other_infos.employee_id','employee.id')
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->where('employee.store_id',$storeId)
                                        ->first();
        return $leaveReport;
    }
    public function getAttendanceReportData($storeId){
		$selected_date = \Carbon\Carbon::now()->startOfDay()->subDays(1)->format("Y-m-d");
		$present_per = 100;
		$actual_man_power = 0;

        $manPowerGet = \App\StoreLocation::select(DB::raw('store_location.actual_man_power as total_actual_man_power'))->find($storeId);
                      /*  ->where('store_location.status',1)
                        ->where('store_location.id',$storeId)
                        ->where('employee.status','confirm')
                        ->select(DB::raw('SUM(store_location.actual_man_power) as total_actual_man_power'))
                        ->first(); */
        $present_man_power = \App\Employee::LeftJoin('attendance','attendance.employee_id','employee.id')
                            ->where('employee.store_id',$storeId)
                            ->where('employee.processstatus','!=','Inactive')
                            ->where('attendance.full_date', '=',$selected_date)
                            ->where('attendance.attendance','p')->count();

		if($manPowerGet){
			$actual_man_power = round($manPowerGet->total_actual_man_power);
		}
		if($actual_man_power > 0){
			$present_per =	round(($present_man_power * 100) / $actual_man_power, 2);
		}

		return ['present_per'=>$present_per,'actual_man_power'=>$actual_man_power,'present_man_power'=>$present_man_power];
	}
}
