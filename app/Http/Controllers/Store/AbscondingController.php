<?php

namespace App\Http\Controllers\Store;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\User;
use App\Role;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use Carbon\Carbon as Carbon;
use App\Helper;
use mPDF;
use PDF;

class AbscondingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        view()->share('route', 'separation');
        view()->share('module', 'Separation');
    }

    public function index(Request $request)
    {
		$storeId = Auth()->user()->store_id;
        $employee = Employee::where('absconding_status',"0")->where('store_id',$storeId)->where('employee.rm_absconding_status',"0")->where('employee.hrms_status',"2")->pluck('full_name','id')->toArray();
        view()->share('employee',$employee);

        return view('storeManager.separation.absconding');
    }

    public function datatable(Request $request)
    {
        $employee = Employee::select([
                    'employee.*',
                ])
                // ->where('user_id',auth()->id())
                ->where('employee.absconding_status',"1")
                //->where('employee.rm_absconding_status',"0")
                ->where('employee.store_id',Auth()->user()->store_id)
                ->orderBy('employee.absconding_time','DESC')
                ->get();

        return Datatables::of($employee)
            ->make(true);
    }

    public function getEmployeeDetail(Request $request)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($request->id);

        if(!$employee){
            return ['status'=>'400','msg'=>'Employee detail not found','data'=>[]];
        }

        return ['status'=>'200','msg'=>'success','data'=>$employee];
    }

    public function addToAbsconding(Request $request)
    {
        $this->validate($request, [
             'absconding_time' => 'required',
             'employee_id' => 'required'
        ]);
        $employee = Employee::find($request->employee_id);

        if(!$employee){
            return ['status'=>'400','msg'=>'Employee detail not found','data'=>[]];
        }
        $store = StoreLocation::find($employee->store_id);
        if($store){
            $store->actual_man_power = $store->actual_man_power - 1;
            $store->save();
        }
        $employee->absconding_status = 1;
        $employee->processstatus = 'Active-Absconding';
        $employee->hold_salary = 'YES';
     //   $employee->rm_absconding_status = '1';
        $employee->absconding_time =  date('Y-m-d',strtotime($request->absconding_time));// \Carbon\Carbon::now();
        $employee->save();

        return ['status'=>'200','msg'=>'Added to Absconding','data'=>$employee];
    }

    public function removeFromAbsconding(Request $request,$id)
    {
        $employee = Employee::find($id);

        if(!$employee){
            return ['status'=>'400','msg'=>'Employee detail not found','data'=>[]];
        }
        $store = StoreLocation::find($employee->store_id);
        if($store){
            $store->actual_man_power = $store->actual_man_power + 1;
            $store->save();
        }
        $employee->absconding_status = 0;
        $employee->processstatus = 'Active';
        $employee->hold_salary = 'NO';
        $employee->absconding_time = null;
        $employee->save();

        return ['status'=>'200','msg'=>'Added to Absconding','data'=>$employee];
    }

    public function abscondingNotificationToRM(Request $request)
    {
        $employee = Employee::where('absconding_time', '<=', \Carbon\Carbon::now()->subDays(10))->where('rm_absconding_status', '')->get();
        if(count($employee)){
            foreach ($employee as $value) {
                Employee::where('id',$value->id)->update(['rm_absconding_status'=>'1']);
            }
        }

        return 'Absconding Notification sent to RM';
    }
}
