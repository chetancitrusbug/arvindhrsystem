<?php

namespace App\Http\Controllers\Hrms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\Resignation;
use App\User;
use App\Log;
use Carbon\Carbon as Carbon;
use App\Helper;
use App\StoreLocation;
use App\HiringReport;

use Auth;

class ResignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
       // view()->share('route', 'separation');
      //  view()->share('module', 'Separation');
    }

    public function index(Request $request)
    {
		return view('hrms.resignation.index');
	}

    public function datatable(Request $request)
    {
        $employee = Resignation::select(['employee.*','resignation.id','last_working_date','resignation.resignation_status',
				'resignation.created_at'])
				->join('employee','employee.id','=','resignation.employee_id')
				->orderBy('resignation.updated_at','DESC');

		$employee->where('resignation.resignation_status',"processing-hr");

		$employee = $employee->get();
        return Datatables::of($employee)
            ->make(true);
    }



	public function update($id, Request $request)
    {

		$result = array();

        $this->validate($request, [
            'resignation_status' => 'required',
            'rid' => 'required',
        ]);


		$item = Resignation::where("id",$request->rid)->first();
        $requestData = $request->except(['rid']);
		$requestData['approve_by'] = Auth::user()->id;



		if($item && $item->employee){

			if($item){
				$item->update($requestData);
				$result['message'] = "Request processed.";
                $result['code'] = 200;
                $this->RemoveEmployeeInHiringReport($item->employee_id);
			}else{
				$result['message'] = "Something went wrong please try again";
				$result['code'] = 400;
			}


			if($request->ajax()){
				return response()->json($result, $result['code']);
			}else{
				if($result['code'] == 400){
					Session::flash('flash_error',$result['message']);
				}else{
					Session::flash('flash_success',$result['message']);
				}

				return redirect()->back();
			}
        }else{
			Session::flash('flash_error',"Access denied..");
		}
		return redirect()->back();
    }
    public function RemoveEmployeeInHiringReport($employee_id)
    {
        $employee = Employee::find($employee_id);
        if($employee){
            $employee->status = 'inactive';
            $employee->processstatus = 'Inactive';
            $employee->save();
            // $store = StoreLocation::find($employee->store_id);
            // if($store){
            //     $store->actual_man_power = $store->actual_man_power - 1;
            //     $store->save();
            // }
            $hiringReport = HiringReport::where('store_id',$employee->store_id)
                                        ->where('monthyear',date('m-Y'))
                                        ->first();
            if($hiringReport){
                $hiringReport->resignation= $hiringReport->resignation + 1;
           //     $hiringReport->gap= $hiringReport->gap + 1;
                $hiringReport->pending= $hiringReport->pending + 1;
                $hiringReport->save();
            }
        }

    }




}
