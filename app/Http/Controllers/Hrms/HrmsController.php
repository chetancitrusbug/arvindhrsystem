<?php

namespace App\Http\Controllers\Hrms;

use App\Http\Controllers\Controller;

class HrmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        return view('hrms.dashboard');
    }
}
