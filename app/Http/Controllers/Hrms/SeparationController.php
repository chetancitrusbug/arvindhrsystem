<?php

namespace App\Http\Controllers\Hrms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use App\Helper;
use Carbon\Carbon as Carbon;

class SeparationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        view()->share('route', 'separation');
        view()->share('module', 'Separation');
    }

    public function index(Request $request)
    {
        return view('hrms.separation.index');
    }

    public function datatable(Request $request)
    {
        /*$employee = Employee::select([
                    'employee.*',
                ])
                ->whereIn('regional_status',[2])
                ->where('employee.hrms_status',"2")
                ->where('employee.joining_date','!=',"")
                ->orderBy('employee.id','DESC')
                ->latest()->get();

        return Datatables::of($employee)
            ->make(true);*/
    }

    public function logView(Request $request,$id)
    {
        view()->share('id',$id);
        return view('hrms.separation.log');
    }

    public function log(Request $request,$id)
    {
        $logs = Log::select([
                    'logs.*',
                    'users.emp_name as user_name',
                    'roles.label as user_role',
                ])
                ->leftJoin('users',function($join){
                    $join->on('users.id','=','logs.user_id');
                })
                ->leftJoin('roles',function($join){
                    $join->on('roles.id','=','logs.user_role_id');
                })
                // ->where('user_id',auth()->id())
                ->where('logs.emp_id',$id)
                ->orderBy('logs.id','DESC')
                ->latest()->get();

        return Datatables::of($logs)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return redirect('hrms/employee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //not allowed
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','grade','designation','subDepartment','subDepartment.department','brand'])->find($id);

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        return view('hrms.separation.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        $request = new Request;
        $currentCity = $permanentCity = [];
        view()->share('is_employee','1');

        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        $city = City::where("status","1")->pluck('name','id')->toArray();
        view()->share('city',$city);

        if(old('current_state',$employee->current_state)){
            $currentCity = $this->getCityFromState($request,['state_id'=>old('current_state',$employee->current_state)]);
        }
        view()->share('currentCity',$currentCity);

        if(old('permanent_state',$employee->permanent_state)){
            $permanentCity = $this->getCityFromState($request,['state_id'=>old('permanent_state',$employee->permanent_state)]);
        }
        view()->share('permanentCity',$permanentCity);

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        $locationCode = StoreLocation::where("status","1")->pluck('location_code','id')->toArray();
        view()->share('locationCode',$locationCode);

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);

        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $subDepartments = [];
        if(old('department_id',$employee->department_id)){
            $subDepartments = $this->getSubDepartments($request,['department_id'=>old('department_id',$employee->department_id)]);
        }
        view()->share('subDepartments',$subDepartments);

        $businessUnits = BusinessUnit::where("status","1")->pluck('name','id')->toArray();
        view()->share('businessUnits',$businessUnits);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->pluck('name','id')->toArray();
        view()->share('employeeClassification',$employeeClassification);

        $variablePayType = VariablePayType::where("status","1")->pluck('name','id')->toArray();
        view()->share('variablePayType',$variablePayType);

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        return view('hrms.separation.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->only(['blood_group','rh_factor','marital_status','nationality','mobile','email','esic_number','emergency_person','emergency_number','emergency_relationship','anniversary_date','ua_number','education','family','joining_date','business_unit_id','legal_entity_id','employee_classification_id','variable_pay_type_id','grade_id','designation_id','department_id','sub_department_id','reporting_manager_name','reporting_manager_employee_code']);

        $requestData['anniversary_date'] = Helper::ymd(str_replace('/', '-', $requestData['anniversary_date']));
        $requestData['joining_date'] = Helper::ymd(str_replace('/', '-', $requestData['joining_date']));

        $employee = Employee::where('id',$id)->update(array_except($requestData,['attach','_method','_token','button','education','previous','family']));

        if(isset($requestData['education']) && !empty($requestData['education'])){
            foreach ($requestData['education'] as $value) {
                if($value['board_uni'] != '' &&
                        $value['institute'] != '' &&
                        $value['qualification'] != '' &&
                        $value['specialization'] != '' &&
                        $value['month'] != '' &&
                        $value['year'] != ''
                    ){
                    $value['employee_id'] = $id;
                    if(isset($value['id'])){
                        $value['deleted_at'] = null;
                        \DB::table('education_detail')->where('id',$value['id'])->update(array_except($value,['education_category_id']));
                    }
                    /*else{
                        EducationDetail::insert($value);
                    }*/
                }
            }
        }

        if(isset($requestData['family']) && !empty($requestData['family'])){
            foreach ($requestData['family'] as $value) {
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    $value['deleted_at'] = null;
                    \DB::table('family_information')->where('id',$value['id'])->update(['name'=>$value['name']]);
                }
                /*else{
                    FamilyInfo::insert($value);
                }*/
            }
        }

        $employee = Employee::find($id);
        // create log
        $log['title'] = "Employee Update";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['note'] = $request->hrms_note;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        return redirect('hrms/employee')->with('flash_success', 'Employee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $employee = Employee::find($id);

        //delete certificates
        $oldDocs = Attachment::where('employee_id',$id)->get();

        if(count($oldDocs)){
            foreach ($oldDocs as $key => $value) {
                if(file_exists($value->file)){
                    unlink($value->file); //delete previously uploaded Attachment
                }
            }
        }

        Attachment::where('employee_id',$id)->delete();

        $employee->delete();

        if($request->has('from_index')){
            $message = "Employee Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee deleted!');

            return redirect('hrms/employee');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'blood_group' => 'required',
            'rh_factor' => 'required',
            'anniversary_date' => 'required|date_format:d/m/Y',
            'marital_status' => 'required',
            'nationality' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'mobile' => ['required','numeric',function($attribute, $value, $fail) use($request,$id) {
                            if(strlen($value) != 10){
                                $fail('The mobile no must be of 10 digits');
                            }
                        }],
            'email' => 'required|email',
            'emergency_person' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'emergency_number' => 'required|numeric',
            'emergency_relationship' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'joining_date'=>['date_format:d/m/Y','nullable',
                            function($attribute, $value, $fail) use($request,$id) {
                                if($value != ''){
                                    if(Helper::ymd(str_replace('/', '-', $value)) <= Helper::ymd(Carbon::now())){
                                        return $fail('The joining date must be a date after '.Helper::dmy(Carbon::now()));
                                    }
                                }
                            },
                        ],
            'education'=>[
                        function($attribute, $value, $fail) use($request,$id) {
                            if(count($value) == 1){
                                foreach ($value as $value) {
                                    if($value['education_category_id'] == '' &&
                                        $value['board_uni'] == '' &&
                                        $value['institute'] == '' &&
                                        $value['qualification'] == '' &&
                                        $value['specialization'] == '' &&
                                        $value['month'] == '' &&
                                        $value['year'] == ''
                                            ){
                                        $fail('atleast one education detail is required');
                                    }
                                }
                            }
                        },
                    ],
        ];

        $message = [];
        foreach($request->education as $key=>$value){
            $rules["education.$key.board_uni"]='required|regex:/^[a-z0-9 .\-]+$/i';
            $rules["education.$key.institute"]='required|regex:/^[a-z0-9 .\-]+$/i';
            $rules["education.$key.qualification"]='required|regex:/^[a-z0-9 .\-]+$/i';
            $rules["education.$key.specialization"]='required|regex:/^[a-z0-9 .\-]+$/i';
            $rules["education.$key.month"]='required';
            $rules["education.$key.year"]='required';

            $message["education.$key.board_uni.required"]='The board / uni field is required';
            $message["education.$key.board_uni.regex"]='The board / uni format is invalid';
            $message["education.$key.institute.required"]='The institute field is required';
            $message["education.$key.institute.regex"]='The institute format is invalid';
            $message["education.$key.qualification.required"]='The qualification field is required';
            $message["education.$key.qualification.regex"]='The qualification format is invalid';
            $message["education.$key.specialization.required"]='The specialization field is required';
            $message["education.$key.specialization.regex"]='The specialization format is invalid';
            $message["education.$key.month.required"]='The month field is required';
            $message["education.$key.year.required"]='The year field is required';
        }

        foreach($request->family as $key=>$value){
            $rules["family.$key.name"]='nullable|regex:/^[a-z0-9 .\-]+$/i';

            $message["family.$key.name.regex"]='The name format is invalid';
        }

        return $this->validate($request, $rules,array_merge([
            'aadhar_number.required_if' => 'The aadhar number field is required when availability of aadhar is available.',
            'aadhar_enrolment_number.required_if' => 'The aadhar enrolment number field is required when availability of aadhar is not available.',
            'pan_number.required_if' => 'The pan number field is required when availability of PAN is available.',
        ],$message));
    }

    public function getSubDepartments(Request $request,$departmentId=0)
    {
        return SubDepartment::where("status","1")->where('department_id',$request->get('department_id',$departmentId))->pluck('name','id')->toArray();
    }

    public function getSourceDetail(Request $request,$name=0)
    {
        return SourceCategory::where("status","1")->where('id',$request->get('name',$name))->first();
    }

    public function getLocationDetail(Request $request,$locationCode=0)
    {
        return StoreLocation::where("status","1")->where('id',$request->get('locationCode',$locationCode))->first();
    }

    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function getStateFromRegion(Request $request,$region=0)
    {
        return State::where("status","1")->where('region_id',$request->get('region_id',$region))->pluck('name','id')->toArray();
    }

    public function changeStatus($table,$status,$id)
    {
        $status = ($status == 0)?1:0;

     /*   if($table == 'invoice'){
            $status = ($status == 0)?'in_progress':'confirm';
        }*/

        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    }

    public function approve($id)
    {
        $employee = Employee::find($id);
        $employee->hrms_status = 2;
        $employee->hrms_approve_id = auth()->id();
        $employee->status = 'confirm';
        $employee->save();

        // create log
        $log['title'] = "Approved by HRMS";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        Session::flash('flash_success', 'Approved');

        return redirect('hrms/employee');
    }

    public function sendBack(Request $request, $id)
    {
        /*$this->validate($request,[
                'note'=>'required'
            ]);*/

        $employee = Employee::find($id);
        $employee->hrms_note = $request->note;
        $employee->hrms_status = 3;
        $employee->status = 'approve';
        $employee->save();

        $log['title'] = "Send Back to Regional HR";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['note'] = $request->note;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        return 'Send Back to Regional HR';
    }
}
