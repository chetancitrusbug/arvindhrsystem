<?php

namespace App\Http\Controllers\Hrms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use App\Helper;
use App\HiringReport;
use App\OtherInfo;

use Carbon\Carbon as Carbon;

class HiringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.hrms.employee');
        $this->middleware('permission:access.hrms.employee.edit')->only(['edit','update']);
        $this->middleware('permission:access.hrms.employee.create')->only(['create', 'store']);
        $this->middleware('permission:access.hrms.employee.delete')->only('destroy');

        view()->share('route', 'hiring');
        view()->share('module', 'Hiring');
    }

    public function index(Request $request)
    {
        $start = new Carbon('first day of this month');
        $end = new Carbon('last day of this month');
        /*$sourceName = SourceCategory::where("status","1")->pluck('category','id')->toArray();
        view()->share('sourceName',$sourceName);*/
        if($request->has('start_date') && $request->start_date && $request->start_date != ""){
			$start = \Carbon\Carbon::createFromFormat("d/m/Y",$request->start_date);
		}
		if($request->has('end_date') && $request->end_date && $request->end_date != ""){
			$end = \Carbon\Carbon::createFromFormat("d/m/Y",$request->end_date);
        }

        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        return view('hrms.hiring.index',compact('start','end'));
    }

    public function datatable(Request $request)
    {
        $start_date =  new \Carbon\Carbon('first day of January 2006');
        $end_date = \Carbon\Carbon::now()->format("Y-m-d");
        $start_date = $start_date->format("Y-m-d");
        if($request->has('start_date') && $request->start_date && $request->start_date != ""){
			$start_date = \Carbon\Carbon::createFromFormat("d/m/Y",$request->start_date);
		}
		if($request->has('end_date') && $request->end_date && $request->end_date != ""){
			$end_date = \Carbon\Carbon::createFromFormat("d/m/Y",$request->end_date);
        }
        $employee = Employee::select([
                    'employee.*',
                    'designation.name as designation',
                    \DB::raw("store_location.store_name"),
                    \DB::raw("store_location.location_code"),
                    'region.name as region',
                ])
                ->leftJoin('designation',function($join){
                    $join->on('employee.designation_id','=','designation.id');
                })
                ->leftJoin('store_location',function($join){
                    $join->on('store_location.id','=','employee.store_id');
                })
                ->leftJoin('region',function($join){
                    $join->on('region.id','=','store_location.id');
                })

                ->whereIn('regional_status',[2,4])
                ->whereNotIn('hrms_status',[2,3])
                ->whereBetween('employee.created_at',[$start_date,$end_date])
                ->orderBy('employee.id','DESC')
                ->latest()
                ->get();
        return Datatables::of($employee)
            ->make(true);
    }

    public function exportHiring(Request $request){
        if($request->has('start_date') && $request->start_date && $request->start_date != ""){
			$start_date = \Carbon\Carbon::createFromFormat("d/m/Y",$request->start_date);
		}
		if($request->has('end_date') && $request->end_date && $request->end_date != ""){
			$end_date = \Carbon\Carbon::createFromFormat("d/m/Y",$request->end_date);
        }
        $allEmployee = Employee::with(['department','subDepartmentEmp','grade','brand','designation','other_infos','employeeClassification','EducationDetail','storeLocation','businessUnit','legalEntity','sourceDetail','previousExperience','FamilyInfo','sourceCategory'])
                ->whereIn('regional_status',[2,4])
                ->whereNotIn('hrms_status',[2,3])
                ->whereBetween('employee.created_at',[$start_date,$end_date])
                ->orderBy('employee.id','DESC')
                //->where('employee.id','4515')
                ->latest()
                ->get();
        $excel_export['sheet1'] = [];
        $excel_export['education'] = [];
        $excel_export['experience'] = [];
        $excel_export['medical'] = [];

        $excel_export['sheet1']['header'] = array('BU HR Name','Temp Docket code','Employee Code','Employee Full Name','Date of joining','Grade','Designation','Legal Entity','Business Unit','Department','Sub-department','Brand','Employee Classification','Location Code','Cost Centre / IO Code','Store Customer Code','Store / Office Name','Office / Store address','Office / Store City','Office / Store State','Principal place of posting code','Principal place of posting_City','Principal place of posting_State','Store / Office category','Store / Office Type','Reporting Manager Ecode','Reporting Manager Name','Source Category','Source Name','Source Code','Relocation Benefits','Relocation Allowence','Notice Buyout Benefits','Notice period','Basic Salary','Statutory Bonus','House Rent Allowance (HRA)','Flexible Benefit Plan (FBP)*','Employer/\'s Contribution to Provident Fund (EPF)','Employer/\'s Contribution to Employee State Insurance (ESI)','Gratuity','Total Fixed Compensation','Performance based Variable Pay','Cost To Company','Monthly Fixed take home before Tax','Bonus','Variable Paytype','Medical coverage type','Bank Name','Bank Account Number','IFSC Code','Official Email ID','Personal Email ID','Mobile No.','Nationality','PAN Number','Aadhar Number','Aadhar Enrolment number','UAN Number','ESI number','Employee SAP Code','Gender','Date of Birth','Marital status','Wedding Anniversary date','Blood Group','RH Factor','Current Address','Current address : City','Current address : State','Current address : Pincode','Current address : Country','Permanent Address','Permanent address : City','Permanent address : State','Permanent address : Pincode','Permanent address : Country','Father’s Name','Emergency Contact Name','Emergency Contact Number','Emergency Contact Relationship');
        $educationIncreament = 0;
        $experienceIncreament = 0;
        $excel_export['education']['header']  = array('Docket Code','Employee code','Employee Full name','Actual DOJ','Category','Board / University','Name of the Institution','Qualification Name','Specialization','Month & Year of passing');
        $excel_export['experience']['header']  = array('Docket Code','Employee code','Employee Full name','Actual DOJ','Company Name*','Date of Joining* (DD/MM/YYYY)','Date of Leaving* (DD-MM-YYYY)','Nature of Employment*','Last held Designation*','Reason for leaving*');
        $excel_export['medical']['header']  = array('Sl No.','Location','ESIC Sub Code','Previous ESIC  No. (If any)','Employee Code','Name / Name as per Aadhaar Records:*','Name of Father / Spouce*','Relationship','Date of Birth dd/mm/yyyy','Employee Aadhaar Number*','Marital Status','Gender','Present Address','Present Pin','Present Mobile Number*','Present State','Present City','Present E Mail ID','Permanent Address','Permanent Pin','Permanent Mobile Number*','Permanent State','Permanent City','Dispensary E Mail ID','Dispensary State','Dispensary District','Dispensary','Date of Appointment:*','Nominee Name / Name as per Aadhaar Records *','Nominee Relationship with I.P*','Address of Nominee*','Nominee State','Nominee District','Nominee Pin','Nominee Mobile No','Nominee Aadhaar No','Father Name','Father Date of Birth dd/mm/yyyy','Father Aadhaar Number*','Mother Name','Mother Date of Birth dd/mm/yyyy','Mother Aadhaar Number*','Spouse Name','Spouse Date of Birth dd/mm/yyyy','Spouse Aadhaar Number*','Children 1','Children 1 Gender','Children 1 Date of Birth dd/mm/yyyy','Children 1 Aadhaar Number*','Children 2','Children 2 Gender','Children 2 Date of Birth dd/mm/yyyy','Children 2 Aadhaar Number*','Children 3','Children 3 Gender','Children 3 Date of Birth dd/mm/yyyy','Children 3 Aadhaar Number*');
        if(count($allEmployee) > 0){
            foreach ($allEmployee as $employee) {
                $excel_export['sheet1'][$employee->id]['BU HR Name'] = '';
                $excel_export['sheet1'][$employee->id]['Temp Docket code'] = $employee->id;

                $excel_export['sheet1'][$employee->id]['Employee Code'] = '';

                $excel_export['sheet1'][$employee->id]['Employee Full Name'] = ucwords(strtolower($employee->full_name));

                $excel_export['sheet1'][$employee->id]['Date of joining'] = $employee->joining_date;
                $excel_export['sheet1'][$employee->id]['Grade'] = $employee->grade->name;
                if(!empty($employee->designation)){
                    $excel_export['sheet1'][$employee->id]['Designation'] = $employee->designation->name;
                }else{
                    $excel_export['sheet1'][$employee->id]['Designation'] = '';
                }
                if(!empty($employee->legalEntity)){
                    $excel_export['sheet1'][$employee->id]['Legal Entity'] = $employee->legalEntity->name;
                }else{
                    $excel_export['sheet1'][$employee->id]['Legal Entity'] = '';
                }
                if(!empty($employee->businessUnit)){
                    $excel_export['sheet1'][$employee->id]['Business Unit'] = $employee->businessUnit->name;
                }else{
                    $excel_export['sheet1'][$employee->id]['Business Unit'] = '';
                }
                if(!empty($employee->department)){
                    $excel_export['sheet1'][$employee->id]['Department'] = $employee->department->name;
                }else{
                    $excel_export['sheet1'][$employee->id]['Department'] = '';
                }
                if(!empty($employee->subDepartmentEmp)){
                    $excel_export['sheet1'][$employee->id]['Sub-department'] = $employee->subDepartmentEmp->name;
                }else{
                    $excel_export['sheet1'][$employee->id]['Sub-department'] = '';
                }
                if(!empty($employee->brand)){
                    $excel_export['sheet1'][$employee->id]['Brand'] = $employee->brand->name;
                }else{
                    $excel_export['sheet1'][$employee->id]['Brand'] = '';
                }
                if(!empty($employee->employeeClassification)){
                    $excel_export['sheet1'][$employee->id]['Employee Classification'] = $employee->employeeClassification->name;
                }else{
                    $excel_export['sheet1'][$employee->id]['Employee Classification'] = '';
                }
                if(!empty($employee->storeLocation)){
                    $excel_export['sheet1'][$employee->id]['Location Code'] = $employee->storeLocation->location_code;
                    $excel_export['sheet1'][$employee->id]['Cost Centre / IO Code'] = $employee->storeLocation->io_code;
                    $excel_export['sheet1'][$employee->id]['Store Customer Code'] = $employee->storeLocation->sap_code;
                    $excel_export['sheet1'][$employee->id]['Store / Office Name'] = $employee->storeLocation->store_name;
                    $excel_export['sheet1'][$employee->id]['Office / Store address'] = $employee->storeLocation->address;
                    $excel_export['sheet1'][$employee->id]['Office / Store City'] = $employee->storeLocation->city->name;
                    $excel_export['sheet1'][$employee->id]['Office / Store State'] = $employee->storeLocation->state->name;
                    $excel_export['sheet1'][$employee->id]['Principal place of posting code'] = $employee->storeLocation->location_code;
                    $excel_export['sheet1'][$employee->id]['Principal place of posting_City'] = $employee->storeLocation->city->name;
                    $excel_export['sheet1'][$employee->id]['Principal place of posting_State'] = $employee->storeLocation->state->name;
                    $category = explode('-',$employee->storeLocation->location_code);
                    /* $excel_export['sheet1'][$employee->id]['Store / Office category'] = $category[1]; */
                    $excel_export['sheet1'][$employee->id]['Store / Office category'] = $employee->storeLocation->store_category;
                    $excel_export['sheet1'][$employee->id]['Store / Office Type'] = $employee->storeLocation->store_type;
                  /*  if(count($category) == 4){
                        $excel_export['sheet1'][$employee->id]['Store / Office Type'] = $category[2];
                    }else{
                        $excel_export['sheet1'][$employee->id]['Store / Office Type'] = 'NA';
                    } */
                    $excel_export['sheet1'][$employee->id]['Reporting Manager Ecode'] = $employee->storeLocation->storemanager->emp_id;
                    $excel_export['sheet1'][$employee->id]['Reporting Manager Name'] = $employee->storeLocation->storemanager->emp_name;
                }else{
                    $excel_export['sheet1'][$employee->id]['Location Code'] = '';
                    $excel_export['sheet1'][$employee->id]['Cost Centre / IO Code'] = '';
                    $excel_export['sheet1'][$employee->id]['Store Customer Code'] = '';
                    $excel_export['sheet1'][$employee->id]['Store / Office Name'] = '';
                    $excel_export['sheet1'][$employee->id]['Office / Store address'] = '';
                    $excel_export['sheet1'][$employee->id]['Office / Store City'] = '';
                    $excel_export['sheet1'][$employee->id]['Office / Store State'] = '';
                    $excel_export['sheet1'][$employee->id]['Principal place of posting code'] = '';
                    $excel_export['sheet1'][$employee->id]['Principal place of posting_City'] = '';
                    $excel_export['sheet1'][$employee->id]['Principal place of posting_State'] = '';
                    $excel_export['sheet1'][$employee->id]['Store / Office category'] = '';
                    $excel_export['sheet1'][$employee->id]['Store / Office Type'] = '';
                    $excel_export['sheet1'][$employee->id]['Reporting Manager Ecode'] = '';
                    $excel_export['sheet1'][$employee->id]['Reporting Manager Name'] = '';
                }
                if(!empty($employee->sourceCategory)){
                    $excel_export['sheet1'][$employee->id]['Source Category'] = $employee->sourceCategory->category;
                }else{
                    $excel_export['sheet1'][$employee->id]['Source Category']  = '';
                }
                if(!empty($employee->sourceDetail)){
                    $excel_export['sheet1'][$employee->id]['Source Name'] = $employee->sourceDetail->source_name;
                }else{
                    $excel_export['sheet1'][$employee->id]['Source Name']  = '';
                }
                $excel_export['sheet1'][$employee->id]['Source Code'] = $employee->source_code;
                $excel_export['sheet1'][$employee->id]['Relocation Benefits'] = 'No';
                $excel_export['sheet1'][$employee->id]['Relocation Allowence'] = 0;
                $excel_export['sheet1'][$employee->id]['Notice Buyout Benefits'] = 'No';
                $excel_export['sheet1'][$employee->id]['Notice period'] = '';
                $excel_export['sheet1'][$employee->id]['Basic Salary'] = '';
                $excel_export['sheet1'][$employee->id]['Statutory Bonus'] = '';
                $excel_export['sheet1'][$employee->id]['House Rent Allowance (HRA)'] = '';
                $excel_export['sheet1'][$employee->id]['Flexible Benefit Plan (FBP)*'] = '';
                $excel_export['sheet1'][$employee->id]['Employer/\'s Contribution to Provident Fund (EPF)'] = '';
                $excel_export['sheet1'][$employee->id]['Employer/\'s Contribution to Employee State Insurance (ESI)'] = '';
                $excel_export['sheet1'][$employee->id]['Gratuity'] = '';
                $excel_export['sheet1'][$employee->id]['Total Fixed Compensation'] = '';
                $excel_export['sheet1'][$employee->id]['Performance based Variable Pay'] = '';
                $excel_export['sheet1'][$employee->id]['Cost To Company'] = '';
                $excel_export['sheet1'][$employee->id]['Monthly Fixed take home before Tax'] = '';
                $excel_export['sheet1'][$employee->id]['Bonus'] = '';
                $excel_export['sheet1'][$employee->id]['Variable Paytype'] = '';
                $excel_export['sheet1'][$employee->id]['Medical coverage type'] = '';
                $excel_export['sheet1'][$employee->id]['Bank Name'] = $employee->bank_name;
                $excel_export['sheet1'][$employee->id]['Bank Account Number'] = $employee->account_no;
                $excel_export['sheet1'][$employee->id]['IFSC Code'] = $employee->ifsc;
                $excel_export['sheet1'][$employee->id]['Official Email ID'] = '';
                $excel_export['sheet1'][$employee->id]['Personal Email ID'] = $employee->email;
                $excel_export['sheet1'][$employee->id]['Mobile No.'] = $employee->mobile;
                $excel_export['sheet1'][$employee->id]['Nationality'] = ucfirst($employee->nationality);
                $excel_export['sheet1'][$employee->id]['PAN Number'] = $employee->pan_number;
                $excel_export['sheet1'][$employee->id]['Aadhar Number'] =  $employee->aadhar_number;
                $excel_export['sheet1'][$employee->id]['Aadhar Enrolment number'] = $employee->aadhar_enrolment_number;
                $excel_export['sheet1'][$employee->id]['UAN Number'] = $employee->ua_number;
                $excel_export['sheet1'][$employee->id]['ESI number'] =  $employee->esic_number;
                $excel_export['sheet1'][$employee->id]['Employee SAP Code'] = '';
                $excel_export['sheet1'][$employee->id]['Gender'] = ucfirst($employee->gender);
                $excel_export['sheet1'][$employee->id]['Date of Birth'] = $employee->date_of_birth;
                $excel_export['sheet1'][$employee->id]['Marital status'] = ucfirst($employee->marital_status);
                if($employee->marital_status == 'married'){
                    $excel_export['sheet1'][$employee->id]['Wedding Anniversary date'] = $employee->anniversary_date;
                }else{
                    $excel_export['sheet1'][$employee->id]['Wedding Anniversary date'] = '';
                }

                $excel_export['sheet1'][$employee->id]['Blood Group'] = $employee->blood_group;
                $excel_export['sheet1'][$employee->id]['RH Factor'] = ucfirst($employee->rh_factor);
                $excel_export['sheet1'][$employee->id]['Current Address'] = ucfirst(strtolower($employee->current_address_line_1)).', '.ucfirst( strtolower($employee->current_address_line_2)).','.ucfirst( strtolower($employee->current_address_line_3));
                $excel_export['sheet1'][$employee->id]['Current address : City'] = ucfirst($employee->currentCity->name);
                $excel_export['sheet1'][$employee->id]['Current address : State'] = ucfirst($employee->currentState->name);
                $excel_export['sheet1'][$employee->id]['Current address : Pincode'] = $employee->current_pincode;
                $excel_export['sheet1'][$employee->id]['Current address : Country'] = 'India';
                $excel_export['sheet1'][$employee->id]['Permanent Address'] = ucfirst( strtolower($employee->permanent_address_line_1)).', '.ucfirst( strtolower($employee->permanent_address_line_2)).','.ucfirst( strtolower($employee->permanent_address_line_3));
                $excel_export['sheet1'][$employee->id]['Permanent address : City'] = ucfirst($employee->permanentCity->name);
                $excel_export['sheet1'][$employee->id]['Permanent address : State'] = ucfirst($employee->permanentState->name);
                $excel_export['sheet1'][$employee->id]['Permanent address : Pincode'] = $employee->permanent_pincode;
                $excel_export['sheet1'][$employee->id]['Permanent address : Country'] = 'India';


                // $excel_export[$employee->id]['qualification'] = $employee->EducationDetail[0]->qualification_name;
                // $excel_export[$employee->id]['board_uni'] = $employee->EducationDetail[0]->board_uni;
                // $excel_export[$employee->id]['institute'] = $employee->EducationDetail[0]->institute;
                // $excel_export[$employee->id]['specialization'] = $employee->EducationDetail[0]->specialization;
                // $excel_export[$employee->id]['passing_month'] = $employee->EducationDetail[0]->month." - ".$employee->EducationDetail[0]->year;
                foreach($employee->EducationDetail as $education){
                    if($education->education_category_id > 0){
                        //$excel_export['education'][$educationIncreament] = array();
                        $excel_export['education'][$educationIncreament]['Temp Docket code'] = $employee->id;
                        $excel_export['education'][$educationIncreament]['Employee Code'] = '';
                        $excel_export['education'][$educationIncreament]['Employee Full Name'] = ucwords(strtolower($employee->full_name));
                        $excel_export['education'][$educationIncreament]['Actual DOJ'] = $employee->joining_date;
                        $excel_export['education'][$educationIncreament]['Category'] = ucwords(strtolower($education->getCategoryName->name));
                        $excel_export['education'][$educationIncreament]['Board / University'] = ucwords(strtolower($education->board_uni));
                        $excel_export['education'][$educationIncreament]['Name of the Institution'] = ucwords(strtolower($education->institute));
                        $excel_export['education'][$educationIncreament]['Qualification Name'] = ucwords(strtolower($education->qualification));
                        $excel_export['education'][$educationIncreament]['Specialization'] = ucwords(strtolower($education->specialization));
                        $excel_export['education'][$educationIncreament]['Month & Year of passing'] = strtoupper($education->month." - ".$education->year);
                        $educationIncreament++;
                    }
                }

                if($employee->previousExperience->count() > 0){
                    foreach($employee->previousExperience as $experience){
                        if($experience->nature_of_employment_id > 0){
                            //$excel_export['education'][$educationIncreament] = array();
                            //'Company Name*','Date of Joining* (DD/MM/YYYY)','Date of Leaving* (DD-MM-YYYY)','Nature of Employment*','Last held Designation*','Reason for leaving*'
                            $excel_export['experience'][$experienceIncreament]['Temp Docket code'] = $employee->id;
                            $excel_export['experience'][$experienceIncreament]['Employee Code'] = '';
                            $excel_export['experience'][$experienceIncreament]['Employee Full Name'] = ucwords(strtolower($employee->full_name));
                            $excel_export['experience'][$experienceIncreament]['Actual DOJ'] = $employee->joining_date;
                            $excel_export['experience'][$experienceIncreament]['Company Name*'] = $experience->company_name;
                            $excel_export['experience'][$experienceIncreament]['Date of Joining* (DD/MM/YYYY)'] = date('d/m/Y',strtotime($experience->joining_date));
                            $excel_export['experience'][$experienceIncreament]['Date of Leaving* (DD-MM-YYYY)'] = date('d/m/Y',strtotime($experience->leaving_date));
                            $excel_export['experience'][$experienceIncreament]['Nature of Employment*'] = $experience->natureEmployee->name;
                            $excel_export['experience'][$experienceIncreament]['Last held Designation*'] = ucfirst(strtolower($experience->designation));
                            $excel_export['experience'][$experienceIncreament]['Reason for leaving*'] = ucfirst(strtolower($experience->reason_of_leaving));
                            $experienceIncreament++;
                        }
                    }
                } else {
                    $excel_export['experience'][$experienceIncreament]['Temp Docket code'] = $employee->id;
                    $excel_export['experience'][$experienceIncreament]['Employee Code'] = '';
                    $excel_export['experience'][$experienceIncreament]['Employee Full Name'] = ucwords(strtolower($employee->full_name));
                    $excel_export['experience'][$experienceIncreament]['Actual DOJ'] = $employee->joining_date;
                    $excel_export['experience'][$experienceIncreament]['Company Name*'] = "Fresher";
                    $excel_export['experience'][$experienceIncreament]['Date of Joining* (DD/MM/YYYY)'] = "";
                    $excel_export['experience'][$experienceIncreament]['Date of Leaving* (DD-MM-YYYY)'] = "";
                    $excel_export['experience'][$experienceIncreament]['Nature of Employment*'] = "";
                    $excel_export['experience'][$experienceIncreament]['Last held Designation*'] = "";
                    $excel_export['experience'][$experienceIncreament]['Reason for leaving*'] = "";
                    $experienceIncreament++;
                }
               // dd($employee->previousExperience);
              /*  foreach($employee->previousExperience as $experience){
                    if($experience->nature_of_employment_id > 0){
                        //$excel_export['education'][$educationIncreament] = array();
                        //'Company Name*','Date of Joining* (DD/MM/YYYY)','Date of Leaving* (DD-MM-YYYY)','Nature of Employment*','Last held Designation*','Reason for leaving*'
                        $excel_export['experience'][$experienceIncreament]['Temp Docket code'] = $employee->id;
                        $excel_export['experience'][$experienceIncreament]['Employee Code'] = '';
                        $excel_export['experience'][$experienceIncreament]['Employee Full Name'] = $employee->full_name;
                        $excel_export['experience'][$experienceIncreament]['Actual DOJ'] = $employee->joining_date;
                        $excel_export['experience'][$experienceIncreament]['Company Name*'] = $experience->company_name;
                        $excel_export['experience'][$experienceIncreament]['Date of Joining* (DD/MM/YYYY)'] = date('d/m/Y',strtotime($experience->joining_date));
                        $excel_export['experience'][$experienceIncreament]['Date of Leaving* (DD-MM-YYYY)'] = date('d/m/Y',strtotime($experience->leaving_date));
                        $excel_export['experience'][$experienceIncreament]['Nature of Employment*'] = $experience->natureEmployee->name;
                        $excel_export['experience'][$experienceIncreament]['Last held Designation*'] = $experience->designation;
                        $excel_export['experience'][$experienceIncreament]['Reason for leaving*'] = $experience->reason_of_leaving;
                        $experienceIncreament++;
                    }
                } */
                $excel_export['medical'][$employee->id]['Sl No.'] = $employee->id;
                $excel_export['medical'][$employee->id]['Location'] = ucfirst(($employee->currentCity->name));
                $excel_export['medical'][$employee->id]['ESIC Sub Code'] = '-';
                $excel_export['medical'][$employee->id]['Previous ESIC  No. (If any)'] = '';
                $excel_export['medical'][$employee->id]['Employee Code'] = '';
                $excel_export['medical'][$employee->id]['Name / Name as per Aadhaar Records:*'] = ucfirst(strtotime($employee->full_name));

                $child = 1;

                 /* Changes for Excel */
                $excel_export['medical'][$employee->id]['Name of Father / Spouce*'] = "";
                $excel_export['medical'][$employee->id]['Relationship'] = 'Father';
                $excel_export['medical'][$employee->id]['Date of Birth dd/mm/yyyy'] = $employee->date_of_birth;
                $excel_export['medical'][$employee->id]['Employee Aadhaar Number*'] = $employee->aadhar_number;
                $excel_export['medical'][$employee->id]['Marital Status'] =  ucfirst($employee->marital_status);
                $excel_export['medical'][$employee->id]['Gender'] =  ucfirst($employee->gender);
                $excel_export['medical'][$employee->id]['Present Address'] =  $employee->current_address_line_1.','.$employee->current_address_line_2.','.$employee->current_address_line_3;
                $excel_export['medical'][$employee->id]['Present Pin'] =  $employee->current_pincode;
                $excel_export['medical'][$employee->id]['Present Mobile Number*'] =  $employee->mobile;
                $excel_export['medical'][$employee->id]['Present State'] = $employee->currentState->name;
                $excel_export['medical'][$employee->id]['Present City'] =  $employee->currentCity->name;
                $excel_export['medical'][$employee->id]['Present E Mail ID'] =  strtolower($employee->email);
                $excel_export['medical'][$employee->id]['Permanent Address'] =  $employee->permanent_address_line_1.','.$employee->permanent_address_line_2.','.$employee->permanent_address_line_3;
                $excel_export['medical'][$employee->id]['Permanent Pin'] =  $employee->permanent_pincode;
                $excel_export['medical'][$employee->id]['Permanent Mobile Number*'] =  $employee->mobile;
                $excel_export['medical'][$employee->id]['Permanent State'] =  $employee->permanentState->name;
                $excel_export['medical'][$employee->id]['Permanent City'] =  $employee->permanentCity->name;
                $excel_export['medical'][$employee->id]['Dispensary E Mail ID'] =  '';
                $excel_export['medical'][$employee->id]['Dispensary State'] =  '';
                $excel_export['medical'][$employee->id]['Dispensary District'] =  '';
                $excel_export['medical'][$employee->id]['Dispensary'] =  '';
                $excel_export['medical'][$employee->id]['Date of Appointment:*'] =   $employee->joining_date;

                $excel_export['medical'][$employee->id]['Nominee Name / Name as per Aadhaar Records *'] =  '';
                $excel_export['medical'][$employee->id]['Nominee Relationship with I.P *'] = '';
                $excel_export['medical'][$employee->id]['Address of Nominee*'] =  '';
                $excel_export['medical'][$employee->id]['Nominee State'] =  '';
                $excel_export['medical'][$employee->id]['Nominee District'] =  '';
                $excel_export['medical'][$employee->id]['Nominee Pin'] =  '';
                $excel_export['medical'][$employee->id]['Nominee Mobile No'] =  '';
                $excel_export['medical'][$employee->id]['Nominee Aadhaar No'] =  '';
                /* Changes for Excel Father */
                // $excel_export['sheet1'][$employee->id]['Father’s Name'] = ucfirst(strtolower($family_detail->name));
                $excel_export['medical'][$employee->id]['Father Name'] =  "-";
                $excel_export['medical'][$employee->id]['Relationship'] =  "-";
                $excel_export['medical'][$employee->id]['Father Date of Birth dd/mm/yyyy'] = "-";
                $excel_export['medical'][$employee->id]['Father Aadhaar Number*'] =  "-";

                /* Mother */
                $excel_export['medical'][$employee->id]['Mother Name'] =  "-";
                $excel_export['medical'][$employee->id]['Relationship'] =  "-";
                $excel_export['medical'][$employee->id]['Mother Date of Birth dd/mm/yyyy'] =  "-";
                $excel_export['medical'][$employee->id]['Mother Aadhaar Number*'] =  "-";

                /* Spouse */
                $excel_export['medical'][$employee->id]['Spouse Name'] =  "-";
                $excel_export['medical'][$employee->id]['Relationship'] =  "-";
                $excel_export['medical'][$employee->id]['Spouse Date of Birth dd/mm/yyyy'] =  "-";
                $excel_export['medical'][$employee->id]['Spouse Aadhaar Number*'] =  "-";

                /* Child */
                $excel_export['medical'][$employee->id]['Children 1'] =  "-";
                $excel_export['medical'][$employee->id]['Children 1 Gender'] =  "-";
                $excel_export['medical'][$employee->id]['Children 1 Date of Birth dd/mm/yyyy'] =  "-";
                $excel_export['medical'][$employee->id]['Children 1 Aadhaar Number*'] =  "-";

                $excel_export['medical'][$employee->id]['Children 2'] =  "-";
                $excel_export['medical'][$employee->id]['Children 2 Gender'] =  "-";
                $excel_export['medical'][$employee->id]['Children 2 Date of Birth dd/mm/yyyy'] =  "-";
                $excel_export['medical'][$employee->id]['Children 2 Aadhaar Number*'] =  "-";

                $excel_export['medical'][$employee->id]['Children 3'] =  "-";
                $excel_export['medical'][$employee->id]['Children 3 Gender'] =  "-";
                $excel_export['medical'][$employee->id]['Children 3 Date of Birth dd/mm/yyyy'] =  "-";
                $excel_export['medical'][$employee->id]['Children 3 Aadhaar Number*'] =  "-";
                $excel_export['sheet1'][$employee->id]['Father’s Name'] = '';
                foreach ($employee->FamilyInfo as $family_detail) {

                    if($family_detail->relation == 'Father'){
                        $excel_export['sheet1'][$employee->id]['Father’s Name'] = ucwords(strtolower($family_detail->name));
                        $excel_export['medical'][$employee->id]['Father Name'] =  ucwords(strtolower($family_detail->name));
                        $excel_export['medical'][$employee->id]['Relationship'] =  $family_detail->relation;
                        $excel_export['medical'][$employee->id]['Father Date of Birth dd/mm/yyyy'] = ($family_detail->date.'/'.$family_detail->month.'/'.$family_detail->year != '//')? $family_detail->date.'/'.$family_detail->month.'/'.$family_detail->year : '' ;
                        $excel_export['medical'][$employee->id]['Father Aadhaar Number*'] =  $family_detail->aadhar_number;
                    }
                    elseif ($family_detail->relation == 'Mother') {
                        $excel_export['medical'][$employee->id]['Mother Name'] =  ucfirst(strtolower($family_detail->name));
                        $excel_export['medical'][$employee->id]['Relationship'] =  $family_detail->relation;
                        $excel_export['medical'][$employee->id]['Mother Date of Birth dd/mm/yyyy'] =  ($family_detail->date.'/'.$family_detail->month.'/'.$family_detail->year != '//')? $family_detail->date.'/'.$family_detail->month.'/'.$family_detail->year : '';
                        $excel_export['medical'][$employee->id]['Mother Aadhaar Number*'] =  $family_detail->aadhar_number;
                    } elseif ($family_detail->relation == 'Spouse') {
                        $excel_export['medical'][$employee->id]['Spouse Name'] =  ucfirst(strtolower($family_detail->name));
                        // $excel_export['medical'][$employee->id]['Relationship'] =  $family_detail->relation;
                        $excel_export['medical'][$employee->id]['Spouse Date of Birth dd/mm/yyyy'] =  ($family_detail->date.'/'.$family_detail->month.'/'.$family_detail->year != '//')? $family_detail->date.'/'.$family_detail->month.'/'.$family_detail->year : '';
                        $excel_export['medical'][$employee->id]['Spouse Aadhaar Number*'] =  $family_detail->aadhar_number;
                    } elseif ($family_detail->relation == 'Child' && $child < 4) {
                        if($family_detail->name){
                            $excel_export['medical'][$employee->id]['Children '.$child] = ucfirst(strtolower( $family_detail->name));
                            // $excel_export['medical'][$employee->id]['Relationship'] =  $family_detail->relation;
                            $excel_export['medical'][$employee->id]['Children '.$child.' Gender'] =  ucfirst(strtolower($family_detail->gender));

                            $excel_export['medical'][$employee->id]['Children '.$child.' Date of Birth dd/mm/yyyy'] =  ($family_detail->date.'/'.$family_detail->month.'/'.$family_detail->year != '//')? $family_detail->date.'/'.$family_detail->month.'/'.$family_detail->year : '';
                            $excel_export['medical'][$employee->id]['Children '.$child.' Aadhaar Number*'] =  $family_detail->aadhar_number;
                            $child++;
                        }
                    }
                }
                $excel_export['medical'][$employee->id]['Name of Father / Spouce*'] = ucwords($excel_export['medical'][$employee->id]['Father Name']);
                $excel_export['medical'][$employee->id]['Relationship'] = 'Father';
                $excel_export['medical'][$employee->id]['Date of Birth dd/mm/yyyy'] = $employee->date_of_birth;
                $excel_export['medical'][$employee->id]['Employee Aadhaar Number*'] = $employee->aadhar_number;
                $excel_export['medical'][$employee->id]['Marital Status'] =  ucfirst(strtolower($employee->marital_status));
                $excel_export['medical'][$employee->id]['Gender'] =  ucfirst(strtolower($employee->gender));
                $excel_export['medical'][$employee->id]['Present Address'] =  ucfirst(strtolower($employee->current_address_line_1.','.$employee->current_address_line_2.','.$employee->current_address_line_3));
                $excel_export['medical'][$employee->id]['Present Pin'] =  $employee->current_pincode;
                $excel_export['medical'][$employee->id]['Present Mobile Number*'] =  $employee->mobile;
                $excel_export['medical'][$employee->id]['Present State'] = ucfirst(strtolower($employee->currentState->name));
                $excel_export['medical'][$employee->id]['Present City'] =  ucfirst(strtolower($employee->currentCity->name));
                $excel_export['medical'][$employee->id]['Present E Mail ID'] =  $employee->email;
                $excel_export['medical'][$employee->id]['Permanent Address'] =  ucfirst(strtolower($employee->permanent_address_line_1.','.$employee->permanent_address_line_2.','.$employee->permanent_address_line_3));
                $excel_export['medical'][$employee->id]['Permanent Pin'] =  $employee->permanent_pincode;
                $excel_export['medical'][$employee->id]['Permanent Mobile Number*'] =  $employee->mobile;
                $excel_export['medical'][$employee->id]['Permanent State'] =  ucfirst(strtolower($employee->permanentState->name));
                $excel_export['medical'][$employee->id]['Permanent City'] =  ucfirst(strtolower($employee->permanentCity->name));
                $excel_export['medical'][$employee->id]['Dispensary E Mail ID'] =  '';
                $excel_export['medical'][$employee->id]['Dispensary State'] =  '';
                $excel_export['medical'][$employee->id]['Dispensary District'] =  '';
                $excel_export['medical'][$employee->id]['Dispensary'] =  '';
                $excel_export['medical'][$employee->id]['Date of Appointment:*'] =   $employee->joining_date;
                $excel_export['medical'][$employee->id]['Nominee Name / Name as per Aadhaar Records *'] =  '';
                $excel_export['medical'][$employee->id]['Nominee Relationship with I.P *'] = '';
                $excel_export['medical'][$employee->id]['Address of Nominee*'] =  '';
                $excel_export['medical'][$employee->id]['Nominee State'] =  '';
                $excel_export['medical'][$employee->id]['Nominee District'] =  '';
                $excel_export['medical'][$employee->id]['Nominee Pin'] =  '';
                $excel_export['medical'][$employee->id]['Nominee Mobile No'] =  '';
                $excel_export['medical'][$employee->id]['Nominee Aadhaar No'] =  '';

                $excel_export['sheet1'][$employee->id]['Emergency Contact Name'] = ($employee->emergency_person != ' ') ? ucwords(strtolower($employee->emergency_person)): ' ';
                $excel_export['sheet1'][$employee->id]['Emergency Contact Number'] = $employee->emergency_number;
                $excel_export['sheet1'][$employee->id]['Emergency Contact Relationship'] = ($employee->emergency_relationship != '') ? ucfirst($employee->emergency_relationship) : ' ';
                //foreach($employee->)
            }

        }
        \Excel::create("All Hiring Employee - ". date('d-m-Y H-i-s'),function($excel) use ($excel_export){
            $excel->setTitle('All Hiring Employee');
            $excel->sheet('Sheet1',function($sheet) use ($excel_export){
                $sheet->fromArray($excel_export['sheet1'],null,'A1',false,false);
            });
            $excel->sheet('Education', function($sheet) use ($excel_export) {
                $sheet->fromArray($excel_export['education'],null,'A1',false,false);
            });
            $excel->sheet('Experience', function($sheet) use ($excel_export) {
                $sheet->fromArray($excel_export['experience'],null,'A1',false,false);
            });
            $excel->sheet('Form Medical', function($sheet) use ($excel_export) {
                $sheet->fromArray($excel_export['medical'],null,'A1',false,false);
            });
        })->download('xlsx');
        return redirect()->back();
    }
    public function logView(Request $request,$id)
    {
        view()->share('id',$id);
        return view('hrms.hiring.log');
    }

    public function log(Request $request,$id)
    {
        $logs = Log::select([
                    'logs.*',
                    'users.emp_name as user_name',
                    'roles.label as user_role',
                ])
                ->leftJoin('users',function($join){
                    $join->on('users.id','=','logs.user_id');
                })
                ->leftJoin('roles',function($join){
                    $join->on('roles.id','=','logs.user_role_id');
                })
                // ->where('user_id',auth()->id())
                ->where('logs.emp_id',$id)
                ->orderBy('logs.id','DESC')
                ->latest()->get();

        return Datatables::of($logs)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return redirect('hrms/hiring');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //not allowed
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($id);

        if(!$employee){
            return redirect('hrms/hiring')->with('flash_error', 'Employee Not Found!');
        }

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();

        $employee->otherinfo = \App\OtherInfo::where('employee_id',$id)->first();

        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        return view('hrms.hiring.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        if(!$employee){
            return redirect('hrms/hiring')->with('flash_error', 'Employee Not Found!');
        }

        $request = new Request;
        $currentCity = $permanentCity = [];
        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        $city = City::where("status","1")->pluck('name','id')->toArray();
        view()->share('city',$city);

        if(old('current_state',$employee->current_state)){
            $currentCity = $this->getCityFromState($request,['state_id'=>old('current_state',$employee->current_state)]);
        }
        view()->share('currentCity',$currentCity);

        if(old('permanent_state',$employee->permanent_state)){
            $permanentCity = $this->getCityFromState($request,['state_id'=>old('permanent_state',$employee->permanent_state)]);
        }
        view()->share('permanentCity',$permanentCity);

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        $locationCode = StoreLocation::where("status","1")->pluck('location_code','id')->toArray();
        view()->share('locationCode',$locationCode);

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);

        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $subDepartments = [];
        if(old('department_id',$employee->department_id)){
            $subDepartments = $this->getSubDepartments($request,['department_id'=>old('department_id',$employee->department_id)]);
        }
        view()->share('subDepartments',$subDepartments);

        $businessUnits = BusinessUnit::where("status","1")->pluck('name','id')->toArray();
        view()->share('businessUnits',$businessUnits);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->pluck('name','id')->toArray();
        view()->share('employeeClassification',$employeeClassification);

        $variablePayType = VariablePayType::where("status","1")->pluck('name','id')->toArray();
        view()->share('variablePayType',$variablePayType);

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        return view('hrms.hiring.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        if($requestData['pan_status'] == 'not_available'){
            $requestData['pan_number'] = '';
        }
        if($requestData['aadhar_status'] == 'not_available'){
            $requestData['aadhar_number'] = '';
        }
        if($requestData['aadhar_status'] == 'available'){
            $requestData['aadhar_enrolment_number'] = '';
        }
        $requestData['date_of_birth'] = Helper::ymd(str_replace('/', '-', $requestData['date_of_birth']));
        $requestData['anniversary_date'] = Helper::ymd(str_replace('/', '-', $requestData['anniversary_date']));
        $requestData['first_time_employment'] = isset($requestData['first_time_employment'])?1:0;

        if($request->button == 'Update & approve'){
            $requestData['hrms_approve_id'] = auth()->id();
            $requestData['hrms_status'] = 2;
            $requestData['status'] = 'confirm';
            $requestData['processstatus'] = 'Active';
        }elseif($request->button == 'Update & Send Back to RM'){
            $requestData['hrms_status'] = 3;
            $requestData['status'] = 'approve';
        }

        $employee = Employee::where('id',$id)->update(array_except($requestData,['attach','_method','_token','button','education','previous','family']));
        if(isset($requestData['hrms_status']) && $requestData['hrms_status'] == 2){
            $this->AddEmployeeInHiringReport($employee->store_id);
        }

        if(isset($requestData['education']) && !empty($requestData['education'])){
            foreach ($requestData['education'] as $value) {
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    $value['deleted_at'] = null;
                    \DB::table('education_detail')->where('id',$value['id'])->update($value);
                }
            }
        }

        if($requestData['first_time_employment'] == 1 && isset($requestData['previous']) && !empty($requestData['previous'])){
            foreach ($requestData['previous'] as $value) {

                if($value['joining_date']){
                    $value['joining_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $value['joining_date']))->format('Y-m-d');
                }
                if($value['leaving_date']){
                    $value['leaving_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $value['leaving_date']))->format('Y-m-d');
                }
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    $value['deleted_at'] = null;
                    \DB::table('previous_experience')->where('id',$value['id'])->update($value);
                }else{
                    PreviousExperience::insert($value);
                }
            }
        }else{
            PreviousExperience::where('employee_id',$id)->delete();
        }

        if(isset($requestData['family']) && !empty($requestData['family'])){
            foreach ($requestData['family'] as $value) {
                $value['employee_id'] = $id;
                if($value['date'] == ''){
                    $value['date'] = '';
                    $value['month'] = '';
                    $value['year'] = '';
                }else{
                    $value['date'] = \Carbon\Carbon::parse(str_replace('/', '-', $value['date']))->format('Y-m-d');
                    $date = explode('-', $value['date']);
                    $value['date'] = $date[2];
                    $value['month'] = $date[1];
                    $value['year'] = $date[0];
                }
                if(isset($value['id'])){
                    $value['deleted_at'] = null;
                    \DB::table('family_information')->where('id',$value['id'])->update($value);
                }else{
                    FamilyInfo::insert($value);
                }
            }
        }

        // create log
        $log['title'] = "Employee Update";
        $log['emp_id'] = $id;
        $log['emp_name'] = $request->full_name;
        $log['note'] = $request->hrms_note;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        if($request->button == 'Update & approve'){
            return redirect('hrms/hiring')->with('flash_success', 'Employee updated and approved!');
        }elseif($request->button == 'Update & Send Back to RM'){
            return redirect('hrms/hiring')->with('flash_success', 'Employee updated and send back to Regional HR!');
        }
        return redirect('hrms/hiring')->with('flash_success', 'Employee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        Employee::find($id)->delete();
        EducationDetail::where('employee_id',$id)->delete();
        PreviousExperience::where('employee_id',$id)->delete();
        FamilyInfo::where('employee_id',$id)->delete();

        if($request->has('from_index')){
            $message = "Employee Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee deleted!');

            return redirect('hrms/hiring');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'designation_id' => 'required',
            'full_name' => 'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'position' => 'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'blood_group' => 'required',
            'rh_factor' => 'required',
            'date_of_birth' => 'required|date_format:d/m/Y',
            'anniversary_date' => 'nullable|date_format:d/m/Y',
            'marital_status' => 'required',
            'aadhar_status' => 'required',
            'nationality' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'gender' => 'required',
            'pan_status' => 'required',
            'mobile' => ['required','numeric',function($attribute, $value, $fail) use($request,$id) {
                            if(strlen($value) != 10){
                                $fail('The mobile no must be of 10 digits');
                            }
                        }],
            'email' => 'nullable|email',
            'emergency_person' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'emergency_number' => 'required|numeric',
            'emergency_relationship' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'current_address_line_1' => 'required|max:191',
            'current_address_line_2' => 'nullable|max:191',
            'current_address_line_3' => 'nullable|max:191',
            'current_state' => 'required',
            'current_city' => 'required',
            'current_pincode' => 'required|min:6|max:6',
            'permanent_address_line_1' => 'required|max:191',
            'permanent_address_line_2' => 'nullable|max:191',
            'permanent_address_line_3' => 'nullable|max:191',
            'permanent_state' => 'required',
            'permanent_city' => 'required',
            'permanent_pincode' => 'required|min:6|max:6',
            'aadhar_number' => 'nullable|required_if:aadhar_status,available|regex:/^[a-z0-9 .\-]+$/i',
            'aadhar_enrolment_number' => 'nullable|required_if:aadhar_status,not_available|regex:/^[a-z0-9 .\-]+$/i',
            'education'=>[
                        function($attribute, $value, $fail) use($request,$id) {
                            $status = true;
                            foreach ($value as $value) {
                                if($value['education_category_id'] != '' ||
                                    $value['board_uni'] != '' ||
                                    $value['institute'] != '' ||
                                    $value['qualification'] != '' ||
                                    $value['specialization'] != '' ||
                                    $value['month'] != '' ||
                                    $value['year'] != ''
                                        ){
                                    $status = false;
                                }
                            }
                            if($status){
                                $education = 0;
                                $fail('atleast one education detail is required');
                            }
                        },
                    ],
        ];

        if($request->pan_status == 'available'){
            $rules['pan_number'] = 'nullable|required_if:pan_status,available|size:10|regex:/^[a-z0-9 .\-]+$/i';
        }

        $message = [];
        foreach($request->education as $key=>$value){
            $rules["education.$key.board_uni"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.institute"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.qualification"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.specialization"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';

            $message["education.$key.board_uni.regex"]='The board / uni format is invalid';
            $message["education.$key.board_uni.max"]='The board / uni may not be greater than 191 characters.';
            $message["education.$key.institute.regex"]='The institute format is invalid';
            $message["education.$key.institute.max"]='The institute may not be greater than 191 characters.';
            $message["education.$key.qualification.regex"]='The qualification format is invalid';
            $message["education.$key.qualification.max"]='The qualification may not be greater than 191 characters.';
            $message["education.$key.specialization.regex"]='The specialization format is invalid';
            $message["education.$key.specialization.max"]='The qualification may not be greater than 191 characters.';
        }

        if(isset($request->first_time_employment)){
            foreach($request->previous as $key=>$value){
                $rules["previous.$key.company_name"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.designation"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.reason_of_leaving"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.joining_date"]='nullable|date_format:d/m/Y';
                $rules["previous.$key.leaving_date"]='nullable|date_format:d/m/Y|after:'.$value['joining_date'];

                $message["previous.$key.company_name.regex"]='The company name format is invalid';
                $message["previous.$key.company_name.max"]='The company name may not be greater than 191 characters.';
                $message["previous.$key.designation.regex"]='The designation format is invalid';
                $message["previous.$key.designation.max"]='The designation may not be greater than 191 characters.';
                $message["previous.$key.reason_of_leaving.regex"]='The reason of leaving format is invalid';
                $message["previous.$key.reason_of_leaving.max"]='The reason of leaving may not be greater than 191 characters.';
                $message["previous.$key.joining_date.date"]='The joining date is not a valid date';
                $message["previous.$key.joining_date.date_format"]='The joining date format will be d/m/Y';
                $message["previous.$key.leaving_date.date"]='The leaving date is not a valid date';
                $message["previous.$key.leaving_date.date_format"]='The leaving date format will be d/m/Y';
                $message["previous.$key.leaving_date.after"]='The leaving date must be a date after '.$value['joining_date'];
            }
        }

        foreach($request->family as $key=>$value){
            $rules["family.$key.aadhar_number"]='nullable|regex:/^[a-z0-9 .\-]+$/i';
            $rules["family.$key.name"]='nullable|regex:/^[a-z0-9 .\-]+$/i';
            $rules["family.$key.date"]='nullable|date_format:d/m/Y';

            $message["family.$key.aadhar_number.regex"]='The aadhar number format is invalid';
            $message["family.$key.name.regex"]='The name format is invalid';
            $message["family.$key.date.date"]='The date is not a valid date';
            $message["family.$key.date.date_format"]='The date format will be d/m/Y';
            $message["family.$key.leaving_date.date"]='The leaving date is not a valid date';
            $message["family.$key.leaving_date.date_format"]='The leaving date format will be d/m/Y';
        }

        return $this->validate($request, $rules,array_merge([
            'designation_id.required' => 'The designation field is required.',
            'aadhar_number.required_if' => 'The aadhar number field is required when availability of aadhar is available.',
            'aadhar_enrolment_number.required_if' => 'The aadhar enrolment number field is required when availability of aadhar is not available.',
            'pan_number.required_if' => 'The pan number field is required when availability of PAN is available.',
        ],$message));
    }

    public function getSubDepartments(Request $request,$departmentId=0)
    {
        return SubDepartment::where("status","1")->where('department_id',$request->get('department_id',$departmentId))->pluck('name','id')->toArray();
    }

    public function getSourceDetail(Request $request,$name=0)
    {
        return SourceCategory::where("status","1")->where('id',$request->get('name',$name))->first();
    }

    public function getLocationDetail(Request $request,$locationCode=0)
    {
        return StoreLocation::where("status","1")->where('id',$request->get('locationCode',$locationCode))->first();
    }

    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function getStateFromRegion(Request $request,$region=0)
    {
        return State::where("status","1")->where('region_id',$request->get('region_id',$region))->pluck('name','id')->toArray();
    }

    public function changeStatus($table,$status,$id)
    {
        $status = ($status == 0)?1:0;

     /*   if($table == 'invoice'){
            $status = ($status == 0)?'in_progress':'confirm';
        }*/

        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    }

    public function approveView(Request $request,$id){
        view()->share('id',$id);
        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        $employee = Employee::find($id);
        $banks = \App\Banks::where('status','active')->get()->pluck('bank_name','bank_name')->toArray();
		return view('hrms.hiring.hrms_approve',compact('employee','banks'));
    }

    public function approve(Request $request,$id)
    {
        $this->validate($request,[
                'employee_code'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',
                                        function($attribute, $value, $fail) use($request,$id) {
                                                $employee = Employee::where($attribute,$value)->first();
                                                if($employee){
                                                    $fail('Employee code already exist');
                                                }
                                            }
                ],
                'appointment_letter'=>'nullable|mimes:pdf',
                'designation_id'=>'required',
                'ctc'=>'required|numeric|min:1',
                'esic_number'=>'nullable|regex:/^[a-z0-9 .\-]+$/i',
                'id_card_issue'=>'nullable|regex:/^[a-z0-9 .\-]+$/i',
                'bank_name'=>'required|regex:/^[a-z0-9 .\-]+$/i',
                'ifsc'=>'required',
                'account_no'=>'required',
                'branch_name'=>'required|regex:/^[a-z0-9 .\-]+$/i',
                'new_bank_name' => 'required_if:bank_name,==,other'
            ]);

        $employee = Employee::find($id);

        $requestData = $request->all();

        if(isset($requestData['appointment_letter']) && $requestData['appointment_letter']){
            $filename = $employee->full_name.'_'.uniqid(time()) . '.' . $requestData['appointment_letter']->getClientOriginalExtension();
            $requestData['appointment_letter']->move('uploads/employee/appointment_letter', $filename);

            $requestData['appointment_letter'] = 'uploads/employee/appointment_letter/'.$filename;
        }


        $employee->hrms_status = 2;
        $employee->hrms_approve_id = auth()->id();
        $employee->employee_code = $request->employee_code;
        $employee->designation_id = $request->designation_id;
        $employee->ctc = $request->ctc;
        $employee->esic_team_no = $request->esic_team_no;
        $employee->id_card_issue = $request->id_card_issue;
        if($request->bank_name == 'other'){
            $employee->bank_name = $request->new_bank_name;
        } else {
            $employee->bank_name = $request->bank_name;
        }

        $employee->ifsc = $request->ifsc;
        $employee->account_no = $request->account_no;
        $employee->branch_name = $request->branch_name;
        $employee->appointment_letter = $request->appointment_letter;
        $employee->status = 'confirm';
        $employee->save();
        $this->AddEmployeeInHiringReport($employee->store_id);

        $otherInfo = OtherInfo::where('employee_id',$employee->id)->first();
        $otherInfo->salary = $request->ctc;
        $otherInfo->save();
        // create log
        $log['title'] = "Approved by HRMS";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        return redirect('hrms/hiring')->with('flash_success', 'Employee Approved!');
    }

    public function sendBack(Request $request, $id)
    {
        /*$this->validate($request,[
                'note'=>'required'
            ]);*/

        $employee = Employee::find($id);
        $employee->hrms_note = $request->note;
        $employee->hrms_status = 3;
        $employee->status = 'approve';
        $employee->save();

        $log['title'] = "Send Back to Regional HR";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['note'] = $request->note;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        return 'Send Back to Regional HR';
    }

    public function downloadForm($id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','grade','designation','subDepartment','subDepartment.department','brand'])->find($id);

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        // \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        $view = view('storeManager.hiring.download_form', compact('employee'));

        $pdf = \PDF::loadHTML($view);

        return $pdf->download('employee_detail.pdf');

    }

    public function uploadDocuments($id,$upload=0)
    {
        view()->share('id',$id);
        view()->share('upload',$upload);

        $employee = Employee::find($id);

        $employee->attach = Attachment::where('employee_id',$id)->get();
        $attach = [];
        foreach ($employee->attach as $value) {
            if(in_array($value->key, ['education_documents','previous_company_docs'])){
                $attach[$value->key][$value->id]['file'] = $value->file;
                $attach[$value->key][$value->id]['key'] = $value->key;
            }else{
                $attach[$value->key]['file'] = $value->file;
                $attach[$value->key]['key'] = $value->key;
            }
        }
        $employee->attach = $attach;

        $businessUnits = BusinessUnit::where("status","1")->where("name","Unlimited")->first();
        view()->share('businessUnits',$businessUnits);

        $legalEntity = LegalEntity::where("status","1")->where("name","Arvind Lifestyle Brands Limited")->first();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->where("name","Store")->first();
        view()->share('employeeClassification',$employeeClassification);

        $variablePayType = VariablePayType::where("status","1")->where("name","Incentive")->first();
        view()->share('variablePayType',$variablePayType);

        return view('hrms.hiring.upload_documents', compact('employee'));
    }

    public function saveDocuments(Request $request,$id)
    {
        $rules = Helper::uploadDocumentValidationRule($request,$id,'joining_date');

        $uploadRule = Helper::updateDocumentValidationRule($request,'joining_date');

        if($request->upload == 0){
            $this->validate($request, $rules, Helper::validationMessage());
        }else{
            $this->validate($request, $uploadRule,Helper::validationMessage());
        }

        $requestData = $request->all();

        Helper::uploadEmployeeDocuments($requestData);

        return redirect('hrms/hiring')->with('flash_success', 'Documents Uploaded Successfully!');
    }
    public function AddEmployeeInHiringReport($store_id)
    {
        $hiringReport = HiringReport::where('store_id',$store_id)
                                    ->where('monthyear',date('m-Y'))
                                    ->first();
        $store = StoreLocation::find($store_id);
        if($store){
            $store->actual_man_power = $store->actual_man_power + 1;
            $store->save();
        }
        if($hiringReport){
            $hiringReport->hired= $hiringReport->hired + 1;
            $hiringReport->pending= $hiringReport->pending - 1;
            $hiringReport->save();
        }

    }
}
