<?php

namespace App\Http\Controllers\Hrms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use App\Helper;
use Carbon\Carbon as Carbon;

class DataChangeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        view()->share('route', 'data_change');
        view()->share('module', 'Data Change');
    }

    public function index(Request $request)
    {
        $employee = Employee::select([
            \DB::raw("CONCAT(full_name,' (',employee_code,')') as full_name"),
            'id'
        ])
        ->where('regional_status',"2")
        ->where('hrms_status',"2")
        ->pluck('full_name','id')->toArray();

        return view('hrms.data_change.create',compact('employee'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return redirect('hrms/employee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request,[
                'employee_id'=>'required',
            ],[
                'employee_id.required'=>'The employee id field is required',
            ]);


        $employee = Employee::where('id',$request->employee_id)->first();

        if($employee){
            return redirect('/hrms/data-change/'.$employee->id.'/edit');
        }else{
            return redirect('/hrms/data-change')->with('flash_error','Employee not found with this employee name or employee code');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','grade','designation','subDepartment','subDepartment.department','brand'])->find($id);

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        return view('hrms.data_change.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        if(!$employee){
            return redirect('hrms/data-change')->with('flash_error', 'Employee Not Found!');
        }

        $request = new Request;
        $currentCity = $permanentCity = [];
        view()->share('is_employee','1');

        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        $city = City::where("status","1")->pluck('name','id')->toArray();
        view()->share('city',$city);

        if(old('current_state',$employee->current_state)){
            $currentCity = $this->getCityFromState($request,['state_id'=>old('current_state',$employee->current_state)]);
        }
        view()->share('currentCity',$currentCity);

        if(old('permanent_state',$employee->permanent_state)){
            $permanentCity = $this->getCityFromState($request,['state_id'=>old('permanent_state',$employee->permanent_state)]);
        }
        view()->share('permanentCity',$permanentCity);

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        $locationCode = StoreLocation::where("status","1")->pluck('location_code','id')->toArray();
        view()->share('locationCode',$locationCode);

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);

        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $subDepartments = [];
        if(old('department_id',$employee->department_id)){
            $subDepartments = $this->getSubDepartments($request,['department_id'=>old('department_id',$employee->department_id)]);
        }
        view()->share('subDepartments',$subDepartments);

        $businessUnits = BusinessUnit::where("status","1")->pluck('name','id')->toArray();
        view()->share('businessUnits',$businessUnits);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->pluck('name','id')->toArray();
        view()->share('employeeClassification',$employeeClassification);

        $variablePayType = VariablePayType::where("status","1")->pluck('name','id')->toArray();
        view()->share('variablePayType',$variablePayType);

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        return view('hrms.data_change.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        if($requestData['pan_status'] == 'not_available'){
            $requestData['pan_number'] = '';
        }
        if($requestData['aadhar_status'] == 'not_available'){
            $requestData['aadhar_number'] = '';
        }
        if($requestData['aadhar_status'] == 'available'){
            $requestData['aadhar_enrolment_number'] = '';
        }
        $requestData['date_of_birth'] = Helper::ymd(str_replace('/', '-', $requestData['date_of_birth']));
        $requestData['anniversary_date'] = Helper::ymd(str_replace('/', '-', $requestData['anniversary_date']));
        $requestData['first_time_employment'] = isset($requestData['first_time_employment'])?1:0;

        $employee = Employee::where('id',$id)->update(array_except($requestData,['attach','_method','_token','button','education','previous','family']));

        if(isset($requestData['education']) && !empty($requestData['education'])){
            foreach ($requestData['education'] as $value) {
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    $value['deleted_at'] = null;
                    \DB::table('education_detail')->where('id',$value['id'])->update($value);
                }
            }
        }

        if($requestData['first_time_employment'] == 1 && isset($requestData['previous']) && !empty($requestData['previous'])){
            foreach ($requestData['previous'] as $value) {

                if($value['joining_date']){
                    $value['joining_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $value['joining_date']))->format('Y-m-d');
                }
                if($value['leaving_date']){
                    $value['leaving_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $value['leaving_date']))->format('Y-m-d');
                }
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    $value['deleted_at'] = null;
                    \DB::table('previous_experience')->where('id',$value['id'])->update($value);
                }else{
                    PreviousExperience::insert($value);
                }
            }
        }else{
            PreviousExperience::where('employee_id',$id)->delete();
        }

        if(isset($requestData['family']) && !empty($requestData['family'])){
            foreach ($requestData['family'] as $value) {
                $value['employee_id'] = $id;
                if($value['date'] == ''){
                    $value['date'] = '';
                    $value['month'] = '';
                    $value['year'] = '';
                }else{
                    $value['date'] = \Carbon\Carbon::parse(str_replace('/', '-',$value['date']))->format('Y-m-d');
                    $date = explode('-', $value['date']);
                    $value['date'] = $date[2];
                    $value['month'] = $date[1];
                    $value['year'] = $date[0];
                }
                if(isset($value['id'])){
                    $value['deleted_at'] = null;
                    \DB::table('family_information')->where('id',$value['id'])->update($value);
                }else{
                    FamilyInfo::insert($value);
                }
            }
        }

        // create log
        $log['title'] = "Data Change Employee update";
        $log['emp_id'] = $id;
        $log['emp_name'] = $request->full_name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        return redirect('hrms/data-change')->with('flash_success', 'Joinee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $employee = Employee::find($id);

        //delete certificates
        $oldDocs = Attachment::where('employee_id',$id)->get();

        if(count($oldDocs)){
            foreach ($oldDocs as $key => $value) {
                if(file_exists($value->file)){
                    unlink($value->file); //delete previously uploaded Attachment
                }
            }
        }

        Attachment::where('employee_id',$id)->delete();

        $employee->delete();

        if($request->has('from_index')){
            $message = "Employee Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee deleted!');

            return redirect('hrms/employee');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'designation_id' => 'required',
            'full_name' => 'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'position' => 'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'blood_group' => 'required',
            'rh_factor' => 'required',
            'date_of_birth' => 'required|date_format:d/m/Y',
            'anniversary_date' => 'nullable|date_format:d/m/Y',
            'marital_status' => 'required',
            'aadhar_status' => 'required',
            'nationality' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'gender' => 'required',
            'pan_status' => 'required',
            'mobile' => ['required','numeric',function($attribute, $value, $fail) use($request,$id) {
                            if(strlen($value) != 10){
                                $fail('The mobile no must be of 10 digits');
                            }
                        }],
            'email' => 'nullable|email',
            'emergency_person' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'emergency_number' => 'required|numeric',
            'emergency_relationship' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'first_name' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'last_name' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'current_address_line_1' => 'required|max:191',
            'current_address_line_2' => 'nullable|max:191',
            'current_address_line_3' => 'nullable|max:191',
            'current_state' => 'required',
            'current_city' => 'required',
            'current_pincode' => 'required|min:6|max:6',
            'permanent_address_line_1' => 'required|max:191',
            'permanent_address_line_2' => 'nullable|max:191',
            'permanent_address_line_3' => 'nullable|max:191',
            'permanent_state' => 'required',
            'permanent_city' => 'required',
            'permanent_pincode' => 'required|min:6|max:6',
            'aadhar_number' => 'nullable|required_if:aadhar_status,available|regex:/^[a-z0-9 .\-]+$/i',
            'aadhar_enrolment_number' => 'nullable|required_if:aadhar_status,not_available|regex:/^[a-z0-9 .\-]+$/i',
            'education'=>[
                        function($attribute, $value, $fail) use($request,$id) {
                            $status = true;
                            foreach ($value as $value) {
                                if($value['education_category_id'] != '' ||
                                    $value['board_uni'] != '' ||
                                    $value['institute'] != '' ||
                                    $value['qualification'] != '' ||
                                    $value['specialization'] != '' ||
                                    $value['month'] != '' ||
                                    $value['year'] != ''
                                        ){
                                    $status = false;
                                }
                            }
                            if($status){
                                $education = 0;
                                $fail('atleast one education detail is required');
                            }
                        },
                    ],
        ];

        if($request->pan_status == 'available'){
            $rules['pan_number'] = 'nullable|required_if:pan_status,available|size:10|regex:/^[a-z0-9 .\-]+$/i';
        }

        $message = [];
        foreach($request->education as $key=>$value){
            $rules["education.$key.board_uni"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.institute"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.qualification"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.specialization"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';

            $message["education.$key.board_uni.regex"]='The board / uni format is invalid';
            $message["education.$key.board_uni.max"]='The board / uni may not be greater than 191 characters.';
            $message["education.$key.institute.regex"]='The institute format is invalid';
            $message["education.$key.institute.max"]='The institute may not be greater than 191 characters.';
            $message["education.$key.qualification.regex"]='The qualification format is invalid';
            $message["education.$key.qualification.max"]='The qualification may not be greater than 191 characters.';
            $message["education.$key.specialization.regex"]='The specialization format is invalid';
            $message["education.$key.specialization.max"]='The qualification may not be greater than 191 characters.';
        }

        if(isset($request->first_time_employment)){
            foreach($request->previous as $key=>$value){
                $rules["previous.$key.company_name"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.designation"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.reason_of_leaving"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.joining_date"]='nullable|date_format:d/m/Y';
                $rules["previous.$key.leaving_date"]='nullable|date_format:d/m/Y|after:'.$value['joining_date'];

                $message["previous.$key.company_name.regex"]='The company name format is invalid';
                $message["previous.$key.company_name.max"]='The company name may not be greater than 191 characters.';
                $message["previous.$key.designation.regex"]='The designation format is invalid';
                $message["previous.$key.designation.max"]='The designation may not be greater than 191 characters.';
                $message["previous.$key.reason_of_leaving.regex"]='The reason of leaving format is invalid';
                $message["previous.$key.reason_of_leaving.max"]='The reason of leaving may not be greater than 191 characters.';
                $message["previous.$key.joining_date.date"]='The joining date is not a valid date';
                $message["previous.$key.joining_date.date_format"]='The joining date format will be d/m/Y';
                $message["previous.$key.leaving_date.date"]='The leaving date is not a valid date';
                $message["previous.$key.leaving_date.date_format"]='The leaving date format will be d/m/Y';
                $message["previous.$key.leaving_date.after"]='The leaving date must be a date after '.$value['joining_date'];
            }
        }

        foreach($request->family as $key=>$value){
            $rules["family.$key.aadhar_number"]='nullable|regex:/^[a-z0-9 .\-]+$/i';
            $rules["family.$key.name"]='nullable|regex:/^[a-z0-9 .\-]+$/i';
            $rules["family.$key.date"]='nullable|date_format:d/m/Y';

            $message["family.$key.aadhar_number.regex"]='The aadhar number format is invalid';
            $message["family.$key.name.regex"]='The name format is invalid';
            $message["family.$key.date.date"]='The date is not a valid date';
            $message["family.$key.date.date_format"]='The date format will be d/m/Y';
            $message["family.$key.leaving_date.date"]='The leaving date is not a valid date';
            $message["family.$key.leaving_date.date_format"]='The leaving date format will be d/m/Y';
        }

        return $this->validate($request, $rules,array_merge([
            'designation_id.required' => 'The designation field is required.',
            'aadhar_number.required_if' => 'The aadhar number field is required when availability of aadhar is available.',
            'aadhar_enrolment_number.required_if' => 'The aadhar enrolment number field is required when availability of aadhar is not available.',
            'pan_number.required_if' => 'The pan number field is required when availability of PAN is available.',
        ],$message));
    }

    public function getSubDepartments(Request $request,$departmentId=0)
    {
        return SubDepartment::where("status","1")->where('department_id',$request->get('department_id',$departmentId))->pluck('name','id')->toArray();
    }

    public function getSourceDetail(Request $request,$name=0)
    {
        return SourceCategory::where("status","1")->where('id',$request->get('name',$name))->first();
    }

    public function getLocationDetail(Request $request,$locationCode=0)
    {
        return StoreLocation::where("status","1")->where('id',$request->get('locationCode',$locationCode))->first();
    }

    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function getStateFromRegion(Request $request,$region=0)
    {
        return State::where("status","1")->where('region_id',$request->get('region_id',$region))->pluck('name','id')->toArray();
    }

    public function changeStatus($table,$status,$id)
    {
        $status = ($status == 0)?1:0;

     /*   if($table == 'invoice'){
            $status = ($status == 0)?'in_progress':'confirm';
        }*/

        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    }

    public function approve($id)
    {
        $employee = Employee::find($id);
        $employee->hrms_status = 2;
        $employee->hrms_approve_id = auth()->id();
        $employee->status = 'confirm';
        $employee->save();

        // create log
        $log['title'] = "Approved by HRMS";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        Session::flash('flash_success', 'Approved');

        return redirect('hrms/employee');
    }

    public function sendBack(Request $request, $id)
    {
        /*$this->validate($request,[
                'note'=>'required'
            ]);*/

        $employee = Employee::find($id);
        $employee->hrms_note = $request->note;
        $employee->hrms_status = 3;
        $employee->status = 'approve';
        $employee->save();

        $log['title'] = "Send Back to Regional HR";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['note'] = $request->note;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        return 'Send Back to Regional HR';
    }
}
