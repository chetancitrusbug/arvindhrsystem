<?php

namespace App\Http\Controllers\Regional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\Attendance;
use App\AttendanceRequest;
use Carbon\Carbon as Carbon;
use Carbon\CarbonPeriod;
use App\Helper;
use App\Region;
use App\OtherInfo;
use DateTime;
use mPDF;
use PDF;
use Excel;
use Auth;
use PHPExcel_Style_Fill;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.attendance');
        $this->middleware('permission:access.attendance.edit')->only(['edit','update']);
        $this->middleware('permission:access.attendance.create')->only(['create', 'store']);
        $this->middleware('permission:access.attendance.delete')->only('destroy');

        view()->share('route', 'attendance');
        view()->share('module', 'Attendance');
    }

    public function index(Request $request)
    {
        $user = auth()->user();

        $attendance = AttendanceRequest::select([
                    'attendance_request.*',
                    \DB::raw("CONCAT(full_name,' (',employee_code,')') as full_name"),
                    \DB::raw("CONCAT(day,'-',month,'-',year) as date"),
                    'employee.employee_code',
                    'store_location.store_name',
                ])

                ->join('employee',function($join) use($user){
                    $join->on('employee.id','=','attendance_request.employee_id');

                })
                ->leftjoin('attendance',function($join) use($user){
                    $join->on('attendance.id','=','attendance_request.attendance_id');
                })
                ->leftjoin('store_location',function($join) use($user){
                    $join->on('employee.store_id','=','store_location.id');
                })
                ->where('store_location.region_id',$user->region)
                ->where('attendance_request.status',"0")
                ->orderBy('attendance_request.id','DESC')
                ->paginate(10);

        return view('regional.attendance.index',compact('attendance'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('regional.attendance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $pre = Attendance::where('month',$input['month'])->where('year',$input['year'])->get();
        $data = [];
        if(count($pre)){
            foreach ($pre as $value) {
                $data[$value->employee_id][$value->day] = $value->day;
            }
        }

        if(isset($input['att']) && count($input['att'])){
            foreach ($input['att'] as $key => $value) {
                foreach ($value as $day=>$att) {

                    if($att && !in_array($day, isset($data[$key])?$data[$key]:[])){
                        $attValue = [];
                        $attValue['attendance'] = $att;
                        $attValue['employee_id'] = $key;
                        $attValue['day'] = $day;
                        $attValue['month'] = $input['month'];
                        $attValue['year'] = $input['year'];
                        // dump($attValue);
                        Attendance::create($attValue);
                    }
                }
            }
        }

        return redirect('store/attendance?month='.$input['month'].'&year='.$input['year'])->with('flash_success', 'successfully inserted');

        // $this->validateData($request);
        config()->set('ignoreEmpty', true);
        if($request->hasFile('excel')){

            $excel = $data = [];
            $path = $request->file('excel')->getRealPath();

            \Excel::load($path, function($reader) use (&$excel) {
                $reader->formatDates(true);
                $objExcel = $reader->getExcel();

                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $employee = Employee::where('employee_code','!=','')->pluck('id','employee_code')->toArray();

                //  Loop through each row of the worksheet in turn
                for ($row = 1; $row <= $highestRow; $row++)
                {
                    //  Read a row of data into an array
                    /*$sheet->setColumnFormat(array(
                            'B' => PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY,
                        ));*/
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                            NULL, TRUE, FALSE);

                    $data = $rowData[0];

                    if($row == 1){
                        $excel['location'] = $data[0];
                        $excel['date'] = date('Y-m-d' , ($data[6] - 25569) * 86400);
                        $excel['dateMonth'] = Carbon::parse($excel['date'])->format('m-Y');
                        $month = Carbon::parse($excel['date'])->format('m');
                        $year = Carbon::parse($excel['date'])->format('Y');
                        $count=0;
                        $monthList = ['1','3','5','7','8','10','12'];

                        if(in_array($month, $monthList)){
                            $excel['endrow'] = 36;
                        }else{
                            if($month == 2){
                                $excel['endrow'] = ((0 == $year % 4) and (0 != $year % 100) or (0 == $year % 400))?34:33;
                            }else{
                                $excel['endrow'] = 35;
                            }
                        }
                    }
                    if($row > 2 && array_key_exists((int)$data[1], $employee)){
                        // echo "<pre>"; print_r($data[1]); exit();
                        $excel[$row]['employee_id'] = $employee[trim($data[1])];
                        $excel[$row]['employee_code'] = trim($data[1]);
                        $excel[$row]['joining_date'] = date('Y-m-d' , ($data[3] - 25569) * 86400);
                        $excel[$row]['designation'] = caseChange($data[4]);
                        $excel[$row]['store'] = caseChange($data[5]);

                        for($day = 6; $day<=$excel['endrow']; $day++){
                            //data['date']
                            $date = Carbon::parse(++$count.'-'.$excel['dateMonth'])->format('Y-m-d');
                            $excel[$row]['attendance'][$date] = $data[$day];
                        }
                    }
                }
            });
            echo "<pre>"; print_r($excel); exit();
        }
        return redirect('store/attendance')->with('flash_success', 'successfully inserted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $attendance = Employee::find($id);

        // echo "<pre>"; print_r($attendance); exit();
        return view('regional.attendance.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // return view('regional.attendance.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // return redirect('store/hiring/createEducationDetail/'.$id)->with('flash_success', 'Personal information updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        Attachment::where('employee_id',$id)->delete();

        if($request->has('from_index')){
            $message = "Employee Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee deleted!');

            return redirect('store/hiring');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'excel' => 'required|mimes:xls,xlsx',
        ];

        return $this->validate($request, $rules);
    }

    public function changeRequest(Request $request)
    {
        $this->validate($request,[
                'reason'=>'required',
            ]);

        $input = request()->all();

        if(isset($input['id']) && !empty($input['id'])){
            $attendance = Attendance::where('employee_id',$input['id'])->where('day',$input['day'])->where('attendance',$input['attendance'])->first();
            if($attendance){
                $attendance->status = 1;
                $attendance->reason = $input['reason'];
                $attendance->save();
                return 'Attendance request sent successfully';
            }else{
                return 'error in send request';
            }
        }
        return 'Attedance not found';
    }

    public function approveReject()
    {
        $input = request()->all();
        $status = ($input['status'] == 'approve')?1:2;

        AttendanceRequest::where('id',$input['id'])->update(['status'=>$status]);
        $attandance = AttendanceRequest::where('id',$input['id'])->first();
            if(($attandance->attendance != 'p' &&  $attandance->attendance != 'wo') && ($input['attendance'] == 'p' ||  $input['attendance'] == 'wo') ){
            $other_infos = \App\OtherInfo::where('employee_id',$attandance->employee_id)->first();
            if($other_infos){
                $allocated_leave = $other_infos->allocated_leave;
                $other_infos->pending_leave = $other_infos->pending_leave + 1;
                $other_infos->availed_leave = $other_infos->availed_leave - 1;
                $other_infos->save();
            }
        } else if(($attandance->attendance == 'p' || $attandance->attendance == 'wo') && ($input['attendance'] != 'p' && $input['attendance'] != 'wo' )) {
            $other_infos = \App\OtherInfo::where('employee_id',$attandance->employee_id)->first();
            if($other_infos){
                $allocated_leave = $other_infos->allocated_leave;
                $other_infos->pending_leave = $other_infos->pending_leave - 1;
                $other_infos->availed_leave = $other_infos->availed_leave + 1;
                $other_infos->save();
            }
        }

        if($input['status'] == 'approve'){
            Attendance::where('id',$input['attendance_id'])->update(['attendance'=>$input['attendance'],'status'=>'0']);

        }else{
            Attendance::where('id',$input['attendance_id'])->update(['status'=>'0']);
        }



        if($input['status'] == 'approve'){
            return 'Approved';
        }
        return 'Rejected';
    }

    public function attendencecsvExport(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        $excel_export = array();
        $this->validate($request, [
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $start_date = str_replace('/', '-',$request->start_date);
        $end_date = str_replace('/', '-',$request->end_date);
        $region = Region::find(Auth::user()->region);

        $title = "ATTENDANCE FOR THE MONTH OF ". strtoupper(date('d F Y', strtotime($start_date))). " to ".strtoupper(date('d F Y', strtotime($end_date)));
        $title_array = array($title,'','','','','','','');
        $total_date = array("Arvind Lifestyle Brands Ltd",'','','','','','','');
        $total_days = array($region->name,'','','','','','','');
        $excel_hearder = array('SR NO','Employee Code','Employee Name','DOJ','DESIGNATION','Store-Office Name','IO CODE','STORE CODE-RETAK CODE');
        $start_date = new DateTime( $start_date );;
        $end_date = new DateTime( $end_date );;
        $end_date = $end_date->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start_date, $interval ,$end_date);
        foreach ($period as $key => $value ){
                array_push($excel_hearder,$value->format('l')." - ".$value->format('d/m/Y'));
                array_push($total_date,$value->format('j'));
                array_push($total_days,$value->format('l'));
                array_push($title_array,$value->format('d/m/Y'));
         }
         array_push($excel_hearder,"P","W/O","C/O","L","X","Days","Remarks");
         $excel_export[] = $total_date;
        $excel_export[] = $total_days;
        $excel_export[] = $title_array;

        $excel_export[] = $excel_hearder;
        $sr_no = 1;
        $attandance_report = array();
       $employees = Employee::whereNotIn('employee.status',['pending','cancel'])->selectRaw('employee.*,store_location.store_name,store_location.io_code,store_location.sap_code')
                        ->Leftjoin('store_location','store_location.id','employee.store_id')
                        ->where('store_location.region_id',Auth::user()->region)
                        ->where('store_location.status',1)
                        ->with(['designation:id,name','attendance' => function ($q) use ($start_date,$end_date)  {
                            $q->whereBetween('full_date',[$start_date,$end_date]);
                        }])
                        ->get();
        //dd($employees[0]->attendance->pluck('attendance','full_date'));
        // $attandanceemployees  = \App\Attendance::join('employee','employee.id','attendance.employee_id')
        //                             ->Leftjoin('store_location','store_location.id','employee.store_id')
        //                             ->where('store_location.region_id',Auth::user()->region)
        //                             ->where('store_location.status',1)
        //                             ->whereNotIn('employee.status',['pending','cancel'])
        //                             ->whereBetween('full_date',[$start_date,$end_date])
        //                             ->get();

        foreach($employees as $employee){
            $attandance_report = array();
            $attandance_report[$employee->id]['SR NO'] = $sr_no;
            $attandance_report[$employee->id]['Employee Code'] = $employee->employee_code;
            $attandance_report[$employee->id]['Employee Name'] = $employee->full_name;
            $attandance_report[$employee->id]['DOJ'] = $employee->joining_date;
            $attandance_report[$employee->id]['DESIGNATION'] = (isset($employee->designation) && $employee->designation->name ) ? $employee->designation->name : '';
            $attandance_report[$employee->id]['Store-Office Name'] = $employee->store_name;
            $attandance_report[$employee->id]['IO CODE'] = $employee->io_code;
            $attandance_report[$employee->id]['STORE CODE-RETAK CODE'] = $employee->sap_code;
            $x = 00; $wo = 00; $co = 00; $l = 00; $p =00; $remark = ''; $days = 00;
            $employeedates = $employee->attendance->pluck('attendance','full_date');
            $employeeAttendance = array_count_values($employeedates->toArray());
            $p = (isset($employeeAttendance['p'])) ? $employeeAttendance['p'] : 0;;
            $l =   (isset($employeeAttendance['l'])) ? $employeeAttendance['l'] : 0;
            $co =   (isset($employeeAttendance['co']))? $employeeAttendance['co'] : 0 ;
            $wo =   (isset($employeeAttendance['wo']))? $employeeAttendance['wo'] : 0 ;
            $days = $p + $wo;
            //dd($employeedates);
            foreach ($period as $key => $value ){
                
                // $attandance = [];
                // if($employee->attendance){
                //     $attandance = $employee->attendance->where('full_date',$value->format('Y-m-d'))->where('employee_id',$employee->id)->first();
                // }
                // if($attandance){
                //     ($attandance->attendance == 'p')?$p++:'';
                //     ($attandance->attendance == 'l')?$l++:'';
                //     ($attandance->attendance == 'co')?$co++:'';
                //     ($attandance->attendance == 'wo')?$wo++:'';
                //     ($attandance->attendance == 'wo' || $attandance->attendance == 'p')?$days++:'';
                //     $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = strtoupper($attandance->attendance);
                // } else {
                    $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = (isset($employeedates[$value->format('Y-m-d')])) ? $employeedates[$value->format('Y-m-d')] : '-';
                //}

            }
            $attandance_report[$employee->id]["P"] = $p;
            $attandance_report[$employee->id]["W/O"] = $wo;
            $attandance_report[$employee->id]["C/O"] = $co;
            $attandance_report[$employee->id]["L"] = $l;
            $attandance_report[$employee->id]["X"] = $x;
            $attandance_report[$employee->id]["Days"] = $days;
            $attandance_report[$employee->id]["Remarks"] = $remark;
            $sr_no++;
            $excel_export[] = $attandance_report[$employee->id];
        } 
        
        Excel::create($title,function($excel) use ($excel_export){
                $excel->setTitle('Attandace Reports');
                $excel->sheet('Attandace Reports',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                    $sheet->mergeCells('A1:H1');
                    $sheet->mergeCells('A2:H2');
                    $sheet->mergeCells('A3:H3');
                    $sheet->setAllBorders('thin');
                    $sheet->getStyle('A1')->applyFromArray(array(
                        'fill' => array(
                            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '91d3ff')
                        )
                    ));

                    $sheet->getStyle('A2')->applyFromArray(array(
                        'fill' => array(
                            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'e2cce2')
                        )
                    ));


                    $sheet->getStyle('A3')->applyFromArray(array(
                        'fill' => array(
                            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'dc80d8')
                        )
                    ));

                    $sheet->getStyle('A4:H4')->applyFromArray(array(
                        'fill' => array(
                            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '91d3ff')
                        )
                    ));
                });
            })->download('xlsx');
           return back();
    }

    public function attandanceList()
    {
        view()->share('route', 'attendance-list');
        view()->share('module', 'Attendance List');
        $attandance_data = null;
         return view('regional.report.attandance_list',compact('attandance_data'));
    }
    public function attandanceListget(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        $attandance_data = array();
        $this->validate($request, [
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $start_date = str_replace('/', '-', $request->start_date);
        $end_date = str_replace('/', '-', $request->end_date);
        $region = Region::find(Auth::user()->region);
       /* $period = new \DatePeriod(
            new DateTime($start_date),
            new \DateInterval('P1D'),
            new DateTime($end_date)
        ); */
        $title = "ATTENDANCE FOR THE MONTH OF ". strtoupper(date('d F Y', strtotime($start_date))). " to ".strtoupper(date('d F Y', strtotime($end_date)));
        $title_array = array($title);
        $total_date = array("Arvind Lifestyle Brands Ltd");
        $total_days = array($region->name);
        $excel_hearder = array('SR NO','Employee Code','Employee Name','DOJ','DESIGNATION','Store-Office Name','IO CODE','STORE CODE-RETAK CODE');
        $start_date = new DateTime( $start_date );;
        $end_date = new DateTime( $end_date );;
        $end_date = $end_date->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start_date, $interval ,$end_date);
        $dates = [];

         foreach ($period as $key => $value ){
                array_push($excel_hearder,$value->format('l')." - ".$value->format('d/m/Y'));
                array_push($total_date,$value->format('j'));
                array_push($total_days,$value->format('l'));
                array_push($title_array,$value->format('d/m/Y'));
                $dates[$value->format('Y-m-d')] = $value->format('Y-m-d');
         }
         array_push($excel_hearder,"P","W/O","C/O","L","X","Days","Remarks");
        $attandance_data[] = $total_date;
        $attandance_data[] = $total_days;
        $attandance_data[] = $title_array;

        $attandance_data[] = $excel_hearder;
        $sr_no = 1;

        $employees = Employee::whereNotIn('employee.status',['pending','cancel'])->selectRaw('employee.*,store_location.store_name,store_location.io_code,store_location.sap_code')
                        ->Leftjoin('store_location','store_location.id','employee.store_id')
                        ->where('store_location.region_id',Auth::user()->region)
                        ->where('store_location.status',1)
                        ->with(['designation:id,name','attendance' => function ($q) use ($start_date,$end_date)  {
                            $q->whereBetween('full_date',[$start_date,$end_date]);
                        }])
                        ->get();
        //dd($employees[0]->attendance->pluck('attendance','full_date'));
        // $attandanceemployees  = \App\Attendance::join('employee','employee.id','attendance.employee_id')
        //                             ->Leftjoin('store_location','store_location.id','employee.store_id')
        //                             ->where('store_location.region_id',Auth::user()->region)
        //                             ->where('store_location.status',1)
        //                             ->whereNotIn('employee.status',['pending','cancel'])
        //                             ->whereBetween('full_date',[$start_date,$end_date])
        //                             ->get();

        foreach($employees as $employee){
            $attandance_report = array();
            $attandance_report[$employee->id]['SR NO'] = $sr_no;
            $attandance_report[$employee->id]['Employee Code'] = $employee->employee_code;
            $attandance_report[$employee->id]['Employee Name'] = $employee->full_name;
            $attandance_report[$employee->id]['DOJ'] = $employee->joining_date;
            $attandance_report[$employee->id]['DESIGNATION'] = (isset($employee->designation) && $employee->designation->name ) ? $employee->designation->name : '';
            $attandance_report[$employee->id]['Store-Office Name'] = $employee->store_name;
            $attandance_report[$employee->id]['IO CODE'] = $employee->io_code;
            $attandance_report[$employee->id]['STORE CODE-RETAK CODE'] = $employee->sap_code;
            $x = 00; $wo = 00; $co = 00; $l = 00; $p =00; $remark = ''; $days = 00;
            $employeedates = $employee->attendance->pluck('attendance','full_date');
            $employeeAttendance = array_count_values($employeedates->toArray());
            $p = (isset($employeeAttendance['p'])) ? $employeeAttendance['p'] : 0;;
            $l =   (isset($employeeAttendance['l'])) ? $employeeAttendance['l'] : 0;
            $co =   (isset($employeeAttendance['co']))? $employeeAttendance['co'] : 0 ;
            $wo =   (isset($employeeAttendance['wo']))? $employeeAttendance['wo'] : 0 ;
            $days = $p + $wo;
            //dd($employeedates);
            foreach ($period as $key => $value ){
                
                // $attandance = [];
                // if($employee->attendance){
                //     $attandance = $employee->attendance->where('full_date',$value->format('Y-m-d'))->where('employee_id',$employee->id)->first();
                // }
                // if($attandance){
                //     ($attandance->attendance == 'p')?$p++:'';
                //     ($attandance->attendance == 'l')?$l++:'';
                //     ($attandance->attendance == 'co')?$co++:'';
                //     ($attandance->attendance == 'wo')?$wo++:'';
                //     ($attandance->attendance == 'wo' || $attandance->attendance == 'p')?$days++:'';
                //     $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = strtoupper($attandance->attendance);
                // } else {
                    $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = (isset($employeedates[$value->format('Y-m-d')])) ? $employeedates[$value->format('Y-m-d')] : '-';
                //}

            }
            $attandance_report[$employee->id]["P"] = $p;
            $attandance_report[$employee->id]["W/O"] = $wo;
            $attandance_report[$employee->id]["C/O"] = $co;
            $attandance_report[$employee->id]["L"] = $l;
            $attandance_report[$employee->id]["X"] = $x;
            $attandance_report[$employee->id]["Days"] = $days;
            $attandance_report[$employee->id]["Remarks"] = $remark;
            $sr_no++;
            $attandance_data[] = $attandance_report[$employee->id];
        }

        return view('regional.report.attandance_list',compact('attandance_data'));
    }

    public function storevise(Request $request)
    {
        $region_id = Auth::user()->region;
        $start_date = \Carbon\Carbon::now()->startOfDay()->subDays(1);
		$end_date = \Carbon\Carbon::now()->startOfDay()->subDays(1);
        $region = Region::find($region_id);
		if($request->has('start_date') && $request->start_date && $request->start_date != ""){
			$start_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->start_date);
		}
		if($request->has('end_date') && $request->end_date && $request->end_date != ""){
			$end_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->end_date);
		}

		$diff = 1 + $start_date->diffInDays($end_date);

	    $start_date = $start_date->format("Y-m-d");
        $end_date = $end_date->format("Y-m-d");

		$regiondata = \App\StoreLocation::where('store_location.status',1)->select('actual_man_power as total_actual_man_power','store_name as name',"id",'store_location.region_id');

		if($region_id &&  $region_id != ""){
			$regiondata->where('store_location.region_id',$region_id);
		}

		$regiondata = $regiondata->get();

		if($regiondata->count() >0){
            $regiondata = $regiondata->toArray();

			for($i=0; $i< count($regiondata); $i++){
				$man_power_p = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
                ->where('employee.store_id', '=',$regiondata[$i]['id'])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','p')
				->count();

				$man_power_l = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
                ->where('employee.store_id', '=',$regiondata[$i]['id'])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','l')
				->count();

				$man_power_wo = \App\Attendance::leftjoin('employee','employee.id','attendance.employee_id')
				->whereBetween('attendance.full_date',[$start_date,$end_date])
                ->where('employee.store_id', '=',$regiondata[$i]['id'])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','wo')
				->count();

				$man_power_co = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
                ->where('employee.store_id', '=',$regiondata[$i]['id'])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','co')
				->count();

				$regiondata[$i]['man_power_p'] = round($man_power_p/$diff);
				$regiondata[$i]['man_power_l'] = round($man_power_l/$diff);
				$regiondata[$i]['man_power_wo'] = round($man_power_wo/$diff);
				$regiondata[$i]['man_power_co'] = round($man_power_co/$diff);

				if($regiondata[$i]['total_actual_man_power'] && $regiondata[$i]['total_actual_man_power'] > 0){
					$regiondata[$i]['man_power_p_per'] =	round(($regiondata[$i]['man_power_p'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
					$regiondata[$i]['man_power_l_per'] =	round(($regiondata[$i]['man_power_l'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
					$regiondata[$i]['man_power_wo_per'] =	round(($regiondata[$i]['man_power_wo'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
					$regiondata[$i]['man_power_co_per'] =	round(($regiondata[$i]['man_power_co'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
				}else{
					$regiondata[$i]['man_power_p_per'] = 0;
					$regiondata[$i]['man_power_l_per'] = 0;
					$regiondata[$i]['man_power_wo_per'] =0;
					$regiondata[$i]['man_power_co_per'] =0;
				}
            }

		//	echo "<pre>"; print_r($regiondata); exit;
		}else{
			$regiondata = [];
        }

         /*return view('admin.report.attendance-region.store-all',compact('regiondata','region_id')); */
        return view('regional.report.attendancereportstore',compact('regiondata','region_id','region'));
    }

    public function EmployeeAttandance(Request $request,$store_id)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        if($request->has('filter')){
            $start_date  = $request->filter['from_date'];
            $end_date = $request->filter['to_date'];
            return redirect("store/attendance-report/employee?start_date=$start_date&end_date=$end_date");
        } else if(Input::get('start_date') && Input::get('end_date') ){
            $start_date  = Input::get('start_date');
            $end_date = Input::get('end_date');
        } else {
            $start_date  =  \Carbon\Carbon::now()->startOfDay()->subDays(1);
            $end_date =  \Carbon\Carbon::now()->startOfDay()->subDays(1);
        }


        $attandance_data = array();
        $filter['from_date']= $start_date;
        $filter['to_date']= $end_date;

        $title = "ATTENDANCE FOR THE MONTH OF ". strtoupper(date('d F Y', strtotime($start_date))). " to ".strtoupper(date('d F Y', strtotime($end_date)));
        $title_array = array($title);
        $total_date = array("Arvind Lifestyle Brands Ltd");
        $excel_hearder = array('SR NO','Employee Code','Employee Name','DOJ','DESIGNATION','Store-Office Name','IO CODE','STORE CODE-RETAK CODE');
        $store = \App\StoreLocation::find($store_id);
        $total_days = array($store->store_name);
        $start_date = new DateTime( $start_date );
        $end_date = new DateTime( $end_date );
        $end_date = $end_date->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start_date, $interval ,$end_date);
         foreach ($period as $key => $value ){
                array_push($excel_hearder,$value->format('l')." - ".$value->format('d/m/Y'));
                array_push($total_date,$value->format('j'));
                array_push($total_days,$value->format('l'));
                array_push($title_array,$value->format('d/m/Y'));
         }
         array_push($excel_hearder,"P","W/O","C/O","L","X","Days","Remarks");
         $attandance_data[] = $total_date;
        $attandance_data[] = $total_days;
        $attandance_data[] = $title_array;
        $attandance_data[] = $excel_hearder;
        $sr_no = 1;
        $attandance_report = array();
        $employees = Employee::whereNotIn('employee.status',['pending','cancel'])->selectRaw('employee.*,store_location.store_name,store_location.io_code,store_location.sap_code')
                        ->Leftjoin('store_location','store_location.id','employee.store_id')
                         ->where('employee.store_id',$store_id)
                        ->where('store_location.status',1)
                        ->with(['designation:id,name','attendance' => function ($q) use ($start_date,$end_date)  {
                            $q->whereBetween('full_date',[$start_date,$end_date]);
                        }])
                        ->get();
        //dd($employees[0]->attendance->pluck('attendance','full_date'));
        // $attandanceemployees  = \App\Attendance::join('employee','employee.id','attendance.employee_id')
        //                             ->Leftjoin('store_location','store_location.id','employee.store_id')
        //                             ->where('store_location.region_id',Auth::user()->region)
        //                             ->where('store_location.status',1)
        //                             ->whereNotIn('employee.status',['pending','cancel'])
        //                             ->whereBetween('full_date',[$start_date,$end_date])
        //                             ->get();

        foreach($employees as $employee){
            $attandance_report = array();
            $attandance_report[$employee->id]['SR NO'] = $sr_no;
            $attandance_report[$employee->id]['Employee Code'] = $employee->employee_code;
            $attandance_report[$employee->id]['Employee Name'] = $employee->full_name;
            $attandance_report[$employee->id]['DOJ'] = $employee->joining_date;
            $attandance_report[$employee->id]['DESIGNATION'] = (isset($employee->designation) && $employee->designation->name ) ? $employee->designation->name : '';
            $attandance_report[$employee->id]['Store-Office Name'] = $employee->store_name;
            $attandance_report[$employee->id]['IO CODE'] = $employee->io_code;
            $attandance_report[$employee->id]['STORE CODE-RETAK CODE'] = $employee->sap_code;
            $x = 00; $wo = 00; $co = 00; $l = 00; $p =00; $remark = ''; $days = 00;
            $employeedates = $employee->attendance->pluck('attendance','full_date');
            $employeeAttendance = array_count_values($employeedates->toArray());
            $p = (isset($employeeAttendance['p'])) ? $employeeAttendance['p'] : 0;;
            $l =   (isset($employeeAttendance['l'])) ? $employeeAttendance['l'] : 0;
            $co =   (isset($employeeAttendance['co']))? $employeeAttendance['co'] : 0 ;
            $wo =   (isset($employeeAttendance['wo']))? $employeeAttendance['wo'] : 0 ;
            $days = $p + $wo;
            //dd($employeedates);
            foreach ($period as $key => $value ){
                
                // $attandance = [];
                // if($employee->attendance){
                //     $attandance = $employee->attendance->where('full_date',$value->format('Y-m-d'))->where('employee_id',$employee->id)->first();
                // }
                // if($attandance){
                //     ($attandance->attendance == 'p')?$p++:'';
                //     ($attandance->attendance == 'l')?$l++:'';
                //     ($attandance->attendance == 'co')?$co++:'';
                //     ($attandance->attendance == 'wo')?$wo++:'';
                //     ($attandance->attendance == 'wo' || $attandance->attendance == 'p')?$days++:'';
                //     $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = strtoupper($attandance->attendance);
                // } else {
                    $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = (isset($employeedates[$value->format('Y-m-d')])) ? $employeedates[$value->format('Y-m-d')] : '-';
                //}

            }
            $attandance_report[$employee->id]["P"] = $p;
            $attandance_report[$employee->id]["W/O"] = $wo;
            $attandance_report[$employee->id]["C/O"] = $co;
            $attandance_report[$employee->id]["L"] = $l;
            $attandance_report[$employee->id]["X"] = $x;
            $attandance_report[$employee->id]["Days"] = $days;
            $attandance_report[$employee->id]["Remarks"] = $remark;
            $sr_no++;
            $attandance_data[] = $attandance_report[$employee->id];
        } 

        /*
        foreach ($attandance_report as $attandance) {
            $attandance_data[] = $attandance;
        } */
        return view('regional.report.attendanceemployee',compact('attandance_data','filter'));
    }
    public function attendencecsvExportstore(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        $excel_export = array();
        $this->validate($request, [
            'export_start_date' => 'required',
            'export_end_date' => 'required',
            'store_id' =>'required'
        ]);
        $store_id = $request->store_id;
        $start_date = str_replace('/', '-', $request->export_start_date);
        $end_date = str_replace('/', '-', $request->export_end_date);
        $region = Region::find(Auth::user()->region);

        $title = "ATTENDANCE FOR THE MONTH OF ". strtoupper(date('d F Y', strtotime($start_date))). " to ".strtoupper(date('d F Y', strtotime($end_date)));
        $title_array = array($title,'','','','','','','');
        $total_date = array("Arvind Lifestyle Brands Ltd",'','','','','','','');
        $total_days = array($region->name,'','','','','','','');
        $excel_hearder = array('SR NO','Employee Code','Employee Name','DOJ','DESIGNATION','Store-Office Name','IO CODE','STORE CODE-RETAK CODE');
        $start_date = new DateTime( $start_date );;
        $end_date = new DateTime( $end_date );;
        $end_date = $end_date->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start_date, $interval ,$end_date);
         foreach ($period as $key => $value ){
                array_push($excel_hearder,$value->format('l')." - ".$value->format('d/m/Y'));
                array_push($total_date,$value->format('j'));
                array_push($total_days,$value->format('l'));
                array_push($title_array,$value->format('d/m/Y'));
         }
         array_push($excel_hearder,"P","W/O","C/O","L","X","Days","Remarks");
        $excel_export[] = $total_date;
        $excel_export[] = $total_days;
        $excel_export[] = $title_array;

        $excel_export[] = $excel_hearder;
        $sr_no = 1;
        $attandance_report = array();
        $employees = Employee::whereNotIn('employee.status',['pending','cancel'])->selectRaw('employee.*,store_location.store_name,store_location.io_code,store_location.sap_code')
                        ->Leftjoin('store_location','store_location.id','employee.store_id')
                         ->where('employee.store_id',$store_id)
                        ->where('store_location.status',1)
                        ->with(['designation:id,name','attendance' => function ($q) use ($start_date,$end_date)  {
                            $q->whereBetween('full_date',[$start_date,$end_date]);
                        }])
                        ->get();
        //dd($employees[0]->attendance->pluck('attendance','full_date'));
        // $attandanceemployees  = \App\Attendance::join('employee','employee.id','attendance.employee_id')
        //                             ->Leftjoin('store_location','store_location.id','employee.store_id')
        //                             ->where('store_location.region_id',Auth::user()->region)
        //                             ->where('store_location.status',1)
        //                             ->whereNotIn('employee.status',['pending','cancel'])
        //                             ->whereBetween('full_date',[$start_date,$end_date])
        //                             ->get();

        foreach($employees as $employee){
            $attandance_report = array();
            $attandance_report[$employee->id]['SR NO'] = $sr_no;
            $attandance_report[$employee->id]['Employee Code'] = $employee->employee_code;
            $attandance_report[$employee->id]['Employee Name'] = $employee->full_name;
            $attandance_report[$employee->id]['DOJ'] = $employee->joining_date;
            $attandance_report[$employee->id]['DESIGNATION'] = (isset($employee->designation) && $employee->designation->name ) ? $employee->designation->name : '';
            $attandance_report[$employee->id]['Store-Office Name'] = $employee->store_name;
            $attandance_report[$employee->id]['IO CODE'] = $employee->io_code;
            $attandance_report[$employee->id]['STORE CODE-RETAK CODE'] = $employee->sap_code;
            $x = 00; $wo = 00; $co = 00; $l = 00; $p =00; $remark = ''; $days = 00;
            $employeedates = $employee->attendance->pluck('attendance','full_date');
            $employeeAttendance = array_count_values($employeedates->toArray());
            $p = (isset($employeeAttendance['p'])) ? $employeeAttendance['p'] : 0;;
            $l =   (isset($employeeAttendance['l'])) ? $employeeAttendance['l'] : 0;
            $co =   (isset($employeeAttendance['co']))? $employeeAttendance['co'] : 0 ;
            $wo =   (isset($employeeAttendance['wo']))? $employeeAttendance['wo'] : 0 ;
            $days = $p + $wo;
            //dd($employeedates);
            foreach ($period as $key => $value ){
                
                // $attandance = [];
                // if($employee->attendance){
                //     $attandance = $employee->attendance->where('full_date',$value->format('Y-m-d'))->where('employee_id',$employee->id)->first();
                // }
                // if($attandance){
                //     ($attandance->attendance == 'p')?$p++:'';
                //     ($attandance->attendance == 'l')?$l++:'';
                //     ($attandance->attendance == 'co')?$co++:'';
                //     ($attandance->attendance == 'wo')?$wo++:'';
                //     ($attandance->attendance == 'wo' || $attandance->attendance == 'p')?$days++:'';
                //     $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = strtoupper($attandance->attendance);
                // } else {
                    $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = (isset($employeedates[$value->format('Y-m-d')])) ? $employeedates[$value->format('Y-m-d')] : '-';
                //}

            }
            $attandance_report[$employee->id]["P"] = $p;
            $attandance_report[$employee->id]["W/O"] = $wo;
            $attandance_report[$employee->id]["C/O"] = $co;
            $attandance_report[$employee->id]["L"] = $l;
            $attandance_report[$employee->id]["X"] = $x;
            $attandance_report[$employee->id]["Days"] = $days;
            $attandance_report[$employee->id]["Remarks"] = $remark;
            $sr_no++;
            $excel_export[] = $attandance_report[$employee->id];
        } 
        Excel::create($title,function($excel) use ($excel_export){
                $excel->setTitle('Attandace Reports');
                $excel->sheet('Attandace Reports',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                    $sheet->mergeCells('A1:H1');
                    $sheet->mergeCells('A2:H2');
                    $sheet->mergeCells('A3:H3');
                    $sheet->setAllBorders('thin');
                    $sheet->getStyle('A1')->applyFromArray(array(
                        'fill' => array(
                            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '91d3ff')
                        )
                    ));

                    $sheet->getStyle('A2')->applyFromArray(array(
                        'fill' => array(
                            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'e2cce2')
                        )
                    ));


                    $sheet->getStyle('A3')->applyFromArray(array(
                        'fill' => array(
                            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'dc80d8')
                        )
                    ));

                    $sheet->getStyle('A4:H4')->applyFromArray(array(
                        'fill' => array(
                            'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '91d3ff')
                        )
                    ));
                });
            })->download('xlsx');
           return back();
    }

}
