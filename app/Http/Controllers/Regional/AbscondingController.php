<?php

namespace App\Http\Controllers\Regional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\User;
use App\Role;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use Carbon\Carbon as Carbon;
use App\Helper;
use mPDF;
use PDF;
use App\HiringReport;

class AbscondingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        view()->share('route', 'separation');
        view()->share('module', 'Separation');
    }

    public function index(Request $request)
    {
        $employee = Employee::pluck('full_name','id')->toArray();
        view()->share('employee',$employee);

        return view('regional.separation.absconding');
    }

    public function datatable(Request $request)
    {
		$region_id = Auth()->user()->region;
        $employee = Employee::select([
                    'employee.*','store_location.location_code','store_location.store_name','pdo_tbl.id as pdo_id'
                ])
                ->leftJoin('store_location','store_location.id','employee.store_id')
                ->leftJoin('pdo_tbl','pdo_tbl.employee_id','employee.id')

                // ->where('user_id',auth()->id())
                ->whereIn('employee.rm_absconding_status',["2","3","1","0"])
                ->whereIn('employee.absconding_status',["1","2"])
                ->where('store_location.region_id',$region_id)
                ->orderBy('employee.absconding_time','DESC')
                ->get();

        return Datatables::of($employee)
            ->make(true);
    }

    public function getEmployeeDetail(Request $request)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($request->id);

        if(!$employee){
            return ['status'=>'400','msg'=>'Employee detail not found','data'=>[]];
        }

        return ['status'=>'200','msg'=>'success','data'=>$employee];
    }

    public function approveAbsconding(Request $request,$id)
    {
        $employee = Employee::find($id);

        if(!$employee){
            return ['status'=>'400','msg'=>'Employee detail not found','data'=>[]];
        }
        //1=Notification come to RM, 2 = approve
        $employee->rm_absconding_status = 2;
        //$employee->processstatus = 'Inactive';
      //  $employee->absconding_time = \Carbon\Carbon::now();
        $employee->save();

        return ['status'=>'200','msg'=>'Absconding Approved','data'=>$employee];
    }

    public function rejectAbsconding(Request $request,$id)
    {
        $employee = Employee::find($id);

        if(!$employee){
            return ['status'=>'400','msg'=>'Employee detail not found','data'=>[]];
        }
        $store = StoreLocation::find($employee->store_id);
        if($store){
            $store->actual_man_power = $store->actual_man_power + 1;
            $store->save();
        }
        $employee->absconding_status = 0;
        $employee->rm_absconding_status = 0;
        $employee->processstatus = 'Active';
        $employee->absconding_time = null;
        $employee->save();

        return ['status'=>'200','msg'=>'Added to Absconding','data'=>$employee];
    }

	public function downloadLetter($id){
		$employee = Employee::where('employee.id',$id)->select('employee.*','legal_entity.id as legal_entity','legal_entity.name as legal_entity_name','city.name as city_name','state.name as state_name')->whereIn('rm_absconding_status',array(2,3))->leftJoin('legal_entity','legal_entity.id','employee.legal_entity_id')->leftJoin('city','city.id','employee.permanent_city')->leftJoin('state','state.id','employee.permanent_state')->get();
		if(count($employee) > 0){
            $employee = $employee[0];
            $employee = Employee::find($employee->id);
            $employee->rm_absconding_status = 3;
            $employee->status = 'inactive';
            $employee->processstatus = 'Inactive';
            $employee->save();
            $this->RemoveEmployeeInHiringReport($employee->store_id);

			$view = view('regional.separation.download_form', compact('employee'));
			//return view('regional.separation.download_form', compact('employee'));exit;

			$pdf = PDF::loadHTML($view);

			return $pdf->download('Termination For  '.$employee->full_name.'.pdf');
		}else{
			abort(404);
		}
    }

    public function RemoveEmployeeInHiringReport($store_id)
    {
        $hiringReport = HiringReport::where('store_id',$store_id)
                                    ->where('monthyear',date('m-Y'))
                                    ->first();
        $store = StoreLocation::find($store_id);
        if($store){
            //$store->actual_man_power = $store->actual_man_power - 1;
            //$store->save();
        }
        if($hiringReport){
            $hiringReport->absconding = $hiringReport->absconding + 1;
    //        $hiringReport->gap= $hiringReport->gap + 1;
            $hiringReport->pending= $hiringReport->pending + 1;
            $hiringReport->save();
        }
    }

}
