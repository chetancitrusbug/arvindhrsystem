<?php

namespace App\Http\Controllers\Regional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use App\Helper;
use App\SourceDetail;
use Carbon\Carbon as Carbon;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.regional.employee');
        $this->middleware('permission:access.regional.employee.edit')->only(['edit','update']);
        $this->middleware('permission:access.regional.employee.create')->only(['create', 'store']);
        $this->middleware('permission:access.regional.employee.delete')->only('destroy');

        view()->share('route', 'employee');
        view()->share('module', 'Employee');
    }

    public function index(Request $request)
    {
        return view('regional.employee.index');
    }
     public function employee_list(Request $request)
    {
        return view('regional.employee.index');
    }
    public function storelist(Request $request)
    {
        return view('regional.employee.storelist');
    }

    public function datatable(Request $request,$store_id)
    {
        $employee = Employee::select([
                    'employee.*',
                    'designation.name as designation',
                    'store_location.store_name',
                    'store_location.location_code',
                ])
                ->leftJoin('designation',function($join){
                    $join->on('employee.designation_id','=','designation.id');
                })
                ->leftJoin('store_location',function($join){
                    $join->on('employee.store_id','=','store_location.id');
                })
                // ->whereIn('store_status',[1])
                ->where('employee.hrms_status',"2")
                //->where('employee.rm_absconding_status',"!=","3")
                ->where('employee.store_id',$store_id)
                // ->where('employee.joining_date','!=',"")
                 //->where('employee.region_id',auth()->user()->region)
                ->orderBy('employee.id','DESC')
                ->get();

        return Datatables::of($employee)
            ->make(true);
    }

	public function allEmployeeDatatable(Request $request)
    {
        $employee = Employee::select([
                    'employee.*',
                    'designation.name as designation',
                    'store_location.store_name',
                    'store_location.location_code',
                ])
                ->leftJoin('designation',function($join){
                    $join->on('employee.designation_id','=','designation.id');
                })
                ->leftJoin('store_location',function($join){
                    $join->on('employee.store_id','=','store_location.id');
                })
                // ->whereIn('store_status',[1])
                ->where('employee.hrms_status',"2")
                ->where('employee.rm_absconding_status',"!=","3")
                //->where('employee.store_id',$store_id)
                // ->where('employee.joining_date','!=',"")
                ->where('store_location.region_id',auth()->user()->region)
                ->orderBy('employee.id','DESC')
                ->get();

        return Datatables::of($employee)
            ->make(true);
    }


    public function storedatatable(Request $request)
    {

        $store = StoreLocation::select([
                    'store_location.*',
                    'store_location.store_name',
                    'store_location.address',
                ])
           /*     ->leftJoin('designation',function($join){
                    $join->on('employee.designation_id','=','designation.id');
                })
                ->leftJoin('store_location',function($join){
                    $join->on('employee.store_id','=','store_location.id');
                }) */
                // ->whereIn('store_status',[1])

                ->where('store_location.region_id',auth()->user()->region)
                ->where('store_location.status',"1")
                // ->where('employee.joining_date','!=',"")
             //    ->where('employee.region_id',auth()->user()->region)
                ->orderBy('store_location.id','DESC')
                ->get();

        return Datatables::of($store)
            ->make(true);
    }
    public function logView(Request $request,$id)
    {
        view()->share('id',$id);
        return view('regional.employee.log');
    }

    public function log(Request $request,$id)
    {
        $logs = Log::select([
                    'logs.*',
                    'users.emp_name as user_name',
                    'roles.label as user_role',
                ])
                ->leftJoin('users',function($join){
                    $join->on('users.id','=','logs.user_id');
                })
                ->leftJoin('roles',function($join){
                    $join->on('roles.id','=','logs.user_role_id');
                })
                // ->where('user_id',auth()->id())
                ->where('logs.emp_id',$id)
                ->orderBy('logs.id','DESC')
                ->latest()->get();

        return Datatables::of($logs)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return redirect('regional/employee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //not allowed
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($id);

        if(!$employee){
            return redirect('regional/employee')->with('flash_error', 'Employee Not Found!');
        }

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        $employee->otherinfo = \App\OtherInfo::where('employee_id',$id)->first();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        return view('regional.employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        if(!$employee){
            return redirect('regional/employee')->with('flash_error', 'Employee Not Found!');
        }

        $request = new Request;
        $currentCity = $permanentCity = [];
        view()->share('is_employee','1');

        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        $city = City::where("status","1")->pluck('name','id')->toArray();
        view()->share('city',$city);

        if(old('current_state',$employee->current_state)){
            $currentCity = $this->getCityFromState($request,['state_id'=>old('current_state',$employee->current_state)]);
        }
        view()->share('currentCity',$currentCity);

        if(old('permanent_state',$employee->permanent_state)){
            $permanentCity = $this->getCityFromState($request,['state_id'=>old('permanent_state',$employee->permanent_state)]);
        }
        view()->share('permanentCity',$permanentCity);

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);

        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $subDepartments = [];
        if(old('department_id',$employee->department_id)){
            $subDepartments = $this->getSubDepartments($request,['department_id'=>old('department_id',$employee->department_id)]);
        }
        view()->share('subDepartments',$subDepartments);

        $businessUnits = BusinessUnit::where("status","1")->pluck('name','id')->toArray();
        view()->share('businessUnits',$businessUnits);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

		$StoreLocation = StoreLocation::where("status","1")->where('region_id',auth()->user()->region)->pluck('store_name','id')->toArray();
        view()->share('StoreLocation',$StoreLocation);


        $employeeClassification = EmployeeClassification::where("status","1")->pluck('name','id')->toArray();
        view()->share('employeeClassification',$employeeClassification);

        $variablePayType = VariablePayType::where("status","1")->pluck('name','id')->toArray();
        view()->share('variablePayType',$variablePayType);


        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        return view('regional.employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->only(['blood_group','rh_factor','marital_status','nationality','mobile','email','esic_number','emergency_person','emergency_number','emergency_relationship','anniversary_date','ua_number','education','family','joining_date','business_unit_id','legal_entity_id','employee_classification_id','variable_pay_type_id','grade_id','designation_id','department_id','sub_department_id','reporting_manager_name','reporting_manager_employee_code','store_id']);

        $requestData['anniversary_date'] = Helper::ymd(str_replace('/', '-', $requestData['anniversary_date']));

        $employee = Employee::where('id',$id)->update(array_except($requestData,['attach','_method','_token','button','education','previous','family']));

        if(isset($requestData['education']) && !empty($requestData['education'])){
            foreach ($requestData['education'] as $value) {
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    \DB::table('education_detail')->where('id',$value['id'])->update(array_except($value,['education_category_id']));
                }else{
                    EducationDetail::insert($value);
                }
            }
        }
        if(isset($requestData['family']) && !empty($requestData['family'])){
            foreach ($requestData['family'] as $value) {
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    \DB::table('family_information')->where('id',$value['id'])->update(['name'=>$value['name']]);
                }
            }
        }

        $employee = Employee::find($id);
        // create log
        $log['title'] = "Employee Update";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['note'] = $request->regional_note;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);
        $redirect = 'regional/employeelist/'.$employee->store_id;
        return redirect($redirect)->with('flash_success', 'Employee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        Employee::find($id)->delete();
        EducationDetail::where('employee_id',$id)->delete();
        PreviousExperience::where('employee_id',$id)->delete();
        FamilyInfo::where('employee_id',$id)->delete();

        if($request->has('from_index')){
            $message = "Employee Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee deleted!');

            return redirect('regional/employee');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'blood_group' => 'required',
            'rh_factor' => 'required',
            'anniversary_date' => 'nullable|date_format:d/m/Y',
            'marital_status' => 'required',
            'nationality' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'mobile' => ['required','numeric',function($attribute, $value, $fail) use($request,$id) {
                            if(strlen($value) != 10){
                                $fail('The mobile no must be of 10 digits');
                            }
                        }],
            'email' => 'nullable|email',
            'emergency_person' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'emergency_number' => 'required|numeric',
            'emergency_relationship' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'joining_date'=>['date','date_format:d/m/Y','nullable',
                            function($attribute, $value, $fail) use($request,$id) {
                                if($value != ''){
                                    if(Helper::ymd(str_replace('/', '-', $value)) <= Helper::ymd(Carbon::now())){
                                        return $fail('The joining date must be a date after '.Helper::dmy(Carbon::now()));
                                    }
                                }
                            },
                        ],
            'education'=>[
                        function($attribute, $value, $fail) use($request,$id) {
                            $status = true;
                            foreach ($value as $value) {
                                if($value['education_category_id'] != '' ||
                                    $value['board_uni'] != '' ||
                                    $value['institute'] != '' ||
                                    $value['qualification'] != '' ||
                                    $value['specialization'] != '' ||
                                    $value['month'] != '' ||
                                    $value['year'] != ''
                                        ){
                                    $status = false;
                                }
                            }
                            if($status){
                                $education = 0;
                                $fail('atleast one education detail is required');
                            }
                        },
                    ],
        ];

        $message = [];
        foreach($request->education as $key=>$value){
            $rules["education.$key.board_uni"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.institute"]='nullable|max:191';
            $rules["education.$key.qualification"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.specialization"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';

            $message["education.$key.board_uni.regex"]='The board / uni format is invalid';
            $message["education.$key.board_uni.max"]='The board / uni may not be greater than 191 characters.';
            $message["education.$key.institute.regex"]='The institute format is invalid';
            $message["education.$key.institute.max"]='The institute may not be greater than 191 characters.';
            $message["education.$key.qualification.regex"]='The qualification format is invalid';
            $message["education.$key.qualification.max"]='The qualification may not be greater than 191 characters.';
            $message["education.$key.specialization.regex"]='The specialization format is invalid';
            $message["education.$key.specialization.max"]='The qualification may not be greater than 191 characters.';
        }

        foreach($request->family as $key=>$value){
            $rules["family.$key.name"]='nullable|regex:/^[a-z0-9 .\-]+$/i';

            $message["family.$key.name.regex"]='The name format is invalid';
        }

        return $this->validate($request, $rules,array_merge([
            'aadhar_number.required_if' => 'The aadhar number field is required when availability of aadhar is available.',
            'aadhar_enrolment_number.required_if' => 'The aadhar enrolment number field is required when availability of aadhar is not available.',
            'pan_number.required_if' => 'The pan number field is required when availability of PAN is available.',
        ],$message));
    }

    public function getSubDepartments(Request $request,$departmentId=0)
    {
        return SubDepartment::where("status","1")->where('department_id',$request->get('department_id',$departmentId))->pluck('name','id')->toArray();
    }

    public function getSourceDetail(Request $request,$category=0)
    {
        return SourceDetail::where("status","1")->where('source_category_id',$request->get('category',$category))->pluck('source_name','id')->toArray();
    }

    public function getSourceCode(Request $request,$name=0)
    {
        return SourceDetail::where("status","1")->where('id',$request->get('name',$name))->first();
    }

    public function getLocationDetail(Request $request,$locationCode=0)
    {
        return StoreLocation::where("status","1")->where('id',$request->get('locationCode',$locationCode))->first();
    }

    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function getStateFromRegion(Request $request,$region=0)
    {
        return State::where("status","1")->where('region_id',$request->get('region_id',$region))->pluck('name','id')->toArray();
    }

    public function changeStatus($table,$status,$id)
    {
        $status = ($status == 0)?1:0;

        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    }

    public function sendBack(Request $request,$id)
    {
        /*$this->validate($request,[
                'note'=>'required'
            ]);*/

        $employee = Employee::find($id);
        $employee->regional_note = $request->note;
        $employee->regional_status = 3;
        $employee->store_status = 3;
        $employee->status = 'pending';

        // create log
        $log['title'] = "Send Back to Store Manager";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['note'] = $request->note;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        $employee->save();
        return 'Send Back to Store Manager';
    }

    public function getEmployeeDetail(Request $request)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($request->id);

        if(!$employee){
            return ['status'=>'400','msg'=>'Employee detail not found','data'=>[]];
        }

        return ['status'=>'200','msg'=>'success','data'=>$employee];
    }
}
