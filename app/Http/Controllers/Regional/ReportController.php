<?php

namespace App\Http\Controllers\Regional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use DB;
use App\City;
use App\State;
use App\Employee;
use App\StoreLocation;
use Carbon\Carbon as Carbon;
use Yajra\Datatables\Datatables;
use App\StoreCollection;

class ReportController extends Controller
{
    //
    public function ColletionReportstore(Request $request)
    {
        view()->share('module', 'Reports');
        view()->share('route', 'sales-report');
        return view('regional.report.productivityreport');
    }
    public function ColletionReportstoredatatable(Request $request,$region_id)
    {
        $current_year = date('Y');
        $current_month = date('m');
        $current_month--;
        if($current_month < 1){
            $current_month = 12;
            $current_year--;
        }
        if(Input::get('month') && Input::get('year') ){
            $current_month  = Input::get('month');
            $current_year = Input::get('year');
        }
        $collection_report = array();
        $stores = StoreLocation::where('store_location.status',1)
                                            ->where('store_location.region_id',$region_id)
                                            ->get();

        $collection_reports = StoreCollection::selectRaw('store_collections.*,SUM(store_collections.amount) as total_amount,store_location.store_name, store_location.id as store_id')
                                            ->join('store_location','store_location.id','store_collections.store_id')
                                            ->join('region','region.id','store_location.region_id')
                                            ->where('store_collections.month',$current_month)
                                            ->where('store_collections.year',$current_year)
                                            ->where('store_location.status',1)
                                            ->where('store_location.region_id',$region_id)
                                            ->groupby('store_collections.store_id')
                                            ->get();
        $total_employees = Employee::selectRaw('employee.store_id,count(employee.id) as total_employee,SUM(employee.ctc) as total_ctc')
                                        ->join('store_location','store_location.id','employee.store_id')
                                        ->join('region','region.id','store_location.region_id')
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->groupby('employee.store_id')
                                        ->get();
        foreach ($stores as $store) {
            $total_employee = $total_employees->where('store_id',$store->id)->first();
            $collection = $collection_reports->where('store_id',$store->id)->first();

            $collection_report[] = array(
                'store_id'=>$store->id,
                'io_code'=>$store->io_code,
                'store_name'=>$store->store_name,
                'total_amount'=>($collection)?$collection->total_amount:0,
                'total_employee'=>($total_employee)?$total_employee->total_employee:0,
                'avg_ctc' =>($total_employee)?($total_employee->total_ctc > 0  && $total_employee->total_employee >0)? round($total_employee->total_ctc/$total_employee->total_employee,2):'-':'-',
                'per_person_income' => ($collection && $total_employee)?($collection->total_amount !=0  && $total_employee->total_employee >0)? round($collection->total_amount/$total_employee->total_employee,2):'-':'-'
            );
        }
        return Datatables::of($collection_report)
            ->make(true);
    }

    public function LeaveReportstore(Request $request)
    {
        view()->share('module', 'Reports');
        view()->share('route', 'leave-report');
        return view('regional.report.leavereportstore');
    }
    public function LeaveReportstoredatatable(Request $request,$region_id)
    {
        $leaveReport = StoreLocation::selectRaw('store_location.`io_code`,store_location.io_code,store_location.store_name , store_location.id AS store_id,SUM(other_infos.allocated_leave) AS total_allocated_leave, SUM(other_infos.pending_leave) AS total_pending_leave, SUM(other_infos.availed_leave) AS availed_leave')
            ->leftjoin('employee',function($jointEmployee){
                $jointEmployee->on('store_location.id','=','employee.store_id')
                ->where('employee.processstatus','!=','Inactive');
            })
            ->leftjoin('other_infos','other_infos.employee_id','employee.id')
            ->leftjoin('region','region.id','store_location.region_id')
            ->where('store_location.status',1)
            ->where('store_location.region_id',$region_id)
            ->whereNULL('store_location.deleted_at')
            ->groupBy('store_location.id')
            ->orderby('store_location.id')
            ->get();
        return Datatables::of($leaveReport)
            ->make(true);
    }
    public function LeaveReportemployee(Request $request,$store_id)
    {
        view()->share('module', 'Reports');
        view()->share('route', 'leave-report');
        return view('regional.report.leavereportemployee');
    }
    public function LeaveReportemployeedatatable(Request $request,$store_id)
    {
        $leaveReport = Employee::selectRaw('employee.employee_code,employee.full_name as employee_name,other_infos.allocated_leave, other_infos.pending_leave,other_infos.availed_leave')
                                        ->leftjoin('other_infos','other_infos.employee_id','employee.id')
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->where('employee.store_id',$store_id)
                                        ->get();
        return Datatables::of($leaveReport)
            ->make(true);
    }

}
