<?php

namespace App\Http\Controllers\Regional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Auth;
use DB;
use App\StoreLocation;
use App\Employee;
use App\Region;
use App\OtherInfo;
use App\HiringReport;
use App\ResignationReason;
use App\Resignation;
use Yajra\Datatables\Datatables;
use App\StoreCollection;
use DateTime;
use Excel;

class RegionalController extends Controller
{
    function __construct()
    {
        $array['sm'] = 'Store Manager';
        $array['dm'] = 'Department Manager';
        $array['tdm'] = 'TDM';
        $array['vm'] = 'Vendore Manager';
        $array['logistics'] = 'Logistics';
        $array['maintenance'] = 'Maintenance';
        $array['cashier'] = 'Cashier';
        $array['head_cashier'] = 'Head Cashier';
        $array['csa'] = 'Customer Service Assosiate';
        $this->arrayForLabelDesignation = $array;
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
		$regionId = Auth::user()->region;
		$region = Region::find($regionId);
		$manPowerGet = StoreLocation::where('store_location.status',1)->where('store_location.region_id',$regionId)->select(DB::raw('SUM(store_location.budget_man_power) as total_budget_man_power'),DB::raw('SUM(store_location.actual_man_power) as total_actual_man_power'),'region.name','region_id')->leftJoin('region','region.id','store_location.region_id')->groupBy('store_location.region_id')->get();

		$averageCost = Employee::where('employee.processstatus','!=','Inactive')->where('store_location.status',1)->leftJoin('store_location','store_location.id','employee.store_id')->leftJoin('region','region.id','store_location.region_id')->select(DB::raw('sum(ctc) as salary'),DB::raw('count(employee.id) as total_employee'),DB::raw('( CASE WHEN SUM(ctc) / COUNT(employee.id) > 0 THEN SUM(ctc)/COUNT(employee.id) ELSE 0 END) AS avg_cost'),'region.name','region.id as region_id')->where('region.id',$regionId)->groupBy('store_location.region_id')->get();
		if(count($averageCost) > 0){
			$averageCost = $averageCost[0];
		}else{
			$averageCost['total_employee'] = 0;
			$averageCost['avg_cost'] = 0;
		}

        $hygiene['id_card'] = OtherInfo::select(DB::raw('count(*) as id_count'))->join('employee','employee.id','other_infos.employee_id')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('employee.processstatus','!=','Inactive')->whereNULL('employee.deleted_at')->where('other_infos.id_card_status','NO')->where('store_location.region_id',$regionId)->get();
		if(count($hygiene['id_card']) > 0)
			$hygiene['id_card'] = $hygiene['id_card'][0]->id_count;

        $hygiene['uniform'] = OtherInfo::select(DB::raw('count(*) as uniform_count'))->join('employee','employee.id','other_infos.employee_id')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('employee.processstatus','!=','Inactive')->whereNULL('employee.deleted_at')->where('other_infos.uniform_status','NO')->where('store_location.region_id',$regionId)->get();
		if(count($hygiene['uniform']) > 0)
		$hygiene['uniform'] = $hygiene['uniform'][0]->uniform_count;

        $hygiene['l0l1_untrained'] = OtherInfo::select(DB::raw('count(*) as l0l1_untrained_count'))->join('employee','employee.id','other_infos.employee_id')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('employee.processstatus','!=','Inactive')->whereNULL('employee.deleted_at')->where('other_infos.l0l1_status','NO')->where('store_location.region_id',$regionId)->get();

		if(count($hygiene['l0l1_untrained']) > 0)
		$hygiene['l0l1_untrained'] = $hygiene['l0l1_untrained'][0]->l0l1_untrained_count;

        $hygiene['l0l1_certified'] = OtherInfo::select(DB::raw('count(*) as l0l1_certificate_count'))->join('employee','employee.id','other_infos.employee_id')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('employee.processstatus','!=','Inactive')->whereNULL('employee.deleted_at')->where('other_infos.l0l1_certificate_status','NO')->where('store_location.region_id',$regionId)->get();

		if(count($hygiene['l0l1_certified']) > 0)
		$hygiene['l0l1_certified'] = $hygiene['l0l1_certified'][0]->l0l1_certificate_count;

        $hygiene['certificate_pending'] = OtherInfo::select(DB::raw('count(*) as l0l1_untrained_count'))->join('employee','employee.id','other_infos.employee_id')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('other_infos.l0l1_certificate_status','NO')->where('employee.processstatus','!=','Inactive')->where('store_location.region_id',$regionId)->whereNULL('employee.deleted_at')->get();

		if(count($hygiene['certificate_pending']) > 0)
		$hygiene['certificate_pending'] = $hygiene['certificate_pending'][0]->l0l1_untrained_count;

        $hygiene['appoint_letter'] = StoreLocation::leftJoin('employee','store_location.id','employee.store_id')
            ->leftJoin('other_infos', function ($join) {
                $join->on('other_infos.employee_id', '=', 'employee.id')
                    ->where('other_infos.appointment_status', '=', 'NO');
            })
            ->select(DB::raw('count(other_infos.id) as appointment_status_count'),'region.name','employee.store_id','store_location.store_name','store_location.io_code as store_code')
            ->join('region','store_location.region_id','region.id')
            ->where('store_location.region_id',$regionId)
            ->whereNULL('region.deleted_at')
            ->whereNULL('employee.deleted_at')
            ->whereNULL('store_location.deleted_at')
            ->where('store_location.status',1)
            ->where('region.status',1)
            ->where('employee.processstatus','!=','Inactive')
            ->get();

		if(count($hygiene['appoint_letter']) > 0)
        $hygiene['appoint_letter'] = $hygiene['appoint_letter'][0]->appointment_status_count;

        $current_year = date('Y');
        $inmonths = array();

        for($i = 1; $i<=12;$i++){
            $next_year = $current_year+1;
            ($i < 4)?$inmonths[]= sprintf("%02d", $i)."-$next_year"  : $inmonths[]=  sprintf("%02d", $i)."-$current_year";
        }

        $hiringReport = array();
        $inmonths = array(date('m-Y'));

        $hiringReport = HiringReport::selectRaw('sum(hiring_reports.gap) as gap_total,sum(hiring_reports.pending) as pending_total,sum(hiring_reports.hired) as hired_total')
                        ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                        ->leftJoin('region','region.id','store_location.region_id')
                        ->whereIn('hiring_reports.monthyear',$inmonths)
                        ->where('hiring_reports.region_id',$regionId)
                        ->orderBy('store_location.id')
                        ->first();

        $totalattritionReport = HiringReport::selectRaw('*,sum(hiring_reports.resignation + hiring_reports.absconding) as total,sum(hiring_reports.resignation) as resignation_total,sum(hiring_reports.absconding) as absconding_total')
                        ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                        ->whereIn('hiring_reports.monthyear',[date('m-Y')])
                        ->where('hiring_reports.region_id',$regionId)
                        ->get();
        $aging_report = $this->agingregion($regionId);
        $reasonreportdata  = $this->attritionReportReasonRegiontotal($regionId);


        $manpowerPercentageReport = HiringReport::selectRaw('(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,SUM(store_location.actual_man_power) as total_actual_man_power')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->where('region.id',$regionId)
                                ->whereIn('monthyear',[date('m-Y')])
                                ->first();
        $hiringinprocessreport = $this->hiringinprocessreport($regionId);

        $total_pending_employee = Employee::selectRaw('count(employee.id) as total_pending')
                                            ->leftJoin('store_location','store_location.id','employee.store_id')
                                            ->leftJoin('region','region.id','store_location.region_id')
                                            ->where('region.status',1)
                                            ->where('store_location.status',1)
                                            ->where('region.id',$regionId)
                                            ->whereNotIn('employee.status',['confirm','reject','inactive'])
                                            ->first();
        $genderwisetotalemployee = $this->genderwisetotalemployee($regionId);
        $hiring_employee = $this->genderwisehiring($regionId);
        $genderwiseattrition = $this->genderwiseattrition($regionId);
        $productivity_report = $this->productivity_report($regionId);
        $getAttendanceReportData = $this->getAttendanceReportData($regionId);
        $leaveReport = $this->leaveReport($regionId);
        return view('regional.dashboard',compact('manPowerGet','hygiene','averageCost','hiringReport','region','totalattritionReport','aging_report','manpowerPercentageReport','reasonreportdata','hiringinprocessreport','total_pending_employee','genderwisetotalemployee','hiring_employee','genderwiseattrition','productivity_report','leaveReport','getAttendanceReportData'));
    }

	public function manpowerstore(){
		view()->share('route', 'man-power');
        $region = Region::find(Auth::user()->region);
        $storebudge = $this->storebudgetstorewise(Auth::user()->region);
        // $storeBudgetManPower = \App\BudgetStore::leftJoin('store_location1', 'store_location.id', 'budget_stores.store_id')
        //     ->where('store_location.status', 1)
        //     ->where('store_location.region_id', $region->id)
        //     ->get();
        $store_reports = $storebudge['reports'];
        $designations = $this->arrayForLabelDesignation;//$storebudge['designations'];
		return view('regional.report.manpowerStore',compact('region','store_reports','designations'));
	}

    public function storebudgetstorewise($region_id)
    {
        $stores = \App\StoreLocation::where('region_id', $region_id)->where('status', 1)->get();
        $storeBudgetManPower = \App\BudgetStore::leftJoin('store_location', 'store_location.id', 'budget_stores.store_id')
            ->where('store_location.status', 1)
            ->where('store_location.region_id', $region_id)
            ->get();
        $designations = $this->arrayForLabelDesignation;;
        $store_reports = array();
        foreach ($stores as $store) {
            $data = array();
            $data['store_id'] = $store->id;
            $data['io_code'] = $store->io_code;
            $data['store_name'] = $store->store_name;
            if ($designations) {
                $total = 0;
                foreach ($designations as $key => $value) {
                    $manpowerbudget = $storeBudgetManPower->where('store_id', $store->id)->where('label', $key)->first();
                    if ($manpowerbudget) {
                        $total = $total + $manpowerbudget->budgetManpower;
                        $data['designation'][$key] = $manpowerbudget->budgetManpower;
                    } else {
                        $data['designation'][$key] = 0;
                    }
                }
                $data['designation'][] = $total;
            }
            $store_reports[] = $data;
        }
        $return['reports'] = $store_reports;
        $return['designations'] = $designations;
        return $return;
    }

	public function manpowerstoredata(Request $request){
		$regionId = Auth::user()->region;
		// $manPowerGet = StoreLocation::where('store_location.status',1)->where('store_location.region_id',$regionId)->select('store_location.*',
        //                     \DB::raw("CONCAT(io_code,' (',sap_code,')') as store_code"),
        //                     \DB::raw("(budget_man_power - actual_man_power) as gap"),
        //                     'city.name as city',
		// 					'region.name')->leftJoin('region','region.id','store_location.region_id')->leftjoin('city',function($join){
        //                     $join->on('city.id','=','store_location.city_id');
        //                 })->get();
    
		// return Datatables::of($manPowerGet)
        //     ->make(true);
        $manPower = StoreLocation::select([
            'store_location.*',
            \DB::raw("CONCAT(io_code,' (',sap_code,')') as store_code"),
            \DB::raw("(budget_man_power - actual_man_power) as gap"),
            'city.name as city',
            'region.name'
        ])
            ->leftjoin('city', function ($join) {
                $join->on('city.id', '=', 'store_location.city_id');
            })
            ->leftJoin('region', 'region.id', 'store_location.region_id');
        
            $manPower->where('store_location.region_id', $regionId);
        
        $manPower = $manPower->orderBy('store_location.budget_man_power', 'DESC')
            ->get();

        return Datatables::of($manPower)
            ->make(true);
	}
	public function manpowercoststore(){
		view()->share('route', 'manpowercost');
		$region = Region::find(Auth::user()->region);
		return view('regional.report.manpowercostStore',compact('region'));
	}

	public function manpowercoststoredata(Request $request){
		$regionId = Auth::user()->region;
		$averageCost = Employee::where('employee.processstatus','!=','Inactive')->where('store_location.status',1)->leftJoin('store_location','store_location.id','employee.store_id')->leftJoin('region','region.id','store_location.region_id')->select(DB::raw('sum(ctc) as salary'),DB::raw('count(employee.id) as total_employee'),DB::raw('( CASE WHEN SUM(ctc) / COUNT(employee.id) > 0 THEN SUM(ctc)/COUNT(employee.id) ELSE 0 END) AS avg_cost'),'region.name','region.id as region_id','store_location.store_name','store_location.io_code as store_code','city.name as city')->leftjoin('city',function($join){
                            $join->on('city.id','=','store_location.city_id');
                        })->where('region.id',$regionId)->groupBy('store_location.id')->get();
		return Datatables::of($averageCost)
            ->make(true);
	}

	public function hygienestore(){
		view()->share('route', 'hygiene');
		$region = Region::find(Auth::user()->region);
		return view('regional.report.hiegineStore',compact('region'));
	}

	public function hygienestoredata(Request $request){
        $id = Auth::user()->region;
        $region = Region::find($id);
        $store = StoreLocation::where('store_location.region_id',$id)->get();
        $hygiene['id_card'] = StoreLocation::
        leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.id_card_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as id_card'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('id_card','store_code');
        
        $hygiene['uniform'] = StoreLocation::leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.uniform_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as uniform_count'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('uniform_count','store_code');;


        $hygiene['l0l1_untrained'] = StoreLocation::leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.l0l1_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as l0l1_untrained_count'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('l0l1_untrained_count','store_code');


        $hygiene['l0l1_certified'] = StoreLocation::leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.l0l1_certificate_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as l0l1_certificate_count'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('l0l1_certificate_count','store_code');


        $hygiene['certificate_pending'] = StoreLocation::leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.l0l1_certificate_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as certificate_pending_count'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('certificate_pending_count','store_code');


        $hygiene['appoint_letter'] = StoreLocation::leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.appointment_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as appointment_status_count'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('appointment_status_count','store_code');
        $data = array();
        $i=0;
        
		foreach($store as $key => $value){
            $store_code = $value->io_code;
            $data[$i]['store_name'] = $value->store_name;
			$data[$i]['store_name'] = $value->store_name;
			$data[$i]['store_code'] = $value->io_code;
			$data[$i]['region_name'] = ($region)?$region->name:'';
			$data[$i]['id_card'] = isset($hygiene['id_card'][$store_code])? $hygiene['id_card'][$store_code] : 0;
			$data[$i]['uniform'] =  isset($hygiene['uniform'][$store_code])?$hygiene['uniform'][$store_code] : 0;
			$data[$i]['l0l1_untrained'] =  isset($hygiene['l0l1_untrained'][$store_code])?$hygiene['l0l1_untrained'][$store_code] : 0;
			$data[$i]['l0l1_certified'] =  isset($hygiene['l0l1_certified'][$store_code])?$hygiene['l0l1_certified'][$store_code] : 0;
			$data[$i]['certificate'] =  isset($hygiene['certificate_pending'][$store_code])?$hygiene['certificate_pending'][$store_code] : 0;
			$data[$i]['appointment'] =  isset($hygiene['appoint_letter'][$store_code])?$hygiene['appoint_letter'][$store_code] : 0;
            
            $i++;
            
		}
        
        //dd($data);
		$data = collect($data);
        return Datatables::of($data)
            ->make(true);
    }
    public function agingregion($regionId)
    {
        $infant_attrition = 0;
        $baby_attrition = 0;
        $attrition = 0;
        $leaved_employee = Employee::selectRaw('resignation.last_working_date,employee.id,employee.joining_date,employee.date_of_leave')
                    ->Join('resignation','employee.id','resignation.employee_id')
                    ->leftJoin('store_location','store_location.id','employee.store_id')
                    ->leftJoin('region','region.id','store_location.region_id')
                    ->where('store_location.status',1)
                    ->where('region.status',1)
                    ->where('region.id',$regionId)
                    ->whereNull('resignation.deleted_at')
                    ->where(function ($query) {
                            $query->where('resignation.last_working_date', '>=', date('Y-m-d',strtotime('1-4-'.date('Y'))))
                                ->orWhere('employee.date_of_leave', '>=', date('Y-m-d',strtotime('1-4-'.date('Y'))));
                    })->get();
        $baby_attrition = 0;
        foreach ($leaved_employee as $employee) {

            $fdate = str_replace('/', '-',$employee->joining_date);
            $tdate = (!empty($employee->date_of_leave))?$employee->date_of_leave:$employee->last_working_date;

            $datetime1 = new DateTime($fdate);
            $datetime2 = new DateTime($tdate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');

            if($days <= 30){
                $infant_attrition= $infant_attrition+1;
            } else if($days >30 && $days <=90 ){
                $baby_attrition = $baby_attrition+1;
            } else {
                $attrition = $attrition+1;
            }
        }

        $aging_report['baby_attrition'] = $baby_attrition;
        $aging_report['infant_attrition'] = $infant_attrition;
        $aging_report['attrition'] = $attrition;
        $aging_report['total'] =count($leaved_employee);
        return $aging_report;
    }

    public function attritionReportReasonRegiontotal($region_id)
    {
        $reasons =ResignationReason::where('status','Active')
                                    ->where('parent_id',0)
                                    ->get();
        $stores = StoreLocation::where('status',1)
                                    ->where('region_id',$region_id)
                                    ->get();
      /*  $store_ids = array();
        if($stores){
            foreach ($stores as $store) {
                $store_ids[] =  $store->id;
            }
        } */
        $current_year = date('Y');
        $current_month = date('m');
        if($current_month < 4){
            $current_year--;
        }
        $reasonwise = Resignation::selectRaw('resignation.reason_id,store_location.store_name')
                                ->Join('employee','employee.id','resignation.employee_id')
                                ->Join('store_location','store_location.id','employee.store_id')
                                ->Join('region','region.id','store_location.region_id')
                                ->where('region.id',$region_id)
                                ->where('store_location.status',1)
                                ->where('region.status',1)
                                ->whereNull('resignation.deleted_at')
                                ->wheredate('resignation.last_working_date','>=',date('Y-m-d',strtotime('1-4-'.$current_year)))
                                ->get();
        $report_data = array();
            if($stores){
                foreach ($reasons as $reason) {
                            $reasonwisedata = $reasonwise->where('reason_id',$reason->id)->count();
                            $report_data[$reason->id] =(!empty($reasonwisedata))?$reasonwisedata:0;
                }
            }
        $return_array['reasons'] = $reasons;
        $return_array['report_data'] = $report_data;
        return $return_array;
    }
    public function hiringinprocessreport($regionId)
    {
        $store_side_pending = Employee::selectRaw('count(employee.id) as total_pending')
                                    ->leftJoin('store_location','store_location.id','employee.store_id')
                                    ->where('store_location.region_id',$regionId)
                                    ->whereNotIn('employee.hrms_status',[2])
                                    ->whereIn('employee.store_status',[3,0])
                                    ->whereNotIn('employee.regional_status',[2])
                                    ->whereIN('employee.status',['pending','approve'])
									->whereNULL('store_location.deleted_at')
                                    ->where('store_location.status',1)
                                    ->first();
        $region_side_pending = Employee::selectRaw('count(employee.id) as total_pending')
                                    ->leftJoin('store_location','store_location.id','employee.store_id')
                                    ->where('store_location.region_id',$regionId)
                                    ->whereNotIn('employee.hrms_status',[2])
                                    ->whereIn('employee.store_status',[1])
                                    ->whereNotIn('employee.store_status',[3])
                                    ->whereNotIn('employee.regional_status',[2])
                                    ->whereIN('employee.status',['pending','approve'])
                                    ->whereNULL('store_location.deleted_at')
                                    ->where('store_location.status',1)
                                    ->first();
        $hrms_side_pending = Employee::selectRaw('count(employee.id) as total_pending')
                                    ->leftJoin('store_location','store_location.id','employee.store_id')
                                    ->where('store_location.region_id',$regionId)
                                    ->where('employee.hrms_status',1)
                                    ->whereIn('employee.regional_status',[2])
                                    ->whereIN('employee.status',['pending','approve'])
                                    ->whereNotIn('employee.hrms_status',[2,3])
                                    ->whereNULL('store_location.deleted_at')
                                    ->where('store_location.status',1)
                                    ->first();
        $data['store_pending'] = $store_side_pending->total_pending;
        $data['region_pending'] = $region_side_pending->total_pending;
        $data['hrms_pending'] = $hrms_side_pending->total_pending;
        return $data;
    }

    public function genderwisetotalemployee($region_id)
    {
        $report_data = array();

                $gendermale = Employee::selectRaw('count(employee.id) as total_employee')
                                            ->join('store_location','store_location.id','employee.store_id')
                                            ->where('employee.processstatus','!=','Inactive')
                                            ->where('employee.gender','male')
                                            ->where('store_location.status',1)
                                            ->where('store_location.region_id',$region_id)
                                            ->first();

                $genderfemale = Employee::selectRaw('count(employee.id) as total_employee')
                                            ->join('store_location','store_location.id','employee.store_id')
                                            ->where('employee.processstatus','!=','Inactive')
                                            ->where('employee.gender','female')
                                            ->where('store_location.status',1)
                                            ->where('store_location.region_id',$region_id)
                                            ->first();
                $totalemployee = Employee::selectRaw('count(employee.id) as total_employee')
                                            ->join('store_location','store_location.id','employee.store_id')
                                            ->where('employee.processstatus','!=','Inactive')
                                            ->where('store_location.status',1)
                                            ->where('store_location.region_id',$region_id)
                                            ->first();
            $report_data['total'] = $totalemployee->total_employee;
            $report_data['male'] = $gendermale->total_employee;
            $report_data['female'] = $genderfemale->total_employee;
        return $report_data;
    }

    public function genderwisehiring($region_id)
    {

            $gendermale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->whereDate('employee.joining_date', '>=', date('Y-m-01'))
                                    ->where('employee.gender','male')
                                    ->where('region.id',$region_id)
                                    ->first();
            $genderfemale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->where('employee.gender','female')
                                    ->whereDate('employee.joining_date', '>=', date('Y-m-01'))
                                    ->where('store_location.region_id',$region_id)
                                    ->first();
            $total_employee = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->whereDate('employee.joining_date', '>=', date('Y-m-01'))
                                    ->where('store_location.region_id',$region_id)
                                    ->first();

            $report_data['total'] = $total_employee->total_employee;
            $report_data['male'] = $gendermale->total_employee;
            $report_data['female'] = $genderfemale->total_employee;


        return $report_data;

    }

    public function genderwiseattrition($region_id)
    {
        $regions = Region::where('status',1)->get();
        $report_data = array();

            $gendermale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->join('resignation','resignation.employee_id','employee.id')
                                    ->where('employee.processstatus','Inactive')
                                    ->where('employee.gender','male')
                                    ->where(function($query){
                                        $query->whereDate('employee.date_of_leave', '>=', date('Y-m-1'));
                                        $query->orwhereDate('resignation.last_working_date', '>=',date('Y-m-1'));
                                    })
                                    ->where('region.id',$region_id)
                                    ->first();
            $genderfemale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->join('resignation','resignation.employee_id','employee.id')
                                    ->where('employee.processstatus','Inactive')
                                    ->where('employee.gender','female')
                                    ->where(function($query){
                                        $query->whereDate('employee.date_of_leave', '>=', date('Y-m-1'));
                                        $query->orwhereDate('resignation.last_working_date', '>=',date('Y-m-1'));
                                    })
                                    ->where('region.id',$region_id)
                                    ->first();

            $report_data['male'] = $gendermale->total_employee;
            $report_data['female'] = $genderfemale->total_employee;


        return $report_data;
    }
    public function productivity_report($regionId)
    {
        $current_year = date('Y');
        $current_month = date('m');
        $current_month--;
        if($current_month < 1){
            $current_year--;
        }

        $data = array();
        $collection_report = StoreCollection::selectRaw('store_collections.*,SUM(store_collections.amount) as total_amount,store_location.store_name, store_location.id as store_id')
                                            ->join('store_location','store_location.id','store_collections.store_id')
                                            ->join('region','region.id','store_location.region_id')
                                            ->where('store_collections.month',$current_month)
                                            ->where('store_collections.year',$current_year)
                                            ->where('store_location.status',1)
                                            ->where('store_location.region_id',$regionId)
                                            ->groupby('store_collections.store_id')
                                            ->first();
        $total_employee = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->Leftjoin('store_location','store_location.id','employee.store_id')
                                    ->where('store_location.region_id',$regionId)
                                    ->first();

        $data['total_collection'] = ($collection_report)?$collection_report->total_amount:"-";
        $data['total_employee'] = ($total_employee)?$total_employee->total_employee:"-";
        $data['per_person_income'] = ($collection_report)?($collection_report->total_amount !=0  && $total_employee->total_employee >0)? round($collection_report->total_amount/$total_employee->total_employee,2):'-':'-';

        return $data;
    }
    public function manpower_cost($regionId)
    {
        $current_year = date('Y');
        $current_month = date('m');
        $current_month--;
        if($current_month < 1){
            $current_year--;
        }

        $data = array();

        $collection_report = StoreCollection::selectRaw('store_collections.*,SUM(store_collections.amount) as total_amount')
                                                    ->Leftjoin('store_location','store_location.id','store_collections.store_id')
                                                    ->where('store_location.region_id',$regionId)
                                                    ->where('store_collections.month',$current_month)
                                                    ->where('store_collections.year',$current_year)
                                                    ->first();
        $total_employee = Employee::selectRaw('count(employee.id) as total_employee,SUM(employee.ctc) as total_ctc')
                                    ->Leftjoin('store_location','store_location.id','employee.store_id')
                                    ->where('store_location.region_id',$regionId)
                                    ->where('employee.processstatus','!=','Inactive')
                                    ->first();
        $data['total_collection'] = $collection_report->total_amount;
        $data['total_employee'] = $total_employee->total_employee;
        $data['avg_ctc'] = ($total_employee->total_ctc > 0  && $total_employee->total_employee >0)? round($total_employee->total_ctc/$total_employee->total_employee,2):'-';
        $data['per_person_income'] = ($collection_report->total_amount !=0  && $total_employee->total_employee >0)? round($collection_report->total_amount/$total_employee->total_employee,2):'-';

        return $data;
    }

    public function leaveReport($regionId)
    {
        $leaveReport = Employee::selectRaw('SUM(other_infos.allocated_leave) as total_allocated_leave, SUM(other_infos.pending_leave) as total_pending_leave, SUM(other_infos.availed_leave) as availed_leave')
                                        ->leftjoin('other_infos','other_infos.employee_id','employee.id')
                                        ->leftJoin('store_location','store_location.id','employee.store_id')
										->leftjoin('region','region.id','store_location.region_id')
                                        ->where('region.status',1)
                                        ->where('employee.status','confirm')
                                        ->where('region.id',$regionId)
                                        ->whereNULL('region.deleted_at')
										->whereNULL('store_location.deleted_at')
										->where('store_location.status',1)
                                        ->where('region.status',1)
                                        ->orderby('region.id')
                                        ->first();
        return $leaveReport;
    }
    public function getAttendanceReportData($regionId){
		$selected_date = \Carbon\Carbon::now()->startOfDay()->subDays(1)->format("Y-m-d");
		$present_per = 100;
		$actual_man_power = 0;

        $manPowerGet = \App\StoreLocation::where('store_location.status',1)
                        ->where('store_location.region_id',$regionId)
                        ->select(DB::raw('SUM(store_location.actual_man_power) as total_actual_man_power'))
                        ->first();

        $present_man_power = \App\Employee::LeftJoin('attendance','attendance.employee_id','employee.id')
                            ->leftJoin('store_location','store_location.id','employee.store_id')
							->leftjoin('region','region.id','store_location.region_id')
                            ->where('region.id',$regionId)
                            ->whereNotIn('employee.status',['pending','cancel'])
                            ->where('attendance.full_date', '=',$selected_date)
                            ->where('attendance.attendance','p')
                            ->whereNULL('region.deleted_at')
                            ->whereNULL('store_location.deleted_at')
                            ->where('store_location.status',1)
                            ->where('region.status',1)
                            ->count();

		if($manPowerGet){
			$actual_man_power = $manPowerGet->total_actual_man_power;
		}
		if($actual_man_power > 0){
			$present_per =	round(($present_man_power * 100) / $actual_man_power, 2);
		}

		return ['present_per'=>$present_per,'actual_man_power'=>$actual_man_power,'present_man_power'=>$present_man_power];
    }
    

    public function exportstorebudget()
    {
        $region_id = Auth::user()->region;
        $reports = $this->storebudgetstorewise($region_id );


        $excel_export = array();
        $designations = $reports['designations'];
        $store_reports = $reports['reports'];
        $excel_header = array('Region Name');
        foreach ($designations as $designation) {
            $excel_header[] = $designation;
        }
        $excel_header[] = "Total";
        $excel_export[] = $excel_header;

        foreach ($store_reports as $store_report){
            $report_data = array();
            $report_data['store_name'] = $store_report['store_id'];
            $report_data['io_code'] = $store_report['io_code'];
            foreach ($store_report['designation'] as $key => $value){
                $report_data[] =$value;
            }
            $excel_export[] =$report_data;
        }

        Excel::create('Man Power Budget Store',function($excel) use ($excel_export){
                $excel->setTitle('Man Power Budget Store');
                $excel->sheet('Man Power Budget Store',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                });
            })->download('xlsx');
        return back();
    }
}
