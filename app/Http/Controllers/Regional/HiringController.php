<?php

namespace App\Http\Controllers\Regional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use App\Helper;
use App\HiringReport;
use App\ResignationReason;
use App\Resignation;
use Carbon\Carbon as Carbon;
use DateTime;
use Auth;
use DB;

class HiringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.regional.employee');
        $this->middleware('permission:access.regional.employee.edit')->only(['edit','update']);
        $this->middleware('permission:access.regional.employee.create')->only(['create', 'store']);
        $this->middleware('permission:access.regional.employee.delete')->only('destroy');

        view()->share('route', 'hiring');
        view()->share('module', 'Hiring');
    }

    public function index(Request $request)
    {
        $sourceCategory = SourceCategory::where("status","1")->pluck('category','id')->toArray();
        view()->share('sourceCategory',$sourceCategory);

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        return view('regional.hiring.index');
    }

    public function datatable(Request $request)
    {
        $employee = Employee::select([
                    'employee.*',
                    'designation.name as designation',
                    'store_location.store_name',
                    'store_location.location_code',
                ])
                ->leftJoin('designation',function($join){
                    $join->on('employee.designation_id','=','designation.id');
                })
                ->leftJoin('store_location',function($join){
                    $join->on('employee.store_id','=','store_location.id');
                })
                ->whereIn('store_status',[1])
                ->where('employee.hrms_status',"!=","2")
                ->where('employee.store_hold',"0")
                ->where('store_location.region_id',auth()->user()->region)
                ->orderBy('employee.id','DESC')
                ->get();

        return Datatables::of($employee)
            ->make(true);
    }

    public function logView(Request $request,$id)
    {
        view()->share('id',$id);
        return view('regional.hiring.log');
    }

    public function log(Request $request,$id)
    {
        $logs = Log::select([
                    'logs.*',
                    'users.emp_name as user_name',
                    'roles.label as user_role',
                ])
                ->leftJoin('users',function($join){
                    $join->on('users.id','=','logs.user_id');
                })
                ->leftJoin('roles',function($join){
                    $join->on('roles.id','=','logs.user_role_id');
                })
                // ->where('user_id',auth()->id())
                ->where('logs.emp_id',$id)
                ->orderBy('logs.id','DESC')
                ->latest()->get();

        return Datatables::of($logs)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return redirect('regional/employee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //not allowed
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($id);

        if(!$employee){
            return redirect('regional/hiring')->with('flash_error', 'Employee Not Found!');
        }

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        $employee->otherinfo = \App\OtherInfo::where('employee_id',$id)->first();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        return view('regional.hiring.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employee = Employee::find($id);

        if(!$employee){
            return redirect('regional/hiring')->with('flash_error', 'Employee Not Found!');
        }

        $request = new Request;
        $currentCity = $permanentCity = [];
        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        $city = City::where("status","1")->pluck('name','id')->toArray();
        view()->share('city',$city);

        if(old('current_state',$employee->current_state)){
            $currentCity = $this->getCityFromState($request,['state_id'=>old('current_state',$employee->current_state)]);
        }
        view()->share('currentCity',$currentCity);

        if(old('permanent_state',$employee->permanent_state)){
            $permanentCity = $this->getCityFromState($request,['state_id'=>old('permanent_state',$employee->permanent_state)]);
        }
        view()->share('permanentCity',$permanentCity);

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        $locationCode = StoreLocation::where("status","1")->pluck('location_code','id')->toArray();
        view()->share('locationCode',$locationCode);

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);

        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $subDepartments = [];
        if(old('department_id',$employee->department_id)){
            $subDepartments = $this->getSubDepartments($request,['department_id'=>old('department_id',$employee->department_id)]);
        }
        view()->share('subDepartments',$subDepartments);

        $businessUnits = BusinessUnit::where("status","1")->pluck('name','id')->toArray();
        view()->share('businessUnits',$businessUnits);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->pluck('name','id')->toArray();
        view()->share('employeeClassification',$employeeClassification);

        $variablePayType = VariablePayType::where("status","1")->pluck('name','id')->toArray();
        view()->share('variablePayType',$variablePayType);

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        return view('regional.hiring.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        if($requestData['pan_status'] == 'not_available'){
            $requestData['pan_number'] = '';
        }
        if($requestData['aadhar_status'] == 'not_available'){
            $requestData['aadhar_number'] = '';
        }
        if($requestData['aadhar_status'] == 'available'){
            $requestData['aadhar_enrolment_number'] = '';
        }
        $requestData['date_of_birth'] = Helper::ymd(str_replace('/', '-', $requestData['date_of_birth']));
        $requestData['anniversary_date'] = Helper::ymd(str_replace('/', '-', $requestData['anniversary_date']));
        $requestData['first_time_employment'] = isset($requestData['first_time_employment'])?1:0;

        if($request->button == 'Update & Send to HR'){
            $requestData['regional_approve_id'] = auth()->id();
            $requestData['regional_status'] = 2;
            $requestData['hrms_status'] = 1;
            $requestData['status'] = 'approve';
        }elseif($request->button == 'Update & Send Back to SM'){
            $requestData['regional_status'] = 3;
            $requestData['store_status'] = 3;
            $requestData['status'] = 'pending';
        }

        $employee = Employee::where('id',$id)->update(array_except($requestData,['attach','_method','_token','button','education','previous','family']));

        if(isset($requestData['education']) && !empty($requestData['education'])){
            foreach ($requestData['education'] as $value) {
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    $value['deleted_at'] = null;
                    \DB::table('education_detail')->where('id',$value['id'])->update($value);
                }else{
                    EducationDetail::insert($value);
                }
            }
        }

        if($requestData['first_time_employment'] == 1 && isset($requestData['previous']) && !empty($requestData['previous'])){
            foreach ($requestData['previous'] as $value) {

                if($value['joining_date']){
                    $value['joining_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $value['joining_date']))->format('Y-m-d');
                }
                if($value['leaving_date']){
                    $value['leaving_date'] = \Carbon\Carbon::parse(str_replace('/', '-', $value['leaving_date']))->format('Y-m-d');
                }
                $value['employee_id'] = $id;
                if(isset($value['id'])){
                    $value['deleted_at'] = null;
                    \DB::table('previous_experience')->where('id',$value['id'])->update($value);
                }else{
                    PreviousExperience::insert($value);
                }
            }
        }else{
            PreviousExperience::where('employee_id',$id)->delete();
        }

        if(isset($requestData['family']) && !empty($requestData['family'])){
            foreach ($requestData['family'] as $value) {
                $value['employee_id'] = $id;
                if($value['date'] == ''){
                    $value['date'] = '';
                    $value['month'] = '';
                    $value['year'] = '';
                }else{
                    $value['date'] = \Carbon\Carbon::parse(str_replace('/', '-',$value['date']))->format('Y-m-d');
                    $date = explode('-', $value['date']);
                    $value['date'] = $date[2];
                    $value['month'] = $date[1];
                    $value['year'] = $date[0];
                }
                if(isset($value['id'])){
                    $value['deleted_at'] = null;
                    \DB::table('family_information')->where('id',$value['id'])->update($value);
                }else{
                    FamilyInfo::insert($value);
                }
            }
        }
        // create log
        $log['title'] = "Employee Update";
        $log['emp_id'] = $id;
        $log['emp_name'] = $request->full_name;
        $log['note'] = $request->regional_note;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        if($request->button == 'Update & Send to HR'){
            return redirect('regional/hiring')->with('flash_success', 'Employee updated and request send to HR Manager!');
        }elseif($request->button == 'Update & Send Back to SM'){
            return redirect('regional/hiring')->with('flash_success', 'Employee updated and send back to Store Manager!');
        }
        return redirect('regional/hiring')->with('flash_success', 'Employee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        Employee::find($id)->delete();
        EducationDetail::where('employee_id',$id)->delete();
        PreviousExperience::where('employee_id',$id)->delete();
        FamilyInfo::where('employee_id',$id)->delete();

        if($request->has('from_index')){
            $message = "Employee Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee deleted!');

            return redirect('regional/hiring');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'designation_id' => 'required',
            'full_name' => 'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'position' => 'required|max:191',
            'blood_group' => 'required',
            'rh_factor' => 'required',
            'marital_status' => 'required',
            'date_of_birth' => 'required|date_format:d/m/Y',
            'anniversary_date' => 'nullable|required_if:marital_status,married|date_format:d/m/Y',
            'aadhar_status' => 'required',
            'nationality' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'gender' => 'required',
            'pan_status' => 'required',
            'mobile' => ['required','numeric',function($attribute, $value, $fail) use($request,$id) {
                            if(strlen($value) != 10){
                                $fail('The mobile no must be of 10 digits');
                            }
                        }],
            'email' => 'nullable|email',
            'emergency_person' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'emergency_number' => 'required|numeric',
            'emergency_relationship' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'current_address_line_1' => 'required|max:191',
            'current_address_line_2' => 'nullable|max:191',
            'current_address_line_3' => 'nullable|max:191',
            'esic_number' => 'required|max:10|min:10',
            'current_state' => 'required',
            'current_city' => 'required',
            'current_pincode' => 'required|min:6|max:6',
            'permanent_address_line_1' => 'required|max:191',
            'permanent_address_line_2' => 'nullable|max:191',
            'permanent_address_line_3' => 'nullable|max:191',
            'permanent_state' => 'required',
            'permanent_city' => 'required',
            'permanent_pincode' => 'required|min:6|max:6',
            'aadhar_number' => 'nullable|required_if:aadhar_status,available|regex:/^[a-z0-9 .\-]+$/i',
            'aadhar_enrolment_number' => 'nullable|required_if:aadhar_status,not_available|regex:/^[a-z0-9 .\-]+$/i|min:14|max:14',
            'education'=>[
                        function($attribute, $value, $fail) use($request,$id) {
                            $status = true;
                            foreach ($value as $value) {
                                if($value['education_category_id'] != '' ||
                                    $value['board_uni'] != '' ||
                                    $value['institute'] != '' ||
                                    $value['qualification'] != '' ||
                                    $value['specialization'] != '' ||
                                    $value['month'] != '' ||
                                    $value['year'] != ''
                                        ){
                                    $status = false;
                                }
                            }
                            if($status){
                                $education = 0;
                                $fail('atleast one education detail is required');
                            }
                        },
                    ],
        ];

        if($request->pan_status == 'available'){
            $rules['pan_number'] = 'nullable|required_if:pan_status,available|size:10|regex:/^[a-z0-9 .\-]+$/i';
        }

        $message = [];
        foreach($request->education as $key=>$value){
            $rules["education.$key.board_uni"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.institute"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.qualification"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
            $rules["education.$key.specialization"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';

            $message["education.$key.board_uni.regex"]='The board / uni format is invalid';
            $message["education.$key.board_uni.max"]='The board / uni may not be greater than 191 characters.';
            $message["education.$key.institute.regex"]='The institute format is invalid';
            $message["education.$key.institute.max"]='The institute may not be greater than 191 characters.';
            $message["education.$key.qualification.regex"]='The qualification format is invalid';
            $message["education.$key.qualification.max"]='The qualification may not be greater than 191 characters.';
            $message["education.$key.specialization.regex"]='The specialization format is invalid';
            $message["education.$key.specialization.max"]='The qualification may not be greater than 191 characters.';
        }

        if(isset($request->first_time_employment)){
            foreach($request->previous as $key=>$value){
                $rules["previous.$key.company_name"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.designation"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.reason_of_leaving"]='nullable|regex:/^[a-z0-9 .\-]+$/i|max:191';
                $rules["previous.$key.joining_date"]='nullable|date_format:d/m/Y';
                $rules["previous.$key.leaving_date"]='nullable|date_format:d/m/Y|after:'.$value['joining_date'];

                $message["previous.$key.company_name.regex"]='The company name format is invalid';
                $message["previous.$key.company_name.max"]='The company name may not be greater than 191 characters.';
                $message["previous.$key.designation.regex"]='The designation format is invalid';
                $message["previous.$key.designation.max"]='The designation may not be greater than 191 characters.';
                $message["previous.$key.reason_of_leaving.regex"]='The reason of leaving format is invalid';
                $message["previous.$key.reason_of_leaving.max"]='The reason of leaving may not be greater than 191 characters.';
                $message["previous.$key.joining_date.date"]='The joining date is not a valid date';
                $message["previous.$key.joining_date.date_format"]='The joining date format will be d/m/Y';
                $message["previous.$key.leaving_date.date"]='The leaving date is not a valid date';
                $message["previous.$key.leaving_date.date_format"]='The leaving date format will be d/m/Y';
                $message["previous.$key.leaving_date.after"]='The leaving date must be a date after '.$value['joining_date'];
            }
        }

        foreach($request->family as $key=>$value){
            $rules["family.$key.aadhar_number"]='nullable|regex:/^[a-z0-9 .\-]+$/i';
            $rules["family.$key.name"]='nullable|regex:/^[a-z0-9 .\-]+$/i';
            $rules["family.$key.date"]='nullable|date_format:d/m/Y';

            $message["family.$key.aadhar_number.regex"]='The aadhar number format is invalid';
            $message["family.$key.name.regex"]='The name format is invalid';
            $message["family.$key.date.date"]='The date is not a valid date';
            $message["family.$key.date.date_format"]='The date format will be d/m/Y';
            $message["family.$key.leaving_date.date"]='The leaving date is not a valid date';
            $message["family.$key.leaving_date.date_format"]='The leaving date format will be d/m/Y';
        }

        return $this->validate($request, $rules,array_merge([
            'designation_id.required' => 'The designation field is required.',
            'aadhar_number.required_if' => 'The aadhar number field is required when availability of aadhar is available.',
            'aadhar_enrolment_number.required_if' => 'The aadhar enrolment number field is required when availability of aadhar is not available.',
            'pan_number.required_if' => 'The pan number field is required when availability of PAN is available.',
        ],$message));
    }

    public function getSubDepartments(Request $request,$departmentId=0)
    {
        return SubDepartment::where("status","1")->where('department_id',$request->get('department_id',$departmentId))->pluck('name','id')->toArray();
    }

    public function getSourceDetail(Request $request,$name=0)
    {
        return SourceCategory::where("status","1")->where('id',$request->get('name',$name))->first();
    }

    public function getLocationDetail(Request $request,$locationCode=0)
    {
        return StoreLocation::where("status","1")->where('id',$request->get('locationCode',$locationCode))->first();
    }

    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function getStateFromRegion(Request $request,$region=0)
    {
        return State::where("status","1")->where('region_id',$request->get('region_id',$region))->pluck('name','id')->toArray();
    }

    public function changeStatus($table,$status,$id)
    {
        $status = ($status == 0)?1:0;

        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    }

    public function approve(Request $request, $id)
    {
        $rules = [
                'source_category'=>'required',
                'grade_id'=>'required',
                'joining_date'=>'nullable|date_format:d/m/Y',
            ];

        if(request()->category_name == 'Rehire'){
            $rules['source_name'] = 'required';
            $rules['old_employee_code'] = 'required';
        }elseif(request()->category_name == 'Employee Referral'){
            $rules['employee_name'] = 'required';
            $rules['employee_code'] = 'required';
        }else{
            $rules['source_code'] = 'required';
        }

        $this->validate($request,$rules);

        $employee = Employee::find($id);
        $employee->regional_approve_id = auth()->id();
        $employee->source_category_id = request()->source_category;
        $employee->source_detail_id = request()->source_name;
        $employee->source_code = request()->source_code;
        $employee->employee_name = request()->employee_name;
        $employee->employee_code = request()->employee_code;
        $employee->old_employee_code = request()->old_employee_code;
        $employee->employee_refferal_amount = request()->employee_refferal_amount;
        $employee->employee_refferal_payment_month = request()->employee_refferal_payment_month;
        $employee->grade_id = request()->grade_id;
        if(request()->joining_date != ''){
            $employee->joining_date = request()->joining_date;
        }
        $employee->regional_status = 2;
        $employee->hrms_status = 1;
        $employee->status = 'approve';
        $employee->save();

        // create log
        $log['title'] = "Approved by Regional HR";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        return 'Approved ! Request send to HRMS';
    }

    public function sendBack(Request $request,$id)
    {
        /*$this->validate($request,[
                'note'=>'required'
            ]);*/

        $employee = Employee::find($id);
        $employee->regional_note = $request->note;
        $employee->regional_status = 3;
        $employee->store_status = 3;
        $employee->status = 'pending';

        // create log
        $log['title'] = "Send Back to Store Manager";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['note'] = $request->note;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        $employee->save();
        return 'Send Back to Store HR';
    }

    public function reject(Request $request,$id)
    {
        $this->validate($request,[
                'reject_reason'=>'required'
            ]);

        $employee = Employee::find($id);
        $employee->reject_reason = $request->reject_reason;
        $employee->regional_status = 4;

        // create log
        $log['title'] = "Reject by Regional HR";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['note'] = $request->reject_reason;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        $employee->save();
        return 'Rejected By Regional HR';
    }

    public function hold(Request $request,$id)
    {
        $employee = Employee::find($id);
        $employee->regional_status = 5;

        // create log
        $log['title'] = "Hold by Regional HR";
        $log['emp_id'] = $id;
        $log['emp_name'] = $employee->full_name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        $employee->save();

        Session::flash('flash_success', 'Hold by Regional HR!');

        return redirect('regional/hiring');
    }

    public function downloadForm($id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','grade','designation','subDepartment','subDepartment.department','brand'])->find($id);

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        // \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        $view = view('storeManager.hiring.download_form', compact('employee'));

        $pdf = \PDF::loadHTML($view);

        return $pdf->download('employee_detail.pdf');

    }

    public function uploadDocuments($id,$upload=0)
    {
        view()->share('id',$id);
        view()->share('upload',$upload);

        $employee = Employee::find($id);

        $employee->attach = Attachment::where('employee_id',$id)->get();
        $attach = [];
        foreach ($employee->attach as $value) {
            if(in_array($value->key, ['education_documents','previous_company_docs'])){
                $attach[$value->key][$value->id]['file'] = $value->file;
                $attach[$value->key][$value->id]['key'] = $value->key;
            }else{
                $attach[$value->key]['file'] = $value->file;
                $attach[$value->key]['key'] = $value->key;
            }
        }
        $employee->attach = $attach;

        $businessUnits = BusinessUnit::where("status","1")->where("name","Unlimited")->first();
        view()->share('businessUnits',$businessUnits);

        $legalEntity = LegalEntity::where("status","1")->where("name","Arvind Lifestyle Brands Limited")->first();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->where("name","Store")->first();
        view()->share('employeeClassification',$employeeClassification);

        $variablePayType = VariablePayType::where("status","1")->where("name","Incentive")->first();
        view()->share('variablePayType',$variablePayType);

        return view('regional.hiring.upload_documents', compact('employee'));
    }

    public function saveDocuments(Request $request,$id)
    {
        $rules = Helper::uploadDocumentValidationRule($request,$id,'joining_date');

        $uploadRule = Helper::updateDocumentValidationRule($request,'joining_date');

        if($request->upload == 0){
            $this->validate($request, $rules, Helper::validationMessage());
        }else{
            $this->validate($request, $uploadRule,Helper::validationMessage());
        }

        $requestData = $request->all();

        Helper::uploadEmployeeDocuments($requestData);

        return redirect('regional/hiring')->with('flash_success', 'Documents Uploaded Successfully!');
    }

    public function storeHiringReport(Request $request)
    {

		view()->share('route', 'hiringReport');
        $current_year = date('Y');
        $current_month = date('m');
        if($current_month < 4){
            $current_year--;
        }
        $inmonths = array();
        for($i = 1; $i<=12;$i++){
            $next_year = $current_year+1;
            ($i < 4)?$inmonths[]= sprintf("%02d", $i)."-$next_year"  : $inmonths[]=  sprintf("%02d", $i)."-$current_year";
        }
        $hiringReport = array();
        $hiringReport = HiringReport::leftJoin('store_location','store_location.id','hiring_reports.store_id')
                        ->leftJoin('region','region.id','store_location.region_id')
                        ->whereIn('hiring_reports.monthyear',$inmonths)
                        ->where('hiring_reports.store_id',$request->route('store_id'))
                        ->orderBy('hiring_reports.id','DESC')
                        ->get();
        $store_name = '';
        if($hiringReport){
            if(isset($hiringReport[0])){
                $store_name = $hiringReport[0]->store_name;
            }

        }
        return view('regional.report.storehiring',compact('hiringReport','inmonths','store_name'));
    }

    public function storewiseAttritionReport(Request $request)
    {
		view()->share('route', 'attrition');
        return view('regional.report.attritionStorewise');
    }

    public function attritionReportStoredatatable(Request $request,$region_id)
    {
        $mtdattritionReport = HiringReport::selectRaw('hiring_reports.*,(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,store_location.store_name,region.name')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('monthyear',[date('m-Y')])
                                ->where('store_location.status',1)
                                ->where('region.status',1)
                                ->where('hiring_reports.region_id',$region_id)
                                ->groupBy('hiring_reports.store_id')
                                ->get();
        $Stores = StoreLocation::selectraw('region.name,store_location.*')->leftJoin('region','region.id','store_location.region_id')->where('region_id',$region_id)->where('store_location.status',1)->get();
        $data = array();
        foreach ($Stores as $store) {
            $mtdaa = $mtdattritionReport->where('store_id',$store)->first();
            array_push($data,array(
                'name' => $store->name,
                'store_id' => $store->id,
                'store_name' => $store->store_name,
                'io_code' => $store->io_code,
                'Total'=>($mtdaa)?$mtdaa->Total:0,
                'absconding'=>($mtdaa)?$mtdaa->absconding:0,
                'resignation'=>($mtdaa)?$mtdaa->resignation:0,
            ));
        }


        return Datatables::of($data)
            ->make(true);
    }

    public function attritionStoreReportStoredatatable(Request $request,$store_id)
    {
        view()->share('route', 'attrition');
        view()->share('module', 'Reports');
        $totalattritionReport = HiringReport::selectRaw('hiring_reports.*,(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,store_location.store_name,region.name,store_location.actual_man_power')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('monthyear',[date('m-Y')])
                                ->where('hiring_reports.store_id',$store_id)
                                ->first();
        $store = StoreLocation::find($store_id);
        return view('regional.report.storeattritionalReport',compact('totalattritionReport','store'));
    }

    public function agingregionstore(Request $request)
    {
        view()->share('route', 'attrition-aging');
        view()->share('module', 'Reports');
        return view('regional.report.storeaging');
    }
    public function agingregionstoredatatable(Request $request,$region_id)
    {
        $stores = StoreLocation::where('status',1)->where('region_id',$region_id)->get();
        $store_id = array();
        $store_array = array();
        foreach ($stores as $store) {
                $store_array[$store->id]['store_id'] = $store->id;
                $store_array[$store->id]['io_code'] = $store->io_code;
                $store_array[$store->id]['store_name'] = $store->store_name;
                $store_array[$store->id]['total'] = 0;
                $store_array[$store->id]['infant_attrition'] = 0;
                $store_array[$store->id]['baby_attrition'] = 0;
                $store_array[$store->id]['attrition'] = 0;
                $store_id[] = $store->id;
        }
        $leaved_employee = Employee::selectRaw('employee.store_id,region.id as region_id,store_location.store_name,resignation.last_working_date,employee.id,employee.joining_date,employee.date_of_leave')
                    ->Join('resignation','employee.id','resignation.employee_id')
                    ->leftJoin('store_location','store_location.id','employee.store_id')
                    ->leftJoin('region','region.id','store_location.region_id')
                    ->where('store_location.status',1)
                    ->where('region.status',1)
                    ->whereIn('employee.store_id',$store_id)
                    ->whereNull('resignation.deleted_at')
                    ->where(function ($query) {
                            $query->where('resignation.last_working_date', '>=', date('Y-m-d',strtotime('1-4-'.date('Y'))))
                                ->orWhere('employee.date_of_leave', '>=', date('Y-m-d',strtotime('1-4-'.date('Y'))));
                    })
                    ->orderBy('employee.store_id')
                    ->get();

        foreach ($leaved_employee as $employee) {

            $fdate = str_replace('/', '-',$employee->joining_date);
            $tdate = (!empty($employee->date_of_leave))?$employee->date_of_leave:$employee->last_working_date;

            $datetime1 = new DateTime($fdate);
            $datetime2 = new DateTime($tdate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');

            $store_array[$employee->store_id]['total'] = $store_array[$employee->store_id]['total']+1;

            if($days <= 30){
                $store_array[$employee->store_id]['infant_attrition'] = (isset($store_array[$employee->store_id]['infant_attrition']))?$store_array[$employee->store_id]['infant_attrition']+1:1;
            } else if($days >30 && $days <=90 ){
                $store_array[$employee->store_id]['baby_attrition'] = (isset($store_array[$employee->store_id]['baby_attrition']))?$store_array[$employee->store_id]['baby_attrition']+1:1;
            } else {
                $store_array[$employee->store_id]['attrition'] = (isset($store_array[$employee->store_id]['attrition']))?$store_array[$employee->store_id]['attrition']+1:1;
            }
        }

        return Datatables::of($store_array)
            ->make(true);

    }

    public function attritionReportReasonRegion(Request $request)
    {
        view()->share('module', 'Reports');
        view()->share('route', 'attrition-reason');
        $region_id =Auth::user()->region;
        $reasons =ResignationReason::where('status','Active')
                                    ->where('parent_id',0)
                                    ->get();
        $stores = StoreLocation::where('status',1)
                                    ->where('region_id',$region_id)
                                    ->get();
        $report_data = array();
        $current_year = date('Y');
        $current_month = date('m');
        if($current_month < 4){
            $current_year--;
        }
            if($stores){
                $reasonwise = Resignation::selectRaw('resignation.reason_id,store_location.store_name,store_location.id as store_id')
                                    ->Join('employee','employee.id','resignation.employee_id')
                                    ->Join('store_location','store_location.id','employee.store_id')
                                    ->Join('region','region.id','store_location.region_id')
                                    ->where('store_location.region_id',$region_id)
                                    ->where('store_location.status',1)
                                    ->where('region.status',1)
                                    ->whereNull('resignation.deleted_at')
                                    ->wheredate('resignation.last_working_date','>=',date('Y-m-d',strtotime('1-4-'.$current_year)))
                                    ->get();
                foreach ($stores as $store) {
                    $data = array();
                    $data['store_name'] = $store->store_name;
                    $data['io_code'] = $store->io_code;
                    $data['store_id'] = $store->id;
                    foreach ($reasons as $reason) {
                                $reasonwisedata = $reasonwise->where('store_id',$store->id)
                                                            ->where('reason_id',$reason->id)->count();
                                $data[$reason->id] =(!empty($reasonwisedata))?$reasonwisedata:0;
                    }
                    $report_data[] = $data;
                }
            }
        $return_array['reasons'] = $reasons;
        $return_array['report_data'] = $report_data;

        return view('regional.report.reasonstorewise',compact('reasons','report_data'));
    }

    public function hiringReportbyStore(Request $request)
    {
        view()->share('module', 'Reports');
		view()->share('route', 'hiringReport');
        $regionId = Auth::user()->region;
        $current_year = date('Y');
        $current_month = date('m');
        if($current_month < 4){
            $current_year--;
        }
        $inmonths = array();
        for($i = 1; $i<=12;$i++){
            $next_year = $current_year+1;
            ($i < 4)?$inmonths[]= sprintf("%02d", $i)."-$next_year"  : $inmonths[]=  sprintf("%02d", $i)."-$current_year";
        }
        $hiringReport = array();
        $inmonths = array(date('m-Y'));
        $hiringReport = HiringReport::selectraw(
                                'hiring_reports.*,store_location.*'
                        )
                        ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                        ->leftJoin('region','region.id','store_location.region_id')
                        ->where('region.status',1)
                        ->where('store_location.status',1)
                        ->whereIn('hiring_reports.monthyear',$inmonths)
                        ->where('hiring_reports.region_id',$regionId)
                        ->orderBy('store_location.id')
                        ->get();

        $stores = StoreLocation::where('status',1)->where('region_id',$regionId)->get();
        $total_pending_employees = Employee::selectRaw('count(employee.id) as total_pending,store_location.id as store_id')
                                                ->join('store_location','store_location.id','employee.store_id')
                                                ->where('store_location.region_id',$regionId)
                                                ->whereNotIn('employee.status',['confirm','reject','inactive'])
                                                ->groupBy('store_location.id')
                                                ->get();
        $hiringReports = array();
        foreach ($stores as $store) {
                $hiring = $hiringReport->where('store_id',$store->id)->first();
                $total_pending_employee = $total_pending_employees->where('store_id',$store->id)->first();
            $hiringReports[] = array(
                        'store_id' =>$store->id,
                        'io_code' =>$store->io_code,
                        'gap' =>($hiring)?$hiring->gap:0,
                        'hired' =>($hiring)?$hiring->hired:0,
                        'pending' =>($hiring)?$hiring->pending:0,
                        'store_name' =>$store->store_name,
                        'in_process' =>($total_pending_employee)?$total_pending_employee->total_pending:0,
            );
        }
        return view('regional.report.hiringstorewise',compact('hiringReports'));
    }

    public function manpowerpercentageReportbyStore(Request $request)
    {
        view()->share('module', 'Reports');
        view()->share('route', 'manpower');
        $region_id = Auth::user()->region;
         $manpowerPercentageReport = HiringReport::selectRaw('region.name,store_location.id,store_location.store_name,store_location.io_code,(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,SUM(store_location.actual_man_power) as total_actual_man_power')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('monthyear',[date('m-Y')])
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->groupBy('store_location.id')
                                ->get();
        $region_name = '';
        if($manpowerPercentageReport && isset($manpowerPercentageReport[0])){
            $region_name = $manpowerPercentageReport[0]->name;
        }
        return view('regional.report.manpowerpercentage',compact('manpowerPercentageReport','region_name'));
    }

    public function hiringinprocessStore(Request $request)
    {
		view()->share('route', 'hiring-inprocess');
        return view('regional.report.hiringprocessstore');
    }
    public function hiringinprocessStoredatatable(Request $request,$region_id)
    {
        $stores = StoreLocation::where('region_id',$region_id)->where('status',1)->get();
        $store_side_pendings = StoreLocation::selectRaw('store_location.id as store_id,count(employee.id) as total_pending')
                                ->leftjoin('employee','employee.store_id','store_location.id')
                                ->where('store_location.region_id',$region_id)
                                ->whereNotIn('employee.hrms_status',[2])
                                ->whereIn('employee.store_status',[3,0])
                                ->whereNotIn('employee.regional_status',[2])
                                ->whereIN('employee.status',['pending','approve'])
                                ->groupBy('employee.store_id')
                                ->get();
        $region_side_pendings = StoreLocation::selectRaw('store_location.id as store_id,count(employee.id) as total_pending')
                                ->leftjoin('employee','employee.store_id','store_location.id')
                                ->where('store_location.region_id',$region_id)
                                ->whereNotIn('employee.hrms_status',[2])
                                ->whereIn('employee.store_status',[1])
                                ->whereNotIn('employee.store_status',[3])
                                ->whereNotIn('employee.regional_status',[2])
                                ->whereIN('employee.status',['pending','approve'])
                                ->groupBy('employee.store_id')
                                ->get();
        $hrms_side_pendings = StoreLocation::selectRaw('store_location.id as store_id,count(employee.id) as total_pending')
                                ->leftjoin('employee','employee.store_id','store_location.id')
                                ->where('store_location.region_id',$region_id)
                                ->where('employee.hrms_status',1)
                                ->whereIn('employee.regional_status',[2])
                                ->whereIN('employee.status',['pending','approve'])
                                ->whereNotIn('employee.hrms_status',[2,3])
                                ->groupBy('employee.store_id')
                                ->get();

        $store_report_data = array();
        if ($stores) {
            $data = array();
            foreach ($stores as $store) {
                $data['io_code'] = $store->io_code;
                $data['store_name'] = $store->store_name;
                $data['store_id'] = $store->id;
                $store_id = $store->id;
                $store_side_pending = $store_side_pendings->where('store_id',$store_id)->first();
                $region_side_pending = $region_side_pendings->where('store_id',$store_id)->first();
                $hrms_side_pending = $hrms_side_pendings->where('store_id',$store_id)->first();

                $data['store_pending'] = ($store_side_pending)?$store_side_pending->total_pending:0;
                $data['region_pending'] = ($region_side_pending)?$region_side_pending->total_pending:0;
                $data['hrms_pending'] = ($hrms_side_pending)?$hrms_side_pending->total_pending:0;
                $store_report_data[] = $data;
            }

        }
        return Datatables::of($store_report_data)
            ->make(true);
    }

     public function genderwiseattritionstore($region_id,$request)
    {
        $stores = StoreLocation::selectRaw('store_location.*')
                                ->Join('region','region.id','store_location.region_id')
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->get();
        $storesSecond = StoreLocation::selectRaw('store_location.*')
                                ->Join('region','region.id','store_location.region_id')
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->get()
                                ->pluck('id');                   
        if($request->get('start_date') != '' && $request->get('end_date') != ''){
            $previoseDate = $request->get('start_date');
            $currentDate = $request->get('end_date');
        }else{
            $currnetYear = date('Y');
            $previouseYear = date('Y')-1;
            $current_month = date('m');
            if($current_month >= 4){
                $previouseYear = date('Y');
                $currnetYear = date('Y')+1;
            }
            $previoseDate = date($previouseYear.'-04-01');
            $currentDate = date($currnetYear.'-04-01');
        }
        
        $gendermale = Employee::selectRaw('count(employee.id) as total_employee,employee.store_id')
            ->where('employee.processstatus',"Inactive")
            ->where('employee.gender','male')
            ->whereIN('employee.rm_absconding_status',['2','3'])
            ->where(function($query) use ($previoseDate,$currentDate) { 
                
                $query->whereBetween('employee.absconding_time', [$previoseDate,$currentDate]);
            })
            ->whereIN('employee.store_id',$storesSecond)
            ->groupby('employee.store_id')
            ->get()->pluck('total_employee','employee.store_id');
                                
		$gendermaleResignation = \App\Resignation::SelectRaw("count(employee.id) as total_employee,employee.store_id")
            ->join('employee','employee.id','resignation.employee_id')
            ->where(function($query) use ($previoseDate,$currentDate) { 
                        $query->whereBetween('employee.date_of_leave', [$previoseDate,$currentDate]);
                        $query->orwhereBetween('resignation.last_working_date',[$previoseDate,$currentDate]);
                })
            ->where('employee.gender','male')
            ->whereNotIn('employee.rm_absconding_status',['1','2','3'])
            ->where('employee.processstatus',"Inactive")
            ->whereIN('employee.store_id',$storesSecond)
            ->groupby('employee.store_id')
            ->where('resignation.resignation_status',"complete")
                ->get()->pluck('total_employee','employee.store_id');
        $genderFemaleResignation = \App\Resignation::SelectRaw("count(employee.id) as total_employee,employee.store_id")
            ->join('employee','employee.id','resignation.employee_id')
            ->where(function($query) use ($previoseDate,$currentDate) { 
                        $query->whereBetween('resignation.last_working_date', [$previoseDate,$currentDate]);
                })
            ->where('employee.gender','female')
            ->whereNotIn('employee.rm_absconding_status',['1','2','3'])
            ->where('employee.processstatus',"Inactive")
            ->whereIN('employee.store_id',$storesSecond)
            ->groupby('employee.store_id')
            ->where('resignation.resignation_status',"complete")
             ->get()->pluck('total_employee','employee.store_id');
        $genderfemale = Employee::selectRaw('count(employee.id) as total_employee,employee.store_id')
            ->where('employee.processstatus',"Inactive")
            ->where('employee.gender','female')
            ->whereIN('employee.rm_absconding_status',['2','3'])
            ->whereIN('employee.store_id',$storesSecond)
            ->where(function($query) use ($previoseDate,$currentDate) { 
                $query->whereBetween('employee.date_of_leave',  [$previoseDate,$currentDate]);
                $query->orwhereBetween('employee.absconding_time', [$previoseDate,$currentDate]);
            })
            ->groupby('employee.store_id')
             ->get()->pluck('total_employee','employee.store_id');
        foreach($stores as $store){
            $male = isset($gendermale[$store->id])? $gendermale[$store->id] : 0;
            $maleResignation = isset($gendermaleResignation[$store->id])? $gendermaleResignation[$store->id] : 0;
            $female = isset($genderfemale[$store->id])? $genderfemale[$store->id] : 0;
            $femaleResignation = isset($genderFemaleResignation[$store->id])? $genderFemaleResignation[$store->id] : 0;
            $report_data[$store->id]['store_name'] = $store->store_name;
            $report_data[$store->id]['io_code'] = $store->io_code;
            $report_data[$store->id]['store_id'] = $store->id;
            $report_data[$store->id]['male'] = $male + $maleResignation;
            $report_data[$store->id]['female'] = $female + $femaleResignation;

        }
        
        return $report_data;
    }

     public function genderwisehiringstore($region_id,$request)
    {
        $stores = StoreLocation::selectRaw('store_location.*')
                                ->Join('region','region.id','store_location.region_id')
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->get();
         if($request->get('start_date') != '' && $request->get('end_date') != ''){
            $previoseDate = $request->get('start_date');
            $currentDate = $request->get('end_date');
        }else{
            $currnetYear = date('Y');
            $previouseYear = date('Y')-1;
            $current_month = date('m');
            if($current_month >= 4){
                $previouseYear = date('Y');
                $currnetYear = date('Y')+1;
            }
            $previoseDate = date($previouseYear.'-04-01');
            $currentDate = date($currnetYear.'-04-01');
        }
        $report_data = array();
        foreach($stores as $store){
            $gendermale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.status','confirm')
                                    ->where('employee.gender','male')
                                    ->whereBetween('employee.joining_date', [$previoseDate,$currentDate])
                                    ->where('employee.store_id',$store->id)
                                    ->first();
            $genderfemale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.status','confirm')
                                    ->where('employee.gender','female')
                                    ->whereBetween('employee.joining_date', [$previoseDate,$currentDate])
                                    ->where('employee.store_id',$store->id)
                                    ->first();
            $report_data[$store->id]['io_code'] = $store->io_code;
            $report_data[$store->id]['store_name'] = $store->store_name;
            $report_data[$store->id]['store_id'] = $store->id;
            $report_data[$store->id]['male'] = $gendermale->total_employee;
            $report_data[$store->id]['female'] = $genderfemale->total_employee;

        }
        return $report_data;
    }


    public function genderwisetotalemployeestore($region_id)
    {
        $stores = StoreLocation::selectRaw('store_location.*')
                                ->Join('region','region.id','store_location.region_id')
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->get();
        $report_data = array();
        foreach($stores as $store){
            $gendermale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->where('employee.status','confirm')
                                    ->where('employee.gender','male')
                                    ->where('employee.store_id',$store->id)
                                    ->first();
            $genderfemale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->where('employee.status','confirm')
                                    ->where('employee.gender','female')
                                    ->where('employee.store_id',$store->id)
                                    ->first();
            $report_data[$store->id]['io_code'] = $store->io_code;
            $report_data[$store->id]['store_name'] = $store->store_name;
            $report_data[$store->id]['store_id'] = $store->id;
            $report_data[$store->id]['male'] = $gendermale->total_employee;
            $report_data[$store->id]['female'] = $genderfemale->total_employee;

        }
        return $report_data;
    }

    public function storeadditionalReports(Request $request)
    {
		view()->share('route', 'additional');
        $region_id = Auth::user()->region;
        $genderwiseattrition = $this->genderwiseattritionstore($region_id,$request);
        $genderwisehiring = $this->genderwisehiringstore($region_id,$request);
        $genderwisetotalemployee = $this->genderwisetotalemployeestore($region_id);
        return view('regional.report.additionalreportstore',compact('genderwiseattrition','genderwisehiring','genderwisetotalemployee'));
    }


}
