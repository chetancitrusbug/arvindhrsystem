<?php

namespace App\Http\Controllers\Regional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use Session;
use App\Employee;
use App\Resignation;
use App\User;
use App\Log;
use Carbon\Carbon as Carbon;
use App\Helper;
use App\ResignationReason;

use Auth;

class ResignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    protected $employee_notification;
    function __construct()
    {
       // view()->share('route', 'separation');
      //  view()->share('module', 'Separation');
    }

    public function index(Request $request)
    {
        $reason_parent = ResignationReason::where('parent_id',0)
                                    ->where('status','Active')
                                    ->pluck('reason','id')->toArray();
		return view('regional.resignation.index',compact('reason_parent'));
    }

    public function datatable(Request $request)
    {
        $employee = Resignation::select(['employee.*','resignation.id','resignation.last_working_date','resignation.date_of_resignation','resignation.resignation_status','resignation.agreement_copy',
				'resignation.created_at'])
                ->join('employee','employee.id','=','resignation.employee_id')
                ->leftJoin('store_location','store_location.id','employee.store_id')
                ->where('store_location.region_id',Auth::user()->region)
                ->where('store_location.status',1)
                ->whereNULL('store_location.deleted_at')
                ->orderBy('resignation.updated_at','DESC');

        $employee->where('resignation.resignation_status',"pending");
        $employee->orwhere('resignation.resignation_status',"documentation-reg");

		$employee = $employee->get();
        return Datatables::of($employee)
            ->make(true);
    }



	public function update($id, Request $request)
    {
		$result = array();

        $this->validate($request, [
            'resignation_status' => 'required',
            'rid' => 'required',

        ]);

		$item = Resignation::where("id",$request->rid)->first();
        $requestData = $request->except(['rid']);
		$requestData['approve_by'] = Auth::user()->id;
		if($item && $item->employee && $item->employee->region_id == Auth::user()->region){

			if($item){
                $item->update($requestData);
                if($requestData['resignation_status'] == 'decline')
                {
                    $store = \App\StoreLocation::find($item->employee->store_id);
                    if($store){
                        $store->actual_man_power = $store->actual_man_power + 1;
                        $store->save();
                    }
                }
				$result['message'] = "Request processed.";
				$result['code'] = 200;

			}else{
				$result['message'] = "Something went wrong please try again";
				$result['code'] = 400;
			}

			if($request->ajax()){
				return response()->json($result, $result['code']);
			}else{
				if($result['code'] == 400){
					Session::flash('flash_error',$result['message']);
				}else{
					Session::flash('flash_message',$result['message']);
				}

				return redirect()->back();
			}
        }else{
			Session::flash('flash_error',"Access denied..");
		}
		return redirect()->back();
    }

	public function destroy($id,Request $request)
    {
        $item = Resignation::where("id",$id)->first();
        if($item && $item->employee && $item->employee->region_id == Auth::user()->region){
        //	Employee::whereId($item->employee_id)->update(["resignation_status"=>"non"]);
            $store = StoreLocation::find($item->employee->store_id);
            if($store){
                $store->actual_man_power = $store->actual_man_power + 1;
                $store->save();
            }
            $item->delete();
            $result['message'] = "Resignation request cancelled .";
            $result['code'] = 200;

        }else{
            $result['message'] = "something went wrong..";
            $result['code'] = 400;
        }

        if($request->ajax()){
            return response()->json($result, $result['code']);
        }else{
            //Session::flash('flash_message',$result['message']);
           // return redirect('admin/module-units');
        }
    }
    public function resignationLetterDownload($id)
    {
		$result = array();

        $resignation = Resignation::whereId($id)->first();
		$employee = null;

        $reason_parent = ResignationReason::where('parent_id',0)
                                    ->where('status','Active')
                                    ->pluck('reason','id')->toArray();

		if($resignation && $resignation->employee){
			$employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($resignation->employee_id);

			if($resignation->resignation_status == "documentation" || $resignation->resignation_status == "complete" || $resignation->resignation_status == "documentation-reg" ){
				$fd = json_decode($resignation->resignation_form,true);
				//dd($form_data);
				return view('Regional.resignation.print',compact('resignation','employee','fd'));
			}else{
                return redirect('regional/resignation');
			//	return view('Regional.resignation.letter-form',compact('resignation','employee','reason_parent'));
			}
		}else{
			Session::flash('flash_error',"Access denied..");
			return redirect()->back();
		}

    }
    public function updateRegignation(Request $request)
    {
        $result = array();
        $this->validate($request, [
            'document' => 'required|mimes:jpeg,bmp,png,gif,svg,pdf',
            'resignation_status' => 'required',
            'rid' => 'required',
        ]);


		$item = Resignation::where("resignation.id",$request->rid)->first();

		if($item){
			if ($request->hasFile('document')) {
				$name = $this->uploadFile($request);
				if($name){
                    $item->reg_agreement_copy = $name;
				}
            }

			$item->resignation_status = $request->resignation_status;
            $item->save();

            /*
			if($request->has('resignation_status') && $request->resignation_status == "complete"){
				Session::flash('flash_success',"Employee Relieved..");
				$employee = $item->employee;
				$employee->delete();
			}
			if($request->has('resignation_status') && $request->resignation_status == "decline"){
				Session::flash('flash_success',"Resignation request declined..");
				$item->delete();
            } */
		}else{

			Session::flash('flash_error',"Access denied..");
        }

		return redirect()->back();
    }
    public function uploadFile(Request $request)
    {
        $name = null;
        if ($request->hasFile('document')) {
            $file = $request->file('document');
            $timestamp = uniqid();
            $name = $timestamp.'.'.$file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/resignation',$name);
		}
		return $name;
    }

}
