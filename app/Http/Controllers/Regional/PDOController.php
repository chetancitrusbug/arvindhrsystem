<?php

namespace App\Http\Controllers\Regional;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PDO;

class PDOController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'employee_id' => 'required',
            "pdo_date"=>"required|date_format:Y-m-d",
            'pdo_number' => "required",
            'courier_name' => "required",
        ]);

        $pdo = new PDO();
        $pdo->employee_id = $request->employee_id;
        $pdo->pdo_date = $request->pdo_date;
        $pdo->pdo_number = $request->pdo_number;
        $pdo->courier_name = $request->courier_name;
        $pdo->save();
        if($pdo){
            \Session::flash('success_message','PDO detailed added successfully.');
            return redirect()->back();
        }
        \Session::flash('error_message','PDO detailed added successfully.');
        return redirect()->back();
    }

    public function GetPDODetail($pdo_id)
    {
        $pdo['pdo'] = PDO::find($pdo_id);

        if($pdo['pdo']){
            $employee = \App\Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($pdo['pdo']->employee_id);
            $pdo['employee'] = $employee;

            return ["status"=>true,'data'=>$pdo];
        }
        return ["status"=>false,'data'=>[]];
    }
}
