<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Yajra\Datatables\Datatables;
use DB;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\SourceDetail;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use Carbon\Carbon as Carbon;
use App\HiringReport;
use App\Resignation;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

		/*$employees = Employee::select([
                    'store_id','absconding_status','rm_absconding_status','absconding_time'
                ])
				->whereIn('employee.rm_absconding_status',["3"])
                ->whereIn('employee.absconding_status',["1","2"])
                ->orderBy('employee.absconding_time','DESC')
                ->get(); 
		foreach($employees as $employee){
			$hiringReports = HiringReport::where('store_id',$employee->store_id)->where('monthyear',date('m-Y',strtotime($employee->absconding_time)))->first();
			if($hiringReports != null){
				$hiringReports->absconding = $hiringReports->absconding + 1;
				$hiringReports->save();
				
			}else{
				$hiringInsert = HiringReport::insert(array('store_id' => $employee->store_id,'monthyear' => date('m-Y',strtotime($employee->absconding_time)), 'absconding' => 1 ));
			}
		}	 */
	/*	
		$employees = Resignation::select(['employee.store_id','date_of_resignation',])->where('resignation.resignation_status','approved')->join('employee','employee.id','resignation.employee_id')->get(); 
		foreach($employees as $employee){
			$hiringReports = HiringReport::where('store_id',$employee->store_id)->where('monthyear',date('m-Y',strtotime($employee->date_of_resignation)))->first();
			if($hiringReports != null){
				$hiringReports->resignation = $hiringReports->resignation + 1;
				$hiringReports->save();
				
			}else{
				$hiringInsert = HiringReport::insert(array('store_id' => $employee->store_id,'monthyear' => date('m-Y',strtotime($employee->date_of_resignation)), 'resignation' => 1 ));
			}
		}
		*/
		

        /*if (Auth::user()->hasRole('SU')) {
            return redirect('/admin/store-location');
        } else if (Auth::user()->hasRole('SM')) {
            return redirect('/store/hiring');
        } else if (Auth::user()->hasRole('HRMS')) {
            return redirect('/hrms/hiring');
        } else if (Auth::user()->hasRole('RM')) {
            return redirect('/regional/hiring');
    }*/ /*
		ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
       $allEmployee = Employee::selectRaw("employee.id, employee.employee_code AS Employee_Code,
                                employee.`full_name` AS Employee_Name,
                                employee.`gender`,
                                employee.`date_of_birth`,
                                employee.`joining_date` AS DOJ,
                                employee.`date_of_leave` AS DOL,
                                CONCAT_WS(' ', employee.`current_address_line_1`,employee.`current_address_line_2`,employee.`current_address_line_3`, employee.`current_city`, employee.`current_pincode`,employee.`current_state`) AS Current_Address,
                                CONCAT_WS(' ', employee.`permanent_address_line_1`,employee.`permanent_address_line_2`,employee.`permanent_address_line_3`,employee.`permanent_city`,employee.`permanent_pincode`,employee.`permanent_pincode`) AS Parmenent_Address,
                                employee.processstatus,
                                employee.status,
                                employee.payroll_type,
                                employee.company,
                                employee.`aadhar_number`,
                                employee.`aadhar_enrolment_number`,
                                employee.`pan_number`,
                                employee.`emergency_person`,
                                employee.`emergency_number`,
                                employee.`emergency_relationship`,
                                employee.`blood_group`,
                                employee.`marital_status`,
                                employee.`nationality`,
                                employee.`mobile` AS Mobile_Number,
                                employee.`email` AS Personal_email,
                                employee.`esic_number`,
                                employee.`anniversary_date`,
                                employee.`ua_number`,
								grade.`name` as grade_name,
                                designation.name AS DESIGNATION_NAME,
                                department.name AS Department_name,
                                sub_department.name AS Sub_Department_name,
                                other_infos.appointment_status AS Appointment_Latter,
                                other_infos.id_card_status AS IDCard_Status,
                                other_infos.uniform_status AS uniform_status,
                                other_infos.l0l1_status,
                                other_infos.l0l1_certificate_status,
                                store_location.io_code AS Store_IO_CODE,
                                store_location.store_name,
                                store_location.sap_code AS Store_Code,
                                city.`name` AS City,
                                region.`name` AS Region,
                                state.`name` AS State,
                                employee.`bank_name`,
                                employee.`account_no` AS Bank_Account_no,
                                employee.`ifsc`,
                                employee.`branch_name`
                                "
                            )
                    ->leftJoin('designation','designation.id','employee.designation_id')
                    ->leftJoin('department','department.id','employee.department_id')
                    ->leftJoin('sub_department','sub_department.id','employee.sub_department_id')
                    ->leftJoin('other_infos','other_infos.employee_id','employee.id')
                    ->leftJoin('grade','grade.id','employee.grade_id')
                    ->leftJoin('store_location','store_location.id','employee.store_id')
                    ->leftJoin('region','region.id','store_location.region_id')
                    ->leftJoin('city','city.id','store_location.city_id')
                    ->leftJoin('state','state.id','city.state_id')
                    ->where('employee.processstatus','!=','Inactive')
                 //   ->where('employee.hrms_status',"=","2")
                    ->where('store_location.status','=','1')
					->groupby('employee.id')
					//->limit(1)
					//->withTrashed()
                    ->get();
                    $excel_export = array();
                $excel_export['header'] = array('Employee Code','Employee Name',"Gender","Date Of Birth","DOJ","DOL","Current Address","Parmenent Address","Employee Status","System Status","Payroll Type","Company","Aadhar Number", "Aadhar Enrolment Number","Pan Number", "Emergency Person","Emergency Number","Emergency Relationship","Blood Group","Marital Status","Nationality","Mobile Number","Personal Email","ESIC Number","Anniversary Date","UA Number","Higher Qualification","Qualification Name","Board/Uni","Institute","Specialization","Month & Year of passing","Father Name","Mother Name","Spouse Name","Child Name 1","Child Name 2","Grade Name","DESIGNATION NAME","Department Name","Sub Department Name","Appointment Latter","ID Card Status","Uniform Status","L0L1 status","L0L1 Certificate Status", "Store IO CODE","Store Name","Store Code","City","Region","State","Bank Name","Bank Account No","IFSC","Branch Name");
                $education_details = EducationDetail::selectRaw('education_detail.*, education_category.name as qualification_name')
                                                        ->leftJoin('education_category','education_category.id','education_detail.education_category_id')
                                                        ->orderBy('year','DESC')
                                                        ->get();
                    $family_detail = FamilyInfo::all();
                foreach ($allEmployee as $employee) {
                    $education_detail = $education_details->where('employee_id',$employee->id)->first();
                    $family_details = $family_detail->where('employee_id',$employee->id);

                    $excel_export[$employee->id]['Employee_Code'] = $employee->Employee_Code;
                    $excel_export[$employee->id]['Employee_Name'] = $employee->Employee_Name;
                    $excel_export[$employee->id]['gender'] = $employee->gender;
                    $excel_export[$employee->id]['date_of_birth'] = $employee->date_of_birth;
                    $excel_export[$employee->id]['DOJ'] = $employee->DOJ;
                    $excel_export[$employee->id]['DOL'] = $employee->DOL;
                    $excel_export[$employee->id]['Current_Address'] = $employee->Current_Address;
                    $excel_export[$employee->id]['Parmenent_Address'] = $employee->Parmenent_Address;
                    $excel_export[$employee->id]['processstatus'] = $employee->processstatus;
                    $excel_export[$employee->id]['status'] = $employee->status;
                    $excel_export[$employee->id]['payroll_type'] = $employee->payroll_type;
                    $excel_export[$employee->id]['company'] = $employee->company;
                    $excel_export[$employee->id]['aadhar_number'] = $employee->aadhar_number;
                    $excel_export[$employee->id]['aadhar_enrolment_number'] = $employee->aadhar_enrolment_number;
                    $excel_export[$employee->id]['pan_number'] = $employee->pan_number;
                    $excel_export[$employee->id]['emergency_person'] = $employee->emergency_person;
                    $excel_export[$employee->id]['emergency_number'] = $employee->emergency_number;
                    $excel_export[$employee->id]['emergency_relationship'] = $employee->emergency_relationship;
                    $excel_export[$employee->id]['blood_group'] = $employee->blood_group;
                    $excel_export[$employee->id]['marital_status'] = $employee->marital_status;
                    $excel_export[$employee->id]['nationality'] = $employee->nationality;
                    $excel_export[$employee->id]['Mobile_Number'] = $employee->Mobile_Number;
                    $excel_export[$employee->id]['Personal_email'] = $employee->Personal_email;
                    $excel_export[$employee->id]['esic_number'] = $employee->esic_number;
                    $excel_export[$employee->id]['anniversary_date'] = $employee->anniversary_date;
                    $excel_export[$employee->id]['ua_number'] = $employee->ua_number;
					$excel_export[$employee->id]['high_qualification'] = (isset($education_detail->qualification_name))? $education_detail->qualification_name : '';
                    $excel_export[$employee->id]['qualification'] = (isset($education_detail->qualification)) ? $education_detail->qualification : '';
                    $excel_export[$employee->id]['board_uni'] = (isset($education_detail->board_uni))?$education_detail->board_uni:'';
                    $excel_export[$employee->id]['institute'] = (isset($education_detail->institute))?$education_detail->institute:'';
                    $excel_export[$employee->id]['specialization'] = (isset($education_detail->specialization))?$education_detail->specialization:'';
                    $excel_export[$employee->id]['passing_month'] = (isset($education_detail->month) && isset($education_detail->year))? $education_detail->month."-".$education_detail->year : '';
                    $excel_export[$employee->id]['Father_Name'] = '';
                    $excel_export[$employee->id]['Mother_Name'] = '';
                    $excel_export[$employee->id]['Spouse_Name'] = '';
                    $excel_export[$employee->id]["Child_Name_1"] = '';
                    $excel_export[$employee->id]["Child_Name_2"] = '';
                    $child = 1;
                    foreach ($family_details as $family_detail) {
                        if($family_detail->relation == 'Father'){
                            $excel_export[$employee->id]['Father_Name'] = $family_detail->name;
                        } elseif ($family_detail->relation == 'Mother') {
                            $excel_export[$employee->id]['Mother_Name'] = $family_detail->name;
                        } elseif ($family_detail->relation == 'Spouse') {
                            $excel_export[$employee->id]['Spouse_Name'] = $family_detail->name;
                        } elseif ($family_detail->relation == 'Child' && $child < 2) {
                            $excel_export[$employee->id]["Child_Name_$child"] = $family_detail->name;
                            $child++;
                        }
                    }
					$excel_export[$employee->id]['grade_name'] = $employee->grade_name;
                    $excel_export[$employee->id]['DESIGNATION_NAME'] = $employee->DESIGNATION_NAME;
                    $excel_export[$employee->id]['Department_name'] = $employee->Department_name;
                    $excel_export[$employee->id]['Sub_Department_name'] = $employee->Sub_Department_name;
                    $excel_export[$employee->id]['Appointment_Latter'] = $employee->Appointment_Latter;
                    $excel_export[$employee->id]['IDCard_Status'] = $employee->IDCard_Status;
                    $excel_export[$employee->id]['uniform_status'] = $employee->uniform_status;
                    $excel_export[$employee->id]['l0l1_status'] = $employee->l0l1_status;
                    $excel_export[$employee->id]['l0l1_certificate_status'] = $employee->l0l1_certificate_status;
                    $excel_export[$employee->id]['Store_IO_CODE'] = $employee->Store_IO_CODE;
                    $excel_export[$employee->id]['store_name'] = $employee->store_name;
                    $excel_export[$employee->id]['Store_Code'] = $employee->Store_Code;
                    $excel_export[$employee->id]['City'] = $employee->City;
                    $excel_export[$employee->id]['Region'] = $employee->Region;
                    $excel_export[$employee->id]['State'] = $employee->State;
                    $excel_export[$employee->id]['bank_name'] = $employee->bank_name;
                    $excel_export[$employee->id]['Bank_Account_no'] = $employee->Bank_Account_no;
                    $excel_export[$employee->id]['ifsc'] = $employee->ifsc;
                    $excel_export[$employee->id]['branch_name'] = $employee->branch_name;

                }

                \Excel::create("All Employee - ". date('d-m-Y H-i-s'),function($excel) use ($excel_export){
                $excel->setTitle('All Employee');
                $excel->sheet('All Employee',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                });
            })->download('xlsx');

		exit;
*/
        return view('home'); 
    }

    public function GetLoginUser($user_type)
    {
        $user_type = strtoupper($user_type);
        $role = \App\Role::where('name',$user_type)->first();
        $userdata = \App\User::select([
                            'users.*',
                            'region.name as region',
                            'state.name as state',
                            'city.name as city',
                            'store_location.io_code',
                            'store_location.sap_code',
                            'store_location.store_name'
                        ])
                        ->with('roles')->join('role_user', 'role_user.user_id', '=', 'users.id')->where('role_user.role_id', $role->id)
                        ->leftjoin('region','region.id','users.region')
                        ->leftjoin('store_location','store_location.id','users.store_id')
                        ->leftjoin('state','state.id','store_location.state_id')
                        ->leftjoin('city','city.id','store_location.city_id')
                        ->groupBy('users.id')
                        ->orderBy('users.id','DESC')
                        ->get();
            return $userdata;
    }

    public function GetAllLogin()
    {
        $super_user =  $this->GetLoginUser('SU');
        $store_manager =  $this->GetLoginUser('SM');
        $regional_manager =  $this->GetLoginUser('RM');
        $hrms_manager =  $this->GetLoginUser('HRMS');
        $admin_excel_export = array();
            $admin_excel_export['header'] = array("User Code","User Name","Email","Password","Status");
            foreach ($super_user as $user) {
                $admin_excel_export[$user->id]['User_Code'] = $user->emp_id;
                $admin_excel_export[$user->id]['emp_name'] = $user->emp_name;
                $admin_excel_export[$user->id]['email'] = $user->email;
                $admin_excel_export[$user->id]['password'] = "123456";
                $admin_excel_export[$user->id]['status'] = $user->status;
            }


            $hrms_excel_export['header'] = array("User Code","User Name","Email","Password","Status");
            foreach ($hrms_manager as $user) {
                $hrms_excel_export[$user->id]['User_Code'] = $user->emp_id;
                $hrms_excel_export[$user->id]['emp_name'] = $user->emp_name;
                $hrms_excel_export[$user->id]['email'] = $user->email;
                $hrms_excel_export[$user->id]['password'] = "123456";
                $hrms_excel_export[$user->id]['status'] = $user->status;
            }

            $regional_manager_excel_export['header'] = array("User Code","User Name","Email","Password",'Region',"Status");
            foreach ($regional_manager as $user) {
                $regional_manager_excel_export[$user->id]['User_Code'] = $user->emp_id;
                $regional_manager_excel_export[$user->id]['emp_name'] = $user->emp_name;
                $regional_manager_excel_export[$user->id]['email'] = $user->email;
                $regional_manager_excel_export[$user->id]['password'] = "123456";
                $regional_manager_excel_export[$user->id]['region'] = $user->region;
                $regional_manager_excel_export[$user->id]['status'] = $user->status;
            }

            $store_manager_excel_export['header'] = array("User Code","User Name","Email","Password","Region Name","Store Name","Store City","Store State","Status");
            foreach ($store_manager as $user) {
                $store_manager_excel_export[$user->id]['User_Code'] = $user->emp_id;
                $store_manager_excel_export[$user->id]['emp_name'] = $user->emp_name;
                $store_manager_excel_export[$user->id]['email'] = $user->email;
                $store_manager_excel_export[$user->id]['password'] = "123456";
                $store_manager_excel_export[$user->id]['region'] = $user->region;
                $store_manager_excel_export[$user->id]['store_name'] = $user->store_name;
                $store_manager_excel_export[$user->id]['city'] = $user->city;
                $store_manager_excel_export[$user->id]['state'] = $user->state;
                $store_manager_excel_export[$user->id]['status'] = $user->status;
            }
            $excel_export = array();
            $excel_export['store_login'] = $store_manager_excel_export;
            $excel_export['regional_login'] = $regional_manager_excel_export;
            $excel_export['hrms_login'] = $hrms_excel_export;
            $excel_export['admin'] = $admin_excel_export;

            \Excel::create("All Login User - ". date('d-m-Y H-i-s'),function($excel) use ($excel_export){
            $excel->setTitle('All Login User');
            $excel->sheet('Admin',function($sheet) use ($excel_export){
                $sheet->fromArray($excel_export['admin'],null,'A1',false,false);
            });
            $excel->sheet('HRMS Login', function($sheet) use ($excel_export) {
                $sheet->fromArray($excel_export['hrms_login'],null,'A1',false,false);
            });
            $excel->sheet('Regional Login', function($sheet) use ($excel_export) {
                $sheet->fromArray($excel_export['regional_login'],null,'A1',false,false);
            });
            $excel->sheet('Store Login', function($sheet) use ($excel_export) {
                $sheet->fromArray($excel_export['store_login'],null,'A1',false,false);
            });
        })->download('xlsx');
        return redirect()->back();
    }
}
