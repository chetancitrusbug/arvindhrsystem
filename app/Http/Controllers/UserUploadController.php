<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserUpload;
use Carbon\Carbon;
use App\IdeaCategory;
use App\Employee;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserUploadController extends Controller
{
    //
    public function showForm(Request $request)
    {
        $idea_category = array();
        $idea_category = IdeaCategory::where('status','1')
                        ->pluck('cat_name','id')
                        ->toArray();
        return view('user_upload',compact('idea_category'));
    }

    public function upload_files(Request $request)
    {
        $this->validate($request, [
            'document' => 'max:1999',
            'employee_code' => 'required|numeric',
            'employee_name' => 'required',
            'employee_email' => 'required|email',
            'expected_out_come' => 'required',
            'idea_category' => 'required',
            'description' => 'required',

        ]);
        if ($request->hasFile('document')) {
            $request->fileuploadPath = $this->uploadFile($request);
        }
        $uploadfile = new UserUpload();
        $uploadfile->employee_code = $request->employee_code;
        $uploadfile->idea_name = $request->idea_name;
        $uploadfile->idea_category = $request->idea_category;
        $uploadfile->description = $request->description;
        $uploadfile->expected_out_come = $request->expected_out_come;
        $uploadfile->fileuploadPath = $request->fileuploadPath;
        $uploadfile->ip_address = $request->ip();
        $uploadfile->save();

  /*      $employee = Employee::where('employee_code',$request->employee_code)->first(); */

            $data = array('name'=>"Arvind HR");

            Mail::send('email.thankyou', compact('employee'), function ($message) use ($request) {
            $message->from('hr@arvind.com', 'Arvind HR')
                        ->sender('hr@arvind.com', 'Arvind HR')
                        ->to($request->employee_email, $request->employee_name)
                        ->subject('Thank you For Suggestion');
            });

        return redirect('thankyou');
    }
    public function uploadFile(Request $request)
    {
        $name = null;
        $current_time =date('d/m/Y');
       // $current_time = Carbon::now()->toDateTimeString();
        if ($request->hasFile('document')) {
            $file = $request->file('document');
            $timestamp = $current_time.uniqid();
            $name = $timestamp.'.'.$file->getClientOriginalExtension();
            $file->move(public_path() . '/uploads/Userfile/',$name);
		}
		return $name;
    }

    public function thankyou(Request $request)
    {
        return view('thankyou');
    }

    public function getuserdata(Request $request,$employee_code)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->where('employee_code',$employee_code)->first();
        if($employee){
                $employee->toJson();
                echo $employee;
        } else {
            echo "";
        }
    }
}
