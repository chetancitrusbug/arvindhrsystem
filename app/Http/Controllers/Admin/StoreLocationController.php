<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use App\User;
use App\Role;
use App\State;
use App\City;
use App\Region;
use App\Brand;
use App\StoreLocation;
use App\LegalEntity;
use App\Rules\Unique;
use App\StoreCollection;
use Mail;

use Yajra\Datatables\Datatables;

class StoreLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.storelocation');
        $this->middleware('permission:access.storelocation.edit')->only(['edit','update']);
        $this->middleware('permission:access.storelocation.create')->only(['create', 'store']);
        $this->middleware('permission:access.storelocation.delete')->only('destroy');

        view()->share('route', 'store_location');
        view()->share('module', 'Store Location');
    }

    public function index(Request $request)
    {
    	return view('admin.store_location.index');
    }

    public function datatable(Request $request)
    {
        $storeLocation = StoreLocation::select([
                            'store_location.*',
                            \DB::raw("CONCAT(io_code,' (',sap_code,')') as store_code"),
                            'state.name as state',
                            'region.name as region',
                            'city.name as city',
                            'brand.name as brand',
                        ])
                        ->leftjoin('state',function($join){
                            $join->on('state.id','=','store_location.state_id');
                        })
                        ->leftjoin('city',function($join){
                            $join->on('city.id','=','store_location.city_id');
                        })
                        ->leftjoin('region',function($join){
                            $join->on('region.id','=','store_location.region_id');
                        })
                        ->leftjoin('brand',function($join){
                            $join->on('brand.id','=','store_location.brand_id');
                        })
                        ->orderBy('store_location.id','DESC')
                        ->get();

        return Datatables::of($storeLocation)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $city = [];
        $brands = Brand::where("status","1")->pluck('name','id')->toArray();
        view()->share('brands',$brands);

        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        $region = Region::where("status","1")->pluck('name','id')->toArray();
        view()->share('region',$region);

        if(old('state_id',null)){
            $city = $this->getCityFromState($request,['state_id'=>old('state_id',null)]);
        }
        view()->share('city',$city);

        $legalEntity = LegalEntity::pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        return view('admin.store_location.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;

        $storeLocation = StoreLocation::create(array_except($requestData,['attach']));

        $user['emp_id'] = $requestData['emp_id'];
        $user['store_id'] = $storeLocation->id;
        $user['emp_name'] = $requestData['emp_name'];
        $user['email'] = $requestData['store_email'];
        $user['mobile_no'] = $requestData['mobile_no'];
        $user['region'] = $requestData['region_id'];
        $user['state'] = $requestData['state_id'];
        $user['city'] = $requestData['city_id'];
        $user['pincode'] = $requestData['pincode'];
        $user['internal_order'] = $requestData['internal_order'];
        $user['brand_id'] = $requestData['brand_id'];
        $user['legal_entity_id'] = $requestData['legal_entity_id'];
        // $user['password'] = bcrypt($requestData['store_password']);
        $user['status'] = 1;

        $user = User::create($user);

        $role=Role::where('name','SM')->first();

        $role = \DB::table('role_user')->insert(['role_id'=>$role->id,'user_id'=>$user->id]);

        return redirect('admin/store-location')->with('flash_success', 'Store Location added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $storeLocation = StoreLocation::with(['brand','state','city','region'])->find($id);

        if(!$storeLocation){
            return redirect('admin/store-location')->with('flash_error', 'Store Location Not Found!');
        }

        return view('admin.store_location.show', compact('storeLocation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $storeLocation = StoreLocation::select([
                                'store_location.*',
                                'users.store_id',
                                'users.id as user_id',
                                'users.emp_name',
                                'users.emp_id',
                                'users.internal_order',
                                'users.legal_entity_id',
                                'users.email as store_email',
                                'users.mobile_no',
                            ])
                            ->leftjoin('users',function($join){
                                $join->on('users.store_id','=','store_location.id');
                            })
                            ->where('store_location.id',$id)->first();

        if(!$storeLocation){
            return redirect('admin/store-location')->with('flash_error', 'Store Location Not Found!');
        }

        $request = new Request();

        $city = [];

        $brands = Brand::where("status","1")->pluck('name','id')->toArray();
        view()->share('brands',$brands);

        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        $region = Region::where("status","1")->pluck('name','id')->toArray();
        view()->share('region',$region);

        if(old('state_id',$storeLocation->state_id)){
            $city = $this->getCityFromState($request,['state_id'=>old('state_id',$storeLocation->state_id)]);
        }
        view()->share('city',$city);

        $legalEntity = LegalEntity::pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        return view('admin.store_location.edit', compact('storeLocation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        $storeLocation = StoreLocation::where('id',$id)->update(array_except($requestData,['_method','_token','store_confirm_password']));

        $user = User::where('store_id',$id)->first();
        $userInsert['emp_id'] = $requestData['emp_id'];
        $userInsert['store_id'] = $id;
        $userInsert['emp_name'] = $requestData['emp_name'];
        $userInsert['email'] = $requestData['store_email'];
        $userInsert['mobile_no'] = $requestData['mobile_no'];
        $userInsert['region'] = $requestData['region_id'];
        $userInsert['state'] = $requestData['state_id'];
        $userInsert['city'] = $requestData['city_id'];
        $userInsert['pincode'] = $requestData['pincode'];
        $userInsert['internal_order'] = $requestData['internal_order'];
        $userInsert['brand_id'] = $requestData['brand_id'];
        $userInsert['legal_entity_id'] = $requestData['legal_entity_id'];
        // if($requestData['store_password']){
        //     $userInsert['password'] = bcrypt($requestData['store_password']);
        // }
        $userInsert['status'] = 1;

        if(!$user){
            $user = User::create($userInsert);

            $role=Role::where('name','SM')->first();

            $role = \DB::table('role_user')->create(['role_id'=>$role->id,'user_id'=>$user->id]);
        }else{
            User::where('id',$user->id)->update(array_except($userInsert,['email']));
        }

        return redirect('admin/store-location')->with('flash_success', 'Store Location updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $storeLocation = StoreLocation::find($id);

        $storeLocation->delete();

        if($request->has('from_index')){
            $message = "Store Location Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Store Location deleted!');

            return redirect('admin/store-location');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'location_code'=>['required','max:191','regex:/^[a-z0-9 ._\-]+$/i',new Unique('store_location','location code',$id)],
            'io_code'=>'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'sap_code'=>'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'store_name'=>'required|max:191|regex:/^[a-z0-9 ._\-]+$/i',
            'brand_id'=>'required',
            'sq_feet'=>'required|numeric',
            'budget_man_power'=>'required|numeric|min:0',
            'address'=>'required',
            'city_id'=>'required',
            'state_id'=>'required',
            'region_id'=>'required',
            'store_email'=>['required','email',function($attribute, $value, $fail) use($request,$id) {
                                $user = User::where('email',$value)->first();
                                if($user && $user->store_id != $id){
                                    $fail('User already exist with this email');
                                }
                            }],
            'emp_name'=>'required',
            'emp_id'=>'required',
            'mobile_no'=>'required',
            'pincode'=>'required',
            'internal_order'=>'required',
            'legal_entity_id'=>'required',
        ];

        if(isset($request->store_password) && $request->store_password != ''){
            $rules['store_password'] = 'required|same:store_confirm_password';
        }

        return $this->validate($request, $rules,[
            'brand_id.required' => 'The brand field is required',
            'state_id.required' => 'The state field is required',
            'city_id.required' => 'The city field is required',
            'region_id.required' => 'The region field is required',
            'sq_feet.required' => 'The square feet field is required',
            'sq_feet.numeric' => 'The square feet must be a number.',
            'emp_name.required' => 'The employee number field is required.',
            'emp_id.required' => 'The employee id field is required.',
            'legal_entity_id.required' => 'The legal entity field is required.',
        ]);
    }

    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function showUpload()
    {
        return view('admin.store_location.upload');
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);
		//dd($request->hasFile('excel'));
        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->get();

            $brand = array_change_key_case(Brand::pluck('id','name')->toArray(), CASE_LOWER);
            $region = array_change_key_case(Region::pluck('id','name')->toArray(), CASE_LOWER);
            $state = array_change_key_case(State::pluck('id','name')->toArray(), CASE_LOWER);
            $city = array_change_key_case(City::pluck('id','name')->toArray(), CASE_LOWER);
            $locationCode = array_change_key_case(StoreLocation::pluck('io_code','io_code')->toArray(), CASE_LOWER);
            $arr = [];
            $user = [];
            if($data->count()){
				//dd($data);
				//$data = $data[0];
				foreach ($data as $key => $value) {
					if(!in_array(caseChange($value->internal_orderio),$locationCode) && $value->internal_orderio != ''){

                        $user[$key]['email'] = trim($value->store_email);
                        $user[$key]['emp_id'] = trim($value->sm_emp_id);
                        $user[$key]['emp_name'] = trim($value->store_manager);
                        $user[$key]['store_email'] = trim($value->store_manager_email_id);
                        $user[$key]['personal_email'] = trim($value->store_manager_email_id);
                        $user[$key]['password'] = Hash::make('123456');
                        $user[$key]['mobile_no'] = trim($value->sm_contact_no);

                        $arr[$key]['status'] = 1;

                        $arr[$key]['location_code'] = trim(caseChange($value->location_code));
                        $arr[$key]['io_code'] = trim($value->internal_orderio);
                        $arr[$key]['sap_code'] = trim($value->store_code);
                        $arr[$key]['store_name'] = trim($value->store_name);
                        $arr[$key]['address'] = $value->store_address;
                        if(isset($value->store_type) && isset($value->store_category)){
                            $arr[$key]['store_type'] = $value->store_type;
                            $arr[$key]['store_category'] = $value->store_category;
                        }
						$locationCode = array_merge([caseChange($value->internal_orderio)=>trim($value->internal_orderio)], $locationCode);

                        if(array_key_exists(caseChange($value->brand),$brand)){
                            $arr[$key]['brand_id'] = $brand[caseChange($value->brand)];
                        }else{
							$brandInsert['name'] = $value->brand;
                            $brandInsert['status'] = 1;
                            $brandCreate = Brand::create($brandInsert);
                            $arr[$key]['brand_id'] = $brandCreate->id;
                            $brand = array_merge([caseChange($value->brand)=>$brandCreate->id], $brand);
                        }

                        if(array_key_exists(caseChange($value->region),$region)){
                            $arr[$key]['region_id'] = $region[caseChange($value->region)];
							$user[$key]['region'] = $region[caseChange($value->region)];
                        }else{
                            $regionInsert['name'] = $value->region;
                            $regionInsert['status'] = 1;
                            $regionCreate = Region::create($regionInsert);
                            $arr[$key]['region_id'] = $regionCreate->id;
                            $region = array_merge([caseChange($value->region)=>$regionCreate->id], $region);
							$user[$key]['region'] = $regionCreate->id;
                        }

                        if(array_key_exists(caseChange($value->state),$state)){
                            $arr[$key]['state_id'] = $state[caseChange($value->state)];
							$user[$key]['state'] = $state[caseChange($value->state)];
                        }else{
                            $stateInsert['region_id'] = $arr[$key]['region_id'];
                            $stateInsert['name'] = $value->state;
                            $stateInsert['status'] = 1;
                            $stateCreate = State::create($stateInsert);
                            $arr[$key]['state_id'] = $stateCreate->id;
                            $state = array_merge([caseChange($value->state)=>$stateCreate->id], $state);
							$user[$key]['state'] = $stateCreate->id;
                        }

                        if(array_key_exists(caseChange($value->city),$city)){
                            $arr[$key]['city_id'] = $city[caseChange($value->city)];
							$user[$key]['city'] = $city[caseChange($value->city)];
                        }else{
							if($value->city != ''){
								$cityInsert['state_id'] = $arr[$key]['state_id'];
								$cityInsert['name'] = $value->city;
								$cityInsert['status'] = 1;
								$cityCreate = City::create($cityInsert);
								$arr[$key]['city_id'] = $cityCreate->id;
								$city = array_merge([caseChange($value->city)=>$cityCreate->id], $city);
								$user[$key]['city'] = $cityCreate->id;
							}
                        }

                        $store = StoreLocation::create($arr[$key]);
                        $user[$key]['store_id'] = $store->id;
						$InUserExist = User::where('email',$user[$key]['email'])->first();
						if(!$InUserExist){
							$userCreate = User::create($user[$key]);
							$userCreate->assignRole('SM');
						}else{
							$email = $user[$key]['email'];
							unset($user[$key]['email']);
							User::where('email',$email)->update($user[$key]);
						}


                    }else{
						$user[$key]['email'] = trim($value->store_email);
                        $user[$key]['emp_id'] = trim($value->sm_emp_id);
                        $user[$key]['emp_name'] = trim($value->store_manager);
                        $user[$key]['store_email'] = trim($value->store_manager_email_id);
                        $user[$key]['personal_email'] = trim($value->store_manager_email_id);
                        $user[$key]['password'] = Hash::make('123456');
                        $user[$key]['mobile_no'] = trim($value->sm_contact_no);

                        $arr[$key]['status'] = 1;

                        $arr[$key]['location_code'] = trim(caseChange($value->location_code));
                        //$arr[$key]['io_code'] = trim($value->internal_orderio);
                        $arr[$key]['sap_code'] = trim($value->store_code);
                        $arr[$key]['store_name'] = trim($value->store_name);
                        $arr[$key]['address'] = $value->store_address;
						$locationCode = array_merge([caseChange($value->internal_orderio)=>trim($value->internal_orderio)], $locationCode);

                        if(array_key_exists(caseChange($value->brand),$brand)){
                            $arr[$key]['brand_id'] = $brand[caseChange($value->brand)];
                        }else{
							$brandInsert['name'] = $value->brand;
                            $brandInsert['status'] = 1;
                            $brandCreate = Brand::create($brandInsert);
                            $arr[$key]['brand_id'] = $brandCreate->id;
                            $brand = array_merge([caseChange($value->brand)=>$brandCreate->id], $brand);
                        }

                        if(array_key_exists(caseChange($value->region),$region)){
                            $arr[$key]['region_id'] = $region[caseChange($value->region)];
							$user[$key]['region'] = $region[caseChange($value->region)];
                        }else{
                            $regionInsert['name'] = $value->region;
                            $regionInsert['status'] = 1;
                            $regionCreate = Region::create($regionInsert);
                            $arr[$key]['region_id'] = $regionCreate->id;
                            $region = array_merge([caseChange($value->region)=>$regionCreate->id], $region);
							$user[$key]['region'] = $regionCreate->id;
                        }

                        if(array_key_exists(caseChange($value->state),$state)){
                            $arr[$key]['state_id'] = $state[caseChange($value->state)];
							$user[$key]['state'] = $state[caseChange($value->state)];
                        }else{
                            $stateInsert['region_id'] = $arr[$key]['region_id'];
                            $stateInsert['name'] = $value->state;
                            $stateInsert['status'] = 1;
                            $stateCreate = State::create($stateInsert);
                            $arr[$key]['state_id'] = $stateCreate->id;
                            $state = array_merge([caseChange($value->state)=>$stateCreate->id], $state);
							$user[$key]['state'] = $stateCreate->id;
                        }

                        if(array_key_exists(caseChange($value->city),$city)){
                            $arr[$key]['city_id'] = $city[caseChange($value->city)];
							$user[$key]['city'] = $city[caseChange($value->city)];
                        }else{
							if($value->city != ''){
								$cityInsert['state_id'] = $arr[$key]['state_id'];
								$cityInsert['name'] = $value->city;
								$cityInsert['status'] = 1;
								$cityCreate = City::create($cityInsert);
								$arr[$key]['city_id'] = $cityCreate->id;
								$city = array_merge([caseChange($value->city)=>$cityCreate->id], $city);
								$user[$key]['city'] = $cityCreate->id;
							}
                        }

                        $store = StoreLocation::where('io_code',trim($value->internal_orderio))->update($arr[$key]);
                        $store = StoreLocation::where('io_code',trim($value->internal_orderio))->first();
						$user[$key]['store_id'] = $store->id;
						$InUserExist = User::where('email',$user[$key]['email'])->first();
						if(!$InUserExist){
							$userCreate = User::create($user[$key]);
							$userCreate->assignRole('SM');
						}else{
							$email = $user[$key]['email'];
							unset($user[$key]['email']);
							User::where('email',$email)->update($user[$key]);
						}
					}
                }

                if(!empty($arr)){
                    // echo '<pre>';print_r($arr);echo '</pre>';
                    // echo '<pre>';print_r($user);echo '</pre>';
					/*
                    foreach($arr as $key => $array){
                        $store = StoreLocation::create($array);
                        $user[$key]['store_id'] = $store->id;
                        $user = User::create($user[$key]);
                        $user->assignRole('SM');
                    } */
                    //StoreLocation::insert($arr);
                    return redirect('admin/store-location')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/store-location-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
/*
    public function updateStoreLocation(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);
		//dd($request->hasFile('excel'));
        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->get();

            $brand = array_change_key_case(Brand::pluck('id','name')->toArray(), CASE_LOWER);
            $region = array_change_key_case(Region::pluck('id','name')->toArray(), CASE_LOWER);
            $state = array_change_key_case(State::pluck('id','name')->toArray(), CASE_LOWER);
            $city = array_change_key_case(City::pluck('id','name')->toArray(), CASE_LOWER);
            $locationCode = array_change_key_case(StoreLocation::pluck('io_code','io_code')->toArray(), CASE_LOWER);
            $arr = [];
            $user = [];
            if($data->count()){
				//dd($data);
				//$data = $data[0];
				foreach ($data as $key => $value) {
					if(in_array(caseChange($value->io_code),$locationCode) && $value->io_code != ''){
                        $arr = [];
                        $arr['store_type'] = $value->store_type;
                        $arr['store_category'] = $value->store_category;
                        $store = StoreLocation::where('io_code',trim($value->io_code))->update($arr);
					}
                }

                if(!empty($arr)){
                    // echo '<pre>';print_r($arr);echo '</pre>';
                    // echo '<pre>';print_r($user);echo '</pre>';

                    //StoreLocation::insert($arr);
                    return redirect('admin/store-location')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/store-location-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
*/
    public function StoreCollectionForm(Request $request)
    {
        view()->share('route', 'store_collection');
        return view('admin.store_location.collectionform');
    }
    public function StoreCollectionUpload(Request $request)
    {
        $this->validate($request,[
                'excel_file'=>'required|mimes:xls,xlsx',
        ]);
        ini_set('max_execution_time', 300);
        if($request->hasFile('excel_file')){
            $path = $request->file('excel_file')->getRealPath();
            $data = \Excel::load($path,function($reader){})->get();
            $locationCode = array_change_key_case(StoreLocation::pluck('id','io_code')->toArray(), CASE_LOWER);
            $arr = [];
            $has_error = false;
            $error = array();
            if(count($data)){
                /*dd($data);
                foreach($data as $keys => $value) {
                    if (array_key_exists(caseChange($value->store_iocode), $locationCode)) {
                        $arr[$key]['store_id'] = $locationCode[caseChange($value->store_iocode)];
                        $arr[$key]['label'] = trim($value->sm);
                    }
                }*/
                foreach ($data as $keys => $val) {

                    foreach ($val as $key => $value) {
                        if(array_key_exists(caseChange($value->store_iocode),$locationCode)){
                            $arr[$key]['store_id'] = $locationCode[caseChange($value->store_iocode)];
                            $arr[$key]['amount'] = intval(preg_replace('/[^0-9]+/', '', $value->amount));
                       /*     if(isset($value->month) && isset($value->year) && !empty($value->year) && !empty($value->month)){ */
                                $arr[$key]['month'] = intval(preg_replace('/[^0-9]+/', '', $value->month));
                                $arr[$key]['year'] =  intval(preg_replace('/[^0-9]+/', '', $value->year));
                         /*   } else {
                                $month = Date('m',strtotime($request['date_of_month']));
                                $year = Date('Y',strtotime($request['date_of_month']));
                                $arr[$key]['month'] = intval(preg_replace('/[^0-9]+/', '',$month ));
                                $arr[$key]['year'] =  intval(preg_replace('/[^0-9]+/', '', $year));
                            } */

                            $store_collection = StoreCollection::selectRaw('store_collections.*,store_location.store_name,store_location.io_code')
                                                                ->leftjoin('store_location','store_location.id','store_collections.store_id')
                                                                ->where('store_id',$arr[$key]['store_id'])
                                                                ->where('month',$arr[$key]['month'])
                                                                ->where('year',$arr[$key]['year'])
                                                                ->first();
                            if($store_collection === null){
                                $collection = StoreCollection::create($arr[$key]);
                            } else {
                                $updated_value = $arr[$key];
                                    $data = array('message'=>"Arvind HR");
                                    Mail::send('email.productupdate', compact('updated_value','store_collection'), function ($message) use ($store_collection) {
                                        $message->from('hr@arvind.in', 'Admin Arvind HR Web')
                                                    ->sender('hr@arvind.in', 'Arvind HR Web')
                                                    ->to('admin.unlimited@arvindhr.in', 'Admin Arvind')
                                                    ->subject('Collection Upadated for '.$store_collection->store_name." Store");
                                    });
                                    $store_collection->amount = $arr[$key]['amount'];
                                        StoreCollection::where('id', $store_collection->id)
                                                ->update(['amount' => $arr[$key]['amount']]);
                                    $store_collection->save();

                            }

                        } else {
                            $has_error = true;
                            $error[$key] =$value->store_iocode;
                        }
                    }
                }
                if(!empty($arr) && !$has_error){
                    return redirect()->back()->with('flash_success', 'Record imported successfully!');
                }
            }
            return redirect()->back()->with('error', 'Other Store Collection Successfully created. Except this stores. Miss matched This io code : '.implode(", ",$error));
        }
            return redirect()->back()->with('flash_error', 'Not any data to upload.');
    }

    public function StoreDetails($store_id)
    {
        $store_detail = StoreLocation::find($store_id);
        if($store_detail){
            return ['status'=>true,'detail'=>$store_detail];
        }
        return ['status'=>false];
    }

}
