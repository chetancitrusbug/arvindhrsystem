<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\State;
use App\City;
use App\Region;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.state');
        $this->middleware('permission:access.state.edit')->only(['edit','update']);
        $this->middleware('permission:access.state.create')->only(['create', 'store']);
        $this->middleware('permission:access.state.delete')->only('destroy');
        view()->share('route', 'state');
        view()->share('module', 'State');
    }
    
    public function index(Request $request)
    {
    	return view('admin.state.index');
    }

    public function datatable(Request $request)
    {
        $state = State::select([
                    'state.*',
                    'region.name as region',
                ])
                ->leftJoin('region',function($join){
                    $join->on('region.id','=','state.region_id');
                })
                ->latest()->get();

        return Datatables::of($state)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $regions = Region::where("status","1")->pluck('name','id')->toArray();
        view()->share('regions',$regions);
        
        return view('admin.state.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $state = State::create($requestData);
        
        return redirect('admin/state')->with('flash_success', 'State added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $state = State::find($id);
        
        return view('admin.state.show', compact('state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $regions = Region::where("status","1")->pluck('name','id')->toArray();
        view()->share('regions',$regions);

        $state = State::find($id);

        if(!$state){
            return redirect('admin/state')->with('flash_error', 'State Not Found!');
        }
        
        return view('admin.state.edit', compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $state = State::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/state')->with('flash_success', 'State updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $state = State::find($id);

        City::where('state_id',$id)->delete();

        $state->delete();

        if($request->has('from_index')){
            $message = "State Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'State deleted!');

            return redirect('admin/state');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'region_id'=>'required',
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',new Unique('state','state',$id)],
        ];

        return $this->validate($request, $rules,[
            'region_id.required' => 'The Region field is required',
            'name.required' => 'The State field is required',
            'name.regex' => 'The State format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.state.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $region = array_change_key_case(Region::pluck('id','name')->toArray(), CASE_LOWER);
            $state = array_change_key_case(State::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->name),$state)){
                        $arr[$key]['status'] = 1;

                        if(array_key_exists(caseChange($value->region),$region)){
                            $arr[$key]['region_id'] = $region[caseChange($value->region)];
                        }else{
                            $regionInsert['name'] = $value->region;
                            $regionInsert['status'] = 1;
                            $regionCreate = Region::create($regionInsert);
                            $arr[$key]['region_id'] = $regionCreate->id;
                            $region = array_merge([trim($value->region)=>$regionCreate->id], $region);
                        }
                        $arr[$key]['name'] = trim($value->name);

                        $state = array_merge([caseChange($value->name)=>trim($value->name)], $state);
                    }
                }
                
                if(!empty($arr)){
                    State::insert($arr);
                    return redirect('admin/state')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/state-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
