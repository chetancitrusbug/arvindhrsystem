<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Brand;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.brand');
        $this->middleware('permission:access.brand.edit')->only(['edit','update']);
        $this->middleware('permission:access.brand.create')->only(['create', 'store']);
        $this->middleware('permission:access.brand.delete')->only('destroy');

        view()->share('route', 'brand');
        view()->share('module', 'Brand');
    }
    
    public function index(Request $request)
    {
    	return view('admin.brand.index');
    }

    public function datatable(Request $request)
    {
        $brand = Brand::latest()->get();

        return Datatables::of($brand)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $brand = Brand::create($requestData);
        
        return redirect('admin/brand')->with('flash_success', 'Brand added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $brand = Brand::find($id);

        if(!$brand){
            return redirect('admin/brand')->with('flash_error', 'Brand Not Found!');
        }
        
        return view('admin.brand.show', compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $brand = Brand::find($id);

        if(!$brand){
            return redirect('admin/brand')->with('flash_error', 'Brand Not Found!');
        }
        
        return view('admin.brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $brand = Brand::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/brand')->with('flash_success', 'Brand updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $brand = Brand::find($id);
        
        $brand->delete();

        if($request->has('from_index')){
            $message = "Brand Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Brand deleted!');

            return redirect('admin/brand');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',new Unique('brand','brand',$id)],
        ];

        return $this->validate($request, $rules,[
            'name.required' => 'The brand field is required',
            'name.regex' => 'The brand format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.brand.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $brand = array_change_key_case(Brand::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->name),$brand)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->name);

                        $brand = array_merge([caseChange($value->name)=>trim($value->name)], $brand);
                    }
               }
                
                if(!empty($arr)){
                    Brand::insert($arr);
                    return redirect('admin/brand')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/brand-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
