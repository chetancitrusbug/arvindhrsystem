<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Yajra\Datatables\Datatables;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use Carbon\Carbon as Carbon;
use App\HiringReport;
use App\Resignation;
use App\ResignationReason;
use DB;
use DateTime;

class HiringReportController extends Controller
{
    //

    public function CronRun(Request $request)
    {
        $stores = StoreLocation::where('status','1')->get();
        foreach ($stores as $store) {

			$current_months_hiringReports = HiringReport::where('store_id',$store->id)
            
                                            ->where('monthyear',date('m-Y'))
                                            ->first();
            if($current_months_hiringReports === null){
                $prevoius_month_hiringReports = HiringReport::where('store_id',$store->id)
                                            ->where('monthyear',date("m-Y",strtotime("-1 month")))
                                            ->first();
                $prevoius_month_hiringReports = null;
                $hiringReport = new HiringReport();
                $budget_man_power = ($store->budget_man_power > 0)? $store->budget_man_power : 0;
				$actual_man_power = ($store->actual_man_power > 0)? $store->actual_man_power : 0;
                if($prevoius_month_hiringReports === null){
                    $hiringReport->gap = $budget_man_power - $actual_man_power;
                    $hiringReport->pending = $budget_man_power - $actual_man_power;
                } else {
                    $hiringReport->gap = $prevoius_month_hiringReports->pending;
                    $hiringReport->pending = $prevoius_month_hiringReports->pending;
                }
                $hiringReport->hired = 0;
                $hiringReport->absconding = 0;
                $hiringReport->resignation = 0;

                $hiringReport->monthyear = date("m-Y");
                $hiringReport->store_id = $store->id;
                $hiringReport->region_id = $store->region_id;

                $hiringReport->save();
            }
        }
        echo "Cron Completed";
    }

    public function CronRunAbsconding(){
        //$employee = Employee::where('processstatus','Active-Absconding')->select("DATEDIFF( '" . date('Y-m-d') . "', employee.absconding_time) AS dd,employee.id,employee.absconding_status,employee.rm_absconding_status,employee.absconding_time,employee.processstatus")->having('dd', '>', 14)->get();
        $employees = DB::select("SELECT DATEDIFF( '".date('Y-m-d')."', employee.`absconding_time`) AS dd, employee.id,employee.`absconding_status`,employee.`rm_absconding_status`,employee.`absconding_time`,employee.`processstatus` FROM `employee` WHERE `processstatus` = 'Active-Absconding'  AND `employee`.`deleted_at` IS NULL and employee.`rm_absconding_status` IN ('2','3') HAVING dd > 14");
        foreach($employees as $employee){
            //$employee['processstatus'] = 'Inactive';
            $id = $employee->id;
            $data['processstatus'] = 'Inactive';
            $data['status'] = 'inactive';
            $emplo = Employee::where('employee.id',$id)->first()->update($data);
            var_dump($emplo);
        }

    }
    public function CronHiringReports(){
       $start = new \Carbon\Carbon('first day of this month');
        $stores = StoreLocation::where('status',1)->get();
        foreach ($stores as $store) {

            $current_months_hiringReports = HiringReport::where('store_id',$store->id)
                                            ->where('monthyear',date('m-Y'))
                                            ->first();
            if($current_months_hiringReports === null){
                $prevoius_month_hiringReports = HiringReport::where('store_id',$store->id)
                                            ->where('monthyear',date("m-Y",strtotime("-1 month")))
                                            ->first();

                $hiringReport = new HiringReport();
                if($prevoius_month_hiringReports === null){
                    $hiringReport->gap = 0;//@($store->budget_man_power - $store->actual_man_power);
                    $hiringReport->pending = @($store->budget_man_power - $store->actual_man_power);
                    $start = new \Carbon\Carbon('first day of this month');
                    $employees = \App\Employee::SelectRaw('count(id) as total_abscounding')
                            ->whereIN('rm_absconding_status',['2','3'])
                            ->whereBetween('absconding_time',[$start,\Carbon\Carbon::now()])
                            ->where('store_id',$store->id)
                            ->whereIN('processstatus',['Inactive'])
                            ->first();
                    $resigned_employee = \App\Resignation::selectRaw('count(employee.id) as total_resignation')
                                            ->leftJoin('employee','employee.id','resignation.employee_id')
                                            ->whereBetween('resignation.last_working_date',[$start,\Carbon\Carbon::now()])
                                            ->where('employee.store_id',$store->id)
                                            ->where('employee.processstatus',"Inactive")
											->where('resignation.resignation_status',"complete")
                                            ->first();
                    $hiringReport->hired = 0;
                    $hiringReport->absconding = $employees->total_abscounding;
                    $hiringReport->resignation = $resigned_employee->total_resignation;
                } else {
                    $hiringReport->gap = $prevoius_month_hiringReports->pending;
                    $hiringReport->pending = $prevoius_month_hiringReports->pending;
                    $hiringReport->hired = 0;
                    $hiringReport->absconding = 0;
                    $hiringReport->resignation = 0;
                }


                $hiringReport->monthyear = date("m-Y");
                $hiringReport->store_id = $store->id;
                $hiringReport->region_id = $store->region_id;

                $hiringReport->save();
            }
        }
        echo "Cron Hiring Completed";  
		/*$employees = \App\Employee::SelectRaw("employee.id,DATE_FORMAT(absconding_time,'%m-%Y') as month,store_id,store_location.region_id")
							->join('store_location','employee.store_id','store_location.id')
                            ->whereIN('rm_absconding_status',['2','3'])
                            //->whereBetween('absconding_time',[$start,\Carbon\Carbon::now()])
                            ->whereIN('processstatus',['Inactive',])
                            ->get();
							
		foreach($employees as $employee){
			
			$hiringReports = HiringReport::where('store_id',$employee->store_id)->where('monthyear',$employee->month)->first();
			if(!empty($hiringReports)){
				$hiringReports->absconding = $hiringReports->absconding + 1;
				$hiringReports->save();
			}else{
				$hiringReports = new HiringReport();
				$hiringReports->gap = 0;//@($store->budget_man_power - $store->actual_man_power);
				$hiringReports->pending = 0; 
				$hiringReports->absconding = 1;
				$hiringReports->monthyear = $employee->month;
				$hiringReports->store_id = $employee->store_id;
                $hiringReports->region_id = $employee->region_id;
				$hiringReports->save();
			}
		}  *//*
		$employees = \App\Resignation::SelectRaw("employee.id,DATE_FORMAT(resignation.last_working_date,'%m-%Y') as month,store_id,store_location.region_id")
							->join('employee','employee.id','resignation.employee_id')
							->join('store_location','employee.store_id','store_location.id')
							->where('employee.processstatus',"Inactive")
							->where('resignation.resignation_status',"complete")
                            ->get();
							
		foreach($employees as $employee){
			
			$hiringReports = HiringReport::where('store_id',$employee->store_id)->where('monthyear',$employee->month)->first();
			if(!empty($hiringReports)){
				$hiringReports->resignation = $hiringReports->resignation + 1;
				$hiringReports->save();
			}else{
				$hiringReports = new HiringReport();
				$hiringReports->gap = 0;//@($store->budget_man_power - $store->actual_man_power);
				$hiringReports->pending = 0; 
				$hiringReports->resignation = 1;
				$hiringReports->monthyear = $employee->month;
				$hiringReports->store_id = $employee->store_id;
                $hiringReports->region_id = $employee->region_id;
				$hiringReports->save();
			}
		}   */
    }

    public function hiringReport(Request $request)
    {
        return view('admin.hiring_report.index');
    }



    public function datatable(Request $request)
    {
        $current_year = date('Y');
        $current_month = date('m');
        if($current_month < 4){
            $current_year--;
        }

        $inmonths = array();
        for($i = 1; $i<=12;$i++){
            $next_year = $current_year+1;
            if($i < 10) $i = '0'.$i;
            ($i < 4)?$month= "$i-$next_year"  : $month=  "$i-$current_year";
            $inmonths[]= $month;

        }

        $hiringReport = HiringReport::selectRaw('*,sum(hiring_reports.pending) as pending_p,sum(hiring_reports.hired) as hired_h,sum(hiring_reports.gap) as gap_g ')
                        ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                        ->leftJoin('region','region.id','store_location.region_id')
                        ->whereIn('hiring_reports.monthyear',$inmonths)
                        ->groupBy('hiring_reports.region_id')
                        ->groupBy('hiring_reports.monthyear')
                        ->get();


        return Datatables::of($hiringReport)
            ->make(true);
    }

    public function hiringreportRegion(Request $request){
        $hiringReport = array();
        $inmonths = array();
        if($request->start_date != '' && $request->end_date != ''){
            $start_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->start_date);
            $end_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->end_date);
            $interval = new \DateInterval('P1M');
            $period   = new \DatePeriod($start_date, $interval, $end_date);
            foreach ($period as $dt) {
                $inmonths[] = $dt->format("m-Y");
            }
            
        }else{
            
            $current_year = date('Y');
            $current_month = date('m');
            if($current_month < 4){
                $current_year--;
            }
            
            for($i = 4; $i<=12;$i++){
                $next_year = $current_year+1;
                if($i < 10) $i = '0'.$i;
                ($i < 4)?$month= "$i-$next_year"  : $month=  "$i-$current_year";
                $inmonths[]= $month;

            }
            for($i = 1; $i<=3;$i++){
                $next_year = $current_year+1;

                if($i < 10) $i = '0'.$i;

                ($i < 4)?$month= "$i-$next_year"  : $month=  "$i-$current_year";
                $inmonths[]= $month;

            }
        }

        

		
         $yearhiringReport = HiringReport::selectRaw('(SUM(gap)+ SUM(hired))as total,SUM(pending) as pending,SUM(gap) as gap,SUM(hired) as hired,monthyear,store_location.region_id')
                                        ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                        ->leftJoin('region','region.id','store_location.region_id')
                                        ->whereIN('monthyear',$inmonths)
                                        ->whereNotNull('store_location.region_id')
                                        ->whereNull('store_location.deleted_at')
                                        ->where('store_location.status',1)
                                        ->whereNull('region.deleted_at')
                                        ->where('region.status',1)
                                        ->groupBy('monthyear','store_location.region_id')->get();
        $regions = Region::where('status',1)->get();
        foreach ($inmonths as $key => $value) {
            if($yearhiringReport && count($yearhiringReport) > 0){
                foreach ($regions as $region) {
                    $monthhiringReport = $yearhiringReport->where('monthyear',$value)->where('region_id',$region->id)->first();
                    if($monthhiringReport){
                        $hiringReport["$value"][$region->id] = $monthhiringReport;
                    }else {
                        $monthhiringReport['total'] = 0;
                        $monthhiringReport['pending'] = 0;
                        $monthhiringReport['gap'] = 0;
                        $monthhiringReport['hired'] = 0;
                        $monthhiringReport['monthyear'] = $value;
                        $monthhiringReport['region_id'] = $region->id;
                        $monthhiringReport = (json_decode(json_encode($monthhiringReport)));
                        $hiringReport["$value"][$region->id] = $monthhiringReport;
                    }
                }
              /*  if($monthhiringReport && count($monthhiringReport) > 0){

                } */
            }
        }
		
        $monthhiringReport = HiringReport::selectRaw('region.id as region_id, (SUM(hiring_reports.gap)+ SUM(hiring_reports.hired))as total,SUM(hiring_reports.pending) as pending,SUM(hiring_reports.gap) as gap,SUM(hiring_reports.hired) as hired ')
                                        ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                        ->leftJoin('region','region.id','store_location.region_id')
                                        ->where('hiring_reports.monthyear',date('m-Y'))
                                        ->where('region.status',1)
                                        ->whereNull('region.deleted_at')
                                        ->where('store_location.status',1)
                                        ->whereNull('store_location.deleted_at')

                                        ->groupBy('store_location.region_id')
                                        ->get();
        $monthinprocesshiringreports = array();
        $total_pending_employees = Employee::selectRaw('count(employee.id) as total_pending,region.id')
                                                ->Join('store_location','store_location.id','employee.store_id')
                                                ->Join('region','region.id','store_location.region_id')
                                                ->whereNotIn('employee.status',['confirm','reject','inactive'])
                                                ->where('store_location.status',1)
                                                ->groupBy('region.id')
                                                ->get();

        foreach ($regions as $region) {
                $total_pending_employee = $total_pending_employees->where('id',$region->id)->first();
                $monthhiring = $monthhiringReport->where('region_id',$region->id)->first();
                $monthinprocesshiringreports[] = array(
                    'region_name' =>$region->name,
                    'region_id' =>$region->id,
                    'total' => ($monthhiring)?$monthhiring->total:0,
                    'pending' => ($monthhiring)?$monthhiring->pending:0,
                    'gap' => ($monthhiring)?$monthhiring->gap:0,
                    'hired' => ($monthhiring)?$monthhiring->hired:0,
                    'in_process' => ($total_pending_employee)?$total_pending_employee->total_pending:0
            );
        }

       /* $query = "SELECT * FROM hiring_reports h WHERE h.monthyear = '$months' GROUP BY h.region_id";
                                            $month_hiring_reports = DB::select($query); */

        $region = StoreLocation::leftJoin('region','region.id','store_location.region_id')
                    ->where('region.status',1)
                    ->where('store_location.status',1)
                    ->groupBy('store_location.region_id')->get();
		return view('admin.hiring_report.regional_report',compact('region','inmonths','hiringReport','monthinprocesshiringreports'));
    }

    public function hiringreportStore(Request $request){
        $current_year = date('Y');
        $inmonths = array();
        for($i = 1; $i<=12;$i++){
            $next_year = $current_year+1;
            ($i < 4)?$inmonths[]= sprintf("%02d", $i)."-$next_year"  : $inmonths[]=  sprintf("%02d", $i)."-$current_year";
        }
        $hiringReport = array();
        $inmonths = array(date('m-Y'));
        $region_id = $request->route('region_id');
        $hiringReport = HiringReport::leftJoin('store_location','store_location.id','hiring_reports.store_id')
                        ->leftJoin('region','region.id','store_location.region_id')
                        ->whereIn('hiring_reports.monthyear',$inmonths)
                        ->where('hiring_reports.region_id',$region_id)
                        ->where('region.status',1)
                        ->whereNull('region.deleted_at')
                        ->where('store_location.status',1)
                        ->whereNull('store_location.deleted_at')
                        ->groupBy('hiring_reports.store_id')
                        ->orderBy('store_location.id')
                        ->get();
        $region_name = '';
        if($hiringReport && isset($hiringReport[0])){
            $region_name = $hiringReport[0]->name;
        }

        $hiringReports = array();
        $pendingemployees = Employee::selectRaw('employee.store_id,count(employee.id) as total_pending')
                                                ->whereNotIn('employee.status',['confirm','reject','inactive'])
                                                ->groupBy('employee.store_id')
                                                ->get();
        $stores = StoreLocation::where('status',1)->where('region_id',$region_id)->get();
        foreach ($stores as $store) {
                $total_pending_employee = $pendingemployees->where('store_id',$store->id)->first();
                $hiring = $hiringReport->where('store_id',$store->id)->first();
                $hiringReports[] = array(
                        'io_code' =>$store->io_code,
                        'store_id' =>$store->id,
                        'gap' =>($hiring)?$hiring->gap:0,
                        'hired' =>($hiring)?$hiring->hired:0,
                        'pending' =>($hiring)?$hiring->pending:0,
                        'store_name' =>$store->store_name,
                        'in_process' =>($total_pending_employee)?$total_pending_employee->total_pending:0,
                );
        }
		return view('admin.hiring_report.store_report',compact('hiringReports','inmonths','region_name'));
    }

    public function hiringreportStoreemployee($store_id)
    {
        $store = StoreLocation::find($store_id);
        return view('admin.hiring_report.store_pending_employee',compact('store'));
    }
    public function hiringreportpendingemployeeview($employee_id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($employee_id);

        if(!$employee){
            return redirect('admin/employee')->with('flash_error', 'Employee Not Found!');
        }

        $employee->education = EducationDetail::where('employee_id',$employee_id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$employee_id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$employee_id)->get()->toArray();

        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        return view('admin.hiring_report.viewpendingemployee', compact('employee'));
    }
    public function hiringreportStoreemployeedatatable($store_id)
    {
        $hiringreportStoreemployee = array();
        $hiringreportStoreemployee = Employee::selectRaw('employee.*,designation.name as designation_name')
                                            ->leftJoin('designation','designation.id','employee.designation_id')
                                            ->whereNotIn('employee.status',['confirm','reject','inactive'])
                                            ->where('employee.store_id',$store_id)
                                                ->get();
        return Datatables::of($hiringreportStoreemployee)
            ->make(true);
    }

    public function hiringreportStoreReport(Request $request){
        $current_year = date('Y');
        $current_month = date('m');
        if($current_month < 4){
            $current_year--;
        }
        $inmonths = array();
        for($i = 1; $i<=12;$i++){
            $next_year = $current_year+1;
            ($i < 4)?$inmonths[]= sprintf("%02d", $i)."-$next_year"  : $inmonths[]=  sprintf("%02d", $i)."-$current_year";
        }
        $hiringReport = array();
        $hiringReport = HiringReport::leftJoin('store_location','store_location.id','hiring_reports.store_id')
                        ->leftJoin('region','region.id','store_location.region_id')
                        ->where('store_location.status',1)
                        ->whereIn('hiring_reports.monthyear',$inmonths)
                        ->where('hiring_reports.store_id',$request->route('store_id'))
                        ->orderBy('store_location.id')
                        ->get();
        $store_name = '';
        if($hiringReport && isset($hiringReport[0])){
            $store_name = $hiringReport[0]->store_name;
        }
		return view('admin.hiring_report.store_report_all',compact('hiringReport','inmonths','store_name'));
    }

    public function attritionReportRegion(Request $request)
    {
        view()->share('route', 'attrition-report');
        view()->share('module', 'Attrition Report');
        $regions = \App\Region::where('status',1)->get();
         $hiringReport = array();
        $inmonths = array();
        if($request->start_date != '' && $request->end_date != ''){
            $start_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->start_date);
            $end_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->end_date);
            $interval = new \DateInterval('P1M');
            $period   = new \DatePeriod($start_date, $interval, $end_date);
            foreach ($period as $dt) {
                $inmonths[] = $dt->format("m-Y");
            }
            
        }else{
            
            $current_year = date('Y');
            $current_month = date('m');
            if($current_month < 4){
                $current_year--;
            }
            
            for($i = 4; $i<=12;$i++){
                $next_year = $current_year+1;
                if($i < 10) $i = '0'.$i;
                ($i < 4)?$month= "$i-$next_year"  : $month=  "$i-$current_year";
                $inmonths[]= $month;

            }
            for($i = 1; $i<=3;$i++){
                $next_year = $current_year+1;

                if($i < 10) $i = '0'.$i;

                ($i < 4)?$month= "$i-$next_year"  : $month=  "$i-$current_year";
                $inmonths[]= $month;

            }
        }
     /*   $attritionReport = HiringReport::selectRaw('*,sum(absconding) as absconding_total,sum(resignation) as resignation_total')
        ->whereIn('monthyear',$inmonths)
        ->orderby('monthyear')
        ->groupBy('monthyear')
        ->get(); */




        $yearattritionReport = HiringReport::selectRaw('sum(absconding) as absconding_total,sum(resignation) as resignation_total,monthyear,store_location.region_id')
                                        ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                        ->leftJoin('region','region.id','store_location.region_id')
                                        ->whereIN('monthyear',$inmonths)
                                        ->whereNotNull('store_location.region_id')
                                        ->whereNull('store_location.deleted_at')
                                        ->where('store_location.status',1)
                                        ->whereNull('region.deleted_at')
                                        ->where('region.status',1)
                                        ->groupBy('monthyear','store_location.region_id')->get();
        $regions = Region::where('status',1)->get();
        $attritionReport = array();
        foreach ($inmonths as $key => $value) {
            if($yearattritionReport && count($yearattritionReport) > 0){
                foreach ($regions as $region) {
                    $monthattritionReport = $yearattritionReport->where('monthyear',$value)->where('region_id',$region->id)->first();
                    if($monthattritionReport){
                        $attritionReport["$value"][$region->id] = $monthattritionReport;
                    }else {
                        $monthattritionReport['absconding_total'] = 0;
                        $monthattritionReport['resignation_total'] = 0;
                        $monthattritionReport['monthyear'] = $value;
                        $monthattritionReport['region_id'] = $region->id;
                        $monthattritionReport = (json_decode(json_encode($monthattritionReport)));
                        $attritionReport["$value"][$region->id] = $monthattritionReport;
                    }
                }
              /*  if($monthhiringReport && count($monthhiringReport) > 0){

                } */
            }
        }


            $manpowerPercentages = HiringReport::selectRaw('region.id,region.name,(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,SUM(store_location.actual_man_power) as total_actual_man_power')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('hiring_reports.monthyear',[date('m-Y')])
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->groupBy('region.id')
                                ->get();
			$store_location = StoreLocation::selectRaw('store_location.region_id,SUM(store_location.actual_man_power) as total_actual_man_power')->groupBy('store_location.region_id')->where('store_location.status',1)->get()->pluck('total_actual_man_power','region_id');
			
        $manpowerPercentageReport = array();
        foreach ($regions as $region) {
            $manpowerPercentage = $manpowerPercentages->where('id',$region->id)->first();
            $data = array(
                'region_id' =>$region->id,
                'region_name' =>$region->name,
                'Total' => ($manpowerPercentage)?$manpowerPercentage->Total:0,
                'total_actual_man_power' => ($store_location[$region->id])?$store_location[$region->id]:0,
            );
            $manpowerPercentageReport[] = $data;
        }
		return view('admin.hiring_report.attritionregional_report',compact('manpowerPercentageReport','regions','attritionReport','inmonths'));
    }

    public function attritionReportRegionPromise(){
        view()->share('route', 'attrition-promise');
        view()->share('module', 'Promise Attrition Report');
        $regions = \App\Region::where('status',1)->get();
        $promiseAttritionTotalAbscondings = Employee::selectRaw('count(employee.id) as total_absconding,store_location.region_id')->join('store_location','store_location.id','employee.store_id')->whereIn('employee.processstatus',['Active-Absconding'])->groupBy('store_location.region_id')->get();

        $promiseAttritionTotalResigneds = Employee::selectRaw('count(employee.id) as total_resigned,store_location.region_id')->join('store_location','store_location.id','employee.store_id')->whereIn('employee.processstatus',['Active-Resigned'])->groupBy('store_location.region_id')->get();
        $manpowerPercentageReport = array();
        foreach ($regions as $region) {
            $promiseAttritionTotalAbsconding = $promiseAttritionTotalAbscondings->where('region_id',$region->id)->first();
            $promiseAttritionTotalResigned = $promiseAttritionTotalResigneds->where('region_id',$region->id)->first();
            $regionAbsconding = (isset($promiseAttritionTotalAbsconding) && $promiseAttritionTotalAbsconding != NUll) ? $promiseAttritionTotalAbsconding->total_absconding : 0;
            $regionResigned = (isset($promiseAttritionTotalResigned) && $promiseAttritionTotalResigned != NUll) ? $promiseAttritionTotalResigned->total_resigned : 0;

            $total =  $regionAbsconding + $regionResigned;
            $data = array(
                'region_id' =>$region->id,
                'region_name' =>$region->name,
                'Total' => ($total)?$total:0,
                'promiseAttritionTotalAbsconding' => $regionAbsconding,
                'promiseAttritionTotalResigned' => $regionResigned,
            );
            $manpowerPercentageReport[] = $data;
        }
        return view('admin.hiring_report.attritionregionalreportpromise',compact('manpowerPercentageReport'));
    }

    public function attritionReportRegionPromiseRegionWise(Request $request,$regionId){
        view()->share('route', 'attrition-promise');
        view()->share('module', 'Promise Attrition Report');
        $regions = \App\Region::where('status',1)->where('id',$regionId)->first();
        $promiseAttritionTotalAbscondings = Employee::selectRaw('count(employee.id) as total_absconding,store_location.region_id,store_location.store_name')->join('store_location','store_location.id','employee.store_id')->where('store_location.status',1)->whereIn('employee.processstatus',['Active-Absconding'])->where('store_location.region_id',$regionId)->groupBy('store_location.region_id')->get();

        $promiseAttritionTotalResigneds = Employee::selectRaw('count(employee.id) as total_resigned,store_location.id,store_location.store_name,store_location.io_code')->join('store_location','store_location.id','employee.store_id')->where('store_location.status',1)->whereIn('employee.processstatus',['Active-Resigned'])->groupBy('store_location.id')->where('store_location.region_id',$regionId)->get();
        $manpowerPercentageReport = array();
        foreach ($promiseAttritionTotalResigneds as $store_data) {
            $resigned =  $store_data->total_resigned;
            $storeAbsconding = $promiseAttritionTotalAbscondings->where('store_location.id',$store_data->id)->first();
            $absconding = (isset($storeAbsconding) && $storeAbsconding != NUll) ? $storeAbsconding->total_absconding : 0;
            $total =  $absconding + $resigned;
            $data = array(
                'store_name' =>$store_data->store_name,
                'io_code' =>$store_data->io_code,
                'Total' => ($total)?$total:0,
                'absconding' => $absconding,
                'resigned' => $resigned,
            );
            $manpowerPercentageReport[] = $data;
        }

        return view('admin.hiring_report.attritionstorereportpromise',compact('manpowerPercentageReport','regions'));

    }
    public function regionattritiondatatable(Request $request)
    {
        if($request->start_date != '' && $request->end_date != ''){
            $start_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->start_date);
            $end_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->end_date);
            $interval = new \DateInterval('P1M');
            $period   = new \DatePeriod($start_date, $interval, $end_date);
            foreach ($period as $dt) {
                $inmonths[] = $dt->format("m-Y");
            }
            
        }else{
            
            $current_year = date('Y');
            $current_month = date('m');
            if($current_month < 4){
                $current_year--;
            }
            
            for($i = 4; $i<=12;$i++){
                $next_year = $current_year+1;
                if($i < 10) $i = '0'.$i;
                ($i < 4)?$month= "$i-$next_year"  : $month=  "$i-$current_year";
                $inmonths[]= $month;

            }
            for($i = 1; $i<=3;$i++){
                $next_year = $current_year+1;

                if($i < 10) $i = '0'.$i;

                ($i < 4)?$month= "$i-$next_year"  : $month=  "$i-$current_year";
                $inmonths[]= $month;

            }
        }
        $hiringReport = array();


        $attritionReports = HiringReport::selectRaw('hiring_reports.*,region.name,(SUM(hiring_reports.absconding)+ SUM(hiring_reports.resignation))as total,SUM(hiring_reports.absconding) as total_absconding,SUM(hiring_reports.resignation) as total_resignation')
                                        ->leftJoin('region','region.id','hiring_reports.region_id')
                                        ->where('region.status',1)
                                        ->whereIn('hiring_reports.monthyear',$inmonths)
                                        ->groupBy('hiring_reports.region_id')
                                        ->get();
        return Datatables::of($attritionReports)
            ->make(true);
    }

    public function regionmanpowerpecentagedatatable(Request $request)
    {
         $manpowerPercentageReport = HiringReport::selectRaw('region.name,(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,SUM(store_location.actual_man_power) as total_actual_man_power')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('monthyear',[date('m-Y')])
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->groupBy('region.id')
                                ->get();

        return Datatables::of($manpowerPercentageReport)
            ->make(true);
    }

    public function attritionReportStore(Request $request)
    {
        return view('admin.hiring_report.attrionStorewise');
    }
    public function attritionReportStoredatatable(Request $request,$region_id)
    {
        $mtdattritionReport = array();
        if($request->start_date != '' && $request->end_date != ''){
            $start_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->start_date);
            $end_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->end_date);
            $interval = new \DateInterval('P1M');
            $period   = new \DatePeriod($start_date, $interval, $end_date);
            foreach ($period as $dt) {
                $inmonths[] = $dt->format("m-Y");
            }
            
        }else{
            $current_year = date('Y');
            $current_month = date('m');
            $inmonths = array();
            if($current_month < 4){
                $current_year--;
            }
            for($i = 1; $i<=12;$i++){
                $next_year = $current_year+1;
                ($i < 4)?$inmonths[]= sprintf("%02d", $i)."-$next_year"  : $inmonths[]=  sprintf("%02d", $i)."-$current_year";
            }
        }
		
        $mtdattritionReport = HiringReport::selectRaw('hiring_reports.*,(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,store_location.store_name,region.name,SUM(hiring_reports.absconding) AS absconding_total,SUM(hiring_reports.resignation) AS resignation_total ')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('monthyear',$inmonths)
                                ->where('store_location.status',1)
                                ->where('region.status',1)
                                ->where('hiring_reports.region_id',$region_id)
                                ->groupBy('hiring_reports.store_id')
                                ->get();
								
        $Stores = StoreLocation::selectraw('region.name,store_location.*')->leftJoin('region','region.id','store_location.region_id')->where('region_id',$region_id)->where('store_location.status',1)->get();
        $data = array();
        foreach ($Stores as $store) {
            $mtdaa = $mtdattritionReport->where('store_id',$store->id)->first();
			array_push($data,array(
                'name' => $store->name,
                'store_id' => $store->id,
                'store_name' => $store->store_name,
                'io_code' => $store->io_code,
                'Total'=>($mtdaa)?$mtdaa->Total:0,
                'absconding'=>($mtdaa)?$mtdaa->absconding_total:0,
                'resignation'=>($mtdaa)?$mtdaa->resignation_total:0,
            ));
        }

        return Datatables::of($data)
            ->make(true);
    }

    public function attritionreportreportStoreReport(Request $request,$store_id)
    {
        $totalattritionReport = HiringReport::selectRaw('hiring_reports.*,(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,store_location.store_name,region.name,store_location.actual_man_power')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('monthyear',[date('m-Y')])
                                ->where('hiring_reports.store_id',$store_id)
                                ->first();
        return view('admin.hiring_report.attritionStore',compact('totalattritionReport'));
    }

    public function attritionReportReason(Request $request)
    {
        $reasons =ResignationReason::where('status','Active')
                                    ->where('parent_id',0)
                                    ->get();
        $regions = Region::where('status',1)->get();
        $report_data = array();
        $current_year = date('Y');
            $current_month = date('m');
            if($current_month < 4){
                $current_year--;
            }
        $reasonwise = Employee::selectRaw('region.id as region_id,resignation.reason_id')
                            ->Join('resignation','employee.id','resignation.employee_id')
                            ->Join('store_location','store_location.id','employee.store_id')
                            ->Join('region','region.id','store_location.region_id')
                            ->whereNull('resignation.deleted_at')
                            ->wheredate('resignation.last_working_date','>=',date('Y-m-d',strtotime('1-4-'.$current_year)))
                            ->get();
        foreach ($regions as $region) {
            $data = array();
            $data['region_name'] = $region->name;
            $data['region_id'] = $region->id;
            foreach ($reasons as $reason) {
                $reasonwisedata = $reasonwise->where('region_id',$region->id)->where('reason_id',$reason->id)->count();
                $data[$reason->id] =($reasonwisedata)?$reasonwisedata:0;
            }
            $report_data[] = $data;

        }
        return view('admin.hiring_report.attritionbyreason',compact('reasons','report_data'));
    }

    public function attritionReportReasonStore(Request $request,$store_id)
    {
        $reasons =ResignationReason::where('status','Active')
                                    ->where('parent_id',0)
                                    ->get();
        $store = StoreLocation::where('status',1)->where('id',$store_id)->first();
        $report_data = array();
            $data = array();
            $data['store_name'] = $store->store_name;
            $current_year = date('Y');
            $current_month = date('m');
            if($current_month < 4){
                $current_year--;
            }
            $reasonwise = Employee::selectRaw('count(resignation.reason_id) As total,region.name,resignation.reason_id')
                            ->Join('resignation','employee.id','resignation.employee_id')
                            ->Join('store_location','store_location.id','employee.store_id')
                            ->Join('region','region.id','store_location.region_id')
                            ->where('store_location.id',$store_id)
                            ->where('resignation.reason_id',$reason->id)
                            ->whereNull('resignation.deleted_at')
                            ->wheredate('resignation.last_working_date','>=',date('Y-m-d',strtotime('1-4-'.$current_year)))
                            ->groupBy('resignation.reason_id')
                            ->get();
            foreach ($reasons as $reason) {
                        $reasonwisedata =$reasonwise->where('reason_id',$reason->id);
                            $data[$reason->id] =($reasonwisedata)?$reasonwisedata->total:0;
            }
            $report_data[] = $data;

        return view('admin.hiring_report.attritionbyreasonstore',compact('reasons','report_data'));
    }

    public function attritionReportReasonRegion(Request $request,$region_id)
    {
        $reasons =ResignationReason::where('status','Active')
                                    ->where('parent_id',0)
                                    ->get();
        $stores = StoreLocation::where('status',1)
                                    ->where('region_id',$region_id)
                                    ->get();
        $report_data = array();
        $current_year = date('Y');
            $current_month = date('m');
            if($current_month < 4){
                $current_year--;
            }
            $reasonwise = Resignation::selectRaw('store_location.id as store_id,resignation.reason_id')
                                    ->Join('employee','employee.id','resignation.employee_id')
                                    ->Join('store_location','store_location.id','employee.store_id')
                                    ->Join('region','region.id','store_location.region_id')
                                    ->whereNull('resignation.deleted_at')
                                    ->wheredate('resignation.last_working_date','>=',date('Y-m-d',strtotime('1-4-'.$current_year)))
                                    ->get();
            if($stores){
                foreach ($stores as $store) {
                    $data = array();
                    $data['store_name'] = $store->store_name;
                    $data['store_id'] = $store->id;
                    $data['io_code'] = $store->io_code;
                    foreach ($reasons as $reason) {
                        $reasonwisedata = $reasonwise->where('store_id',$store->id)->where('reason_id',$reason->id)->count();
                        $data[$reason->id] =(!empty($reasonwisedata))?$reasonwisedata:0;
                    }
                    $report_data[] = $data;
                }
            }


        return view('admin.hiring_report.attritionbyreasonstore',compact('reasons','report_data'));
    }


    public function manpowerpercentageRegion(Request $request,$region_id)
    {
        $region = Region::find($region_id);
        $manpowerPercentageReports = HiringReport::selectRaw('store_location.id as store_id,(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('monthyear',[date('m-Y')])
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->groupBy('store_location.id')
                                ->get();
        $stores = StoreLocation::where('status',1)->where('region_id',$region_id)->get();
        $manpowerPercentageReport = array();
        foreach($stores as $store){
            $hirings = $manpowerPercentageReports->where('store_id',$store->id)->first();
            $data = array(
                'store_id'=>$store->id,
                'io_code'=>$store->io_code,
                'store_name'=>$store->store_name,
                'Total'=>($hirings)?$hirings->Total:0,
                'total_actual_man_power'=>$store->actual_man_power,
            );
            $manpowerPercentageReport[] = $data;
        }

        if($region){
            $region_name = $region->name;
        }
        return view('admin.hiring_report.manpowerpercentageregion',compact('manpowerPercentageReport','region_name'));
    }

    public function manpowerpercentageStore(Request $request,$store_id)
    {
        $manpowerPercentageReport = HiringReport::selectRaw('region.name,store_location.store_name,store_location.id,store_location.io_code,(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,SUM(store_location.actual_man_power) as total_actual_man_power')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('monthyear',[date('m-Y')])
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.id',$store_id)
                                ->groupBy('store_location.id')
                                ->get();
        $store_name = '';
        $region_name = '';
        if($manpowerPercentageReport && isset($manpowerPercentageReport[0])){
            $store_name = $manpowerPercentageReport[0]->store_name;
            $region_name = $manpowerPercentageReport[0]->name;
        }
        return view('admin.hiring_report.manpowerpercentagestore',compact('manpowerPercentageReport','store_name','region_name'));
    }
    public function agingregion(Request $request)
    {
        return view('admin.hiring_report.agingreason');
    }
    public function agingregiondatatable(Request $request)
    {
        $regions = Region::where('status',1)->get();

       /* $leaved_employee = Employee::selectRaw('region.id as region_id,region.name as region_name,resignation.last_working_date,employee.id,employee.joining_date,employee.date_of_leave')
                    ->Join('resignation','employee.id','resignation.employee_id')
                    ->leftJoin('store_location','store_location.id','employee.store_id')
                    ->leftJoin('region','region.id','store_location.region_id')
                    ->where('store_location.status',1)
                    ->where('region.status',1)
                    ->whereNull('resignation.deleted_at')
                    ->where(function ($query) {
                            $query->where('resignation.last_working_date', '>=', date('Y-m-d',strtotime('1-4-'.date('Y'))))
                                ->orWhere('employee.date_of_leave', '>=', date('Y-m-d',strtotime('1-4-'.date('Y'))));
                    })
                    ->orderBy('employee.store_id')
                    ->get();*/
        if($request->start_date != '' && $request->end_date){
            $previouseDate = $request->start_date;
            $currentDate = $request->end_date;
        }else{
            $currnetYear = date('Y');
            $previouseYear = date('Y')-1;
            $current_month = date('m');
            if($current_month >= 4){
                $previouseYear = date('Y');
                $currnetYear = date('Y')+1;
            }
            $previouseDate = $previouseYear.'-04-01';
            $currentDate = $currnetYear.'-03-31';
        }
		
		$leaved_employee = \App\Resignation::SelectRaw('region.id as region_id,region.name as region_name,resignation.last_working_date,employee.id,employee.joining_date,employee.date_of_leave')
					->join('employee','employee.id','resignation.employee_id')
					->where(function($query) use ($previouseDate,$currentDate) { 
								$query->whereBetween('employee.date_of_leave',  [date($previouseDate),date($currentDate)]);
								$query->orwhereBetween('resignation.last_working_date', [date($previouseDate),date($currentDate)]);
						})
					->leftJoin('store_location','store_location.id','employee.store_id')
                    ->leftJoin('region','region.id','store_location.region_id')
					->whereNotIn('employee.rm_absconding_status',['1','2','3'])
					->where('employee.processstatus',"Inactive")
					->where('resignation.resignation_status',"complete")
					->get();
		$leaved_employee_absconding = Employee::selectRaw('region.id as region_id,region.name as region_name,employee.absconding_time,employee.id,employee.joining_date,employee.date_of_leave')
				->where('employee.processstatus',"Inactive")
				->leftJoin('store_location','store_location.id','employee.store_id')
				->leftJoin('region','region.id','store_location.region_id')
				->whereIN('employee.rm_absconding_status',['2','3'])
				->where(function($query) use ($previouseDate,$currentDate) {
					$query->whereBetween('employee.absconding_time',[date($previouseDate),date($currentDate)]);
				})
				->get();
        $baby_attrition = 0;
        $regional_array = array();
        foreach ($leaved_employee as $employee) {

            $fdate = str_replace('/', '-',$employee->joining_date);
            $tdate = (!empty($employee->date_of_leave))?$employee->date_of_leave:$employee->last_working_date;

            $datetime1 = new DateTime($fdate);
            $datetime2 = new DateTime($tdate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');
            $regional_array[$employee->region_id]['region_id'] = $employee->region_id;
            $regional_array[$employee->region_id]['region_name'] = $employee->region_name;
            $regional_array[$employee->region_id]['total'] = (isset($regional_array[$employee->region_id]['total']))?$regional_array[$employee->region_id]['total']+1:1;
            if(!isset($regional_array[$employee->region_id]['infant_attrition']) && !isset($regional_array[$employee->region_id]['baby_attrition']) && !isset($regional_array[$employee->region_id]['attrition'])){

                $regional_array[$employee->region_id]['infant_attrition'] = 0;
                $regional_array[$employee->region_id]['baby_attrition'] = 0;
                $regional_array[$employee->region_id]['attrition'] = 0;
            }
            if($days <= 30){
                $regional_array[$employee->region_id]['infant_attrition'] = (isset($regional_array[$employee->region_id]['infant_attrition']))?$regional_array[$employee->region_id]['infant_attrition']+1:1;
            } else if($days >30 && $days <=90 ){
                $regional_array[$employee->region_id]['baby_attrition'] = (isset($regional_array[$employee->region_id]['baby_attrition']))?$regional_array[$employee->region_id]['baby_attrition']+1:1;
            } else {

                $regional_array[$employee->region_id]['attrition'] = (isset($regional_array[$employee->region_id]['attrition']))?$regional_array[$employee->region_id]['attrition']+1:1;
            }
        }
		foreach ($leaved_employee_absconding as $employee) {

            $fdate = str_replace('/', '-',$employee->joining_date);
            $tdate = (!empty($employee->absconding_time))?$employee->absconding_time:$employee->absconding_time;

            $datetime1 = new DateTime($fdate);
            $datetime2 = new DateTime($tdate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');
            $regional_array[$employee->region_id]['region_id'] = $employee->region_id;
            $regional_array[$employee->region_id]['region_name'] = $employee->region_name;
            $regional_array[$employee->region_id]['total'] = (isset($regional_array[$employee->region_id]['total']))?$regional_array[$employee->region_id]['total']+1:1;
            if(!isset($regional_array[$employee->region_id]['infant_attrition']) && !isset($regional_array[$employee->region_id]['baby_attrition']) && !isset($regional_array[$employee->region_id]['attrition'])){

                $regional_array[$employee->region_id]['infant_attrition'] = 0;
                $regional_array[$employee->region_id]['baby_attrition'] = 0;
                $regional_array[$employee->region_id]['attrition'] = 0;
            }
            if($days <= 30){
                $regional_array[$employee->region_id]['infant_attrition'] = (isset($regional_array[$employee->region_id]['infant_attrition']))?$regional_array[$employee->region_id]['infant_attrition']+1:1;
            } else if($days >30 && $days <=90 ){
                $regional_array[$employee->region_id]['baby_attrition'] = (isset($regional_array[$employee->region_id]['baby_attrition']))?$regional_array[$employee->region_id]['baby_attrition']+1:1;
            } else {

                $regional_array[$employee->region_id]['attrition'] = (isset($regional_array[$employee->region_id]['attrition']))?$regional_array[$employee->region_id]['attrition']+1:1;
            }
        }
        foreach ($regions as $region) {
            if(!array_key_exists($region->id,$regional_array)){
                $regional_array[$region->id]['region_id'] = $region->id;
                $regional_array[$region->id]['region_name'] = $region->name;
                $regional_array[$region->id]['total'] = 0;
                $regional_array[$region->id]['infant_attrition'] = 0;
                $regional_array[$region->id]['baby_attrition'] = 0;
                $regional_array[$region->id]['attrition'] = 0;
            }
        }
        ksort($regional_array);
        return Datatables::of($regional_array)
            ->make(true);
    }
    public function agingregionstore(Request $request,$region_id)
    {
        return view('admin.hiring_report.agingreasonstore');
    }
    public function agingregionstoredatatable(Request $request,$region_id)
    {
        $stores = StoreLocation::where('status',1)->where('region_id',$region_id)->get();
        $store_id = array();
        $store_array = array();
        foreach ($stores as $store) {
                $store_array[$store->id]['store_id'] = $store->id;
                $store_array[$store->id]['io_code'] = $store->io_code;
                $store_array[$store->id]['store_name'] = $store->store_name;
                $store_array[$store->id]['total'] = 0;
                $store_array[$store->id]['infant_attrition'] = 0;
                $store_array[$store->id]['baby_attrition'] = 0;
                $store_array[$store->id]['attrition'] = 0;
                $store_id[] = $store->id;
        }
        $leaved_employee = Employee::selectRaw('employee.store_id,region.id as region_id,store_location.store_name,resignation.last_working_date,employee.id,employee.joining_date,employee.date_of_leave')
                    ->Join('resignation','employee.id','resignation.employee_id')
                    ->leftJoin('store_location','store_location.id','employee.store_id')
                    ->leftJoin('region','region.id','store_location.region_id')
                    ->where('store_location.status',1)
                    ->where('region.status',1)
                    ->whereIn('employee.store_id',$store_id)
                    ->whereNull('resignation.deleted_at')
                    ->where(function ($query) {
                            $query->where('resignation.last_working_date', '>=', date('Y-m-d',strtotime('1-4-'.date('Y'))))
                                ->orWhere('employee.date_of_leave', '>=', date('Y-m-d',strtotime('1-4-'.date('Y'))));
                    })
                    ->orderBy('employee.store_id')
                    ->get();

        foreach ($leaved_employee as $employee) {

            $fdate = str_replace('/', '-',$employee->joining_date);
            $tdate = (!empty($employee->date_of_leave))?$employee->date_of_leave:$employee->last_working_date;

            $datetime1 = new DateTime($fdate);
            $datetime2 = new DateTime($tdate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');

            $store_array[$employee->store_id]['total'] = $store_array[$employee->store_id]['total']+1;

            if($days <= 30){
                $store_array[$employee->store_id]['infant_attrition'] = (isset($store_array[$employee->store_id]['infant_attrition']))?$store_array[$employee->store_id]['infant_attrition']+1:1;
            } else if($days >30 && $days <=90 ){
                $store_array[$employee->store_id]['baby_attrition'] = (isset($store_array[$employee->store_id]['baby_attrition']))?$store_array[$employee->store_id]['baby_attrition']+1:1;
            } else {
                $store_array[$employee->store_id]['attrition'] = (isset($store_array[$employee->store_id]['attrition']))?$store_array[$employee->store_id]['attrition']+1:1;
            }
        }

        return Datatables::of($store_array)
            ->make(true);

    }

    public function hiringinprocessRegion(Request $request)
    {
        return view('admin.hiring_report.inpreocessregion');
    }

    public function hiringinprocessRegiondatatable(Request $request)
    {

        $regions = Region::where('status',1)->get();
        $store_side_pendings = Region::selectRaw('region.id as region_id,count(employee.id) as total_pending')
                                ->leftjoin('store_location','store_location.region_id','region.id')
                                ->leftjoin('employee','employee.store_id','store_location.id')
                                ->whereNotIn('employee.hrms_status',[2])
                                ->whereIn('employee.store_status',[3,0])
                                ->whereNotIn('employee.regional_status',[2])
                                ->whereIN('employee.status',['pending','approve'])
                                ->groupBy('region.id')
                                ->get();
        $region_side_pendings = Region::selectRaw('region.id as region_id,count(employee.id) as total_pending')
                                ->leftjoin('store_location','store_location.region_id','region.id')
                                ->leftjoin('employee','employee.store_id','store_location.id')
                                ->whereNotIn('employee.hrms_status',[2])
                                ->whereIn('employee.store_status',[1])
                                ->whereNotIn('employee.store_status',[3])
                                ->whereNotIn('employee.regional_status',[2])
                                ->whereIN('employee.status',['pending','approve'])
                                ->groupBy('region.id')
                                ->get();
        $hrms_side_pendings = Region::selectRaw('region.id as region_id,count(employee.id) as total_pending')
                                ->leftjoin('store_location','store_location.region_id','region.id')
                                ->leftjoin('employee','employee.store_id','store_location.id')
                                ->where('employee.hrms_status',1)
                                ->whereIn('employee.regional_status',[2])
                                ->whereIN('employee.status',['pending','approve'])
                                ->whereNotIn('employee.hrms_status',[2,3])
                                ->groupBy('region.id')
                                ->get();
        $region_report_data = array();
        if ($regions) {
            foreach ($regions as $region) {
                $data = array();
                $data['region_name'] = $region->name;
                $data['region_id'] = $region->id;
                $region_id = $region->id;
                $store_side_pending = $store_side_pendings->where('region_id',$region_id)->first();
                $region_side_pending = $region_side_pendings->where('region_id',$region_id)->first();
                $hrms_side_pending = $hrms_side_pendings->where('region_id',$region_id)->first();

                $data['store_pending'] = ($store_side_pending)?$store_side_pending->total_pending:0;
                $data['region_pending'] = ($region_side_pending)?$region_side_pending->total_pending:0;
                $data['hrms_pending'] = ($hrms_side_pending)?$hrms_side_pending->total_pending:0;
                $region_report_data[] = $data;
            }

        }
        return Datatables::of($region_report_data)
            ->make(true);

    }

    public function hiringinprocessStore(Request $request)
    {
        return view('admin.hiring_report.inpreocessregionstore');
    }
    public function hiringinprocessStoredatatable(Request $request,$region_id)
    {
        $store_side_pendings = StoreLocation::selectRaw('store_location.id as store_id,count(employee.id) as total_pending')
                                ->leftjoin('employee','employee.store_id','store_location.id')
                                ->where('store_location.region_id',$region_id)
                                ->whereNotIn('employee.hrms_status',[2])
                                ->whereIn('employee.store_status',[3,0])
                                ->whereNotIn('employee.regional_status',[2])
                                ->whereIN('employee.status',['pending','approve'])
                                ->groupBy('employee.store_id')
                                ->get();
        $region_side_pendings = StoreLocation::selectRaw('store_location.id as store_id,count(employee.id) as total_pending')
                                ->leftjoin('employee','employee.store_id','store_location.id')
                                ->where('store_location.region_id',$region_id)
                                ->whereNotIn('employee.hrms_status',[2])
                                ->whereIn('employee.store_status',[1])
                                ->whereNotIn('employee.store_status',[3])
                                ->whereNotIn('employee.regional_status',[2])
                                ->whereIN('employee.status',['pending','approve'])
                                ->groupBy('employee.store_id')
                                ->get();
        $hrms_side_pendings = StoreLocation::selectRaw('store_location.id as store_id,count(employee.id) as total_pending')
                                ->leftjoin('employee','employee.store_id','store_location.id')
                                ->where('store_location.region_id',$region_id)
                                ->where('employee.hrms_status',1)
                                ->whereIn('employee.regional_status',[2])
                                ->whereIN('employee.status',['pending','approve'])
                                ->whereNotIn('employee.hrms_status',[2,3])
                                ->groupBy('employee.store_id')
                                ->get();
        $stores = StoreLocation::where('region_id',$region_id)->where('status',1)->get();
        $store_report_data = array();
        if ($stores) {
            foreach ($stores as $store) {
                $data = array();
                $data['store_name'] = $store->store_name;
                $data['store_id'] = $store->id;
                $data['io_code'] = $store->io_code;
                $store_id = $store->id;
                    $store_side_pending = $store_side_pendings->where('store_id',$store_id)->first();
                    $region_side_pending = $region_side_pendings->where('store_id',$store_id)->first();
                    $hrms_side_pending = $hrms_side_pendings->where('store_id',$store_id)->first();

                    $data['store_pending'] = ($store_side_pending)?$store_side_pending->total_pending:0;
                    $data['region_pending'] = ($region_side_pending)?$region_side_pending->total_pending:0;
                    $data['hrms_pending'] = ($hrms_side_pending)?$hrms_side_pending->total_pending:0;
                    $store_report_data[] = $data;
            }
        }

        return Datatables::of($store_report_data)
            ->make(true);
    }

    public function hiringsourceregion(Request $request)
    {
        $hiringSourceRequirements = array();
        $sourceCategories = SourceCategory::where('status',1)->get();
        $regions = Region::where('status',1)->get();
        $hiringSources = Employee::selectRaw('employee.id as employee_id,employee.source_category_id,region.id as region_id')
                                        ->leftJoin('store_location','store_location.id','employee.store_id')
                                        ->leftJoin('region','region.id','store_location.region_id')
                                        ->whereDate('employee.created_at','>=',date('Y-m-01'))
                                        ->where('employee.status','confirm')
                                        ->get();
        if($sourceCategories && $regions){
            foreach($regions as $region){
                $data = array('region_name' => $region->name,
                           'region_id' => $region->id,);
                foreach ($sourceCategories as $sourceCategory) {
                        $hiringSource = $hiringSources->where('region_id',$region->id)->where('source_category_id',$sourceCategory->id)->count();
                        $data[$sourceCategory->id]=($hiringSource)?$hiringSource:0;
                }
                $hiringSourceRequirements[] = $data;
            }

        }
        return view('admin.hiring_report.hiringsourceregion',compact('hiringSourceRequirements','sourceCategories'));

    }
    public function hiringsourceregionstore(Request $request,$region_id)
    {
        $hiringSourceRequirements = array();
        $sourceCategories = SourceCategory::where('status',1)->get();
        $stores = StoreLocation::selectRaw('store_location.*')
                                ->LeftJoin('region','region.id','store_location.region_id')
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->get();
        $hiringSources = Employee::selectRaw('employee.store_id,employee.source_category_id')
                                ->leftJoin('store_location','store_location.id','employee.store_id')
                                ->where('store_location.region_id',$region_id)
                                ->whereDate('employee.created_at','>=',date('Y-m-01'))
                                ->where('employee.status','confirm')
                                ->get();

        if($sourceCategories && $stores){
            foreach($stores as $store){
                $data = array('store_name' => $store->store_name,
                           'store_id' => $store->id,
                           'io_code' => $store->io_code
                        );
                foreach ($sourceCategories as $sourceCategory) {
                    $hiringSource = $hiringSources->where('store_id',$store->id)->where('source_category_id',$sourceCategory->id)->count();
                    $data[$sourceCategory->id]=($hiringSource)?$hiringSource:0;
                }
                $hiringSourceRequirements[] = $data;
            }

        }
        return view('admin.hiring_report.hiringsourcestore',compact('hiringSourceRequirements','sourceCategories'));
    }

    public function genderwiseattrition()
    {
		$currnetYear = date('Y');
		$previouseYear = date('Y')-1;
		$current_month = date('m');
        if($current_month >= 4){
            $previouseYear = date('Y');
			$currnetYear = date('Y')+1;
        }
        $regions = Region::where('status',1)->get();
        $report_data = array();
        $employeeAbsconding = Employee::selectRaw('employee.id,employee.gender,region.id as region_id')
			->join('store_location','store_location.id','employee.store_id')
			->join('region','region.id','store_location.region_id')
			->where('employee.processstatus',"Inactive")
			->whereIN('employee.rm_absconding_status',['2','3'])
			->where(function($query) use ($previouseYear,$currnetYear) { 
				$query->whereBetween('employee.absconding_time', [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
			})
			->get();
		$employeeResignation = \App\Resignation::SelectRaw("employee.id,employee.gender,region.id as region_id")
			->join('employee','employee.id','resignation.employee_id')
			->join('store_location','store_location.id','employee.store_id')
			->join('region','region.id','store_location.region_id')
			->where(function($query) use ($previouseYear,$currnetYear) { 
						$query->whereBetween('employee.date_of_leave',  [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
						$query->orwhereBetween('resignation.last_working_date', [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
				})
			->whereNotIn('employee.rm_absconding_status',['1','2','3'])
			->where('employee.processstatus',"Inactive")
			->where('resignation.resignation_status',"complete")
			->get();
        foreach($regions as $region){
            $gendermaleEmployeeAbsconding = $employeeAbsconding->where('region_id',$region->id)->where('gender','male')->count();
            $genderfemaleEmployeeAbsconding = $employeeAbsconding->where('region_id',$region->id)->where('gender','female')->count(); 
			$gendermaleEmployeeResignation = $employeeResignation->where('region_id',$region->id)->where('gender','male')->count();
            $genderfemaleEmployeeResignation = $employeeResignation->where('region_id',$region->id)->where('gender','female')->count();
			
            $report_data[$region->id]['region_name'] = $region->name;
            $report_data[$region->id]['region_id'] = $region->id;
            $report_data[$region->id]['male'] = $gendermaleEmployeeAbsconding + $gendermaleEmployeeResignation;
            $report_data[$region->id]['female'] = $genderfemaleEmployeeAbsconding + $genderfemaleEmployeeResignation;

        }
        return $report_data;
    }
     public function genderwiseattritionstore($region_id)
    {
        $stores = StoreLocation::selectRaw('store_location.*')
                                ->Join('region','region.id','store_location.region_id')
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->get();
        $employee = Employee::selectRaw('employee.id,employee.gender,store_location.id as store_id')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->join('resignation','resignation.employee_id','employee.id')
                                    ->where('employee.status','inactive')
                                    ->where('store_location.region_id',$region_id)
                                    ->where(function($query){
                                        $query->whereDate('employee.date_of_leave', '>=', date('Y-m-1'));
                                        $query->orwhereDate('resignation.last_working_date', '>=',date('Y-m-1'));
                                    })
                                    ->get();
        $report_data = array();
        foreach($stores as $store){
            $gendermale = $employee->where('region_id',$store->id)->where('gender','male')->count();
            $genderfemale = $employee->where('region_id',$store->id)->where('gender','female')->count();
            $report_data[$store->id]['store_name'] = $store->store_name;
            $report_data[$store->id]['io_code'] = $store->io_code;
            $report_data[$store->id]['store_id'] = $store->id;
            $report_data[$store->id]['male'] = ($gendermale)?$gendermale:0;
            $report_data[$store->id]['female'] = ($genderfemale)?$genderfemale:0;

        }
        return $report_data;
    }
    public function genderwisehiring()
    {
		$currnetYear = date('Y');
		$previouseYear = date('Y')-1;
		$current_month = date('m');
        if($current_month >= 4){
            $previouseYear = date('Y');
			$currnetYear = date('Y')+1;
        }
        $regions = Region::where('status',1)->get();
        $gendermales = Employee::selectRaw('count(employee.id) as total_employee,region.id as region_id')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.status','confirm')
                                    ->whereBetween('employee.joining_date',[date($previouseYear.'-04-01'),date($currnetYear.'-03-31')])
                                    ->where('employee.gender','male')
                                    ->groupBy('region.id')
                                    ->get();
        $genderfemales = Employee::selectRaw('count(employee.id) as total_employee,region.id as region_id')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.status','confirm')
                                    ->whereBetween('employee.joining_date',[date($previouseYear.'-04-01'),date($currnetYear.'-03-31')])
                                    ->where('employee.gender','female')
                                    ->groupBy('region.id')
                                    ->get();
        $report_data = array();
        foreach($regions as $region){
            $gendermale = $gendermales->where('region_id',$region->id)->first();
            $genderfemale = $genderfemales->where('region_id',$region->id)->first();

            $report_data[$region->id]['region_name'] = $region->name;
            $report_data[$region->id]['region_id'] = $region->id;
            $report_data[$region->id]['male'] = ($gendermale)?$gendermale->total_employee:0;
            $report_data[$region->id]['female'] = ($genderfemale)?$genderfemale->total_employee:0;

        }
        return $report_data;

    }
     public function genderwisehiringstore($region_id)
    {
            $stores = StoreLocation::selectRaw('store_location.*')
                                ->Join('region','region.id','store_location.region_id')
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->get();
            $gendermales = Employee::selectRaw('count(employee.id) as total_employee,store_location.id as store_id')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.status','confirm')
                                    ->where('employee.gender','male')
                                    ->whereDate('employee.joining_date', '>=', date('Y-m-1'))
                                    ->where('store_location.region_id',$region_id)
                                    ->groupBy('store_location.id')
                                    ->get();
            $genderfemales = Employee::selectRaw('count(employee.id) as total_employee,store_location.id as store_id')
                                    ->join('store_location','store_location.id','employee.store_id')
                                    ->join('region','region.id','store_location.region_id')
                                    ->where('employee.status','confirm')
                                    ->where('employee.gender','female')
                                    ->whereDate('employee.joining_date', '>=', date('Y-m-1'))
                                    ->where('store_location.region_id',$region_id)
                                    ->groupBy('store_location.id')
                                    ->get();
        $report_data = array();
        foreach($stores as $store){
            $gendermale = $gendermales->where('store_id',$store->id)->first();
            $genderfemale = $genderfemales->where('store_id',$store->id)->first();
            $report_data[$store->id]['store_name'] = $store->store_name;
            $report_data[$store->id]['store_id'] = $store->id;
            $report_data[$store->id]['io_code'] = $store->io_code;
            $report_data[$store->id]['male'] = ($gendermale)?$gendermale->total_employee:0;
            $report_data[$store->id]['female'] = ($genderfemale)?$genderfemale->total_employee:0;

        }
        return $report_data;
    }

    public function genderwisetotalemployee()
    {
        $regions = Region::where('status',1)->get();
        $report_data = array();
        $gendermales = Employee::selectRaw('count(employee.id) as total_employee,store_location.region_id')
                                            ->join('store_location','store_location.id','employee.store_id')
                                            ->where('employee.processstatus','!=','Inactive')
                                            ->where('employee.gender','male')
                                            ->where('store_location.status',1)
                                            ->groupBy('store_location.region_id')
                                            ->get();
        $genderfemales = Employee::selectRaw('count(employee.id) as total_employee,store_location.region_id')
                                            ->join('store_location','store_location.id','employee.store_id')
                                            ->where('employee.processstatus','!=','Inactive')
                                            ->where('employee.gender','female')
                                            ->where('store_location.status',1)
                                            ->groupBy('store_location.region_id')
                                            ->get();
        foreach($regions as $region){
            $gendermale = $gendermales->where('region_id',$region->id)->first();
            $genderfemale = $genderfemales->where('region_id',$region->id)->first();

            $report_data[$region->id]['region_name'] = $region->name;
            $report_data[$region->id]['region_id'] = $region->id;
            $report_data[$region->id]['male'] = ($gendermale)?$gendermale->total_employee:0;
            $report_data[$region->id]['female'] = ($genderfemale)?$genderfemale->total_employee:0;
        }
        return $report_data;
    }
    public function genderwisetotalemployeestore($region_id)
    {
        $stores = StoreLocation::selectRaw('store_location.*')
                                ->Join('region','region.id','store_location.region_id')
                                ->where('region.status',1)
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->get();
        $gendermales = Employee::selectRaw('count(employee.id) as total_employee,employee.store_id')
                                    ->where('employee.status','confirm')
                                    ->where('employee.gender','male')
                                    ->groupBy('employee.store_id')
                                    ->get();
            $genderfemales = Employee::selectRaw('count(employee.id) as total_employee,employee.store_id')
                                    ->where('employee.status','confirm')
                                    ->where('employee.gender','female')
                                    ->groupBy('employee.store_id')
                                    ->get();
        $report_data = array();
        foreach($stores as $store){
            $gendermale = $gendermales->where('store_id',$store->id)->first();
            $genderfemale = $genderfemales->where('store_id',$store->id)->first();
            $report_data[$store->id]['store_name'] = $store->store_name;
            $report_data[$store->id]['store_id'] = $store->id;
            $report_data[$store->id]['io_code'] = $store->io_code;
            $report_data[$store->id]['male'] =($gendermale)? $gendermale->total_employee:0;
            $report_data[$store->id]['female'] =($genderfemale)?$genderfemale->total_employee:0;

        }
        return $report_data;
    }
    public function additionalReports(Request $request)
    {
        $genderwiseattrition = $this->genderwiseattrition();
        $genderwisehiring = $this->genderwisehiring();
        $genderwisetotalemployee = $this->genderwisetotalemployee();
        return view('admin.hiring_report.additionreportregional',compact('genderwiseattrition','genderwisehiring','genderwisetotalemployee'));
    }

    public function storeadditionalReports(Request $request,$region_id)
    {
        $genderwiseattrition = $this->genderwiseattritionstore($region_id);
        $genderwisehiring = $this->genderwisehiringstore($region_id);
        $genderwisetotalemployee = $this->genderwisetotalemployeestore($region_id);
        return view('admin.hiring_report.additionalreportstore',compact('genderwiseattrition','genderwisehiring','genderwisetotalemployee'));
    }

}
