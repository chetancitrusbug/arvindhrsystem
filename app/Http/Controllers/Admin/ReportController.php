<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use DB;
use App\City;
use App\State;
use App\Employee;
use App\StoreLocation;
use App\OtherInfo;
use Carbon\Carbon as Carbon;
use Yajra\Datatables\Datatables;
use App\StoreCollection;


class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        /*$this->middleware('permission:access.city');
        $this->middleware('permission:access.city.edit')->only(['edit','update']);
        $this->middleware('permission:access.city.create')->only(['create', 'store']);
        $this->middleware('permission:access.city.delete')->only('destroy');*/
    }

    public function hiringReport(Request $request)
    {
        view()->share('route', 'hiring-report');
        view()->share('module', 'Hiring Report');

        view()->share('groupby', $request->has('groupby')?$request->groupby:'region');
        view()->share('city', $request->has('city')?$request->city:'');
        view()->share('state', $request->has('state')?$request->state:'');
        view()->share('region', $request->has('region')?$request->region:'');

        $filter = $request->filter;

        $employee = StoreLocation::select([
                    'store_location.id',
                    'employee.id as emp_id',
                    'store_location.store_name',
                    'store_location.region_id',
                    'store_location.state_id',
                    'store_location.city_id',
                    'employee.full_name',
                    'employee.hrms_status',
                    'region.name as region_name',
                    'employee.store_id',
                    'state.name as state_name',
                    'city.name as city_name',
                    DB::raw("COUNT(employee.id) as total_emp"),
                    DB::raw("SUM(IF(employee.hrms_status = 2,1,0)) as approved"),
                    DB::raw("SUM(IF(employee.hrms_status = 3,1,0)) as rejected"),
                ])
                ->leftjoin('employee',function($join){
                    $join->on('employee.store_id','=','store_location.id');
                    $join->on(DB::raw("MONTH(employee.created_at)"),'=',DB::raw("MONTH(CURRENT_DATE())"));
                })
                ->leftjoin('region',function($join){
                    $join->on('region.id','=','store_location.region_id');
                })
                ->leftjoin('state',function($join){
                    $join->on('state.id','=','store_location.state_id');
                })
                ->leftjoin('city',function($join){
                    $join->on('city.id','=','store_location.city_id');
                })
                ->whereNull('employee.deleted_at');
                // ->where(DB::raw("MONTH(employee.created_at)"),DB::raw("MONTH(CURRENT_DATE())"));

                if(isset($filter['from_date']) && !empty($filter['from_date'])){
                    $from_date = Carbon::parse($filter['from_date'])->format('Y-m-d');
                    $employee->where('employee.created_at','>=',$from_date);
                }
                if(isset($filter['to_date']) && !empty($filter['to_date'])){
                    $to_date = Carbon::parse($filter['to_date'])->format('Y-m-d');
                    $employee->where('employee.created_at','<=',$to_date);
                }

                if($request->groupby == "store"){
                    $employee->where('store_location.city_id',$request->city);
                    $employee->groupBy('store_location.id');
                }elseif($request->groupby == "state"){
                    $employee->where('store_location.region_id',$request->region);
                    $employee->groupBy('store_location.state_id');
                }elseif($request->groupby == "city"){
                    $employee->where('store_location.state_id',$request->state);
                    $employee->groupBy('store_location.city_id');
                }elseif($request->groupby == "employee"){
                    $employee->where('employee.store_id',$request->store);
                    $employee->groupBy('employee.id');
                }else{
                    $employee->groupBy('store_location.region_id');
                }
                $employee = $employee->get();

        return view('admin.report.hiring', compact('employee','filter'));
    }

    public function attendanceReport(Request $request)
    {
        view()->share('route', 'attendance_report');
        view()->share('module', 'Attendance Report');

        view()->share('groupby', $request->has('groupby')?$request->groupby:'region');
        view()->share('city', $request->has('city')?$request->city:'');
        view()->share('state', $request->has('state')?$request->state:'');
        view()->share('region', $request->has('region')?$request->region:'');

        $filter = $request->filter;

        $employee = StoreLocation::select([
                    'store_location.id',
                    'employee.id as emp_id',
                    'store_location.store_name',
                    'store_location.region_id',
                    'store_location.state_id',
                    'store_location.city_id',
                    'employee.full_name',
                    'employee.hrms_status',
                    'region.name as region_name',
                    'employee.store_id',
                    'state.name as state_name',
                    'city.name as city_name',
                    DB::raw("COUNT(employee.id) as total_emp"),
                    DB::raw("SUM(store_location.actual_man_power) as actual_man_power"),
                    DB::raw("count(attendance.id) as present"),
                    DB::raw("SUM(IF(employee.hrms_status = 3,1,0)) as rejected"),
                ])
                ->leftjoin('employee',function($join){
                    $join->on('employee.store_id','=','store_location.id');
                    $join->on(DB::raw("MONTH(employee.created_at)"),'=',DB::raw("MONTH(CURRENT_DATE())"));
                })
                ->leftjoin('region',function($join){
                    $join->on('region.id','=','store_location.region_id');
                })
                ->leftjoin('state',function($join){
                    $join->on('state.id','=','store_location.state_id');
                })
                ->leftjoin('city',function($join){
                    $join->on('city.id','=','store_location.city_id');
                })
                ->leftjoin('attendance',function($join){
                    $join->on('employee.id','=','attendance.employee_id');
                    $join->on('attendance.attendance','=',\DB::raw("'p'"));
                })
                ->whereNull('employee.deleted_at');
                // ->where(DB::raw("MONTH(employee.created_at)"),DB::raw("MONTH(CURRENT_DATE())"));

                if(isset($filter['from_date']) && !empty($filter['from_date'])){
                    $from_date = Carbon::parse($filter['from_date'])->format('Y-m-d');
                    $employee->where('employee.created_at','>=',$from_date);
                }
                if(isset($filter['to_date']) && !empty($filter['to_date'])){
                    $to_date = Carbon::parse($filter['to_date'])->format('Y-m-d');
                    $employee->where('employee.created_at','<=',$to_date);
                }

                if($request->groupby == "store"){
                    $employee->where('store_location.city_id',$request->city);
                    $employee->groupBy('store_location.id');
                }elseif($request->groupby == "state"){
                    $employee->where('store_location.region_id',$request->region);
                    $employee->groupBy('store_location.state_id');
                }elseif($request->groupby == "city"){
                    $employee->where('store_location.state_id',$request->state);
                    $employee->groupBy('store_location.city_id');
                }elseif($request->groupby == "employee"){
                    $employee->where('employee.store_id',$request->store);
                    $employee->groupBy('attendance.employee_id');
                }else{
                    $employee->groupBy('store_location.region_id');
                }
                $employee = $employee->get();

        return view('admin.report.attendance', compact('employee','filter'));
    }

    public function ColletionReportregiondatatable(Request $request)
    {
        $current_year = date('Y');
        $current_month = date('m');
        $current_month--;
        if($current_month < 1){
            $current_month = 12;
            $current_year--;
        }
        if(Input::get('month') && Input::get('year') ){
            $current_month  = Input::get('month');
            $current_year = Input::get('year');
        }

        $collection_report = array();
        $regions = \App\Region::where('status',1)->get();
        $collection_reports = StoreCollection::selectRaw('store_collections.*,SUM(store_collections.amount) as total_amount,region.name as region_name, region.id as region_id')
                                            ->join('store_location','store_location.id','store_collections.store_id')
                                            ->join('region','region.id','store_location.region_id')
                                            ->where('store_collections.month',$current_month)
                                            ->where('store_collections.year',$current_year)
                                            ->groupby('store_location.region_id')
                                            ->get();
        $total_employees = Employee::selectRaw('store_location.region_id,count(employee.id) as total_employee,SUM(employee.ctc) as total_ctc')
                                        ->join('store_location','store_location.id','employee.store_id')
                                        ->join('region','region.id','store_location.region_id')
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->groupBy('store_location.region_id')
                                        ->get();
        foreach ($regions as $region) {
            $total_employee = $total_employees->where('region_id',$region->id)->first();
            $collection = $collection_reports->where('region_id',$region->id)->first();
            $collection_report[] = array(
                'region_id'=>$region->id,
                'region_name'=>$region->name,
                'total_amount'=>($collection)?$collection->total_amount:0,
                'total_employee'=>($total_employee)?$total_employee->total_employee:0,
                'avg_ctc' =>($total_employee)?($total_employee->total_ctc > 0  && $total_employee->total_employee >0)? round($total_employee->total_ctc/$total_employee->total_employee,2):'-':'-',
                'per_person_income' => ($collection)?($collection->total_amount !=0  && $total_employee->total_employee >0)? round($collection->total_amount/$total_employee->total_employee,2):'-':'-'
            );
        }
        return Datatables::of($collection_report)
            ->make(true);
    }

    public function ColletionReportregion(Request $request)
    {
        return view('admin.report.productivity.regionproductivity');
    }
    public function ColletionReportstore(Request $request,$region_id)
    {
        return view('admin.report.productivity.storeproductivity');
    }

    public function ColletionReportstoredatatable(Request $request,$region_id)
    {
        $current_year = date('Y');
        $current_month = date('m');
        $current_month--;
        if($current_month < 1){
            $current_year--;
            $current_month = 12;
        }
        if(Input::get('month') && Input::get('year') ){
            $current_month  = Input::get('month');
            $current_year = Input::get('year');
        }

        $collection_report = array();
        $stores = StoreLocation::where('store_location.status',1)
                                            ->where('store_location.region_id',$region_id)
                                            ->get();
        $collection_reports = StoreCollection::selectRaw('store_collections.*,SUM(store_collections.amount) as total_amount,store_location.store_name, store_location.id as store_id')
                                            ->join('store_location','store_location.id','store_collections.store_id')
                                            ->join('region','region.id','store_location.region_id')
                                            ->where('store_collections.month',$current_month)
                                            ->where('store_collections.year',$current_year)
                                            ->where('store_location.status',1)
                                            ->where('store_location.region_id',$region_id)
                                            ->groupby('store_location.id')
                                            ->get();
        $total_employees = Employee::selectRaw('employee.store_id,count(employee.id) as total_employee,SUM(employee.ctc) as total_ctc')
                                        ->join('store_location','store_location.id','employee.store_id')
                                        ->join('region','region.id','store_location.region_id')
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->groupby('employee.store_id')
                                        ->get();
        foreach ($stores as $store) {
            $total_employee = $total_employees->where('store_id',$store->id)->first();
            $collection = $collection_reports->where('store_id',$store->id)->first();

            $collection_report[] = array(
                'store_id'=>$store->id,
                'io_code'=>$store->io_code,
                'store_name'=>$store->store_name,
                'total_amount'=>($collection)?$collection->total_amount:0,
                'total_employee'=>($total_employee)?$total_employee->total_employee:0,
                'avg_ctc' =>($total_employee)?($total_employee->total_ctc > 0  && $total_employee->total_employee >0)? round($total_employee->total_ctc/$total_employee->total_employee,2):'-':'-',
                'per_person_income' => ($collection && $total_employee)?($collection->total_amount !=0  && $total_employee->total_employee >0)? round($collection->total_amount/$total_employee->total_employee,2):'-':'-'
            );
        }
        return Datatables::of($collection_report)
            ->make(true);
    }

    public function ColletionReportregionFillter(Request $request)
    {
        $this->validate($request, [
             'month' => 'required',
             'year' => 'required',
        ]);
        $url = url('admin/productivity-report/fillter/');
        return view('admin.report.productivity.regionproductivityfillter');
    }

    public function ColletionReportregionFilltershow(Request $request)
    {
        $current_year = date('Y');
        $current_month = date('m');
        $current_month--;
        if($current_month < 1){
            $current_year--;
        }

        $collection_report = array();
        $collection_reports = StoreCollection::selectRaw('store_collections.*,SUM(store_collections.amount) as total_amount,store_location.store_name, store_location.id as store_id')
                                            ->join('store_location','store_location.id','store_collections.store_id')
                                            ->join('region','region.id','store_location.region_id')
                                            ->where('store_collections.month',$current_month)
                                            ->where('store_collections.year',$current_year)
                                            ->where('store_location.status',1)
                                            ->where('store_location.region_id',$region_id)
                                            ->groupby('store_collections.store_id')
                                            ->get();
        $total_employees = Employee::selectRaw('count(employee.id) as total_employee,SUM(employee.ctc) as total_ctc,employee.store_id')
                                        ->join('store_location','store_location.id','employee.store_id')
                                        ->join('region','region.id','store_location.region_id')
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->groupBy('employee.store_id')
                                        ->get();
        foreach ($collection_reports as $collection) {
            $total_employee = $total_employees->where('store_id',$collection->store_id)->first();
            $collection_report[] = array(
                'store_id'=>$collection->store_id,
                'store_name'=>$collection->store_name,
                'total_amount'=>$collection->total_amount,
                'total_employee'=>$total_employee->total_employee,
                'avg_ctc' =>($total_employee->total_ctc > 0  && $total_employee->total_employee >0)? round($total_employee->total_ctc/$total_employee->total_employee,2):'-',
                'per_person_income' => ($collection->total_amount !=0  && $total_employee->total_employee >0)? round($collection->total_amount/$total_employee->total_employee,2):'-'
            );
        }

        return view('admin.report.productivity.fillterproductivity',compact('collection_report'));
    }

    public function LeaveReportregion(Request $request)
    {
        return view('admin.report.leave.leavereportregion');
    }
    public function LeaveReportregiondatatable(Request $request)
    {
        $leaveReport = Employee::selectRaw('region.name as region_name, region.id as region_id,SUM(other_infos.allocated_leave) as total_allocated_leave, SUM(other_infos.pending_leave) as total_pending_leave, SUM(other_infos.availed_leave) as availed_leave')
                                        ->leftjoin('other_infos','other_infos.employee_id','employee.id')
                                        ->leftJoin('store_location','store_location.id','employee.store_id')
                                        ->leftjoin('region','region.id','store_location.region_id')
                                        ->where('region.status',1)
                                        ->where('store_location.status',1)
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->whereNULL('region.deleted_at')
                                        ->whereNULL('store_location.deleted_at')
                                        ->groupBy('store_location.region_id')
                                        ->orderby('region.id')
                                        ->get();
        return Datatables::of($leaveReport)
            ->make(true);
    }

    public function LeaveReportstore(Request $request,$region_id)
    {
        return view('admin.report.leave.leavereportstore');
    }
    public function LeaveReportstoredatatable(Request $request,$region_id)
    {
        $leaveReport = StoreLocation::selectRaw('store_location.`io_code`,store_location.io_code,store_location.store_name , store_location.id AS store_id,SUM(other_infos.allocated_leave) AS total_allocated_leave, SUM(other_infos.pending_leave) AS total_pending_leave, SUM(other_infos.availed_leave) AS availed_leave')
            ->leftjoin('employee',function($jointEmployee){
                $jointEmployee->on('store_location.id','=','employee.store_id')
                ->where('employee.processstatus','!=','Inactive');
            })
            ->leftjoin('other_infos','other_infos.employee_id','employee.id')
            ->leftjoin('region','region.id','store_location.region_id')
            ->where('store_location.status',1)
            ->where('store_location.region_id',$region_id)
            ->whereNULL('store_location.deleted_at')
            ->groupBy('store_location.id')
            ->orderby('store_location.id')
            ->get();
        return Datatables::of($leaveReport)
            ->make(true);
    }
    public function LeaveReportemployee(Request $request,$store_id)
    {
        return view('admin.report.leave.leavereportemployee');
    }
    public function LeaveReportemployeedatatable(Request $request,$store_id)
    {
        $leaveReport = Employee::selectRaw('employee.employee_code,employee.full_name as employee_name,other_infos.allocated_leave, other_infos.pending_leave,other_infos.availed_leave')
                                        ->leftjoin('other_infos','other_infos.employee_id','employee.id')
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->where('employee.store_id',$store_id)
                                        ->get();
        return Datatables::of($leaveReport)
            ->make(true);
    }
}
