<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Designation;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.designation');
        $this->middleware('permission:access.designation.edit')->only(['edit','update']);
        $this->middleware('permission:access.designation.create')->only(['create', 'store']);
        $this->middleware('permission:access.designation.delete')->only('destroy');
        
        view()->share('route', 'designation');
        view()->share('module', 'Designation');
    }
    
    public function index(Request $request)
    {
    	return view('admin.designation.index');
    }

    public function datatable(Request $request)
    {
        $designation = Designation::latest()->get();

        return Datatables::of($designation)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.designation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $designation = Designation::create(array_except($requestData,['attach']));
        
        return redirect('admin/designation')->with('flash_success', 'Designation added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $designation = Designation::find($id);

        if(!$designation){
            return redirect('admin/designation')->with('flash_error', 'Designation Not Found!');
        }
        
        return view('admin.designation.show', compact('storeLocation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $designation = Designation::find($id);

        if(!$designation){
            return redirect('admin/designation')->with('flash_error', 'Designation Not Found!');
        }
        
        return view('admin.designation.edit', compact('designation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $designation = Designation::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/designation')->with('flash_success', 'Designation updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $designation = Designation::find($id);

        $designation->delete();

        if($request->has('from_index')){
            $message = "Designation Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Designation deleted!');

            return redirect('admin/designation');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',new Unique('designation','designation',$id)],
        ];

        return $this->validate($request, $rules,[
            'name.required' => 'The designation field is required',
            'name.regex' => 'The designation format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.designation.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $designation = array_change_key_case(Designation::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->name),$designation)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->name);

                        $designation = array_merge([caseChange($value->name)=>trim($value->name)], $designation);
                    }
                }
                
                if(!empty($arr)){
                    Designation::insert($arr);
                    return redirect('admin/designation')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/designation-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
