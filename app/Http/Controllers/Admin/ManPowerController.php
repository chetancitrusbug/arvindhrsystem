<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\StoreLocation;
use App\Designation;
use App\ManPowerDetail;
use App\Rules\Unique;
use App\Region;
use Yajra\Datatables\Datatables;
use Excel;

class ManPowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        /*$this->middleware('permission:access.man_power');
        $this->middleware('permission:access.man_power.edit')->only(['edit','update']);
        $this->middleware('permission:access.man_power.create')->only(['create', 'store']);
        $this->middleware('permission:access.man_power.delete')->only('destroy');*/

        $array['sm'] = 'Store Manager';
        $array['dm'] = 'Department Manager';
        $array['tdm'] = 'TDM';
        $array['vm'] = 'Vendore Manager';
        $array['logistics'] = 'Logistics';
        $array['maintenance'] = 'Maintenance';
        $array['cashier'] = 'Cashier';
        $array['head_cashier'] = 'Head Cashier';
        $array['csa'] = 'Customer Service Assosiate';
        $this->arrayForLabelDesignation = $array;
        view()->share('store_designation', $array);
        
        view()->share('route', 'man_power');
        view()->share('module', 'Man Power');
    }

    public function index(Request $request)
    {
    	return view('admin.man_power.index');
    }

	public function manPowerRegionWiseStore($id){
		$manPower = array();

		$region = Region::find($id);
		return view('admin.report.manpower.regionWiseStore',compact('id','region'));
	}

    public function datatable(Request $request,$id)
    {

        $manPower = StoreLocation::select([
                            'store_location.*',
                            \DB::raw("CONCAT(io_code,' (',sap_code,')') as store_code"),
                            \DB::raw("(budget_man_power - actual_man_power) as gap"),
                            'city.name as city',
							'region.name'
                        ])
                        ->leftjoin('city',function($join){
                            $join->on('city.id','=','store_location.city_id');
                        })
						->leftJoin('region','region.id','store_location.region_id');
		if($id > 0){
			$manPower->where('store_location.region_id',$id);
		}
	    $manPower = $manPower->orderBy('store_location.budget_man_power', 'DESC')
                        ->get();

        return Datatables::of($manPower)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.man_power.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $manPower = $this->getManPower($id);

        if(!$manPower){
            return redirect('admin/man-power')->with('flash_error', 'Man Power Not Found!');
        }

        $designation = Designation::where('status','1')->pluck('name','id')->toArray();
        $manPowerDetail = ManPowerDetail::where('store_location_id',$id)->get();

        return view('admin.man_power.show', compact('manPower','designation','manPowerDetail'));
    }

    public function getManPower($id)
    {
        return StoreLocation::select([
                            'store_location.*',
                            \DB::raw("CONCAT(io_code,' (',sap_code,')') as store_code"),
                            \DB::raw("(budget_man_power - actual_man_power) as gap"),
                            'city.name as city',
                        ])
                        ->leftjoin('city',function($join){
                            $join->on('city.id','=','store_location.city_id');
                        })
                        ->where('store_location.id',$id)
                        ->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $manPower = $this->getManPower($id);;

        if(!$manPower){
            return redirect('admin/man-power')->with('flash_error', 'Man Power Not Found!');
        }

        $designation = Designation::where('status','1')->pluck('name','id')->toArray();
        $manPowerDetail = ManPowerDetail::where('store_location_id',$id)->pluck('man_power','designation_id')->toArray();

        return view('admin.man_power.edit', compact('manPower','designation','manPowerDetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $requestData = $request->all();

        $budgetManPower = array_sum($requestData['designation']);
        $manPowerDetail = ManPowerDetail::where('store_location_id',$id)->pluck('id','designation_id')->toArray();

        foreach ($requestData['designation'] as $key => $value) {
            $data = [];
            $data['store_location_id'] = $id;
            $data['designation_id'] = $key;
            $data['man_power'] = $value;
            if(array_key_exists($key, $manPowerDetail)){
                ManPowerDetail::where('id',$manPowerDetail[$key])->update(['man_power'=>$value]);
            }else{
                ManPowerDetail::create($data);
            }
        }

        $manPower = StoreLocation::where('id',$id)->update(['actual_man_power'=>$budgetManPower]);

        return redirect('admin/man-power')->with('flash_success', 'Man Power updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $manPower = StoreLocation::find($id);

        $manPower->delete();

        if($request->has('from_index')){
            $message = "Variable Pay Type Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Variable Pay Type deleted!');

            return redirect('admin/man-power');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',new Unique('variable_pay_type','variable pay type',$id)],
        ];

        return $this->validate($request, $rules,[
            'name.required' => 'The Variable Pay Type field is required',
            'name.regex' => 'The Variable Pay Type format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.man_power.upload');
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $store = array_change_key_case(StoreLocation::pluck('id','store_name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                $valLocationCode = [];
                foreach ($data as $key => $value) {
                    if(array_key_exists(caseChange($value->store_name),$store)){
                        $arr[$key]['store_id'] = $store[caseChange($value->store_name)];
                        $arr[$key]['man_power'] = $value->budget_man_power;
                    }else{
                        array_push($valLocationCode, $key+2);
                    }
                }

                if(count($valLocationCode) > 0){
                    return redirect('admin/man-power-upload')
                                ->with('valLocationCode', implode(', ', $valLocationCode))
                                ->with('flash_error', 'See below errors!');
                }

                if(!empty($arr)){
                    foreach ($arr as $value) {
                        StoreLocation::where('id',$value['store_id'])->update(['budget_man_power'=>$value['man_power']]);
                    }
                    // StoreLocation::insert($arr);
                    return redirect('admin/man-power')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/man-power-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }


	public function manPowerRegion(){
		$manPower = array();
		$manPowerGet = StoreLocation::where('store_location.status',1)->select(DB::raw('SUM(store_location.budget_man_power) as total_budget_man_power'),DB::raw('SUM(store_location.actual_man_power) as total_actual_man_power'),'region.name','region_id')->leftJoin('region','region.id','store_location.region_id')->groupBy('store_location.region_id')->get();
        $reports = $this->storebudgetregionwise ();
        $region_reports = $reports['region_reports'];
        $designations = $reports['designations'];
		return view('admin.report.manpower.region',compact('manPowerGet','region_reports','designations'));
    }

    public function storebudgetregionwise()
    {
        view()->share('route', 'store-budget-report');
        $region =  \App\Region::where('status',1)->get();
        $storeBudgetManPower = \App\BudgetStore::leftJoin('store_location','store_location.id','budget_stores.store_id')
                                                    ->where('store_location.status',1)
                                                    ->get();
        //$designations = \App\Designation::where('status',1)->pluck('name','id')->toArray();
        $store_reports = array();
        foreach ($region as $region ) {
            $data = array();
            $data['region_id'] = $region->id;
            $data['region_name'] = $region->name;
            if($this->arrayForLabelDesignation){
                $total = 0;
                foreach($this->arrayForLabelDesignation as $key => $value){

                    $manpowerbudget = $storeBudgetManPower->where('region_id',$region->id)->where('label',$key)->sum('budgetManpower');
                    if($manpowerbudget){
                        $total = $total+$manpowerbudget;
                        $data['designation'][] =$manpowerbudget;
                    } else {
                        $data['designation'][] = 0;
                    }
                }
                $data['designation'][] = $total;
            }
            $store_reports[] = $data;
        }
        $return['designations'] = $this->arrayForLabelDesignation;
        $return['region_reports'] = $store_reports;
        return $return;
    }
    public function manPowerbudgetRegionExport(Request $request)
    {
        $excel_export = array();
        $data = $this->storebudgetregionwise();
        $designations = $data['designations'];
        $region_reports = $data['region_reports'];
        $excel_header = array('Region Name');
        foreach ($designations as $designation) {
            $excel_header[] = $designation;
        }
        $excel_header[] = "Total";
        $excel_export[] = $excel_header;

        foreach ($region_reports as $region_report){
            $report_data = array();
            $report_data['region_name'] = $region_report['region_id'];
            foreach ($region_report['designation'] as $key => $value){
                $report_data[] =$value;
            }
            $excel_export[] =$report_data;
        }

        Excel::create('Man Power Budget Region',function($excel) use ($excel_export){
                $excel->setTitle('Man Power Budget Region');
                $excel->sheet('Man Power Budget Region',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                });
            })->download('xlsx');
        return back();
    }

    public function exportmanpower(Request $request)
    {
        $manPowerGet = StoreLocation::where('store_location.status',1)->select(DB::raw('SUM(store_location.budget_man_power) as total_budget_man_power'),DB::raw('SUM(store_location.actual_man_power) as total_actual_man_power'),'region.name','region_id')->leftJoin('region','region.id','store_location.region_id')->groupBy('store_location.region_id')->get();
        $excel_header = array('Region','Budgeted','Available','GAP','Avg GAP Aging','% GAP');
        $excel_export = array();
        $excel_export[] = $excel_header;
        foreach($manPowerGet as $key => $value){
            $data  = array(
                'Region' => $value->name,
                'Budgeted' => ($value->total_budget_man_power > 0)? $value->total_budget_man_power : 0,
                'Available' =>($value->total_actual_man_power > 0)? $value->total_actual_man_power : 0,
                'GAP' => ($value->total_budget_man_power > 0 && $value->total_actual_man_power > 0)? $value->total_budget_man_power - $value->total_actual_man_power : 0,
                'Avg GAP Aging' =>0,
                '% GAP'=>($value->total_budget_man_power > 0 && $value->total_actual_man_power > 0)? ($value->total_budget_man_power - $value->total_actual_man_power)/$value->total_budget_man_power : 0
            );
            $excel_export[] =$data;
        }

        Excel::create('Man Power Region',function($excel) use ($excel_export){
                $excel->setTitle('Man Power Region');
                $excel->sheet('Man Power Region',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                });
            })->download('xlsx');
    }

    public function exportstoremanpower(Request $request,$region_id)
    {
        $excel_header = array('Store Name','Store Code','Location','Region','Budget Man Power','Actual Man Power','GAP');
        $excel_export = array();
        $manPower = StoreLocation::select([
                            'store_location.store_name',
                            \DB::raw("CONCAT(io_code,' (',sap_code,')') as store_code"),
                            'city.name as city',
                            'region.name',
                            'store_location.budget_man_power',
                            'store_location.actual_man_power',
                            \DB::raw("(budget_man_power - actual_man_power) as gap"),
                        ])
                        ->leftjoin('city',function($join){
                            $join->on('city.id','=','store_location.city_id');
                        })
						->leftJoin('region','region.id','store_location.region_id');
		if($region_id > 0){
			$manPower->where('store_location.region_id',$region_id);
		}
	    $manPower = $manPower->orderBy('store_location.id','DESC')
        ->get()->toArray();
        $excel_export = $manPower;
        array_unshift($excel_export,$excel_header);
        Excel::create('Man Power Store',function($excel) use ($excel_export){
                $excel->setTitle('Man Power Store');
                $excel->sheet('Man Power Store',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                });
            })->download('xlsx');
    }
}
