<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Region;
use App\State;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.region');
        $this->middleware('permission:access.region.edit')->only(['edit','update']);
        $this->middleware('permission:access.region.create')->only(['create', 'store']);
        $this->middleware('permission:access.region.delete')->only('destroy');
        
        view()->share('route', 'region');
        view()->share('module', 'Region');
    }
    
    public function index(Request $request)
    {
    	return view('admin.region.index');
    }

    public function datatable(Request $request)
    {
        $region = Region::latest()->get();

        return Datatables::of($region)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $state = State::where('status','1')->pluck('name','id')->toArray();
        view()->share('state',$state);

        return view('admin.region.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        $requestData['store_states'] = implode(',', $requestData['store_states']);
     	
        $region = Region::create($requestData);
        
        return redirect('admin/region')->with('flash_success', 'Region added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $region = Region::find($id);
        
        return view('admin.region.show', compact('region'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $region = Region::find($id);

        if(!$region){
            return redirect('admin/region')->with('flash_error', 'Region Not Found!');
        }

        $state = State::where('status','1')->pluck('name','id')->toArray();
        view()->share('state',$state);
        
        return view('admin.region.edit', compact('region'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        $requestData['store_states'] = implode(',', $requestData['store_states']);
        
        $region = Region::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/region')->with('flash_success', 'Region updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $region = Region::find($id);

        $region->delete();

        if($request->has('from_index')){
            $message = "Region Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Region deleted!');

            return redirect('admin/region');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name'=>['required','max:191',new Unique('region','region',$id)],
        ];

        return $this->validate($request, $rules,[
            'name.required' => 'The Region field is required',
            'name.regex' => 'The Region format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.region.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $region = array_change_key_case(Region::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->name),$region)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->name);

                        $region = array_merge([caseChange($value->name)=>trim($value->name)], $region);
                    }
               }
                
                if(!empty($arr)){
                    Region::insert($arr);
                    return redirect('admin/region')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/region-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
