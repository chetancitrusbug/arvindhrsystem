<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Grade;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.grade');
        $this->middleware('permission:access.grade.edit')->only(['edit','update']);
        $this->middleware('permission:access.grade.create')->only(['create', 'store']);
        $this->middleware('permission:access.grade.delete')->only('destroy');
        
        view()->share('route', 'grade');
        view()->share('module', 'Grade');
    }
    
    public function index(Request $request)
    {
    	return view('admin.grade.index');
    }

    public function datatable(Request $request)
    {
        $grade = Grade::latest()->get();

        return Datatables::of($grade)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.grade.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $grade = Grade::create($requestData);
        
        return redirect('admin/grade')->with('flash_success', 'Grade added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $grade = Grade::find($id);

        if(!$grade){
            return redirect('admin/grade')->with('flash_error', 'Grade Not Found!');
        }
        
        return view('admin.grade.show', compact('storeLocation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $grade = Grade::find($id);

        if(!$grade){
            return redirect('admin/grade')->with('flash_error', 'Grade Not Found!');
        }
        
        return view('admin.grade.edit', compact('grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $grade = Grade::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/grade')->with('flash_success', 'Grade updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $grade = Grade::find($id);

        $grade->delete();

        if($request->has('from_index')){
            $message = "Grade Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Grade deleted!');

            return redirect('admin/grade');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',new Unique('grade','grade',$id)],
        ];

        return $this->validate($request, $rules,[
            'name.required' => 'The grade field is required',
            'name.regex' => 'The grade format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.grade.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $grade = array_change_key_case(Grade::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->name),$grade)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->name);

                        $grade = array_merge([caseChange($value->name)=>trim($value->name)], $grade);
                    }
                }
                
                if(!empty($arr)){
                    Grade::insert($arr);
                    return redirect('admin/grade')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/grade-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
