<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminPasswordChange extends Controller
{
    public function changepassword(Request $request)
    {
        $this->validate($request, [
             'old_password' => 'required',
             'password' => 'required|min:6|different:old_password',
             'verify_password'   =>  'required|same:password',
        ]);
        $old_password = $request->old_password;
        if (Hash::check($old_password, Auth::user()->password)) {
            Auth::user()->update(['password'=>bcrypt($request->password)]);
            Session::flash('message', 'password chnaged successfully.');
            return back();
        } else {
            Session::flash('error_message', 'Please Enter Correct Old Password.');
            return back();
        }

    }
    public function changepasswordform(Request $request)
    {
         return view('storeManager.changepassword');
    }
}
