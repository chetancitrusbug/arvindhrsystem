<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Department;
use App\SubDepartment;
use Yajra\Datatables\Datatables;

class SubDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.subdepartment');
        $this->middleware('permission:access.subdepartment.edit')->only(['edit','update']);
        $this->middleware('permission:access.subdepartment.create')->only(['create', 'store']);
        $this->middleware('permission:access.subdepartment.delete')->only('destroy');
        view()->share('route', 'sub_department');
        view()->share('module', 'Sub Department');
    }
    
    public function index(Request $request)
    {
    	return view('admin.sub_department.index');
    }

    public function datatable(Request $request)
    {
        $subDepartment = SubDepartment::select([
                            'sub_department.*',
                            'department.name as department',
                        ])
                        ->leftJoin('department',function($join){
                            $join->on('department.id','=','sub_department.department_id');
                        })->latest()->get();

        return Datatables::of($subDepartment)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);

        return view('admin.sub_department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();

        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $subDepartment = SubDepartment::create($requestData);
        
        return redirect('admin/sub-department')->with('flash_success', 'Sub Department added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $subDepartment = SubDepartment::find($id);

        if(!$subDepartment){
            return redirect('admin/sub-department')->with('flash_error', 'Sub Department Not Found!');
        }
        
        return view('admin.sub_department.show', compact('subDepartment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);
        
        $subDepartment = SubDepartment::find($id);
        
        if(!$subDepartment){
            return redirect('admin/sub-department')->with('flash_error', 'Sub Department Not Found!');
        }
        
        return view('admin.sub_department.edit', compact('subDepartment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $subDepartment = SubDepartment::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/sub-department')->with('flash_success', 'Sub Department updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $subDepartment = SubDepartment::find($id);

        $subDepartment->delete();

        if($request->has('from_index')){
            $message = "Sub Department Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Sub Department deleted!');

            return redirect('admin/sub-department');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'department_id'=>'required',
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',
                        function($attribute, $value, $fail) use($request,$id) {
                            $subDepartment = SubDepartment::where('department_id',$request->department_id)->where('name',$value)->first();
                            if ($subDepartment && $id != $subDepartment->id) {
                                $fail('sub department already exist');
                            }
                        },
                    ],
        ];
        
        return $this->validate($request, $rules,[
            'department_id.required' => 'The department field is required',
            'name.required' => 'The sub department field is required',
        ]);
    }

    public function showUpload()
    {
        return view('admin.sub_department.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $department = array_change_key_case(Department::pluck('id','name')->toArray(), CASE_LOWER);
            $subDepartment = array_change_key_case(SubDepartment::pluck('department_id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    // if(!array_key_exists(caseChange($value->name),$subDepartment)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->name);

                        if(array_key_exists(caseChange($value->department),$department)){
                            $arr[$key]['department_id'] = $department[caseChange($value->department)];
                        }else{
                            $departmentInsert['name'] = $value->department;
                            $departmentInsert['status'] = 1;
                            $departmentCreate = Department::create($departmentInsert);
                            $arr[$key]['department_id'] = $departmentCreate->id;
                            $department = array_merge([trim($value->department)=>$departmentCreate->id], $department);
                        }

                        $subDepartment = array_merge([caseChange($value->name)=>$arr[$key]['department_id']], $subDepartment);
                    // }
                }
                
                if(!empty($arr)){
                    SubDepartment::insert($arr);
                    return redirect('admin/sub-department')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/sub-department-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
