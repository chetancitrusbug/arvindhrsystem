<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserUpload;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\SourceDetail;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use Excel;

class UserUploadController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.user_upload.index');
    }
    public function useruploaddatatable(Request $request)
    {
        $designation = UserUpload::selectRaw('employee.full_name,employee.email,user_uploads.*')
                            ->leftjoin('employee','employee.employee_code','user_uploads.employee_code')->get();
        return Datatables::of($designation)
            ->make(true);

    }
    public function show(Request $request,$id)
    {
        $user_upload = UserUpload::leftjoin('idea_categories','idea_categories.id','user_uploads.idea_category')
                                    ->where('user_uploads.id',$id)
                                    ->first();
        $employee = array();

        if($user_upload){
            $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->where('employee_code',$user_upload->employee_code)->first();
            if($employee){
                 /* return redirect('admin/user-upload')->with('flash_error', 'Opps Employee Not Found!'); */
                $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
                $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
                $employee->family = FamilyInfo::select([
                                'family_information.*',
                                \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                            ])->where('employee_id',$id)->get()->toArray();

                $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
                view()->share('educationCategory',$educationCategory);

                $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
                view()->share('natureOfEmployment',$natureOfEmployment);
            }
            return view('admin.user_upload.user_code', compact('employee','user_upload'));
        }
        return redirect('admin/user-upload')->with('flash_error', 'Opps Something went wrong');

    }
    public function ExportToExcel(Request $request)
    {
        $user_upload = UserUpload::selectRaw('employee.full_name,employee.email,user_uploads.*,idea_categories.cat_name')
                            ->leftjoin('employee','employee.employee_code','user_uploads.employee_code')
                            ->leftjoin('idea_categories','idea_categories.id','user_uploads.idea_category')
                            ->orderby('user_uploads.id')
                            ->get();

        $excel_hearder= array('ID','Employee Code','Employee Name','Employee Email','Idea Name','Idea Category','Description','Expected Out Come','User File Upload Path');
        $excel_export[] = $excel_hearder;
        if($user_upload){
            foreach ($user_upload as $idea) {
                $excel_export[]= array(
                    'ID'=>$idea->id,
                    'Employee Code'=>$idea->employee_code,
                    'Employee Name'=>$idea->full_name,
                    'Employee Email'=>$idea->email,
                    'Idea Name'=>$idea->idea_name,
                    'Idea Category'=>$idea->cat_name,
                    'Description'=>$idea->description,
                    'Expected Out Come'=>$idea->expected_out_come,
                    'User File Upload Path'=>($idea->fileuploadPath)?url('/uploads/Userfile/')."/".$idea->fileuploadPath:''
                );
            }
            Excel::create('User Suggestions',function($excel) use ($excel_export){
                $excel->setTitle('User Suggestions');
                $excel->sheet('User Suggestions',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                });
            })->download('xlsx');
        } else {

        }
    }
}
