<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\SourceCategory;
use App\SourceDetail;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class SourceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.sourcecategory');
        $this->middleware('permission:access.sourcecategory.edit')->only(['edit','update']);
        $this->middleware('permission:access.sourcecategory.create')->only(['create', 'store']);
        $this->middleware('permission:access.sourcecategory.delete')->only('destroy');
        
        view()->share('route', 'source_category');
        view()->share('module', 'Source Category');
    }
    
    public function index(Request $request)
    {
    	return view('admin.source_category.index');
    }

    public function datatable(Request $request)
    {
        $sourceCategory = SourceCategory::latest()->get();

        return Datatables::of($sourceCategory)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.source_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $sourceCategory = SourceCategory::create($requestData);
        
        return redirect('admin/source-category')->with('flash_success', 'Source Category added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $sourceCategory = SourceCategory::find($id);
        
        return view('admin.source_category.show', compact('sourceCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $sourceCategory = SourceCategory::find($id);

        if(!$sourceCategory){
            return redirect('admin/source-category')->with('flash_error', 'Source Category Not Found!');
        }
        
        return view('admin.source_category.edit', compact('sourceCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $sourceCategory = SourceCategory::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/source-category')->with('flash_success', 'Source Category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $sourceCategory = SourceCategory::find($id);
        
        $sourceCategory->delete();

        if($request->has('from_index')){
            $message = "Source Category Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Source Category deleted!');

            return redirect('admin/source-category');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'category'=>'required|regex:/^[a-z0-9 .\-]+$/i',
            /*'source_name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',
                                function($attribute, $value, $fail) use($request,$id) {
                                    $category = SourceCategory::where('category',$request->category)->where('source_name',$request->source_name)->first();
                                    if ($category && $id != $category->id) {
                                        $fail('source name already exist');
                                    }
                                },
                        ],
            'source_code'=>'required|max:191|regex:/^[a-z0-9 .\-]+$/i',*/
        ];

        return $this->validate($request, $rules);
    }

    public function showUpload()
    {
        return view('admin.source_category.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $sourceCategory = array_change_key_case(SourceCategory::pluck('category','id')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    $arr[$key]['status'] = 1;
                    $arr[$key]['source_name'] = trim($value->source_name);
                    $arr[$key]['source_code'] = trim($value->source_code);
                    $arr[$key]['source_category_id'] = trim($value->category);

                    if(array_key_exists(caseChange($value->category),$sourceCategory)){
                        $arr[$key]['source_category_id'] = $sourceCategory[caseChange($value->category)];
                    }else{
                        $sourceCategoryInsert['category'] = $value->category;
                        $sourceCategoryInsert['status'] = 1;
                        $sourceCategoryCreate = SourceCategory::create($sourceCategoryInsert);
                        $arr[$key]['source_category_id'] = $sourceCategoryCreate->id;
                        $sourceCategory = array_merge([caseChange($value->category)=>$sourceCategoryCreate->id], $sourceCategory);
                    }
                }
                // echo "<pre>"; print_r($arr); exit();
                if(!empty($arr)){
                    SourceDetail::insert($arr);
                    return redirect('admin/source-category')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/source-category-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
