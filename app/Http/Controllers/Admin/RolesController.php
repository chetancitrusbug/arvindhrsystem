<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Role;
use App\Permission;
use Session;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:access.roles');
        $this->middleware('permission:access.role.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.role.create')->only(['create', 'store']);
        $this->middleware('permission:access.role.delete')->only('destroy');
        view()->share('route', 'role');
        view()->share('module', 'Role');
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $roles = Role::where('name', 'LIKE', "%$keyword%")->orWhere('label', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $roles = Role::latest()->paginate($perPage);
        }

        return view('admin.roles.index', compact('roles'));
    }

    public function datatable(){
        //$roles = Role::all();
        $roles = Role::where('id','>',0);
        return Datatables::of($roles)
            ->make(true);
            exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $permissions = Permission::with('child')->parent()->get();

        $isChecked = function ($name) {
            return '';
        };

        return view('admin.roles.create', compact('permissions', 'isChecked'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'permissions' => 'required']);

        $requestData = $request->all();

        $role = Role::create($requestData);

        $role->permissions()->detach();

        foreach ($request->permissions as $permission_name) {
            $permission = Permission::whereName($permission_name)->first();
            $role->givePermissionTo($permission);
        }

        // \ActionLog::addToLog("Add Role"," New Role ". $role->label ." is added ",$role->getTAble(),$role->id);

        Session::flash('flash_success', __('Role added!'));

        return redirect('admin/roles/' . $role->id . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $role = Role::with('main_permission.child')->lower()->findOrFail($id);

        $permissions = Permission::with('child')->parent()->get();

        return view('admin.roles.show', compact('role', 'permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
        $role = Role::with('permissions')->findOrFail($id);

        $permissions = Permission::with('child')->parent()->get();

        $isChecked = function ($name) use ($role) {

            if ($role->permissions->contains('name', $name)) {
                return 'checked';
            }
            return '';
        };

        return view('admin.roles.edit', compact('role', 'permissions','isChecked'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'permissions' => 'required']);

        $role = Role::findOrFail($id);

        $requestData = $request->all();

        $role->update($requestData);

        $role->permissions()->detach();

        foreach ($request->permissions as $permission_name) {
            $permission = Permission::whereName($permission_name)->first();
            $role->givePermissionTo($permission);
        }

        // \ActionLog::addToLog("Edit Role"," Role ". $role->label ." is updated ",$role->getTAble(),$role->id);


        Session::flash('flash_success', __('Role updated!'));

        return redirect('admin/roles/' . $id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        Role::destroy($id);

        if($request->has('from_index')){
            $message = "Role Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Role deleted!');

            return redirect('admin/roles');
        }
    }
}
