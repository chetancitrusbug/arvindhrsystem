<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Department;
use App\SubDepartment;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.department');
        $this->middleware('permission:access.department.edit')->only(['edit','update']);
        $this->middleware('permission:access.department.create')->only(['create', 'store']);
        $this->middleware('permission:access.department.delete')->only('destroy');
        
        view()->share('route', 'department');
        view()->share('module', 'Department');
    }
    
    public function index(Request $request)
    {
    	return view('admin.department.index');
    }

    public function datatable(Request $request)
    {
        $department = Department::latest()->get();

        return Datatables::of($department)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $department = Department::create($requestData);
        
        return redirect('admin/department')->with('flash_success', 'Department added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $department = Department::find($id);

        if(!$department){
            return redirect('admin/department')->with('flash_error', 'Department Not Found!');
        }
        
        return view('admin.department.show', compact('storeLocation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $department = Department::find($id);

        if(!$department){
            return redirect('admin/department')->with('flash_error', 'Department Not Found!');
        }
        
        return view('admin.department.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $department = Department::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/department')->with('flash_success', 'Department updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $department = Department::find($id);
        SubDepartment::where('department_id',$id)->delete();
        
        $department->delete();

        if($request->has('from_index')){
            $message = "Department Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Department deleted!');

            return redirect('admin/department');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',new Unique('department','department',$id)],
        ];

        return $this->validate($request, $rules,[
            'name.required' => 'The department field is required',
            'name.regex' => 'The department format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.department.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $department = array_change_key_case(Department::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->name),$department)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->name);

                        $department = array_merge([caseChange($value->name)=>trim($value->name)], $department);
                    }
               }
                
                if(!empty($arr)){
                    Department::insert($arr);
                    return redirect('admin/department')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/department-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
