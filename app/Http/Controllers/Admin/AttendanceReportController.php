<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Yajra\Datatables\Datatables;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use Carbon\Carbon as Carbon;
use App\HiringReport;
use App\Resignation;
use App\ResignationReason;
use DB;
use DateTime;
use Carbon\CarbonPeriod;


class AttendanceReportController extends Controller
{
	public function region(Request $request)
    {
		$start_date = \Carbon\Carbon::now()->startOfDay()->subDays(1);
		$end_date = \Carbon\Carbon::now()->startOfDay()->subDays(1);

		if($request->has('start_date') && $request->start_date && $request->start_date != ""){
			$start_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->start_date);
		}
		if($request->has('end_date') && $request->end_date && $request->end_date != ""){
			$end_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->end_date);
		}

		$diff = 1 + $start_date->diffInDays($end_date);

		$start_date = $start_date->format("Y-m-d");
		$end_date = $end_date->format("Y-m-d");


		$regiondata = StoreLocation::where('store_location.status',1)->select(DB::raw('SUM(store_location.actual_man_power) as total_actual_man_power'),'region.name',"store_location.id",'store_location.region_id')->join('region','region.id','store_location.region_id')->groupBy('store_location.region_id')->get();

		if($regiondata->count() >0){
			$regiondata = $regiondata->toArray();

			for($i=0; $i< count($regiondata); $i++){
				$man_power_p = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
				->join('store_location','store_location.id','employee.store_id')
                ->where('store_location.region_id', '=',$regiondata[$i]['region_id'])
                ->whereNotIn('employee.status',['pending','cancel'])
                ->wherenull('store_location.deleted_at')
				->where('attendance','p')
				->count();

				$man_power_l = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
                ->join('store_location','store_location.id','employee.store_id')
                ->where('store_location.region_id', '=',$regiondata[$i]['region_id'])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','l')
				->count();

				$man_power_wo = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
                ->join('store_location','store_location.id','employee.store_id')
                ->where('store_location.region_id', '=',$regiondata[$i]['region_id'])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','wo')
				->count();

				$man_power_co = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
                ->join('store_location','store_location.id','employee.store_id')
                ->where('store_location.region_id', '=',$regiondata[$i]['region_id'])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','co')
				->count();

				$regiondata[$i]['man_power_p'] = round($man_power_p/$diff);
				$regiondata[$i]['man_power_l'] = round($man_power_l/$diff);
				$regiondata[$i]['man_power_wo'] = round($man_power_wo/$diff);
				$regiondata[$i]['man_power_co'] = round($man_power_co/$diff);

				if($regiondata[$i]['total_actual_man_power'] && $regiondata[$i]['total_actual_man_power'] > 0){
					$regiondata[$i]['man_power_p_per'] =	round(($regiondata[$i]['man_power_p'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
					$regiondata[$i]['man_power_l_per'] =	round(($regiondata[$i]['man_power_l'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
					$regiondata[$i]['man_power_wo_per'] =	round(($regiondata[$i]['man_power_wo'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
					$regiondata[$i]['man_power_co_per'] =	round(($regiondata[$i]['man_power_co'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
				}else{
					$regiondata[$i]['man_power_p_per'] = 0;
					$regiondata[$i]['man_power_l_per'] = 0;
					$regiondata[$i]['man_power_wo_per'] =0;
					$regiondata[$i]['man_power_co_per'] =0;
				}
			}
		//	echo "<pre>"; print_r($regiondata); exit;
		}else{
			$regiondata = [];
		}



		$allData = $this->getAlldata($request);

          return view('admin.report.attendance-region.index',compact('regiondata','allData'));


    }
    public function storevise($region_id,Request $request)
    {
		$start_date = \Carbon\Carbon::now()->startOfDay()->subDays(1);
		$end_date = \Carbon\Carbon::now()->startOfDay()->subDays(1);

		if($request->has('start_date') && $request->start_date && $request->start_date != ""){
			$start_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->start_date);
		}
		if($request->has('end_date') && $request->end_date && $request->end_date != ""){
			$end_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->end_date);
		}

		$diff = 1 + $start_date->diffInDays($end_date);

		$start_date = $start_date->format("Y-m-d");
		$end_date = $end_date->format("Y-m-d");

        $region = Region::find($region_id);
		$regiondata = StoreLocation::where('store_location.status',1)->select('actual_man_power as total_actual_man_power','store_name as name',"id",'store_location.region_id','store_location.io_code');

		if($region_id &&  $region_id != ""){
			$regiondata->where('store_location.region_id',$region_id);
		}

		$regiondata = $regiondata->get();

		if($regiondata->count() >0){
			$regiondata = $regiondata->toArray();

			for($i=0; $i< count($regiondata); $i++){
				$man_power_p = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
                ->where('employee.store_id', '=',$regiondata[$i]['id'])
                ->whereNotIn('employee.status',['pending','cancel'])
                ->where('attendance','p')
				->count();

				$man_power_l = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
                ->where('employee.store_id', '=',$regiondata[$i]['id'])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','l')
				->count();

				$man_power_wo = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
                ->where('employee.store_id', '=',$regiondata[$i]['id'])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','wo')
				->count();

				$man_power_co = \App\Attendance::join('employee','employee.id','attendance.employee_id')
				->whereBetween('full_date',[$start_date,$end_date])
                ->where('employee.store_id', '=',$regiondata[$i]['id'])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','co')
				->count();

				$regiondata[$i]['man_power_p'] = round($man_power_p/$diff);
				$regiondata[$i]['man_power_l'] = round($man_power_l/$diff);
				$regiondata[$i]['man_power_wo'] = round($man_power_wo/$diff);
				$regiondata[$i]['man_power_co'] = round($man_power_co/$diff);

				if($regiondata[$i]['total_actual_man_power'] && $regiondata[$i]['total_actual_man_power'] > 0){
					$regiondata[$i]['man_power_p_per'] =	round(($regiondata[$i]['man_power_p'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
					$regiondata[$i]['man_power_l_per'] =	round(($regiondata[$i]['man_power_l'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
					$regiondata[$i]['man_power_wo_per'] =	round(($regiondata[$i]['man_power_wo'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
					$regiondata[$i]['man_power_co_per'] =	round(($regiondata[$i]['man_power_co'] * 100) / $regiondata[$i]['total_actual_man_power'], 2);
				}else{
					$regiondata[$i]['man_power_p_per'] = 0;
					$regiondata[$i]['man_power_l_per'] = 0;
					$regiondata[$i]['man_power_wo_per'] =0;
					$regiondata[$i]['man_power_co_per'] =0;
				}
			}
		//	echo "<pre>"; print_r($regiondata); exit;
		}else{
			$regiondata = [];
		}

          return view('admin.report.attendance-region.store-all',compact('regiondata','region_id','region'));


    }

	public function getAlldata($request)
    {
		$start_date = \Carbon\Carbon::now()->startOfDay()->subDays(1);
		$end_date = \Carbon\Carbon::now()->startOfDay()->subDays(1);

		if($request->has('start_date') && $request->start_date && $request->start_date != ""){
			$start_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->start_date);
		}
		if($request->has('end_date') && $request->end_date && $request->end_date != ""){
			$end_date = \Carbon\Carbon::createFromFormat("Y-m-d",$request->end_date);
		}

				$diff = 1 + $start_date->diffInDays($end_date);

				$start_date = $start_date->format("Y-m-d");
				$end_date = $end_date->format("Y-m-d");


				$manPowerGet = \App\StoreLocation::where('store_location.status',1)
					->select(DB::raw('SUM(store_location.actual_man_power) as total_actual_man_power'))
					->first();

				if($manPowerGet){
					$actual_man_power = round($manPowerGet->total_actual_man_power);
				}else{
					$actual_man_power = 0;
				}


				$man_power_p = \App\Attendance::join('employee','employee.id','attendance.employee_id')
                ->whereBetween('full_date',[$start_date,$end_date])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','p')
				->count();

				$man_power_l = \App\Attendance::join('employee','employee.id','attendance.employee_id')
                ->whereBetween('full_date',[$start_date,$end_date])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','l')
				->count();

				$man_power_wo = \App\Attendance::join('employee','employee.id','attendance.employee_id')
                ->whereBetween('full_date',[$start_date,$end_date])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','wo')
				->count();

				$man_power_co = \App\Attendance::join('employee','employee.id','attendance.employee_id')
                ->whereBetween('full_date',[$start_date,$end_date])
                ->whereNotIn('employee.status',['pending','cancel'])
				->where('attendance','co')
				->count();

				$man_power_p = round($man_power_p/$diff);
				$man_power_l = round($man_power_l/$diff);
				$man_power_wo = round($man_power_wo/$diff);
				$man_power_co = round($man_power_co/$diff);

				if($actual_man_power > 0){
					$man_power_p_per =	round(($man_power_p * 100) / $actual_man_power, 2);
					$man_power_l_per =	round(($man_power_l * 100) / $actual_man_power, 2);
					$man_power_wo_per =	round(($man_power_wo * 100) / $actual_man_power, 2);
					$man_power_co_per =	round(($man_power_co * 100) / $actual_man_power, 2);
				}else{
					$man_power_p_per =	0;
					$man_power_l_per =	0;
					$man_power_wo_per =	0;
					$man_power_co_per =	0;
				}

				$regiondata = [
							"actual_man_power"=>$actual_man_power,
								"man_power_p"=>$man_power_p,"man_power_l"=>$man_power_l,"man_power_wo"=>$man_power_wo,"man_power_co"=>$man_power_co,
								"man_power_p_per"=>$man_power_p_per,"man_power_l_per"=>$man_power_l_per,"man_power_wo_per"=>$man_power_wo_per,"man_power_co_per"=>$man_power_co_per
							  ];

				return $regiondata;

       //return view('admin.report.attendance-all',compact('regiondata'));
    }

    public function EmployeeAttandance(Request $request,$store_id)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        if($request->has('filter')){
            $start_date  = $request->filter['from_date'];
            $end_date = $request->filter['to_date'];
            return redirect("admin/attendance-report/employee/$store_id/?start_date=$start_date&end_date=$end_date");
        } else if(Input::get('start_date') && Input::get('end_date') ){
            $start_date  = Input::get('start_date');
            $end_date = Input::get('end_date');
        } else {
            $start_date  = \Carbon\Carbon::now()->startOfDay()->subDays(1);
            $end_date = \Carbon\Carbon::now()->startOfDay()->subDays(1);
        }


        $attandance_data = array();
        $filter['from_date']= $start_date;
        $filter['to_date']= $end_date;

        $title = "ATTENDANCE FOR THE MONTH OF ". strtoupper(date('d F Y', strtotime($start_date))). " to ".strtoupper(date('d F Y', strtotime($end_date)));
        $title_array = array($title);
        $total_date = array("Arvind Lifestyle Brands Ltd");
        $excel_hearder = array('SR NO','Employee Code','Employee Name','DOJ','DESIGNATION','Store-Office Name','IO CODE','STORE CODE-RETAK CODE');
        $store = StoreLocation::find($store_id);
        $total_days = array($store->store_name);
        $start_date = new DateTime( $start_date );
        $end_date = new DateTime( $end_date );
        $end_date = $end_date->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start_date, $interval ,$end_date);
         foreach ($period as $key => $value ){
                array_push($excel_hearder,$value->format('l')." - ".$value->format('d/m/Y'));
                array_push($total_date,$value->format('j'));
                array_push($total_days,$value->format('l'));
                array_push($title_array,$value->format('d/m/Y'));
         }
         array_push($excel_hearder,"P","W/O","C/O","L","X","Days","Remarks");
        $sr_no = 1;
         $attandance_data[] = $total_date;
        $attandance_data[] = $total_days;
        $attandance_data[] = $title_array;

        $attandance_data[] = $excel_hearder;
        $employees = Employee::whereNotIn('employee.status',['pending','cancel'])->selectRaw('employee.*,store_location.store_name,store_location.io_code,store_location.sap_code')
                        ->Leftjoin('store_location','store_location.id','employee.store_id')
                        ->where('employee.store_id',$store_id)
                        ->where('store_location.status',1)
                        ->with(['designation:id,name','attendance' => function ($q) use ($start_date,$end_date)  {
                            $q->whereBetween('full_date',[$start_date,$end_date]);
                        }])
                        ->get();
        // $attandanceemployees  = \App\Attendance::join('employee','employee.id','attendance.employee_id')
        //                             ->Leftjoin('store_location','store_location.id','employee.store_id')
        //                             ->where('employee.store_id',$store_id)
        //                             ->whereNotIn('employee.status',['pending','cancel'])
        //                             ->whereBetween('full_date',[$start_date,$end_date])
        //                             ->get();

        foreach($employees as $employee){
            $attandance_report = array();
            $attandance_report[$employee->id]['SR NO'] = $sr_no;
            $attandance_report[$employee->id]['Employee Code'] = $employee->employee_code;
            $attandance_report[$employee->id]['Employee Name'] = $employee->full_name;
            $attandance_report[$employee->id]['DOJ'] = $employee->joining_date;
            $attandance_report[$employee->id]['DESIGNATION'] = $employee->designation_name;
            $attandance_report[$employee->id]['Store-Office Name'] = $employee->store_name;
            $attandance_report[$employee->id]['IO CODE'] = $employee->io_code;
            $attandance_report[$employee->id]['STORE CODE-RETAK CODE'] = $employee->sap_code;
            $x = 00; $wo = 00; $co = 00; $l = 00; $p =00; $remark = ''; $days = 00;
            foreach ($period as $key => $value ){
                $attandance = [];
                if($employee->attendance){
                    $attandance = $employee->attendance->where('full_date',$value->format('Y-m-d'))->where('employee_id',$employee->id)->first();
                }
                if($attandance){
                    ($attandance->attendance == 'p')?$p++:'';
                    ($attandance->attendance == 'l')?$l++:'';
                    ($attandance->attendance == 'co')?$co++:'';
                    ($attandance->attendance == 'wo')?$wo++:'';
                    ($attandance->attendance == 'wo' || $attandance->attendance == 'p')?$days++:'';
                    $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = strtoupper($attandance->attendance);
                } else {
                    $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = "-";
                }

            }
            $attandance_report[$employee->id]["P"] = $p;
            $attandance_report[$employee->id]["W/O"] = $wo;
            $attandance_report[$employee->id]["C/O"] = $co;
            $attandance_report[$employee->id]["L"] = $l;
            $attandance_report[$employee->id]["X"] = $x;
            $attandance_report[$employee->id]["Days"] = $days;
            $attandance_report[$employee->id]["Remarks"] = $remark;
            $sr_no++;
            $attandance_data[] = $attandance_report[$employee->id];
        }
        return view('admin.report.attendance-region.employeereport',compact('attandance_data','filter'));
    }

    public function attendencecsvExportstore(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        $excel_export = array();
        $this->validate($request, [
            'export_start_date' => 'required',
            'export_end_date' => 'required',
            'store_id' =>'required'
        ]);
        $store_id = $request->store_id;
        $start_date = $request->export_start_date;
        $end_date = $request->export_end_date;
        $store = \App\StoreLocation::find($store_id);

        $title = "ATTENDANCE FOR THE MONTH OF ". strtoupper(date('d F Y', strtotime($start_date))). " to ".strtoupper(date('d F Y', strtotime($end_date)));
        $title_array = array($title,'','','','','','','');
        $total_date = array("Arvind Lifestyle Brands Ltd",'','','','','','','');
        $total_days = array($store->store_name,'','','','','','','');
        $excel_hearder = array('SR NO','Employee Code','Employee Name','DOJ','DESIGNATION','Store-Office Name','IO CODE','STORE CODE-RETAK CODE');

        $start_date = new DateTime( $start_date );;
        $end_date = new DateTime( $end_date );;
        $end_date = $end_date->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start_date, $interval ,$end_date);
         foreach ($period as $key => $value ){
                array_push($excel_hearder,$value->format('l')." - ".$value->format('d/m/Y'));
                array_push($total_date,$value->format('j'));
                array_push($total_days,$value->format('l'));
                array_push($title_array,$value->format('d/m/Y'));
         }
         array_push($excel_hearder,"P","W/O","C/O","L","X","Days","Remarks");
         $excel_export[] = $total_date;
        $excel_export[] = $total_days;
        $excel_export[] = $title_array;

        $excel_export[] = $excel_hearder;
        $sr_no = 1;

        $employees = Employee::whereNotIn('employee.status',['pending','cancel'])->selectRaw('employee.*,store_location.store_name,store_location.io_code,store_location.sap_code')
                        ->Leftjoin('store_location','store_location.id','employee.store_id')
                        ->where('employee.store_id',$store_id)
                        ->where('store_location.status',1)
                        ->with(['designation:id,name','attendance' => function ($q) use ($start_date,$end_date)  {
                            $q->whereBetween('full_date',[$start_date,$end_date]);
                        }])
                        ->get();
        // $attandanceemployees  = \App\Attendance::join('employee','employee.id','attendance.employee_id')
        //                             ->where('employee.store_id',$store_id)
        //                             ->whereNotIn('employee.status',['pending','cancel'])
        //                             ->whereBetween('full_date',[$start_date,$end_date])
        //                             ->get();

        foreach($employees as $employee){
            $attandance_report = array();
            $attandance_report[$employee->id]['SR NO'] = $sr_no;
            $attandance_report[$employee->id]['Employee Code'] = $employee->employee_code;
            $attandance_report[$employee->id]['Employee Name'] = $employee->full_name;
            $attandance_report[$employee->id]['DOJ'] = $employee->joining_date;
            $attandance_report[$employee->id]['DESIGNATION'] = $employee->designation_name;
            $attandance_report[$employee->id]['Store-Office Name'] = $employee->store_name;
            $attandance_report[$employee->id]['IO CODE'] = $employee->io_code;
            $attandance_report[$employee->id]['STORE CODE-RETAK CODE'] = $employee->sap_code;
            $x = 00; $wo = 00; $co = 00; $l = 00; $p =00; $remark = ''; $days = 00;
            foreach ($period as $key => $value ){
                $attandance = [];
                if($employee->attendance){
                    $attandance = $employee->attendance->where('full_date',$value->format('Y-m-d'))->where('employee_id',$employee->id)->first();
                }
                if($attandance){
                    ($attandance->attendance == 'p')?$p++:'';
                    ($attandance->attendance == 'l')?$l++:'';
                    ($attandance->attendance == 'co')?$co++:'';
                    ($attandance->attendance == 'wo')?$wo++:'';
                    ($attandance->attendance == 'wo' || $attandance->attendance == 'p')?$days++:'';
                    $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = strtoupper($attandance->attendance);
                } else {
                    $attandance_report[$employee->id][$value->format('l')." - ".$value->format('d/m/Y')] = "-";
                }

            }
            $attandance_report[$employee->id]["P"] = $p;
            $attandance_report[$employee->id]["W/O"] = $wo;
            $attandance_report[$employee->id]["C/O"] = $co;
            $attandance_report[$employee->id]["L"] = $l;
            $attandance_report[$employee->id]["X"] = $x;
            $attandance_report[$employee->id]["Days"] = $days;
            $attandance_report[$employee->id]["Remarks"] = $remark;
            $sr_no++;
            $excel_export[] = $attandance_report[$employee->id];
        }

        \Excel::create($title,function($excel) use ($excel_export){
                $excel->setTitle('Attandace Reports');
                $excel->sheet('Attandace Reports',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                    $sheet->mergeCells('A1:H1');
                    $sheet->mergeCells('A2:H2');
                    $sheet->mergeCells('A3:H3');
                    $sheet->setAllBorders('thin');
                    $sheet->getStyle('A1')->applyFromArray(array(
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '91d3ff')
                        )
                    ));

                    $sheet->getStyle('A2')->applyFromArray(array(
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'e2cce2')
                        )
                    ));


                    $sheet->getStyle('A3')->applyFromArray(array(
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'dc80d8')
                        )
                    ));

                    $sheet->getStyle('A4:H4')->applyFromArray(array(
                        'fill' => array(
                            'type'  => \PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '91d3ff')
                        )
                    ));
                });
            })->download('xlsx');
           return back();
    }


}
