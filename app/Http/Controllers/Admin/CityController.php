<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\City;
use App\State;
use Yajra\Datatables\Datatables;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.city');
        $this->middleware('permission:access.city.edit')->only(['edit','update']);
        $this->middleware('permission:access.city.create')->only(['create', 'store']);
        $this->middleware('permission:access.city.delete')->only('destroy');

        view()->share('route', 'city');
        view()->share('module', 'City');
    }
    
    public function index(Request $request)
    {
    	return view('admin.city.index');
    }

    public function datatable(Request $request)
    {
        $city = City::select([
                    'city.*',
                    'state.name as state',
                ])->leftJoin('state',function($join){
                    $join->on('state.id','=','city.state_id');
                })->latest()->get();

        return Datatables::of($city)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        return view('admin.city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $city = City::create($requestData);
        
        return redirect('admin/city')->with('flash_success', 'City added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $city = City::find($id);
        
        return view('admin.city.show', compact('city'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        $city = City::find($id);

        if(!$city){
            return redirect('admin/city')->with('flash_error', 'City Not Found!');
        }
        
        return view('admin.city.edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $city = City::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/city')->with('flash_success', 'City updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $city = City::find($id);
        
        $city->delete();

        if($request->has('from_index')){
            $message = "City Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'City deleted!');

            return redirect('admin/city');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'state_id'=>'required',
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',
                        function($attribute, $value, $fail) use($request,$id) {
                            $city = City::where('state_id',$request->state_id)->where('name',$value)->first();
                            if ($city && $id != $city->id) {
                                $fail('City already exist');
                            }
                        },
                    ],
        ];

        return $this->validate($request, $rules,[
            'state_id.required' => 'The State field is required',
            'name.required' => 'The City field is required',
            'name.regex' => 'The City format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.city.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $state = array_change_key_case(State::pluck('id','name')->toArray(), CASE_LOWER);
            $city = array_change_key_case(city::pluck('state_id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->city),$city)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->city);

                        if(array_key_exists(caseChange($value->state),$state)){
                            $arr[$key]['state_id'] = $state[caseChange($value->state)];
                        }else{
                            $stateInsert['name'] = $value->state;
                            $stateInsert['status'] = 1;
                            $stateCreate = State::create($stateInsert);
                            $arr[$key]['state_id'] = $stateCreate->id;
                            $state = array_merge([trim($value->state)=>$stateCreate->id], $state);
                        }

                        $city = array_merge([caseChange($value->city)=>$arr[$key]['state_id']], $city);
                    }
                }
                
                if(!empty($arr)){
                    City::insert($arr);
                    return redirect('admin/city')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/city-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
