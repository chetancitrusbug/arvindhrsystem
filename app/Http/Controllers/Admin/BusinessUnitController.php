<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\BusinessUnit;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class BusinessUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.businessunit');
        $this->middleware('permission:access.businessunit.edit')->only(['edit','update']);
        $this->middleware('permission:access.businessunit.create')->only(['create', 'store']);
        $this->middleware('permission:access.businessunit.delete')->only('destroy');
        
        view()->share('route', 'business_unit');
        view()->share('module', 'Business Unit');
    }
    
    public function index(Request $request)
    {
    	return view('admin.business_unit.index');
    }

    public function datatable(Request $request)
    {
        $businessUnit = BusinessUnit::latest()->get();

        return Datatables::of($businessUnit)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.business_unit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $businessUnit = BusinessUnit::create($requestData);
        
        return redirect('admin/business-unit')->with('flash_success', 'Business Unit added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $businessUnit = BusinessUnit::find($id);

        if(!$businessUnit){
            return redirect('admin/business-unit')->with('flash_error', 'Business Unit Not Found!');
        }
        
        return view('admin.business_unit.show', compact('businessUnit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $businessUnit = BusinessUnit::find($id);

        if(!$businessUnit){
            return redirect('admin/business-unit')->with('flash_error', 'Business Unit Not Found!');
        }
        
        return view('admin.business_unit.edit', compact('businessUnit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $businessUnit = BusinessUnit::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/business-unit')->with('flash_success', 'Business Unit updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $businessUnit = BusinessUnit::find($id);
        
        $businessUnit->delete();

        if($request->has('from_index')){
            $message = "Business Unit Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Business Unit deleted!');

            return redirect('admin/business-unit');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',new Unique('business_unit','business unit',$id)],
        ];

        return $this->validate($request, $rules,[
            'name.required' => 'The Business Unit field is required',
            'name.regex' => 'The Business Unit format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.business_unit.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $businessUnit = array_change_key_case(BusinessUnit::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->name),$businessUnit)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->name);

                        $businessUnit = array_merge([caseChange($value->name)=>trim($value->name)], $businessUnit);
                    }
               }
                
                if(!empty($arr)){
                    BusinessUnit::insert($arr);
                    return redirect('admin/business-unit')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/business-unit-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
