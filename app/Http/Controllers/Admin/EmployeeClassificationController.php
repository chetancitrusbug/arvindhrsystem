<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\EmployeeClassification;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class EmployeeClassificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.employeeclassification');
        $this->middleware('permission:access.employeeclassification.edit')->only(['edit','update']);
        $this->middleware('permission:access.employeeclassification.create')->only(['create', 'store']);
        $this->middleware('permission:access.employeeclassification.delete')->only('destroy');
        
        view()->share('route', 'employee_classification');
        view()->share('module', 'Employee Classification');
    }
    
    public function index(Request $request)
    {
        return view('admin.employee_classification.index');
    }

    public function datatable(Request $request)
    {
        $employeeClassification = EmployeeClassification::latest()->get();

        return Datatables::of($employeeClassification)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.employee_classification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $employeeClassification = EmployeeClassification::create($requestData);
        
        return redirect('admin/employee-classification')->with('flash_success', 'Employee Classification added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employeeClassification = EmployeeClassification::find($id);
        
        return view('admin.employee_classification.show', compact('employeeClassification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employeeClassification = EmployeeClassification::find($id);

        if(!$employeeClassification){
            return redirect('admin/employee-classification')->with('flash_error', 'Employee Classification Not Found!');
        }
        
        return view('admin.employee_classification.edit', compact('employeeClassification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $employeeClassification = EmployeeClassification::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/employee-classification')->with('flash_success', 'Employee Classification updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $employeeClassification = EmployeeClassification::find($id);
        
        $employeeClassification->delete();

        if($request->has('from_index')){
            $message = "Employee Classification Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee Classification deleted!');

            return redirect('admin/employee-classification');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',new Unique('employee_classification','employee classification',$id)],
        ];

        return $this->validate($request, $rules,[
            'name.required' => 'The Employee Classification field is required',
            'name.regex' => 'The Employee Classification format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.employee_classification.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $employeeClassification = array_change_key_case(EmployeeClassification::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->name),$employeeClassification)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->name);

                        $employeeClassification = array_merge([caseChange($value->name)=>trim($value->name)], $employeeClassification);
                    }
               }
                
                if(!empty($arr)){
                    EmployeeClassification::insert($arr);
                    return redirect('admin/employee-classification')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/employee-classification-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
