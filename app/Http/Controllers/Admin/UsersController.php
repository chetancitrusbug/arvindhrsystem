<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\User;
use App\Role;
use App\Region;
use App\State;
use App\City;
use App\StoreLocation;
use App\Brand;
use App\LegalEntity;
use App\Setting;
use Carbon\Carbon as Carbon;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        view()->share('route', 'users');
        view()->share('module', 'Users');
    }

    public function index(Request $request)
    {
        return view('admin.users.index');
    }

    public function datatable(Request $request)
    {
        $manPower = User::select([
                            'users.*',
                            'region.name as region',
                            'state.name as state',
                            'city.name as city',
                        ])
                        ->leftjoin('region',function($join){
                            $join->on('region.id','=','users.region');
                        })
                        ->leftjoin('state',function($join){
                            $join->on('state.id','=','users.state');
                        })
                        ->leftjoin('city',function($join){
                            $join->on('city.id','=','users.city');
                        })
                        ->orderBy('users.id','DESC')
                        ->get();

        return Datatables::of($manPower)
            ->make(true);
    }

    public function hrmslist(Request $request)
    {
        return view('admin.users.hrmslist');
    }
    public function regionalhrlist(Request $request)
    {
        return view('admin.users.regionalhrlist');
    }
    public function storemanagerlist(Request $request)
    {
        return view('admin.users.storemanagerlist');
    }
    public function userdatatable(Request $request,$user_type)
    {
        $user_type = strtoupper($user_type);
        $role = Role::where('name',$user_type)->first();
        $userdata = User::select([
                            'users.*',
                            'region.name as region',
                            'state.name as state',
                            'city.name as city',
                            'store_location.io_code',
                            'store_location.sap_code'
                        ])
                        ->with('roles')->join('role_user', 'role_user.user_id', '=', 'users.id')->where('role_user.role_id', $role->id)
                        ->leftjoin('region','region.id','users.region')
                        ->leftjoin('store_location','store_location.id','users.store_id')
                        ->leftjoin('state','state.id','users.state')
                        ->leftjoin('city','city.id','users.city')
                        ->groupBy('users.id')
                        ->orderBy('users.id','DESC')
                        ->get();

        return Datatables::of($userdata)
            ->make(true);
    }
    public function hrmscreate(Request $request)
    {
        $role = Role::where('name','=','HRMS')->pluck('label','name')->toArray();
        view()->share('role',$role);
        $user_type = 'HRMS';
        $title = 'Create HRMS';
        $is_update  = false;
        return view('admin.users.createuser',compact('user_type','title','is_update'));
    }
    public function regionalhrcreate(Request $request)
    {
        $city = [];
        $storeLocation = StoreLocation::where("status","1")->pluck('store_name','id')->toArray();
        view()->share('storeLocation',$storeLocation);

        $region = Region::where("status","1")->pluck('name','id')->toArray();
        view()->share('region',$region);

        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        if(old('state_id',null)){
            $city = $this->getCityFromState($request,['state_id'=>old('state_id',null)]);
        }
        view()->share('city',$city);

        $brands = Brand::where("status","1")->pluck('name','id')->toArray();
        view()->share('brands',$brands);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $role = Role::where('name','=','RM')->pluck('label','name')->toArray();
        view()->share('role',$role);
        $user_type = 'RM';
        $title = 'Create Regional HR';
        $is_update  = false;
        return view('admin.users.createuser',compact('user_type','title','is_update'));
    }
    public function storemanagercreate(Request $request)
    {
        $city = [];
        $storeLocation = StoreLocation::where("status","1")->pluck('store_name','id')->toArray();
        view()->share('storeLocation',$storeLocation);

        $region = Region::where("status","1")->pluck('name','id')->toArray();
        view()->share('region',$region);

        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        if(old('state_id',null)){
            $city = $this->getCityFromState($request,['state_id'=>old('state_id',null)]);
        }
        view()->share('city',$city);

        $brands = Brand::where("status","1")->pluck('name','id')->toArray();
        view()->share('brands',$brands);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $role = Role::where('name','=','SM')->pluck('label','name')->toArray();
        view()->share('role',$role);
        $user_type = 'SM';
        $title = 'Create Store Login';
        $is_update  = false;
        return view('admin.users.createuser',compact('user_type','title','is_update'));
    }




    public function create(Request $request)
    {
        $city = [];
        $storeLocation = StoreLocation::where("status","1")->pluck('store_name','id')->toArray();
        view()->share('storeLocation',$storeLocation);

        $region = Region::where("status","1")->pluck('name','id')->toArray();
        view()->share('region',$region);

        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        if(old('state_id',null)){
            $city = $this->getCityFromState($request,['state_id'=>old('state_id',null)]);
        }
        view()->share('city',$city);

        $brands = Brand::where("status","1")->pluck('name','id')->toArray();
        view()->share('brands',$brands);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $role = Role::where('label','!=','SU')->pluck('label','name')->toArray();
        view()->share('role',$role);

        return view('admin.users.create');
    }

    public function userstore(Request $request)
    {
        $return_url = 'admin/user/hrms';
        $success_message = "HRMS Added Successfully";
        $rule = [
            'emp_name' => 'required|max:191',
            'email' => 'required|email|unique:users|max:191',
            'password' => 'required|same:confirm_password',
            'mobile_no' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/',
            'emp_id' => 'required',
            'role' => 'required',
        ];

        if($request->role == 'SM' || $request->role == 'RM'){
            $rule['region_id'] = 'required';

        }
        if($request->role == 'RM'){
            $rule['region_id'] = 'required|unique:users,region';

            $return_url = 'admin/user/regionalhr';
            $success_message = "Regional HR Added Successfully";
        }
		if($request->role == 'SM'){
			$rule['store_id'] = 'required|unique:users,store_id';
			$rule['state_id'] = 'required';
            $rule['city_id'] = 'required';
            $rule['pincode'] = 'nullable|numeric';
            $rule['brand_id'] = 'required';
            $rule['legal_entity_id'] = 'nullable';
            $return_url = 'admin/user/storemanager';
            $success_message = "Store Manager Added Successfully";
        }


        $this->validate($request, $rule, [
                            'emp_name.required'=>'The employee name field is required.',
                            'emp_id.required'=>'The employee id field is required.',
                            'store_id.required'=>'The store field is required.',
                            'region_id.required'=>'The region field is required.',
                            'state_id.required'=>'The state field is required.',
                            'city_id.required'=>'The city field is required.',
                            'brand_id.required'=>'The brand field is required.',
                            'legal_entity_id.required'=>'The legal entity field is required.',
                        ]);

        $data = $request->except(['password','role']);

        $data['password'] = bcrypt($request->password);
        $data['region'] = $request->region_id;
        $data['state'] = $request->state_id;
        $data['city'] = $request->city_id;

        $user = User::create($data);

        $role=Role::where('name',$request->role)->first();

        $user->roles()->attach($role);

        \Session::flash('flash_success', $success_message);
        return redirect($return_url);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rule = [
            'emp_name' => 'required|max:191',
            'email' => 'required|email|unique:users|max:191',
            'password' => 'required|same:confirm_password',
            'mobile_no' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/',
            'emp_id' => 'required',

            'region_id' => 'required',

            'role' => 'required',
        ];
		if($request->role == 'SM'){
			$rule['store_id'] = 'required';
			$rule['state_id'] = 'required';
            $rule['city_id'] = 'required';
            $rule['pincode'] = 'required|numeric';
            $rule['internal_order'] = 'required|numeric';
            $rule['brand_id'] = 'required';
            $rule['legal_entity_id'] = 'required';
		}

        $this->validate($request, $rule, [
                            'emp_name.required'=>'The employee name field is required.',
                            'emp_id.required'=>'The employee id field is required.',
                            'store_id.required'=>'The store field is required.',
                            'region_id.required'=>'The region field is required.',
                            'state_id.required'=>'The state field is required.',
                            'city_id.required'=>'The city field is required.',
                            'brand_id.required'=>'The brand field is required.',
                            'legal_entity_id.required'=>'The legal entity field is required.',
                        ]);

        $data = $request->except(['password','role']);

        $data['password'] = bcrypt($request->password);
        $data['region'] = $request->region_id;
        $data['state'] = $request->state_id;
        $data['city'] = $request->city_id;

        $user = User::create($data);

        $role=Role::where('name',$request->role)->first();

        $user->roles()->attach($role);

        \Session::flash('flash_success', 'User added!');
        return redirect('admin/users');
    }

    public function hrmsedit($id)
    {
        $user = User::with('roles')->where('id',$id)->first();

        if ($user) {
            $request = new Request();

            $role = Role::pluck('label','name')->toArray();
            view()->share('role',$role);
            $is_update = true;
            $user_type = 'HRMS';
            return view('admin.users.edituser', compact('user','is_update','user_type'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/user/hrms');
        }
    }
    public function regionalhredit($id)
    {
        $user = User::with('roles')->where('id',$id)->first();

        if ($user) {
            $request = new Request();
            $city = [];
            $storeLocation = StoreLocation::where("status","1")->pluck('store_name','id')->toArray();
            view()->share('storeLocation',$storeLocation);

            $region = Region::where("status","1")->pluck('name','id')->toArray();
            view()->share('region',$region);

            $states = State::where("status","1")->pluck('name','id')->toArray();
            view()->share('states',$states);

            if(old('state_id',$user->state)){
                $city = $this->getCityFromState($request,['state_id'=>old('state_id',$user->state)]);
            }
            view()->share('city',$city);

            $brands = Brand::where("status","1")->pluck('name','id')->toArray();
            view()->share('brands',$brands);

            $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
            view()->share('legalEntity',$legalEntity);

            $role = Role::pluck('label','name')->toArray();
            view()->share('role',$role);
            $is_update = true;
            $user_type = 'RM';
            return view('admin.users.edituser', compact('user','is_update','user_type'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/user/hrms');
        }
    }

    public function storemanageredit($id)
    {
        $user = User::with('roles')->where('id',$id)->first();

        if ($user) {
            $request = new Request();
            $city = [];
            $storeLocation = StoreLocation::where("status","1")->pluck('store_name','id')->toArray();
            view()->share('storeLocation',$storeLocation);

            $region = Region::where("status","1")->pluck('name','id')->toArray();
            view()->share('region',$region);

            $states = State::where("status","1")->pluck('name','id')->toArray();
            view()->share('states',$states);

            if(old('state_id',$user->state)){
                $city = $this->getCityFromState($request,['state_id'=>old('state_id',$user->state)]);
            }
            view()->share('city',$city);

            $brands = Brand::where("status","1")->pluck('name','id')->toArray();
            view()->share('brands',$brands);

            $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
            view()->share('legalEntity',$legalEntity);

            $role = Role::pluck('label','name')->toArray();
            view()->share('role',$role);
            $is_update = true;
            $user_type = 'SM';
            return view('admin.users.edituser', compact('user','is_update','user_type'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/user/hrms');
        }
    }

    public function userupdate($id, Request $request)
    {
        $return_url = 'admin/user/hrms';
        $success_message = "HRMS Updated Successfully";
        $rule = [
            'emp_name' => 'required|max:191',
            'mobile_no' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/',
            'emp_id' => 'required',
            'role' => 'required',
        ];
        $requestData = $request->except(['region_id','role','state_id','city_id','_method','_token','confirm_password','password']);

        if(isset($request->password) && $request->password != ''){
            $rule['password'] = 'required|same:confirm_password';
            $requestData['password'] = $request->password;
        }

        if($request->role == 'RM'){
            $rule['region_id'] = 'required|unique:users,region,'.$id;
            $return_url = 'admin/user/regionalhr';
            $success_message = "Regional HR Updated Successfully";
        }
		if($request->role == 'SM'){
            $rule['region_id'] = 'required';
			$rule['store_id'] = 'required|unique:users,store_id,'.$id;
			$rule['state_id'] = 'required';
            $rule['city_id'] = 'required';
            $rule['pincode'] = 'nullable|numeric';
            $rule['brand_id'] = 'required';
            $rule['legal_entity_id'] = 'nullable';
            $return_url = 'admin/user/storemanager';
            $success_message = "Store Manager Updated Successfully";
        }


        $this->validate($request, $rule, [
                            'emp_name.required'=>'The employee name field is required.',
                            'emp_id.required'=>'The employee id field is required.',
                            'store_id.required'=>'The store field is required.',
                            'region_id.required'=>'The region field is required.',
                            'state_id.required'=>'The state field is required.',
                            'city_id.required'=>'The city field is required.',
                            'brand_id.required'=>'The brand field is required.',
                            'legal_entity_id.required'=>'The legal entity field is required.',
        ]);

        $this->validate($request, $rule);



        if(isset($request->password) && $request->password){
            $requestData['password'] = bcrypt($request->password);
        }else{
            unset($requestData['password']);
        }

        $requestData['region'] = $request->region_id;
        $requestData['state'] = $request->state_id;
        $requestData['city'] = $request->city_id;

        $requestData['status'] = isset($requestData['status'])?1:0;

        $user = User::where('id',$id)->update($requestData);

        $role=Role::where('name',$request->role)->first();
        $role = \DB::table('role_user')->where('user_id',$id)->update(['role_id'=>$role->id]);

        \Session::flash('flash_success', $success_message);
        return redirect($return_url);
    }

    public function show($id)
    {
        $user = User::with(['roles','legalEntity','brand','regionName','stateName','cityName','storeLocation'])->find($id);

        if(!$user){
            return redirect('admin/users')->with('flash_error', 'User Not Found!');
        }

        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $user = User::with('roles')->where('id',$id)->first();

        if ($user) {

            $request = new Request();
            $city = [];
            $storeLocation = StoreLocation::where("status","1")->pluck('store_name','id')->toArray();
            view()->share('storeLocation',$storeLocation);

            $region = Region::where("status","1")->pluck('name','id')->toArray();
            view()->share('region',$region);

            $states = State::where("status","1")->pluck('name','id')->toArray();
            view()->share('states',$states);

            if(old('state_id',$user->state)){
                $city = $this->getCityFromState($request,['state_id'=>old('state_id',$user->state)]);
            }
            view()->share('city',$city);

            $brands = Brand::where("status","1")->pluck('name','id')->toArray();
            view()->share('brands',$brands);

            $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
            view()->share('legalEntity',$legalEntity);

            $role = Role::pluck('label','name')->toArray();
            view()->share('role',$role);

            return view('admin.users.edit', compact('user'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/users');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $rule = [
            'emp_name' => 'required|max:191',
            'password' => 'nullable|same:confirm_password',
            'mobile_no' => 'required|regex:/^(?=.*[0-9])[ +()0-9]+$/',
            'emp_id' => 'required',
            'store_id' => 'required',
            'region_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'pincode' => 'required|numeric',
            'internal_order' => 'required|numeric',
            'brand_id' => 'required',
            'legal_entity_id' => 'required',
            'role' => 'required',
        ];

        $this->validate($request, $rule, [
                            'emp_name.required'=>'The employee name field is required.',
                            'emp_id.required'=>'The employee id field is required.',
                            'store_id.required'=>'The store field is required.',
                            'region_id.required'=>'The region field is required.',
                            'state_id.required'=>'The state field is required.',
                            'city_id.required'=>'The city field is required.',
                            'brand_id.required'=>'The brand field is required.',
                            'legal_entity_id.required'=>'The legal entity field is required.',
                        ]);

        if(isset($request->password) && $request->password != ''){
            $rule['password'] = 'required|same:confirm_password';
        }

        $this->validate($request, $rule);

        $requestData = $request->except(['email','confirm_password','role','region_id','state_id','city_id','_method','_token']);

        if(isset($request->password) && $request->password){
            $requestData['password'] = bcrypt($request->password);
        }else{
            unset($requestData['password']);
        }

        $requestData['region'] = $request->region_id;
        $requestData['state'] = $request->state_id;
        $requestData['city'] = $request->city_id;

        $requestData['status'] = isset($requestData['status'])?1:0;

        $user = User::where('id',$id)->update($requestData);

        $role=Role::where('name',$request->role)->first();
        $role = \DB::table('role_user')->where('user_id',$id)->update(['role_id'=>$role->id]);

        \Session::flash('flash_success', 'User updated!');
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        User::where('id',$id)->delete();
        User::where('parent_id',$id)->delete();
        \DB::table('plan_user')->where('user_id',$id)->update(['status'=>'0']);

        if($request->has('from_index')){
            $message = "User Deleted !!";
            return response()->json(['message' => $message],200);
        }else{
            \Session::flash('flash_success', 'User deleted!');
            return redirect('admin/users');
        }
    }


    public function userdestroy(Request $request,$id)
    {
        User::where('id',$id)->delete();
        User::where('parent_id',$id)->delete();
        \DB::table('plan_user')->where('user_id',$id)->update(['status'=>'0']);

        \Session::flash('flash_success', 'User deleted!');
        return redirect()->back();

    }
    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function importSroreManager(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();
            // $businessUnit = array_change_key_case(BusinessUnit::pluck('id','name')->toArray(), CASE_LOWER);
            // $category = array_change_key_case(SourceCategory::pluck('category','id')->toArray(), CASE_LOWER);
            $employee = array_change_key_case(Employee::whereNotNull('employee_code')->pluck('id','employee_code')->toArray(), CASE_LOWER);
            $store = array_change_key_case(StoreLocation::pluck('id',\DB::raw("TRIM(location_code) as location_code"))->toArray(), CASE_LOWER);
            $brand = array_change_key_case(Brand::pluck('id','name')->toArray(), CASE_LOWER);
            $legalEntity = array_change_key_case(LegalEntity::pluck('id','name')->toArray(), CASE_LOWER);
            $region = array_change_key_case(Region::pluck('id','name')->toArray(), CASE_LOWER);
            $state = array_change_key_case(State::pluck('id','name')->toArray(), CASE_LOWER);
            $city = array_change_key_case(City::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                $valStore = $valEmployee = [];

                foreach ($data as $key => $value) {
                    $arr[$key]['email'] = $value->store_code.'@arvinduniversity.com';
                    $arr[$key]['internal_order'] = $value->internal_orderio;
                    $arr[$key]['pincode'] = $value->pincode;
                    $arr[$key]['mobile_no'] = $value->contact;
                    $arr[$key]['emp_name'] = $value->store_manager;
                    $arr[$key]['email'] = $value->store_email;
                    $arr[$key]['mobile_no'] = $value->sm_contact_no;
                    $arr[$key]['contact'] = $value->contact;
                    $arr[$key]['emp_id'] = $value->sm_emp_id;

                    if(array_key_exists(caseChange($value->brand),$brand)){
                        $arr[$key]['brand_id'] = $brand[caseChange($value->brand)];
                    }else{
                        $brandInsert['name'] = $value->brand;
                        $brandInsert['status'] = 1;
                        $brandCreate = Brand::create($brandInsert);
                        $arr[$key]['brand_id'] = $brandCreate->id;
                        $brand = array_merge([caseChange($value->brand)=>$brandCreate->id], $brand);
                    }

                    if(array_key_exists(caseChange($value->legal_entity),$legalEntity)){
                        $arr[$key]['legal_entity_id'] = $legalEntity[caseChange($value->legal_entity)];
                    }else{
                        $legalEntityInsert['name'] = $value->legal_entity;
                        $legalEntityInsert['status'] = 1;
                        $legalEntityCreate = LegalEntity::create($legalEntityInsert);
                        $arr[$key]['legal_entity_id'] = $legalEntityCreate->id;
                        $legalEntity = array_merge([caseChange($value->legal_entity)=>$legalEntityCreate->id], $legalEntity);
                    }

                    if(array_key_exists(caseChange($value->region),$region) || $value->region == ''){
                        $arr[$key]['region'] = isset($region[caseChange($value->region)])?$region[caseChange($value->region)]:0;
                    }else{
                        $regionInsert['name'] = $value->region;
                        $regionInsert['status'] = 1;
                        $regionCreate = Region::create($regionInsert);
                        $arr[$key]['region'] = $regionCreate->id;
                        $region = array_merge([caseChange($value->region)=>$regionCreate->id], $region);
                    }

                    if(array_key_exists(caseChange($value->state),$state) || $value->state == ''){
                        $arr[$key]['state'] = isset($state[caseChange($value->state)])?$state[caseChange($value->state)]:0;
                    }else{
                        $stateInsert['region_id'] = $arr[$key]['region'];
                        $stateInsert['name'] = $value->state;
                        $stateInsert['status'] = 1;
                        $stateCreate = State::create($stateInsert);
                        $arr[$key]['state'] = $stateCreate->id;
                        $state = array_merge([caseChange($value->state)=>$stateCreate->id], $state);
                    }

                    if(array_key_exists(caseChange($value->city),$city) || $value->city == ''){
                        $arr[$key]['city'] = isset($city[caseChange($value->city)])?$city[caseChange($value->city)]:0;
                    }else{
                        $cityInsert['state_id'] = $arr[$key]['state'];
                        $cityInsert['name'] = $value->city;
                        $cityInsert['status'] = 1;
                        $cityCreate = City::create($cityInsert);
                        $arr[$key]['city'] = $cityCreate->id;
                        $city = array_merge([caseChange($value->city)=>$cityCreate->id], $city);
                    }

                    if(array_key_exists(caseChange($value->location_code),$store)){
                        $arr[$key]['store_id'] = $store[caseChange($value->location_code)];
                    }else{
                        $storeInsert['location_code'] = $value->location_code;
                        $storeInsert['io_code'] = $value->internal_orderio;
                        $storeInsert['sap_code'] = $value->store_code;
                        $storeInsert['store_name'] = $value->store_name;
                        $storeInsert['brand_id'] = $arr[$key]['brand_id'];
                        $storeInsert['address'] = $value->store_address;
                        $storeInsert['city_id'] = $arr[$key]['city'];
                        $storeInsert['state_id'] = $arr[$key]['state'];
                        $storeInsert['region_id'] = $arr[$key]['region'];
                        $storeInsert['status'] = 1;
                        $storeCreate = StoreLocation::create($storeInsert);
                        $arr[$key]['store_id'] = $storeCreate->id;
                        $store = array_merge([caseChange($value->location_code)=>$storeCreate->id], $store);
                    }
                }

                if(count($valStore) > 0 || count($valEmployee) > 0){
                    return redirect('admin/users/import')
                                ->with('valStore', implode(', ', $valStore))
                                ->with('valEmployee', implode(', ', $valEmployee))
                                ->with('flash_error', 'See below errors!');
                }

                if(!empty($arr)){
                    foreach ($arr as $value) {
                        $value['password'] = bcrypt('123456');
                        $value['status'] = 1;
                        $user = User::create($value);
                        $user->assignRole('SM');
                    }

                    return redirect('admin/users/import')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }

        return 'import successfull';
    }
}
