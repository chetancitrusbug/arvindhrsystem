<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\VariablePayType;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class VariablePayTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.variablepaytype');
        $this->middleware('permission:access.variablepaytype.edit')->only(['edit','update']);
        $this->middleware('permission:access.variablepaytype.create')->only(['create', 'store']);
        $this->middleware('permission:access.variablepaytype.delete')->only('destroy');

        view()->share('route', 'variable_pay_type');
        view()->share('module', 'Variable Pay Type');
    }
    
    public function index(Request $request)
    {
    	return view('admin.variable_pay_type.index');
    }

    public function datatable(Request $request)
    {
        $variablePayType = VariablePayType::latest()->get();

        return Datatables::of($variablePayType)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.variable_pay_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $variablePayType = VariablePayType::create($requestData);
        
        return redirect('admin/variable-pay-type')->with('flash_success', 'Variable Pay Type added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $variablePayType = VariablePayType::find($id);
        
        return view('admin.variable_pay_type.show', compact('variablePayType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $variablePayType = VariablePayType::find($id);

        if(!$variablePayType){
            return redirect('admin/variable-pay-type')->with('flash_error', 'Variable pay type Not Found!');
        }
        
        return view('admin.variable_pay_type.edit', compact('variablePayType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $variablePayType = VariablePayType::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/variable-pay-type')->with('flash_success', 'Variable Pay Type updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $variablePayType = VariablePayType::find($id);
        
        $variablePayType->delete();

        if($request->has('from_index')){
            $message = "Variable Pay Type Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Variable Pay Type deleted!');

            return redirect('admin/variable-pay-type');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',new Unique('variable_pay_type','variable pay type',$id)],
        ];

        return $this->validate($request, $rules,[
            'name.required' => 'The Variable Pay Type field is required',
            'name.regex' => 'The Variable Pay Type format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.variable_pay_type.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $variablePayType = array_change_key_case(VariablePayType::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->name),$variablePayType)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->name);

                        $variablePayType = array_merge([caseChange($value->name)=>trim($value->name)], $variablePayType);
                    }
               }
                
                if(!empty($arr)){
                    VariablePayType::insert($arr);
                    return redirect('admin/variable-pay-type')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/variable-pay-type-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
