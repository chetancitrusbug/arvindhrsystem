<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;
use App\StoreBudgetManPower;
use App\BudgetStore;

class StoreBudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $array['sm'] = 'Store Manager';
        $array['dm'] = 'Department Manager';
        $array['tdm'] = 'TDM';
        $array['vm'] = 'Vendore Manager';
        $array['logistics'] = 'Logistics';
        $array['maintenance'] = 'Maintenance';
        $array['cashier'] = 'Cashier';
        $array['head_cashier'] = 'Head Cashier';
        $array['csa'] = 'Customer Service Assosiate';
        $this->arrayForLabelDesignation = $array;
        view()->share('store_designation', $array);
        view()->share('route', 'store-budget');
        view()->share('module', 'Store Budget Man Power');
    }
    public function index()
    {
        return view('admin.storemanpower.index');
    }
    public function getregional()
    {
        return view('admin.storemanpower.regional');
    }

    public function getnational()
    {
        return view('admin.storemanpower.national');
    }


    public function datatable(Request $request)
    {
        $stores = \App\StoreLocation::selectRaw('store_location.*,sum(budget_stores.budgetManpower) as total_budget_manpower')
                                ->leftJoin('budget_stores','budget_stores.store_id','store_location.id')
                                ->where('store_location.status',1)
                                ->groupby('store_location.id')
                                ->latest()
                                ->get();
        return Datatables::of($stores)
            ->make(true);
    }
    public function storedatatable(Request $request,$region_id)
    {
        $stores = \App\StoreLocation::selectRaw('store_location.*,sum(budget_stores.budgetManpower) as total_budget_manpower')
                                ->leftJoin('budget_stores','budget_stores.store_id','store_location.id')
                                ->where('store_location.status',1)
                                ->where('store_location.region_id',$region_id)
                                ->groupby('store_location.id')
                                ->latest()
                                ->get();
        return Datatables::of($stores)
            ->make(true);
    }
    public function regiondatatable(Request $request)
    {
        $stores = \App\Region::selectRaw('region.*,sum(budget_stores.budgetManpower) as total_budget_manpower')
                                ->leftJoin('store_location','store_location.region_id','region.id')
                                ->leftJoin('budget_stores','budget_stores.store_id','store_location.id')
                                ->where('store_location.status',1)
                                ->groupby('region.id')
                                ->latest()
                                ->get();
        return Datatables::of($stores)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $stores = \App\StoreLocation::where("status","1")->pluck('store_name','id')->toArray();
        view()->share('stores',$stores);
        return view('admin.storemanpower.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[
                'store_id'=>'required',
        ]);
        $designation_ids = $request->designation_id;
        $budgets = $request->budget;
        $store_id = $request->store_id;
        $data = array();
        foreach($designation_ids as $key => $value){
            $storebudget = BudgetStore::where('store_id',$store_id)
                            ->where('label',$value)->first();
            if($storebudget){
                $storebudget->budgetManpower = ($budgets[$value])?$budgets[$value]:0;
                $storebudget->save();
            } else {
                $data[$key]['store_id'] = $store_id;
                $data[$key]['label'] = $value;
                $data[$key]['budgetManpower'] = ($budgets[$value])?$budgets[$value]:0;
            }

        }
        // foreach($designation_ids as $key => $value){
        //     $storebudget = StoreBudgetManPower::where('store_id',$store_id)
        //                     ->where('designation_id',$value)->first();
        //     if($storebudget){
        //         $storebudget->budget = ($budgets[$key])?$budgets[$key]:0;
        //         $storebudget->save();
        //     } else {
        //         $data[$key]['store_id'] = $store_id;
        //         $data[$key]['designation_id'] = $value;
        //         $data[$key]['budget'] = ($budgets[$key])?$budgets[$key]:0;
        //     }

        // }
        if(!empty($data)){
            BudgetStore::insert($data);
        }
        return redirect('admin/store-budget-national')->with('flash_success', 'Store Budget Manpower added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = \App\StoreLocation::find($id);
        $storebudget = BudgetStore::where('store_id',$id)->get();
        // $designations = \App\Designation::where('status',1)->get();
        // $storeBudgetManPower = StoreBudgetManPower::selectRaw('store_budget_man_powers.*, designation.name as designation_name')
        //                                             ->leftJoin('store_location','store_location.id','store_budget_man_powers.store_id')
        //                                             ->leftJoin('designation','designation.id','store_budget_man_powers.designation_id')
        //                                             ->where('store_budget_man_powers.store_id',$id)
        //                                             ->get();
        // $storebudget = array();
        // foreach($designations as $designation){
        //    $budget =  $storeBudgetManPower->where('designation_id',$designation->id)->first();

        //     $storebudget[$designation->id]['name'] = $designation->name;
        //     $storebudget[$designation->id]['budget'] = ($budget)?$budget->budget:0;

        // }

        return view('admin.storemanpower.show',compact('storebudget','store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $BudgetStore = BudgetStore::where('store_id',$id)->get();
        $storeBudgetManPower = BudgetStore::where('store_id',$id)->pluck('budgetManpower','label')->toArray();
        $store_id = $id;

            $request = new Request();
            //$designations = \App\Designation::where('status',1)->get();
            $stores = \App\StoreLocation::where("status","1")->pluck('store_name','id')->toArray();

            view()->share('stores',$stores);
            view()->share('store_id',$store_id);
            return view('admin.storemanpower.update',compact('storeBudgetManPower'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
                'store_id'=>'required',
        ]);
        $designation_ids = $request->designation_id;
        $budgets = $request->budget;
        $store_id = $request->store_id;
        $data = array();

        foreach($designation_ids as $key => $value){
            $storebudget = BudgetStore::where('store_id',$store_id)
                            ->where('label',$value)->first();
            if($storebudget){
                $storebudget->budgetManpower = ($budgets[$value])?$budgets[$value]:0;
                $storebudget->save();
            } else {
                $data[$key]['store_id'] = $store_id;
                $data[$key]['label'] = $value;
                $data[$key]['budgetManpower'] = ($budgets[$value])?$budgets[$value]:0;
            }

        }
        if(!empty($data)){
            BudgetStore::insert($data);
        }
        return redirect('admin/store-budget-national')->with('flash_success', 'Store Budget Manpower Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function showUpload()
    {
        return view('admin.storemanpower.upload');
    }

    public function ExcelUpload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);
        
        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            //$designations = array_change_key_case(\App\Designation::pluck('id','name')->toArray(), CASE_LOWER);
            $stores = array_change_key_case(\App\StoreLocation::pluck('id','io_code')->toArray(), CASE_LOWER);
            $arr = [];
            $i = 0;
            if($data->count()){
                $store_detail_to_add  = array();
                foreach ($data as $key => $value) {
                    $store_id = '';
                    foreach($value as $k => $v){
                            $value_array = array();
                        if($k == 'io_code' && array_key_exists(caseChange($v),$stores)){
                            $store_id = $stores[caseChange($v)];
                        } elseif (!empty($store_id) && $store_id > 0 && $k != '0' ) {
                             $storebudget = BudgetStore::where('store_id',$store_id)
                            ->where('label',$k)->first();
                            if(!empty($storebudget)){
                                //dd($storebudget);
                                $storebudget->budgetManpower = ($v)?number_format($v,0):0;
                                $storebudget->save();
                            }else{
                                if($k != 'total'){
                                    $store_detail_to_add[$i]['store_id'] = $store_id;
                                    $store_detail_to_add[$i]['label'] = caseChange($k);
                                    $store_detail_to_add[$i]['budgetManpower'] = ($v)?number_format($v,0):0;
                                    $i++;
                                }else{
                                    $store_location = \App\StoreLocation::find($store_id);
                                    $store_location->budget_man_power = ($v)?number_format($v,0):0;
                                    $store_location->save();
                                }
                            }
                            // $storebudget = StoreBudgetManPower::where('store_id',$store_id)
                            //                                 ->where('designation_id',$designations[caseChange($k)])
                            //                                 ->first();
                            // if($storebudget){
                            //     $storebudget->budget = ($value->budget)?caseChange($value->budget):0;
                            //     $storebudget->save();
                            // } else {
                            //     $a['store_id'] = $store_id;
                            //     $a['designation_id'] = $designations[caseChange($k)];
                            //     $a['budget'] = ($value->budget)?caseChange($v):0;
                            //     array_push($arr,$a);
                            // }
                            
                        }

                    }

               }
               if(!empty($data)){
                    BudgetStore::insert($store_detail_to_add);
                }
                // if(!empty($arr)){
                //     StoreBudgetManPower::insert($arr);
                // }
                return redirect('admin/store-budget-national')
                    ->with('flash_success', 'Record imported successfully!');
            }
        }
        return redirect('admin/store-budget-national')
                    ->with('flash_error', 'Not any data to upload.');

    }

    public function storebudgetregionwise(Request $request)
    {
        view()->share('route', 'store-budget-report');
        $region =  \App\Region::where('status',1)->get();
        $storeBudgetManPower = BudgetStore::leftJoin('store_location','store_location.id','budget_stores.store_id')
                                                     ->where('store_location.status',1)
                                                     ->get();
        //$designations = \App\Designation::where('status',1)->pluck('name','id')->toArray();
        $designations  = $this->arrayForLabelDesignation;
        $store_reports = array();
        foreach ($region as $region ) {
            $data = array();
            $data['region_id'] = $region->id;
            $data['region_name'] = $region->name;
            if($this->arrayForLabelDesignation){
                $total = 0;
                foreach($this->arrayForLabelDesignation as $key => $value){
                       $manpowerbudget = $storeBudgetManPower->where('region_id',$region->id)->where('label',$key)->sum('budgetManpower');
                    if($manpowerbudget){
                        $total = $total+$manpowerbudget;
                        $data['designation'][] =$manpowerbudget;
                    } else {
                        $data['designation'][] = 0;
                    }
                }
                $data['designation'][] = $total;
            }
            $store_reports[] = $data;
        }
        return view('admin.storemanpower.regionreports', compact('designations','store_reports'));
    }

    public function storebudgetstorewise(Request $request,$region_id)
    {
        view()->share('route', 'store-budget-report');
        $stores =  \App\StoreLocation::where('region_id',$region_id)->where('status',1)->get();
        $storeBudgetManPower = BudgetStore::leftJoin('store_location','store_location.id', 'budget_stores.store_id')
                                                    ->where('store_location.status',1)
                                                    ->where('store_location.region_id',$region_id)
                                                    ->get();
        $designations = $this->arrayForLabelDesignation;
        $store_reports = array();
        foreach ($stores as $store ) {
            $data = array();
            $data['store_id'] = $store->id;
            $data['io_code'] = $store->io_code;
            $data['store_name'] = $store->store_name;
            if($designations){
                $total = 0;
                foreach($designations as $key => $value){
                    $manpowerbudget = $storeBudgetManPower->where('store_id',$store->id)->where('label',$key)->first();
                    if($manpowerbudget){
                        $total = $total+$manpowerbudget->budgetManpower;
                        $data['designation'][] =$manpowerbudget->budgetManpower;
                    } else {
                        $data['designation'][] = 0;
                    }
                }
                $data['designation'][] = $total;
            }
            $store_reports[] = $data;
        }
        return view('admin.storemanpower.storereports', compact('designations','store_reports'));
    }
    public function exportstorebudget(Request $request,$region_id)
    {
        $excel_export = array();
        $stores =  \App\StoreLocation::where('region_id',$region_id)->where('status',1)->get();
        $storeBudgetManPower = StoreBudgetManPower::leftJoin('store_location','store_location.id', 'budget_stores.store_id')
                                                    ->where('store_location.status',1)
                                                    ->where('store_location.region_id',$region_id)
                                                    ->get();
        $designations = $this->arrayForLabelDesignation;
        $excel_header = array('Store Name','Store IO Code');
        foreach ($designations as $designation) {
            $excel_header[] = $designation;
        }
        $excel_header[] = "Total";
        $excel_export[] = $excel_header;
        foreach ($stores as $store ) {
            $data = array();
            $data['io_code'] = $store->io_code;
            $data['store_name'] = $store->store_name;
            if($designations){
                $total = 0;
                foreach($designations as $key => $value){
                    $manpowerbudget = $storeBudgetManPower->where('store_id',$store->id)->where('label',$key)->first();
                    if($manpowerbudget){
                        $total = $total+$manpowerbudget->budget;
                        $data[$key]=$manpowerbudget->budget;
                    } else {
                        $data[$key] = 0;
                    }
                }
                $data['total'] = $total;
            }
            $excel_export[] = $data;
        }

        \Excel::create('Man Power Budget Store',function($excel) use ($excel_export){
                $excel->setTitle('Man Power Budget Store');
                $excel->sheet('Man Power Budget Store',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                });
            })->download('xlsx');
        return back();
    }

    public function LableExcelUpload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            try{

                $path = $request->file('excel')->getRealPath();
                $data = \Excel::load($path)->first();

                $designations = array_change_key_case(\App\Designation::pluck('id','name')->toArray(), CASE_LOWER);
                $stores = array_change_key_case(\App\StoreLocation::pluck('id','io_code')->toArray(), CASE_LOWER);
                $arr = array();
                if($data->count()){

                    foreach ($data as $key => $value) {
                        $store_id = null;
                       /* if(is_array($value)){ */
                            foreach($value as $k => $v){
                                $value_array = array();
                                if($k =='io_code' && array_key_exists(caseChange($v),$stores)){
                                    $store_id = $stores[caseChange($v)];
                                } else if (!empty($store_id)) {
                                    $is_valid_lable = false;
                                    $actual_manpower = null;
                                    if($k == 'sm'){
                                        $is_valid_lable = true;
                                        $designation_id = array();
                                        $actual_manpower = 0;
                                        if(array_key_exists(caseChange('Assistant Showroom Manager'),$designations)){
                                            $designation_id[] = $designations[caseChange('Assistant Showroom Manager')];
                                        }
                                        if(array_key_exists(caseChange('Store Manager in Training'),$designations)){
                                            $designation_id[] = $designations[caseChange('Store Manager in Training')];
                                        }
                                        if(array_key_exists(caseChange('Showroom Manager'),$designations)){
                                            $designation_id[] = $designations[caseChange('Showroom Manager')];
                                        }
                                        if(array_key_exists(caseChange('Store Manager Trainee'),$designations)){
                                            $designation_id[] = $designations[caseChange('Store Manager Trainee')];
                                        }

                                    if(!empty($designation_id)){
                                            $actual_manpower = \App\Employee::selectRaw('count(employee.id) as total_manpower')
                                                                        ->where('store_id',$store_id)
                                                                        ->whereIN('designation_id',$designation_id)->first();
                                        }
                                    } else
                                    if($k == 'dm'){
                                        $is_valid_lable = true;
                                        $designation_id = array();
                                        $actual_manpower = 0;
                                        if(array_key_exists(caseChange('Retail Trainee'),$designations)){
                                            $designation_id[] = $designations[caseChange('Retail Trainee')];
                                        }
                                        if(array_key_exists(caseChange('Department Manager'),$designations)){
                                            $designation_id[] = $designations[caseChange('Department Manager')];
                                        }
                                        if(!empty($designation_id)){
                                            $actual_manpower = \App\Employee::selectRaw('count(employee.id) as total_manpower')
                                                                        ->where('store_id',$store_id)
                                                                        ->whereIN('designation_id',$designation_id)->first();
                                        }
                                    } else
                                    if($k == 'tdm'){
                                        $is_valid_lable = true;
                                        $designation_id = array();
                                        $actual_manpower = 0;
                                        if(array_key_exists(caseChange('Trainee Department Manager'),$designations)){
                                            $designation_id[] = $designations[caseChange('Trainee Department Manager')];
                                        }
                                        if(!empty($designation_id)){
                                            $actual_manpower = \App\Employee::selectRaw('count(employee.id) as total_manpower')
                                                                        ->where('store_id',$store_id)
                                                                        ->whereIN('designation_id',$designation_id)->first();
                                        }
                                    } else
                                    if($k == 'vm'){
                                        $is_valid_lable = true;
                                        $designation_id = array();
                                        $actual_manpower = 0;
                                        if(array_key_exists(caseChange('Store Visual Merchandiser'),$designations)){
                                            $designation_id[] = $designations[caseChange('Trainee Department Manager')];
                                        }
                                        if(!empty($designation_id)){
                                            $actual_manpower = \App\Employee::selectRaw('count(employee.id) as total_manpower')
                                                                        ->where('store_id',$store_id)
                                                                        ->whereIN('designation_id',$designation_id)->first();
                                        }

                                    } else
                                    if($k == 'logistics'){
                                        $is_valid_lable = true;
                                        $designation_id = array();
                                        $actual_manpower = 0;
                                        if(array_key_exists(caseChange('Logistic Officer'),$designations)){
                                            $designation_id[] = $designations[caseChange('Logistic Officer')];
                                        }
                                        if(!empty($designation_id)){
                                            $actual_manpower = \App\Employee::selectRaw('count(employee.id) as total_manpower')
                                                                        ->where('store_id',$store_id)
                                                                        ->whereIN('designation_id',$designation_id)->first();
                                        }

                                    } else
                                    if($k == 'maintenance'){
                                        $is_valid_lable = true;
                                        $designation_id = array();
                                        $actual_manpower = 0;
                                        if(array_key_exists(caseChange('Electrical & Maintenance'),$designations)){
                                            $designation_id[] = $designations[caseChange('Electrical & Maintenance')];
                                        }
                                        if(!empty($designation_id)){
                                            $actual_manpower = \App\Employee::selectRaw('count(employee.id) as total_manpower')
                                                                        ->where('store_id',$store_id)
                                                                        ->whereIN('designation_id',$designation_id)->first();
                                        }

                                    } else
                                    if($k == 'cashier'){
                                        $is_valid_lable = true;

                                        $designation_id = array();
                                        $actual_manpower = 0;
                                        if(array_key_exists(caseChange('Cashier'),$designations)){
                                            $designation_id[] = $designations[caseChange('Cashier')];
                                        }
                                        if(array_key_exists(caseChange('Senior Cashier'),$designations)){
                                            $designation_id[] = $designations[caseChange('Senior Cashier')];
                                        }
                                        if(!empty($designation_id)){
                                            $actual_manpower = \App\Employee::selectRaw('count(employee.id) as total_manpower')
                                                                        ->where('store_id',$store_id)
                                                                        ->whereIN('designation_id',$designation_id)->first();
                                        }

                                    } else
                                    if($k == 'head_cashier'){
                                        $is_valid_lable = true;
                                        $designation_id = array();
                                        $actual_manpower = 0;
                                        if(array_key_exists(caseChange('Head Cashier'),$designations)){
                                            $designation_id[] = $designations[caseChange('Head Cashier')];
                                        }
                                        if(!empty($designation_id)){
                                            $actual_manpower = \App\Employee::selectRaw('count(employee.id) as total_manpower')
                                                                        ->where('store_id',$store_id)
                                                                        ->whereIN('designation_id',$designation_id)->first();
                                        }

                                    } else
                                    if($k == 'csa'){
                                        $is_valid_lable = true;
                                        $designation_id = array();
                                        $actual_manpower = 0;
                                        if(array_key_exists(caseChange('Customer Service Associate'),$designations)){
                                            $designation_id[] = $designations[caseChange('Customer Service Associate')];
                                        }
                                        if(array_key_exists(caseChange('Senior Customer Service Associate'),$designations)){
                                            $designation_id[] = $designations[caseChange('Senior Customer Service Associate')];
                                        }
                                        if(!empty($designation_id)){
                                            $actual_manpower = \App\Employee::selectRaw('count(employee.id) as total_manpower')
                                                                        ->where('store_id',$store_id)
                                                                        ->whereIN('designation_id',$designation_id)->first();
                                        }

                                    } else
                                    if($k == 'total_staff'){
                                        $designation_id = array();
                                        $actual_manpower = 0;
                                        \App\StoreLocation::where('id', $store_id)->update(['budget_man_power'=>$v]);

                                    }

                                    $storebudget = \App\BudgetStore::where('store_id',$store_id)
                                                                    ->where('label',caseChange($k))
                                                                    ->first();
                                    if($storebudget){
                                        $storebudget->budgetManpower = ($v)?($v):0;
                                        $storebudget->actualManpower = (!empty($actual_manpower))?$actual_manpower->total_manpower:0;
                                        $storebudget->save();
                                    } else {
                                        if($is_valid_lable){
                                            $a['store_id'] = $store_id;
                                            $a['label'] = caseChange($k);
                                            $a['budgetManpower'] = ($v)?($v):0;
                                            $a['actualManpower'] = (!empty($actual_manpower))?$actual_manpower->total_manpower:0;
                                            array_push($arr,$a);
                                        }

                                    }
                                }

                            }
                     /*   }else {
                            \Session::flash('flash_error',"Not Valid Format of Excel Sheet");
                            return back()->withError("Not Valid Format of Excel Sheet");
                        } */

                }
                    if(!empty($arr)){
                        \App\BudgetStore::insert($arr);
                    }

                    return redirect()->back()->with('flash_success', 'Record imported successfully!');
                }
            }
            catch(Exception $e){
                \Session::flash('flash_error',$exception->getMessage());
                return back()->withError($exception->getMessage());
            }
        }

        return redirect()->back()->with('flash_error', 'Not any data to upload.');

    }

    public function showLableFileUpload()
    {
        return view('admin.storemanpower.lablemanpower');
    }

}
