<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
// use App\Http\Controllers\MainController as Controller;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\SourceDetail;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use App\EducationCategory;
use App\NatureOfEmployment;
use App\EducationDetail;
use App\PreviousExperience;
use App\FamilyInfo;
use Carbon\Carbon as Carbon;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.admin.employee');
        $this->middleware('permission:access.admin.employee.edit')->only(['edit','update']);
        $this->middleware('permission:access.admin.employee.create')->only(['create', 'store']);
        $this->middleware('permission:access.admin.employee.delete')->only('destroy');

        view()->share('route', 'employee');
        view()->share('module', 'Employee');
    }

    public function index(Request $request)
    {
        return view('admin.employee.index');
    }
    public function storelist(Request $request)
    {
        return view('admin.employee.store');
    }
    public function regionallist(Request $request)
    {
        return view('admin.employee.regional');
    }

    public function datatable(Request $request)
    {

        $employee = Employee::select([
                    'employee.*',
                    'designation.name as designation',
                    'store_location.store_name',
                    'store_location.location_code',
                ])
                ->leftJoin('designation',function($join){
                    $join->on('employee.designation_id','=','designation.id');
                })
                ->leftJoin('store_location',function($join){
                    $join->on('employee.store_id','=','store_location.id');
                })
                ->whereIn('hrms_status',[2])
                ->where('store_location.status',1)
                ->orderBy('employee.id','DESC');

        if($request->status != 'All'){
            $employee->where('processstatus',$request->status);
        }
           $employee =  $employee->get();

        return Datatables::of($employee)
            ->make(true);
    }
    public function storeemployeedatatable(Request $request)
    {
        $employee = Employee::select([
                    'employee.*',
                    'designation.name as designation',
                    'store_location.store_name',
                    'store_location.location_code',
                ])
                ->leftJoin('designation',function($join){
                    $join->on('employee.designation_id','=','designation.id');
                })
                ->leftJoin('store_location',function($join){
                    $join->on('employee.store_id','=','store_location.id');
                })
                ->whereIn('hrms_status',[2])
                ->where('employee.status','confirm')
                ->where('store_id',$request->route('store_id'))
                ->orderBy('employee.id','DESC')
                ->get();

        return Datatables::of($employee)
            ->make(true);
    }

    public function employee_list(Request $request)
    {
        return view('admin.employee.store_employee_list');
    }
    public function storedatatable(Request $request)
    {
        $store = StoreLocation::select([
                    'store_location.*',
                    'store_location.io_code as store_code'
                ])
                ->where('store_location.region_id',$request->route('regional_id'))
                ->where('store_location.status',"1")

                ->orderBy('store_location.id','DESC')
                ->get();

        return Datatables::of($store)
            ->make(true);
    }

    public function regionaldatatable(Request $request)
    {
        $employee = Region::selectRaw(
                    'region.*,
                    region.id as region_id,
                    count(store_location.id) as total_store'
                )
                ->leftJoin('store_location','store_location.region_id','region.id')
                ->where('region.status',1)
                ->where('store_location.status',1)
                ->orderBy('region.id','ASC')
                ->groupby('region.id')
                ->get();

        return Datatables::of($employee)
            ->make(true);
    }

    public function logView(Request $request,$id)
    {
        view()->share('id',$id);
        return view('admin.employee.log');
    }

    public function log(Request $request,$id)
    {
        $logs = Log::select([
                    'logs.*',
                    'users.emp_name as user_name',
                    'roles.label as user_role',
                ])
                ->leftJoin('users',function($join){
                    $join->on('users.id','=','logs.user_id');
                })
                ->leftJoin('roles',function($join){
                    $join->on('roles.id','=','logs.user_role_id');
                })
                // ->where('user_id',auth()->id())
                ->where('logs.emp_id',$id)
                ->orderBy('logs.id','DESC')
                ->latest()->get();

        return Datatables::of($logs)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return redirect('admin/employee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //not allowed
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = Employee::with(['currentState','currentCity','permanentState','permanentCity','region','attachment','businessUnit','legalEntity','employeeClassification','sourceCategory','sourceDetail','storeLocation','grade','designation','subDepartment','subDepartment.department','brand'])->find($id);

        if(!$employee){
            return redirect('admin/employee')->with('flash_error', 'Employee Not Found!');
        }

        $employee->education = EducationDetail::where('employee_id',$id)->get()->toArray();
        $employee->previous = PreviousExperience::where('employee_id',$id)->get()->toArray();
        $employee->family = FamilyInfo::select([
                        'family_information.*',
                        \DB::raw("CONCAT(date,'/',month,'/',year) as date"),
                    ])->where('employee_id',$id)->get()->toArray();

        $employee->otherinfo = \App\OtherInfo::where('employee_id',$id)->first();
        $educationCategory = EducationCategory::where("status","1")->pluck('name','id')->toArray();
        view()->share('educationCategory',$educationCategory);

        $natureOfEmployment = NatureOfEmployment::where("status","1")->pluck('name','id')->toArray();
        view()->share('natureOfEmployment',$natureOfEmployment);

        return view('admin.employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {

    }

    public function getSubDepartments(Request $request,$departmentId=0)
    {
        return SubDepartment::where("status","1")->where('department_id',$request->get('department_id',$departmentId))->pluck('name','id')->toArray();
    }

    public function getSourceDetail(Request $request,$name=0)
    {
        return SourceCategory::where("status","1")->where('id',$request->get('name',$name))->first();
    }

    public function getLocationDetail(Request $request,$locationCode=0)
    {
        return StoreLocation::where("status","1")->where('id',$request->get('locationCode',$locationCode))->first();
    }

    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function showUpload()
    {
        return view('admin.employee.upload');
    }

    public function upload(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){

            try {
                $path = $request->file('excel')->getRealPath();
                $data = \Excel::load($path)->get();

                $grade = array_change_key_case(Grade::pluck('id','name')->toArray(), CASE_LOWER);
                $employeeCode = array_change_key_case(Employee::pluck('id','employee_code')->toArray(), CASE_LOWER);
                $designation = array_change_key_case(Designation::pluck('id','name')->toArray(), CASE_LOWER);
                $department = array_change_key_case(Department::pluck('id','name')->toArray(), CASE_LOWER);
                $departmentId = SubDepartment::pluck('department_id','id')->toArray();
                $subDepartment = array_change_key_case(SubDepartment::pluck('id','name')->toArray(), CASE_LOWER);
                $locationCode = array_change_key_case(StoreLocation::pluck('id','io_code')->toArray(), CASE_LOWER);
                $education = array_change_key_case(EducationCategory::pluck('id',\DB::raw("TRIM(name) as name"))->toArray(), CASE_LOWER);
                $region = array_change_key_case(Region::pluck('id','name')->toArray(), CASE_LOWER);
                $stateId = array_change_key_case(City::pluck('state_id','id')->toArray(), CASE_LOWER);
                $state = array_change_key_case(State::pluck('id','name')->toArray(), CASE_LOWER);
                $city = array_change_key_case(City::pluck('id','name')->toArray(), CASE_LOWER);
                if($data->count()){
                    $valnotExists = $valLocationCode = $valSourceDetail = $valSourceCategory = $valJoiningKit = $valCtc = $valAmount = $valMonth = [];

                    if(count($data) >= 1 && count($data) <= 3 ){
                        $data = $data[0];
                    }
				//echo "<pre>"; print_r($data->count()); exit();

                    foreach ($data as $key => $value) {

                        if((array_key_exists(caseChange(trim($value->stakeholder_region)),$region) || $value->stakeholder_region == '') && array_key_exists(caseChange($value->store_io_code),$locationCode)){

                            $arr[$key]['user_id'] = auth()->id();
                            $arr[$key]['status'] = 'confirm';
                            $arr[$key]['full_name'] = trim($value->empname);
                            $arr[$key]['employee_code'] = trim($value->empcode);
                            $arr[$key]['gender'] = (trim(strtolower($value->gender),' ') == 'male')?'male':'female';

                            $date = date_create(trim($value->dob));
                            if ($date) {
                                $date_of_birth = \Carbon\Carbon::parse(trim($value->dob))->format('Y-m-d');
                            } else {
                                $date_of_birth = null;
                            }

                            $arr[$key]['date_of_birth'] = $date_of_birth;

                            $date = date_create(trim($value->doj));
                            if ($date) {
                                $joining_date = \Carbon\Carbon::parse(str_replace('/', '-',trim($value->doj)))->format('Y-m-d');
                            } else {
                                $joining_date = null;
                            }

                            $arr[$key]['joining_date'] = $joining_date;
                            $date_of_leave = null;
                            if($value->dol != ''){
                            $date = date_create(trim($value->dol));
                                if ($date) {
                                    $date_of_leave = \Carbon\Carbon::parse(trim($value->dol))->format('Y-m-d');
                                } else {
                                    $date_of_leave = null;
                                }
                            }

                            $arr[$key]['date_of_leave'] = $date_of_leave;
                            $arr[$key]['current_address_line_1'] = trim($value->current_address);
                            $arr[$key]['permanent_address_line_1'] = trim($value->permanent_address);
                            $arr[$key]['processstatus'] = trim($value->processstatus);
                            $arr[$key]['payroll_type'] = trim($value->payroll_type);
                            $arr[$key]['company'] = trim($value->company);
                            $arr[$key]['aadhar_number'] = trim($value->aadhar_number);
                            $arr[$key]['aadhar_enrolment_number'] = trim($value->aadhar_enrolment_number);
                            $arr[$key]['pan_number'] = trim($value->pan);
                            $arr[$key]['emergency_person'] = trim($value->emergency_contact_person_name);
                            $arr[$key]['emergency_number'] = trim($value->emergency_contact_number);
                            $arr[$key]['emergency_relationship'] = trim($value->emergency_contact_relationship);
                            $arr[$key]['blood_group'] = trim($value->blood_group);
                            $arr[$key]['marital_status'] = strtolower(trim($value->marital_status));
                            $arr[$key]['nationality'] = trim($value->nationality);
                            $arr[$key]['mobile'] = trim($value->mobile_number);
                            $arr[$key]['email'] = trim($value->personal_email_id);
                            $arr[$key]['esic_number'] = trim($value->esic_number);
                            $date = date_create(trim($value->wedding_anniversary_date));
                            if ($date) {
                                $aniversary_date = \Carbon\Carbon::parse(trim($value->wedding_anniversary_date))->format('Y-m-d');
                            } else {
                            $aniversary_date = null;
                            }
                            $arr[$key]['anniversary_date'] = $aniversary_date;
                            $arr[$key]['ua_number'] = trim($value->universal_account_number_uan);
                            $arr[$key]['highest_qualification'] = trim($value->highest_qualification);
                            $arr[$key]['appointment_letter'] = trim($value->appointment_letter);
                            $arr[$key]['id_card'] = trim($value->id_card);
                            $arr[$key]['uniform'] = trim($value->uniform);
                            $arr[$key]['l0_l1_status'] = trim($value->l0_l1_status);
                            $arr[$key]['l0_l1_certificate'] = trim($value->l0_l1_certificate);
                            $arr[$key]['bank_name'] = trim($value->bank_name);
                            $arr[$key]['account_no'] = trim($value->bank_account_no);
                            $arr[$key]['ifsc'] = trim($value->ifsc_code);
                            $arr[$key]['branch_name'] = trim($value->branch_name);
                            $arr[$key]['store_status'] = 1;
                            $arr[$key]['regional_status'] = 2;
                            $arr[$key]['hrms_status'] = 2;
                            if( $arr[$key]['date_of_leave'] != ''){
                                $arr[$key]['status'] = 'inactive';
                            }

                            if(array_key_exists(caseChange(trim($value->grade_name)),$grade)){
                                $arr[$key]['grade_id'] = $grade[caseChange(trim($value->grade_name))];
                            }else{
                                $gradeInsert['name'] = trim($value->grade_name);
                                $gradeInsert['status'] = 1;
                                $gradeCreate = Grade::create($gradeInsert);
                                $arr[$key]['grade_id'] = $gradeCreate->id;
                                $grade = array_merge([caseChange(trim($value->grade_name))=>$gradeCreate->id], $grade);
                            }

                            if(array_key_exists(caseChange(trim($value->designation_name)),$designation)){
                                $arr[$key]['designation_id'] = $designation[caseChange(trim($value->designation_name))];
                            }else{
                                $designationInsert['name'] = trim($value->designation_name);
                                $designationInsert['status'] = 1;
                                $designationCreate = Designation::create($designationInsert);
                                $arr[$key]['designation_id'] = $designationCreate->id;
                                $designation = array_merge([caseChange(trim($value->designation_name))=>$designationCreate->id], $designation);
                            }

                            if(array_key_exists(caseChange(trim($value->department_name)),$department)){
                                $arr[$key]['department_id'] = $department[caseChange(trim($value->department_name))];
                            }else{
                                $departmentInsert['name'] = trim($value->department_name);
                                $departmentInsert['status'] = 1;
                                $departmentCreate = Department::create($departmentInsert);
                                $arr[$key]['department_id'] = $departmentCreate->id;
                                $department = array_merge([caseChange(trim($value->department_name))=>$departmentCreate->id], $department);
                            }

                            if(array_key_exists(caseChange(trim($value->subdepartment)),$subDepartment)){
                                $arr[$key]['sub_department_id'] = $subDepartment[caseChange(trim($value->subdepartment))];
                            }else{
                                $subDepartmentInsert['department_id'] = $arr[$key]['department_id'];
                                $subDepartmentInsert['name'] = $value->subdepartment;
                                $subDepartmentInsert['status'] = 1;
                                $subDepartmentCreate = SubDepartment::create($subDepartmentInsert);
                                $arr[$key]['sub_department_id'] = $subDepartmentCreate->id;
                                $subDepartment = array_merge([caseChange(trim($value->subdepartment))=>$subDepartmentCreate->id], $subDepartment);
                            }

                            //location code
                            if(array_key_exists(caseChange($value->store_io_code),$locationCode)){
                                $storeCode = caseChange(trim($value->store_io_code));
                                $arr[$key]['store_id'] = $locationCode[$storeCode];
                            }else{
                                array_push($valLocationCode, $key+2);
                            // $arr[$key]['store_id'] = caseChange(trim($value->store_io_code));
                            }

                            if(array_key_exists(caseChange(trim($value->stakeholder_region)),$region) || $value->stakeholder_region == ''){
                                $arr[$key]['region_id'] = isset($region[caseChange(trim($value->stakeholder_region))])?$region[caseChange(trim($value->stakeholder_region))]:0;
                            }else{
                                $regionInsert['name'] = trim($value->stakeholder_region);
                                $regionInsert['status'] = 1;
                               // $regionCreate = Region::create($regionInsert);
                                $arr[$key]['region_id'] = $regionCreate->id;
                                $region = array_merge([caseChange(trim($value->stakeholder_region))=>$regionCreate->id], $region);
                            }

                            if(array_key_exists(caseChange(trim($value->state)),$state) || $value->state == ''){
                                $arr[$key]['state_id'] = isset($state[caseChange(trim($value->state))])?$state[caseChange(trim($value->state))]:0;
                            }else{
                                $stateInsert['region_id'] = $arr[$key]['region_id'];
                                $stateInsert['name'] = trim($value->state);
                                $stateInsert['status'] = 1;
                                $stateCreate = State::create($stateInsert);
                                $arr[$key]['state_id'] = $stateCreate->id;
                                $state = array_merge([caseChange(trim($value->state))=>$stateCreate->id], $state);
                            }

                            if(array_key_exists(caseChange(trim($value->city)),$city) || $value->city == ''){
                                $arr[$key]['city_id'] = isset($city[caseChange(trim($value->city))])?$city[caseChange(trim($value->city))]:0;
                            }else{
                                $cityInsert['state_id'] = $arr[$key]['state_id'];
                                $cityInsert['name'] = trim($value->city);
                                $cityInsert['status'] = 1;
                                $cityCreate = City::create($cityInsert);
                                $arr[$key]['city_id'] = $cityCreate->id;
                                $city = array_merge([caseChange(trim($value->city))=>$cityCreate->id], $city);
                            }

                            if(array_key_exists(caseChange(trim($value->highest_qualification)),$education)){
                                $arr[$key]['edu']['education_category_id'] = $education[caseChange(trim($value->highest_qualification))];
                            }else{
                                $educationInsert['name'] = trim($value->highest_qualification);
                                $educationInsert['status'] = 1;
                                $educationCreate = EducationCategory::create($educationInsert);
                                $arr[$key]['edu']['education_category_id'] = $educationCreate->id;
                                $education = array_merge([caseChange(trim($value->highest_qualification))=>$educationCreate->id], $education);
                            }

                            $arr[$key]['edu']['board_uni'] = trim($value->board_university);
                            $arr[$key]['edu']['institute'] = trim($value->name_of_the_institution);
                            $arr[$key]['edu']['qualification'] = trim($value->qualification_name);
                            $arr[$key]['edu']['specialization'] = trim($value->specialization);
                            $arr[$key]['edu']['year'] = trim($value->month_year_of_passing);


                            $arr[$key]['family']['self'] = trim($value->empname);
                            $arr[$key]['family']['father_name'] = trim($value->father_name);
                            $arr[$key]['family']['mother_name'] = trim($value->mother_name);
                            $arr[$key]['family']['spouse_name'] = trim($value->spouse_name);
                            $arr[$key]['family']['child1_name'] = trim($value->child1_name);
                            $arr[$key]['family']['child2_name'] = trim($value->child2_name);

                        } else {
                            array_push($valnotExists, trim($value->empcode));
                        }
                    }
                    if(count($valLocationCode) > 0){
                        return redirect('admin/show-employee-upload')
                                    ->with('valLocationCode', implode(', ', $valLocationCode))
                                    ->with('flash_error', 'See below errors!');
                    }

                    if(!empty($arr)){
                        $changes_in_employee = array();
                        foreach ($arr as $key => $value) {
                            // Employee Code
                            if(array_key_exists(caseChange($value['employee_code']),$employeeCode) ){
                                $employee_data = $value;
                                $employee_id = $employeeCode[caseChange($value['employee_code'])];
                                $employee = Employee::find($employee_id);
                                if($employee){
                                    $changes_employee = array();
									if($employee->gender != $employee_data['gender'] && !empty($employee_data['gender'])){

                                        $employee->gender = $employee_data['gender'];
                                    }
                                    $employee->save();
                                }
                            } else {
                                $employee = Employee::create(array_except($value,['edu','family']));
                                if($value['status'] != 'inactive'){
                                    $employee_store = \App\StoreLocation::find($value['store_id']);
                                    if($employee_store){
                                        $employee_store->actual_man_power = $employee_store->actual_man_power + 1;
                                        $employee_store->save();
                                    }
                                }
                                $value['edu']['employee_id'] = $employee->id;
                                EducationDetail::create($value['edu']);
                                for($i=1; $i<=4; $i++){
                                    $eduDetail['employee_id'] = $employee->id;
                                    EducationDetail::create($eduDetail);
                                }

                                foreach ($value['family'] as $eduKey=>$eduVal) {
                                    $data = [];
                                    $data['employee_id'] = $employee->id;
                                    if($eduKey == 'self'){
                                        $data['relation'] = 'Self';
                                        $data['aadhar_number'] = $value['aadhar_number'];
                                        if($value['date_of_birth']){
                                            $date = explode('-', $value['date_of_birth']);
                                            $data['date'] = $date[2];
                                            $data['month'] = $date[1];
                                            $data['year'] = $date[0];
                                        }
                                        FamilyInfo::create($data);
                                    }
                                    if($eduKey == 'father_name'){
                                        $data['relation'] = 'Father';
                                        $data['gender'] = 'male';
                                        FamilyInfo::create($data);
                                    }
                                    if($eduKey == 'mother_name'){
                                        $data['relation'] = 'Mother';
                                        $data['gender'] = 'female';
                                        FamilyInfo::create($data);
                                    }
                                    if($eduKey == 'spouse_name'){
                                        $data['relation'] = 'Spouse';
                                        FamilyInfo::create($data);
                                    }
                                    if($eduKey == 'child1_name'){
                                        $data['relation'] = 'Child';
                                        FamilyInfo::create($data);
                                    }
                                    if($eduKey == 'child2_name'){
                                        $data['relation'] = 'Child';
                                        FamilyInfo::create($data);
                                    }
                                }
                            }
                        }
                        // if(!empty($changes_in_employee)){
                        //     \DB::table('employee_changes')->insert($changes_in_employee);
                        // }
                        // Employee::insert($arr);
                        if(count($valnotExists) > 0){
                            return redirect('admin/show-employee-upload')
                                        ->with('valnotExists', implode(', ', $valnotExists))
                                        ->with('flash_error', 'See below errors!')
                                        ->with('flash_success', 'Record imported successfully!');
                        }
                        return redirect('admin/employee')
                                    ->with('flash_success', 'Record imported successfully!');
                    }
                    if(count($valnotExists) > 0){
                        return redirect('admin/show-employee-upload')
                                    ->with('valnotExists', implode(', ', $valnotExists))
                                    ->with('flash_error', 'See below errors!');
                    }
                }
            } catch (\Exception $exception) {
                \Session::flash('flash_error',$exception->getMessage());
                return back()->withError($exception->getMessage());
            }
        }
        return redirect('admin/show-employee-upload')
                    ->with('flash_error', 'Request data does not have any files to import.');
    }

    public function ExportEmployeeEducationDetail(Request $request)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);


            $allEmployee = Employee::with(['department','subDepartmentEmp','designation','other_infos','EducationDetail','storeLocation','FamilyInfo','resignation'=> function($query){
                $query->where('resignation_status','complete');
            }
            ])
                    // ->leftJoin('region','region.id','store_location.region_id')
                    // ->leftJoin('city','city.id','store_location.city_id')
                    // ->leftJoin('state','state.id','city.state_id')
                    ->whereIn('employee.hrms_status',[2]);
            if($request->status != 'All' && $request->status != ''){
                $allEmployee->where('processstatus',$request->status);
            }
            $allEmployee = $allEmployee->get();
            $excel_export['header'] = array('Employee Code','Employee Name',"Gender","Date Of Birth","DOJ","DOL","Current Address","Parmenent Address","Process Status","Ctc","Payroll Type","Aadhar Number", "Aadhar Enrolment Number","Pan Number", "Emergency Person","Emergency Number","Emergency Relationship","Blood Group","Marital Status","Nationality","Mobile Number","Personal Email","ESIC Number","Anniversary Date","UA Number","Qualification","Board/Uni","Institute","Specialization","Month & Year of passing","Father Name","Mother Name","Spouse Name","Child Name 1","Child Name 2","DESIGNATION NAME","Department Name","Sub Department Name","Appointment Latter","ID Card Status","Uniform Status","L0L1 status","L0L1 Certificate Status", "Store IO CODE","Store Name","Store Code","City","Region","State","Bank Name","Bank Account No","IFSC","Branch Name");
                foreach ($allEmployee as $employee) {
					
                    $excel_export[$employee->id]['Employee_Code'] = $employee->employee_code;
                    $excel_export[$employee->id]['Employee_Name'] = $employee->full_name;
                    $excel_export[$employee->id]['gender'] = $employee->gender;
                    $excel_export[$employee->id]['date_of_birth'] = $employee->date_of_birth;
                    $excel_export[$employee->id]['DOJ'] = $employee->joining_date;
                    $excel_export[$employee->id]['DOL'] = $employee->date_of_leave;
					if($employee->date_of_leave == '' && !empty($employee->resignation)){
						$excel_export[$employee->id]['DOL'] = $employee->resignation->last_working_date;
                    }
                   $Current_Address = $employee->current_address_line_1.' '.$employee->current_address_line_2.' '.$employee->current_address_line_3.' ';
				   $Current_Address .= ($employee->currentCity != null) ? $employee->currentCity->name : '';
				   $Current_Address .= ', ';
				   $Current_Address .= ($employee->currentState != null) ? $employee->currentState->name : '';
				   $Current_Address .= '-'.$employee->current_pincode;
                   $Parmenent_Address = $employee->permanent_address_line_1.' '.$employee->permanent_address_line_2.' '.$employee->permanent_address_line_3.' ';
				   $Parmenent_Address .= ($employee->permanentCity != null) ? $employee->permanentCity->name : '';
				   $Parmenent_Address .= '-'.$employee->permanent_pincode;

                    $excel_export[$employee->id]['Current_Address'] = $Current_Address;
                    $excel_export[$employee->id]['Parmenent_Address'] = $Parmenent_Address;
                    $excel_export[$employee->id]['processstatus'] = $employee->processstatus;
                    $excel_export[$employee->id]['salary_amount'] = $employee->salary_amount;
                    $excel_export[$employee->id]['payroll_type'] = $employee->payroll_type;
                    $excel_export[$employee->id]['aadhar_number'] = $employee->aadhar_number;
                    $excel_export[$employee->id]['aadhar_enrolment_number'] = $employee->aadhar_enrolment_number;
                    $excel_export[$employee->id]['pan_number'] = $employee->pan_number;
                    $excel_export[$employee->id]['emergency_person'] = $employee->emergency_person;
                    $excel_export[$employee->id]['emergency_number'] = $employee->emergency_number;
                    $excel_export[$employee->id]['emergency_relationship'] = $employee->emergency_relationship;
                    $excel_export[$employee->id]['blood_group'] = $employee->blood_group;
                    $excel_export[$employee->id]['marital_status'] = $employee->marital_status;
                    $excel_export[$employee->id]['nationality'] = $employee->nationality;
                    $excel_export[$employee->id]['Mobile_Number'] = $employee->mobile;
                    $excel_export[$employee->id]['Personal_email'] = $employee->email;
                    $excel_export[$employee->id]['esic_number'] = $employee->esic_number;
                    $excel_export[$employee->id]['anniversary_date'] = $employee->anniversary_date;
                    $excel_export[$employee->id]['ua_number'] = $employee->ua_number;
                    $excel_export[$employee->id]['qualification'] = $employee->EducationDetail[0]->qualification_name;
                    $excel_export[$employee->id]['board_uni'] = $employee->EducationDetail[0]->board_uni;
                    $excel_export[$employee->id]['institute'] = $employee->EducationDetail[0]->institute;
                    $excel_export[$employee->id]['specialization'] = $employee->EducationDetail[0]->specialization;
                    $excel_export[$employee->id]['passing_month'] = $employee->EducationDetail[0]->month." - ".$employee->EducationDetail[0]->year;
                    $excel_export[$employee->id]['Father_Name'] = '';
                    $excel_export[$employee->id]['Mother_Name'] = '';
                    $excel_export[$employee->id]['Spouse_Name'] = '';
                    $excel_export[$employee->id]["Child_Name_1"] = '';
                    $excel_export[$employee->id]["Child_Name_2"] = '';
                    $child = 1;
                    foreach ($employee->FamilyInfo as $family_detail) {
                        if($family_detail->relation == 'Father'){
                            $excel_export[$employee->id]['Father_Name'] = $family_detail->name;
                        } elseif ($family_detail->relation == 'Mother') {
                            $excel_export[$employee->id]['Mother_Name'] = $family_detail->name;
                        } elseif ($family_detail->relation == 'Spouse') {
                            $excel_export[$employee->id]['Spouse_Name'] = $family_detail->name;
                        } elseif ($family_detail->relation == 'Child' && $child < 2) {
                            $excel_export[$employee->id]["Child_Name_$child"] = $family_detail->name;
                            $child++;
                        }
                    }
                    if(!empty($employee->designation)){
                        $excel_export[$employee->id]['DESIGNATION_NAME'] = $employee->designation->name;
                    }else{
                        $excel_export[$employee->id]['DESIGNATION_NAME'] = '';
                    }

                    if(!empty($employee->department)){
                        $excel_export[$employee->id]['Department_name'] = $employee->department->name;
                    }else{
                        $excel_export[$employee->id]['Department_name'] = '';
                    }
                    if(!empty($employee->subDepartmentEmp)){
                        $excel_export[$employee->id]['Sub_Department_name'] = $employee->subDepartmentEmp->name;
                    }else{
                        $excel_export[$employee->id]['Sub_Department_name'] = '';
                    }

                    if(!empty($employee->other_infos)){
                        $excel_export[$employee->id]['Appointment_Latter'] = $employee->other_infos->appointment_status;
                        $excel_export[$employee->id]['IDCard_Status'] = $employee->other_infos->id_card_status;
                        $excel_export[$employee->id]['uniform_status'] = $employee->other_infos->uniform_status;
                        $excel_export[$employee->id]['l0l1_status'] = $employee->other_infos->l0l1_status;
                        $excel_export[$employee->id]['l0l1_certificate_status'] = $employee->other_infos->l0l1_certificate_status;
                    }else{
                        $excel_export[$employee->id]['Appointment_Latter'] = 'NO';
                        $excel_export[$employee->id]['IDCard_Status'] = 'NO';
                        $excel_export[$employee->id]['uniform_status'] = 'NO';
                        $excel_export[$employee->id]['l0l1_status'] = 0;
                        $excel_export[$employee->id]['l0l1_certificate_status'] = 'NO';
                    }
                    if(!empty($employee->storeLocation)){
                        $excel_export[$employee->id]['Store_IO_CODE'] = $employee->storeLocation->io_code;
                        $excel_export[$employee->id]['store_name'] = $employee->storeLocation->store_name;
                        $excel_export[$employee->id]['Store_Code'] = $employee->storeLocation->sap_code;
                        $excel_export[$employee->id]['City'] = $employee->storeLocation->city->name;
                        $excel_export[$employee->id]['Region'] = $employee->storeLocation->region->name;
                        $excel_export[$employee->id]['State'] = $employee->storeLocation->state->name;
                    }else{
                        $excel_export[$employee->id]['Store_IO_CODE'] = '';
                        $excel_export[$employee->id]['store_name'] = '';
                        $excel_export[$employee->id]['Store_Code'] = '';
                        $excel_export[$employee->id]['City'] = '';
                        $excel_export[$employee->id]['Region'] = '';
                        $excel_export[$employee->id]['State'] = '';
                    }

                    $excel_export[$employee->id]['bank_name'] = $employee->bank_name;
                    $excel_export[$employee->id]['Bank_Account_no'] = $employee->Bank_Account_no;
                    $excel_export[$employee->id]['ifsc'] = $employee->ifsc;
                    $excel_export[$employee->id]['branch_name'] = $employee->branch_name;
                }

                \Excel::create("All Employee - ". date('d/m/Y H-i-s'),function($excel) use ($excel_export){
                $excel->setTitle('All Employee');
                $excel->sheet('All Employee',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                });
            })->download('xlsx');
            return back();
    }
}
