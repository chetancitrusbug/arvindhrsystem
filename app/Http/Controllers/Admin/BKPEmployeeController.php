<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
// use App\Http\Controllers\MainController as Controller;
use App\Http\Controllers\Controller;

use Yajra\Datatables\Datatables;
use App\Employee;
use App\StoreLocation;
use App\Grade;
use App\Department;
use App\SubDepartment;
use App\BusinessUnit;
use App\Brand;
use App\LegalEntity;
use App\EmployeeClassification;
use App\SourceCategory;
use App\VariablePayType;
use App\State;
use App\City;
use App\Region;
use App\Attachment;
use App\Designation;
use App\Log;
use Carbon\Carbon as Carbon;

class BKPEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.admin.employee');
        $this->middleware('permission:access.admin.employee.edit')->only(['edit','update']);
        $this->middleware('permission:access.admin.employee.create')->only(['create', 'store']);
        $this->middleware('permission:access.admin.employee.delete')->only('destroy');

        view()->share('route', 'employee');
        view()->share('module', 'Employee');
    }

    public function index(Request $request)
    {
        return view('admin.employee.index');
    }

    public function datatable(Request $request)
    {
        $employee = Employee::select([
                    'employee.*',
                ])
                ->whereIn('hrms_status',[2])
                ->orderBy('employee.id','DESC')
                ->get();

        return Datatables::of($employee)
            ->make(true);
    }

    public function logView(Request $request,$id)
    {
        view()->share('id',$id);
        return view('admin.employee.log');
    }

    public function log(Request $request,$id)
    {
        $logs = Log::select([
                    'logs.*',
                    'users.emp_name as user_name',
                    'roles.label as user_role',
                ])
                ->leftJoin('users',function($join){
                    $join->on('users.id','=','logs.user_id');
                })
                ->leftJoin('roles',function($join){
                    $join->on('roles.id','=','logs.user_role_id');
                })
                // ->where('user_id',auth()->id())
                ->where('logs.emp_id',$id)
                ->orderBy('logs.id','DESC')
                ->latest()->get();

        return Datatables::of($logs)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return redirect('admin/employee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        //not allowed
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = Employee::with(['grade','designation','subDepartment','subDepartment.department','businessUnit','legalEntity','employeeClassification','sourceCategory','variablePayType','brand','state','city','region','attachment'])->find($id);

        return view('admin.employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        $employee->attach = Attachment::where('employee_id',$id)->get();
        $attach = [];
        foreach ($employee->attach as $value) {
            if(in_array($value->key, ['education_documents','previous_company_docs'])){
                $attach[$value->key][$value->id]['file'] = $value->file;
                $attach[$value->key][$value->id]['key'] = $value->key;
            }else{
                $attach[$value->key]['file'] = $value->file;
                $attach[$value->key]['key'] = $value->key;
            }
        }
        $employee->attach = $attach;

        $request = new Request();

        $grades = Grade::where("status","1")->pluck('name','id')->toArray();
        view()->share('grades',$grades);

        $departments = Department::where("status","1")->pluck('name','id')->toArray();
        view()->share('departments',$departments);

        $designation = Designation::where("status","1")->pluck('name','id')->toArray();
        view()->share('designation',$designation);

        $subDepartments = $city = [];
        if(old('department_id',$employee->department_id)){
            $subDepartments = $this->getSubDepartments($request,['department_id'=>old('department_id',$employee->department_id)]);
        }
        view()->share('subDepartments',$subDepartments);

        $sourceDetail = $this->getSourceDetail($request,old('source_name',$employee->source_name));
        $employee->source_category = isset($sourceDetail->category)?$sourceDetail->category:'';
        $employee->source_code = isset($sourceDetail->source_code)?$sourceDetail->source_code:'';

        $businessUnits = BusinessUnit::where("status","1")->pluck('name','id')->toArray();
        view()->share('businessUnits',$businessUnits);

        $brands = Brand::where("status","1")->pluck('name','id')->toArray();
        view()->share('brands',$brands);

        $legalEntity = LegalEntity::where("status","1")->pluck('name','id')->toArray();
        view()->share('legalEntity',$legalEntity);

        $employeeClassification = EmployeeClassification::where("status","1")->pluck('name','id')->toArray();
        view()->share('employeeClassification',$employeeClassification);

        $sourceName = SourceCategory::where("status","1")->pluck('source_name','id')->toArray();
        view()->share('sourceName',$sourceName);

        $variablePayType = VariablePayType::where("status","1")->pluck('name','id')->toArray();
        view()->share('variablePayType',$variablePayType);

        $states = State::where("status","1")->pluck('name','id')->toArray();
        view()->share('states',$states);

        $region = Region::where("status","1")->pluck('name','id')->toArray();
        view()->share('region',$region);

        $locationCode = StoreLocation::where("status","1")->pluck('location_code','id')->toArray();
        view()->share('locationCode',$locationCode);

        if(old('state_id',$employee->state_id)){
            $city = $this->getCityFromState($request,['state_id'=>old('state_id',$employee->state_id)]);
        }
        view()->share('city',$city);

        return view('admin.employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['joining_date'] = Carbon::parse(str_replace('/', '-',$requestData['joining_date']))->format('Y-m-d');

        $employee = Employee::where('id',$id)->update(array_except($requestData,['attach','_method','_token','source_category','source_code']));

        if(isset($requestData['attach']) && !empty($requestData['attach'])){
            $name = str_replace(' ','_',$request->name);
            foreach ($requestData['attach'] as $key=>$value) {
                $file = [];
                $file['key'] = $key;
                $file['employee_id'] = $id;

                if(in_array($key, ['education_documents','previous_company_docs'])){
                    $oldDocs = Attachment::where('key',$key)->where('employee_id',$id)->get();

                    if(count($oldDocs)){
                        foreach ($oldDocs as $k => $v) {
                            if(file_exists($v->file)){
                                unlink($v->file); //delete previously uploaded Attachment
                            }
                        }
                    }

                    Attachment::where('key',$key)->where('employee_id',$id)->delete();

                    foreach ($value as $doc_key=>$doc_value) {
                        $filename = $name.'_'.uniqid(time()) . '.' . $doc_value->getClientOriginalExtension();
                        $doc_value->move('uploads/employee/'.$key, $filename);

                        $file['file'] = 'uploads/employee/'.$key.'/'.$filename;
                        $attachment = Attachment::create($file);
                    }
                }else{
                    $oldDocs = Attachment::where('key',$key)->where('employee_id',$id)->get();

                    if(count($oldDocs)){
                        foreach ($oldDocs as $k => $v) {
                            if(file_exists($v->file)){
                                unlink($v->file); //delete previously uploaded Attachment
                            }
                        }
                    }

                    Attachment::where('key',$key)->where('employee_id',$id)->delete();

                    $filename = $name.'_'.uniqid(time()) . '.' . $value->getClientOriginalExtension();
                    $value->move('uploads/employee/'.$key, $filename);
                    $file['file'] = 'uploads/employee/'.$key.'/'.$filename;
                    $attachment = Attachment::create($file);
                }
            }
        }
        //create log
        $log['title'] = "Employee Update";
        $log['emp_id'] = $id;
        $log['emp_name'] = $request->name;
        $log['updated_at'] = date('Y-m-d H:i:s');
        logCreate($log);

        return redirect('admin/employee')->with('flash_success', 'Employee updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $employee = Employee::find($id);

        //delete certificates
        $oldDocs = Attachment::where('employee_id',$id)->get();

        if(count($oldDocs)){
            foreach ($oldDocs as $key => $value) {
                if(file_exists($value->file)){
                    unlink($value->file); //delete previously uploaded Attachment
                }
            }
        }

        Attachment::where('employee_id',$id)->delete();

        $employee->delete();

        if($request->has('from_index')){
            $message = "Employee Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Employee deleted!');

            return redirect('admin/employee');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name' => 'required|regex:/^[a-z0-9 .\-]+$/i',
            'joining_date' => 'required|date_format:d/m/Y',
            'grade_id' => 'required',
            'designation_id' => 'required',
            'department_id' => 'required',
            'sub_department_id' => 'required',
            'business_unit_id' => 'required',
            'reporting_manager_employee_code' => 'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'reporting_manager_name' => 'required|max:191|regex:/^[a-z0-9 .\-]+$/i',
            'brand_id' => 'required',
            'legal_entity_id' => 'required',
            'location_code' => ['required'],
            'io_code' => ['required'],
            'employee_classification_id' => 'required',
            'source_category' => 'required',
            'source_name' => 'required',
            'source_code' => 'required',
            'employee_refferal_amount' => 'required|numeric',
            'employee_refferal_payment_month' => 'required',
            'ctc' => 'required|numeric',
            'soft_copy_attached' => 'required',
            'pod_no' => 'required|max:50',
            'state_id' => 'required',
            'city_id' => 'required',
            'region_id' => 'required',
            'store_name' => ['required','max:191'],
            'attach.scanned_photo' => 'mimes:jpg,jpeg',
            'attach.employment_form' => 'mimes:xls,xlsx,csv',
            'attach.form_11' => 'mimes:pdf',
            'attach.form_02' => 'mimes:pdf',
            'attach.form_f' => 'mimes:pdf',
            'attach.hiring_checklist' => 'mimes:pdf',
            'attach.inteview_assessment' => 'mimes:pdf',
            'attach.resume' => 'mimes:pdf',
            'attach.aadhar_card' => 'mimes:pdf,jpg,jpeg',
            'attach.pan_card' => 'mimes:pdf,jpg,jpeg',
            'attach.bank_passbook' => 'mimes:pdf,jpg,jpeg',
            'attach.offer_accept' => 'mimes:pdf',
            'attach.education_documents' => [function($attribute,$value,$fail) use ($request,$id){
                                            $format = ['pdf','jpg','jpeg','zip'];
                                            $status = false;
                                            foreach ($request->attach['education_documents'] as $value) {
                                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                                    $status = true;
                                                }
                                            }
                                            if($status){
                                                $fail('The education documents must be a file of type: pdf,jpg,jpeg,zip');
                                            }
                                        }],
            'attach.previous_company_docs' => [function($attribute,$value,$fail) use ($request,$id){
                                            $format = ['pdf','jpg','jpeg','zip'];
                                            $status = false;
                                            foreach ($request->attach['previous_company_docs'] as $value) {
                                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                                    $status = true;
                                                }
                                            }
                                            if($status){
                                                $fail('The previous company documents must be a file of type: pdf,jpg,jpeg,zip');
                                            }
                                        }],
        ];

        $rules2 = [];
        if($id == 0){
            $rules2 = [
                'attach.scanned_photo' => 'required|mimes:jpg,jpeg',
                'attach.employment_form' => 'required|mimes:xls,xlsx,csv',
                'attach.form_11' => 'required|mimes:pdf',
                'attach.form_02' => 'required|mimes:pdf',
                'attach.form_f' => 'required|mimes:pdf',
                'attach.hiring_checklist' => 'required|mimes:pdf',
                'attach.inteview_assessment' => 'required|mimes:pdf',
                'attach.resume' => 'required|mimes:pdf',
                'attach.education_documents' => ['required',function($attribute,$value,$fail) use ($request,$id){
                                            $format = ['pdf','jpg','jpeg','zip'];
                                            $status = false;
                                            foreach ($request->attach['education_documents'] as $value) {
                                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                                    $status = true;
                                                }
                                            }
                                            if($status){
                                                $fail('The education documents must be a file of type: pdf,jpg,jpeg,zip');
                                            }
                                        }],
                'attach.aadhar_card' => 'required|mimes:pdf,jpg,jpeg',
                'attach.pan_card' => 'required|mimes:pdf,jpg,jpeg',
                'attach.bank_passbook' => 'required|mimes:pdf,jpg,jpeg',
                'attach.offer_accept' => 'required|mimes:pdf',
                'attach.previous_company_docs' => ['required',function($attribute,$value,$fail) use ($request,$id){
                                            $format = ['pdf','jpg','jpeg','zip'];
                                            $status = false;
                                            foreach ($request->attach['previous_company_docs'] as $value) {
                                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                                    $status = true;
                                                }
                                            }
                                            if($status){
                                                $fail('The previous company documents must be a file of type: pdf,jpg,jpeg,zip');
                                            }
                                        }],
            ];
        }

        return $this->validate($request, array_merge($rules,$rules2),[
            'grade_id.required' => 'The grade field is required',
            'designation_id.required' => 'The designation field is required',
            'department_id.required' => 'The department field is required',
            'sub_department_id.required' => 'The sub department field is required',
            'business_unit_id.required' => 'The business unit field is required',
            'brand_id.required' => 'The brand field is required',
            'legal_entity_id.required' => 'The legal entity field is required',
            'employee_classification_id.required' => 'The employee classification field is required',
            'source_category.required' => 'The source category field is required',
            'soft_copy_attached.required' => 'The completed soft copy of Joining kit attached field is required',
            'state_id.required' => 'The state field is required',
            'city_id.required' => 'The city field is required',
            'region_id.required' => 'The region field is required',
            'attach.scanned_photo.mimes' => 'The scanned photo must be a file of type: jpg.',
            'attach.scanned_photo.required' => 'The scanned photo field is required',
            'attach.employment_form.mimes' => 'The employment form attachment must be a file of type:  xls, xlsx, csv.',
            'attach.employment_form.required' => 'The employment form attachment field is required',
            'attach.form_11.required' => 'The form-11 attachment field is required',
            'attach.form_11.mimes' => 'The form-11 attachment must be a file of type:  pdf.',
            'attach.form_02.required' => 'The form-02 attachment field is required',
            'attach.form_02.mimes' => 'The form-02 attachment must be a file of type:  pdf',
            'attach.form_f.required' => 'The form-f attachment field is required',
            'attach.form_f.mimes' => 'The form-f attachment must be a file of type:  pdf',
            'attach.hiring_checklist.required' => 'The Hiring Checklist attachment field is required',
            'attach.hiring_checklist.mimes' => 'The Hiring Checklist attachment must be a file of type:  pdf',
            'attach.inteview_assessment.required' => 'The Interview Assessment attachment field is required',
            'attach.inteview_assessment.mimes' => 'The Interview Assessment must be a file of type:  pdf',
            'attach.resume.required' => 'The Resume attachment field is required',
            'attach.resume.mimes' => 'The Resume attachment must be a file of type:  pdf',
            'attach.education_documents.required' => 'The Education Documents attachment field is required',
            'attach.aadhar_card.required' => 'The Aadhar Card attachment field is required',
            'attach.aadhar_card.mimes' => 'The Aadhar Card attachment must be a file of type:  pdf,jpg,jpeg',
            'attach.pan_card.required' => 'The Pan Card attachment field is required',
            'attach.pan_card.mimes' => 'The Pan Card attachment must be a file of type:  pdf,jpg,jpeg',
            'attach.bank_passbook.required' => 'The Bank Passbook/Cheque attachment field is required',
            'attach.bank_passbook.mimes' => 'The Bank Passbook/Cheque attachment must be a file of type:  pdf,jpg,jpeg',
            'attach.offer_accept.required' => 'The offer letter acceptancey mail attachment field is required',
            'attach.offer_accept.mimes' => 'The offer letter acceptancey mail attachment must be a file of type:  pdf',
            'attach.previous_company_docs.required' => 'The Previouse company offer letter / payslips / releiving letter attachment field is required',
        ]);
    }

    public function getSubDepartments(Request $request,$departmentId=0)
    {
        return SubDepartment::where("status","1")->where('department_id',$request->get('department_id',$departmentId))->pluck('name','id')->toArray();
    }

    public function getSourceDetail(Request $request,$name=0)
    {
        return SourceCategory::where("status","1")->where('id',$request->get('name',$name))->first();
    }

    public function getLocationDetail(Request $request,$locationCode=0)
    {
        return StoreLocation::where("status","1")->where('id',$request->get('locationCode',$locationCode))->first();
    }

    public function getCityFromState(Request $request,$state=0)
    {
        return City::where("status","1")->where('state_id',$request->get('state_id',$state))->pluck('name','id')->toArray();
    }

    public function changeStatus($table,$status,$id)
    {
        $status = ($status == 0)?1:0;

     /*   if($table == 'invoice'){
            $status = ($status == 0)?'in_progress':'confirm';
        }*/

        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    }
}
