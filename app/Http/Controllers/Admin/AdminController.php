<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\StoreLocation;
use App\Employee;
use App\Region;
use App\OtherInfo;
use DB;
use Yajra\Datatables\Datatables;
use App\HiringReport;
use App\SourceCategory;
use App\StoreCollection;
use DateTime;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
		$manPower = array();
		$manPowerGet = StoreLocation::where('status',1)->select(DB::raw('SUM(budget_man_power) as total_budget_man_power'),DB::raw('SUM(actual_man_power) as total_actual_man_power'),DB::raw('count(id) as total_store'))->get();
		if(count($manPowerGet) > 0){
			$manPower['total_actual_man_power'] = ($manPowerGet[0]->total_actual_man_power > 0) ? $manPowerGet[0]->total_actual_man_power : 0;
			$manPower['total_budget_man_power'] = ($manPowerGet[0]->total_budget_man_power > 0)? $manPowerGet[0]->total_budget_man_power : 0;
			$manPower['total_gap'] = $manPowerGet[0]->total_budget_man_power - $manPower['total_actual_man_power'];
			if($manPowerGet[0]->total_actual_man_power > 0 && $manPower['total_gap'] > 0){
                $manPower['avg_gap_total'] = ($manPower['total_gap']/$manPower['total_gap']);
				$manPower['avg_gap'] = round(($manPower['total_gap']/$manPowerGet[0]->total_actual_man_power ) * 100,2);
			}else{
                $manPower['avg_gap_total'] = 0;
				$manPower['avg_gap'] = 0;
			}
		}else{
			$manPower['total_actual_man_power'] = 0;
			$manPower['total_budget_man_power'] = 0;
			$manPower['total_gap'] = 0;
			$manPower['avg_gap'] = 0;
		}
		$hygiene['id_card'] = OtherInfo::select(DB::raw('count(*) as id_count'))->join('employee','employee.id','other_infos.employee_id')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('employee.processstatus','!=','Inactive')->whereNULL('employee.deleted_at')->where('other_infos.id_card_status','NO')->get();
		$hygiene['id_card'] = $hygiene['id_card'][0]->id_count;

		$hygiene['uniform'] = OtherInfo::select(DB::raw('count(*) as uniform_count'))->join('employee','employee.id','other_infos.employee_id')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('employee.processstatus','!=','Inactive')->whereNULL('employee.deleted_at')->where('other_infos.uniform_status','NO')->get();

		$hygiene['uniform'] = $hygiene['uniform'][0]->uniform_count;

		$hygiene['l0l1_untrained'] = OtherInfo::select(DB::raw('count(*) as l0l1_untrained_count'))->join('employee','employee.id','other_infos.employee_id')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('employee.processstatus','!=','Inactive')->whereNULL('employee.deleted_at')->where('other_infos.l0l1_status','NO')->get();
		$hygiene['l0l1_untrained'] = $hygiene['l0l1_untrained'][0]->l0l1_untrained_count;

		$hygiene['l0l1_certified'] = OtherInfo::select(DB::raw('count(*) as l0l1_certificate_count'))->join('employee','employee.id','other_infos.employee_id')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('employee.processstatus','!=','Inactive')->whereNULL('employee.deleted_at')->where('other_infos.l0l1_certificate_status','NO')->get();
		$hygiene['l0l1_certified'] = $hygiene['l0l1_certified'][0]->l0l1_certificate_count;

		$hygiene['certificate_pending'] = OtherInfo::select(DB::raw('count(*) as l0l1_untrained_count'))->join('employee','employee.id','other_infos.employee_id')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('other_infos.l0l1_certificate_status','NO')->where('employee.processstatus','!=','Inactive')->whereNULL('employee.deleted_at')->get();
		$hygiene['certificate_pending'] = $hygiene['certificate_pending'][0]->l0l1_untrained_count;

		$hygiene['appoint_letter'] = OtherInfo::select(DB::raw('count(*) as appointment_status_count'))->join('employee','employee.id','other_infos.employee_id')->where('other_infos.appointment_status','NO')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->where('employee.processstatus','!=','Inactive')->whereNULL('employee.deleted_at')->get();
		$hygiene['appoint_letter'] = $hygiene['appoint_letter'][0]->appointment_status_count;

		$averageCost = Employee::where('employee.processstatus','!=','Inactive')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->select(DB::raw('sum(ctc) as salary'),DB::raw('count(employee.id) as total_employee'))->get();

		$averageCost = $averageCost[0];
        //dd($averageCost);

        $current_year = date('Y');
        $current_month = date('m');
        if($current_month < 4){
            $current_year--;
        }

        $inmonths = array();
        for($i = 1; $i<=12;$i++){
            $next_year = $current_year+1;
            ($i < 4)?$inmonths[]= sprintf("%02d", $i)."-$next_year"  : $inmonths[]=  sprintf("%02d", $i)."-$current_year";
        }
        $attritionReport = HiringReport::selectRaw('*,sum(absconding) as absconding_total,sum(resignation) as resignation_total')
                        ->whereIn('monthyear',$inmonths)
                       /* ->groupBy('monthyear') */
                        ->first();

        $totalattritionReport = HiringReport::selectRaw('(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,sum(hiring_reports.resignation) as resignation_total,sum(hiring_reports.absconding) as absconding_total')
                        ->whereIn('monthyear',[date('m-Y')])
                        ->first()->toArray();

        $hiringReportChart = HiringReport::selectRaw('*,sum(hiring_reports.pending) as pending_p,sum(hiring_reports.hired) as hired_h,sum(hiring_reports.gap) as gap_g ')
                                        ->whereIn('hiring_reports.monthyear',$inmonths)
                                        ->groupBy('hiring_reports.monthyear')
                                        //   ->groupBy('hiring_reports.monthyear')
                                        ->get();

        $hiringReport = HiringReport::
                        selectRaw('*,sum(pending) as pending_p,sum(hired) as hired_h,sum(hiring_reports.gap) as gap_g ')
                        ->whereIn('monthyear',$inmonths)
                        ->get();
        $totalhiringReport = HiringReport::selectRaw('sum(pending) as pending_total,sum(hired) as hired_total,sum(hiring_reports.gap) as gap_total')
                        ->whereIn('monthyear',[date('m-Y')])
                        ->first();

        $manpowerPercentageReport = HiringReport::selectRaw('(SUM(hiring_reports.absconding) + SUM(hiring_reports.resignation)) As Total,SUM(store_location.actual_man_power) as total_actual_man_power')
                                ->leftJoin('store_location','store_location.id','hiring_reports.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->whereIn('monthyear',[date('m-Y')])
                                ->first();

      /*  return view('admin.dashboard',compact('manPower','hygiene','averageCost','hiringReport','totalhiringReport','hiringReportChart','totalattritionReport','attritionReport')); */

		$gender['male'] = Employee::where('employee.processstatus','!=','Inactive')->where('employee.gender','male')->select(DB::raw('count(employee.id) as total_employee'))->get();
		$gender['male'] = $gender['male'][0]->total_employee;
		$gender['female'] = Employee::where('employee.processstatus','!=','Inactive')->where('employee.gender','female')->select(DB::raw('count(employee.id) as total_employee'))->get();
		$gender['female'] = $gender['female'][0]->total_employee;

        $hiringSourceRequirements = $this->hiringSourceRequirements();
        $aging_report = $this->agingregion();
        $pending_employee_data = $this->hiringinprocessreport();
        $gender_attrition = $this->genderwiseattrition();
        $total_pending_employee = Employee::selectRaw('count(employee.id) as total_pending')->join('store_location','store_location.id','employee.store_id')->whereNotIn('employee.status',['confirm','reject','inactive'])->first();

        $productivity_report = $this->productivity_report();
        $productivity_manpower_cost = $this->manpower_cost();

		$attendanceReport = \App\Attendance::getAttendanceReportData();
        $leaveReport = $this->leaveReport();


        $promiseattrition['total_absconding'] = Employee::selectRaw('count(employee.id) as total_absconding')->whereIn('employee.processstatus',['Active-Absconding'])->first();

        $promiseattrition['total_resigned'] = Employee::selectRaw('count(employee.id) as total_resigned')->whereIn('employee.processstatus',['Active-Resigned'])->first();

        $promiseattrition['total_absconding'] = $promiseattrition['total_absconding']->total_absconding;
        $promiseattrition['total_resigned'] = $promiseattrition['total_resigned']->total_resigned;


        return view('admin.dashboard',compact('manPower','hygiene','averageCost','hiringReport','totalhiringReport','hiringReportChart','totalattritionReport','attritionReport','gender','manpowerPercentageReport','aging_report','pending_employee_data','total_pending_employee','hiringSourceRequirements','gender_attrition','productivity_report','productivity_manpower_cost','attendanceReport','leaveReport','promiseattrition'));


    }

	public function resetAttendanceDate(){
		$attritions = \App\Attendance::where('full_date', '=', '')->orWhereNull('full_date')->take(10000)->get();

		foreach($attritions as $atd){
			$atd->full_date = $atd->year."-".date("m", strtotime($atd->month))."-".$atd->day;
			$atd->save();
		}
		exit;
	}


	public function manPowerRegion(){
		$manPower = array();
		$manPowerGet = StoreLocation::where('store_location.status',1)->select(DB::raw('SUM(store_location.budget_man_power) as total_budget_man_power'),DB::raw('SUM(store_location.actual_man_power) as total_actual_man_power'),'region.name')->leftJoin('region','region.id','store_location.region_id')->groupBy('store_location.region_id')->get();

		return view('admin.report.manpower.region',compact('manPowerGet'));
	}
	public function hygieneRegion(){
        $hygiene['id_card'] = Region::leftJoin('store_location','store_location.region_id','region.id')
        ->leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.id_card_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->where('store_location.status',1)
        ->whereNULL('store_location.deleted_at')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as id_card'),'region.name','store_location.region_id')
        ->groupBy('store_location.region_id')
        ->get();

		$hygiene['uniform'] = Region::leftJoin('store_location','store_location.region_id','region.id')
        ->leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.uniform_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->where('store_location.status',1)
        ->whereNULL('store_location.deleted_at')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as uniform_count'),'region.name')
        ->groupBy('store_location.region_id')
        ->get();


		$hygiene['l0l1_untrained'] = Region::leftJoin('store_location','store_location.region_id','region.id')
        ->leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.l0l1_status','NO')->where('employee.processstatus','!=','Inactive')
        ->where('store_location.status',1)
        ->whereNULL('store_location.deleted_at')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as l0l1_untrained_count'),'region.name')
        ->groupBy('store_location.region_id')
        ->get();


		$hygiene['l0l1_certified'] = Region::leftJoin('store_location','store_location.region_id','region.id')
        ->leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.l0l1_certificate_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->where('store_location.status',1)
        ->whereNULL('store_location.deleted_at')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as l0l1_certificate_count'),'region.name')
        ->groupBy('store_location.region_id')
        ->get();


		$hygiene['certificate_pending'] = Region::leftJoin('store_location','store_location.region_id','region.id')
        ->leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.l0l1_certificate_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->where('store_location.status',1)
        ->whereNULL('store_location.deleted_at')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as certificate_pending_count'),'region.name')
        ->groupBy('store_location.region_id')
        ->get();


		$hygiene['appoint_letter'] = Region::leftJoin('store_location','store_location.region_id','region.id')
        ->leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.appointment_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->where('store_location.status',1)
        ->whereNULL('store_location.deleted_at')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as appointment_status_count'),'region.name')
        ->groupBy('store_location.region_id')
        ->get();

		return view('admin.report.hygion.region',compact('hygiene'));
	}

	public function hygieneStore($id){
		$region = Region::find($id);
		return view('admin.report.hygion.regionwisestoredata',compact('id','region'));
	}

	public function hygieneDatatable($id,Request $request){
        $region = Region::find($id);
        $store = StoreLocation::where('store_location.region_id',$id)->get();
        $hygiene['id_card'] = StoreLocation::
        leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.id_card_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as id_card'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('id_card','store_code');
        
        $hygiene['uniform'] = StoreLocation::leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.uniform_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as uniform_count'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('uniform_count','store_code');;


        $hygiene['l0l1_untrained'] = StoreLocation::leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.l0l1_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as l0l1_untrained_count'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('l0l1_untrained_count','store_code');


        $hygiene['l0l1_certified'] = StoreLocation::leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.l0l1_certificate_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as l0l1_certificate_count'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('l0l1_certificate_count','store_code');


        $hygiene['certificate_pending'] = StoreLocation::leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.l0l1_certificate_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as certificate_pending_count'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('certificate_pending_count','store_code');


        $hygiene['appoint_letter'] = StoreLocation::leftJoin('employee','employee.store_id','store_location.id')
        ->leftJoin('other_infos','other_infos.employee_id','employee.id')
        ->where('other_infos.appointment_status','NO')
        ->where('employee.processstatus','!=','Inactive')
        ->whereNULL('employee.deleted_at')
        ->select(DB::raw('count(other_infos.id) as appointment_status_count'),'employee.store_id','store_location.store_name','store_location.io_code as store_code')
        ->where('store_location.region_id',$id)
        ->groupBy('store_location.id')
        ->get()->pluck('appointment_status_count','store_code');

		$data = array();

        $i=0;
        
		foreach($store as $key => $value){
            $store_code = $value->io_code;
            $data[$i]['store_name'] = $value->store_name;
			$data[$i]['store_name'] = $value->store_name;
			$data[$i]['store_code'] = $value->io_code;
			$data[$i]['region_name'] = ($region)?$region->name:'';
			$data[$i]['id_card'] = isset($hygiene['id_card'][$store_code])? $hygiene['id_card'][$store_code] : 0;
			$data[$i]['uniform'] =  isset($hygiene['uniform'][$store_code])?$hygiene['uniform'][$store_code] : 0;
			$data[$i]['l0l1_untrained'] =  isset($hygiene['l0l1_untrained'][$store_code])?$hygiene['l0l1_untrained'][$store_code] : 0;
			$data[$i]['l0l1_certified'] =  isset($hygiene['l0l1_certified'][$store_code])?$hygiene['l0l1_certified'][$store_code] : 0;
			$data[$i]['certificate'] =  isset($hygiene['certificate_pending'][$store_code])?$hygiene['certificate_pending'][$store_code] : 0;
			$data[$i]['appointment'] =  isset($hygiene['appoint_letter'][$store_code])?$hygiene['appoint_letter'][$store_code] : 0;       
            $i++;
        }
        //dd($data);
		$data = collect($data);
        return Datatables::of($data)
            ->make(true);
	}

	public function manPowerCostRegion(){
		$manPower = array();
		$averageCost = Employee::where('employee.processstatus','!=','Inactive')->where('store_location.status',1)->join('store_location','store_location.id','employee.store_id')->leftJoin('region','region.id','store_location.region_id')->select(DB::raw('sum(ctc) as salary'),DB::raw('count(employee.id) as total_employee'),'region.name','region.id as region_id')->groupBy('store_location.region_id')->get();


		return view('admin.report.manpowercostregion',compact('averageCost'));
	}

	public function manPowerCostRegionStore($id){
		$region = Region::find($id);
		return view('admin.report.manpowercostregionstore',compact('id','region'));
	}

	public function manPowerCostRegionStoreData(Request $request,$id){
		$averageCost = Employee::where('employee.processstatus','!=','Inactive')->where('store_location.status',1)->where('store_location.region_id',$id)->join('store_location','store_location.id','employee.store_id')->leftJoin('region','region.id','store_location.region_id')->select(DB::raw('sum(ctc) as salary'),DB::raw('count(employee.id) as total_employee'),DB::raw('( CASE WHEN SUM(ctc) / COUNT(employee.id) > 0 THEN SUM(ctc)/COUNT(employee.id) ELSE 0 END) AS avg_cost'),'region.name as region_name','region.id as region_id','store_location.store_name','store_location.io_code as store_code')->groupBy('store_location.id')->get();
        return Datatables::of($averageCost)
            ->make(true);
    }
    public function agingregion()
    {
        $infant_attrition = 0;
        $baby_attrition = 0;
        $attrition = 0;
        $current_year = date('Y');
        $current_month = date('m');
        if($current_month < 4){
            $current_year--;
        }
        /*$leaved_employee = Employee::selectRaw('resignation.last_working_date,employee.id,employee.joining_date,employee.date_of_leave')
                    ->Join('resignation','employee.id','resignation.employee_id')
                    ->leftJoin('store_location','store_location.id','employee.store_id')
                    ->leftJoin('region','region.id','store_location.region_id')
                    ->where('store_location.status',1)
                    ->where('region.status',1)
                    ->whereNull('resignation.deleted_at')
                    ->where(function ($query) use($current_year) {
                            $query->where('resignation.last_working_date', '>=', date('Y-m-d',strtotime('1-4-'.$current_year)))
                                ->orWhere('employee.date_of_leave', '>=', date('Y-m-d',strtotime('1-4-'.$current_year)));
                    })->get();*/
		$currnetYear = date('Y');
		$previouseYear = date('Y')-1;
		$current_month = date('m');
        if($current_month >= 4){
            $previouseYear = date('Y');
			$currnetYear = date('Y')+1;
        }
		/*$leaved_employee = Employee::selectRaw('resignation.last_working_date,employee.id,employee.joining_date,employee.date_of_leave')
				->Join('resignation','employee.id','resignation.employee_id')
				->leftJoin('store_location','store_location.id','employee.store_id')
				->leftJoin('region','region.id','store_location.region_id')
				->where('store_location.status',1)
				->where('region.status',1)
				->whereNull('resignation.deleted_at')
				->whereNotIn('employee.rm_absconding_status',['1','2','3'])
				->where('employee.processstatus',"Inactive")
				->where('resignation.resignation_status',"complete")
				->where(function($query) use ($previouseYear,$currnetYear) { 
					$query->whereBetween('employee.date_of_leave',  [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
					$query->orwhereBetween('resignation.last_working_date', [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
				})
				->get(); */
				
		$leaved_employee = \App\Resignation::SelectRaw('resignation.last_working_date,employee.id,employee.joining_date,employee.date_of_leave')
					->join('employee','employee.id','resignation.employee_id')
					->where(function($query) use ($previouseYear,$currnetYear) { 
								$query->whereBetween('employee.date_of_leave',  [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
								$query->orwhereBetween('resignation.last_working_date', [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
						})
					->whereNotIn('employee.rm_absconding_status',['1','2','3'])
					->where('employee.processstatus',"Inactive")
					->where('resignation.resignation_status',"complete")
					->get();
		$leaved_employee_absconding = Employee::selectRaw('employee.absconding_time,employee.id,employee.joining_date,employee.date_of_leave')
				->where('employee.processstatus',"Inactive")
				->whereIN('employee.rm_absconding_status',['2','3'])
				->where(function($query) use ($previouseYear,$currnetYear) { 
					
					$query->whereBetween('employee.absconding_time', [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
				})
				->get(); 
        $baby_attrition = 0;
		$total = 0;
        foreach ($leaved_employee as $employee) {

            $fdate = str_replace('/', '-',$employee->joining_date);
            $tdate = (!empty($employee->date_of_leave))?$employee->date_of_leave:$employee->last_working_date;

            $datetime1 = new DateTime($fdate);
            $datetime2 = new DateTime($tdate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');

            if($days <= 30){
                $infant_attrition= $infant_attrition+1;
            } else if($days >30 && $days <=90 ){
                $baby_attrition = $baby_attrition+1;
            } else {
                $attrition = $attrition+1;
            }
			$total++;
        }
		foreach($leaved_employee_absconding as  $employee){
			$fdate = str_replace('/', '-',$employee->joining_date);
            $tdate = (!empty($employee->absconding_time))?$employee->absconding_time:$employee->absconding_time;

            $datetime1 = new DateTime($fdate);
            $datetime2 = new DateTime($tdate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');

            if($days <= 30){
                $infant_attrition= $infant_attrition+1;
            } else if($days >30 && $days <=90 ){
                $baby_attrition = $baby_attrition+1;
            } else {
                $attrition = $attrition+1;
            }
			$total++;
		}

        $aging_report['baby_attrition'] = $baby_attrition;
        $aging_report['infant_attrition'] = $infant_attrition;
        $aging_report['attrition'] = $attrition;
        $aging_report['total'] = $total;
        return $aging_report;
    }
    public function hiringinprocessreport()
    {
        $store_side_pending = Employee::selectRaw('count(employee.id) as total_pending')
                                    ->whereNotIn('employee.hrms_status',[2])
                                    ->whereIn('employee.store_status',[3,0])
                                    ->whereNotIn('employee.regional_status',[2])
                                    ->whereIN('employee.status',['pending','approve'])
                                    ->first();
        $region_side_pending = Employee::selectRaw('count(employee.id) as total_pending')
                                    ->whereNotIn('employee.hrms_status',[2])
                                    ->whereIn('employee.store_status',[1])
                                    ->whereNotIn('employee.store_status',[3])
                                    ->whereNotIn('employee.regional_status',[2])
                                    ->whereIN('employee.status',['pending','approve'])
                                    ->first();
        $hrms_side_pending = Employee::selectRaw('count(employee.id) as total_pending')
                                    ->where('employee.hrms_status',1)
                                    ->whereIn('employee.regional_status',[2])
                                    ->whereIN('employee.status',['pending','approve'])
                                    ->whereNotIn('employee.hrms_status',[2,3])
                                    ->first();
        $data['store_pending'] = $store_side_pending->total_pending;
        $data['region_pending'] = $region_side_pending->total_pending;
        $data['hrms_pending'] = $hrms_side_pending->total_pending;
        return $data;
    }

    public function hiringSourceRequirements()
    {
        $hiringSourceRequirements = array();
        $sourceCategories = SourceCategory::where('status',1)->get();
        if($sourceCategories){
            $hiringSources = Employee::selectRaw('source_category_id')
                    ->whereDate('employee.created_at','>=',date('Y-m-01'))
                    ->where('employee.status','confirm')
                    ->get();
            foreach ($sourceCategories as $sourceCategory) {
                        $hiringSource = $hiringSources->where('source_category_id',$sourceCategory->id)->count();
                       $hiringSourceRequirements[] = array(
                           'sourece_id' => $sourceCategory->id,
                           'sourece_name' => $sourceCategory->category,
                           'total_hired' => ($hiringSource)?$hiringSource:0,
                       );
            }
        }
        return $hiringSourceRequirements;
    }

    public function genderwiseattrition()
    {
		$currnetYear = date('Y');
		$previouseYear = date('Y')-1;
		$current_month = date('m');
        if($current_month >= 4){
            $previouseYear = date('Y');
			$currnetYear = date('Y')+1;
        }
		$gendermale = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->where('employee.processstatus',"Inactive")
                                    ->where('employee.gender','male')
									->whereIN('employee.rm_absconding_status',['2','3'])
                                    ->where(function($query) use ($previouseYear,$currnetYear) { 
                                        
                                        $query->whereBetween('employee.absconding_time', [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
                                    })
                                    ->first();
		$gendermaleResignation = \App\Resignation::SelectRaw("count(employee.id) as total_employee")
							->join('employee','employee.id','resignation.employee_id')
							->where(function($query) use ($previouseYear,$currnetYear) { 
                                        $query->whereBetween('employee.date_of_leave',  [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
                                        $query->orwhereBetween('resignation.last_working_date', [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
								})
							->where('employee.gender','male')
							->whereNotIn('employee.rm_absconding_status',['1','2','3'])
							->where('employee.processstatus',"Inactive")
							->where('resignation.resignation_status',"complete")
                            ->first();
							
		$genderFemaleResignation = \App\Resignation::SelectRaw("count(employee.id) as total_employee")
							->join('employee','employee.id','resignation.employee_id')
							->where(function($query) use ($previouseYear,$currnetYear) { 
                                        $query->whereBetween('resignation.last_working_date', [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
								})
							->where('employee.gender','female')
							->whereNotIn('employee.rm_absconding_status',['1','2','3'])
							->where('employee.processstatus',"Inactive")
							->where('resignation.resignation_status',"complete")
                            ->first();
        $genderfemale = Employee::selectRaw('count(employee.id) as total_employee')
                                   ->where('employee.processstatus',"Inactive")
                                    ->where('employee.gender','female')
									->whereIN('employee.rm_absconding_status',['2','3'])
                                    ->where(function($query) use ($previouseYear,$currnetYear) { 
                                        $query->whereBetween('employee.date_of_leave',  [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
                                        $query->orwhereBetween('employee.absconding_time', [date($previouseYear.'-04-01'),date($currnetYear.'-03-31')]);
                                    })
                                    ->first();
		
		$gender['male'] = $gendermale->total_employee + $gendermaleResignation->total_employee;
		$gender['female'] = $genderfemale->total_employee + $genderFemaleResignation->total_employee;
        return $gender;
    }

    public function productivity_report()
    {
        $current_month = date('m');
        $current_year = date('Y');
        $current_month--;
        if($current_month < 1){
            $current_month=12;
            $current_year--;
        }
        $data = array();
        $collection_report = StoreCollection::selectRaw('store_collections.*,SUM(store_collections.amount) as total_amount')
                                                    ->where('store_collections.month',$current_month)
                                                    ->where('store_collections.year',$current_year)
                                                    ->first();
        /* $total_employee = Employee::selectRaw('count(employee.id) as total_employee')
                                    ->whereNotIn('employee.status',['inactive','pending','reject','cancel'])
                                    ->first(); */
        $total_employee = StoreLocation::selectRaw('sum(actual_man_power) as total_employee')
                                    ->where('status',1)
                                    ->first();
        $data['total_collection'] = $collection_report->total_amount;
        $data['total_employee'] = $total_employee->total_employee;
        $data['per_person_income'] = ($collection_report->total_amount !=0  && $total_employee->total_employee >0)? round($collection_report->total_amount/$total_employee->total_employee,2):'-';

        return $data;
    }
    public function manpower_cost()
    {
        $data = array();
		$current_month = date('m');
        $current_year = date('Y');
        $current_month--;
        if($current_month < 1){
            $current_month=12;
            $current_year--;
        }
        $collection_report = StoreCollection::selectRaw('store_collections.*,SUM(store_collections.amount) as total_amount')
                                                    ->where('store_collections.month',$current_month)
                                                    ->where('store_collections.year',$current_year)
                                                    ->first();
        $total_employee = Employee::selectRaw('count(employee.id) as total_employee,SUM(employee.ctc) as total_ctc')
                                    ->whereNotIn('employee.status',['inactive','pending','reject'])
                                    ->first();
        $data['total_collection'] = $collection_report->total_amount;
        $data['total_employee'] = $total_employee->total_employee;
        $data['avg_ctc'] = ($total_employee->total_ctc > 0  && $total_employee->total_employee >0)? round($total_employee->total_ctc/$total_employee->total_employee,2):'-';
        $data['per_person_income'] = ($collection_report->total_amount !=0  && $total_employee->total_employee >0)? round($collection_report->total_amount/$total_employee->total_employee,2):'-';

        return $data;
    }

    public function leaveReport()
    {
        $leaveReport = Employee::selectRaw('SUM(other_infos.allocated_leave) as total_allocated_leave, SUM(other_infos.pending_leave) as total_pending_leave, SUM(other_infos.availed_leave) as availed_leave')
                                        ->leftjoin('other_infos','other_infos.employee_id','employee.id')
                                        ->leftJoin('store_location','store_location.id','employee.store_id')
                                        ->leftjoin('region','region.id','store_location.region_id')
                                        ->where('store_location.status',1)
                                        ->where('region.status',1)
                                        ->whereNULL('region.deleted_at')
                                        ->whereNULL('store_location.deleted_at')
                                        ->where('employee.processstatus','!=','Inactive')
                                        ->first();
        return $leaveReport;
    }
    public function avgcostExport(Request $request)
    {
        $averageCost = Employee::where('employee.processstatus','!=','Inactive')
                                ->where('store_location.status',1)
                                ->join('store_location','store_location.id','employee.store_id')
                                ->leftJoin('region','region.id','store_location.region_id')
                                ->select(DB::raw('sum(ctc) as salary'),
                                DB::raw('count(employee.id) as total_employee'),
                                'region.name','region.id as region_id')
                                ->groupBy('store_location.region_id')
                                ->get();
        $excel_header = array('Region','Employee','Avg Cost');
        $excel_export = array();
        $excel_export[] =$excel_header;
        foreach($averageCost as $key => $value){
            $data = array(
                'Region' => $value->name,
                'Employee' =>($value->total_employee > 0) ? $value->total_employee : 0,
                'Avg Cost'=>($value->total_employee > 0) ? number_format($value->salary/$value->total_employee,2) : 0
            );
           $excel_export[] = $data;
        }
        \Excel::create('Avg Cost',function($excel) use ($excel_export){
                $excel->setTitle('Avg Cost');
                $excel->sheet('Avg Cost',function($sheet) use ($excel_export){
                    $sheet->fromArray($excel_export,null,'A1',false,false);
                });
            })->download('xlsx');
    }
}
