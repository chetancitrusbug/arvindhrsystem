<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\LegalEntity;
use App\Rules\Unique;
use Yajra\Datatables\Datatables;

class LegalEntityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    function __construct()
    {
        $this->middleware('permission:access.legalentity');
        $this->middleware('permission:access.legalentity.edit')->only(['edit','update']);
        $this->middleware('permission:access.legalentity.create')->only(['create', 'store']);
        $this->middleware('permission:access.legalentity.delete')->only('destroy');
        
        view()->share('route', 'legal_entity');
        view()->share('module', 'Legal Entity');
    }
    
    public function index(Request $request)
    {
    	return view('admin.legal_entity.index');
    }

    public function datatable(Request $request)
    {
        $legalEntity = LegalEntity::latest()->get();

        return Datatables::of($legalEntity)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('admin.legal_entity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
     	
        $legalEntity = LegalEntity::create($requestData);
        
        return redirect('admin/legal-entity')->with('flash_success', 'Legal Entity added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $legalEntity = LegalEntity::find($id);
        
        return view('admin.legal_entity.show', compact('legalEntity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $legalEntity = LegalEntity::find($id);

        if(!$legalEntity){
            return redirect('admin/legal-entity')->with('flash_error', 'Legal Entity Not Found!');
        }
        
        return view('admin.legal_entity.edit', compact('legalEntity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validateData($request,$id);

        $requestData = $request->all();
        $requestData['status'] = isset($requestData['status'])?1:0;
        
        $legalEntity = LegalEntity::where('id',$id)->update(array_except($requestData,['_method','_token']));
        
        return redirect('admin/legal-entity')->with('flash_success', 'Legal Entity updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request,$id)
    {
        $legalEntity = LegalEntity::find($id);
        
        $legalEntity->delete();

        if($request->has('from_index')){
            $message = "Legal Entity Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Legal Entity deleted!');

            return redirect('admin/legal-entity');
        }
    }

    public function validateData($request, $id = 0)
    {
        $rules = [
            'name'=>['required','max:191','regex:/^[a-z0-9 .\-]+$/i',new Unique('legal_entity','legal entity',$id)],
        ];

        return $this->validate($request, $rules,[
            'name.required' => 'The legal entity field is required',
            'name.regex' => 'The legal entity format is invalid',
        ]);
    }

    public function showUpload()
    {
        return view('admin.legal_entity.upload'); 
    }

    public function upload(Request $request)
    {
        $this->validate($request,[
                'excel'=>'required|mimes:xls,xlsx,csv',
            ]);

        if($request->hasFile('excel')){
            $path = $request->file('excel')->getRealPath();
            $data = \Excel::load($path)->first();

            $legalEntity = array_change_key_case(LegalEntity::pluck('id','name')->toArray(), CASE_LOWER);
            $arr = [];

            if($data->count()){
                foreach ($data as $key => $value) {
                    if(!array_key_exists(caseChange($value->name),$legalEntity)){
                        $arr[$key]['status'] = 1;
                        $arr[$key]['name'] = trim($value->name);

                        $legalEntity = array_merge([caseChange($value->name)=>trim($value->name)], $legalEntity);
                    }
               }
                
                if(!empty($arr)){
                    LegalEntity::insert($arr);
                    return redirect('admin/legal-entity')
                                ->with('flash_success', 'Record imported successfully!');
                }
            }
        }
        return redirect('admin/legal-entity-upload')
                    ->with('flash_error', 'Not any data to upload.');
    }
}
