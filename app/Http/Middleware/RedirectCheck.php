<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

class RedirectCheck
{
    public $attributes;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->hasRole('HRMS')){
            $request->attributes->add(['role' => 'HRMS']);
        }elseif(auth()->user()->hasRole('RM')){
            $request->attributes->add(['role' => 'RM']);
        }elseif(auth()->user()->hasRole('SU')){
            $request->attributes->add(['role' => 'SU']);
        }elseif(auth()->user()->hasRole('SM')){
            $request->attributes->add(['role' => 'SM']);
        }
        /*if (!Auth::guard('web_admin')->check()) {
            return redirect('/admin_login');
        }*/

        return $next($request);
    }
}
