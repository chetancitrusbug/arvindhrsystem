<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PreviousExperience extends Model
{
    use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'previous_experience';

    protected $fillable = ['employee_id','nature_of_employment_id','company_name','joining_date','leaving_date','designation','reason_of_leaving'];

    public function getJoiningDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d/m/Y');
        }
        return $value;
    }

    public function getLeavingDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d/m/Y');
        }
        return $value;
    }

    public function natureEmployee(){
        return $this->hasOne('App\NatureOfEmployment','id','nature_of_employment_id');
    }
}
