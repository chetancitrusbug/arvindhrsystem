<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserUpload extends Model
{
    //
    protected $table = 'user_uploads';
    protected $fillable = ['employee_code','fileuploadPath','ip_address'];
}
