<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetStore extends Model
{
    use SoftDeletes;
    protected $table = 'budget_stores';
    protected $fillable = ['store_id','label','budgetManpower','actualManpower'];
}
