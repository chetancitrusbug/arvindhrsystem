<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StoreLocation extends Model
{
    use SoftDeletes;
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'store_location';

    protected $fillable = ['location_code','io_code','sap_code','store_name','budget_man_power','actual_man_power','sq_feet','brand_id','address','city_id','state_id','region_id','status','store_email','store_password','emp_name','emp_id','mobile_no','pincode','internal_order','legal_entity_id'];

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function storemanager(){
        return $this->belongsTo('App\User','id','store_id');
    }
}
