<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class PDO extends Model
{
    use SoftDeletes;

    protected $table = 'pdo_tbl';
    protected $primaryKey = 'id';

	protected $guarded = ['id'];

    protected $fillable = ['employee_id','pdo_date','pdo_number','courier_name'];

    public function employee()
    {
        return $this->hasOne('App\Employee', 'id', 'employee_id');
    }
}
