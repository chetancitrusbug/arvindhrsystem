<?php
$current = date('Y');
for($i=1950; $i <= $current; $i++){
    $year[$i] = $i;
}

for($i=2018; $i <= $current; $i++){
    $attendaceYear[$i] = $i;
}

return [
    'soft_copy_attached'=>[
        'yes'=>'Yes',
        'no'=>'No',
        'na'=>'NA',
    ],

    'month_list'=>[
        'january'=>'January',
        'february'=>'February',
        'march'=>'March',
        'april'=>'April',
        'may'=>'May',
        'jun'=>'Jun',
        'july'=>'July',
        'august'=>'August',
        'september'=>'September',
        'october'=>'October',
        'november'=>'November',
        'december'=>'December',
    ],

    'month_number'=>[
        'january'=>'1',
        'february'=>'2',
        'march'=>'3',
        'april'=>'4',
        'may'=>'5',
        'jun'=>'6',
        'july'=>'7',
        'august'=>'8',
        'september'=>'9',
        'october'=>'10',
        'november'=>'11',
        'december'=>'12',
    ],

    'year_list'=>$year,

    'attendance_year_list'=>$attendaceYear,

    'duplicate' => [
        'region'=>'name',
    ],

    'region'=>[
        'name',
        'status',
    ],

    'relation_ship'=>[
        '0'=>'Self',
        '1'=>'Father',
        '2'=>'Mother',
        '3'=>'Spouse',
        '4'=>'Child',
        '5'=>'Child',
    ],
    'relationship' =>[
        'Father' => 'Father'
        ,'Mother' => 'Mother'
        ,'Wife' => 'Wife'
        ,'Husband' => 'Husband'
        ,'Daughter' => 'Daughter'
        ,'Son' => 'Son'
        ,'Grandmother' => 'Grandmother'
        ,'Grandfather' => 'Grandfather'
        ,'Niece' => 'Niece'
        ,'Nephew' => 'Nephew'
        ,'Uncle' => 'Uncle'
        ,'Aunt' => 'Aunt'
        ,'Cousin' => 'Cousin'
        ,'Friend' => 'Friend'
        ,'Brother' => 'Brother'
        ,'Sister' => 'Sister'
    ],

    'gender'=>[
        'male'=>'Male',
        'female'=>'Female',
    ],

    'blood_group'=>[
        'O'=>'O',
        'A'=>'A',
        'B'=>'B',
        'AB'=>'AB',
    ],

    'rh_factor'=>[
        'positive'=>'Positive',
        'negative'=>'Negative',
    ],

    'marital_status'=>[
        'single'=>'Single',
        'married'=>'Married',
    ],

    'aadhar_status'=>[
        'available'=>'Available',
        'not_available'=>'Not Available',
    ],

    'pan_status'=>[
        'available'=>'Available',
        'not_available'=>'Not Available',
    ],

    'attendance'=>[
        'wo'=>'W/O',
        'p'=>'Presant',
        'a'=>'Absent',
        'l'=>'Leave',
        'co'=>'C/o',
        'lop'=>'LOP',
        'h'=>'Holiday',
    ],
];
