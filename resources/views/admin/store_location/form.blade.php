<div class="form-group{{ $errors->has('location_code') ? ' has-error' : ''}}">
    {!! Form::label('location_code', '* Location Code: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('location_code', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('location_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('io_code') ? ' has-error' : ''}}">
    {!! Form::label('io_code', '* IO Code: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('io_code', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('io_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('sap_code') ? ' has-error' : ''}}">
    {!! Form::label('sap_code', '* SAP Code: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('sap_code', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('sap_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('store_name') ? ' has-error' : ''}}">
    {!! Form::label('store_name', '* Store Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('store_name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('store_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if (isset($storeLocation->user_id) && $storeLocation->user_id != '')
    <div class="form-group{{ $errors->has('store_email') ? ' has-error' : ''}}">
        {!! Form::label('store_email', '* Store Email: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('store_email', old('store_email',$storeLocation->store_email), ['class' => 'form-control input-sm','disabled']) !!}
            {!! $errors->first('store_email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    {!! Form::hidden('store_email', $storeLocation->store_email, ['class' => 'form-control input-sm']) !!}
@else
    <div class="form-group{{ $errors->has('store_email') ? ' has-error' : ''}}">
        {!! Form::label('store_email', '* Store Email: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('store_email', null, ['class' => 'form-control input-sm']) !!}
            {!! $errors->first('store_email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif
{{-- <div class="form-group{{ $errors->has('store_password') ? ' has-error' : ''}}">
    {!! Form::label('store_password', '* Store Password: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::password('store_password', ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('store_password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('store_confirm_password') ? ' has-error' : ''}}">
    {!! Form::label('store_confirm_password', '* Store Confirm Password: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::password('store_confirm_password', ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('store_confirm_password', '<p class="help-block">:message</p>') !!}
    </div>
</div> --}}
<div class="form-group{{ $errors->has('emp_name') ? ' has-error' : ''}}">
    {!! Form::label('emp_name', '* Employee Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('emp_name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('emp_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('emp_id') ? ' has-error' : ''}}">
    {!! Form::label('emp_id', '* Employee ID: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('emp_id', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('emp_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('mobile_no') ? ' has-error' : ''}}">
    {!! Form::label('mobile_no', '* Mobile No: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('mobile_no', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('mobile_no', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('pincode') ? ' has-error' : ''}}">
    {!! Form::label('pincode', '* Pincode: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('pincode', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('pincode', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('internal_order') ? ' has-error' : ''}}">
    {!! Form::label('internal_order', '* Internal Order: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('internal_order', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('internal_order', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('sq_feet') ? ' has-error' : ''}}">
    {!! Form::label('sq_feet', '* Square Feet: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('sq_feet', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('sq_feet', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('budget_man_power') ? ' has-error' : ''}}">
    {!! Form::label('budget_man_power', '* Budget Man Power: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('budget_man_power', null, ['class' => 'form-control input-sm','min'=>'0']) !!}
        {!! $errors->first('budget_man_power', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('brand_id') ? ' has-error' : ''}}">
    {!! Form::label('brand_id', '* Select Brand: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('brand_id', [''=>'-- Select Brand --']+$brands, null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('brand_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('legal_entity_id') ? ' has-error' : ''}}">
    {!! Form::label('legal_entity_id', '* Select Legal Entity: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('legal_entity_id', [''=>'-- Select Legal Entity --']+$legalEntity, null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('legal_entity_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
    {!! Form::label('address', '* Address: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('address', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('region_id') ? ' has-error' : ''}}">
    {!! Form::label('region_id', '* Select Region: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('region_id', [''=>'-- Select Region --']+$region, null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('state_id') ? ' has-error' : ''}}">
    {!! Form::label('state_id', '* Select State: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('state_id', [''=>'-- Select State --']+$states, null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('state_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('city_id') ? ' has-error' : ''}}">
    {!! Form::label('city_id', '* Select City: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('city_id', [''=>'-- Select city --']+$city, null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::checkbox('status',1, isset($storeLocation->status)?$storeLocation->status:1, ['class'=>['status']]) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@section('scripts')
    <script>
        $('select[name="state_id"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/admin/city-list') }}",
                data: {state_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="city_id"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="city_id"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });
    </script>
@endsection