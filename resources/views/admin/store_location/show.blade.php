@extends('layouts.backend')
@section('title',"View Store Location")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="user-info-list panel panel-default">
                <div class="panel-heading panel-heading-divider">Store # <strong>{{ $storeLocation->store_name }}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/store-location') }}" title="Back">
                            <button class="btn btn-space btn-warning btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    <table class="no-border no-strip skills">
                        <tbody class="no-border-x no-border-y">
                            <tr>
                                <td class="item">Store Name</td>
                                <td>{{ $storeLocation->store_name }}</td>
                            </tr>
                            <tr>
                                <td class="item">Location Code</td>
                                <td>{{ $storeLocation->location_code }}</td>
                            </tr>
                            <tr>
                                <td class="item">SAP Code</td>
                                <td>{{ $storeLocation->sap_code }}</td>
                            </tr>
                            <tr>
                                <td class="item">IO Code</td>
                                <td>{{ $storeLocation->io_code }}</td>
                            </tr>
                            <tr>
                                <td class="item">Brand</td>
                                <td>{{ $storeLocation->brand->name }}</td>
                            </tr>
                            <tr>
                                <td class="item">Address</td>
                                <td>{{ $storeLocation->address }}</td>
                            </tr>
                            <tr>
                                <td class="item">State</td>
                                <td>{{ $storeLocation->state->name }}</td>
                            </tr>
                            <tr>
                                <td class="item">City</td>
                                <td>{{ $storeLocation->city->name }}</td>
                            </tr>
                            <tr>
                                <td class="item">Region</td>
                                <td>{{ $storeLocation->region->name }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection