@extends('layouts.backend')
@section('title',"Store Location")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Store Location List
                        <span class="panel-subtitle mt-10">
                            <a href="{{ url('/admin/store-location/create') }}" title="Create">
                                <button class="btn btn-space btn-success">Create</button>
                            </a>
							<a href="{{ url('/uploads/store_location_sample.xlsx') }}" title="Back" download="sample_store_location_upload" target="_blank">
								<button class="btn btn-space btn-primary">Sample file download</button>
							</a>
                            <a href="{{ url('admin/store-location-upload') }}" title="Create" data-url="" class="btn btn-space btn-primary">
                                Excel Upload
                            </a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive table-minw-fixed">
                            <table style="width:100%;" id="store-location-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Store Code(SAP Code)</th>
                                        <th>Store Name</th>
                                        <th>Brand</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Region</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/admin/store-location-data') }}";
        var edit_url = "{{ url('/admin/store-location') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#store-location-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'store_code',name : 'store_code',"searchable": true, "orderable": false},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},
                { data: 'brand',name : 'brand',"searchable": true, "orderable": false},
                { data: 'city',name : 'city',"searchable": true, "orderable": false},
                { data: 'state',name : 'state',"searchable": true, "orderable": false},
                { data: 'region',name : 'region',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='mdi mdi-edit'></i></a>&nbsp;";

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/store-location')}} data-msg='store location' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";
                        return v+e;
                    }
                }
            ]
        });
</script>


@endsection