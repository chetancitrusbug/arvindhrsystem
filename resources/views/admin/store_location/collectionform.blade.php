@extends('layouts.backend')
@section('title',"Create Store Location")
@section('css')
<style type="text/css">
    input[type="file"] {
        border: none;
    }
</style>
@endsection

@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Upload Store Sales
                    <span class="panel-subtitle">
                        <div class="abs-right20">
                            <a href="{{ url('/admin/regionallist') }}" title="Back">
                                <button class="btn btn-space btn-warning">Back</button>
                            </a>
                            <a href="{{ url('/uploads/store_sales_sample.xlsx') }}" title="Sample File" download="store_sales_sample" target="_blank">
                                <button class="btn btn-space btn-primary">Sample file download</button>
                            </a>
                        </div>
                    </span>
                </div>
                <div class="panel-body">
                    @if (Session::has('error'))
                        <div role="alert" class="alert alert-danger">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                            <span class="icon mdi mdi-close-circle-o"></span>{{ Session::get('error') }}
                        </div>
                    @endif
                    {!! Form::open(['class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        <div class="form-group {{ $errors->has('io_code') ? ' has-error' : ''}}">
                            {!! Form::label('excel_file', '* Select File: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::file('excel_file', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('io_code', '
                                <p class="help-block">:message</p>') !!}
                            </div>
                        </div>
						<div class="form-group ">
							Note : Please Upload data in given formate only.
						</div>
                        <div class="form-group hide {{ $errors->has('date_of_month') ? ' has-error' : ''}}">
                            @php
                                $date_of_month = Date('d/m/Y');
                            @endphp
                            {!! Form::label('date_of_month', '* Date of Collection Month: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6 input-group date">
                                {!! Form::text('date_of_month',
                                old('date_of_month',$date_of_month), ['class' => 'form-control input-sm date_of_month']) !!} {!! $errors->first('date_of_month','<p class="help-block">:message</p>') !!}
                                <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            </div>
                        </div>


                    <div class="form-group col-sm-4">
                        {!! Form::submit('Upload Collection', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$("#date_of_month_div").datetimepicker({
    format:'dd/mm/yyyy',
    endDate: new Date(),
    maxDate: 'today',
    autoclose: true,
    componentIcon:'.mdi.mdi-calendar'
});
</script>
@endsection