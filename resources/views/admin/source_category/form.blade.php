<div class="form-group{{ $errors->has('category') ? ' has-error' : ''}}">
    {!! Form::label('category', '* Category: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('category', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::checkbox('status',1, isset($sourceCategory->status)?$sourceCategory->status:1, ['class'=>['status']]) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>