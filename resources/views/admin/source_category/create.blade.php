@extends('layouts.backend')
@section('title',"Create Source Category")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Create New Source Category
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/source-category') }}" title="Back">
                            <button class="btn btn-space btn-warning btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/source-category', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        @include ('admin.source_category.form')

                        <div class="form-group col-sm-9">
                            {!! Form::submit('Create', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection