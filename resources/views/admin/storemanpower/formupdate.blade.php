<div class="form-group{{ $errors->has('store_id') ? ' has-error' : ''}}">
    {!! Form::label('store_id', '* Store: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('store_id', [''=>'-- Select Store --']+$stores, $store_id ,['class' => 'form-control select2 input-sm',"disabled"=>'true']) !!}
        {!! $errors->first('store_id', '<p class="help-block">:message</p>') !!}
    </div>
     {!! Form::hidden('store_id', $store_id  ) !!}
</div>
{{-- @if ($designations)
    @foreach ($designations as $designation)
        <div class="form-group{{ $errors->has('budget') ? ' has-error' : ''}}">

            {!! Form::label('budget', $designation->name, ['class' => 'col-sm-3 control-label designation']) !!}
            <div class="col-sm-6">
                {!! Form::hidden('designation_id[]', $designation->id ) !!}
                {!! Form::number('budget[]', array_key_exists($designation->id,$storeBudgetManPower)?$storeBudgetManPower[$designation->id]:0, ['class' => 'form-control input-sm budget']) !!}
                {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
@endforeach @endif --}}
@if ($store_designation)
    @foreach ($store_designation as $key => $value)
        <div class="form-group{{ $errors->has('budget') ? ' has-error' : ''}}">
            {!! Form::label('budget', $value, ['class' => 'col-sm-3 control-label designation']) !!}
            <div class="col-sm-6">
                {!! Form::hidden('designation_id[]', $key ) !!}
                {!! Form::number('budget['.$key.']', (isset($storeBudgetManPower[$key])) ? $storeBudgetManPower[$key] : null, ['class' => 'form-control input-sm budget']) !!}
                {!! $errors->first('region_id', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endforeach
@endif
<div class="form-group{{ $errors->has('region_id') ? ' has-error' : ''}}">
    {!! Form::label('total',"Total" , ['class' => 'col-sm-3 control-label designation']) !!}
    <div class="col-sm-6">
        {!! Form::text('total', 0, ['id'=>'total_budget','disabled'=>'disabled','class' => 'form-control input-sm']) !!} {!! $errors->first('region_id',
        '
        <p class="help-block">:message</p>') !!}
    </div>
</div>