@extends('layouts.backend')
@section('title',"Store Budget Manpower Report")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Region Budget Manpower Report
                        <a class="pull-right"><button class="btn btn-primary">Back</button></a>
                    </div>
                    <div class="panel-body ">
                        <div class="table-responsive">
                            <table style="width:100%;" id="employee-budget-table" class="table table-striped responsive table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Store Name</th>
                                        @foreach ($designations as $designation)
                                        <th>{{$designation}}</th>
                                        @endforeach
                                        <th>Total</th>

                                    </tr>
                                </thead>
                                @if (count($store_reports) > 0)
                                <tbody>

                                    @foreach ($store_reports as $store_report)
                                        <tr class="clickable-row" data-href="{{url('admin/store-budget-store')}}/{{$store_report['region_id']}}">
                                            <td><a href="{{url('admin/store-budget-store')}}/{{$store_report['region_id']}}">{{$store_report['region_name']}}</a></td>
                                            @foreach ($store_report['designation'] as $key => $value)
                                            <td>{{$value}}</td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    datatable = $('#employee-budget-table').DataTable({
        responsive: true,
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
    });
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
</script>
@endsection