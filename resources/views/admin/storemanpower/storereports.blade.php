@extends('layouts.backend')
@section('title',"Store Budget Manpower Report")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Store Budget Manpower Report
                        <span class="panel-subtitle">
                            <div class="abs-right20">
                                <a class="" href="{{url('admin/store-budget-store-export')}}/{{request()->route('region_id')}}"><button class="btn btn-primary">Export Report</button></a>
                                <a class="" href="{{url('admin/manpower-report/region')}}"><button class="btn btn-warning">Back</button></a>
                            </div>
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="employee-budget-table" class="table table-striped responsive table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                @php
                                                    $total_array = array();
                                                @endphp
                                                <th>Store IO Code</th>
                                                <th>Store Name</th>
                                                @foreach ($designations as $designation)
                                                    <th>{{$designation}}</th>
                                                @endforeach
                                                <th>Total</th>

                                            </tr>
                                        </thead>
                                            @if (count($store_reports) > 0)
                                                <tbody>

                                                @foreach ($store_reports as $store_report)
                                                    <tr>
                                                        <td>{{$store_report['io_code']}}</td>
                                                        <td>{{$store_report['store_name']}}</td>
                                                        @foreach ($store_report['designation'] as $key => $value)
                                                            @php
                                                                if(isset($total_array[$key])){
                                                                    $total_array[$key] = $total_array[$key] + $value;
                                                                } else {
                                                                    $total_array[$key] = $value;
                                                                }
                                                            @endphp
                                                            <td>{{$value}}</td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <th colspan="2">Total</th>
                                                    @foreach ($total_array as $key =>$value)
                                                    <th>{{$value}}</th>
                                                    @endforeach
                                                </tfoot>
                                            @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    datatable = $('#employee-budget-table').DataTable({
        responsive: true,
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
    });
</script>
@endsection