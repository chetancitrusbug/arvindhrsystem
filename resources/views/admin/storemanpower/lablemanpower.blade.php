@extends('layouts.backend')
@section('title',"Import Store Budget Manpower")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Import Store Budget Manpower
                    <span class="panel-subtitle">
                        <div class="abs-right20">
                            <a href="{{ url()->previous() }}" title="Back">
                                <button class="btn btn-space btn-warning">Back</button>
                            </a>
                            <a href="{{ url('/uploads/store_budget_sample.xlsx') }}" title="Sample file download" download="store_budget_sample" target="_blank">
                                <button class="btn btn-space btn-primary">Sample file download</button>
                            </a>
                        </div>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/store-Lablebudget-excelUpload', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true])
                    !!}

                    <div class="form-group has-error">
                        {!! Form::label('', '', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-6">
                            @if(Session::has('valLocationCode') && Session::get('valLocationCode') != '')
                            <p class="help-block">invalid Location code entered in row {{ Session::get('valLocationCode') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('excel') ? ' has-error' : ''}}">
                        {!! Form::label('excel', '* Excel: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="col-sm-5">
                            {!! Form::file('excel', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('excel', '
                            <p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group ">
                        Note : Please Upload data in given formate only.
                    </div>
                    <div class="form-group col-sm-4">
                        {!! Form::submit('Upload', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection