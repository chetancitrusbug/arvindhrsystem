@extends('layouts.backend')
@section('title',"Store Budget Manpower Create")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Create Budget
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/store-budget') }}" title="Back">
                            <button class="btn btn-space btn-warning btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/store-budget', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true])
                    !!}
                        @include ('admin.storemanpower.form')
                    <div class="form-group col-sm-9">
                        {!! Form::submit('Create', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('.budget').bind('input', function() {
            var sum = 0;
            $(".budget").each(function(){
                sum += +$(this).val();
            });
            $("#total_budget").val(sum);
        });
    </script>
@endsection