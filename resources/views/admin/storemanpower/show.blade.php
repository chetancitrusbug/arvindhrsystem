@extends('layouts.backend')
@section('title',"Store Budget Manpower")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="user-info-list panel panel-default">
                <div class="panel-heading panel-heading-divider">View Store Budget({{$store->store_name}}[{{$store->io_code}}])
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/store-budget-region') }}/{{$store->region_id}}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    @php
                        $total = 0;
                    @endphp
                    @if ($storebudget)
                    <table class="no-border no-strip skills">
                        <thead>
                            <tr>
                                <td>Designation</td>
                                <td>Budget Manpower</td>
                            </tr>
                        </thead>
                        <tbody class="no-border-x no-border-y">
                        @foreach ($storebudget as $storedata)
                        @php
                            $total = $total+$storedata['budgetManpower'];
                        @endphp
                                <tr>
                                    <td class="item">{{$store_designation[$storedata['label']]}}</td>
                                    <td>{{($storedata['budgetManpower'])?$storedata['budgetManpower']:0}}</td>
                                </tr>
                        @endforeach
                                <tr >
                                    <td class="item bold">Total</td>
                                    <td class="bold">{{$total}}</td>
                                </tr>
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('.budget').bind('input', function() {
            var sum = 0;
            $(".budget").each(function(){
                sum += +$(this).val();
            });
            $("#total_budget").val(sum);
        });
</script>
@endsection