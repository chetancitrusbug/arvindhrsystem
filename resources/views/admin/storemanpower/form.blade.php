<div class="form-group{{ $errors->has('store_id') ? ' has-error' : ''}}">
    {!! Form::label('store_id', '* Store: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('store_id', [''=>'-- Select Store --']+$stores, null ,['class' => 'form-control select2 input-sm']) !!}
        {!! $errors->first('store_id', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
@if ($store_designation)
    @foreach ($store_designation as $key => $value)
        <div class="form-group{{ $errors->has('budget') ? ' has-error' : ''}}">
            {!! Form::label('budget', $value, ['class' => 'col-sm-3 control-label designation']) !!}
            <div class="col-sm-6">
                {!! Form::hidden('designation_id[]', $key ) !!}
                {!! Form::number('budget['.$key.']', null, ['class' => 'form-control input-sm budget']) !!}
                {!! $errors->first('region_id', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endforeach
@endif

<div class="form-group{{ $errors->has('region_id') ? ' has-error' : ''}}">
    {!! Form::label('total',"Total" , ['class' => 'col-sm-3 control-label designation']) !!}
    <div class="col-sm-6">
        {!! Form::text('total', 0, ['id'=>'total_budget','disabled'=>'disabled','class' => 'form-control input-sm']) !!}
        {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>