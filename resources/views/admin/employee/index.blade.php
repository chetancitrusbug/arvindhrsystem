@extends('layouts.backend')
@section('title',"Employee")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Employee List
                        <span class="panel-subtitle mt-10">
                            <div class="abs-right20">
                                <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                                    <button class="btn btn-space btn-primary btn-back">Go Back</button>
                                </a>
                                <select id="changeStatus">
                                    <option value="All">All</option>
                                    <option value="Active">Active</option>
                                    <option value="Active-Absconding">Active-Absconding</option>
                                    <option value="Active-Resigned">Active-Resigned</option>
                                    <option value="Inactive">Inactive</option>
                                </select>

                            </div>
                            <a  href="{{ url('admin/show-employee-upload') }}" title="Excel Upload" data-url="" class="btn btn-space btn-primary">
                                Excel Upload
                            </a>
                            <a href="{{ url('/uploads/employee_sample.xlsx') }}" title="Sample file download" download="sample_employee_upload" target="_blank">
                                <button class="btn btn-space btn-primary">Sample file download</button>
                            </a>
                            
                            <a target="_blank" data-link="{{ url('admin/export-employee-details') }}" href="{{ url('admin/export-employee-details') }}" id="excel-download"><button class="btn btn-space btn-primary">Export All Employee</button><span></span></a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive table-minw-fixed">
                         
                            <table style="width:100%;" id="employee-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Employee Code</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Store Name</th>
                                        <th>Location</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('#changeStatus').on('change',function(){
            $('#excel-download').attr('href',$('#excel-download').attr('data-link')+'?status='+$('#changeStatus').val());
            datatable.draw();

        });
        var url ="{{ url('/admin/employee-data') }}";
        var edit_url = "{{ url('/admin/employee') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480,
            "scrollX": true,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            ajax: {
                url:url,
                type:"get",
                data: function (d) {
                    d.status = $('#changeStatus').val();
                }
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { data: 'designation',name : 'designation',"searchable": true, "orderable": false},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},
                { data: 'location_code',name : 'location_code',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
						var nclass = '';


						if(o.status == 'inactive'){
							nclass = 'highlight';
						}
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" class='btn btn-warning btn-sm' ><i class='mdi mdi-eye " + nclass +"' ></i></a>&nbsp;";

                        /*e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='employee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";*/

                        /*d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/employee')}} data-msg='employee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";*/

                        l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-success btn-sm' title='View Log' ><i class='mdi mdi-menu' aria-hidden='true'></i></button></a>&nbsp;";
                        return v+e+l;
                    }

                }
            ]
        });
</script>


@endsection