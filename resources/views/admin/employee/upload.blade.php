@extends('layouts.backend')
@section('title',"Import Employee")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Import Employee
                    <span class="panel-subtitle">
                        <div class="abs-right20">
                            <a href="{{ url('/admin/employee') }}" title="Back">
                                <button class="btn btn-space btn-warning">Back</button>
                            </a>

                            <a href="{{ url('/uploads/employee_sample.xlsx') }}" title="Back" download="sample_employee_upload" target="_blank">
                                <button class="btn btn-space btn-primary">Sample file download</button>
                            </a>
                        </div>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/employeeUpload', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                        <div class="form-group has-error">
                            {!! Form::label('', '', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                @if(Session::has('valLocationCode') && Session::get('valLocationCode') != '')
                                    <p class="help-block">invalid Location code entered in row {{ Session::get('valLocationCode') }}</p>
                                @endif

                                @if(Session::has('valAmount') && Session::get('valAmount') != '')
                                    <p class="help-block">invalid amount entered in row {{ Session::get('valAmount') }}</p>
                                @endif

                                @if(Session::has('valCtc') && Session::get('valCtc') != '')
                                    <p class="help-block">invalid CTC entered in row {{ Session::get('valCtc') }}</p>
                                @endif

                                @if(Session::has('valJoiningKit') && Session::get('valJoiningKit') != '')
                                    <p class="help-block">invalid Joining kit attached option in row {{ Session::get('valJoiningKit') }}</p>
                                @endif

                                @if(Session::has('valSourceCategory') && Session::get('valSourceCategory') != '')
                                    <p class="help-block">invalid source category in row {{ Session::get('valSourceCategory') }}</p>
                                @endif

                                @if(Session::has('valSourceDetail') && Session::get('valSourceDetail') != '')
                                    <p class="help-block">invalid source name in row {{ Session::get('valSourceDetail') }}</p>
                                @endif

                                @if(Session::has('valMonth') && Session::get('valMonth') != '')
                                    <p class="help-block">invalid month in row {{ Session::get('valMonth') }}</p>
                                @endif
                                @if(Session::has('valnotExists') && Session::get('valnotExists') != '')
                                    <p class="help-block">Missmatch region or store location in Employee detail of this employee Code {{ Session::get('valnotExists') }}</p>
                                @endif
                            </div>
                        </div>
                        @include('storeManager.employee.import')

                        <div class="form-group col-sm-4">
                            {!! Form::submit('Upload', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection