@extends('layouts.backend')

@section('content')
<style>
    .clickable-row {
        cursor: pointer
    }
</style>
    <div class="be-content">
	    <div class="main-content container-fluid">
          <div class="row">
            <div class="col-md-6">
				<div class="widget widget-fullwidth widget-1">
				  <div class="widget-head">
					<div class="tools"></div><span class="title">Manpower Report - National</span><span class="description"></span>
				  </div>
				  <div class="widget-chart-container">
					<div id="pie-chart4" style="height: 200px;"></div>
					<div class="chart-table xs-pt-15">
                    <div class="table-responsive">
                        <table class="table table-striped clickable-row" data-href="{{ url('admin/manpower-report/region') }}">
                            <thead class="primary">
                            <tr>
                                <th>Budgeted</th>
                                <th class="number">Available</th>
                                <th class="number">GAP</th>
                                <th class="number">Avg GAP Ageing</th>
                                <th class="number">% GAP</th>
                            </tr>
                            </thead>
                            <tbody class="no-border-x">

                            <tr>
                                <td>{{$manPower['total_budget_man_power']}}</td>
                                <td class="number">{{$manPower['total_actual_man_power']}}</td>
                                <td class="number">{{$manPower['total_gap']}}</td>
                                <td class="number">{{$manPower['total_gap']}}</td>
                                <td class="number">{{$manPower['avg_gap']}}%</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
					</div>
				  </div>
				</div>
            </div>
			<div class="col-md-6">
				<div class="row">
                    {{-- New Changes --}}
                    <div class="white-bgf clearfix">
                        <div class="widget widget-fullwidth widget-1">
                            <div class="widget-head">
                                <span class="title">Attendance Report</span>
                            </div>
                            <div class="widget-chart-container">
                                <!-- <div id="pie-chart4" style="height: 200px;"></div> -->
                                <div class="chart-table">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="widget widget-fullwidth widget-1">
                                                <div class="widget-head">
                                                    <div class="tools">

                                                    </div>
                                                    <span class="title">Manning Report - National({{\Carbon\Carbon::now()->startOfDay()->subDays(1)->format("d/m/Y")}})</span>

                                                </div>
                                                <div class="widget-chart-container">
                                                    <div class="chart-table xs-pt-15">
                                                        <div class="table-responsive">
                                                            <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                                                <thead class="primary">
                                                                    <tr>
                                                                        <th>Live Manpower</th>
                                                                        <th>Present Manpower</th>
                                                                        <th>Present Manpower (%)</th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                                    <tr class="clickable-row" data-href="{{url('admin/attendance-report/region')}}">
                                                                        <td>{{$attendanceReport['actual_man_power']}}</td>
                                                                        <td>{{$attendanceReport['present_man_power']}}</td>
                                                                        <td>{{$attendanceReport['present_per']}}</td>
                                                                    </tr>
                                                                </tbody>


                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    {{-- new Changes END --}}
					{{-- <div class="col-md-12">
						<div class="widget widget-fullwidth widget-1">
							<div class="widget-head ">
								<div class="tools"></div><span class="title">Hiring Report - National(MTD)</span>
								<span class="description"></span>
							</div>
							<div class="table-responsive">
								<table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
									<thead>
										<tr>
											<th>Total GAP</th>
											<th>Total In Process</th>
											<th>Total Hired</th>
											<th>Total Pending</th>
										</tr>
									</thead>
									@if ($totalhiringReport)
										<tbody>
											<tr class="clickable-row" data-href="{{ url('admin/hiring-report/region') }}">
												<td>{{$totalhiringReport->gap_total}}</td>
												<td>{{($total_pending_employee->total_pending)?$total_pending_employee->total_pending:0}}</td>
												<td>{{$totalhiringReport->hired_total}}</td>
												<td>{{$totalhiringReport->pending_total}}</td>
											</tr>
										</tbody>
									@else
										<tr>
											<td>
												No Data Found
											</td>
										</tr>
									@endif
								</table>
							</div>
						</div>
					</div> --}}
					{{-- <div class="col-md-12">
						<div class="widget widget-fullwidth widget-1">
							<div class="widget-head">
								<span class="title">Hiring - Inprocess Report</span>
								<span class="description"></span>
							</div>
							<div class="table-responsive">
								<table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
									<thead>
										<tr>
											<th>Pending at Store</th>
											<th>Pending at RHR</th>
											<th>Peding at HRMS</th>
										</tr>
									</thead>
									@if (count($pending_employee_data) > 0)
									<tbody>
										<tr class="clickable-row" data-href="{{url('admin/hiring-inprocess/')}}">
											<td>{{$pending_employee_data['store_pending']}}</td>
											<td>{{$pending_employee_data['region_pending']}}</td>
											<td>{{$pending_employee_data['hrms_pending']}}</td>
										</tr>
									</tbody>
									@else
									<tr>
										<td>
											No Data Found
										</td>
									</tr>
									@endif
								</table>
							</div>
						</div>
					</div> --}}
				</div>
            </div>

            <div class="col-md-12">
				<div class="row">
                    {{-- Hiring Report MTD New Design --}}
                        <div class="col-md-6">
                            <div class="widget widget-fullwidth widget-1">
                                <div class="widget-head ">
                                    <div class="tools"></div><span class="title">Hiring Report - National(MTD)</span>
                                    <span class="description"></span>
                                </div>
                                <div class="table-responsive">
                                    <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Total GAP</th>
                                                <th>Total In Process</th>
                                                <th>Total Hired</th>
                                                <th>Total Pending</th>
                                            </tr>
                                        </thead>
                                        @if ($totalhiringReport)
                                        <tbody>
                                            <tr class="clickable-row" data-href="{{ url('admin/hiring-report/region') }}">
                                                <td>{{($totalhiringReport->gap_total)?$totalhiringReport->gap_total:0}}</td>
                                                <td>{{($total_pending_employee->total_pending)?$total_pending_employee->total_pending:0}}</td>
                                                <td>{{$totalhiringReport->hired_total}}</td>
                                                <td>{{$totalhiringReport->pending_total}}</td>
                                            </tr>
                                        </tbody>
                                        @else
                                        <tr>
                                            <td>
                                                No Data Found
                                            </td>
                                        </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="widget widget-fullwidth widget-1">
                            <div class="widget-head">
                                <span class="title">Hiring Report - National(YTD)</span>
                                <span class="description"></span>
                            </div>
                            <div class="table-responsive">
                                <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                    <thead>
                                        <tr class="clickable-row" data-href="{{ url('admin/hiring-report/region') }}">
                                            <th>National</th>
                                            <th>GAP</th>
                                            <th>Hired</th>
                                            <th>Pending</th>
                                        </tr>
                                    </thead>
                                    @if ($hiringReport)
                                    <tbody>
                                        @foreach ($hiringReport as $hiring)
                                        <tr class="clickable-row" data-href="{{ url('admin/hiring-report/region') }}">
                                            <td>Total</td>
                                            <td>{{$hiring->gap_g}}</td>
                                            <td>{{$hiring->hired_h}}</td>
                                            <td>{{$hiring->pending_p}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    @endif
                                </table>
                            </div>

                            {{-- <div class="chart-table mt-25">
                                <div class="widget-head">
                                    <span class="title">Hiring Report - National(MTD)</span>
                                    <span class="description"></span>
                                </div>
                                @php
                                        $hiringreportchart = array('region_name'=> array(),'pending'=>array(),'hired'=>array(),'gap'=>array());
                                        foreach ($hiringReportChart as $key => $value) {
                                        $hiringreportchart['region_name'][$key] = date('M-Y',strtotime('01-'.$value->monthyear));
                                        $hiringreportchart['pending'][$key]=($value->pending_p > 0)?$value->pending_p:0;
                                        $hiringreportchart['hired'][$key]= ($value->hired_h > 0)?$value->hired_h:0;
                                        $hiringreportchart['gap'][$key]= ($value->gap_g > 0)?$value->gap_g:0;
                                    }
                                @endphp
                                <canvas id="bar-chart" style="height: 200px;"></canvas>
                            </div> --}}
                            </div>
                        </div>

                    {{-- Hiring Report MTD New Design END --}}






				</div>
            </div>


            {{-- New Design For Attrition Reports --}}
            <div class="col-md-12">
                <div class="row">
                    <div class="widget widget-fullwidth widget-1">

                        <div class="widget-chart-container">
                            <div class="chart-table">
                                <div class="col-md-6">

                                    <div class="widget-head">
                                        <span class="title">Attrition Report - National(MTD)</span>
                                        <span class="description"></span>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget clickable-row" data-href="{{ url('admin/attrition-report/region') }}">
                                            <thead>
                                                <tr>
                                                    <th>Month</th>
                                                    <th>Total</th>
                                                    <th>Absconding</th>
                                                    <th>Resignation</th>
                                                </tr>
                                            </thead>
                                            @if ($totalattritionReport)
                                            <tbody>
                                                <tr>
                                                    <td>{{date('M-Y')}}</td>
                                                    <td>{{($totalattritionReport['Total'])?$totalattritionReport['Total']:0}}</td>
                                                    <td>{{($totalattritionReport['absconding_total'])?$totalattritionReport['absconding_total']:0}}</td>
                                                    <td>{{($totalattritionReport['resignation_total'])?$totalattritionReport['resignation_total']:0}}</td>
                                                </tr>
                                            </tbody>
                                            @else
                                            <tr>
                                                <td>
                                                    No Data Found
                                                </td>
                                            </tr>
                                            @endif
                                        </table>
                                    </div>

                                    <div class="widget-head mt-25">
                                        <span class="title">Attrition report - By percetage of live Manpower</span>
                                        <span class="description"></span>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="man-powerpercentage-report-table" class="table table-striped table-hover table-fw-widget clickable-row" data-href="{{ url('admin/attrition-report/region') }}">
                                            <thead>
                                                <tr>
                                                    <th>Total Man Power</th>
                                                    <th>Total Attrition</th>
                                                    <th>Percentage</th>
                                                </tr>
                                            </thead>
                                            @if ($manpowerPercentageReport)
                                            <tbody>
                                                <tr>
                                                    <td>{{($manPower['total_actual_man_power'])?$manPower['total_actual_man_power']:0}}</td>
                                                    <td>{{($manpowerPercentageReport->Total)?$manpowerPercentageReport->Total:0}}</td>
                                                    <td>
                                                        @if ($manPower['total_actual_man_power'] > 0 && $manpowerPercentageReport->Total > 0)
                                                        {{
                                                         round(($manpowerPercentageReport->Total/$manPower['total_actual_man_power'])*100,2) }} %
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                            @else
                                            <tr>
                                                <td>
                                                    No Data Found
                                                </td>
                                            </tr>
                                            @endif
                                        </table>
                                    </div>

                                    <div class="widget-head mt-25">
                                        <span class="title">To be Separated</span>
                                        <span class="description"></span>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="man-powerpercentage-report-table" class="table table-striped table-hover table-fw-widget clickable-row" data-href="{{ url('admin/attrition-report-promise/region') }}?show=promise-attrition">
                                            <thead>
                                                <tr>
                                                    <th>Total</th>
                                                    <th>Total Absconding</th>
                                                    <th>Total Resigned</th>
                                                </tr>
                                            </thead>
                                            @if ($promiseattrition)
                                            <tbody>
                                                <tr>
                                                    <td>{{
                                                        ($promiseattrition['total_absconding'] +  $promiseattrition['total_resigned'])? $promiseattrition['total_absconding'] +  $promiseattrition['total_resigned']: 0
                                                        }}</td>
                                                    <td>{{($promiseattrition['total_absconding'])? $promiseattrition['total_absconding'] : 0}}</td>
                                                    <td>
                                                      {{($promiseattrition['total_resigned']) ? $promiseattrition['total_resigned'] : 0}}
                                                    </td>
                                                </tr>
                                            </tbody>
                                            @else
                                            <tr>
                                                <td>
                                                    No Data Found
                                                </td>
                                            </tr>
                                            @endif
                                        </table>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="widget-head">
                                        <div class="tools"></div>
                                        <span class="title">Attrition Report - National(YTD)</span>
                                        <span class="description"></span>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget clickable-row" data-href="{{ url('admin/attrition-report/region') }}">
                                            <thead>
                                                <tr>
                                                    <th>Month</th>
                                                    <th>Absconding</th>
                                                    <th>Resignation</th>
                                                </tr>
                                            </thead>
                                            @if ($attritionReport)
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        Total
                                                    </td>
                                                    <td>{{$attritionReport->absconding_total}}</td>
                                                    <td>{{$attritionReport->resignation_total}}</td>
                                                </tr>
                                            </tbody>
                                            @else
                                            <tr>
                                                <td>
                                                    No Data Found
                                                </td>
                                            </tr>
                                            @endif
                                        </table>
                                    </div>
                                    <div class="widget-head mt-25">
                                        <span class="title">Gender Wise Attrition Report - National</span><span class="description"></span>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead class="primary">
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Male</th>
                                                    <th>Female</th>

                                                </tr>
                                            </thead>
                                            <tbody class="no-border-x">
                                                <tr class="clickable-row" data-href="{{ url('admin/gender-attrition-report/region') }}">
                                                    @if ($gender_attrition)
                                                    <td>Attrition</td>
                                                    <td>{{($gender_attrition['male'] > 0 )? $gender_attrition['male'] : 0 }}</td>
                                                    <td>{{($gender_attrition['female'] > 0 )? $gender_attrition['female'] : 0 }}</td>
                                                    @else
                                                    <td>No Data Found</td>
                                                    @endif
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="widget-head mt-25">
                                    <span class="title">Attrition report - By Aging</span>
                                    <span class="description"></span>
                                </div>
                                <div class="table-responsive">
                                    <table id="man-powerpercentage-report-table" class="table table-striped table-hover table-fw-widget clickable-row" data-href="{{ url('admin/attrition-report/region') }}">
                                        <thead>
                                            <tr>
                                                <th>Total</th>
                                                <th>Infant attrition Within month</th>
                                                <th>Baby attrition Within a quartere</th>
                                                <th>Attrition</th>
                                            </tr>
                                        </thead>
                                        @if ($aging_report)
                                        <tbody>
                                            <tr>
                                                <td>{{$aging_report['total']}}</td>
                                                <td>{{$aging_report['infant_attrition']}}</td>
                                                <td>{{$aging_report['baby_attrition']}}</td>
                                                <td>{{$aging_report['attrition']}}</td>
                                            </tr>
                                        </tbody>
                                        @else
                                        <tr>
                                            <td>
                                                No Data Found
                                            </td>
                                        </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            {{-- New Design For Attrition Reports End --}}

            <div class="col-md-12">
                <div class="widget widget-fullwidth widget-1">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Hygiene Report - National</span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <div class="table-responsive">
                                <table class="table table-striped clickable-row" data-href="{{ url('admin/hygiene-report/region') }}">
                                    <thead class="primary">
                                        <tr>
                                            <th>Id Card Not Available</th>
                                            <th class="number">Uniform not available</th>
                                            <th class="number">L0L1 untrained</th>
                                            <th class="number">L0L1 non certified</th>
                                            <th class="number">Certificate pending</th>
                                            <th class="number">Appointment Letter</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        <tr>
                                            <td class="number">{{($hygiene['id_card'] > 0 )? $hygiene['id_card'] : 0 }}</td>
                                            <td class="number">{{($hygiene['uniform'] > 0 )? $hygiene['uniform'] : 0 }}</td>
                                            <td class="number">{{($hygiene['l0l1_untrained'] > 0 )? $hygiene['l0l1_untrained'] : 0 }}</td>
                                            <td class="number">{{($hygiene['l0l1_certified'] > 0 )? $hygiene['l0l1_certified'] : 0 }}</td>
                                            <td class="number">{{($hygiene['certificate_pending'] > 0 )? $hygiene['certificate_pending'] : 0 }}</td>
                                            <td class="number">{{($hygiene['appoint_letter'] > 0 )? $hygiene['appoint_letter'] : 0 }}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth widget-1">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Man Power Cost Report - National</span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <div class="table-responsive">
                                <table class="table table-striped clickable-row" data-href="{{ url('admin/manpower-cost-report/region') }}">
                                    <thead class="primary">
                                        <tr>
                                            <th>Pan India</th>
                                            <th>No of Employee</th>
                                            <th>Avg Cost</th>

                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        <tr>
                                            <td>Total</td>
                                            <td>{{($averageCost['total_employee'] > 0 )? $averageCost['total_employee'] : 0 }}</td>
                                            <td>{{($averageCost['total_employee'] > 0 )? number_format($averageCost['salary']/$averageCost['total_employee'],2)
                                                : 0 }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- <div class="col-md-12 clearfix">
                <div class="white-bgf clearfix">
                    <div class="widget widget-fullwidth widget-1">
                        <div class="widget-head">
                            <span class="title">Attendance Report</span>
                        </div>
                        <div class="widget-chart-container">
                            <!-- <div id="pie-chart4" style="height: 200px;"></div> -->
                            <div class="chart-table">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="widget widget-fullwidth widget-1">
                                            <div class="widget-head">
                                                <div class="tools">

                                                </div>
                                                <span class="title">Manning Report - National({{\Carbon\Carbon::now()->startOfDay()->subDays(1)->format("d/m/Y")}})</span>

                                            </div>
                                            <div class="widget-chart-container">
                                                <div class="chart-table xs-pt-15">
                                                    <div class="table-responsive">
                                                        <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                                            <thead class="primary">
                                                                <tr>
                                                                    <th>Live Manpower</th>
                                                                    <th>Present Manpower</th>
                                                                    <th>Present Manpower (%)</th>
                                                                </tr>
                                                            </thead>

                                                            <tbody>
                                                                <tr class="clickable-row" data-href="{{url('admin/attendance-report/region')}}">
                                                                    <td>{{$attendanceReport['actual_man_power']}}</td>
                                                                    <td>{{$attendanceReport['present_man_power']}}</td>
                                                                    <td>{{$attendanceReport['present_per']}}</td>
                                                                </tr>
                                                            </tbody>


                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}

			<div class="col-md-12">
				<div class="row">
                    <div class="widget widget-fullwidth widget-1 widget-custom"  style="min-height:490px">

                        <div class="widget-chart-container">

                            <div class="chart-table clearfix">
                                <div class="col-md-6">
                                    {{-- <div class="widget-head mt-25">
                                        <span class="title">Hiring Report - National(YTD)</span>
                                        <span class="description"></span>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                            <thead>
                                                <tr class="clickable-row" data-href="{{ url('admin/hiring-report/region') }}">
                                                    <th>Month</th>
                                                    <th>GAP</th>
                                                    <th>Hired</th>
                                                    <th>Pending</th>
                                                </tr>
                                            </thead>
                                            @if ($hiringReport)
                                            <tbody>
                                                @foreach ($hiringReport as $hiring)
                                                <tr class="clickable-row" data-href="{{ url('admin/hiring-report/region') }}">
                                                    <td>@php
                                                        echo date('M-Y',strtotime('01-'.$hiring->monthyear))
                                                    @endphp</td>
                                                    <td>{{$hiring->gap_g}}</td>
                                                    <td>{{$hiring->hired_h}}</td>
                                                    <td>{{$hiring->pending_p}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            @endif
                                        </table>
                                    </div> --}}

                                    <div class="chart-table mt-25">
                                        <div class="widget-head">
                                            <span class="title">Hiring Report - National(MTD)</span>
                                            <span class="description"></span>
                                        </div>
                                        @php
                                             $hiringreportchart = array('region_name'=> array(),'pending'=>array(),'hired'=>array(),'gap'=>array());
                                             foreach ($hiringReportChart as $key => $value) {
                                                $hiringreportchart['region_name'][$key] = date('M-Y',strtotime('01-'.$value->monthyear));
                                                $hiringreportchart['pending'][$key]=($value->pending_p > 0)?$value->pending_p:0;
                                                $hiringreportchart['hired'][$key]= ($value->hired_h > 0)?$value->hired_h:0;
                                                $hiringreportchart['gap'][$key]= ($value->gap_g > 0)?$value->gap_g:0;
                                            }
                                        @endphp
                                        <canvas id="bar-chart" style="height: 200px;"></canvas>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="widget-head mt-25">
                                        <span class="title">Hiring - source of recruitment</span><span class="description"></span>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead class="primary">
                                                <tr>
                                                    <th>Sorce</th>
                                                    <th>Total Hired</th>
                                                </tr>
                                            </thead>
                                            <tbody class="no-border-x">
                                                @if ($hiringSourceRequirements) @foreach ($hiringSourceRequirements as $hiringSourceRequirement)
                                                <tr class="clickable-row" data-href="{{ url('admin/hiring-source-report') }}">
                                                    <td>{{(isset($hiringSourceRequirement['sourece_name']))? $hiringSourceRequirement['sourece_name'] : 0 }}</td>
                                                    <td>{{($hiringSourceRequirement['total_hired'] > 0 )? $hiringSourceRequirement['total_hired'] : 0 }}</td>
                                                </tr>
                                                @endforeach @else
                                                <tr>
                                                    <td>No Data Found</td>
                                                </tr>
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

			</div>



			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="widget widget-fullwidth widget-1">
						  <div class="widget-head">
							<div class="tools"></div><span class="title">Additional Report - National</span><span class="description"></span>
						  </div>
						  <div class="widget-chart-container">
							<div class="chart-table xs-pt-15">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead class="primary">
                                        <tr>
                                            <th>Title</th>
                                            <th>Male</th>
                                            <th>Female</th>

                                        </tr>
                                        </thead>
                                        <tbody class="no-border-x">
                                        <tr>
                                            <td>Gender</td>
                                            <td>{{($gender['male'] > 0 )? $gender['male'] : 0 }}</td>
                                            <td>{{($gender['female'] > 0 )? $gender['female'] : 0 }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
							</div>
						  </div>
						</div>
					</div>
                </div>

            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget widget-fullwidth widget-1">
                            <div class="widget-head">
                                <div class="tools"></div>
                                <span class="title">Leave Report</span>
                                <span class="description"></span>
                            </div>
                            <div class="widget-chart-container">
                                <div class="chart-table xs-pt-15">
                                    <div class="table-responsive">
                                        <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                            <thead class="primary">
                                                <tr>
                                                    <th>Allocated Leave</th>
                                                    <th>Pending Leave</th>
                                                    <th>Availed Leave</th>
                                                </tr>
                                            </thead>
                                            @if ($leaveReport)
                                            <tbody>
                                                <tr class="clickable-row" data-href="{{url('admin/leave-report/')}}">
                                                    <td>{{$leaveReport->total_allocated_leave}}</td>
                                                    <td>{{$leaveReport->total_pending_leave}}</td>
                                                    <td>{{$leaveReport->availed_leave}}</td>
                                                </tr>
                                            </tbody>
                                            @else
                                            <tr>
                                                <td>
                                                    No Data Found
                                                </td>
                                            </tr>
                                            @endif
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>







                <div class="col-md-12">
                    <div class="white-bgf mt-25 clearfix">
                        <div class="widget widget-fullwidth widget-1">
                            <div class="widget-head">
                                <div class="tools">

                                </div>
                                <span class="title clickable-row" data-href="{{url('admin/productivity-report/')}}">Productivity Report</span>
                            </div>
                            <div class="widget-chart-container">
                                <div class="chart-table">
                                <div class="col-md-6">
                                    <div class="widget-head">

                                        <span class="title">Productivity report by manpower</span>
                                        <span class="description"></span>
                                    </div>
                                    <div class="table-responsive">
                                    <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Sales</th>
                                                <th>No. of employee</th>
                                                <th>Sales per employee</th>
                                            </tr>
                                        </thead>
                                        @if (count($pending_employee_data) > 0)
                                        <tbody>
                                            <tr class="clickable-row" data-href="{{url('admin/productivity-report/')}}">
                                                <td>{{$productivity_report['total_collection']}}</td>
                                                <td>{{$productivity_report['total_employee']}}</td>
                                                <td>{{$productivity_report['per_person_income']}}</td>
                                            </tr>
                                        </tbody>
                                        @else
                                        <tr>
                                            <td>
                                                No Data Found
                                            </td>
                                        </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-6">
                                <div class="widget-head">

                                    <span class="title">Productivity report by manpower cost</span>

                                </div>
                                <div class="table-responsive">
                                <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                    <thead>
                                        <tr>
                                            <th>Sales</th>
                                            <th>Avg cost per emp</th>
                                            <th>Sales per employee</th>
                                        </tr>
                                    </thead>
                                    @if (count($productivity_manpower_cost) > 0)
                                    <tbody>
                                        <tr class="clickable-row" data-href="{{url('admin/productivity-report/')}}">
                                            <td>{{$productivity_manpower_cost['total_collection']}}</td>
                                            <td>{{$productivity_manpower_cost['avg_ctc']}}</td>
                                            <td>{{$productivity_manpower_cost['per_person_income']}}</td>
                                        </tr>
                                    </tbody>
                                    @else
                                    <tr>
                                        <td>
                                            No Data Found
                                        </td>
                                    </tr>
                                    @endif
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

          </div>
        </div>
      </div>

@endsection

@section('scripts')
      <?php if(Session::has('flash_success') &&  Session::get('flash_success') !='' ){
      ?>
      <script>

        $(document).ready(function(){
          $('document').find('#success').trigger('click');
          $('#success').click(function(){
            alert('here');
          })
        });
      </script>
    <?php } ?>
	<script>
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
	var colors = {
	danger: "rgb(234, 67, 53)",
	grey: "rgb(204, 204, 204)",
	primary: "rgb(66, 133, 244)",
	success: "rgb(52, 168, 83)",
	warning: "rgb(251, 188, 5)"
	};
		function widget_chartpie4(){
      var data = [

        { label: "Gap", data: <?php echo $manPower['total_gap'];?>},
		{ label: "Avialable", data: <?php echo $manPower['total_actual_man_power'];?>},

      ];

      var color1 = tinycolor( colors.primary ).brighten( 9 ).toString();
      var color2 = tinycolor( colors.danger ).lighten( 13 ).toString();


      $.plot('#pie-chart4', data, {
        series: {
          pie: {
            show: true,
            innerRadius: 0.35,
            shadow:{
              top: 5,
              left: 15,
              alpha:0.3
            },
            stroke:{
              width:0
            },
            label: {
                show: true,
                formatter: function (label, series) {
                    return '<div style="font-size:12px;text-align:center;padding:2px;color:#333;">' + label + '</div>';
                }
            },
            highlight:{
              opacity: 0.08
            }
          }
        },
        grid: {
          hoverable: true,
          clickable: true
        },

        legend: {
          show: false
        }
      });
    }
	widget_chartpie4()

    var colors = {
        danger:"rgb(234, 67, 53)",
        grey: "rgb(204, 204, 204)",
        primary: "rgb(66, 133, 244)",
        success: "rgb(52, 168, 83)",
        warning: "rgb(251, 188, 5)"
    };
    function barChart(){
         /*Set the chart colors */
        var color1 = tinycolor( colors.success);
        var color2 = tinycolor( colors.warning );
        /*Get the canvas element */
        var color3 = tinycolor( colors.primary );
        var ctx = document.getElementById("bar-chart");
        var data = {
            labels: <?php echo json_encode($hiringreportchart['region_name']);?>,
            datasets: [
                {
                    label: "Hired",
                    borderColor: color1,
                    backgroundColor: color1.setAlpha(.8),
                    data: <?php echo json_encode($hiringreportchart['hired']);?>
                },
                {
                    label: "Gap", borderColor: color2,
                    backgroundColor: color2.setAlpha(.5),
                    data: <?php echo json_encode($hiringreportchart['gap']);?>
                },
                {
                    label: "Pending",
                    borderColor: color3,
                    backgroundColor: color3.setAlpha(.5),
                    data:<?php echo json_encode($hiringreportchart['pending']);?>
                }
            ]
        };
        var bar = new Chart(ctx,
        {
            type: 'bar',
            data: data,
            options: {
                elements: {
                    rectangle: {
                         borderWidth: 2,
                         borderColor:'rgb(0, 255, 0)',
                         borderSkipped: 'bottom'
                    }
                },
            }
        });
    }
    barChart()
	</script>
@endsection

@push('modal')
  @include('admin.modals.backendSucess')
@endpush
