@extends('layouts.backend')
@section('title',"Import Variable Pay Type")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Import Variable Pay Type
                    <span class="panel-subtitle">
                        <div class="abs-right20">
                            <a href="{{ url('/admin/variable-pay-type') }}" title="Back">
                                <button class="btn btn-space btn-warning">Back</button>
                            </a>
                            <a href="{{ url('/uploads/variable_pay_type_sample.xlsx') }}" title="Back" download="variable_pay_type_sample" target="_blank">
                                <button class="btn btn-space btn-primary">Sample file download</button>
                            </a>
                        </div>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/variablePayTypeUpload', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                        <div class="form-group has-error">
                            {!! Form::label('', '', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                @if(Session::has('valLocationCode') && Session::get('valLocationCode') != '')
                                    <p class="help-block">invalid Location code entered in row {{ Session::get('valLocationCode') }}</p>
                                @endif
                            </div>
                        </div>
                        @include('storeManager.employee.import')

                        <div class="form-group col-sm-4">
                            {!! Form::submit('Upload', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection