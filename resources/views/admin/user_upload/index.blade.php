@extends('layouts.backend')
@section('title',"Designation")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">User File Upload List
                        <span class="panel-subtitle">
                            <a href="{{ route('admin_ExportToExcel') }}" title="Create" data-url="" class="btn btn-space btn-primary">Download In Excel </a>

                        </span>
                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="designation-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Employee Code</th>
                                    <th>Employee Name</th>
                                    <th>Email</th>
                                    <th>Download File</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('/admin/user-upload-data') }}";
        var download_url = "{{ url('/uploads/Userfile/') }}";
        var edit_url = "{{ url('admin/usercodeview/') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#designation-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": true},
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": true},
                { data: 'email',name : 'email',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "name" : 'fileuploadPath',
                    "searchable": false,
                    "orderable": true,
                    "render": function (o) {
                        if(o.fileuploadPath != '' && o.fileuploadPath != null ){
                            return "<a target='_blank' href='"+download_url+"/"+o.fileuploadPath+"' value="+o.id+" data-id="+o.id+"><button class='btn btn-success btn-sm' title='Download' ><i class='mdi mdi-download' ></i></button></a>&nbsp;";
                        }
                        return "";
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                       e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='mdi mdi-edit'></i></a>&nbsp;";

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/designation')}} data-msg='Designation' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";
                        return v;
                    }

                }
            ]
        });

</script>
@endsection