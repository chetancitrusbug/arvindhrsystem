@extends('layouts.backend')
@section('title',"Edit Business Unit")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Edit Business Unit # <strong>{{$businessUnit->name}}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/business-unit') }}" title="Back">
                            <button class="btn btn-space btn-primary btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                   {!! Form::model($businessUnit, ['method' => 'PATCH','url' => ['/admin/business-unit', $businessUnit->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        @include ('admin.business_unit.form')

                        <div class="form-group col-sm-4">
                            {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection