@extends('layouts.backend')
@section('title',"View Employee")
@section('css')
    <style type="text/css">
        i.mdi.mdi-case-download{
            font-size: 30px;
        }
        .case{
            /*text-align: right;*/
        }
    </style>
@endsection
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">

        <div class="row">
            <div class="user-info-list panel panel-default">
                <div class="panel-heading panel-heading-divider">Employee # <strong>{{ $employee->name }}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/employee') }}" title="Back">
                            <button class="btn btn-space btn-primary btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="tab-container">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#detail" data-toggle="tab"><strong>Basic Detail</strong></a></li>
                                    <li><a href="#attachment" data-toggle="tab"><strong>Attachments</strong></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="detail" class="tab-pane active cont">
                                        <table class="no-border no-strip skills">
                                            <tbody class="no-border-x no-border-y">
                                                <tr>
                                                    <th class="item">Name</th>
                                                    <td>{{ $employee->name }}</td>
                                                    <th class="item">Joining Date</th>
                                                    <td>{{ date('d/m/Y',strtotime($employee->joining_date)) }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Grade</th>
                                                    <td>{{ $employee->grade->name }}</td>
                                                    <th class="item">Designation</th>
                                                    <td>{{ $employee->designation->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Department</th>
                                                    <td>{{ $employee->subDepartment->department->name }}</td>
                                                    <th class="item">Sub Department</th>
                                                    <td>{{ $employee->subDepartment->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Business Unit</th>
                                                    <td>{{ $employee->businessUnit->name }}</td>
                                                    <th class="item">Reporting Manager Employee Code</th>
                                                    <td>{{ $employee->reporting_manager_employee_code }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Reporting Manager Name</th>
                                                    <td>{{ $employee->reporting_manager_name }}</td>
                                                    <th class="item">Brand</th>
                                                    <td>{{ $employee->brand->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Legal Entity</th>
                                                    <td>{{ $employee->legalEntity->name }}</td>
                                                    <th class="item">Location Code</th>
                                                    <td>{{ $employee->location_code }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">IO Code</th>
                                                    <td>{{ $employee->io_code }}</td>
                                                    <th class="item">Store Name</th>
                                                    <td>{{ $employee->store_name }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Employee Classification</th>
                                                    <td>{{ $employee->employeeClassification->name }}</td>
                                                    <th class="item">Source Category</th>
                                                    <td>{{ $employee->sourceCategory->category }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Source Name</th>
                                                    <td>{{ $employee->sourceCategory->source_name }}</td>
                                                    <th class="item">Source Code</th>
                                                    <td>{{ $employee->sourceCategory->source_code }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Employee refferal amount to be paid</th>
                                                    <td>{{ $employee->employee_refferal_amount }}</td>
                                                    <th class="item">Employee referral payout month</th>
                                                    <td>{{ $employee->employee_refferal_payment_month }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">CTC</th>
                                                    <td>{{ $employee->ctc }}</td>
                                                    <th class="item">Variable Pay Type</th>
                                                    <td>{{ $employee->variablePayType->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Completed soft copy of Joining kit attached</th>
                                                    <td>{{ $employee->soft_copy_attached }}</td>
                                                    <th class="item">POD No</th>
                                                    <td>{{ $employee->pod_no }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Region</th>
                                                    <td>{{ $employee->region->name }}</td>
                                                    <th class="item">State</th>
                                                    <td>{{ $employee->state->name }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">City</th>
                                                    <td>{{ $employee->city->name }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="attachment" class="tab-pane cont">
                                        <table class="no-border no-strip skills">
                                            <tbody class="no-border-x no-border-y">
                                                @php
                                                    $title = [
                                                        'scanned_photo'=>'Scanned Photo',
                                                        'employment_form'=>'Employement form attachment',
                                                        'form_11'=>'Form-11',
                                                        'form_02'=>'Form-02',
                                                        'form_f'=>'Form-F',
                                                        'hiring_checklist'=>'Hiring Checklist',
                                                        'inteview_assessment'=>'Interview Assessment',
                                                        'resume'=>'Resume',
                                                        'education_documents'=>'Education Documents',
                                                        'aadhar_card'=>'Aadhar Card',
                                                        'pan_card'=>'Pan Card',
                                                        'bank_passbook'=>'Bank Passbook/Cheque',
                                                        'offer_accept'=>'offer letter acceptancey mail',
                                                        'previous_company_docs'=>'Previouse company offer letter / payslips / releiving letter',
                                                    ];
                                                @endphp

                                                @foreach($employee->attachment as $value)
                                                    <tr>
                                                        <th class="item">{{ $title[$value->key] }}</th>
                                                        <td>
                                                            <div class="previous_company_docs-attach case"><a href="{{ url('/').'/'.$value->file }}" download="{{ $value->file }}"><i class="mdi mdi-case-download" title="{{ $title[$value->key] }}"></i></a></div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection