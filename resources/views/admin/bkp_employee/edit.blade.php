@extends('layouts.backend')
@section('title',"Edit employee")
@section('css')
<style type="text/css">
    .scanned_photo {
        position: relative;
    }

    .scanned_photo-attach {
        position: absolute;
        left: 65px;
        top: 0px;
    }

    img {
        height: 50px;
        width: 50px;
    }

    .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
            color: #fff !important;
            background-color: #2572f2 !important;
            border-color: transparent;
            border-top-color: #0c57d3 !important;
            box-shadow: inset 0 2px 0 #1266f1 !important;
        }
    input[type="file"]{
        border: none;
    }
    i.mdi.mdi-case-download{
        font-size: 30px;
    }
    .case{
        text-align: right;
    }
    .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
                color: #fff !important;
                background-color: #2572f2 !important;
                border-color: transparent;
                border-top-color: #0c57d3 !important;
                box-shadow: inset 0 2px 0 #1266f1 !important;
            }
</style>

@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Edit Employee # <strong>{{$employee->name}}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/employee') }}" title="Back">
                            <button class="btn btn-space btn-primary btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                   {!! Form::model($employee, ['method' => 'PATCH','url' => ['/admin/employee', $employee->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        @include ('admin.employee.form')

                        <div class="form-group col-sm-4">
                            {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection