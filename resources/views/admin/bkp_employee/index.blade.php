@extends('layouts.backend')
@section('title',"Employee")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Employee List</div>
                    <div class="panel-body">
                        <table style="width:100%;" id="employee-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <!-- <th>Location Code</th>
                                    <th>Store Name</th> -->
                                    <th>DOJ</th>
                                    <th>CTC</th>
                                    <th>Status</th>
                                    <th>Regional Status</th>
                                    <th>HRMS Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/admin/employee-data') }}";
        var edit_url = "{{ url('/admin/employee') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480,
            "scrollX": true,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'name',name : 'name',"searchable": true, "orderable": false},
                /*{ data: 'location_code',name : 'location_code',"searchable": true, "orderable": false},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},*/
                { data: 'joining_date',name : 'joining_date',"searchable": true, "orderable": false},
                { data: 'ctc',name : 'ctc',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
                        if(o.status == 'pending'){
                            return '<label class="label label-danger">Pending<label>';
                        }else if(o.status == 'confirm' || o.status == 'approve'){
                            return '<label class="label label-success">Approve<label>';
                        }
                    }
                },
                {
                    "data": null,
                    "name" : 'regional_status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
                        if(o.regional_status == 1 || o.regional_status == 0){
                            return '<label class="label label-danger">Pending<label>';
                        }else if(o.regional_status == 2){
                            return '<label class="label label-success">Approve<label>';
                        }else if(o.regional_status == 3){
                            return '<label class="label label-warning">send back<label>';
                        }
                    }
                },
                {
                    "data": null,
                    "name" : 'hrms_status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
                        if(o.hrms_status == 1 || o.hrms_status == 0){
                            return '<label class="label label-danger">Pending<label>';
                        }else if(o.hrms_status == 2){
                            return '<label class="label label-success">Approve<label>';
                        }else if(o.hrms_status == 3){
                            return '<label class="label label-warning">send back<label>';
                        }
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";

                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        /*e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='employee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";*/

                        /*d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/employee')}} data-msg='employee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";*/

                        l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-success btn-sm' title='View' ><i class='mdi mdi-menu' aria-hidden='true'></i></button></a>&nbsp;";
                        return v+e+l;
                    }

                }
            ]
        });
</script>


@endsection