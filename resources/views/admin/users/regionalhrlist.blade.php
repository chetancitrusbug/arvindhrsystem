@extends('layouts.backend')
@section('title',"Regional HR List")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Regional HR List
                        <span class="panel-subtitle">
                            <a href="{{ url('/admin/user/regionalhr/create') }}" title="Create">
                                <button class="btn btn-space btn-success btn-top-right">Create</button>
                            </a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive table-minw-fixed">
                            <table style="width:100%;" id="users-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Employee Code</th>
                                        <th>Employee Name</th>
                                        <th>Email</th>
                                        <th>Region</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('/admin/user/userdatatable/rm') }}";
        var view_url = "{{ url('/admin/users') }}";
        var edit_url = "{{ url('/admin/user/regionalhr/edit') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#users-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'emp_id',name : 'emp_id',"searchable": true, "orderable": false},
                { data: 'emp_name',name : 'emp_name',"searchable": true, "orderable": false},
                { data: 'email',name : 'email',"searchable": true, "orderable": false},
                { data: 'region',name : 'region',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": true,
                    "render": function (o) {
                        if(o.status == 1){
                            return "<input type='checkbox' class='status status-change' checked data-table='users' data-status="+o.status+" onchange=statusChange() data-url={{url('admin/change-status')}} value="+o.id+" data-id="+o.id+">&nbsp;";
                        }
                        return "<input type='checkbox' class='status status-change' data-url={{url('admin/change-status')}} data-table='users' data-status="+o.status+" value="+o.id+" data-id="+o.id+">&nbsp;";
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+view_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        e = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='mdi mdi-edit'></i></a>&nbsp;";

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/users')}} data-msg='store location' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";
                        return v+e;
                    }
                }
            ]
        });

</script>
@endsection