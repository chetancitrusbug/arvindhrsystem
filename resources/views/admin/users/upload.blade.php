@extends('layouts.backend')
@section('title',"Import Designation")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Import Designation
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/designation') }}" title="Back">
                            <button class="btn btn-space btn-warning btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => 'admin/users/import', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        @csrf   

                        <div class="form-group has-error">
                            {!! Form::label('', '', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                @if(Session::has('valStore') && Session::get('valStore') != '')
                                    <p class="help-block">invalid store name in row {{ Session::get('valStore') }}</p>
                                @endif

                                @if(Session::has('valEmployee') && Session::get('valEmployee') != '')
                                    <p class="help-block">employee code not exist of row {{ Session::get('valEmployee') }}</p>
                                @endif
                            </div>
                        </div>

                        @include('storeManager.employee.import')

                        <div class="form-group col-sm-4">
                            {!! Form::submit('Upload', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection