@extends('layouts.backend')
@section('title',"Edit User")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Edit User # <strong>{{$user->emp_name}}</strong>
                    <span class="panel-subtitle">
                        @php
                            $redirect = '';
                            if ($user_type == 'SM'){
                                $redirect = 'admin/user/storemanager';
                            } else if ($user_type == 'HRMS'){
                                $redirect = 'admin/user/hrms';
                            } elseif ($user_type == 'RM') {
                                $redirect = 'admin/user/regionalhr';
                            }
                        @endphp
                        <a href="{{ url($redirect) }}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::model($user, ['method' => 'PATCH','class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true])
                    !!}
                            @include ('admin.users.userform')

                    <div class="form-group col-sm-9">
                        {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
