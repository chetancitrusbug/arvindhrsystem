@extends('layouts.backend')
@section('title',"View User")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="user-info-list panel panel-default">
                <div class="panel-heading panel-heading-divider">User # <strong>{{ $user->emp_name }}</strong>
                    <span class="panel-subtitle">
                        <a href="{!! URL::previous() !!}" title="Back">
                            <button class="btn btn-space btn-warning btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    <table class="no-border no-strip skills">
                        <tbody class="no-border-x no-border-y">
                            <tr>
                                <td class="item">Employee Name</td>
                                <td>{{ $user->emp_name }}</td>
                            </tr>
                            <tr>
                                <td class="item">Employee ID</td>
                                <td>{{ $user->emp_id }}</td>
                            </tr>
                            <tr>
                                <td class="item">Email</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td class="item">Mobile No</td>
                                <td>{{ $user->mobile_no }}</td>
                            </tr>
                            <tr>
                                <td class="item">Brand</td>
                                <td>{{ $user->brand->name or '' }}</td>
                            </tr>
                            <tr>
                                <td class="item">Legal Entity</td>
                                <td>{{ $user->legalEntity->name or '' }}</td>
                            </tr>
                            <tr>
                                <td class="item">Region</td>
                                <td>{{ $user->regionName->name or '' }}</td>
                            </tr>
                            <tr>
                                <td class="item">state</td>
                                <td>{{ $user->stateName->name or '' }}</td>
                            </tr>
                            <tr>
                                <td class="item">city</td>
                                <td>{{ $user->cityName->name or '' }}</td>
                            </tr>
                            <tr>
                                <td class="item">Store Name</td>
                                <td>{{ $user->storeLocation->store_name or '' }}</td>
                            </tr>
                            <tr>
                                <td class="item">Internal Order</td>
                                <td>{{ $user->internal_order or '' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
