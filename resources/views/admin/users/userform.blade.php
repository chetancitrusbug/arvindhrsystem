<div class="form-group{{ $errors->has('emp_name') ? ' has-error' : ''}}">
    {!! Form::label('emp_name', '* Employee Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('emp_name', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('emp_name', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('email', null, ['class' => 'form-control input-sm','disabled'=>($is_update)?'disabled':false]) !!} {!! $errors->first('email', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('password', ' Password: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::password('password', ['class' => 'form-control input-sm']) !!} {!! $errors->first('password', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('confirm_password') ? ' has-error' : ''}}">
    {!! Form::label('confirm_password', ' Confirm Password: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::password('confirm_password', ['class' => 'form-control input-sm']) !!} {!! $errors->first('confirm_password', '
        <p
            class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('mobile_no') ? ' has-error' : ''}}">
    {!! Form::label('mobile_no', '* Mobile No: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('mobile_no', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('mobile_no', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('emp_id') ? ' has-error' : ''}}">
    {!! Form::label('emp_id', '* Employee ID: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('emp_id', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('emp_id', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('role') ? ' has-error' : ''}}" style="display:none">
    {!! Form::label('role', 'User Role: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('role', $role, old('role',(isset($user->roles[0]->name)?$user->roles[0]->name:''))
        ,['class' => 'form-control input-sm roleChange', 'readonly'=>true]) !!} {!! $errors->first('role', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

@if ($user_type == 'SM')


<div id="userRoleChange">
    <div class="form-group{{ $errors->has('store_id') ? ' has-error' : ''}}">
        {!! Form::label('store_id', '* Select Store: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::select('store_id', [''=>'-- Select Store --']+$storeLocation, null ,['class' => 'form-control input-sm']) !!} {!!
            $errors->first('store_id', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group{{ $errors->has('state_id') ? ' has-error' : ''}}">
        {!! Form::label('state_id', '* Select State: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::select('state_id', [''=>'-- Select State --']+$states, isset($user->state)?$user->state:null ,['class' => 'form-control input-sm','disabled' => ($user_type == 'SM')?'disabled':'']) !!} {!! $errors->first('state_id', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('city_id') ? ' has-error' : ''}}">
        {!! Form::label('city_id', '* Select City: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::select('city_id', [''=>'-- Select city --']+$city, isset($user->city)?$user->city:null ,['class' => 'form-control input-sm','disabled' => ($user_type == 'SM')?'disabled':'']) !!} {!! $errors->first('city_id', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('pincode') ? ' has-error' : ''}}">
        {!! Form::label('pincode', '* PIN Code: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('pincode', null, ['class' => 'form-control input-sm','disabled' => ($user_type == 'SM')?'disabled':'']) !!} {!! $errors->first('pincode', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
    {{-- <div class="form-group{{ $errors->has('internal_order') ? ' has-error' : ''}}">
        {!! Form::label('internal_order', '* Internal Order: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('internal_order', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('internal_order', '
            <p
                class="help-block">:message</p>') !!}
        </div>
    </div> --}}
    <div class="form-group{{ $errors->has('brand_id') ? ' has-error' : ''}}">
        {!! Form::label('brand_id', '* Select Brand: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::select('brand_id', [''=>'-- Select brand id --']+$brands, null ,['class' => 'form-control input-sm','disabled' => ($user_type == 'SM')?'disabled':'']) !!} {!! $errors->first('brand_id',
            '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('legal_entity_id') ? ' has-error' : ''}}">
        {!! Form::label('legal_entity_id', '* Select Legal Entity: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::select('legal_entity_id', [''=>'-- Select legal entity --']+$legalEntity, null ,['class' => 'form-control input-sm','disabled' => ($user_type == 'SM')?'disabled':''])
            !!} {!! $errors->first('legal_entity_id', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
@endif
@if ($user_type == 'SM' || $user_type == 'RM')

<div class="form-group{{ $errors->has('region_id') ? ' has-error' : ''}}">
    {!! Form::label('region_id', '* Select Region: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('region_id', [''=>'-- Select Region --']+$region, isset($user->region)?$user->region:null ,['class' => 'form-control input-sm','disabled' => ($user_type == 'SM')?'disabled':'']) !!}
        {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>


@endif
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::checkbox('status',1, isset($state->status)?$state->status:1, ['class'=>['status']]) !!} {!! $errors->first('status','<p class="help-block">:message</p>') !!}
    </div>
</div>


@section('scripts')
<script>
    var user_type = ('{{$user_type}}');
    $("#region_id").attr('disabled','disabled');
    $("#state_id").attr('disabled','disabled');
    $("#city_id").attr('disabled','disabled');
    $("#pincode").attr('disabled','disabled');
    $("#brand_id").attr('disabled','disabled');
    $("#legal_entity_id").attr('disabled','disabled');
    if (user_type != "SM") {
        $("#region_id").removeAttr('disabled');
    }

        $("#module_form").submit(function(e){
            password = $.trim($("input[name='password']").val());
            if(password != ""){
                confirm_password = $.trim($("input[name='confirm_password']").val());
                if(password != confirm_password){
                    e.preventDefault();
                    alert('Password and Confirm password not match');
                    $("input[name='password']").focus();
                } else {
                    $("#region_id").removeAttr('disabled');
                    $("#state_id").removeAttr('disabled');
                    $("#city_id").removeAttr('disabled');
                    $("#pincode").removeAttr('disabled');
                    $("#brand_id").removeAttr('disabled');
                    $("#legal_entity_id").removeAttr('disabled');
                }
            }
                    $("#region_id").removeAttr('disabled');
                    $("#state_id").removeAttr('disabled');
                    $("#city_id").removeAttr('disabled');
                    $("#pincode").removeAttr('disabled');
                    $("#brand_id").removeAttr('disabled');
                    $("#legal_entity_id").removeAttr('disabled');
            return true;
    });
    $('#store_id').change(function() {
        var url = "{{ url('/admin/store_details') }}"+"/"+$('#store_id').val();

            $.ajax({
                url: url,
                data: {},
            })
            .done(function(data) {
                if(data.status){
                    $("#city_id").val("");
                    city_id = data.detail.city_id;
                    legal_entity_id = data.detail.legal_entity_id;
                    $("#region_id").val(data.detail.region_id);
                    $("#state_id").val(data.detail.state_id);
                    $.ajax({
                        url: "{{ url('/admin/city-list') }}",
                        data: {state_id: data.detail.state_id},
                    })
                    .done(function(citydata) {
                        $('select[name="city_id"] option:not(:first)').remove();
                        if(citydata != ''){
                            $.each(citydata, function(index, val) {
                                var selected = '';
                                if(index == data.detail.city_id){
                                    selected = "selected = selected";
                                }
                                $('select[name="city_id"]').append("<option value='"+index+"' "+selected+" >"+val+"</option>");
                            });
                        }
                    });
                    $("#city_id").val(data.detail.city_id);
                    $("#pincode").val(data.detail.pincode);
                    $("#brand_id").val(data.detail.brand_id).trigger('change');
                    $("#legal_entity_id").val(data.detail.legal_entity_id);
                }
            });
        });

</script>
@endsection