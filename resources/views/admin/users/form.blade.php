<div class="form-group{{ $errors->has('emp_name') ? ' has-error' : ''}}">
    {!! Form::label('emp_name', '* Employee Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('emp_name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('emp_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', '* Email: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('email', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('password') ? ' has-error' : ''}}">
	{!! Form::label('password', '* Password: ', ['class' => 'col-sm-3 control-label']) !!}
	<div class="col-sm-6">
		{!! Form::password('password', ['class' => 'form-control input-sm']) !!}
		{!! $errors->first('password', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group {{ $errors->has('confirm_password') ? ' has-error' : ''}}">
	{!! Form::label('confirm_password', '* Confirm Password: ', ['class' => 'col-sm-3 control-label']) !!}
	<div class="col-sm-6">
		{!! Form::password('confirm_password', ['class' => 'form-control input-sm']) !!}
		{!! $errors->first('confirm_password', '<p class="help-block">:message</p>') !!}
	</div>
</div>

<div class="form-group{{ $errors->has('mobile_no') ? ' has-error' : ''}}">
    {!! Form::label('mobile_no', '* Mobile No: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('mobile_no', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('mobile_no', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('emp_id') ? ' has-error' : ''}}">
    {!! Form::label('emp_id', '* Employee ID: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('emp_id', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('emp_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('role') ? ' has-error' : ''}}">
    {!! Form::label('role', '* Select Role: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('role', [''=>'-- Select role --']+$role, old('role',(isset($user->roles[0]->name)?$user->roles[0]->name:'')) ,['class' => 'form-control input-sm roleChange']) !!}
        {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div id="userRoleChange" style="display:none">
<div class="form-group{{ $errors->has('store_id') ? ' has-error' : ''}}">
    {!! Form::label('store_id', '* Select Store: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('store_id', [''=>'-- Select Store --']+$storeLocation, null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('store_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('state_id') ? ' has-error' : ''}}">
    {!! Form::label('state_id', '* Select State: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('state_id', [''=>'-- Select State --']+$states, isset($user->state)?$user->state:null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('state_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('city_id') ? ' has-error' : ''}}">
    {!! Form::label('city_id', '* Select City: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('city_id', [''=>'-- Select city --']+$city, isset($user->city)?$user->city:null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('pincode') ? ' has-error' : ''}}">
    {!! Form::label('pincode', '* PIN Code: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('pincode', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('pincode', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('internal_order') ? ' has-error' : ''}}">
    {!! Form::label('internal_order', '* Internal Order: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('internal_order', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('internal_order', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('brand_id') ? ' has-error' : ''}}">
    {!! Form::label('brand_id', '* Select Brand: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('brand_id', [''=>'-- Select brand id --']+$brands, null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('brand_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('legal_entity_id') ? ' has-error' : ''}}">
    {!! Form::label('legal_entity_id', '* Select Legal Entity: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('legal_entity_id', [''=>'-- Select legal entity --']+$legalEntity, null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('legal_entity_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
</div>
<div class="form-group{{ $errors->has('region_id') ? ' has-error' : ''}}">
    {!! Form::label('region_id', '* Select Region: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('region_id', [''=>'-- Select Region --']+$region, isset($user->region)?$user->region:null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::checkbox('status',1, isset($state->status)?$state->status:1, ['class'=>['status']]) !!}
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@section('scripts')
    <script>
        $('select[name="state_id"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/admin/city-list') }}",
                data: {state_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="city_id"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="city_id"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });
		$('.roleChange').on('change',function(){
			if($(this).val() == 'SM'){
				$("#userRoleChange").show();
			}else{
				$("#userRoleChange").hide();
			}
			
		});
    </script>
@endsection