<div class="form-group{{ $errors->has('parent_id') ? ' has-error' : ''}}">
    {!! Form::label('parent_id', 'Parent Permission: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('parent_id',$permissions, null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
        {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
	    {!! Form::text('name', null, ['class' => 'form-control input-sm']) !!}
	    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
	</div>
</div>
<div class="form-group{{ $errors->has('label') ? ' has-error' : ''}}">
    {!! Form::label('label', 'Label: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
	    {!! Form::text('label', null, ['class' => 'form-control input-sm']) !!}
	    {!! $errors->first('label', '<p class="help-block">:message</p>') !!}
	</div>
</div>
