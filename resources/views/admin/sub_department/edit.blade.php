@extends('layouts.backend')
@section('title',"Edit Sub Department")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Edit Sub Department # <strong>{{$subDepartment->name}}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/sub-department') }}" title="Back">
                            <button class="btn btn-space btn-warning btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                   {!! Form::model($subDepartment, ['method' => 'PATCH','url' => ['/admin/sub-department', $subDepartment->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        @include ('admin.sub_department.form')

                        <div class="form-group col-sm-9">
                            {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection