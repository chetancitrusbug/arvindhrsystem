@extends('layouts.backend')
@section('title',"Edit Employee Classification")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Edit Employee Classification # <strong>{{$employeeClassification->name}}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/employee-classification') }}" title="Back">
                            <button class="btn btn-space btn-warning btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                   {!! Form::model($employeeClassification, ['method' => 'PATCH','url' => ['/admin/employee-classification', $employeeClassification->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        @include ('admin.employee_classification.form')

                        <div class="form-group col-sm-9">
                            {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection