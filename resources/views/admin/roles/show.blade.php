@extends('layouts.backend')
@section('title',"Show Role")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="user-info-list panel panel-default">
                <div class="panel-heading panel-heading-divider">Role # <strong>{{ $role->name }}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/roles') }}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>
                        <a href="{{ url('/admin/roles/' . $role->id . '/edit') }}" title="Edit Permission">
                            <button class="btn btn-primary btn-sm">Edit</button>
                        </a>
                        {!! Form::open([
                            'method' => 'DELETE',
                            'url' => ['/admin/roles', $role->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Permission',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                    </span>
                </div>
                <div class="panel-body">
                    <table class="no-border no-strip skills">
                        <tbody class="no-border-x no-border-y">
                            <tr>
                                <th class="item">Id</th>
                                <th class="item">name</th>
                                <th class="item">Label</th>
                            </tr>
                            <tr>
                                <td>{{ $role->id }}</td>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->label }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
