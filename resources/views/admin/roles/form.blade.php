<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Name: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if(isset($role) && $role->name != '')
        {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required','readonly'=>true]) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        @else
        {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('label') ? ' has-error' : ''}}">
    {!! Form::label('label', 'Label: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('label', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('label', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('permissions') ? ' has-error' : ''}}">
    {!! Form::label('label', '* Permissions: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <ul>
            @foreach($permissions as $permission)
                <li>
                    <label class=""><input type="checkbox" name="permissions[]" class="parent"
                                           data-parent="{!! $permission->id !!}"
                                           value="{{ $permission->name }}" {!! isset($isChecked)?$isChecked($permission->name):"" !!} ><span
                                class="text-danger">{{ $permission->label }}</span>
                        [ {{$permission->name}} ]
                    </label>
                    
                    <ul class="child">
                        @foreach($permission->child as $perm)
                            <li>
                                <label>
                                    <input type="checkbox" name="permissions[]"
                                              class="child-{!! $perm->parent_id !!}"
                                              {!! isset($isChecked)?$isChecked($perm->name):"" !!}
                                              value="{{ $perm->name }}"><span
                                            class="text-info ">{{ $perm->label }}</span> [ {{$perm->name}} ]
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </li>

            @endforeach
        </ul>
        {!! $errors->first('permissions', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@section('scripts')
    <script>

        $(document).ready(function () {

            $('.parent').change(function (e) {

                var $this = $(this),
                    parent = $this.data('parent'),
                    child = $('.child-' + parent);

                if ($this.is(':checked')) {
                    child.prop('checked', true);
                } else {
                    child.prop('checked', false);
                }
                e.preventDefault();
            });
        });
    </script>
@endsection
