@extends('layouts.backend')

@section('content')
<style>.clickable-row{cursor:pointer}</style>
	<div class="be-content">
	    <div class="main-content container-fluid">
          <div class="row">

			<div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading panel-heading-divider">
                  <div class="tools"></div><span class="title">Manpower Cost Report - Region</span>
										<span class="panel-subtitle">
											<div class="abs-right20">
												<a href="{{url('admin/manpower-cost-report/region-export')}}" class="" title="Download">
													<button class="btn btn-space btn-primary">Export Data</button>
												</a>
												<a href="{{url('admin')}}" class="pull-right" title="Go Back">
													<button class="btn btn-space btn-warning">Go Back</button>
												</a>
											</div>
										</span>
                  <span class="panel-subtitle"></span>
                </div>
                <div class="panel-body">
						<div class="col-md-12">
							<div class="chart-table xs-pt-15">
								<div class="table-responsive table-minw-fixed">
									<table style="width:100%;" id="manpowercost" class="table table-striped">
									<thead class="primary">
										<tr>
										<th>Region</th>
										<th>Employee</th>
										<th>Avg Cost</th>

										</tr>
									</thead>
									<tbody class="no-border-x">
										@foreach($averageCost as $key => $value)
											<tr class="clickable-row"  data-href="{{ url('admin/manpowercost-report/region-wise-store/') }}/{{$value->region_id}}">
												<td>{{$value->name}}</td>
												<td>{{($value->total_employee > 0) ? $value->total_employee : 0 }}</td>
												<td>{{($value->total_employee > 0) ? number_format($value->salary/$value->total_employee,2) : 0 }}</td>
											</tr>
										@endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total</th>
                                            <th>Employee</th>
                                            <th>Avg Cost</th>
                                        </tr>
                                    </tfoot>
									</table>
								</div>

							</div>
						</div>

                </div>
              </div>
            </div>
          </div>
        </div>
	</div>
@endsection

@section('scripts')
<script>
	$(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
    $("#manpowercost").DataTable({
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
		 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
         "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_employee = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_avg = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            avg = 0;
            if(total_avg > 0 && total_employee > 0){
                avg = total_avg/total_employee;
            }

            $( api.column( 1 ).footer() ).html( total_employee );
            $( api.column( 2 ).footer() ).html( "-" );  /* avg.toFixed(2) */

        }
    });


</script>
@endsection
