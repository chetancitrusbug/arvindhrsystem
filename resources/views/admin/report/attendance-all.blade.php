@extends('layouts.backend')
@section('title',"Attendance Report")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Attendace Report
                        <span class="panel-subtitle">
                            <a href="{{url('admin')}}" class="pull-right" title="Create">
                                <button class="btn btn-space btn-warning btn-back">Go Back</button>
                            </a>
                        </span>
                    </div>

					 <div class="blk-div clearfix">
                            {!! Form::open(['url' => url('admin/attendance-report'), 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true,'method' => 'get']) !!}
							{!! Form::hidden("start_date", null ,['id'=>'input_start_date']) !!}
							{!! Form::hidden("end_date", null ,['id'=>'input_end_date']) !!}

                                <div class="col-sm-4">
                                    <div style="width: 100%">
                                        <div id="reportrange" class="form-control input-sm"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                            <span></span> <b class="caret"></b>
                                        </div>
                                    </div>
                                </div>

								<div class="col-sm-3 pull-left">
                                    <div class="form-group">
                                        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                                    </div>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    <div class="panel-body clearfix">

						<div class="widget-chart-container">
                            
                            <!-- <div id="pie-chart4" style="height: 200px;"></div> -->
                            <div class="chart-table xs-pt-15">
                                <div class="col-md-6">
                                    <div class="widget-head">
                                        <span class="title">Attendace Present Report</span>
                                    </div>
                                    <div class="table-responsive table-minw-fixed">
                                        <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                            <thead>
                                                <tr>
                                                    <th>Live Manpower</th>
                                                    <th>Present Manpower</th>
                                                    <th>Present Manpower (%)</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td>{{$regiondata['actual_man_power']}}</td>
                                                    <td>{{$regiondata['man_power_p']}}</td>
                                                    <td>{{$regiondata['man_power_p_per']}}</td>

                                                </tr>
                                            </tbody>


                                        </table>
                                    </div>    
                                </div>
                                <div class="col-md-6">
                                    <div class="widget-head">
                                        <span class="title">Leave Report</span>
                                    </div>
                                    <div class="table-responsive table-minw-fixed">
                                        <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                            <thead>
                                                <tr>
                                                    <th>Live Manpower</th>
                                                    <th>Leave Manpower</th>
                                                    <th>Leave Manpower (%)</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td>{{$regiondata['actual_man_power']}}</td>
                                                    <td>{{$regiondata['man_power_l']}}</td>
                                                    <td>{{$regiondata['man_power_l_per']}}</td>

                                                </tr>
                                            </tbody>


                                        </table>
                                    </div>    
                                </div>
                            </div>

							<div class="chart-table xs-pt-15">
                                <div class="col-md-6">
                                    <div class="widget-head">
                                        <span class="title">W/O Report</span>
                                    </div>
                                    <div class="table-responsive table-minw-fixed">
                                        <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                            <thead>
                                                <tr>
                                                    <th>Live Manpower</th>
                                                    <th>W/O Manpower</th>
                                                    <th>W/O Manpower (%)</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td>{{$regiondata['actual_man_power']}}</td>
                                                    <td>{{$regiondata['man_power_wo']}}</td>
                                                    <td>{{$regiondata['man_power_wo_per']}}</td>

                                                </tr>
                                            </tbody>


                                        </table>
                                    </div>    
                                </div>
                                <div class="col-md-6">
                                    <div class="widget-head">
                                        <span class="title">C/O Report</span>
                                    </div>
                                    <div class="table-responsive table-minw-fixed">
                                        <table id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                            <thead>
                                                <tr>
                                                    <th>Live Manpower</th>
                                                    <th>C/O Manpower</th>
                                                    <th>C/O Manpower (%)</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td>{{$regiondata['actual_man_power']}}</td>
                                                    <td>{{$regiondata['man_power_co']}}</td>
                                                    <td>{{$regiondata['man_power_co_per']}}</td>

                                                </tr>
                                            </tbody>


                                        </table>
                                    </div>    
                                </div>
                            </div>

                        </div>
						

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
	var range_start ="";
    var range_end ="";

        $(".datetimepicker-atendance").datetimepicker({
            autoclose: true,
			maxDate: new Date(),
            format: 'dd/mm/yyyy',
            componentIcon: '.mdi.mdi-calendar',
        /*    pickerPosition: "bottom-left", */
            navIcons:{
                rightIcon: 'mdi mdi-chevron-right',
                leftIcon: 'mdi mdi-chevron-left'
            }
        });

		/*************************daterange selection*****************/



    var start = moment().subtract(1, 'days');//moment.utc('2015-01-01','YYYY-MM-DD');
    var end = moment().subtract(1, 'days');
    @if(\Request::has('star_date') && \Request::has('end_date'))
     start = moment.utc("{{\Request::get('star_date')}}",'YYYY-MM-DD');
     end = moment.utc("{{\Request::get('end_date')}}",'YYYY-MM-DD');
    @endif

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');


        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');


        }

		$("#input_start_date").val(range_start);
		$("#input_end_date").val(range_end);


    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "@lang('comman.daterange.all')":[moment.utc('2015-01-01','YYYY-MM-DD'),moment().subtract(1, 'days')],
            "@lang('comman.daterange.yesterday')": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            "Week till date": [moment().startOf('week'), moment().subtract(1, 'days')],
            "Month till date": [moment().startOf('month'), moment().subtract(1, 'days')],
			"Year till date": [moment().startOf('year'), moment().subtract(1, 'days')],
            "@lang('comman.daterange.lastmonth')": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            "@lang('comman.daterange.lastyear')": [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, cb);

     cb(start, end);
    </script>
@endsection
