@extends('layouts.backend')
@section('title',"Man power Region ($region->name)")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Man Power Cost For Region (<small>{{$region->name}}</small>)
                        <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary ">Go Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="man-power-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Store Name</th>
                                    <th>Store Code</th>
                                    <th>Region</th>
									<th>Employee</th>
									<th>Avg Cost</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>Total</th>
                                    <th>Employee</th>
                                    <th>Avg Cost</th>
                                </tr>
                            </tfoot>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('admin/manpowercost-report/region-wise-store-data').'/'.$id }}";

        datatable = $('#man-power-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
			searchable:false,
            "caseInsensitive": false,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'store_name',name : 'store_name',"searchable": false, "orderable": false, "width":200},
                { data: 'store_code',name : 'store_code',"searchable": false, "orderable": false, "width":150},
                { data: 'region_name',name : 'region_name',"searchable": false, "orderable": false, "width":150},
                { data: 'total_employee',name : 'total_employee',"searchable": false, "orderable": false, "width":150},
                {
                    data : null ,
                    render :function(o){
                        return parseFloat(o.avg_cost).toFixed(2);
                    },
					"searchable": false, "orderable": false, "width":150
                },


            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total_employee = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                total_avg = api
                    .column( 4 )
                    .data() 12478
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                avg = 0;
                if(total_avg > 0 && total_employee > 0){
                    avg = total_avg/total_employee;
                }

                $( api.column( 3 ).footer() ).html( total_employee );
                $( api.column( 4 ).footer() ).html( "-" );  /* avg.toFixed(2) */

            }
        });
</script>
@endsection