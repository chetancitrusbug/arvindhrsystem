@extends('layouts.backend')
@section('title',"Leave Reports Employee")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix"> Leave Reports - Employee
                        <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning">Go Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="leave-report" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Employee Code</th>
                                                <th>Employee Name</th>
                                                <th>Allocated leaves</th>
                                                <th>Pending Leaves</th>
                                                <th>Availed leaves</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Allocated leaves</th>
                                                <th>Pending Leaves</th>
                                                <th>Availed leaves</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('admin/leave-report-employee-data') }}/{{request()->route('store_id')}}";
    var edit_url = "{{ url('admin/leave-report-employee') }}"
        datatable = $('#leave-report').DataTable({
            dom: 'Blfrtip',
            buttons: ['excel'],
            processing: true,
            serverSide: true,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
			searchable:false,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": true, "width":200},
                { data: 'employee_name',name : 'employee_name',"searchable": true, "orderable": true, "width":200},
                { data: 'allocated_leave',name : 'allocated_leave',"searchable": false, "orderable": false, "width":150},
                { data: 'pending_leave',name : 'pending_leave',"searchable": false, "orderable": false, "width":150},
                { data: 'availed_leave',name : 'availed_leave',"searchable": false, "orderable": false, "width":150},
            /*    {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d="";
                        v = "<a href='"+edit_url+"/"+o.store_id+"' value="+o.store_id+" data-id="+o.store_id+"><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";
                        return v;
                    }
                } */
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                al = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                pl = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                all = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                $( api.column( 2 ).footer() ).html( al );
                $( api.column( 3 ).footer() ).html( pl );
                $( api.column( 4 ).footer() ).html( all );

            }
        });

</script>
@endsection