<?php /*@extends('layouts.backend')

@section('content')
<style>.clickable-row{cursor:pointer}</style>
	<div class="be-content">
	    <div class="main-content container-fluid">
          <div class="row">
            <div class="col-md-12">
				<div class="widget widget-fullwidth">
				  <div class="widget-head">
					<div class="tools"></div><span class="title">Manpower Report - Region(<small>{{$manPowerGet[0]->regionName}}</small>)</span><span class="description"></span>
				  </div>
				  <div class="widget-chart-container">
				  {{-- <canvas id="bar-chart"></canvas> --}}
					<div class="chart-table xs-pt-15">
					  <table class="table table-striped">
						<thead class="primary">
						  <tr>
							<th>Store</th>
							<th>Io Code</th>
							<th>Region</th>
							<th>Budgeted</th>
							<th class="number">Available</th>
							<th class="number">GAP</th>
							<th class="number">Avg GAP Aging</th>
							<th class="number">% GAP</th>
						  </tr>
						</thead>
						<tbody class="no-border-x">
						@php
						$manPower = array();
							foreach($manPowerGet as $key => $value){
						@endphp
						  <tr class="clickable-row"  >
							<td>{{$value->store_name}}</td>
							<td>{{$value->io_code}}</td>
							<td>{{$value->regionName}}</td>
							<td>{{($value->total_budget_man_power > 0)? $value->total_budget_man_power : 0 }}</td>
							<td class="number">{{($value->total_actual_man_power > 0)? $value->total_actual_man_power : 0 }}</td>
							<td class="number">{{($value->total_budget_man_power > 0 && $value->total_actual_man_power > 0)? $value->total_budget_man_power - $value->total_actual_man_power : 0 }}</td>
							<td class="number">0</td>
							<td class="number">{{($value->total_budget_man_power > 0 && $value->total_actual_man_power > 0)? ($value->total_budget_man_power - $value->total_actual_man_power)/$value->total_budget_man_power : 0 }}</td>
						  </tr>
						@php
							}
						@endphp
						</tbody>
					  </table>
					</div>
				  </div>
				</div>
            </div>
          </div>
        </div>
	</div>
@endsection

@section('scripts')
<script>




</script>
@endsection --}} */ ?>

@extends('layouts.backend')
@section('title',"Man power Region ($region->name)")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Man Power List For Region (<small>{{$region->name}}</small>)
						{{--<a href="{{url('admin/manpower-report/region-wise-store-export')}}/{{$id}}" class="" title="Download">
                            <button class="btn btn-space btn-primary">Export Data</button>
                        </a> --}}
                        <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning ">Go Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="man-power-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Store Name</th>
                                                <th>Store Code</th>
                                                <th>Location</th>
                                                <th>Region</th>
                                                <th>Budget Man Power</th>
                                                <th>Actual Man Power</th>
                                                <th>Gap</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th colspan="4">Total</th>

                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/admin/man-power-data').'/'.$id }}";
        var edit_url = "{{ url('/admin/man-power') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#man-power-table').DataTable({
            dom: 'Blfrtip',
            buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "scrollY": 480, "scrollX": true,

            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
			columns: [
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false, "width":200},
                { data: 'store_code',name : 'store_code',"searchable": true, "orderable": false, "width":150},
                { data: 'city',name : 'city',"searchable": true, "orderable": false, "width":150},
                { data: 'name',name : 'name',"searchable": false, "orderable": false, "width":150},
                { data: 'budget_man_power',name : 'budget_man_power',"searchable": true, "orderable": false, "width":100},
                { data: 'actual_man_power',name : 'actual_man_power',"searchable": true, "orderable": false, "width":100},
                { data: 'gap',name : 'gap',"searchable": true, "orderable": false, "width":100},

            ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_budget = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_available = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_gap = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 4 ).footer() ).html( total_budget );
            $( api.column( 5 ).footer() ).html( total_available );
            $( api.column( 6 ).footer() ).html( total_gap );



        }
        });
</script>
@endsection
