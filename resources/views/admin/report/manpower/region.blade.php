@extends('layouts.backend')

@section('content')
<style>.clickable-row{cursor:pointer}</style>
	<div class="be-content">
	    <div class="main-content container-fluid">
          <div class="row">
            <!--<div class="col-md-12">
				<div class="panel panel-default">
				  <div class="panel-heading panel-heading-divider">
					<div class="tools"></div><span class="title">Manpower Report - Region</span><span class="description"></span>
				  </div>
				  <div class="panel-body" >
					<canvas id="bar-chart" style="height:400px"></canvas>
					<div class="chart-table xs-pt-15">
					  <table class="table table-striped">
						<thead class="primary">
						  <tr>
							<th>Region</th>
							<th>Budgeted</th>
							<th class="number">Available</th>
							<th class="number">GAP</th>
							<th class="number">Avg GAP Aging</th>
							<th class="number">% GAP</th>
						  </tr>
						</thead>
						<tbody class="no-border-x">
						@php
						$manPower = array();
							foreach($manPowerGet as $key => $value){
								$manPower['region_name'][$key] =$value->name;
								$manPower['total_actual_man_power'][$key] =$value->total_actual_man_power;
                                $manPower['gap'][$key] =$value->total_budget_man_power - $value->total_actual_man_power;

						@endphp
						  <tr class="clickable-row"  data-href="{{ url('admin/manpower-report/region-wise-store/') }}/{{$value->region_id}}">
							<td>{{$value->name}}</td>
							<td>{{($value->total_budget_man_power > 0)? $value->total_budget_man_power : 0 }}</td>
							<td class="number">{{($value->total_actual_man_power > 0)? $value->total_actual_man_power : 0 }}</td>
							<td class="number">{{($value->total_budget_man_power > 0 && $value->total_actual_man_power > 0)? $value->total_budget_man_power - $value->total_actual_man_power : 0 }}</td>
							<td class="number">0</td>
							<td class="number">{{($value->total_budget_man_power > 0 && $value->total_actual_man_power > 0)? round((($value->total_budget_man_power - $value->total_actual_man_power)/$value->total_budget_man_power),2) : 0 }}</td>

						  </tr>
						@php
							}
						@endphp
						</tbody>
					  </table>
					</div>
				  </div>
				</div>
            </div>-->
			<div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading panel-heading-divider">
									<div class="tools"></div>
										<span class="title">Manpower Report - Region</span>
										<span class="panel-subtitle">
											<div class="abs-right20">
												<a href="{{url('admin/manpower-report/region-export')}}" class="" title="Download">
													<button class="btn btn-space btn-primary">Export Data</button>
												</a>
												<a href="{{url('admin')}}" class="pull-right" title="Go Back">
													<button class="btn btn-space btn-primary">Go Back</button>
												</a>
											</div>

										</span>
                <span class="panel-subtitle"></span>
                </div>
                <div class="panel-body">
					<div class="row">
						<div class="col-md-9 chart-manpower">
							<canvas id="bar-chart"></canvas>
						</div>
						<div class="col-md-12">
							<div class="chart-table xs-pt-15">
								<div class="table-responsive table-minw-fixed">
									<table style="width:100%;" id="manpower_table" class="table table-striped">
									<thead class="primary">
										<tr>
										<th>Region</th>
										<th>Budgeted</th>
										<th class="number">Available</th>
										<th class="number">GAP</th>
										<th class="number">Avg GAP Ageing</th>
										<th class="number">% GAP</th>
										</tr>
									</thead>
									<tbody class="no-border-x">
									@php
									$manPower = array();
										foreach($manPowerGet as $key => $value){
											$manPower['region_name'][$key] =$value->name;
											$manPower['total_actual_man_power'][$key] =$value->total_actual_man_power;
											$manPower['gap'][$key] =$value->total_budget_man_power - $value->total_actual_man_power;

									@endphp
										<tr class="clickable-row"  data-href="{{ url('admin/manpower-report/region-wise-store/') }}/{{$value->region_id}}">
										<td>{{$value->name}}</td>
										<td>{{($value->total_budget_man_power > 0)? $value->total_budget_man_power : 0 }}</td>
										<td class="number">{{($value->total_actual_man_power > 0)? $value->total_actual_man_power : 0 }}</td>
										<td class="number">{{($value->total_budget_man_power > 0 && $value->total_actual_man_power > 0)? $value->total_budget_man_power - $value->total_actual_man_power : 0 }}</td>
										<td class="number">0</td>
										<td class="number">{{($value->total_budget_man_power > 0 && $value->total_actual_man_power > 0)? round(($value->total_budget_man_power - $value->total_actual_man_power)/$value->total_budget_man_power,2) : 0 }}</td>
										</tr>
									@php
										}
									@endphp
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total</th>
                                            <th></th>
                                            <th class="text-right"></th>
                                            <th class="text-right"></th>
                                            <th class="text-right"></th>
                                            <th class="text-right"></th>
                                        </tr>
                                    </tfoot>
									</table>
								</div>
							</div>
						</div>
                    </div>

                    <div class="panel-body custom-padding-zero">
                        <hr style="height:10px"/>
                        <div class="row" >
                        <div class="panel-heading">Region Budget Manpower Report
                            <a href="{{url('admin/manpower-budget/region-export')}}" class="pull-right"><button class="btn btn-primary mxs-mt-10"> Export Data</button></a>
                        </div>
                        </div>
                        <div class="panel-body custom-padding-zero">
													<div class="table-responsive">
                                <table style="width:100%;" id="employee-budget-table" class="table table-striped responsive table-hover table-fw-widget">
                                    <thead>
                                        <tr>
                                            <th>Region Name</th>
                                            @php
                                                $total_array = array();
                                            @endphp
                                            @foreach ($designations as $designation)
                                            <th>{{$designation}}</th>
                                            @endforeach
                                            <th>Total</th>

                                        </tr>
                                    </thead>
                                    @if (count($region_reports) > 0)
                                    <tbody>

                                        @foreach ($region_reports as $region_report)
                                        <tr class="clickable-row" data-href="{{url('admin/store-budget-store')}}/{{$region_report['region_id']}}">
                                            <td><a href="{{url('admin/store-budget-store')}}/{{$region_report['region_id']}}">{{$region_report['region_name']}}</a></td>
                                            @foreach ($region_report['designation'] as $key => $value)
                                                @php
                                                    if(isset($total_array[$key])){
                                                        $total_array[$key] = $total_array[$key] + $value;
                                                    } else {
                                                        $total_array[$key] = $value;
                                                    }

                                                @endphp
                                            <td>{{$value}}</td>
                                            @endforeach
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <th>Total</th>
                                        @foreach ($total_array as $key =>$value)
                                            <th>{{$value}}</th>
                                        @endforeach
                                    </tfoot>

                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>



              </div>
            </div>
          </div>
        </div>
	</div>
@endsection

@section('scripts')
<script>
	$(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
	var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
	var colors = {
	danger: "rgb(234, 67, 53)",
	grey: "rgb(204, 204, 204)",
	primary: "rgb(66, 133, 244)",
	success: "rgb(52, 168, 83)",
	warning: "rgb(251, 188, 5)"
	};
	function barChart(){
			//Set the chart colors
      var color1 = tinycolor( colors.success );
			var color2 = tinycolor( colors.warning );

      //Get the canvas element
			var ctx = document.getElementById("bar-chart");

			var data = {
	      labels: <?php echo json_encode($manPower['region_name']);?>,
	      datasets: [{
	        label: "Actual Manapower",
	        borderColor: color1,
	        backgroundColor: color1.setAlpha(.8),
	        data: <?php echo json_encode($manPower['total_actual_man_power']);?>
	      }, {
	        label: "Gap",
	        borderColor: color2,
	        backgroundColor: color2.setAlpha(.5),
	        data: <?php echo json_encode($manPower['gap']);?>
	      }]
	    };

	    var bar = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
          elements: {
            rectangle: {
              borderWidth: 2,
              borderColor: 'rgb(0, 255, 0)',
              borderSkipped: 'bottom'
            }
          },
        }
      });
		}
		barChart()
        $("#employee-budget-table").DataTable({
        dom: 'Blfrtip',
        buttons: [],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
        });
        $("#manpower_table").DataTable({
        dom: 'Blfrtip',
        buttons: [],
        "pageLength": 6,
        "scrollY": true, "scrollX": true,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_budget = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_available = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_gap = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 1 ).footer() ).html( total_budget );
            $( api.column( 2 ).footer() ).html( total_available );
            $( api.column( 3 ).footer() ).html( total_gap );
            $( api.column( 4 ).footer() ).html( "-" );
            $( api.column( 5 ).footer() ).html( "-" );


        }

        } );

</script>
@endsection
