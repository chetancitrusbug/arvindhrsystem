@extends('layouts.backend')

@section('content')
<style>.clickable-row{cursor:pointer}</style>
	<div class="be-content">
	    <div class="main-content container-fluid">
          <div class="row">

			<div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading panel-heading-divider">
                  <div class="tools"></div><span class="title">Hygiene Report - Region</span>
                        <a href="{{url('admin')}}" class="pull-right" title="Go Back">
														<button class="btn btn-space btn-warning">Go Back</button>
												</a>
                  <span class="panel-subtitle"></span>
                </div>
                <div class="panel-body">

					<div class="col-md-12">
						<div class="chart-table xs-pt-15">
						<div class="table-responsive table-minw-fixed">
						  <table style="width:100%;" id="heigin_report" class="table table-striped">
							<thead class="primary">
							  <tr>
								<th>Region</th>
								<th>Id Card Not Available</th>
								<th >Uniform not available</th>
								<th>L0L1 untrained</th>
								<th>L0L1 non certified</th>
								<th>Certificate pending</th>
								<th>Appointment Letter</th>
							  </tr>
							</thead>
							<tbody class="no-border-x">
								@php
									foreach($hygiene['id_card'] as $key => $value){
								@endphp
									<tr class="clickable-row" data-href="{{ url('admin/hygiene-report/region-wise-store/') }}/{{$value->region_id}}">
										<td>{{$value->name}}</td>
										<td>{{$value->id_card}}</td>
										<td>{{$hygiene['uniform'][$key]->uniform_count}}</td>
										<td>{{$hygiene['l0l1_untrained'][$key]->l0l1_untrained_count}}</td>
										<td>{{$hygiene['l0l1_certified'][$key]->l0l1_certificate_count}}</td>
										<td>{{$hygiene['certificate_pending'][$key]->certificate_pending_count}}</td>
										<td>{{$hygiene['appoint_letter'][$key]->appointment_status_count}}</td>
									</tr>
								@php

									}
								@endphp
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Total</th>
                                    <th>Id Card Not Available</th>
                                    <th>Uniform not available</th>
                                    <th>L0L1 untrained</th>
                                    <th>L0L1 non certified</th>
                                    <th>Certificate pending</th>
                                    <th>Appointment Letter</th>
                                </tr>
                            </tfoot>
							</table>
								</div>
						</div>
					</div>

                </div>
              </div>
            </div>
          </div>
        </div>
	</div>
@endsection

@section('scripts')
<script>
	$(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });

    $("#heigin_report").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_id_card = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_uniform = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_l0l1 = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_l0l1_certificate = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_certificate_pending = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_appointment = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 1 ).footer() ).html( total_id_card );
            $( api.column( 2 ).footer() ).html( total_uniform );
            $( api.column( 3 ).footer() ).html( total_l0l1 );
            $( api.column( 4 ).footer() ).html( total_l0l1_certificate );
            $( api.column( 5 ).footer() ).html( total_certificate_pending );
            $( api.column( 6 ).footer() ).html( total_appointment );


        }

    });


</script>
@endsection
