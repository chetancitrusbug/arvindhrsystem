@extends('layouts.backend')
@section('title',"Productivity Reports Region")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Productivity report by manpower
                        <a href="{{url('admin')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary mxs-mt-10">Go Back</button>
                        </a>
                        @php
                            $current_year = date('Y');
                            $current_month = date('m');
                            $continue_month = date('m');
                            $current_month = $current_month-1;
                            $months = array();
                            $years = array();
                            if($continue_month == 1){
                                $current_month = 12;
                                $current_year--;
                            }
                            for ($j = $current_year; $j >= $current_year-5; $j--) {
                                $years[$j] = $j;
                            }
                            for($i=1; $i <= $current_month ;$i++){
                                $months[$i]=date( 'M',strtotime( "$current_year-$i-01"));
                            }
                            $current_month_name = date('M',strtotime("$current_year-$current_month-01"));


                            if(app('request')->input('year') && app('request')->input('month') ){
                                $current_month = app('request')->input('month');
                                $current_year = app('request')->input('year');
                            }
                        @endphp
                    </div>
                    <div class="panel-body">

                        <div class="form form-custom-001">
                            {!! Form::open(['method'=>'GET', 'class' => 'form-horizontal group-border-dashed','id'
                            => 'module_form','autocomplete'=>'off', 'files'=>false]) !!}
                            <div class="form-group col-sm-3 {{ $errors->has('month') ? ' has-error' : ''}}">
                                {!! Form::label('', '* Select Month: ', ['class' => '']) !!}
                                <div data-min-view="2" id="" class="input-group date">
                                    {!! Form::select('month', $months,$current_month, ['class' => 'col-sm-12 form-control input-sm month','id'=>'month']) !!}
                                     {!! $errors->first('month','<p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="form-group col-sm-3 {{ $errors->has('year') ? ' has-error' : ''}}">
                                {!! Form::label('', '* Select year: ', ['class' => '']) !!}
                                <div data-min-view="2" id="" class="input-group date">
                                    {!! Form::select('year', $years,$current_year, ['class' => 'col-sm-12 form-control input-sm month','id'=>'year'])
                                    !!} {!! $errors->first('year','
                                    <p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group col-sm-3">
                                {!! Form::label('', '&nbsp; ', ['class' => '']) !!}
                                <div class="">
                                    {{Form::submit('Filter',['class'=>'btn btn-primary'])}}
                                </div>
                            </div>
                            {{Form::close()}}
                        </div>

                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table id="man-power-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Region Name</th>
                                                <th>Sales</th>
                                                <th>No. of employee</th>
                                                <th>Sales per employee</th>
                                                <th>Avg cost per emp</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('admin/productivity-report-data') }}";

    var extraurl = "?year={{app('request')->input('year')}}&month={{app('request')->input('month')}}";
    url = url+extraurl;
    var edit_url = "{{ url('admin/productivity-report-store') }}"
        datatable = $('#man-power-table').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
			searchable:false,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'region_name',name : 'region_name',"searchable": false, "orderable": false, "width":200},
                { data: 'total_amount',name : 'total_amount',"searchable": false, "orderable": true, "width":150},
                { data: 'total_employee',name : 'total_employee',"searchable": false, "orderable": true, "width":150},
                { data: 'per_person_income',name : 'per_person_income',"searchable": false, "orderable": true, "width":150},
                { data: 'avg_ctc',name : 'avg_ctc',"searchable": false, "orderable": true, "width":150},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d="";
                        v = "<a href='"+edit_url+"/"+o.region_id+extraurl+"' value="+o.region_id+" data-id="+o.region_id+"><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";
                        return v;
                    }
                }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_sales = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_employee = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotal = api
                .column( 1, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            avg_per_employee =0;
            avg_cost = 0;
            if(total_sales > 0 && total_employee > 0){
                avg_per_employee = total_sales/total_employee;

            }



            $( api.column( 1 ).footer() ).html( total_sales );
            $( api.column( 2 ).footer() ).html( total_employee );
            $( api.column( 3 ).footer() ).html( avg_per_employee.toFixed(2) );

        }
        });

        $("#start_date_div").datetimepicker({ format:'dd/mm/yyyy', endDate: new Date(), maxDate: 'today', autoclose: true, componentIcon:
        '.mdi.mdi-calendar' }); $("#end_date_div").datetimepicker({ endDate: new Date(), maxDate: 'today', format:'dd/mm/yyyy', autoclose:
        true, componentIcon: '.mdi.mdi-calendar' });
        $("#year").change(function() {
             var current_year = (new Date()).getFullYear();
              var current_month = (new Date()).getMonth();
            var selected_year = $(this).val()
            var monthArray = new Array();
            monthArray[1] = "Jan";
            monthArray[2] = "Feb";
            monthArray[3] = "Mar";
            monthArray[4] = "Apr";
            monthArray[5] = "May";
            monthArray[6] = "Jun";
            monthArray[7] = "Jul";
            monthArray[8] = "Aug";
            monthArray[9] = "Sep";
            monthArray[10] = "Oct";
            monthArray[11] = "Nov";
            monthArray[12] = "Dec";

            if(current_year != selected_year){
                     $("#month").html(''); for(m = 1; m <=12; m++) {
                         var monthElem=document.createElement( "option");
                         monthElem.value=m ; monthElem.textContent=monthArray[m];
                        $( "#month").append(monthElem);
                    }
            } else {
                $( "#month").html( '');
                for(m=1 ; m <=current_month; m++) {
                    var monthElem=document.createElement("option");
                    monthElem.value=m ;
                    monthElem.textContent=monthArray[m];
                    $( "#month").append(monthElem);
                }
            }
        });

</script>
@endsection