@extends('layouts.backend')
@section('title',"Attendance Report")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Attendace Report
                        <span class="panel-subtitle">
                            @if($groupby != "rigion")
                                <a href="{{ $_SERVER['HTTP_REFERER'] }}" title="Create">
                                    <button class="btn btn-space btn-warning btn-back">Go Back</button>
                                </a>
                            @endif
        
                            {!! Form::open(['url' => url('admin/attendance-report'), 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                                {{Form::hidden('city',$city,[])}}
                                {{Form::hidden('state',$state,[])}}
                                {{Form::hidden('region',$region,[])}}
                                {{Form::hidden('groupby',$groupby,[])}}
                                <div class="col-sm-3">
                                    <div class="form-group{{ $errors->has('filter.from_date') ? ' has-error' : ''}}">
                                        {!! Form::label('filter[from_date]', '* From Date: ', ['class' => 'pull-left control-label']) !!}
                                        <div class="col-sm-6">
                                            <div data-min-view="2"  class="input-group date datetimepicker1">
                                                {!! Form::text('filter[from_date]', $filter['from_date'], ['class' => 'form-control input-sm']) !!}
                                                <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                            </div>
                                            {!! $errors->first('filter.from_date', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group{{ $errors->has('filter.to_date') ? ' has-error' : ''}}">
                                        {!! Form::label('filter[to_date]', '* To Date: ', ['class' => 'pull-left control-label']) !!}
                                        <div class="col-sm-6">
                                            <div data-min-view="2"  class="input-group date datetimepicker1">
                                                {!! Form::text('filter[to_date]', $filter['to_date'], ['class' => 'form-control input-sm']) !!}
                                                <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                            </div>
                                            {!! $errors->first('filter.to_date', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3 pull-left">
                                    <div class="form-group">
                                        {!! Form::submit('Submit', ['class' => 'btn btn-primary pull-left']) !!}
                                    </div>
                                </div>

                            {!! Form::close() !!}
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table id="region-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>
                                                    @if($groupby == "store")
                                                        Store Name
                                                    @elseif($groupby == "employee")
                                                        Employee
                                                    @elseif($groupby == "region")
                                                        Region
                                                    @elseif($groupby == "state")
                                                        State
                                                    @elseif($groupby == "city")
                                                        City
                                                    @endif
                                                </th>
                                                <th width="200">Total Days</th>
                                                <th width="200">Present Days</th>
                                                <th width="200">Gap</th>
                                                {{-- @if($groupby == "store")
                                                    <th width="200">Rejected</th>
                                                @endif --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($employee))
                                                @foreach($employee as $value)
                                                    {{-- @dump($value->count_emp) --}}
                                                    {{-- @dump($value->count_emp) --}}
                                                    <tr>
                                                        <td>
                                                            @if($groupby == "store")
                                                                <a href="{{ url('admin/attendance-report').'?groupby=employee&store='.$value->id.'&filter[from_date]='.$filter['from_date'].'&filter[to_date]='.$filter['to_date'] }}">{{$value->store_name}}</a>
                                                            @elseif($groupby == "employee")
                                                                {{$value->full_name}}
                                                            @elseif($groupby == "region")
                                                                <a href="{{ url('admin/attendance-report').'?groupby=state&region='.$value->region_id.'&filter[from_date]='.$filter['from_date'].'&filter[to_date]='.$filter['to_date'] }}">{{$value->region_name}}</a>
                                                            @elseif($groupby == "state")
                                                                <a href="{{ url('admin/attendance-report').'?groupby=city&state='.$value->state_id.'&filter[from_date]='.$filter['from_date'].'&filter[to_date]='.$filter['to_date'] }}">{{$value->state_name}}</a>
                                                            @elseif($groupby == "city")
                                                                <a href="{{ url('admin/attendance-report').'?groupby=store&city='.$value->city_id.'&filter[from_date]='.$filter['from_date'].'&filter[to_date]='.$filter['to_date'] }}">{{$value->city_name}}</a>
                                                            @endif
                                                        </td>
                                                        @php
                                                            $actual = $value->actual_man_power*25;
                                                            $gap = $actual - $value->present;
                                                        @endphp
                                                        <td>{!! $actual !!}</td>
                                                        <td>{!! $value->present !!}</td>
                                                        <td>{!! $gap !!}</td>
                                                        {{-- @if($groupby == "store")
                                                            <td>{!! $value->rejected !!}</td>
                                                        @endif --}}
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4">There are no data</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>    
                        </div>    

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(".datetimepicker1").datetimepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            componentIcon: '.mdi.mdi-calendar',
        /*    pickerPosition: "bottom-left", */
            navIcons:{
                rightIcon: 'mdi mdi-chevron-right',
                leftIcon: 'mdi mdi-chevron-left'
            }
        });
    </script>
@endsection