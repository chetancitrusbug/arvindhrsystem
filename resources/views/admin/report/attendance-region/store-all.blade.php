@extends('layouts.backend')
@section('title',"Attendance Report")
@section('content')

@php( $url_params = "")


    @if(\Request::has('start_date') && \Request::has('end_date'))
		@php( $url_params = "start_date=".\Request::get('start_date')."&end_date=".\Request::get('end_date') )
	@endif

<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Attendace Report ({{($region)?$region->name:''}})

                        <a href="{{url('admin')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary">Go Back</button>
                        </a>

                    </div>

					 <div class="blk-div margin-top-20 clearfix">
                        {!! Form::open(['url' => url('admin/attendance-report/region'), 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true,'method' => 'get']) !!}
                        {!! Form::hidden("start_date", null ,['id'=>'input_start_date']) !!}
                        {!! Form::hidden("end_date", null ,['id'=>'input_end_date']) !!}

                            <div class="col-sm-4">
                                <div style="width: 100%">
                                    <div id="reportrange" class="form-control input-sm"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-3 pull-left">
                                <div class="">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                                </div>
                                <br/>
                            </div>

                        {!! Form::close() !!}
                    </div>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table id="region-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Store Code</th>
                                                <th>Store </th>
                                                <th>Live Manpower </th>
                                                <th>Present Manpower </th>
                                                <th>% Manpower on floor </th>
                                                <th>Leave </th>
                                                <th>% on Leave </th>
                                                <th>W/O </th>
                                                <th>% on week off </th>
                                                <th>C.O </th>
                                                <th>% on C/O </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php( $total_actual_man_power_all = 0)
                                            @php( $man_power_p_all = 0)
                                            @php( $man_power_p_per_all = 0)
                                            @php( $man_power_l_all = 0)
                                            @php( $man_power_l_per_all = 0)
                                            @php( $man_power_wo_all = 0)
                                            @php( $man_power_wo_per_all = 0)
                                            @php( $man_power_co_all = 0)
                                            @php( $man_power_co_per_all = 0)

                                            @if(count($regiondata))
                                                @foreach($regiondata as $value)
                                                    <tr>
                                                        <td>  {!! $value['io_code'] !!} </td>
                                                        <td> <a href="{{url('admin/attendance-report/employee')}}/{{ $value['id'] }}?{{$url_params}}" title="Store Vise Report"> {!! $value['name'] !!} </a></td>
                                                        <td>{!! $value['total_actual_man_power'] !!}</td>
                                                        <td>{!! $value['man_power_p'] !!}</td>
                                                        <td>{!! $value['man_power_p_per'] !!}</td>
                                                        <td>{!! $value['man_power_l'] !!}</td>
                                                        <td>{!! $value['man_power_l_per'] !!}</td>
                                                        <td>{!! $value['man_power_wo'] !!}</td>
                                                        <td>{!! $value['man_power_wo_per'] !!}</td>
                                                        <td>{!! $value['man_power_co'] !!}</td>
                                                        <td>{!! $value['man_power_co_per'] !!}</td>


                                                    </tr>

                                                    @php( $total_actual_man_power_all+= $value['total_actual_man_power'])
                                                    @php( $man_power_p_all+= $value['man_power_p'])
                                                    @php( $man_power_p_per_all+= $value['man_power_p_per'])
                                                    @php( $man_power_l_all+= $value['man_power_l'])
                                                    @php( $man_power_l_per_all+= $value['man_power_l_per'])
                                                    @php( $man_power_wo_all+= $value['man_power_wo'])
                                                    @php( $man_power_wo_per_all+= $value['man_power_wo_per'])
                                                    @php( $man_power_co_all+= $value['man_power_co'])
                                                    @php( $man_power_co_per_all+= $value['man_power_co_per'])
                                                @endforeach
                                        </tbody>
                                                <tfoot>
                                                <tr>
                                                <th colspan="2">Total </th>
                                                <th> {{$total_actual_man_power_all}} </th>
                                                <th> {{$man_power_p_all}} </th>
                                                <th> @if($total_actual_man_power_all > 0) {{ round(($man_power_p_all * 100) / $total_actual_man_power_all, 2)}} @else 0 @endif</th>
                                                <th> {{$man_power_l_all}} </th>
                                                <th> @if($total_actual_man_power_all > 0){{ round(($man_power_l_all * 100) / $total_actual_man_power_all, 2)}} @else 0 @endif </th>
                                                <th> {{$man_power_wo_all}} </th>
                                                <th> @if($total_actual_man_power_all > 0){{ round(($man_power_wo_all * 100) / $total_actual_man_power_all, 2)}}  @else 0 @endif</th>
                                                <th> {{$man_power_co_all}} </th>
                                                <th> @if($total_actual_man_power_all > 0){{ round(($man_power_co_all * 100) / $total_actual_man_power_all, 2)}} @else 0 @endif </th>

                                            </tr>
                                            </tfoot>
                                            @else
                                                <tr>
                                                    <td colspan="10">There are no data</td>
                                                </tr>
                                            @endif

                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
	datatable = $('#region-table').DataTable();
       var range_start ="";
    var range_end ="";



		/*************************daterange selection*****************/



    var start = moment().subtract(1, 'days');//moment.utc('2015-01-01','YYYY-MM-DD');
    var end = moment().subtract(1, 'days');
    @if(\Request::has('start_date') && \Request::has('end_date'))
     start = moment.utc("{{\Request::get('start_date')}}",'YYYY-MM-DD');
     end = moment.utc("{{\Request::get('end_date')}}",'YYYY-MM-DD');
    @endif

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');


        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');


        }

		$("#input_start_date").val(range_start);
		$("#input_end_date").val(range_end);


    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "@lang('comman.daterange.all')":[moment.utc('2015-01-01','YYYY-MM-DD'),moment().subtract(1, 'days')],
            "@lang('comman.daterange.yesterday')": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            "Week till date": [moment().startOf('week'), moment().subtract(1, 'days')],
            "Month till date": [moment().startOf('month'), moment().subtract(1, 'days')],
			"Year till date": [moment().startOf('year'), moment().subtract(1, 'days')],
            "@lang('comman.daterange.lastmonth')": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            "@lang('comman.daterange.lastyear')": [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
        }
    }, cb);

     cb(start, end);
    </script>
@endsection
