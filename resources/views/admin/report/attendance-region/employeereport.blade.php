@extends('layouts.backend')
@section('title',"Employee Attendance Report")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Attendace Report
                        <span class="panel-subtitle">
                            <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                                <button class="btn btn-space btn-warning">Go Back</button>
                            </a>
                        </span>                                
                        <div class="blk-div clearfix">
                            {!! Form::open(['class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true,'method'
                            => 'get']) !!}
                            {!! Form::hidden("start_date", null ,['id'=>'input_start_date']) !!}
                            {!! Form::hidden("end_date", null,['id'=>'input_end_date']) !!}

                            <div class="col-sm-4">
                                <div style="width: 100%">
                                    <div id="reportrange" class="form-control input-sm" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                        <span></span> <b class="caret"></b>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-3 pull-left">
                                <div class="">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="panel-body ">
                        <div class="padding20">         
                            <div class="widget-head">
                                <div class="tools">
                                    {!! Form::open(['url'=>url('admin/attendence-csv-export-store'),'class' => 'form-horizontal group-border-dashed','id' =>
                                    'module_form','autocomplete'=>'off', 'files'=>false]) !!} 
                                    {!! Form::hidden('export_start_date',old('export_start_date',''),
                                    ['id'=>'export_start_date','class' => 'form-control input-sm start_date']) !!} 
                                    {!! Form::hidden('export_end_date',
                                    old('export_end_date',''), ['id'=>'export_end_date','class' => 'form-control input-sm end_date']) !!}
                                     {!! Form::hidden('store_id',old('store_id',request()->route('store_id')), ['id'=>'store_id','class' => 'form-control input-sm store_id']) !!}
                                    {{Form::submit('Export To Excel',['class'=>'btn btn-primary'])}} {{Form::close()}}
                                </div>
                                <span class="title">Attandence List </span><span class="description"></span>
                            </div>
                            <div class="table-responsive tablescroll-xy">
                                <table id="employee_attendance" class="table table-striped table-hover table-fw-widget ">
                                    @php
                                        $j = 0;
                                    @endphp
                                    @foreach ($attandance_data as $attandance)
                                        <tr>
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($attandance as $item)
                                                @if ($i == 1 && $j < 3)
                                                <th colspan="8">{{$item}}</th>
                                                @else
                                                    <td>{{$item}}</td>
                                                    @endif
                                                @php
                                                    $i++;
                                                @endphp
                                            @endforeach
                                        </tr>
                                        @php $j++;  @endphp
                                    @endforeach
                                </table>
                            </div>
                        </div>                
                    </div>
                </div>
               
            </div>
        </div>
        </span>
    </div>

            
      

</div>
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script>
    var range_start ="";
    var range_end ="";
	//datatable = $('#employee_attendance').DataTable();
    $(".datetimepicker-atendance").datetimepicker({
        autoclose: true,
        maxDate: new Date(),
        format: 'dd/mm/yyyy',
        componentIcon: '.mdi.mdi-calendar',
     /*   pickerPosition: "bottom-left", */
        navIcons:{
            rightIcon: 'mdi mdi-chevron-right',
            leftIcon: 'mdi mdi-chevron-left' }
    });

        /*************************daterange selection*****************/

        var start = moment().subtract(1, 'days');
        //moment.utc('2015-01-01','YYYY-MM-DD');
        var end = moment().subtract(1, 'days');
        @if(\Request::has('start_date') &&  \Request::has('end_date'))
            start = moment.utc("{{\Request::get('start_date')}}",'YYYY-MM-DD');
            end = moment.utc("{{\Request::get('end_date')}}",'YYYY-MM-DD');
        @endif
        $("#export_start_date").val(start.format('YYYY-MM-DD'));
        $("#export_end_date").val(end.format('YYYY-MM-DD'));
        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            if(range_start==""){
                range_start = start.format('YYYY-MM-DD');
                range_end = end.format('YYYY-MM-DD');
            }else{
                range_start = start.format('YYYY-MM-DD');
                range_end = end.format('YYYY-MM-DD');
            }
            $("#input_start_date").val(range_start);
            $("#input_end_date").val(range_end);
        }
        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                "@lang('comman.daterange.all')":[moment.utc('2015-01-01','YYYY-MM-DD'),moment().subtract(1,'days')],
                "@lang('comman.daterange.yesterday')": [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                "Week till date": [moment().startOf('week'), moment().subtract(1, 'days')],
                "Month till date": [moment().startOf('month'), moment().subtract(1, 'days')],
                "Year till date": [moment().startOf('year'), moment().subtract(1, 'days')],
                "@lang('comman.daterange.lastmonth')": [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                "@lang('comman.daterange.lastyear')": [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
            }
        }, cb);
        cb(start, end);
</script>
@endsection