@extends('layouts.backend')
@section('title',"Man power")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Man Power List
                        <span class="panel-subtitle">
                            <a href="{{ url('admin/man-power-upload') }}" title="Create" data-url="" class="btn btn-space btn-primary">
                                Excel Upload
                            </a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <table  id="man-power-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Store Name</th>
                                    <th>Store Code</th>
                                    <th>Location</th>
                                    <th>Budget Man Power</th>
                                    <th>Actual Man Power</th>
                                    <th>Gap</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/admin/man-power-data') }}";
        var edit_url = "{{ url('/admin/man-power') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#man-power-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false, "width":200},
                { data: 'store_code',name : 'store_code',"searchable": true, "orderable": false, "width":150},
                { data: 'city',name : 'city',"searchable": true, "orderable": false, "width":150},
                { data: 'budget_man_power',name : 'budget_man_power',"searchable": true, "orderable": false, "width":100},
                { data: 'actual_man_power',name : 'actual_man_power',"searchable": true, "orderable": false, "width":100},
                { data: 'gap',name : 'gap',"searchable": true, "orderable": false, "width":100},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='mdi mdi-edit'></i></a>&nbsp;";

                        /* d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/man-power')}} data-msg='Variable Pay Type' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;"; */
                        return v+e;
                    }

                }
            ]
        });
</script>
@endsection