@extends('layouts.backend')
@section('title',"View Man Power")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="user-info-list panel panel-default">
                <div class="panel-heading panel-heading-divider">Man Power # <strong>{{ $manPower->store_name }}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/man-power') }}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    <table class="no-border no-strip skills" style="width:30%">
                        <tbody class="no-border-x no-border-y">
                            <tr>
                                <th class="item">Store Name</th>
                                <td>: {{ $manPower->store_name }}</td>
                            </tr>
                            <tr>
                                <th class="item">Store Code</th>
                                <td>: {{ $manPower->store_code }}</td>
                            </tr>
                            <tr>
                                <th class="item">Location</th>
                                <td>: {{ $manPower->city }}</td>
                            </tr>
                            <tr>
                                <th class="item">Budget Man Power</th>
                                <td>: {{ $manPower->budget_man_power }}</td>
                            </tr>
                            <tr>
                                <th class="item">Actual Man Power</th>
                                <td>: {{ $manPower->actual_man_power }}</td>
                            </tr>
                            <tr>
                                <th class="item">Gap</th>
                                <td>: {{ $manPower->gap }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <hr/>
                    <table class="table table-bordered" style="width:50%">
                        <tbody class="no-border-x no-border-y">
                            <tr>
                                <th>Designation</th>
                                <th>Man Power</th>
                            </tr>
                            {!! Form::model($manPower, ['method' => 'PATCH','url' => ['/admin/man-power', $manPower->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                                @foreach ($designation as $key=>$value)
                                    <tr>
                                        <td>{{$value}}</td>
                                        <td style="padding-left:10px;">{!! Form::number("designation[$key]",isset($manPowerDetail[$key])?$manPowerDetail[$key]:null , ['class' => 'form-control input-sm','min'=>'1']) !!}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="2">{!! Form::submit('Update', ['class' => 'btn btn-space btn-primary pull-right']) !!}</td>
                                </tr>
                            {!! Form::close() !!}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('input[type="number"]').on('change',function(){
            $(this).val(Math.abs($(this).val()));
        });
    </script>
@endsection
