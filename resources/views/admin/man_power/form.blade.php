<div class="form-group{{ $errors->has('store_name') ? ' has-error' : ''}}">
    {!! Form::label('store_name', '* Store Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('store_name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('store_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<table class="no-border no-strip skills" style="width:30%">
    <tbody class="no-border-x no-border-y">
        <tr>
            <th class="item">Store Name</th>
            <td>: {{ $manPower->store_name }}</td>
        </tr>
        <tr>
            <th class="item">Store Code</th>
            <td>: {{ $manPower->store_code }}</td>
        </tr>
        <tr>
            <th class="item">Location</th>
            <td>: {{ $manPower->city }}</td>
        </tr>
        <tr>
            <th class="item">Budget Man Power</th>
            <td>: {{ $manPower->budget_man_power }}</td>
        </tr>
        <tr>
            <th class="item">Actual Man Power</th>
            <td>: {{ $manPower->actual_man_power }}</td>
        </tr>
        <tr>
            <th class="item">Gap</th>
            <td>: {{ $manPower->gap }}</td>
        </tr>
    </tbody>
</table>