@extends('layouts.backend')
@section('title',"Import Legal Entity")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Import Legal Entity
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/legal-entity') }}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>

                        <a href="{{ url('/uploads/legal_entity_sample.xlsx') }}" title="Back" download="legal_entity_sample" target="_blank">
                            <button class="btn btn-space btn-primary">Sample file download</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/admin/legalEntityUpload', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                        <div class="form-group has-error">
                            {!! Form::label('', '', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                @if(Session::has('valLocationCode') && Session::get('valLocationCode') != '')
                                    <p class="help-block">invalid Location code entered in row {{ Session::get('valLocationCode') }}</p>
                                @endif
                            </div>
                        </div>
                        @include('storeManager.employee.import')

                        <div class="form-group col-sm-4">
                            {!! Form::submit('Upload', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection