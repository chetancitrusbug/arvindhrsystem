@extends('layouts.backend')
@section('title',"Edit Legal Entity")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Edit Legal Entity # <strong>{{$legalEntity->name}}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/admin/legal-entity') }}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                   {!! Form::model($legalEntity, ['method' => 'PATCH','url' => ['/admin/legal-entity', $legalEntity->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        @include ('admin.legal_entity.form')

                        <div class="form-group col-sm-4">
                            {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection