@extends('layouts.backend')
@section('title',"Legal Entity")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Legal Entity List
                        <span class="panel-subtitle">
                            <a href="{{ url('/admin/legal-entity/create') }}" title="Create">
                                <button class="btn btn-space btn-success">Create</button>
                            </a>
                            <a href="{{ url('admin/legal-entity-upload') }}" title="Excel Upload" data-url="" class="btn btn-space btn-primary">
                                Excel Upload
                            </a>
                            <a href="{{ url('/uploads/legal_entity_sample.xlsx') }}" title="Sample file download" download="legal_entity_sample" target="_blank">
                                <button class="btn btn-space btn-primary">Sample file download</button>
                            </a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="legal-entity-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Legal Entity</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/admin/legal-entity-data') }}";
        var edit_url = "{{ url('/admin/legal-entity') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#legal-entity-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'name',name : 'name',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": true,
                    "render": function (o) {
                        if(o.status == 1){
                            return "<input type='checkbox' class='status status-change' checked data-table='legal_entity' data-status="+o.status+" onchange=statusChange() data-url={{url('admin/change-status')}} value="+o.id+" data-id="+o.id+">&nbsp;";
                        }
                        return "<input type='checkbox' class='status status-change' data-url={{url('admin/change-status')}} data-table='legal_entity' data-status="+o.status+" value="+o.id+" data-id="+o.id+">&nbsp;";
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='mdi mdi-edit'></i></a>&nbsp;";

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/legal-entity')}} data-msg='Legal Entity' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";
                        return e;
                    }

                }
            ]
        });
</script>
@endsection