@extends('layouts.backend')
@section('title',"Attrition Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Attrition Report 
                        <a href="{{url('admin/attrition-report/region')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary">Go Back</button>
                        </a>
                    </div>
                    <div class="blk-div margin-top-20 clearfix">
                        {!! Form::open([ 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true,'method' => 'get']) !!}
                        {!! Form::hidden("start_date", null ,['class'=>'input_start_date']) !!}
                        {!! Form::hidden("end_date", null ,['class'=>'input_end_date']) !!}

                        <div class="col-sm-4">
                            <div style="width: 100%">
                                <div id="reportrange" class="form-control input-sm"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    <span></span> <b class="caret"></b>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-3 pull-left">
                            <div class="">
                                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                            </div>
                            <br/>
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="employee-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Region</th>
                                                <th>Store Code</th>
                                                <th>Store Name</th>
                                                <th>Total</th>
                                                <th>Absconding</th>
												<th>Resignation</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Total</th>
                                                <th>Absconding</th>
												<th>Resignation</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var start =moment().startOf('year').add(3,'month');//moment.utc('2015-01-01','YYYY-MM-DD');
    var end = moment().endOf('year').add(3,'month');
    @if(\Request::has('start_date') && \Request::has('end_date'))
     start = moment.utc("{{\Request::get('start_date')}}",'YYYY-MM-DD');
     end = moment.utc("{{\Request::get('end_date')}}",'YYYY-MM-DD');
    @endif
     var range_start ="";
    var range_end ="";
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');


        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');

           
        }

		$(".input_start_date").val(range_start);
		$(".input_end_date").val(range_end);
        

    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "Current":[start,end],
            "@lang('comman.daterange.lastyear')": [moment().subtract(1, 'year').startOf('year').add(3,'month'), moment().subtract(1, 'year').endOf('year').add(3,'month')]
        }
    }, cb);

     cb(start, end);
    var url ="{{ url('/admin/attrition-report/store-data/') }}/{{request()->route('region_id')}}";
        var view_url = "{{ url('/admin/attrition-report/store-report/') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            ajax: {
                url:url,
                type:"get",
                "data": {
                    "start_date":start.format('YYYY-MM-DD'),
                    "end_date":  end.format('YYYY-MM-DD')
                }
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'name',name : 'name',"searchable": true, "orderable": false, "width":200},
                { data: 'io_code',name : 'io_code',"searchable": true, "orderable": false, "width":200},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false, "width":200},
                { data: 'Total',name : 'Total',"searchable": true, "orderable": false, "width":150},
                { data: 'absconding',name : 'absconding',"searchable": true, "orderable": false, "width":150},
                { data: 'resignation',name : 'resignation',"searchable": true, "orderable": false, "width":100},
             /*   {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var p = 0;
                        if(o.actual_man_power > 0 && o.Total > 0){
                            p = (o.actual_man_power/o.Total)*100;
                        }
                        if(p > 0){
                            p = p+"%"
                        }
                        return p;
                    }
                },
            /*    {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var v="";
                        v = "<a href='"+view_url+"/"+o.store_id+"' value="+o.store_id+" data-id="+o.store_id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        return v;
                    }

                } */
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            resignation = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            abscounding = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 3 ).footer() ).html( total );
            $( api.column( 4 ).footer() ).html( resignation );
            $( api.column( 5 ).footer() ).html( abscounding );  /* per.toFixed(2) */


        }
        });

</script>
@endsection