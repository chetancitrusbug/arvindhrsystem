@extends('layouts.backend')
@section('title','Hiring Report')
@section('content')
<style>
    .clickable-row {
        cursor: pointer
    }
</style>
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">
                        <div class="tools"></div><span class="title">Hiring Report - Region
                            <a href="{{url('admin')}}" class="pull-right" title="Go Back">
                                <button class="btn btn-space btn-warning">Go Back</button>
                            </a>
                        </span><span class="panel-subtitle"></span>
                    </div>
                    <div class="blk-div margin-top-20 clearfix">
                        {!! Form::open(['url' => url('admin/hiring-report/region'), 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true,'method' => 'get']) !!}
                        {!! Form::hidden("start_date", null ,['class'=>'input_start_date']) !!}
                        {!! Form::hidden("end_date", null ,['class'=>'input_end_date']) !!}

                        <div class="col-sm-4">
                            <div style="width: 100%">
                                <div id="reportrange" class="form-control input-sm"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    <span></span> <b class="caret"></b>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-3 pull-left">
                            <div class="">
                                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                            </div>
                            <br/>
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="hiring-reports-gap" class="table table-striped">
                                        <thead class="primary">
                                            <tr>
                                                @php
                                                    $total_gap = '';
                                                @endphp
                                                <th>Months</th>
                                                @foreach ($region as $key =>$item)
                                                    <th colspan="2" class="clickable-row" data-href="{{url('admin/hiring-report/store/')}}/{{$item->id}}">{{$item->name}}</th>
                                                    @php
                                                        $total_gap .='<th>GAP</th><th>Hired</th>';
                                                    @endphp
                                                    @endforeach
                                                </tr>
                                            <tr>
                                                <th></th>
                                            {!!$total_gap!!}
                                            </tr>
                                        </thead>
                                        <tbody class="no-border-x">
                                            @foreach($inmonths as $months)
                                            @php
                                        /*    $month_hiring_reports = null;
                                                if(isset($hiringReport[$months])){
                                                    $month_hiring_reports = $hiringReport[$months];
                                                } */
                                            @endphp
                                            {{-- @if ($month_hiring_reports) --}}
                                            <tr>
                                                <td>
                                                        {{date('M-Y',strtotime('01-'.$months))}}
                                                </td>
                                                @foreach ($region as $reg)
                                                    @php
                                                        $month_hiring_report = $hiringReport[$months][$reg->id];
                                                    @endphp
                                                    <td>{{($month_hiring_report) ? $month_hiring_report->gap : 0 }}</td>
                                                    <td>{{($month_hiring_report) ? $month_hiring_report->hired : 0 }}</td>
                                                @endforeach

                                            </tr>
                                            {{-- @endif --}}
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>



            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">
                    <div class="tools"></div><span class="title">Hiring Report - Region (MTD)</span><span class="panel-subtitle">{{date('M-Y')}}</span>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%" id="hiring-report-mtd" class="table table-striped">
                                        <thead class="primary">
                                            <tr>
                                                <th>Region</th>
                                                <th>GAP</th>
                                                <th>In Process</th>
                                                <th>Hired</th>
                                                <th>Pending</th>
                                            </tr>
                                        </thead>
                                        <tbody class="no-border-x">
                                            @if (count($monthinprocesshiringreports) > 0)
                                                @foreach ($monthinprocesshiringreports as $month_hiring_report)
                                                    <tr class="clickable-row" data-href="{{url('admin/hiring-report/store/')}}/{{$month_hiring_report['region_id']}}">
                                                        <td>{{(isset($month_hiring_report['region_name'])) ? $month_hiring_report['region_name'] : '' }}</td>
                                                        <td>{{(isset($month_hiring_report['gap'])) ? $month_hiring_report['gap'] : 0 }}</td>
                                                        <td>{{(isset($month_hiring_report['in_process'])) ? $month_hiring_report['in_process'] : 0 }}</td>
                                                        <td>{{(isset($month_hiring_report['hired'])) ? $month_hiring_report['hired'] : 0 }}</td>
                                                        <td>{{(isset($month_hiring_report['pending'])) ? $month_hiring_report['pending'] : 0 }}</td>
                                                    </tr>
                                                @endforeach

                                            @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th>GAP</th>
                                                <th>In Process</th>
                                                <th>Hired</th>
                                                <th>Pending</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
    $("#hiring-report-mtd").DataTable({
         dom: 'Blfrtip',
         buttons: [
            {
                extend: 'excel',
            }
        ],
         "pageLength": 25,
         "scrollY": 480,
         "scrollX": true,
         "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_gap = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_in_process = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_hired = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_pending = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 1 ).footer() ).html( total_gap );
            $( api.column( 2 ).footer() ).html( total_in_process );
            $( api.column( 3 ).footer() ).html( total_hired );
            $( api.column( 4 ).footer() ).html(total_pending );


        }
    });

    $("#hiring-reports-gap").DataTable({
         dom: 'Blfrtip',
         buttons: ['excel'],
         "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
         "pageLength": 25,
         "order": false,
    });

       var range_start ="";
    var range_end ="";



		/*************************daterange selection*****************/



    //var start = moment().subtract(1, 'days');//moment.utc('2015-01-01','YYYY-MM-DD');
    //var end = moment().subtract(1, 'days');

    var start =moment().startOf('year').add(3,'month');//moment.utc('2015-01-01','YYYY-MM-DD');
    var end = moment().endOf('year').add(3,'month');
    @if(\Request::has('start_date') && \Request::has('end_date'))
     start = moment.utc("{{\Request::get('start_date')}}",'YYYY-MM-DD');
     end = moment.utc("{{\Request::get('end_date')}}",'YYYY-MM-DD');
    @endif

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');


        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');


        }

		$(".input_start_date").val(range_start);
		$(".input_end_date").val(range_end);


    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "Current":[start,end],
            "@lang('comman.daterange.lastyear')": [moment().subtract(1, 'year').startOf('year').add(3,'month'), moment().subtract(1, 'year').endOf('year').add(3,'month')]
        }
    }, cb);

     cb(start, end);
</script>
@endsection