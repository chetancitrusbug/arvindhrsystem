@extends('layouts.backend')
@section('title',"Hiring Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Attrition report - By percetage of live manpower ({{$region_name}})
                        <a href="{{url('admin')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary">Go Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="man-power-percentage-report-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Store Code</th>
                                    <th>Store Name</th>
                                    <th>Total Man Power</th>
                                    <th>Total Attrition</th>
                                    <th>Percentage</th>
                                </tr>
                            </thead>
                            @if (count($manpowerPercentageReport) > 0)
                            <tbody>
                                @foreach ($manpowerPercentageReport as $item)
                                <tr>
                                    <td>{{$item['io_code']}}</td>
                                    <td>{{$item['store_name']}}</td>
                                    <td>{{$item['total_actual_man_power']}}</td>
                                    <td>{{$item['Total']}}</td>
                                    <td>
                                        @if ($item['total_actual_man_power'] > 0 && $item['Total'] > 0)
                                            @php
                                                echo round(($item['total_actual_man_power']/$item['Total'])*100,2);
                                            @endphp %
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                            @else
                            <tr>
                                <td>
                                    No Data Found
                                </td>
                            </tr>
                            @endif
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Total</th>
                                    <th>Total Man Power</th>
                                    <th>Total Attrition</th>
                                    <th>Percentage</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
$("#man-power-percentage-report-table").DataTable({
    dom: 'Blfrtip',
    buttons: ['excel'],
    "pageLength": 25,
    "scrollY": 480, "scrollX": true,
    "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
    "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            actual = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            attrition = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            per = 0;
            if( actual > 0 && attrition > 0){
                per = (attrition * 100)  / actual;
            }

            $( api.column( 2 ).footer() ).html( actual );
            $( api.column( 3 ).footer() ).html( attrition );
            $( api.column( 4 ).footer() ).html( '-' );  /* per.toFixed(2) */


        }
    });
</script>
@endsection