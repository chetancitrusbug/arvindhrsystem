@extends('layouts.backend')
@section('title',"Hiring Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-sm-12">
                
                <div class="panel panel-default panel-table">
                    @php 
                    $y = date('Y') + 1;
                    $start_date = date('Y').'/04/01';
                    $end_date = $y.'/03/31';
                    if(isset($_GET['start_date']) && $_GET['start_date']  != ''){
                        $start_date = $_GET['start_date'];
                    }
                    if(isset($_GET['end_date']) && $_GET['end_date'] != ''){
                        $end_date = $_GET['end_date'];
                    }
                    @endphp
                    <div class="panel-heading panel-heading-divider clearfix">
                        Attrition Report - Region (YTD) <small>{{ date('M/Y',strtotime($start_date))}} - {{ date('M/Y',strtotime($end_date))}}</small>
                        <a href="{{url('admin')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning">Go Back</button>
                        </a>
                    </div>
                    <div class="blk-div margin-top-20 clearfix">
                        {!! Form::open([ 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true,'method' => 'get']) !!}
                        {!! Form::hidden("start_date", null ,['class'=>'input_start_date']) !!}
                        {!! Form::hidden("end_date", null ,['class'=>'input_end_date']) !!}

                        <div class="col-sm-4">
                            <div style="width: 100%">
                                <div id="reportrange" class="form-control input-sm"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    <span></span> <b class="caret"></b>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-3 pull-left">
                            <div class="">
                                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                            </div>
                            <br/>
                        </div>

                        {!! Form::close() !!}
                    </div>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    {{-- <table style="width:100%;" id="attrition-absconding-report" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Region</th>
                                                <th>Total</th>
                                                <th>Resignation</th>
                                                <th>Absconding</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th>Total</th>
                                                <th>Resignation</th>
                                                <th>Absconding</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>

                                    </table> --}}

                                    <table style="width:100%;" id="attrition-reports-tbl" class="table table-striped">
                                        <thead class="primary">
                                            <tr>
                                                @php
                                                    $total_fields = '';
                                                @endphp
                                                <th>Months</th>

                                                @foreach ($regions as $key =>$item)
                                                {{-- {{url('admin/hiring-report/store/')}}/{{$item->id}} --}}
                                                    <th colspan="2" class="#clickable-row" data-href="">{{$item->name}}</th>
                                                    @php
                                                        $total_fields .='<th>Absconding</th><th>Resignation</th>';
                                                    @endphp
                                                    @endforeach
                                                </tr>
                                            <tr>
                                                <th></th>
                                            {!!$total_fields!!}
                                            </tr>
                                        </thead>
                                        <tbody class="no-border-x">
                                            @foreach($inmonths as $months)
                                            <tr>
                                                <td>
                                                        {{date('M-Y',strtotime('01-'.$months))}}
                                                </td>
                                                @foreach ($regions as $reg)
                                                    @php
                                                        $month_attrition_report = $attritionReport[$months][$reg->id];
                                                    @endphp
                                                    <td>{{($month_attrition_report) ? $month_attrition_report->absconding_total : 0 }}</td>
                                                    <td>{{($month_attrition_report) ? $month_attrition_report->resignation_total : 0 }}</td>
                                                @endforeach

                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default panel-table">

                    <div class="panel-heading panel-heading-divider clearfix">Attrition Report - Region (YTD) <small>{{ date('M/Y',strtotime($start_date))}} - {{ date('M/Y',strtotime($end_date))}}</small>
                        {{-- <a href="{{url('admin')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning">Go Back</button>
                        </a> --}}
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="attrition-absconding-report" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Region</th>
                                                <th>Total</th>
                                                <th>Resignation</th>
                                                <th>Absconding</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th>Total</th>
                                                <th>Resignation</th>
                                                <th>Absconding</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Attrition report - By Aging (YTD) <small>{{ date('M/Y',strtotime($start_date))}} - {{ date('M/Y',strtotime($end_date))}}</small> </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table id="aging-resonal" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Region Name</th>
                                                <th>Total</th>
                                                <th>Infant attrition Within month</th>
                                                <th>Baby attrition Within a quarter</th>
                                                <th>Attrition</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th>Total</th>
                                                <th>Infant attrition Within month</th>
                                                <th>Baby attrition Within a quarter</th>
                                                <th>Attrition</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Attrition report - By percetage of live manpower (MTD) <span class="panel-subtitle">{{date('M-Y')}}</span></div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="chart-table xs-pt-15">
                                    <div class="table-responsive table-minw-fixed table-hiring1">
                                        <table id="attrition-report-by-aging" class="table table-striped table-hover table-fw-widget">
                                            <thead>
                                                <tr>
                                                    <th>Region</th>
                                                    <th>Actual Man Power</th>
                                                    <th>Attrition Total</th>
                                                    <th>Percentage</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($manpowerPercentageReport) > 0)
                                                    @foreach ($manpowerPercentageReport as $item)
                                                        <tr class="clickable-row" data-href="{{url('admin/man-power-percentage')}}/{{$item['region_id']}}">
                                                            <td>{{$item['region_name']}}</td>
                                                            <td>{{$item['total_actual_man_power']}}</td>
                                                            <td>{{$item['Total']}}</td>
                                                            <td>
                                                                @if ($item['total_actual_man_power'] > 0 && $item['Total'] > 0)
                                                                    {{
                                                                        round(($item['Total']/$item['total_actual_man_power'])*100,2)
                                                                    }} %
																@else
																	0
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Total</th>
                                                    <th>Actual Man Power</th>
                                                    <th>Attrition Total</th>
                                                    <th>Percentage</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>

    var start =moment().startOf('year').add(3,'month');//moment.utc('2015-01-01','YYYY-MM-DD');
    var end = moment().endOf('year').add(3,'month');
    @if(\Request::has('start_date') && \Request::has('end_date'))
     start = moment.utc("{{\Request::get('start_date')}}",'YYYY-MM-DD');
     end = moment.utc("{{\Request::get('end_date')}}",'YYYY-MM-DD');
    @endif

    $("#attrition-reports-tbl").DataTable({
         dom: 'Blfrtip',
         buttons: ['excel'],
         "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
         "pageLength": 25,
         "order": false,
    });
    var url ="{{ url('/admin/attrition-report/region-data') }}";
    var view_url = "{{ url('/admin/attrition-report/store/') }}";
    var auth_check = "{{ Auth::check() }}";

    datatableAbsocnding = $('#attrition-absconding-report').DataTable({
        dom: 'Blfrtip',  buttons: ['excel'],
        processing: true,
        serverSide: true,
        "caseInsensitive": false,
        "order": [[0,"desc"]],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
		 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
        ajax: {
            url:url,
            type:"get",
            "data": {
                "start_date":start.format('YYYY-MM-DD'),
                "end_date":  end.format('YYYY-MM-DD')
            }
        },
        "drawCallback": function( settings ) {
            statusChange();
        },
        columns: [
            { data: 'name',name : 'name',"searchable": true, "orderable": false, "width":200},
            { data: 'total',name : 'total',"searchable": true, "orderable": false, "width":150},
            { data: 'total_resignation',name : 'total_resignation',"searchable": true, "orderable": false, "width":150},
            { data: 'total_absconding',name : 'total_absconding',"searchable": true, "orderable": false, "width":100},
            {
                "data": null,
                "searchable": false,
                "orderable": false,
                "width":150,
                "render": function (o) {
                    var v="";
                    v = "<a href='"+view_url+"/"+o.region_id+"?start_date="+start.format('YYYY-MM-DD')+"&end_date="+end.format('YYYY-MM-DD')+"' value="+o.region_id+" data-id="+o.region_id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";
                    return v;
                }

            }
        ],
        "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 1 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                resignation = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                abscounding = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                $( api.column( 1 ).footer() ).html( total );
                $( api.column( 2 ).footer() ).html( resignation );
                $( api.column( 3 ).footer() ).html( abscounding );

            }

    });

    var url ="{{ url('/admin/aging-report/region-data') }}";
    var edit_url = "{{ url('admin/aging-report/region') }}";
    var auth_check = "{{ Auth::check() }}";
    datatable2 = $('#aging-resonal').DataTable({
            dom: 'Blfrtip', buttons: ['excel'],
            processing: true,
            serverSide: true,
            searching: true,
            info: true,
            "bLengthChange": true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
                "data": {
                    "start_date":start.format('YYYY-MM-DD'),
                    "end_date":  end.format('YYYY-MM-DD')
                }
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                {data: 'region_name',name : 'region_name',"searchable": true, "orderable": false, "width":200},
                { data: 'total',name : 'total',"searchable": true, "orderable": false, "width":200},
                { data: 'infant_attrition',name : 'infant_attrition',"searchable": true, "orderable": false, "width":150},
                { data: 'baby_attrition',name : 'baby_attrition',"searchable": true, "orderable": false, "width":150},
                { data: 'attrition',name : 'attrition',"searchable": true, "orderable": false, "width":100},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var v="";
                        v = "<a href='"+edit_url+"/"+o.region_id+"?start_date="+start.format('YYYY-MM-DD')+"&end_date="+end.format('YYYY-MM-DD')+"' value="+o.region_id+" data-id="+o.region_id+"><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                         return v;
                    }
                }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total = api
                    .column( 1 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                infant = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                baby = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                attrition = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                $( api.column( 1 ).footer() ).html( total );
                $( api.column( 2 ).footer() ).html( infant );
                $( api.column( 3 ).footer() ).html( baby );
                $( api.column( 4 ).footer() ).html( attrition );

            }
        });

    $("#attrition-report-by-aging").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            actual = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            attrition = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            per = 0;
            if( actual > 0 && attrition > 0){
                per = (attrition * 100)  / actual;
            }

            $( api.column( 1 ).footer() ).html( actual );
            $( api.column( 2 ).footer() ).html( attrition );
            $( api.column( 3 ).footer() ).html( '-' );  /* per.toFixed(2) */


        }

        } );
    $(".clickable-row").click(function() { window.location = $(this).data("href"); });
      var range_start ="";
    var range_end ="";



		/*************************daterange selection*****************/



    //var start = moment().subtract(1, 'days');//moment.utc('2015-01-01','YYYY-MM-DD');
    //var end = moment().subtract(1, 'days');

    

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');


        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');

           
        }

		$(".input_start_date").val(range_start);
		$(".input_end_date").val(range_end);
        

    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "Current":[start,end],
            "@lang('comman.daterange.lastyear')": [moment().subtract(1, 'year').startOf('year').add(3,'month'), moment().subtract(1, 'year').endOf('year').add(3,'month')]
        }
    }, cb);

     cb(start, end);

</script>
@endsection