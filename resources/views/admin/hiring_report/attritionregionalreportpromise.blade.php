@extends('layouts.backend')
@section('title',"Promise Attrition Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-sm-12">
                <div class="panel panel-default panel-table">

                    <div class="panel-heading panel-heading-divider clearfix">Promise Report - Region 
                        <a href="{{url('admin')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning">Go Back</button>
                        </a>
                    </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="chart-table xs-pt-15">
                                    <div class="table-responsive table-minw-fixed table-hiring1">
                                        <table style="width:100%;" id="attrition-report-by-aging" class="table table-striped table-hover table-fw-widget">
                                            <thead>
                                                <tr>
                                                    <th>Region</th>
                                                    <th>Total</th>
                                                    <th>Absconding</th>
                                                    <th>Resigned</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($manpowerPercentageReport) > 0)
                                                    @foreach ($manpowerPercentageReport as $item)
                                                        <tr class="clickable-row" data-href="{{url('admin/attrition-report-promise/regionwise')}}/{{$item['region_id']}}">
                                                            <td>{{$item['region_name']}}</td>
                                                            <td>{{$item['Total']}}</td>
                                                            <td>
                                                                {{$item['promiseAttritionTotalAbsconding']}}
                                                            </td>
                                                            <td>
                                                                {{$item['promiseAttritionTotalResigned']}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Total</th>
                                                    <th>Actual Man Power</th>
                                                    <th>Attrition Total</th>
                                                    <th>Percentage</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    // var url ="{{ url('/admin/attrition-report/region-data') }}";
    // var view_url = "{{ url('/admin/attrition-report/store/') }}";
    // var auth_check = "{{ Auth::check() }}";

    // datatable = $('#attrition-absconding-report').DataTable({
    //     dom: 'Blfrtip',  buttons: ['excel'],
    //     processing: true,
    //     serverSide: true,
    //     "caseInsensitive": false,
    //     "order": [[0,"desc"]],
    //     "pageLength": 25,
    //     "scrollY": 480, "scrollX": true,
	// 	 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
    //     ajax: {
    //         url:url,
    //         type:"get",
    //     },
    //     "drawCallback": function( settings ) {
    //         statusChange();
    //     },
    //     columns: [
    //         { data: 'name',name : 'name',"searchable": true, "orderable": false, "width":200},
    //         { data: 'total',name : 'total',"searchable": true, "orderable": false, "width":150},
    //         { data: 'total_absconding',name : 'total_absconding',"searchable": true, "orderable": false, "width":150},
    //         { data: 'total_resignation',name : 'total_resignation',"searchable": true, "orderable": false, "width":100},
    //         {
    //             "data": null,
    //             "searchable": false,
    //             "orderable": false,
    //             "width":150,
    //             "render": function (o) {
    //                 var v="";
    //                 v = "<a href='"+view_url+"/"+o.region_id+"' value="+o.region_id+" data-id="+o.region_id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";
    //                 return v;
    //             }

    //         }
    //     ],
    //     "footerCallback": function ( row, data, start, end, display ) {
    //             var api = this.api(), data;

    //             // Remove the formatting to get integer data for summation
    //             var intVal = function ( i ) {
    //                 return typeof i === 'string' ?
    //                     i.replace(/[\$,]/g, '')*1 :
    //                     typeof i === 'number' ?
    //                         i : 0;
    //             };

    //             // Total over all pages
    //             total = api
    //                 .column( 1 )
    //                 .data()
    //                 .reduce( function (a, b) {
    //                     return intVal(a) + intVal(b);
    //                 }, 0 );
    //             resignation = api
    //                 .column( 2 )
    //                 .data()
    //                 .reduce( function (a, b) {
    //                     return intVal(a) + intVal(b);
    //                 }, 0 );
    //             abscounding = api
    //                 .column( 3 )
    //                 .data()
    //                 .reduce( function (a, b) {
    //                     return intVal(a) + intVal(b);
    //                 }, 0 );

    //             $( api.column( 1 ).footer() ).html( total );
    //             $( api.column( 2 ).footer() ).html( resignation );
    //             $( api.column( 3 ).footer() ).html( abscounding );

    //         }

    // });

    // var url ="{{ url('/admin/aging-report/region-data') }}";
    // var edit_url = "{{ url('admin/aging-report/region') }}";
    // var auth_check = "{{ Auth::check() }}";
    // datatable2 = $('#aging-resonal').DataTable({
    //         dom: 'Blfrtip', buttons: ['excel'],
    //         processing: true,
    //         serverSide: true,
    //         searching: true,
    //         info: true,
    //         "bLengthChange": true,
    //         "caseInsensitive": false,
    //         "order": [[0,"desc"]],
	// 		 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
    //         "pageLength": 25,
    //         "scrollY": 480, "scrollX": true,
    //         ajax: {
    //             url:url,
    //             type:"get",
    //         },
    //         "drawCallback": function( settings ) {
    //             statusChange();
    //         },
    //         columns: [
    //             {data: 'region_name',name : 'region_name',"searchable": true, "orderable": false, "width":200},
    //             { data: 'total',name : 'total',"searchable": true, "orderable": false, "width":200},
    //             { data: 'infant_attrition',name : 'infant_attrition',"searchable": true, "orderable": false, "width":150},
    //             { data: 'baby_attrition',name : 'baby_attrition',"searchable": true, "orderable": false, "width":150},
    //             { data: 'attrition',name : 'attrition',"searchable": true, "orderable": false, "width":100},
    //             {
    //                 "data": null,
    //                 "searchable": false,
    //                 "orderable": false,
    //                 "width":150,
    //                 "render": function (o) {
    //                     var v="";
    //                     v = "<a href='"+edit_url+"/"+o.region_id+"' value="+o.region_id+" data-id="+o.region_id+"><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

    //                      return v;
    //                 }
    //             }
    //         ],
    //         "footerCallback": function ( row, data, start, end, display ) {
    //             var api = this.api(), data;

    //             // Remove the formatting to get integer data for summation
    //             var intVal = function ( i ) {
    //                 return typeof i === 'string' ?
    //                     i.replace(/[\$,]/g, '')*1 :
    //                     typeof i === 'number' ?
    //                         i : 0;
    //             };

    //             // Total over all pages
    //             total = api
    //                 .column( 1 )
    //                 .data()
    //                 .reduce( function (a, b) {
    //                     return intVal(a) + intVal(b);
    //                 }, 0 );
    //             infant = api
    //                 .column( 2 )
    //                 .data()
    //                 .reduce( function (a, b) {
    //                     return intVal(a) + intVal(b);
    //                 }, 0 );
    //             baby = api
    //                 .column( 3 )
    //                 .data()
    //                 .reduce( function (a, b) {
    //                     return intVal(a) + intVal(b);
    //                 }, 0 );
    //             attrition = api
    //                 .column( 3 )
    //                 .data()
    //                 .reduce( function (a, b) {
    //                     return intVal(a) + intVal(b);
    //                 }, 0 );

    //             $( api.column( 1 ).footer() ).html( total );
    //             $( api.column( 2 ).footer() ).html( infant );
    //             $( api.column( 3 ).footer() ).html( baby );
    //             $( api.column( 4 ).footer() ).html( attrition );

    //         }
    //     });

    $("#attrition-report-by-aging").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            absconding = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            resigned = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            per = 0;
            
            $( api.column( 1 ).footer() ).html( total );
            $( api.column( 2 ).footer() ).html( absconding );
            $( api.column( 3 ).footer() ).html( resigned );  /* per.toFixed(2) */


        }

        } );
    $(".clickable-row").click(function() { window.location = $(this).data("href"); });

</script>
@endsection