@extends('layouts.backend')
@section('title',"Hiring Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Hiring - Inprocess Report- Region
                        <a href="{{url('admin')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning mxs-mt-10">Go Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                    <div class="col-md-12">
                        <div class="chart-table xs-pt-15">
                            <div class="table-responsive table-minw-fixed table-hiring1">
                                <table style="width:100%;" id="inprocess-resonal" class="table table-striped table-hover table-fw-widget">
                                    <thead>
                                        <tr>
                                            <th>Region Name</th>
                                            <th>Pending at Store</th>
                                            <th>Pending at RHR</th>
                                            <th>Peding at HRMS</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Total</th>
                                            <th>Pending at Store</th>
                                            <th>Pending at RHR</th>
                                            <th>Peding at HRMS</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js" type="text/javascript"></script>
<script>
    var url ="{{url('/admin/hiring-inprocess-data')}}";
        var edit_url = "{{url('/admin/hiring-inprocess/store')}}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#inprocess-resonal').DataTable({
			dom: 'Blfrtip',
			buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },

            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'region_name',name : 'region_name',"searchable": true, "orderable": false, "width":200},
                { data: 'store_pending',name : 'store_pending',"searchable": true, "orderable": false, "width":200},
                { data: 'region_pending',name : 'region_pending',"searchable": true, "orderable": false, "width":150},
                { data: 'hrms_pending',name : 'hrms_pending',"searchable": true, "orderable": false, "width":150},
              //  { data: 'actual_man_power',name : 'actual_man_power',"searchable": true, "orderable": false, "width":100},
              //  { data: 'gap',name : 'gap',"searchable": true, "orderable": false, "width":100},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var v="";
                        v = "<a href='"+edit_url+"/"+o.region_id+"' value="+o.region_id+" data-id="+o.region_id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";
                        return v;
                    }

                }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                total_pending_store = api
                    .column( 1 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                total_pending_region = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                total_pending_hrms = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                $( api.column( 1 ).footer() ).html( total_pending_store );
                $( api.column( 2 ).footer() ).html( total_pending_region );
                $( api.column( 3 ).footer() ).html( total_pending_hrms );  /* avg.toFixed(2) */

            }
        });

</script>
@endsection