@extends('layouts.backend')
@section('content')
<style>
    .clickable-row {
        cursor: pointer
    }
</style>
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">
                        <div class="tools"></div><span class="title">Hiring Report - {{$region_name}}</span>
                        <a href="{{url('admin/hiring-report/region')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary">Go Back</button>
                        </a>
                        <span class="panel-subtitle"></span>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <table style="width:100%;" id="store-report-data" class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Store Code</th>
                                            <th>Store Name</th>
                                            <th>GAP</th>
                                            <th>In Process</th>
                                            <th>Hired</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if (count($hiringReports) > 0)
                                            @foreach ($hiringReports as $hiring)
                                                <tr class="clickable-row" data-href="{{url('admin/hiring-pending-employee/store/')}}/{{$hiring['store_id']}}">
                                                    <td>{{$hiring['io_code']}}</td>
                                                    <td>{{$hiring['store_name']}}</td>
                                                    <td>{{(isset($hiring['gap'] )) ? $hiring['gap'] : 0 }}</td>
                                                    <td>{{(isset($hiring['in_process'] )) ? $hiring['in_process'] : 0 }}</td>
                                                    <td>{{(isset($hiring['hired'] )) ? $hiring['hired'] : 0 }}</td>
                                                </tr>
                                            @endforeach
                                        @endif

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Total</th>
                                            <th>GAP</th>
                                            <th>In Process</th>
                                            <th>Hired</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
    $("#store-report-data").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'],
        "pageLength": 25,
        "scrollY": true, "scrollX": true,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_gap = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_in_process = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_hired = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total_gap );
            $( api.column( 3 ).footer() ).html( total_in_process );
            $( api.column( 4 ).footer() ).html( total_hired );

        }

    });

</script>
@endsection