@extends('layouts.backend')
@section('title',"Hiring Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Attrition Report - By Reason of Attrition
                        <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning">Go Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="attrition-region-report" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Store Name</th>
                                                <th>Store Code</th>
                                                @if ($reasons)
                                                    @foreach ($reasons as $reason)
                                                        <th>{{$reason->reason}}</th>
                                                    @endforeach
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($report_data) > 0)

                                                @foreach ($report_data as $report)
                                                    <tr>
                                                        <td>{{$report['store_name']}}</td>
                                                        <td>{{$report['io_code']}}</td>
                                                        @foreach ($reasons as $reason)
                                                            <td>{{$report[$reason->id]}}</td>
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td>
                                                        No Data Found
                                                    </td>
                                                </tr>
                                            @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
$("#attrition-region-report").DataTable({
    dom: 'Blfrtip',
     buttons: ['excel'],
     "pageLength": 25,
     "scrollY": 480, "scrollX": true,
     "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]], });
</script>
@endsection