@extends('layouts.backend')
@section('title',"Attrition Age report - Store")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Attrition Age report - Store
                        <a href="{{url('admin/attrition-report/region')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning">Go Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="aging-resonal" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Store Code</th>
                                                <th>Store Name</th>
                                                <th>Total</th>
                                                <th>Infant attrition Within month</th>
                                                <th>Baby attrition Within a quarter</th>
                                                <th>Attrition</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Total</th>
                                                <th>Infant attrition Within month</th>
                                                <th>Baby attrition Within a quarter</th>
                                                <th>Attrition</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('/admin/aging-report/region-data/')}}/{{request()->route('region_id')}}";
        var edit_url = "{{ url('/admin/man-power') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#aging-resonal').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'io_code',name : 'io_code',"searchable": true, "orderable": false, "width":200},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false, "width":200},
                { data: 'total',name : 'total',"searchable": true, "orderable": false, "width":200},
                { data: 'infant_attrition',name : 'infant_attrition',"searchable": true, "orderable": false, "width":150},
                { data: 'baby_attrition',name : 'baby_attrition',"searchable": true, "orderable": false, "width":150},
                { data: 'attrition',name : 'attrition',"searchable": true, "orderable": false, "width":100},
              //  { data: 'actual_man_power',name : 'actual_man_power',"searchable": true, "orderable": false, "width":100},
              //  { data: 'gap',name : 'gap',"searchable": true, "orderable": false, "width":100},
             /*   {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                 //   "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='mdi mdi-edit'></i></a>&nbsp;";

                        /* d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/man-power')}} data-msg='Variable Pay Type' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";
                        return v+e;
                    }

                } */
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            infant = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            baby = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            attrition = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total );
            $( api.column( 3 ).footer() ).html( infant );
            $( api.column( 4 ).footer() ).html( baby );
            $( api.column( 5 ).footer() ).html( attrition );


        }
        });

</script>
@endsection