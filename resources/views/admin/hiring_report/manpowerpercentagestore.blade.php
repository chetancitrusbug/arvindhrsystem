@extends('layouts.backend')
@section('title',"Hiring Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Store Report
                        <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary btn-back">Go Back</button>
                        </a>
                    </div>
                    <div class="panel-heading">Attrition report - By percetage of live manpower ({{$store_name}} - {{$region_name}})</div>
                    <div class="panel-body">
                        <table style="width:100%;" id="man-power-percentage-report-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Store Name</th>
                                    <th>Total Man Power</th>
                                    <th>Total Attrition</th>
                                    <th>Percentage</th>
                                </tr>
                            </thead>
                            @if ($manpowerPercentageReport)
                            <tbody>
                                @foreach ($manpowerPercentageReport as $item)
                                <tr>
                                    <td>{{$item->store_name}}</td>
                                    <td>{{$item->total_actual_man_power}}</td>
                                    <td>{{$item->Total}}</td>
                                    <td>
                                        @if ($item->total_actual_man_power > 0 && $item->Total > 0)
                                            @php
                                                echo round(($item->total_actual_man_power/$item->Total)*100,2);
                                            @endphp %
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            @else
                            <tr>
                                <td>
                                    No Data Found
                                </td>
                            </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });

$("#man-power-percentage-report-table").DataTable({
    dom: 'Blfrtip',
    buttons: ['excel'],
    "pageLength": 25,
    "scrollY": 480, "scrollX": true,
    "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
});
</script>
@endsection