@extends('layouts.backend')
@section('title',"Hiring Report Pending Employee")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Hiring Report Pending Employee of ({{$store->store_name}})
                        <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning">Go Back</button>
                        </a>
                        <!--  <span class="panel-subtitle">
                            <a href="{{ url('admin/man-power-upload') }}" title="Create" data-url="" class="btn btn-space btn-primary">
                                Excel Upload
                            </a>
                        </span> -->
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Employee Name</th>
                                                <th>Position</th>
                                                <th>Designation Name</th>
                                                <th>Action</th>
                                                <!--<th>Actual Man Power</th>
                                                <th>Gap</th>
                                                <th>Action</th>  -->
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('admin/hiring-pending-employee/store-data') }}/{{request()->route('store_id')}}";
        var edit_url = "{{ url('/admin/hiring-pending-employee/view/') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#hiring-report-table').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false, "width":200},
                { data: 'position',name : 'position',"searchable": true, "orderable": false, "width":150},
                { data: 'designation_name',name : 'designation_name',"searchable": true, "orderable": false, "width":150},
              //  { data: 'actual_man_power',name : 'actual_man_power',"searchable": true, "orderable": false, "width":100},
              //  { data: 'gap',name : 'gap',"searchable": true, "orderable": false, "width":100},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";
                        /*
                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='mdi mdi-edit'></i></a>&nbsp;"; */

                        return v;
                        /* d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/man-power')}} data-msg='Variable Pay Type' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";
                        return v+e; */
                    }

                }
            ]
        });

</script>
@endsection