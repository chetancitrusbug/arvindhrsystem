@extends('layouts.backend')
@section('title',"Attrition Age report - Region")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Attrition Age report - Region
                        <a href="{{url('admin')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary">Go Back</button>
                        </a>

                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="aging-resonal" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Region Name</th>
                                    <th>Total</th>
                                    <th>Infant attrition Within month</th>
                                    <th>Baby attrition Within a quarter</th>
                                    <th>Attrition</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
        var url ="{{ url('/admin/aging-report/region-data') }}";
        var edit_url = "{{ url('admin/aging-report/region') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#aging-resonal').DataTable({

            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'region_name',name : 'region_name',"searchable": true, "orderable": false, "width":200},
                { data: 'total',name : 'total',"searchable": true, "orderable": false, "width":200},
                { data: 'infant_attrition',name : 'infant_attrition',"searchable": true, "orderable": false, "width":150},
                { data: 'baby_attrition',name : 'baby_attrition',"searchable": true, "orderable": false, "width":150},
                { data: 'attrition',name : 'attrition',"searchable": true, "orderable": false, "width":100},
              //  { data: 'actual_man_power',name : 'actual_man_power',"searchable": true, "orderable": false, "width":100},
              //  { data: 'gap',name : 'gap',"searchable": true, "orderable": false, "width":100},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.region_id+"' value="+o.region_id+" data-id="+o.region_id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='mdi mdi-edit'></i></a>&nbsp;";

                        /* d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/man-power')}} data-msg='Variable Pay Type' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;"; */
                        return v;
                    }

                }
            ]
        });

</script>
@endsection