@extends('layouts.backend')
@section('title',"Hiring Source Reports")
@section('content')
<style>
    .clickable-row {
        cursor: pointer
    }
</style>
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">
                        <div class="tools"></div><span class="title">Hiring Sorce Report - Region</span>
                        <a href="{{url('admin/hiring-source-report')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning">Go Back</button>
                        </a>
                        <span class="panel-subtitle"></span>
                    </div>
                    <div class="panel-body" style="overflow-x: scroll;">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <table style="width:100%;" id="hiring-source" class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Store Code</th>
                                            <th>Store Name</th>
                                            @foreach ($sourceCategories as $sourceCategory)
                                            <th>
                                                {{$sourceCategory->category}}
                                            </th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if($hiringSourceRequirements)
                                             @foreach ($hiringSourceRequirements as $hiringSourceRequirement)
                                        <tr>
                                            <td>{{$hiringSourceRequirement['io_code']}}</td>
                                            <td>{{$hiringSourceRequirement['store_name']}}</td>
                                                @foreach ($sourceCategories as $sourceCategory)
                                                    <td>{!!($hiringSourceRequirement[$sourceCategory->id] > 0)? $hiringSourceRequirement[$sourceCategory->id] : 0 !!}</td>
                                            @endforeach
                                        </tr>
                                        @endforeach @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
    $("#hiring-source").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
    });
</script>
@endsection