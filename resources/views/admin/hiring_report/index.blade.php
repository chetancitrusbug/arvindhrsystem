@extends('layouts.backend')
@section('title',"Hiring Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Hiring Report
                        <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary btn-back">Go Back</button>
                        </a>
                      <!--  <span class="panel-subtitle">
                            <a href="{{ url('admin/man-power-upload') }}" title="Create" data-url="" class="btn btn-space btn-primary">
                                Excel Upload
                            </a>
                        </span> -->
                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="hiring-report-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Month</th>
                                    <th>GAP</th>
                                    <th>Hired</th>
                                    <th>Pending</th>
                                    <!--<th>Actual Man Power</th>
                                    <th>Gap</th>
                                    <th>Action</th>  -->
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('/admin/hiring-report-data') }}";
        var edit_url = "{{ url('/admin/man-power') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#hiring-report-table').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": false,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'monthyear',name : 'monthyear',"searchable": true, "orderable": false, "width":200},
                { data: 'gap_g',name : 'gap_g',"searchable": true, "orderable": false, "width":150},
                { data: 'hired_h',name : 'hired_h',"searchable": true, "orderable": false, "width":150},
                { data: 'pending_p',name : 'pending_p',"searchable": true, "orderable": false, "width":100},
              //  { data: 'actual_man_power',name : 'actual_man_power',"searchable": true, "orderable": false, "width":100},
              //  { data: 'gap',name : 'gap',"searchable": true, "orderable": false, "width":100},
             /*   {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                 //   "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" class='btn btn-info btn-sm'><i class='mdi mdi-edit'></i></a>&nbsp;";

                        /* d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('admin/man-power')}} data-msg='Variable Pay Type' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";
                        return v+e;
                    }

                } */
            ]
        });

</script>
@endsection