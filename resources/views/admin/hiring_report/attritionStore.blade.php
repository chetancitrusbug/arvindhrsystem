@extends('layouts.backend')
@section('title',"Hiring Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Attrition Report - {{$totalattritionReport->store_name}} ({{$totalattritionReport->name}})
                        <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary btn-back">Go Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="attrition-reports" class="table table-striped">
                            <thead class="primary">
                                <tr>
                                    <th>Total</th>
                                    <th>Resignation</th>
                                    <th>Absconding</th>
                                    <th>Actual Man Power</th>
                                    <th>Leaving in % (Percentages)</th>
                                </tr>
                            </thead>
                            @if($totalattritionReport)
                                <tbody class="no-border-x">
                                    <tr>
                                        <td>{{($totalattritionReport->Total > 0) ? $totalattritionReport->Total : 0 }}</td>
                                        <td>{{($totalattritionReport->resignation > 0) ? $totalattritionReport->resignation : 0 }}</td>
                                        <td>{{($totalattritionReport->absconding > 0) ? $totalattritionReport->absconding : 0 }}</td>
                                        <td>{{($totalattritionReport->actual_man_power > 0) ? $totalattritionReport->actual_man_power : 0 }}</td>
                                        <td>
                                            @if ($totalattritionReport->actual_man_power > 0 && $totalattritionReport->Total > 0)
                                                @php
                                                    echo ($totalattritionReport->actual_man_power/$totalattritionReport->Total)*100;
                                                @endphp
                                                %
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            @else
                                <tr>
                                    <td>
                                        No Data Found
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#attrition-reports").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]], });
</script>
@endsection