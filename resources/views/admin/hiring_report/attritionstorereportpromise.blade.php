@extends('layouts.backend')
@section('title',"Promise Attrition Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-sm-12">
                <div class="panel panel-default panel-table">

                <div class="panel-heading panel-heading-divider clearfix">Promise Report - {{$regions->name}}
                        <a href="{{url('admin/attrition-report-promise/region')}}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-warning">Go Back</button>
                        </a>
                    </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="chart-table xs-pt-15">
                                    <div class="table-responsive table-minw-fixed table-hiring1">
                                        <table style="width:100%;" id="attrition-report-by-aging" class="table table-striped table-hover table-fw-widget">
                                            <thead>
                                                <tr>
                                                    <th>Store Name</th>
                                                    <th>Io Code</th>
                                                    <th>Total</th>
                                                    <th>Absconding</th>
                                                    <th>Resigned</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($manpowerPercentageReport) > 0)
                                                    @foreach ($manpowerPercentageReport as $item)
                                                        <tr class="">
                                                            <td>{{$item['store_name']}}</td>
                                                            <td>{{$item['io_code']}}</td>
                                                            <td>{{$item['Total']}}</td>
                                                            <td>
                                                                {{$item['absconding']}}
                                                            </td>
                                                            <td>
                                                                {{$item['resigned']}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th>Total</th>
                                                    <th>Actual Man Power</th>
                                                    <th>Attrition Total</th>
                                                    <th>Percentage</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    
    $("#attrition-report-by-aging").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            absconding = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            resigned = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            per = 0;
            
            $( api.column( 2 ).footer() ).html( total );
            $( api.column( 3 ).footer() ).html( absconding );
            $( api.column( 4 ).footer() ).html( resigned );  /* per.toFixed(2) */


        }

        } );
    //$(".clickable-row").click(function() { window.location = $(this).data("href"); });

</script>
@endsection