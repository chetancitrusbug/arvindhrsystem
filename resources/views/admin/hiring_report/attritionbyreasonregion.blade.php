@extends('layouts.backend')
@section('title',"Hiring Reports")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Attrition Report - By Reason of Attrition
                        <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                                                    <button class="btn btn-space btn-warning">Go Back</button>
                                                </a>
                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="attrition-region-report" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Region</th>
                                    @if ($reasons) @foreach ($reasons as $reason)
                                    <th>{{$reason->reason}}</th>
                                    @endforeach @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($report_data as $report)
                                <tr>
                                    <td>{{$report['store_name']}}</td>
                                    @foreach ($reasons as $reason)
                                    <td>{{$report[$reason->id]}}</td>
                                    @endforeach
                                </tr>
                                @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$("#attrition-region-report").DataTable({
    dom: 'Blfrtip',
    buttons: ['excel'],
    "pageLength": 25,
    "scrollY": 480, "scrollX": true,
    "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],});
</script>
@endsection