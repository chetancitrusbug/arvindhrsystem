@extends('layouts.backend')
@section('content')
<style>
    .clickable-row {
        cursor: pointer
    }
</style>
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">
                        <div class="tools"></div><span class="title">Hiring Report - {{$store_name}}</span>
                        <a href="{{ url()->previous() }}" class="pull-right" title="Go Back">
                            <button class="btn btn-space btn-primary">Go Back</button>
                        </a>
                        <span class="panel-subtitle"></span>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <table style="width:100%;" id="store-hiring-reports" class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Month Year</th>
                                            <th>GAP</th>
                                            <th>Hired</th>
                                            <th>Pending</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">

                                        @foreach ($hiringReport as $hiring)
                                        <tr>
                                            <td>
                                                @php
                                                    echo date('M-Y',strtotime('01-'.$hiring->monthyear));
                                                @endphp
                                            </td>
                                            <td>{{($hiring->gap > 0) ? $hiring->gap : 0 }}</td>
                                            <td>{{($hiring->hired > 0) ? $hiring->hired : 0 }}</td>
                                            <td>{{($hiring->pending > 0) ? $hiring->pending : 0 }}</td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
    $("#store-hiring-reports").DataTable({
        dom: 'Blfrtip',
          buttons: ['excel'],
           "pageLength": 25,
           "scrollY": true, "scrollX": true,
         });

</script>
@endsection
