@extends('layouts.backend')
@section('title',"Additional Reports")
@section('content')
<style>
    .clickable-row {
        cursor: pointer
    }
</style>
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">
                        <div class="tools"></div><span class="title">Additional Report - Store</span>
                            <a href="{{ URL::previous() }}" class="pull-right" title="Go Back">
                                <button class="btn btn-space btn-primary">Go Back</button>
                            </a>
                    </div>
                    <div class="panel-body" style="overflow-x: scroll;">

                        <div class="panel-heading panel-heading-divider">
                            <div class="tools"></div><span class="title">Gender Wise Employee Report - Store</span><span class="panel-subtitle"></span>
                        </div>
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <table style="width:100%;" id="total-gender" class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Store Code</th>
                                            <th>Store Name</th>
                                            <th>Male</th>
                                            <th>Female</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if(count($genderwisetotalemployee) > 0 )
                                            @foreach ($genderwisetotalemployee as $gender)
                                                <tr>
                                                    <td>{{$gender['io_code']}}</td>
                                                    <td>{{$gender['store_name']}}</td>
                                                    <td>{!!($gender['male'] > 0)? $gender['male'] : 0 !!}</td>
                                                    <td>{!!($gender['female'] > 0)? $gender['female'] : 0 !!}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="panel-heading panel-heading-divider">
                            <div class="tools"></div><span class="title">Gender Wise Attrition Report - Store</span><span class="panel-subtitle"></span>
                        </div>
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <table style="width:100%;" id="attrition-gender" class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Store Code</th>
                                            <th>Store Name</th>
                                            <th>Male</th>
                                            <th>Female</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if(count($genderwiseattrition) > 0 ) @foreach ($genderwiseattrition as $gender)
                                        <tr>
                                            <td>{{$gender['io_code']}}</td>
                                            <td>{{$gender['store_name']}}</td>
                                            <td>{!!($gender['male'] > 0)? $gender['male'] : 0 !!}</td>
                                            <td>{!!($gender['female'] > 0)? $gender['female'] : 0 !!}</td>
                                        </tr>
                                        @endforeach @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="panel-heading panel-heading-divider">
                            <div class="tools"></div><span class="title">Gender Wise Hiring Report - Store</span><span class="panel-subtitle"></span>
                        </div>
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <table style="width:100%;" id="hiring-gender" class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Store Code</th>
                                            <th>Store Name</th>
                                            <th>Male</th>
                                            <th>Female</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if(count($genderwisehiring) > 0 )
                                            @foreach ($genderwisehiring as $gender)
                                                <tr>
                                                    <td>{{$gender['io_code']}}</td>
                                                    <td>{{$gender['store_name']}}</td>
                                                    <td>{!!($gender['male'] > 0)? $gender['male'] : 0 !!}</td>
                                                    <td>{!!($gender['female'] > 0)? $gender['female'] : 0 !!}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Total</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
    $("#hiring-gender").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 480,
        "scrollX": true,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_male = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_female = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 2 ).footer() ).html( total_male );
            $( api.column( 3 ).footer() ).html( total_female );


        }
        });
    $("#attrition-gender").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'], "pageLength": 25,
        "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
        "scrollY": 480,
        "scrollX": true,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_male = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_female = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 2 ).footer() ).html( total_male );
            $( api.column( 3 ).footer() ).html( total_female );


        }

    } );
    $("#total-gender").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'], "pageLength": 25, "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
        "scrollY": 480,
        "scrollX": true,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_male = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_female = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            $( api.column( 2 ).footer() ).html( total_male );
            $( api.column( 3 ).footer() ).html( total_female );


        }

        } );

</script>
@endsection