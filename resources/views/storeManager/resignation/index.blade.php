@extends('storeManager.layouts.backend')
@section('title',"Resignation")
@section('css')
    <style>
        .paging { padding-right: 20px; padding: 0px 10px !important; }
        #attendance-table { width:3000px;}
        .select{
            padding: 6px 2px;
            height: 30px;
            font-size: 12px;
            width: 55px;
        }
        .table-responsive tbody > tr > td {
            width: 100px;
        }
        .table-responsive tbody > tr > td:first-child{
            width: 200px;
        }
        .request-change{
            cursor: pointer;
        }
        #abs-table{
            width: 95%; margin: 0 auto;
            font-size: 10px;
        }
        .error{
            color: red;
        }
		#last_working_date{
			padding: 0px !important;
		}
		.employee_id{
			width: 85%; !important;
		}
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.resignation')
	@include('storeManager.modals.resignation-detail')
	@include('storeManager.modals.resignation-upload')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
                        <p class="panel-head1">Resignation List
                            <span class="panel-subtitle panel-sheading1">
                                <a href="#" title="Add">
                                    <button class="btn btn-space btn-success add_resignation btn-addfixed">Add</button>
                                </a>
                            </span>
                        </p>

                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1 list-table1">
                                    <table style="width:100%;" id="resignation-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Employee Name</th>
                                                <th>Employee Code</th>
                                                <th>Last Working Date</th>
                                                <th>Status</th>
                                                <th>Created</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
		 $("#last_working_date_div").datetimepicker({
			 minDate: new Date(),
             startDate: new Date(),
			 maxDate: 'today',
    	    autoclose: true,
    	    componentIcon: '.mdi.mdi-calendar',
        });
    $("#last_working_date_div_edit").datetimepicker({
			 minDate: new Date(),
             startDate: new Date(),
			 maxDate: 'today',
    	    autoclose: true,
    	    componentIcon: '.mdi.mdi-calendar',
        });
    $("#date_of_resignation").datetimepicker({
         minDate: new Date(),
         startDate: new Date(),
          maxDate: 'today',
          autoclose: true,
          componentIcon: '.mdi.mdi-calendar'
    });
    $("#date_of_resignation_edit").datetimepicker({
         minDate: new Date(),
         startDate: new Date(),
          maxDate: 'today',
          autoclose: true,
          componentIcon: '.mdi.mdi-calendar'
    });
    $(function() { $("#reason_id").change(function() {
        var reason_id = $('option:selected', this).val();
        var _token = $('meta[name="csrf-token"]').attr('content');
        if(reason_id != ''){
            $.ajax({
                url:"{{route('store_get_subcategory','reason_id')}}",
                method:'POST',
                data: {reason_id:reason_id,_token:_token},
                dataType: "json",
                success:function(result) {
                    if(result){
                        $("#sub_reason_id").empty();
                        console.log(result);
                        $("#sub_reason_id").append('<option value=""> == Select Sub Category ==</option>');
                        $.each(result, function(index, stateObj){
                            $("#sub_reason_id").append('<option value="'+stateObj.id+'">'+stateObj.reason+'</option>');
                            $("#sub_reason_id_edit").append('<option value="'+stateObj.id+'">'+stateObj.reason+'</option>');
                        });
                    }
                }
            });
        }
    });

    });
    $(function() { $("#reason_id_edit").change(function() {
        var reason_id = $('option:selected', this).val();
        var _token = $('meta[name="csrf-token"]').attr('content');
        if(reason_id != ''){
            $.ajax({
                url:"{{route('store_get_subcategory','reason_id')}}",
                method:'POST',
                data: {reason_id:reason_id,_token:_token},
                dataType: "json",
                success:function(result) {
                    if(result){
                        $("#sub_reason_id_edit").empty();
                        $("#sub_reason_id_edit").append('<option value=""> == Select Sub Category ==</option>');
                        $.each(result, function(index, stateObj){
                            $("#sub_reason_id_edit").append('<option value="'+stateObj.id+'">'+stateObj.reason+'</option>');
                        });
                    }
                }
            });
        }
    });
    });



        var url ="{{ url('/store/resignation-data') }}";
		var letter_url ="{{url('store/resignation-letter')}}";
        var remove_url = "{{ url('/store/remove-resignation') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#resignation-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480,
            "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                // statusChange();
            },
            columns: [
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
				{ data: 'last_working_date',name : 'last_working_date',"searchable": true, "orderable": true},
				{
					data: null,
					"searchable": true,
					"orderable": true,
					"render" : function (o){
						var status = o.resignation_status
						if(status == 'approved') return 'Approve';
						if(status == 'cancelled') return 'Cancel';
						if(status == 'complete') return 'Complete';
						if(status == 'decline') return 'Decline';
						if(status == 'documentation') return 'Documentation Required';
                        if(status == 'documentation-reg') return 'Documentation Processing To Regional';
						if(status == 'pending') return 'Pending';
						if(status == 'processing-hr') return 'Processing To Hrms';
					}
				},
				{ data: 'created_at',name : 'created_at',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var doc=""; li1=""; li2=""; li3=""; li4="";li5="";

						li4 = "<li><a  data-id="+o.id+" class='view-itema' title='View application'>View Detail <i class='icon mdi mdi-eye'></i> </a></li>";

                        li1 = "<li><a data-url='"+remove_url+"/"+o.id+"' data-id="+o.id+" class='del-itema' title='Remove from resignation'>Remove <i class='mdi mdi-delete'></i></a></li>";

						if(o.resignation_status != "pending" && o.resignation_status != "decline" && (o.islastday == 1 || o.islastday == "1") ){
						li2 = "<li><a href='"+letter_url+"/"+o.id+"' target='_blank' data-id="+o.id+" class='download-itema' title='Print Clearing Form'>Print Clearing Form <i class='icon mdi mdi-print'></i> </a></li>";
						}

						if(o.resignation_status == "documentation" || o.resignation_status == "approved" || o.resignation_status == "processing-hr" ){
						    li3 = "<li><a  href='#' data-status="+o.resignation_status+"  data-id="+o.id+" class='upload-itema' title='Upload Clearing Form'>Upload Clearing Form<i class='icon mdi mdi-upload'></i> </a></li>";
						}
                        if(o.resignation_status != "complete" && o.resignation_status !='cancelled' && o.resignation_status != 'approved'){
						    li5 = "<li><a  href='#' data-status="+o.resignation_status+"  data-id="+o.id+" class='edit-resume' title='Edit'>Edit Regignation <i class='icon mdi mdi-edit'></i> </a></li>";
						}


						doc = '<div class="btn-group btn-hspace"><button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Open <span class="icon-dropdown mdi mdi-chevron-down"></span></button><ul role="menu" class="dropdown-menu pull-right"> '+li4+li1+li2+li3+li5+'</ul></div>';

                        return doc;
                    }

                }
            ]
        });

        $('.add_resignation').on('click',function(e){
            $('select[name="employee_id"]').select2();
            $('#resignation').niftyModal();
        });
        $('select[name="employee_id"]').on('change',function(e){
            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: "{{url('store/getEmployeeDetail')}}",
                data: {id:$(this).val()},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data) {
                if(data.status == '200'){
                    data = data.data;
                    $('.emp_code').html(data.employee_code);
                    $('.io_code').html(data.store_location.io_code);
                    $('.emp_name').html(data.full_name);
                    $('.store_name').html(data.store_location.store_name);
                    $('.designation').html(data.designation.name);
                    $('.mobile').html(data.mobile);
                    $('.doj').html(data.joining_date);
                    $('.email').html(data.email);
                    $('.name').html(data.full_name);
                    $('.ac_no').html(data.account_no);
                    $('.bank_name').html(data.bank_name);
                    $('.ifsc_code').html(data.ifsc);
                    $('.address').html(data.permanent_address_line_1+', '+data.permanent_address_line_2+', '+data.permanent_address_line_3);
					$("#loading").hide();
					$('#resignation').niftyModal();
                }else if(data.status == '400'){
                    $('.error').html(data.msg);
                }

            })
            .fail(function(data) {
                $('button[type="submit"]').removeAttr('disabled');

            });
        });

        $(document).on('click', '.upload-itema', function (e) {
        	var id = $(this).attr('data-id');

			$.ajax({
				type: "get",
				url: "{{url('store/resignation')}}/"+id,
				success: function (result) {
					item = result.data.item;

					if(item){
						if(item.resignation_status == "documentation"){
							$(".submit_btn_block").show();
							$(".submit_notification_block").hide();
						}else{
							$(".submit_btn_block").hide();
							$(".submit_notification_block").show();
						}
						$('#resignation-upload').niftyModal();
						$("#hidden_res_id").val(id);

					}
				},
				error: function (xhr, status, error) {}
			});

		});


		$(document).on('click', '.edit-resume', function (e) {
        	var id = $(this).attr('data-id');
            //$('#resignation_edit').niftyModal();
			$.ajax({
				type: "get",
				url: "{{url('store/resignation')}}/"+id,
				success: function (result) {
					item = result.data.item;

					if(item){
						if(item.approve_by == 0){
                            data = result.data.employee;
                            $('#edit_resume_id').val(item.id);
                            $('.employee_id [value='+data.id+']').attr('selected', 'true').change();
                            $('.reason_id').val(item.reason_id).change();
                            setTimeout(() => {
                                $("#sub_reason_id_edit").val(item.sub_reason_id);
                            }, 1000);

                            $('#path_of_application_regignation').html(item.application_copy);


                            $('.last_working_date').val(item.last_working_date);
                            $('.date_of_resignation').val(item.date_of_resignation);
                         //   $("#employee_id").val(data.id).change();
                            $('.emp_code').html(data.employee_code);
                            $('.io_code').html(data.store_location.io_code);
                            $('.emp_name').html(data.full_name);
                            $('.store_name').html(data.store_location.store_name);
                            $('.designation').html(data.designation.name);
                            $('.mobile').html(data.mobile);
                            $('.doj').html(data.joining_date);
                            $('.email').html(data.email);
                            $('.name').html(data.full_name);
                            $('.ac_no').html(data.account_no);
                            $('.bank_name').html(data.bank_name);
                            $('.ifsc_code').html(data.ifsc);
                            $('.address').html(data.permanent_address_line_1+', '+data.permanent_address_line_2+', '+data.permanent_address_line_3);



                        //    $('#resignation-detail').niftyModal();
							$("#resignation_edit").niftyModal();

					//		$(".submit_notification_block").hide();
						}else{
							$("#resignation_edit").hide();
					//		$("submit_notification_block").show();
						}
                    //    $('#resignation_edit').niftyModal();
					//	$('#resignation-upload').niftyModal();
					//	$("#hidden_res_id").val(id);

					}
				},
				error: function (xhr, status, error) {}
			});

		});

		$(document).on('click', '.view-itema', function (e) {
        	var id = $(this).attr('data-id');

			$.ajax({
				type: "get",
				url: "{{url('store/resignation')}}/"+id,
				success: function (result) {
					//console.log(data);
					data = result.data.employee;
                    $('.emp_code').html(data.employee_code);
                    $('.io_code').html(data.store_location.io_code);
                    $('.emp_name').html(data.full_name);
                    $('.store_name').html(data.store_location.store_name);
                    $('.designation').html(data.designation.name);
                    $('.mobile').html(data.mobile);
                    $('.doj').html(data.joining_date);
                    $('.email').html(data.email);
                    $('.name').html(data.full_name);
                    $('.ac_no').html(data.account_no);
                    $('.bank_name').html(data.bank_name);
                    $('.ifsc_code').html(data.ifsc);
                    $('.address').html(data.permanent_address_line_1+', '+data.permanent_address_line_2+', '+data.permanent_address_line_3);


					$('#resignation-detail').niftyModal();

					item = result.data.item;

					if(item){
						if(item.application_url && item.application_url != ""){
							var application_url = "<a href='"+item.application_url+"' target='_blank'>Click to view employee application </a>";
							$('.application_url').html(application_url);
						}

						$('.application_status').html(item.resignation_status);
						$('.last_working_date_show').html(item.last_working_date);
						$('.note_from_store_manager').html(item.store_note);
						$('.note_from_regional_manager').html(item.regional_note);
						//$('.application_status').html(item.last_working_date);
					}
				},
				error: function (xhr, status, error) {

				}
			});


        });

		$(document).on('click', '.del-itema', function (e) {
			var id = $(this).attr('data-id');
			url = "{{url('/store/resignation')}}";
			var r = confirm("Are you sure to remove resignation application ?");
			if (r == true) {
				$.ajax({
					type: "DELETE",
					url: url + "/" + id,
					headers: {
						"X-CSRF-TOKEN": "{{ csrf_token() }}"
					},
					success: function (data) {
						location.reload();
					},
					error: function (xhr, status, error) {
						 $('.error').html(xhr.msg);
					}
				});
			}
		});
       /* $('.submit-resignation').on('click',function(){
            url = "{{url('/store/resignation')}}";

            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: url,
                data: {employee_id:$('select[name="employee_id"]').val(),last_working_date:$('#lw_date').val()},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
				success: function (data) {
                    location.reload();
                },
                error: function (xhr, status, error) {
                    $('button[type="submit"]').removeAttr('disabled');
                }
            })


        });
*/

    </script>
@endsection
