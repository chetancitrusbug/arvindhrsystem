
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CLEARANCE LETTER - {{$employee->full_name}} </title>
    <style>
        *{ padding: 0px;margin: 0; }
		@media print {
		table {
			page-break-inside: avoid;
		}
	}
    </style>
</head>
<body style="background-color:#eee;">
   
                  <table style="background-color:#fff;width:810px;height:100%;padding:25px 25px;font-family:Arial, Helvetica, sans-serif;vertical-align:top;border:1px solid #000;margin: 0 auto;">
        <tr>
            <td>    

			
				<br/>
				<br/>
                <table style="width:100%;padding:0px 0px;border-spacing:0px;">
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;"><p>CLEARANCE LETTER (EMP.CODE - <span style="height: 25px;width: 100px;display: inline-block;line-height: 24px;margin: 0px 0 0 0;border-bottom: 1px solid #222;padding:0;">
						{{$employee->employee_code}}
						</span> )</p></td>
                    </tr>
                </table>

				<br/>
				<br/>
				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;">
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;" colspan="2"><p>PERSONAL BANK A/C DETAILS</p></td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:11px;width:50%;" colspan="1">UNLIMITED</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 10px;font-size:12px;width:50%;" colspan="1"><span style="background-color:#ffff00;">Store IO Code:</span><span> {{$employee->storeLocation->io_code}} </span></td>
                    </tr>
                </table>

                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;border-bottom:1px solid #000;" colspan="8">Personal Information</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Emp Code</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->employee_code}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Store IO Code:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">{{$employee->storeLocation->io_code}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Emp Name:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->full_name}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Store Name:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">{{$employee->storeLocation->store_name}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Designation:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->designation->name}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Reporting to:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">
						@if($resignation->storemanager)
						{{ $resignation->storemanager->emp_name}}
					
						@elseif($resignation->regionmanager)
						{{ $resignation->regionmanager->emp_name}}
						@endif
						</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Date of Joining:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->joining_date}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Mobile:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">{{$employee->mobile}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Date of Resignation:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3"> {{$resignation->created_at->format('Y-m-d') }} </td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">E-mail ID:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3"> {{$employee->email}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Relieving Date:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3"> {{$resignation->last_working_date}} </td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"></td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3"></td>
                    </tr>
                </table><!-- end of Personal info.  -->

				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-top:0px;border-bottom:0px;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;border-bottom:1px solid #000;" colspan="8">Personal Bank Account Details</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Name as per Bank:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->full_name}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Bank A/c No:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">{{$employee->account_no}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Bank Name:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->bank_name}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">IFSC Code</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">{{$employee->ifsc}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">House Address for communication:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="7">{{$employee->permanent_address_line_1}}, {{$employee->permanent_address_line_2}}, {{$employee->permanent_address_line_3}}</td>
                    </tr>
                </table><!-- end of Personal Details  -->

<br/>
				<br/>
				<br/>
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;margin:40px 0 0 0;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">SL NO.</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">ITEM</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">NO DUE/ DUE</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">REMARKS</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-bottom:1px solid #000;" colspan="1">SIGN</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">I</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">ADMIN</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['admin_due']) && $fd['admin_due']=="1") checked @endif   name="admin_due" value="1"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['admin_remark'])) value="{{$fd['admin_remark']}}" @endif name="admin_remark" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" name="admin_sign"  style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">1</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Drawer key</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['drawer_key_due']) && $fd['drawer_key_due']=="1") checked @endif  name="drawer_key_due" value="1"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['drawer_key_remark'])) value="{{$fd['drawer_key_remark']}}" @endif name="drawer_key_remark" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text"  name="drawer_key_sign" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">2</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Personal Courier – Charges</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['personal_courier_charges_due']) && $fd['personal_courier_charges_due']=="1") checked @endif  name="personal_courier_charges_due" value="1"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['personal_courier_charges_remark'])) value="{{$fd['personal_courier_charges_remark']}}" @endif name="personal_courier_charges_remark" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" name="personal_courier_charges_sign" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">3</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Mobile Charges</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						<input type="checkbox" @if(isset($fd['mobile_charges_due']) && $fd['mobile_charges_due']=="1") checked @endif  name="mobile_charges_due" value="1"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['mobile_charges_remark'])) value="{{$fd['mobile_charges_remark']}}" @endif name="mobile_charges_remark" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text"  name="mobile_charges_sign" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">4</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Calculator</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['calculator_due']) && $fd['calculator_due']=="1") checked @endif  name="calculator_due" value="1"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text"  @if(isset($fd['calculator_remark'])) value="{{$fd['calculator_remark']}}" @endif name="calculator_remark" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text"  name="calculator_sign" style="height:30px;border:none;padding:2px 5px;">
						</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">5</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Cell Phone</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['cell_phone_due']) && $fd['cell_phone_due']=="1") checked @endif  name="cell_phone_due" value="1"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['cell_phone_remark'])) value="{{$fd['cell_phone_remark']}}" @endif name="cell_phone_remark" style="height:30px;border:none;padding:2px 5px;">
						</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text"  name="cell_phone_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">6</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Car</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						<input type="checkbox" @if(isset($fd['car_due']) && $fd['car_due']=="1") checked @endif  name="car_due" value="1"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['car_remark'])) value="{{$fd['car_remark']}}" @endif name="car_remark" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text"  name="car_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">7</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Outstanding Bills</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['outstanding_bills_due']) && $fd['outstanding_bills_due']=="1") checked @endif  name="outstanding_bills_due" value="1"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['outstanding_bills_remark'])) value="{{$fd['outstanding_bills_remark']}}" @endif name="outstanding_bills_remark" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text"  name="outstanding_bills_sign" style="height:30px;border:none;padding:2px 5px;">
						</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">8</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Library Books Clearance</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['library_books_clearance_due']) && $fd['library_books_clearance_due']=="1") checked @endif  name="library_books_clearance_due" value="1" ></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['library_books_clearance_remark'])) value="{{$fd['library_books_clearance_remark']}}" @endif name="library_books_clearance_remark" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text"  name="library_books_clearance_sign" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                    </tr>
                   

                </table><!-- end of list1 -->
				
				<br/>
				<br/>
				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;margin:40px 0 0 0;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">SL NO.</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">ITEM</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">NO DUE/ DUE</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">REMARKS</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-bottom:1px solid #000;" colspan="1">SIGN</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">II</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">SYSTEMS</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['systems_due']) && $fd['systems_due']=="1") checked @endif  value="1" name="systems_due" ></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['systems_remark'])) value="{{$fd['systems_remark']}}" @endif name="systems_remark" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" name="systems_sign" style="height:30px;border:none;padding:2px 5px;">
						
						</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">1</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Personal Computer / Laptop</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['personal_computer_due']) && $fd['personal_computer_due']=="1") checked @endif  value="1" name="personal_computer_due" ></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['personal_computer_remark'])) value="{{$fd['personal_computer_remark']}}" @endif name="personal_computer_remark" style="height:30px;border:none;padding:2px 5px;">
						</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="personal_computer_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">2</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Desktop Passwords</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['desktop_passwords_due']) && $fd['desktop_passwords_due']=="1") checked @endif  value="1" name="desktop_passwords_due" ></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['desktop_passwords_remark'])) value="{{$fd['desktop_passwords_remark']}}" @endif name="desktop_passwords_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="desktop_passwords_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">3</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">CD Writer/ Multimedia Kit</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['cd_writer_due']) && $fd['cd_writer_due']=="1") checked @endif  value="1" name="cd_writer_due" ></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['cd_writer_remark'])) value="{{$fd['cd_writer_remark']}}" @endif name="cd_writer_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="cd_writer_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">4</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Email ID Disabled</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['email_id_disabled_due']) && $fd['email_id_disabled_due']=="1") checked @endif  value="1" name="email_id_disabled_due" ></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['email_id_disabled_remark'])) value="{{$fd['email_id_disabled_remark']}}" @endif name="email_id_disabled_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="email_id_disabled_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    
                </table><!-- end of list2 -->

				<br/>
				<br/>
				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;margin:10px 0 0 0;">
                 
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">III</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">FINANCE</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['finance_due']) && $fd['finance_due']=="1") checked @endif  value="1" name="finance_due" ></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['finance_remark'])) value="{{$fd['finance_remark']}}" @endif name="finance_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="finance_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">1</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Advances or Dues</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['advances_or_dues_due']) && $fd['advances_or_dues_due']=="1") checked @endif  value="1" name="advances_or_dues_due" ></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['advances_or_dues_remark'])) value="{{$fd['advances_or_dues_remark']}}" @endif name="advances_or_dues_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="advances_or_dues_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                </table><!-- end of list3 -->

				<br/>
				<br/>
				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;margin:40px 0 0 0;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">SL NO.</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">ITEM</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">NO DUE/ DUE</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">REMARKS</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-bottom:1px solid #000;" colspan="1">SIGN</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">IV</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">HUMAN RESOURCE</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['human_resource_due']) && $fd['human_resource_due']=="1") checked @endif  value="1" name="human_resource_due" ></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['human_resource_remark'])) value="{{$fd['human_resource_remark']}}" @endif name="human_resource_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="human_resource_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">1</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">HR Manual/ Induction Manual</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['hr_manual_due']) && $fd['hr_manual_due']=="1") checked @endif  value="1" name="hr_manual_due" ></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" name="hr_manual_remark" @if(isset($fd['hr_manual_remark'])) value="{{$fd['hr_manual_remark']}}" @endif style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="hr_manual_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">2</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">House Deposit</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['house_deposit_due']) && $fd['house_deposit_due']=="1") checked @endif  value="1" name="house_deposit_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['house_deposit_remark'])) value="{{$fd['house_deposit_remark']}}" @endif name="house_deposit_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="house_deposit_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">3</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Custody of Furniture & Fixtures</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['custody_of_furniture_due']) && $fd['custody_of_furniture_due']=="1") checked @endif  value="1" name="custody_of_furniture_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['custody_of_remark'])) value="{{$fd['custody_of_remark']}}" @endif name="custody_of_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="custody_of_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">4</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Loans – V.Loan / Hsg Loan / GP Loan</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['loans_due']) && $fd['loans_due']=="1") checked @endif  value="1" name="loans_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['loans_remark'])) value="{{$fd['loans_remark']}}" @endif name="loans_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="loans_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">5</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">ID Card/ Access Cards</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['id_card_due']) && $fd['id_card_due']=="1") checked @endif  value="1" name="id_card_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['id_card_remark'])) value="{{$fd['id_card_remark']}}" @endif name="id_card_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="id_card_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">6</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Exit Interview Conducted (Organisation Effectiveness)</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['exit_interview_conducted_due']) && $fd['exit_interview_conducted_due']=="1") checked @endif  value="1" name="exit_interview_conducted_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['exit_interview_conducted_remark'])) value="{{$fd['exit_interview_conducted_remark']}}" @endif name="exit_interview_conducted_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="exit_interview_conducted_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    
                </table><!-- end of list4 -->

				<br/>
				<br/>
				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;margin:10px 0 0 0;">
            
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">V</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">RESPECTIVE DEPT/ FUNCTION</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['respective_dept_due']) && $fd['respective_dept_due']=="1") checked @endif  value="1" name="respective_dept_due"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['respective_dept_remark'])) value="{{$fd['respective_dept_remark']}}" @endif name="respective_dept_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="respective_dept_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">1</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Samples / Designs</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['samples_designs_due']) && $fd['samples_designs_due']=="1") checked @endif  value="1" name="samples_designs_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['samples_designs_remark'])) value="{{$fd['samples_designs_remark']}}" @endif  name="samples_designs_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="samples_designs_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">2</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Computer Floppies / CD’s</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['computer_floppies_due']) && $fd['computer_floppies_due']=="1") checked @endif  value="1" name="computer_floppies_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['computer_floppies_remark'])) value="{{$fd['computer_floppies_remark']}}" @endif name="computer_floppies_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="computer_floppies_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">3</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Data Files / Correspondence documents</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['data_files_due']) && $fd['data_files_due']=="1") checked @endif  value="1" name="data_files_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['data_files_remark'])) value="{{$fd['data_files_remark']}}" @endif name="data_files_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="data_files_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">4</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Registers / Manual</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['registers_manual_due']) && $fd['registers_manual_due']=="1") checked @endif  value="1" name="registers_manual_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['registers_manual_remark'])) value="{{$fd['registers_manual_remark']}}" @endif name="registers_manual_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="registers_manual_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">5</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">SO@AR Form <span style="font-weight:600;">(Signed by Dept Head  & Line HR)</span></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['soaar_form_due']) && $fd['soaar_form_due']=="1") checked @endif  value="1" name="soaar_form_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['soaar_form_remark'])) value="{{$fd['soaar_form_remark']}}" @endif  name="soaar_form_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="soaar_form_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">6</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Proper Handing Over has been done and the Immediate Superior & Department/Function is satisfied with the same. (Please attach Copy of the Handing Over Note).</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox" @if(isset($fd['proper_handing_over_due']) && $fd['proper_handing_over_due']=="1") checked @endif  value="1" name="proper_handing_over_due"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
						
						<input type="text" @if(isset($fd['proper_handing_over_remark'])) value="{{$fd['proper_handing_over_remark']}}" @endif name="proper_handing_over_remark" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" name="proper_handing_over_sign" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    
                </table><!-- end of list5 -->

				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
                <table style="width:100%;padding:0px 0px;border-spacing:0px;margin:50px 0 10px 0;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:33.3%;" colspan="1"><span style="border-bottom:1px solid #222;">Signature of Immediate Superior</span></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:33.3%;" colspan="1"><span style="border-bottom:1px solid #222;">Signature of Line HR</span></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:33.3%;" colspan="1"><span style="border-bottom:1px solid #222;">Signature of Functional Head.</span></td>
                    </tr>
                </table>

                <table style="width:100%;padding:0px 0px;border-spacing:0px;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:100%;" colspan="1"><span style="border-bottom:1px solid #222;font-style:italic;">Please forward this form duly signed to Human resource department one day before relieving the employee.</span></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:100%;" colspan="1"><span style="border-bottom:1px solid #222;font-style:italic;">Store Address & Seal</span></td>
                    </tr>
                </table>

                <table style="width:100%;padding:20px 0px 0 0;border-spacing:0px;border-top:1px solid #222;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:100%;" colspan="1"><span style="border-bottom:1px solid #222;background-color:#ffff00;">Handing Over Note</span></td>
                    </tr>
                    
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;" colspan="1">1.	All Files & Formats have been handed over to the New Role Holder/ Immediate Superior.
                            <label style="padding-left: 20px;font-weight:600;"><input @if(isset($fd['handing_over_all_files']) && $fd['handing_over_all_files']=="yes") checked @endif type="radio" value="yes" name="handing_over_all_files"> <span>Yes</span></label>
                            <label style="padding-left: 20px;font-weight:600;"><input @if(isset($fd['handing_over_all_files']) && $fd['handing_over_all_files']=="no") checked @endif type="radio" value="no" name="handing_over_all_files"> <span>No</span></label>
                        </td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;padding-left:50px;" colspan="1">a.	The following Files/Formats in Hard Copy have been handed over:</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;padding-left:50px;" colspan="1">b.	The following Files/Formats in Soft Copy have been forwarded/location clearly specified:</td>
                    </tr>
                    
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;line-height:18px;" colspan="1">2.	The status on the following Work in Progress <span style="font-weight:600;">(WIP)</span> has been clearly communicated to the New Role Holder/ Immediate Superior/ Internal Customer.</td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;" colspan="1">3.	The Original SO@AR Form has been handed over to New Role Holder/ Immediate Superior. 
                            <label style="padding-left: 20px;font-weight:600;"><input @if(isset($fd['handing_over_soaar_form']) && $fd['handing_over_soaar_form']=="yes") checked @endif type="radio" value="yes" name="handing_over_soaar_form"> <span>Yes</span></label>
                            <label style="padding-left: 20px;font-weight:600;"><input @if(isset($fd['handing_over_soaar_form']) && $fd['handing_over_soaar_form']=="no") checked @endif type="radio" value="no" name="handing_over_soaar_form"> <span>No</span></label>
                            <p style="padding:10px 0;font-weight:600;">Please explain in case the same has not been handed over.</p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;line-height:18px;" colspan="1">4.	Remarks from the immediate superior on how the deliverables of this Role will be managed in the immediate days following the exit.</td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;line-height:24px;" colspan="1">
                            5.	Comments from the Immediate Superior & Function Head (Are you satisfied with the handing over 
                            <label style="padding-left: 20px;font-weight:600;"><input @if(isset($fd['confirm_satisfied_handing_over']) && $fd['confirm_satisfied_handing_over']=="yes") checked @endif type="radio" value="yes" name="confirm_satisfied_handing_over"> <span>Yes</span></label>
                            <label style="padding-left: 20px;padding-right: 20px;font-weight:600;">
							
							<input @if(isset($fd['confirm_satisfied_handing_over']) && $fd['confirm_satisfied_handing_over']=="no") checked @endif type="radio" value="no" name="confirm_satisfied_handing_over"> <span>No</span>
							</label> 
                            and convinced that the work will go on to everyone’s satisfaction 
                            <label style="padding-left: 20px;font-weight:600;">
							
							<input @if(isset($fd['convinced_satisfied_handing_over']) && $fd['convinced_satisfied_handing_over']=="yes") checked @endif type="radio" value="yes" name="convinced_satisfied_handing_over"> <span>Yes</span></label>
							
                            <label style="padding-left: 20px;padding-right: 20px;font-weight:600;">
							
							<input @if(isset($fd['convinced_satisfied_handing_over']) && $fd['convinced_satisfied_handing_over']=="no") checked @endif type="radio" value="no" name="convinced_satisfied_handing_over"> <span>No</span></label> 
                            Please elaborate).
                            
                        </td>
                    </tr>
					
					

                </table>



                
            </td>
        </tr>
    </table>
</body>

<script>
window.print();

</script>

</html>