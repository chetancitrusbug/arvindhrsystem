@extends('storeManager.layouts.backend')
@section('title',"Add Attendance")
@section('css')
    <style type="text/css">
        input[type="file"]{
            border: none;
        }
        .family_label{
            border: none;
            padding: 5px 5px 0px;
        }
        .text-center{
            text-align: center;
        }
        td {
            vertical-align: top !important;
        }
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">
                    <p class="panel-head1">Add Attendance</p>
                    <span class="panel-subtitle">
                        <a href="{{ url('/store/attendance') }}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/store/attendance', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                        <div class="form-group has-error">
                            {!! Form::label('', '', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                @if(Session::has('valMonth') && Session::get('valMonth') != '')
                                    <p class="help-block">invalid month in row {{ Session::get('valMonth') }}</p>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('excel') ? ' has-error' : ''}}">
                            {!! Form::label('excel', '* Excel: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-5">
                                {!! Form::file('excel', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('excel', '
                                <p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::submit('Upload', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection