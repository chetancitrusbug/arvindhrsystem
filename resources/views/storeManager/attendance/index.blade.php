@extends('storeManager.layouts.backend')
@section('title',"Attendance")
@section('css')
    <style>
        .paging { padding-right: 20px; padding: 0px 10px !important; }
        #attendance-table { width:3000px;}
        .select{
            padding: 6px 2px;
            height: 30px;
            font-size: 12px;
            width: 55px;
        }
        .table-responsive tbody > tr > td {
            width: 100px;
        }
        .table-responsive tbody > tr > td:first-child{
            width: 200px;
        }
        .request-change{
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
<div class="be-content">

    @include('storeManager.includes.alerts')
    @include('storeManager.modals.attendance_confirmation')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
                    <p class="panel-head1">Attendance List</p>
                        <span class="panel-subtitle">
                        </span>
                    </div>
                    @php
                        $month = request()->get('month',isset($month)?$month:null);
                        $year = request()->get('year',isset($year)?$year:null);
                    @endphp
                    <div class="blk-div clearfix">
                    {!! Form::open(['url' => '/store/attendance-filter', 'class' => '','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        <div class="form-group col-sm-2{{ $errors->has('current_state') ? ' has-error' : ''}}">
                            {!! Form::select("month", [''=>'-- Month --']+config('constants.month_list'),  $month,['class' => 'form-control input-sm month']) !!}
                            {!! $errors->first('month', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-sm-2{{ $errors->has('current_state') ? ' has-error' : ''}}">
                            {!! Form::select("year", [''=>'-- Year --']+config('constants.attendance_year_list'), $year ,['class' => 'form-control input-sm year']) !!}
                            {!! $errors->first("year", '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::submit('Search', ['class' => 'btn btn-md btn-primary']) !!}
                        </div>
                    {!! Form::close() !!}
                    </div>
                    <div class="panel-body clearfix">
                        <div class="table-responsive table-scroll1" >
                            <table id="attendance-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        @if(isset($days) && $days > 0)
                                            @for ($i = 1; $i <= $days; $i++)
                                                <th >{{$i}}</th>
                                            @endfor
                                        @endif
                                    </tr>
                                </thead>
								<tbody>
                                    @if(count($attendance))
                                        @php
                                            $current = Carbon\Carbon::now()->format('Y-m-d');
                                            /* $currentMonth = Carbon\Carbon::now()->format('m');
                                            $currentYear = Carbon\Carbon::now()->format('Y'); */
                                            $monthNumber = config('constants.month_number');
                                            $status = false;
                                        @endphp
                                        {!! Form::open(['url' => '/store/attendance', 'class' => '','id' => 'module_form_add_attendance','autocomplete'=>'off','files'=>true])
                                        !!}
                                            {!! Form::hidden("month", $month ,[]) !!}
                                            {!! Form::hidden("year", $year ,[]) !!}

                                            @foreach ($attendance as $value)
											    <tr>
                                                    <td>{!! $value->full_name !!}</td>
                                                    @if(isset($days) && $days > 0)
                                                        @for ($i = 1; $i <= $days; $i++)
                                                            @php
                                                            $default_date = "01-".$month."-".$year;
                                                            $monthNum = date("m",strtotime($default_date));
                                                            $background_color = '';
                                                            if(!empty($value->date_of_resignation) && $value->hold_salary =='YES' && empty($value->deleted_at) && $value->deleted_at == null){
                                                                $background_color = 'style=background-color:#FFCCCC';
                                                            }
                                                            if(!empty($value->rm_absconding_status) && $value->rm_absconding_status >=2 && $value->hold_salary =='YES'){
                                                                $background_color = 'style=background-color:#FFFACD';
                                                            }

                                                            @endphp

                                                            <td {{$background_color}}>
                                                                @php
                                                                    $disabled_attandace = '';
																	$label = $labelStatus = '';
																	$label = (isset($presents[$value->id][$i]) && $presents[$value->id][$i] != '' )?$presents[$value->id][$i]:null;
																	$labelStatus = (isset($labelChange[$value->id][$i]) && $labelChange[$value->id][$i] == 1)?'label label-danger':null;
																	$date = $year.'-'.$monthNumber[$month].'-'.$i;
                                                                    $date = Carbon\Carbon::parse($date)->format('Y-m-d');
																	if(!array_key_exists($label,config('constants.attendance'))){
																		$label = '';
																	}
																@endphp

                                                                @if(strtotime($date) > strtotime($current) || $label != '')

																@php
																	/*
																	if($i == 26 && $value->id == 172){
																		var_dump(strtotime($date) > strtotime($current));echo '</br>';
																		var_dump($label);
																		//exit($value->id);
																		//echo $arr[$label];exit;
																		exit;
																	}
																	var_dump(($presents[$value->id][$i]));
																		echo '</br> Check'; */
																@endphp

																
																<label class="request-change {{$labelStatus}}" data-id="{{$value->id}}" data-value="{{ ($label)?$label:'-' }}" data-month="{{$month}}" data-year="{{$year}}" data-day="{{ $i }}">{{ ($label)?config('constants.attendance')[$label]:'-' }}</label>
																{{--
																<label class="request-change ">
																	{{ ($label)?config('constants.attendance')[$label]:'-' }} {{config('constants.attendance')[$label]}}
																</label>
																--}}
                                                                @else
                                                                    @php
                                                                    $disabled_attandace = false;
																	if(!empty($value->joining_date) && strtotime(date($i.'-'.$monthNum.'-'.$year)) < strtotime($value->joining_date) ){
																		$disabled_attandace = true;
																	}
																	
                                                                    if(!empty($value->last_working_date) && date('Y-m-d') > $value->last_working_date && $i > date('d',strtotime($value->last_working_date)) && $monthNum >= date('m',strtotime($value->last_working_date)) && $year >= date('Y',strtotime($value->last_working_date)) && !empty($value->deleted_at) && $value->deleted_at == null){
                                                                        $disabled_attandace = true;
                                                                    }
                                                                    if(!empty($value->absconding_time) && date('Y-m-d') > $value->absconding_time && $i > date('d',strtotime($value->absconding_time)) && $monthNum >= date('m',strtotime($value->absconding_time)) && $year >= date('Y',strtotime($value->absconding_time))){
                                                                        $disabled_attandace = true;
                                                                    }
																	
                                                                    @endphp

                                                                    <?php $att = 'att['.$value->id.']['.$i.']'; ?>
                                                                    @if (!$disabled_attandace)
                                                                        {!! Form::select("att[$value->id][$i]", [''=>'-']+config('constants.attendance'), isset($presents[$value->id][$i])?$presents[$value->id][$i]:null
                                                                        ,['disabled' => $disabled_attandace,'class' => 'form-control select']) !!}
                                                                    @else
																		-
                                                                    @endif

                                                                @endif
                                                            </td>
                                                        @endfor
                                                    @endif
                                                </tr>
											@endforeach
                                            {{-- @if($status) --}}
                                                <tr>
                                                    <td colspan="{{isset($days)?$days:0 + 1}}">
													@if(isset($month) && isset($year))
													<!--{!! Form::submit('Add Attendance', ['class' => 'btn btn-space btn-primary']) !!}-->
														<button type="button"  id="add_attendance" class='btn btn-space btn-primary'>Add Attendance</button>
													@endif
													</td>
                                                </tr>
                                                {{-- <tr>
                                                    @php
                                                        $data['month'] = ($month)?strtolower(config('constants.month_list')[$month]):'';
                                                        $data['year'] = ($year)?config('constants.year_list')[$year]:'';
                                                    @endphp
                                                    <td colspan="{{isset($days)?$days:0 + 1}}">{{ $attendance->appends($data)->links() }}</td>
                                                </tr> --}}
                                            {{-- @endif --}}
                                        {!! Form::close() !!}
                                    @else
                                        <tr>
                                            <td colspan="33">There is no data</td>
                                        </tr>
                                    @endif
									
                                    <tr align="right">
                                        {{-- <td colspan="2" class="paging">{{ $attendance->links() }}</td> --}}
                                    </tr>
                                <tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var id = attendance = day = month = button = null;

        $('.request-change').on('click',function(){
            button = $(this);
            $("#loading").show();
            id = $(this).data('id');
            attendance = $(this).data('value');
            day = $(this).data('day');
            month = $(this).data('month');
            year = $(this).data('year');
            $('#attendanceConfirmation').niftyModal();
        });

        $('.att-confirm').on('click',function(){
            $('#attendanceConfirmation').niftyModal('hide');
            $('textarea[name="note"]').val('');
            $('#change-att-model').niftyModal();
        });

        $('.change-submit').on('click',function(){
            $('button[type="submit"]').attr('disabled','disabled');
            $.ajax({
                type: 'POST',
                url: "{{url('/store/change-request')}}",
                data: {
                    id:id,
                    attendance:attendance,
                    day:day,
                    month:month,
                    year:year,
                    note:$('textarea[name="note"]').val(),
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data) {
                $('button[type="submit"]').removeAttr('disabled');
                $('.success-text').html(data);
                $('#change-att-model').niftyModal('hide');
                $('#success').niftyModal();
                button.addClass('label label-danger');

                setTimeout(function () {
                    $('#success').niftyModal('hide');
                    $("#loading").hide();
                }, 2000);
            })
            .fail(function(data) {
                $('button[type="submit"]').removeAttr('disabled');
                jQuery.each(data.responseJSON.errors,function(index,val){
                    $('.'+index+'_error').html(val);
                });
                $('.error').removeClass('hide');
            });
        });

        $('#add_attendance').on('click',function(){
			if($(".month").val() == ''){
				alert('Please Select attendance Month which you want to add Attendance');
			}else if($(".year").val() == ''){
				alert('Please Select attendance Year which you want to add Attendance');
			}else{
				$("#module_form_add_attendance").submit();
			}
		});
        $('.att-confirm-cancel, .change-close').on('click',function(){
            $('#attendanceConfirmation').niftyModal('hide');
            $('#change-att-model').niftyModal('hide');
            $("#loading").hide();
        });
    </script>
@endsection