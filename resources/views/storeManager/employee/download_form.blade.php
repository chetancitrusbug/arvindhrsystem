<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        *{ padding: 0px;margin: 0; }
        body { padding: 30px; }
        td {
            height: 12px !important;
        }
    </style>
</head>
<body>
    {{-- <table style="background-color:#fff;width:750px;padding:30px 30px;font-family:Arial, Helvetica, sans-serif;vertical-align:top;border:1px solid #000;">
        <tr>
            <td> --}}
            <div style="background-color:#fff;width:100%;font-family:Arial, Helvetica, sans-serif;vertical-align:top;border:0px
            solid #000;">
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px solid #222;">
                    <tr>
                        <td style="color:rgb(56, 87, 35);font-weight:600;text-align:right;padding:0 10px;border-bottom:1px solid #000;font-size:20px;width:60%;" colspan="5">Application for Employment</td>
                        <td style="text-align:right;border-bottom:1px solid #000;width:40%;" colspan="3"><img src="{{asset('uploads/logo.png')}}" style="width:160px;padding:10px;" /></td>
                    </tr>
                    <tr>
                        <td style="color:rgb(56, 87, 35);font-weight:600;text-align:left;padding:10px 2px;font-style:italic;font-size:12px;border-bottom:1px solid #000;" colspan="8">All fields indicated as (*) are required to process your application.</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:25%;" colspan="1">Position applied for*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:75%;border-left:1px solid #222;" colspan="7">{{$employee->position}}</td>
                    </tr>
                </table>

                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:none; page-break-after:always !important;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;background-color:rgb(217, 226, 243);border-bottom:1px solid #000;" colspan="8">Personal Information</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Full Name (as per Aadhar)*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:27%;border-bottom:1px solid #000;" colspan="3">{{ $employee->full_name }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Blood Group*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:10%;border-bottom:1px solid #000;" colspan="1">{{ $employee->blood_group }}</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:10%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Rh</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:10%;border-bottom:1px solid #000;" colspan="1">{{ $employee->rh_factor }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Date of Birth (as per Aadhar)*</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">
                            @php
                               $birhtDate = explode('-',$employee->date_of_birth);
                            @endphp
                            {{ $birhtDate[0] }}
                        </td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:9%;border-bottom:1px solid #000;" colspan="1">{{ $birhtDate[1] }}</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:9%;border-left:1px solid #222;border-bottom:1px solid #000;" colspan="1">{{ $birhtDate[2] }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Marital Status*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:30%;border-bottom:1px solid #000;" colspan="3">{{ ucfirst($employee->marital_status) }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Aadhar Number*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">{{ isset(config('constants.aadhar_status')[$employee->aadhar_status])?config('constants.aadhar_status')[$employee->aadhar_status]:'' }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:18%;border-bottom:1px solid #000;" colspan="2">{{ $employee->aadhar_number }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Nationality*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:30%;border-bottom:1px solid #000;" colspan="3">{{ $employee->nationality }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Aadhar Enrolment Number</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:27%;border-bottom:1px solid #000;" colspan="3">{{ $employee->aadhar_enrolment_number }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Gender (as per Aadhar)*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:30%;border-bottom:1px solid #000;" colspan="3">{{isset(config('constants.gender')[$employee->gender])?config('constants.gender')[$employee->gender]:''}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">PAN*</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">{{isset(config('constants.pan_status')[$employee->pan_status])?config('constants.pan_status')[$employee->pan_status]:''}}</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:18%;border-bottom:1px solid #000;" colspan="2">{{ $employee->pan_number }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Mobile Number*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:30%;border-bottom:1px solid #000;" colspan="3">{{ $employee->mobile }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Emergency Contact person name*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:27%;border-bottom:1px solid #000;" colspan="3">{{ $employee->emergency_person }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Personal Email id*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:30%;border-bottom:1px solid #000;" colspan="3">{{ $employee->email }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;" colspan="1">Emergency Contact number*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:27%;" colspan="3">{{ $employee->emergency_number }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-left:1px solid #222;border-right:1px solid #222;" colspan="1">ESIC Number</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:30%;" colspan="3">{{ $employee->esic_number }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;border-top:1px solid #222;" colspan="1">Emergency Contact Relationship*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:27%;border-bottom:1px solid #000;border-top:1px solid #222;" colspan="3">{{ $employee->emergency_relationship }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;border-top:1px solid #222;" colspan="1">Wedding anniversary date</td>

                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:10%;border-bottom:1px solid #000;border-top:1px solid #222;" colspan="1">
                            @php
                                $anniversaryDate = explode('-',$employee->anniversary_date);
                            @endphp
                            {{ (isset($anniversaryDate[0]))?$anniversaryDate[0]:'' }}
                        </td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:10%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;border-top:1px solid #222;"
                            colspan="1">{{ isset($anniversaryDate[1])?$anniversaryDate[1]:'' }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:10%;border-bottom:1px solid #000;border-top:1px solid #222;"
                            colspan="1">{{ isset($anniversaryDate[2])?$anniversaryDate[2]:'' }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">First Name (for email id purpose)*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:27%;border-bottom:1px solid #000;" colspan="3">{{ $employee->first_name }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 4px;font-size:11px;background-color:#e7e6e6;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1" rowspan="2">Universal Account Number (UAN)</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:30%;border-bottom:1px solid #000;" colspan="3" rowspan="2">{{ $employee->ua_number }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Last Name (for email id purpose)*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:27%;border-bottom:1px solid #000;" colspan="3">{{ $employee->last_name }}</td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1" rowspan="4">Current Address*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Line 1</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:68%;border-bottom:1px solid #000;" colspan="6">{{ $employee->current_address_line_1 }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Line 2</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:68%;border-bottom:1px solid #000;" colspan="6">{{ $employee->current_address_line_2 }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Line 3</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:68%;border-bottom:1px solid #000;" colspan="6">{{ $employee->current_address_line_3 }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">City</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">{{ $employee->currentCity->name or '' }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:9%;border-bottom:1px solid #000;" colspan="1">State</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 4px;font-size:11px;background-color:#fff;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">{{ $employee->currentState->name or '' }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Pincode</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:10%;border-bottom:1px solid #000;" colspan="1">{{ $employee->current_pincode }}</td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1" rowspan="4">Permanent Address*</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Line 1</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:68%;border-bottom:1px solid #000;" colspan="6">{{ $employee->permanent_address_line_1 }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Line 2</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:68%;border-bottom:1px solid #000;" colspan="6">{{ $employee->permanent_address_line_2 }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Line 3</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:68%;border-bottom:1px solid #000;" colspan="6">{{ $employee->permanent_address_line_3 }}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">City</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">{{ $employee->permanentCity->name or '' }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:9%;border-bottom:1px solid #000;" colspan="1">State</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 4px;font-size:11px;background-color:#fff;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">{{ $employee->permanentState->name or '' }}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Pincode</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:10%;border-bottom:1px solid #000;" colspan="1">{{ $employee->permanent_pincode or '' }}</td>
                    </tr>

                </table><!-- end of Personal info.  -->
                {{-- <p style="page-break-after: always;">&nbsp;</p>] --}}
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-top:none;border-bottom:none; ">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;background-color:rgb(217, 226, 243);border-bottom:1px solid #000;border-top:1px solid #000;" colspan="7">Education Details*</td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;">Category</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:13%;border-right:1px solid #222;border-bottom:1px solid #000;">Board / University</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:16%;border-right:1px solid #222;border-bottom:1px solid #000;" >Name of the Institution</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" >Qualification Name</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" >Specialization</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:18%;border-bottom:1px solid #000;" colspan="2">Month & Year of passing</td>
                    </tr>

                    @foreach($employee->education as $key=>$value)
                        <tr>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ isset($educationCategory[$value['education_category_id']])?$educationCategory[$value['education_category_id']]:'' }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:13%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ $value['board_uni'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:16%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ $value['institute'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;">{{$value['qualification']}}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ $value['specialization'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:9%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ isset(config('constants.month_list')[$value['month']])?config('constants.month_list')[$value['month']]:'' }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:9%;border-bottom:1px solid #000;">{{ $value['year'] }}</td>
                        </tr>
                    @endforeach
                </table><!-- end of Education Details -->

                <table style="width:100%;padding:0px 0px;border-spacing:0px;border-top:1px solid #222;border:1px solid #222;border-top:none;border-bottom:none; ">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;background-color:rgb(217, 226, 243);border-bottom:1px solid #000;" colspan="6">Previous Work Experience(s)</td>
                    </tr>
                    <tr>
                        <td style="color:#fff;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;background-color:#8eaadc;border-bottom:1px solid #000;" colspan="3">FIRST TIME EMPLOYMENT** (<img src="{{asset('uploads/check.png')}}" style="" height="10px" /> as Applicable)</td>
                        <td style="color:#fff;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;background-color:#8eaadc;border-bottom:1px solid #000;vertical-align: middle;" colspan="1"> YES
                            @if($employee->first_time_employment == '1')
                                <img src="{{asset('uploads/check.png')}}" style="" height="10px" />
                            @endif
                        </td>
                        <td style="color:#fff;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;background-color:#8eaadc;border-bottom:1px solid #000;" colspan="1"> NO
                            @if($employee->first_time_employment == '0')
                                <img src="{{asset('uploads/check.png')}}" style="" height="10px" />
                            @endif
                        </td>
                        <td style="color:#fff;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;background-color:#8eaadc;border-bottom:1px solid #000;" colspan="1"></td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:30%;border-right:1px solid #222;border-bottom:1px solid #000;font-style:italic;">Company Name</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:12%;border-right:1px solid #222;border-bottom:1px solid #000;font-style:italic;">Date of Joining (DD/MM/YYYY)</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:12%;border-right:1px solid #222;border-bottom:1px solid #000;font-style:italic;" >Date of Leaving (DD/MM/YYYY)</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:16%;border-right:1px solid #222;border-bottom:1px solid #000;font-style:italic;" >Nature of Employment</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;font-style:italic;" >Last held Designation</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:15%;border-bottom:1px solid #000;font-style:italic;">Reason for leaving</td>
                    </tr>

                    @foreach($employee->previous as $key=>$value)
                        <tr>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:30%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ $value['company_name'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:12%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ $value['joining_date'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:12%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ $value['leaving_date'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:16%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ isset($natureOfEmployment[$value['nature_of_employment_id']])?$natureOfEmployment[$value['nature_of_employment_id']]:'' }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ $value['designation'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:15%;border-bottom:1px solid #000;">{{ $value['reason_of_leaving'] }}</td>
                        </tr>
                    @endforeach

                </table><!-- end of Previous Work Experience -->

                <table style="width:100%;padding:0px 0px;border-spacing:0px;border-top:1px solid #222;border:1px solid #222;border-top:none;border-bottom:none;page-break-after:always !important;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;background-color:rgb(217, 226, 243);border-bottom:1px solid #000;" colspan="7">Family Information - (For Mediclaim / ESIC / PF)*</td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" rowspan="2">Relationship*</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" rowspan="2">Gender*</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:16%;border-right:1px solid #222;border-bottom:1px solid #000;" rowspan="2">Aadhar Number</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:fff;width:30%;border-right:1px solid #222;border-bottom:1px solid #000;" rowspan="2">Name*</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:24%;border-bottom:1px solid #000;" colspan="3">Date of birth as per Aadhar</td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:7%;border-bottom:1px solid #000;border-right:1px solid #000;">Date*</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:7%;border-bottom:1px solid #000;border-right:1px solid #000;">Month*</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:7%;border-bottom:1px solid #000;">Year*</td>
                    </tr>

                    @foreach($employee->family as $key=>$value)
                        <tr>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#e7e6e6;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" >{{ $value['relation'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ ($value['gender']!='')?config('constants.gender')[$value['gender']]:'' }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:16%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ $value['aadhar_number'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:fff;width:30%;border-right:1px solid #222;border-bottom:1px solid #000;">{{ $value['name'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:7%;border-bottom:1px solid #000;border-right:1px solid #000;">{{ $value['date'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:7%;border-bottom:1px solid #000;border-right:1px solid #000;">{{ $value['month'] }}</td>
                            <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:7%;border-bottom:1px solid #000;">{{ $value['year'] }}</td>
                        </tr>
                    @endforeach
                </table><!-- end of Family Information -->

                <table style="width:100%;padding:0px 0px;border-spacing:0px;border-top:1px solid #222;border:1px solid #222;border-top:none;border-bottom:none;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;background-color:rgb(217, 226, 243);border-bottom:1px solid #222;border-top:1px solid #222;" colspan="2">Declaration by applicant*</td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:100%;line-height:16px;font-style:italic;" colspan="2">
                            I declare that the above mentioned information furnished by me is accurate and correct to the best of my knowledge. I agree and undertake to submit all documents that may be required from me, by the Company, to substantiate the information furnished by me in this employment application. I have no objection in the Company sharing the information herein to any third party organization appointed by the Company for purposes such as   background verification checks, Medical insurance enrollment, Statutory related matters etc. I further confirm that I am not suffering from any medical ailment or infirmity that may be detrimental for discharging my official duties for a prolonged period. In case I suffer from such prolonged medical ailment, infirmity or illness, I shall intimate the Company within 15 days of it coming to my knowledge, either before or during the term of my employment with the Company. In addition to the above mentioned undertaking, I hereby agree that the Company may at its sole discretion  take appropriate action against me as it may deem fit including termination of my services, if in case any of the  said information or documents are found to be false, misrepresented or incorrect.
                        </td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:75%;">Date:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:25%;"></td>
                    </tr>

                    <tr style="vertical-align:top;">
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:11px;background-color:#fff;width:75%;border-bottom:1px solid #000;">Place:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:11px;background-color:#fff;width:25%;border-bottom:1px solid #000;padding-bottom: 45px;">Employee Signature*</td>

                    </tr>
                </table>    <!-- end of Declaration by applicant -->

            </div>
            {{-- </td>
        </tr>
    </table> --}}
</body>
</html>
