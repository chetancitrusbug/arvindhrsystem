<div class="form-group col-sm-12{{ $errors->has('first_time_employment') ? ' has-error' : ''}}">
    {!! Form::label('first_time_employment', 'FIRST TIME EMPLOYMENT (✔ as Applicable): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::checkbox('first_time_employment',1, $employee->first_time_employment, ['class'=>['first_time_employment']]) !!}
        {!! $errors->first('first_time_employment', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="table-responsive table-responsive2">
<table class="table table-condensed table-bordered">
    <tr>
        <th style="width: 15%;">Company Name</th>
        <th style="width: 10%;">Date Of Joining (DD-MM-YYYY)</th>
        <th style="width: 10%;">Date Of Leaving (DD-MM-YYYY)</th>
        <th style="width: 15%;">Nature of Employment</th>
        <th>Last held Designation</th>
        <th>Reason for leaving</th>
        {{-- <th class="actions" align="center"><button class="btn btn-success disable" id="add_2"><i class="mdi mdi-plus"></i></button></th> --}}
    </tr>
    <tbody id="experience_field">
        @php
            $previous = old('previous',isset($employee->previous)?$employee->previous:[]);
        @endphp
        @if(count($previous))
			@php
				$i = 0;
			@endphp
            @foreach($previous as $key=>$value)
                <tr class="experience{{ $key }}" data-row="{{ $key }}">
                    <td class='{{ $errors->has("previous.$key.company_name") ? "has-error" : ""}}'>
                        {!! Form::text("previous[$key][company_name]", $value['company_name'], ['class' => 'form-control input-sm disable']) !!}
               <?php /*         {!! Form::hidden("previous[$key][company_name]", $value['company_name'] ,[]) !!} */?>
                        @if(isset($value['id']) && !empty($value['id']))
                            {!! Form::hidden("previous[$key][id]", $value['id'], ['class' => 'form-control input-sm disable']) !!}
                        @endif
                        {!! $errors->first("previous.$key.company_name", '<p class="help-block">:message</p>') !!}
                    </td>
                    <td class='{{ $errors->has("previous.$key.joining_date") ? "has-error" : ""}}'>
                        <div data-min-view="2"  class="input-group {{ !(isset($is_employee) && $is_employee ==1)?'date datetimepicker1':'' }}">
                            {!! Form::text("previous[$key][joining_date]", $value['joining_date'], ['class' => 'form-control input-sm disable']) !!}
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                        <?php /*     {!! Form::hidden("previous[$key][joining_date]", $value['joining_date'] ,[]) !!} */?>
                        </div>
                        {!! $errors->first("previous.$key.joining_date", '<p class="help-block">:message</p>') !!}
                    </td>
                    <td class='{{ $errors->has("previous.$key.leaving_date") ? "has-error" : ""}}'>
                        <div data-min-view="2"  class="input-group {{ !(isset($is_employee) && $is_employee ==1)?'date datetimepicker1':'' }}">
                            {!! Form::text("previous[$key][leaving_date]", $value['leaving_date'], ['class' => 'form-control input-sm disable']) !!}
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            <?php /* {!! Form::hidden("previous[$key][leaving_date]", $value['leaving_date'] ,[]) !!} */?>
                        </div>
                        {!! $errors->first("previous.$key.leaving_date", '<p class="help-block">:message</p>') !!}
                    </td>
                    <td class='{{ $errors->has("previous.$key.nature_of_employment_id") ? "has-error" : ""}}'>
                        {!! Form::select("previous[$key][nature_of_employment_id]", [''=>'-- Nature Of Employment --']+$natureOfEmployment, $value['nature_of_employment_id'] ,['class' => 'form-control input-sm disable']) !!}
                        <?php /* {!! Form::hidden("previous[$key][nature_of_employment_id]", $value['nature_of_employment_id'] ,[]) !!}*/?>
                        {!! $errors->first("previous.$key.nature_of_employment_id", '<p class="help-block">:message</p>') !!}
                    </td>
                    <td class='{{ $errors->has("previous.$key.designation") ? "has-error" : ""}}'>
                        {!! Form::text("previous[$key][designation]", $value['designation'], ['class' => 'form-control input-sm disable']) !!}
                        <?php /* {!! Form::hidden("previous[$key][designation]", $value['designation'] ,[]) !!} */?>
                        {!! $errors->first("previous.$key.designation", '<p class="help-block">:message</p>') !!}
                    </td>
                    <td class='{{ $errors->has("previous.$key.reason_of_leaving") ? "has-error" : ""}}'>
                        {!! Form::text("previous[$key][reason_of_leaving]", $value['reason_of_leaving'], ['class' => 'form-control input-sm disable']) !!}
                        <?php /* {!! Form::hidden("previous[$key][reason_of_leaving]", $value['reason_of_leaving'] ,[]) !!}*/?>
                        {!! $errors->first("previous.$key.reason_of_leaving", '<p class="help-block">:message</p>') !!}
                    </td>
                    {{-- <td align="center"><button class="btn btn-danger experience_remove disable" data-row='{{ $key }}'><i class="mdi mdi-minus"></i></button></td> --}}
                </tr>
				@php
				$i++;
			@endphp
            @endforeach
		@else
            @for ($i = 0; $i < 5; $i++)
                <tr class="experience1" id="getRow_<?php echo $i?>" data-row="1">
                    <td>
                        {!! Form::text("previous[$i][company_name]", null, ['class' => 'form-control input-sm']) !!}
                    </td>
                    <td>
                        <div data-min-view="2"  class="input-group date datetimepicker1">
                            {!! Form::text("previous[$i][joining_date]", null, ['class' => 'form-control input-sm']) !!}
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                        </div>
                    </td>
                    <td>
                        <div data-min-view="2"  class="input-group date datetimepicker1">
                            {!! Form::text("previous[$i][leaving_date]", null, ['class' => 'form-control input-sm']) !!}
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                        </div>
                    </td>
                    <td>
                        {!! Form::select("previous[$i][nature_of_employment_id]", [''=>'-- Nature Of Employment --']+$natureOfEmployment, null ,['class' => 'form-control input-sm']) !!}
                    </td>
                    <td>
                        {!! Form::text("previous[$i][designation]", null, ['class' => 'form-control input-sm']) !!}
                    </td>
                    <td>
                        {!! Form::text("previous[$i][reason_of_leaving]", null, ['class' => 'form-control input-sm']) !!}
                    </td>
                    {{-- <td align="center"><button type="button" class="btn btn-danger experience_remove" data-row='1'><i class="mdi mdi-minus"></i></button></td> --}}
                </tr>
            @endfor
        @endif
    </tbody>
</table>
</div>

@if(count($previous) == 0)
<div class=" form-group col-sm-6"><label id="addRow" class="btn btn-success">Add Row</label></div>
@endif
@section('scripts')
    <script>
        datePicker();
        function datePicker(){
            $(".datetimepicker1").datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                componentIcon: '.mdi.mdi-calendar',
            /*    pickerPosition: "bottom-left", */
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });
        }

        $('.first_time_employment').on('change',function(e){
            if($(this).prop('checked')){
                $('#experience_form').submit();
            }
        });

        jQuery(document).ready(function($) {
            //$('.disable').attr('disabled','disabled');
        });
		var i = "<?php echo $i-1 ?>";
		var j = "<?php echo $i ?>";
		$("#addRow").on('click',function(){

			$("#experience_field").append('<tr class="experience1" id="getRow_'+j+'">'+$("#getRow_"+i).html()+' </tr>');
			j++;
			i++;
		});
    </script>
@endsection