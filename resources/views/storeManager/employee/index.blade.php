@extends('storeManager.layouts.backend')
@section('title',"Employee")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.confirmation_index')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
                        <p class="panel-head1">Employee List</p>
                        <span class="panel-subtitle">
                            <a href="{{ url('/uploads/attachment.zip') }}" title="Back" download="attachments" target="_blank">
                                <button class="btn btn-space btn-default">Download Joinning Kit</button>
                            </a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="employee-table" class="table table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Employee Code</th>
                                    <th>Name</th>
                                    <th>Designation</th>
                                    <th>Store Name</th>
                                    <th>Location</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/store/employee-data') }}";
        var edit_url = "{{ url('/store/employee') }}";
        var auth_check = "{{ Auth::check() }}";
        var upload_url = "{{ url('/store/employee/upload-documents') }}";

        datatable = $('#employee-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                // statusChange();
				highlight();
            },
			createdRow: function ( row, data, index ) {

				if ( data['rm_absconding_status'] == 2 || data['resignation_status'] == 'complete' ) {
                    $(row).addClass("danger");
					$('tr').addClass('success');

				}

			}
			,
            columns: [
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { data: 'designation',name : 'designation',"searchable": true, "orderable": false},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},
                { data: 'location_code',name : 'location_code',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d=r=l=u= "";

                        if(o.store_status == 0 || o.store_status == 3){
                            r = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/request-region' value="+o.id+" data-id="+o.id+" class='btn btn-warning btn-sm request_from_index' title='Request to Regional HR'><i class='mdi mdi-check-all' ></i></a>&nbsp;";
                            // a = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/approve' value="+o.id+" data-id="+o.id+" data-title='hrms' class='btn btn-success btn-sm approve_from_index' title='Approve'><i class='mdi mdi-redo' ></i></a>&nbsp;";
                        }
						var nclass = '';


						if(o.status == 'inactive'){
							nclass = 'highlight';
						}

                        v = "<a href='"+edit_url+"/"+o.id+"' title='View' class='btn btn-warning btn-sm' value="+o.id+" data-id="+o.id+" ><i class='mdi mdi-eye " + nclass +"' ></i></a>&nbsp;";

                        // if(o.store_status != 1){
                            e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit?employee=1' value="+o.id+" data-id="+o.id+" data-msg='employee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";
                        // }

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('store/employee')}} data-msg='employee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";

                        l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-success btn-sm' title='View Log' ><i class='mdi mdi-menu' aria-hidden='true'></i></button></a>&nbsp;";

                        u = "<a href='"+upload_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm' title='Upload Document'><i class='mdi mdi-cloud-upload' ></i></a>&nbsp;";

                        return v+e+u;
                    }

                }
            ]
        });
</script>
@endsection