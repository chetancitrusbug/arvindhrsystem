<div class="table-responsive table-responsive2">
    <table class="table table-condensed table-bordered">
        <tr class="text-center">
            <th style="width: 15%;">Relationship</th>
            <th>Gender</th>
            <th>Aadhar Number</th>
            <th>Name</th>
            <th style="width: 15%;">Date of birth as per Aadhar</th>
        </tr>
        <tbody>
            @php
                $family = old('family',isset($employee->family)?$employee->family:[]);
            @endphp
            @if(count($family))
                @foreach($family as $key=>$value)
                    <tr>
                        <td>
                            {!! Form::label($value['relation'], $value['relation'], ['class' => 'col-sm-12 lable label-default family_label']) !!}
                            {!! Form::hidden("family[$key][relation]", $value['relation'], ['class' => 'form-control input-sm']) !!}
                            @if(isset($value['id']) && !empty($value['id']))
                                {!! Form::hidden("family[$key][id]", $value['id'], ['class' => 'form-control input-sm']) !!}
                            @endif
                        </td>
                        <td>
                        <?php if($key == 0 && $value['gender'] == ''){
                            $value['gender'] = $employee->gender;
                        }?>
                            {!! Form::select("family[$key][gender]", [''=>'-- gender --']+config('constants.gender'), $value['gender'] ,['class' => 'form-control input-sm ']) !!}
                            {!! Form::hidden("family[$key][gender]", $value['gender'] ,[]) !!}
                        </td>
                        <?php if($key == 0 && $value['aadhar_number'] == ''){
                            $value['aadhar_number'] = $employee->aadhar_number;
                        }?>
                        <td class='{{ $errors->has("family.$key.aadhar_number") ? "has-error" : ""}}'>
                            {!! Form::text("family[$key][aadhar_number]", $value['aadhar_number'], ['class' => 'form-control input-sm ']) !!}
                                <?php /*{!! Form::hidden("family[$key][aadhar_number]", $value['aadhar_number'] ,[]) !!} */?>
                            {!! $errors->first("family.$key.aadhar_number", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("family.$key.name") ? "has-error" : ""}}'>
                            {!! Form::text("family[$key][name]", $value['name'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("family.$key.name", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("family.$key.date") ? "has-error" : ""}}'>
                            <div data-min-view="2"  class="input-group {{ !(isset($is_employee) && $is_employee ==1)?'date datetimepicker1':'' }}">
                                {!! Form::text("family[$key][date]", ($value['date'] != '--' && $value['date'] != '//')?$value['date']:'', ['class' => 'form-control input-sm ']) !!}
                                {!! Form::hidden("family[$key][date]", $value['date'] ,[]) !!}
                                <span class="input-group-addon btn btn-primary "><i class="icon-th mdi mdi-calendar"></i></span>
                            </div>
                            {!! $errors->first("family.$key.date", '<p class="help-block">:message</p>') !!}
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>

@section('scripts')
    <script>
        datePicker();
        function datePicker(){
            $(".datetimepicker1").datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                componentIcon: '.mdi.mdi-calendar',
            /*    pickerPosition: "bottom-left", */
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });
        }

        jQuery(document).ready(function($) {
            $('.disable').attr('disabled','disabled');
        });
    </script>
@endsection