<div class="form-group col-sm-12{{ $errors->has('process_status') ? ' has-error' : ''}}">
    {!! Form::label('process_status', 'Process Status: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('process_status', [''=>'-- Select Process Status --']+array('YES'=>'Active','NO'=>'Inactive'), isset($otherInfo->process_status)?$otherInfo->process_status:null ,['class' => 'form-control select2
        input-sm process_status']) !!} {!! $errors->first('process_status', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12 dateofleaving{{ $errors->has('dol') ? ' has-error' : ''}} ">
    {!! Form::label('dol', 'Date of Leaving: ', ['class' => 'col-sm-3 control-label','data-date-format'=>"dd-mm-yyyy"])
    !!}
    <div class="col-sm-6 col-xs-12">
        <div data-min-view="2" class="input-group {{ !(isset($is_employee) && $is_employee ==1)?'date datetimepicker1':'' }}">
            <input size="16" type="text" value="{{old('dol',isset($otherInfo->dol)?$otherInfo->dol:null)}}" class="form-control input-sm disable"
                data-date-format="dd-mm-yyyy H:i" name="dol"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
        </div>
        {!! $errors->first('dol', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group col-sm-12{{ $errors->has('appointment_latter') ? ' has-error' : ''}}">
    {!! Form::label('appointment_latter', 'Appointment Latter Given?: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('appointment_latter', [''=>'-- Select Appointment Latter Status --']+array('YES'=>'YES','NO'=>'NO'), isset($otherInfo->appointment_status)?$otherInfo->appointment_status:null ,['class' => 'form-control select2
        input-sm']) !!} {!! $errors->first('appointment_latter', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('id_card_status') ? ' has-error' : ''}}">
    {!! Form::label('id_card_status', 'ID Card Generated: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('id_card_status', [''=>'-- Select ID Card Generated --']+array('YES'=>'YES','NO'=>'NO'), isset($otherInfo->id_card_status)?$otherInfo->id_card_status:null ,['class' =>
        'form-control select2 input-sm']) !!} {!! $errors->first('id_card_status', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group col-sm-12{{ $errors->has('uniform_status') ? ' has-error' : ''}}">
    {!! Form::label('uniform_status', 'Uniform : ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('uniform_status', [''=>'-- Select Uniform Generated --']+array('YES'=>'YES','NO'=>'NO'), isset($otherInfo->uniform_status)?$otherInfo->uniform_status:null ,['class' => 'form-control
        select2 input-sm']) !!} {!! $errors->first('uniform_status', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group col-sm-12{{ $errors->has('L0L1_status') ? ' has-error' : ''}}">
    {!! Form::label('L0L1_status', 'L0-L1 Attended: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('L0L1_status', [''=>'-- Select L0L1_status Status --']+array('YES'=>'YES','NO'=>'NO'), isset($otherInfo->l0l1_status)?$otherInfo->l0l1_status:null ,['class' =>
        'form-control select2 input-sm']) !!} {!! $errors->first('L0L1_status', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12 show_l0l1_option {{ $errors->has('l0l1_result') ? ' has-error' : ''}}">
    {!! Form::label('l0l1_result', 'L0-L1 Result: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('l0l1_result', [''=>'-- Select l0l1_result Status --']+array('Fail','Pass'), isset($otherInfo->l0l1_result)?$otherInfo->l0l1_result:null
        ,['class' => 'form-control select2 input-sm']) !!} {!! $errors->first('l0l1_result', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group col-sm-12 show_l0l1_option {{ $errors->has('L0L1_certificate') ? ' has-error' : ''}}">
    {!! Form::label('L0L1_certificate', 'L0-L1 Certificate: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('L0L1_certificate', array('NO'=>'Not Given','YES'=>'Given'), isset($otherInfo->l0l1_certificate_status)?$otherInfo->l0l1_certificate_status:null ,['class' => 'form-control
        select2 input-sm']) !!} {!! $errors->first('L0L1_certificate', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group col-sm-12{{ $errors->has('background_verification') ? ' has-error' : ''}}">
    {!! Form::label('background_verification', 'Background Verification: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('background_verification', [''=>'-- Select background_verification --']+array('N/A'=>'N/A','YES'=>'YES','NO'=>'NO'),
        isset($otherInfo->background_verification)?$otherInfo->background_verification:null ,['class' => 'form-control select2 input-sm']) !!} {!! $errors->first('background_verification', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('bv_report') ? ' has-error' : ''}}">
    {!! Form::label('bv_report', 'BV Report: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('bv_report', [''=>'-- Select BV Report Status --']+array('Verified'=>'Verified','Non verified'=>'Non verified'),
        isset($otherInfo->bv_report_status)?$otherInfo->bv_report_status:null ,['class' => 'form-control select2 input-sm']) !!} {!! $errors->first('bv_report', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group col-sm-12{{ $errors->has('is_disable') ? ' has-error' : ''}}">
    {!! Form::label('is_disable', 'Is PANKH: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('is_disable', [''=>'-- Select is PANKH --']+array('YES'=>'YES','NO'=>'NO'), isset($otherInfo->is_disabled)?$otherInfo->is_disabled:'' ,['class' => 'form-control select2
        input-sm']) !!} {!! $errors->first('bv_report', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div id="disability_desc" style="display:none" class="form-group{{ $errors->has('disability_description') ? ' has-error' : ''}}">
    {!! Form::label('disability_description', '* PANKH Description : ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('disability_description', isset($otherInfo->disability_desc)?$otherInfo->disability_desc:'', ['class' => 'form-control input-sm disable']) !!} {!! $errors->first('disability_description',
        '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('allocated_leave') ? ' has-error' : ''}}">
    {!! Form::label('allocated_leave', '* Allocated Leave: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('allocated_leave', isset($otherInfo->allocated_leave)?$otherInfo->allocated_leave:'', ['class' => 'form-control input-sm disable']) !!} {!! $errors->first('allocated_leave',
        '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('availed_leave') ? ' has-error' : ''}}">
    {!! Form::label('availed_leave', '* Availed Leave: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('availed_leave', isset($otherInfo->availed_leave)?$otherInfo->availed_leave:'', ['class' => 'form-control input-sm disable']) !!} {!! $errors->first('availed_leave',
        '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('pending_leave') ? ' has-error' : ''}}">
    {!! Form::label('pending_leave', '* Pending Leave: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('pending_leave', isset($otherInfo->pending_leave)?$otherInfo->pending_leave:'', ['class' => 'form-control input-sm disable']) !!} {!! $errors->first('pending_leave',
        '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('salary') ? ' has-error' : ''}}">
    {!! Form::label('salary', '* Salary(CTC): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::number('salary', isset($otherInfo->salary)?$otherInfo->salary:'', ['class' => 'form-control input-sm disable']) !!} {!! $errors->first('salary',
        '
        <p class="help-block">:message</p>') !!}
    </div>
</div>

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"
/>
@endsection
@section('scripts')
<script>

    datePicker();
        function datePicker(){
            $(".datetimepicker1").datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                componentIcon: '.mdi.mdi-calendar',
            /*   pickerPosition: "bottom-left", */
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });
        }
		$(".dateofleaving").hide();
        //model dependency on department
        $('select[name="is_disable"]').on('change', function(event) {
            if($(this).val() == 'YES' ){
                $("#disability_desc").show();
            } else {
                $("#disability_desc").hide();
            }
        });
        $('select[name="L0L1_status"]').on('change', function(event) {
            if($(this).val() == 'YES' ){
                $(".show_l0l1_option").show();
            } else {
                $(".show_l0l1_option").hide();
                $('#l0l1_result').val('').change();
            }
        });
        $('select[name="l0l1_result"]').on('change', function(event) {
            if($(this).val() == 1 ){
                $("#L0L1_certificate").removeAttr('disabled');
            } else {
                $('#L0L1_certificate').val('NO').change();
                $("#L0L1_certificate").attr('disabled',true);
            }
        });

        jQuery(document).ready(function($) {
			
            if($("#is_disable").val() == 'YES'){
                $("#disability_desc").show();
            }
            else {
                $("#disability_desc").hide();
            }
            if($("#L0L1_status").val() == 'YES' ){
                $(".show_l0l1_option").show();
            } else {
                $(".show_l0l1_option").hide();
                $('#l0l1_result').val('').change();
            }
            if($("#l0l1_result").val() == 1 ){
                $("#L0L1_certificate").removeAttr('disabled');
            } else {
                $("#L0L1_certificate").attr('disabled',true);
            }
			$(".process_status").change(function(){
				if($(this).val() == 'NO'){
					$(".dateofleaving").show();
				}else{
					$(".dateofleaving").hide();
				}
			})
			$(".process_status").trigger('change');
        });

	


</script>
@endsection