<div class="form-group {{ $errors->has('excel') ? ' has-error' : ''}}">
    {!! Form::label('excel', '* Excel: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('excel', null, ['class' => 'form-control input-sm']) !!}
		
        {!! $errors->first('excel', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group ">
    Note : Please Upload data in given formate only.
</div>