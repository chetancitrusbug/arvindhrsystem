@extends('storeManager.layouts.backend')
@section('title',"Leave Report")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.confirmation_index')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Leave Report
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-primary">Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="employee-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Employee Code</th>
                                    <th>Employee Name</th>
                                    <th>Allocated Leave</th>
                                    <th>Pending Leave</th>
                                    <th>Availed Leaves</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Total</th>
                                    <th>Allocated Leave</th>
                                    <th>Pending Leave</th>
                                    <th>Availed leaves</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('/store/leave-report-employee-data') }}/{{Auth::user()->store_id}}";
        var edit_url = "{{ url('/store/hiring') }}";
        var upload_url = "{{ url('/store/upload-documents') }}";
        var hold_url = "{{ url('/store/hold') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom: 'Blfrtip', buttons: ['excel'],
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                highlight();
            },
            columns: [
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
                { data: 'employee_name',name : 'employee_name',"searchable": true, "orderable": true},
                { data: 'allocated_leave',name : 'allocated_leave',"searchable": true, "orderable": false},
                { data: 'pending_leave',name : 'pending_leave',"searchable": true, "orderable": false},
                { data: 'availed_leave',name : 'availed_leave',"searchable": true, "orderable": false},
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_leave = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_pending_l = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_alived_leave = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total_leave );
            $( api.column( 3 ).footer() ).html( total_pending_l );
            $( api.column( 4 ).footer() ).html( total_alived_leave );

        }
        });

</script>
@endsection