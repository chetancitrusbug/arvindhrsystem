@extends('storeManager.layouts.backend')

@section('content')
      <style>.clickable-row{cursor:pointer}</style>
	<div class="be-content">
	    <div class="main-content container-fluid">
          <div class="row">
			<div class="col-md-12">
				<div class="widget widget-fullwidth">
				  <div class="widget-head">
					<div class="tools"></div><span class="title">Manpower Report - Store (<small>{{$store_location->store_name}}</small>)</span><span class="description"></span>
				  </div>
				  <div class="widget-chart-container">
					<div class="chart-table xs-pt-15">
					  <table class="table table-striped">

						<thead>
							<tr>
								<th>Store Name</th>
								<th>Store Code</th>
								<th>Region</th>
								<th>Budget Man Power</th>
								<th>Actual Man Power</th>
								<th>Gap</th>
							</tr>
						</thead>
						<tbody class="no-border-x">
						@php

						@endphp
						  <tr>
							<td>{{$manPowerGet->store_name}}</td>
							<td>{{$manPowerGet->io_code}}</td>
							<td>{{$manPowerGet->name}}</td>
							<td>{{($manPowerGet->budget_man_power > 0)? $manPowerGet->budget_man_power : 0 }}</td>
							<td>{{($manPowerGet->actual_man_power > 0)? $manPowerGet->actual_man_power  : 0 }}</td>
							<td>{{($manPowerGet->budget_man_power > 0 && $manPowerGet->actual_man_power > 0)? $manPowerGet->budget_man_power - $manPowerGet->actual_man_power : 0 }}</td>

						  </tr>
						@php

						@endphp
						</tbody>
					  </table>
					</div>
				  </div>
				</div>
            </div>
			<div class="col-md-12">
				<div class="widget widget-fullwidth">
				  <div class="widget-head">
					<div class="tools"></div><span class="title">Manpower Cost Report - Store (<small>{{$store_location->store_name}}</small>)</span><span class="description"></span>
				  </div>
				  <div class="widget-chart-container">
					<div class="chart-table xs-pt-15">
					  <table class="table table-striped">
						<thead>
							<tr>
								<th>Store Name</th>
								<th>Store Code</th>
								<th>Region</th>
								<th>Employee</th>
								<th>Avg Cost</th>
						  </tr>
						</thead>

						<tbody class="no-border-x">
						  <tr class="clickable-row"  data-href="{{ url('regional/manpowercost-report/store/') }}">
							<td>{{$manPowerGet->store_name}}</td>
							<td>{{$manPowerGet->io_code}}</td>
							<td>{{$manPowerGet->name}}</td>
							@if(!is_array($averageCost))
								<td>0</td>
								<td>0</td>
							@elseif(count($averageCost) > 0)
								<td>{{($averageCost['total_employee'] > 0 )? $averageCost['total_employee'] : 0 }}</td>
								<td>{{($averageCost['avg_cost'] > 0 )? $averageCost['avg_cost'] : 0 }}</td>
							@endif
						  </tr>

						</tbody>
					  </table>
					</div>
				  </div>
				</div>
            </div>
			<div class="col-md-12">
				<div class="widget widget-fullwidth">
				  <div class="widget-head">
					<div class="tools"></div><span class="title">Hygiene Report - Store (<small>{{$store_location->store_name}}</small>) </span><span class="description"></span>
				  </div>
				  <div class="widget-chart-container">
					<div class="chart-table xs-pt-15">
					  <table class="table table-striped">
						<thead>
							<tr>
								<th>Store Name</th>
								<th>Store Code</th>
								<th>Region</th>
								<th>Id Card Not Available</th>
								<th>Uniform not available</th>
								<th>L0L1 untrained</th>
								<th>L0L1 non certified</th>
								<th>Certificate pending</th>
								<th>Appointment Letter</th>
						    </tr>

						</thead>
						<tbody class="no-border-x">
						@php

						@endphp
						  <tr class="clickable-row">
							<td>{{$manPowerGet->store_name}}</td>
							<td>{{$manPowerGet->io_code}}</td>
							<td>{{$manPowerGet->name}}</td>
							<td>{{($hygiene['id_card'] > 0 )? $hygiene['id_card'] : 0 }}</td>
							<td>{{($hygiene['uniform'] > 0 )? $hygiene['uniform'] : 0 }}</td>
							<td>{{($hygiene['l0l1_untrained'] > 0 )? $hygiene['l0l1_untrained'] : 0 }}</td>
							<td>{{($hygiene['l0l1_certified'] > 0 )? $hygiene['l0l1_certified'] : 0 }}</td>
							<td>{{($hygiene['certificate_pending'] > 0 )? $hygiene['certificate_pending'] : 0 }}</td>
							<td>{{($hygiene['appoint_letter'] > 0 )? $hygiene['appoint_letter'] : 0 }}</td>

						  </tr>
						@php

						@endphp
						</tbody>
					  </table>
					</div>
				  </div>
				</div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Hiring - Report (YTD) </span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <table class="table table-striped">
                                <thead class="primary">
                                    <tr>
                                        <th>Month Year</th>
                                        <th>GAP</th>
                                        <th>Hired</th>
                                        <th>Pending</th>
                                    </tr>
                                </thead>
                                <tbody class="no-border-x">
                                    @foreach ($hiringReport as $hiring)
                                    <tr>
                                        <td>
                                            @php
                                                echo date('M-Y',strtotime('01-'.$hiring->monthyear));
                                            @endphp
                                        </td>
                                        <td>{{($hiring->gap > 0) ? $hiring->gap : 0 }}</td>
                                        <td>{{($hiring->hired > 0) ? $hiring->hired : 0 }}</td>
                                        <td>{{($hiring->pending > 0) ? $hiring->pending : 0 }}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div>
                        <span class="title">Additional Reports - Store</span>
                        <span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15 row">
                            <div class="col-sm-6">
                                <div class="widget-head">
                                    <div class="tools"></div>
                                    <span class="title">Gender wise manpower</span>
                                    <span class="description"></span>
                                </div>
                                <table class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Total</th>
                                            <th>Male</th>
                                            <th>Female</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if (count($genderwisetotalemployee) > 0)
                                        <tr>
                                            <td>{{($genderwisetotalemployee['total'] > 0) ? $genderwisetotalemployee['total'] : 0 }}</td>
                                            <td>{{($genderwisetotalemployee['male'] > 0) ? $genderwisetotalemployee['male'] : 0 }}</td>
                                            <td>{{($genderwisetotalemployee['female'] > 0) ? $genderwisetotalemployee['female'] : 0 }}</td>
                                        </tr>
                                        @else
                                        <tr>
                                            <td>No Data Found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-sm-6">
                                <div class="widget-head">
                                    <span class="title">Gender wise hiring</span>
                                    <span class="description"></span>
                                </div>
                                <table class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Total</th>
                                            <th>Male</th>
                                            <th>Female</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if (count($hiring_employee) > 0)
                                        <tr>
                                            <tr>
                                                <td>{{($hiring_employee['total'] > 0) ? $hiring_employee['total'] : 0 }}</td>
                                                <td>{{($hiring_employee['male'] > 0) ? $hiring_employee['male'] : 0 }}</td>
                                                <td>{{($hiring_employee['female'] > 0) ? $hiring_employee['female'] : 0 }}</td>
                                            </tr>
                                        </tr>
                                        @else
                                        <tr>
                                            <td>No Data Found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-sm-12">
                                <div class="widget-head">
                                    <div class="tools"></div>
                                    <span class="title">Gender wise attrition</span>
                                    <span class="description"></span>
                                </div>
                                <table class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th rowspan="2">Gender</th>
                                            <th>Male</th>
                                            <th>Female</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if (count($genderwiseattrition) > 0)
                                        <tr>
                                            <tr>
                                                <td></td>
                                                <td>{{($genderwiseattrition['male'] > 0) ? $genderwiseattrition['male'] : 0 }}</td>
                                                <td>{{($genderwiseattrition['female'] > 0) ? $genderwiseattrition['female'] : 0 }}</td>
                                            </tr>
                                        </tr>
                                        @else
                                        <tr>
                                            <td>No Data Found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Hiring - Report (MTD) </span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <table class="table table-striped">
                                <thead class="primary">
                                    <tr>
                                        <th>GAP</th>
                                        <th>In Process</th>
                                        <th>Hired</th>
                                        <th>Pending</th>
                                    </tr>
                                </thead>
                                <tbody class="no-border-x">
                                    @if ($totalattritionReport)
                                        <tr>
                                            <td>{{($totalattritionReport->gap > 0) ? $totalattritionReport->gap : 0 }}</td>
                                            <td>{{($total_pending_employee->total_pending > 0) ? $total_pending_employee->total_pending : 0 }}</td>
                                            <td>{{($totalattritionReport->hired > 0) ? $totalattritionReport->hired : 0 }}</td>
                                            <td>{{($totalattritionReport->pending > 0) ? $totalattritionReport->pending : 0 }}</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Attrition - Report (MTD) </span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <table class="table table-striped">
                                <thead class="primary">
                                    <tr>
                                        <th>Store</th>
                                        <th>Region</th>
                                        <th>Total</th>
                                        <th>Resignation</th>
                                        <th>Absconding</th>
                                    </tr>
                                </thead>
                                @if($totalattritionReport)
                                    <tbody class="no-border-x">
                                        <tr>
                                            <td>{{$store_location->store_name}}</td>
                                            <td>{{$store_location->name}}</td>
                                            <td>{{($totalattritionReport->Total > 0) ? $totalattritionReport->Total : 0 }}</td>
                                            <td>{{($totalattritionReport->resignation > 0) ? $totalattritionReport->resignation : 0 }}</td>
                                            <td>{{($totalattritionReport->absconding > 0) ? $totalattritionReport->absconding : 0 }}</td>
                                        </tr>
                                    </tbody>
                                @else
                                    NO Data Found
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Attrition Aging - Report </span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <table class="table table-striped">
                                <thead class="primary">
                                    <tr>
                                        <th>Total</th>
                                        <th>Infant attrition Within month</th>
                                        <th>Baby attrition Within a quarter</th>
                                        <th>Attrition</th>
                                    </tr>
                                </thead>
                                @if($agingReport)
                                <tbody class="no-border-x">
                                    <tr>
                                        <td>{{($agingReport['total'] > 0) ? $agingReport['total'] : 0 }}</td>
                                        <td>{{($agingReport['infant_attrition'] > 0) ? $agingReport['infant_attrition'] : 0 }}</td>

                                        <td>{{($agingReport['baby_attrition'] > 0) ? $agingReport['baby_attrition'] : 0 }}</td>
                                        <td>{{($agingReport['attrition'] > 0) ? $agingReport['attrition'] : 0 }}</td>
                                    </tr>
                                </tbody>
                                @else
                                    <tr>
                                        <td>No Data Found</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Attrition report - By percetage of live manpower</span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <table id="man-powerpercentage-report-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Total Man Power</th>
                                        <th>Total Attrition</th>
                                        <th>Percentage</th>
                                    </tr>
                                </thead>
                                @if ($manpowerPercentageReport)
                                <tbody>
                                    <tr>
                                        <td>{{($manpowerPercentageReport->total_actual_man_power)?$manpowerPercentageReport->total_actual_man_power:0}}</td>
                                        <td>{{($manpowerPercentageReport->Total)?$manpowerPercentageReport->Total:0}}</td>
                                        <td>
                                            @if ($manpowerPercentageReport->total_actual_man_power > 0 && $manpowerPercentageReport->Total > 0)
                                                @php
                                                    echo round(($manpowerPercentageReport->total_actual_man_power/$manpowerPercentageReport->Total)*100,2);
                                                @endphp %
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                                @else
                                <tr>
                                    <td>
                                        No Data Found
                                    </td>
                                </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Hiring - Inprocess Report </span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <table class="table table-striped">
                                <thead class="primary">
                                    <tr>
                                        <th>Pending at Store</th>
                                        <th>Pending at RHR</th>
                                        <th>Peding at HRMS</th>
                                    </tr>
                                </thead>
                                <tbody class="no-border-x">
                                    @if ($hiringinprocessreport)
                                        <tr>
                                            <td>{{$hiringinprocessreport['store_pending']}}</td>
                                            <td>{{$hiringinprocessreport['region_pending']}}</td>
                                            <td>{{$hiringinprocessreport['hrms_pending']}}</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Productivity report by manpower </span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <table class="table table-striped">
                                <thead class="primary">
                                    <tr>
                                        <th>Sales</th>
                                        <th>No. of employee</th>
                                        <th>Sales per employee</th>
                                    </tr>
                                </thead>
                                <tbody class="no-border-x">
                                    @if ($productivity_report)
                                    <tr>
                                        <td>{{$productivity_report['total_amount']}}</td>
                                        <td>{{$productivity_report['total_employee']}}</td>
                                        <td>{{$productivity_report['per_person_income']}}</td>
                                    </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Productivity report by manpower cost </span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <table class="table table-striped">
                                <thead class="primary">
                                    <tr>
                                        <th>Sales</th>
                                        <th>Avg cost per emp</th>
                                        <th>Sales per employee</th>
                                    </tr>
                                </thead>
                                <tbody class="no-border-x">
                                    @if ($productivity_report)
                                    <tr>
                                        <td>{{$productivity_report['total_amount']}}</td>
                                        <td>{{$productivity_report['avg_ctc']}}</td>
                                        <td>{{$productivity_report['per_person_income']}}</td>
                                    </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools">
                            <a href="{{ url('store/leave-report-employee/') }}" class="btn btn-primary">Employee Wise</a>
                        </div>
                        <span class="title">Leave Report </span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <table class="table table-striped">
                                <thead class="primary">
                                    <tr>
                                        <th>Allocated Leave</th>
                                        <th>Pending Leave</th>
                                        <th>Availed Leaves</th>
                                    </tr>
                                </thead>
                                <tbody class="no-border-x">
                                    @if ($leaveReport)
                                        <tr>
                                            <td>{{$leaveReport->total_allocated_leave}}</td>
                                            <td>{{$leaveReport->total_pending_leave}}</td>
                                            <td>{{$leaveReport->availed_leave}}</td>
                                        </tr>
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools">
                            <a href="{{ url('store/attendance-report/employee') }}" class="btn btn-primary">Employee Wise</a>
                        </div>
                        <span class="title">Attendance Report ({{\Carbon\Carbon::now()->startOfDay()->subDays(1)->format("d/m/Y")}})</span><span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <table id="attendance-report-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Live Manpower</th>
                                        <th>Present Manpower</th>
                                        <th>Present Manpower (%)</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>{{$getAttendanceReportData['actual_man_power']}}</td>
                                        <td>{{$getAttendanceReportData['present_man_power']}}</td>
                                        <td>{{$getAttendanceReportData['present_per']}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


          </div>
        </div>
	</div>

@endsection

@section('scripts')
      <?php if(Session::has('flash_success') &&  Session::get('flash_success') !='' ){
      ?>
      <script>

        $(document).ready(function(){
          $('document').find('#success').trigger('click');
          $('#success').click(function(){
            alert('here');
          })
        });
      </script>
    <?php } ?>
	<script>
		/*$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		}); */
	</script>
@endsection

@push('modal')
  @include('storeManager.modals.backendSucess')
@endpush
