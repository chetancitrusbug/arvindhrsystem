<div id="reject" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title">Reject by Regional HR</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>
            <div class="form-group">
                <label>Reason for Reject</label>
                <textarea class="form-control" name="reject_reason" id="reject_reason"></textarea>
                <p style="color: red;" class="reject_reason_error"></p>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
            <button type="submit" data-dismiss="modal" class="btn btn-success submit-reject">Proceed</button>
        </div>
    </div>
</div>