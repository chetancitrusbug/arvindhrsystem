<!-- success Modal-->
<div id="attendanceConfirmation" class="modal-container modal-full-color modal-full-color-success modal-effect-8">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
        </div>
        <div class="modal-body">
            <div class="text-center"><span class="modal-main-icon mdi mdi-check"></span>
                <h3 class="confirmation-title">Are you sure to make change in attendance?</h3>
                <p class='success-text'> </p>
                <div class="xs-mt-50">
                    <button type="button" data-dismiss="modal" class="btn btn-default btn-space att-confirm-cancel">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-success btn-space att-confirm">Approve</button>
                </div>
            </div>
        </div>
        <div class="modal-footer"></div>
    </div>
</div>
<!-- Nifty Modal-->