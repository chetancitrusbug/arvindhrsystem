<div id="sendForword" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_forword_title">Approve by Regional HR</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>
            <div class="form-group">
                <div class="form-group col-sm-12{{ $errors->has('source_category') ? ' has-error' : ''}}">
                    {!! Form::label('source_category', '* Source Category: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::select('source_category', [''=>'--select source category--']+$sourceCategory,null, ['class' => 'form-control input-sm source_category']) !!}
                        <p style="color: red;" class="source_category_error error hide"></p>
                    </div>
                </div>
                <input name="category_name" value="" type="hidden"/>
                <div class="form-group col-sm-12 source_name{{ $errors->has('source_name') ? ' has-error' : ''}}">
                    {!! Form::label('source_name', '* Source Name: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::select('source_name', [''=>'--select source name--'],null ,['class' => 'form-control input-sm']) !!}
                        <p style="color: red;" class="source_name_error error hide"></p>
                    </div>
                </div>
                <div class="form-group col-sm-12 source_code{{ $errors->has('source_code') ? ' has-error' : ''}}">
                    {!! Form::label('source_code', '* Source Code: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::text('source_code', null, ['class' => 'form-control input-sm','disabled']) !!}
                        <p style="color: red;" class="source_code_error error hide"></p>
                    </div>
                </div>
                <div class="form-group col-sm-12 employee_name{{ $errors->has('employee_name') ? ' has-error' : ''}}">
                    {!! Form::label('employee_name', '* Employee Name: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::text('employee_name', null, ['class' => 'form-control input-sm']) !!}
                        {!! Form::hidden('employee_name', null) !!}
                        <p style="color: red;" class="employee_name_error error hide"></p>
                    </div>
                </div>
                <div class="form-group col-sm-12 employee_code{{ $errors->has('employee_code') ? ' has-error' : ''}}">
                    {!! Form::label('employee_code', '* Employee Code: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::text('employee_code', null, ['class' => 'form-control input-sm']) !!}
                        {!! Form::hidden('employee_code', null) !!}
                        <p style="color: red;" class="employee_code_error error hide"></p>
                    </div>
                </div>
                <div class="form-group col-sm-12 old_employee_code{{ $errors->has('old_employee_code') ? ' has-error' : ''}}">
                    {!! Form::label('old_employee_code', '*Old Employee Code: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::text('old_employee_code', null, ['class' => 'form-control input-sm']) !!}
                        {!! Form::hidden('old_employee_code', null) !!}
                        <p style="color: red;" class="old_employee_code_error error hide"></p>
                    </div>
                </div>
                <div class="form-group col-sm-12 employee_refferal_amount{{ $errors->has('employee_refferal_amount') ? ' has-error' : ''}}">
                    {!! Form::label('employee_refferal_amount', '* Employee refferal amount to be paid: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::number('employee_refferal_amount', null, ['class' => 'form-control input-sm checkNumber employee_refferal_amount']) !!}
                        <p style="color: red;" class="employee_refferal_amount_error error hide"></p>
                    </div>
                </div>
                <div class="form-group col-sm-12 employee_refferal_payment_month{{ $errors->has('employee_refferal_payment_month') ? ' has-error' : ''}}">
                    {!! Form::label('employee_refferal_payment_month', '* Employee referral payout month: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::select('employee_refferal_payment_month', [''=>'-- Select Employee referral payout month --']+config('constants.month_list'), null ,['class' => 'form-control input-sm employee_refferal_payment_month select2_model']) !!}
                        <p style="color: red;" class="employee_refferal_payment_month_error error hide"></p>
                    </div>
                </div>
                <div class="form-group col-sm-12{{ $errors->has('grade_id') ? ' has-error' : ''}}">
                    {!! Form::label('grade_id', '* Grade: ', ['class' => 'col-sm-3 control-label']) !!}
                    <div class="col-sm-6">
                        {!! Form::select('grade_id', [''=>'-- Select Grade --']+$grades, null ,['class' => 'form-control input-sm grade_id']) !!}
                        <p style="color: red;" class="grade_id_error error hide"></p>
                    </div>
                </div>
                <div class="form-group col-sm-12{{ $errors->has('joining_date') ? ' has-error' : ''}}">
                    {!! Form::label('joining_date', 'Joining Date: ', ['class' => 'col-sm-3 control-label','data-date-format'=>"dd-mm-yyyy"]) !!}
                    <div class="col-sm-6 col-xs-12">
                        <div data-min-view="2"  class="input-group date datetimepicker1">
                            {!! Form::text('joining_date', null, ['class' => 'form-control input-sm joining_date']) !!}
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                        </div>
                        <p style="color: red;" class="joining_date_error error hide"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
            <button type="submit" data-dismiss="modal" class="btn btn-success submit-forward-region">Proceed</button>
        </div>
    </div>
</div>