<!-- success Modal-->
<div id="attendanceConfirmation" class="modal-container modal-full-color modal-full-color-success modal-effect-8">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
        </div>
        <div class="modal-body">
            <div class="text-center"><span class="modal-main-icon mdi mdi-check"></span>
                <h3 class="confirmation-title">Are you sure to request Region Manager for attandence change?</h3>
                <div class="xs-mt-50">
                    <button type="button" data-dismiss="modal" class="btn btn-default btn-space att-confirm-cancel">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-success btn-space att-confirm">Send</button>
                </div>
            </div>
        </div>
        <div class="modal-footer"></div>
    </div>
</div>
<!-- Nifty Modal-->

<div id="change-att-model" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_forword_title">Add Comment</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>
            <div class="form-group">
                <div class="form-group col-sm-12 note{{ $errors->has('note') ? ' has-error' : ''}}">
                    {!! Form::textarea('note', null, ['class' => 'form-control input-sm']) !!}
                    <p style="color: red;" class="note_error error hide"></p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default change-close">Cancel</button>
            <button type="submit" data-dismiss="modal" class="btn btn-success change-submit" style="margin-right:2.5%;">Proceed</button>
        </div>
    </div>
</div>