<!-- success Modal-->
<div id="success" class="modal-container modal-full-color modal-full-color-success modal-effect-8">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
        </div>
        <div class="modal-body">
            <div class="text-center"><span class="modal-main-icon mdi mdi-check"></span>
                <h3>Success</h3>
                <p class='success-text'> </p>
                {{-- <div class="xs-mt-50">
                    <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-success btn-space success-close">Proceed</button>
                </div> --}}
            </div>
        </div>
        <div class="modal-footer"></div>
    </div>
</div>
<!-- Nifty Modal-->

<!-- Nifty Modal-->
<div id="danger" class="modal-container modal-full-color modal-full-color-danger modal-effect-8">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
        </div>
        <div class="modal-body">
            <div class="text-center"><span class="modal-main-icon mdi mdi-close-circle-o"></span>
                <h3 class="delete-title">Delete Confirm</h3>
                <p class='delete-confirm-text'> </p>
                <div class="xs-mt-50">
                    <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-success btn-space delete-confirm">Proceed</button>
                </div>
            </div>
        </div>
        <div class="modal-footer"></div>
    </div>
</div>
<!-- Nifty Modal-->

<!-- Nifty Modal-->
  <div id="warning" class="modal-container modal-full-color modal-full-color-warning modal-effect-8">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
      </div>
      <div class="modal-body">
        <div class="text-center"><span class="modal-main-icon mdi mdi-alert-triangle"></span>
          <h3 class="warning-title">Warning</h3>
          <p class='warning-confirm-text'></p>
          <div class="xs-mt-50">
            <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
            <button type="button" data-dismiss="modal" class="btn btn-success btn-space warning-confirm">Proceed</button>
          </div>
        </div>
      </div>
      <div class="modal-footer"></div>
    </div>
  </div>

<div id="sendBack" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_back_title"></h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>
            <div class="form-group">
                <label>Note</label>
                <textarea type="note" class="form-control" name="note" id="add_note"></textarea>
                <p style="color: red;" class="note_error"></p>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
            <button type="submit" data-dismiss="modal" class="btn btn-success submit-note">Proceed</button>
        </div>
    </div>
</div>