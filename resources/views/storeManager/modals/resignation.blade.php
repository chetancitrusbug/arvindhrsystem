<div id="resignation" class="modal-container colored-header colored-header-success custom-width modal-effect-9 custom-modal1 custom-modal2">
    <div class="modal-content">
		<form action="{{url('store/resignation')}}" enctype="multipart/form-data" method="POST" >
		{{ csrf_field() }}
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_forword_title">Resignation Request Create</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>
					<div class="form-group col-sm-6">
                      {!! Form::label('employee_id', '* Employee: ', ['class' => '']) !!}
					  {!! Form::select('employee_id', [''=>'--select employee--']+$employee,null, ['class' => 'col-sm-12 form-control input-sm employee_id']) !!}
                      <p style="color: red;" class="employee_id_error error hide"></p>
                    </div>

                    <div class="form-group col-sm-12">
                        {!! Form::label('reason_id', '* Reason: ', ['class' => '']) !!} {!! Form::select('reason_id', [''=>'--select Reasion--']+$reason_parent,null,
                        ['class' => 'col-sm-12 form-control input-sm reason_id','id'=>'reason_id']) !!}
                        <p style="color: red;" class="employee_id_error error hide"></p>
                    </div>
					<div class="form-group col-sm-12">
                        {!! Form::label('sub_reason_id', '* Sub Reason: ', ['class' => '']) !!}
                         {!! Form::select('sub_reason_id', [''=>'--select Sub Reasion--'],null,
                        ['class' => 'col-sm-12 form-control input-sm reason_id','id'=>'sub_reason_id']) !!}
                        <p style="color: red;" class="employee_id_error error hide"></p>

                    </div>

                    <div class="form-group col-sm-6">
                        {!! Form::label('', '* Date Of Resignation: ', ['class' => '']) !!}
                        <div data-min-view="2" id="date_of_resignation" data-date-format="yyyy-mm-dd" class="input-group date ">
                            <input size="16" id="lw_date" name="date_of_resignation" type="text" value="" class="form-control input-sm input-height-39">
                            <span
                                class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                        </div>

                    </div>
                    <div class="form-group col-sm-6">
                        {!! Form::label('', '* Last Working Date: ', ['class' => '']) !!}
                        <div data-min-view="2" id="last_working_date_div" data-date-format="yyyy-mm-dd" class="input-group date ">
                            <input size="16" id="last_working_date" name="last_working_date" type="text" value="" class="form-control input-sm input-height-39">
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                        </div>

                    </div>
					<div class="form-group col-sm-12">
                      <label class="control-label">Resignation / Application</label>
					   <input type="file" name="document" placeholder="Document" class="form-control">
					</div>

            <div class="form-group">


                <table id="abs-table" class="table table-bordered table-hover table-fw-widget" width="70%;">
                    <tbody>
                        <tr>
                            <th width="20%">Employee Code</th>
                            <td width="30%" class="emp_code"></td>
                            <th width="20%">Store IO Code</th>
                            <td width="30%" class="io_code"></td>
                        </tr>
                        <tr>
                            <th>Emp Name</th>
                            <td class="emp_name"></td>
                            <th>Store Name</th>
                            <td class="store_name"></td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td class="designation"></td>
                            <th>Mobile</th>
                            <td class="mobile"></td>
                        </tr>
                        <tr>
                            <th>Date Of joining</th>
                            <td class="doj"></td>
                            <th>Email</th>
                            <td class="email"></td>
                        </tr>
                        <tr>
                            <th>Name as per Bank</th>
                            <td class="name"></td>
                            <th>Bank A/c No</th>
                            <td class="ac_no"></td>
                        </tr>
                        <tr>
                            <th>Bank Name</th>
                            <td class="bank_name"></td>
                            <th>IFSC Code</th>
                            <td class="ifsc_code"></td>
                        </tr>
                        <tr>
                            <th>House Address for communication</th>
                            <td colspan="3" class="address"></td>
                        </tr>
                    </tbody>
                </table>

            </div>



			<div class="form-group">

			</div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
            <button type="submit" data-dismiss="modal" class="btn btn-success submit-resignation">Proceed</button>
        </div>
		</form>
    </div>
</div>


<div id="resignation_edit" class="modal-container colored-header colored-header-success custom-width modal-effect-9 custom-modal1 custom-modal2">
    <div class="modal-content">
        <form id="edit_resume_store" action="{{url('store/resignation_update')}}" enctype="multipart/form-data" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="rid" id="edit_resume_id" />
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
                <h3 class="modal-title send_forword_title">Resignation Request Update</h3>
            </div>
            <div class="modal-body form">
                <div class="error"></div>
                <div class="form-group col-sm-6">
                    {{-- <h3 id="regignation_emp_name"></h3> --}}
                    {!! Form::label('employee_id', '* Employee: ', ['class' => '']) !!} {!! Form::select('employee_id', [''=>'--select employee--']+$employee,null,
                    ['class' => 'col-sm-12 form-control input-sm employee_id','disabled'=>'disabled']) !!}

                    <p style="color: red;" class="employee_id_error error hide"></p>
                </div>

                <div class="form-group col-sm-12">
                    {!! Form::label('reason_id', '* Reason: ', ['class' => '']) !!}
                    {!! Form::select('reason_id', [''=>'--select Reasion--']+$reason_parent,null, ['class' => 'col-sm-12 form-control input-sm reason_id','id'=>'reason_id_edit']) !!}
                    <p style="color: red;" class="employee_id_error error hide"></p>
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('sub_reason_id', '* Sub Reason: ', ['class' => '']) !!} {!! Form::select('sub_reason_id', [''=>'--select
                    Sub Reasion--'],null, ['class' => 'col-sm-12 form-control input-sm sub_reason_id reason_id','id'=>'sub_reason_id_edit'])
                    !!}
                    <p style="color: red;" class="employee_id_error error hide"></p>

                </div>

                <div class="form-group col-sm-6">
                    {!! Form::label('', '* Date Of Resignation: ', ['class' => '']) !!}
                    <div data-min-view="2" id="date_of_resignation_edit" data-date-format="yyyy-mm-dd" class="input-group date ">
                        <input size="16" id="lw_date" name="date_of_resignation" type="text" value="" class="form-control input-sm input-height-39 date_of_resignation">
                        <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('', '* Last Working Date: ', ['class' => '']) !!}
                    <div data-min-view="2" id="last_working_date_div_edit" data-date-format="yyyy-mm-dd" class="input-group date ">
                        <input size="16" id="last_working_date" name="last_working_date" type="text" value="" class="form-control input-sm input-height-39 last_working_date">
                        <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                    </div>

                </div>
                <div class="form-group col-sm-12">
                    <label class="control-label">Resignation / Application</label>

                    <input type="file" name="document" placeholder="Document" class="form-control">
                    <label id="path_of_application_regignation"><label>
                </div>

                <div class="form-group">


                    <table id="abs-table" class="table table-bordered table-hover table-fw-widget" width="70%;">
                        <tbody>
                            <tr>
                                <th width="20%">Employee Code</th>
                                <td width="30%" class="emp_code"></td>
                                <th width="20%">Store IO Code</th>
                                <td width="30%" class="io_code"></td>
                            </tr>
                            <tr>
                                <th>Emp Name</th>
                                <td class="emp_name"></td>
                                <th>Store Name</th>
                                <td class="store_name"></td>
                            </tr>
                            <tr>
                                <th>Designation</th>
                                <td class="designation"></td>
                                <th>Mobile</th>
                                <td class="mobile"></td>
                            </tr>
                            <tr>
                                <th>Date Of joining</th>
                                <td class="doj"></td>
                                <th>Email</th>
                                <td class="email"></td>
                            </tr>
                            <tr>
                                <th>Name as per Bank</th>
                                <td class="name"></td>
                                <th>Bank A/c No</th>
                                <td class="ac_no"></td>
                            </tr>
                            <tr>
                                <th>Bank Name</th>
                                <td class="bank_name"></td>
                                <th>IFSC Code</th>
                                <td class="ifsc_code"></td>
                            </tr>
                            <tr>
                                <th>House Address for communication</th>
                                <td colspan="3" class="address"></td>
                            </tr>
                        </tbody>
                    </table>

                </div>



                <div class="form-group">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
                <button type="submit" data-dismiss="modal" class="btn btn-success submit-resignation">Proceed</button>
            </div>
        </form>
    </div>
</div>