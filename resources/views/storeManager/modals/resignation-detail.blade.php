<div id="resignation-detail" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_forword_title">Resignation Request</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>

            <div class="form-group">


                <table id="abs-table" class="table table-bordered table-hover table-fw-widget" width="70%;">
                    <tbody>

						<tr>
                            <th>Note From Store Manager</th>
                            <td colspan="3" class="note_from_store_manager"></td>
                        </tr>
						<tr>
                            <th>Note From Regional HR</th>
                            <td colspan="3" class="note_from_regional_manager"></td>
                        </tr>
						<tr>
                            <th>Employee Application copy</th>
                            <td colspan="3" class="application_url"></td>
                        </tr>

						<tr>
                            <th>Application Status</th>
                            <td class="application_status"></td>
                            <th>Last Working Date</th>
                            <td class="last_working_date_show"></td>
                        </tr>

                        <tr>
                            <th width="20%">Employee Code</th>
                            <td width="30%" class="emp_code"></td>
                            <th width="20%">Store IO Code</th>
                            <td width="30%" class="io_code"></td>
                        </tr>
                        <tr>
                            <th>Emp Name</th>
                            <td class="emp_name"></td>
                            <th>Store Name</th>
                            <td class="store_name"></td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td class="designation"></td>
                            <th>Mobile</th>
                            <td class="mobile"></td>
                        </tr>
                        <tr>
                            <th>Date Of joining</th>
                            <td class="doj"></td>
                            <th>Email</th>
                            <td class="email"></td>
                        </tr>
                        <tr>
                            <th>Name as per Bank</th>
                            <td class="name"></td>
                            <th>Bank A/c No</th>
                            <td class="ac_no"></td>
                        </tr>
                        <tr>
                            <th>Bank Name</th>
                            <td class="bank_name"></td>
                            <th>IFSC Code</th>
                            <td class="ifsc_code"></td>
                        </tr>
                        <tr>
                            <th>House Address for communication</th>
                            <td colspan="3" class="address"></td>
                        </tr>
                    </tbody>
                </table>

            </div>



			<div class="form-group">

			</div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Close</button>
        </div>
    </div>
</div>


