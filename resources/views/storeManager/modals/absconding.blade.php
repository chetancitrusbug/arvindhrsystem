<div id="absconding" class="modal-container colored-header colored-header-success custom-width modal-effect-9 custom-modal1">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_forword_title">Absconding Separation Add</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>
            <div class="form-group">
                <div class="form-group col-sm-12{{ $errors->has('employee_id') ? ' has-error' : ''}}">
                    {!! Form::label('employee_id', '* Employee: ', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::select('employee_id', [''=>'--select employee--']+$employee,null, ['class' => 'form-control input-sm employee_id']) !!}
                        <p style="color: red;" class="employee_id_error error hide"></p>
                    </div>
                </div>
                <div id="date_of_absconding_div" style="display:none" class="form-group col-sm-12{{ $errors->has('absconding_time') ? ' has-error' : ''}}">
                    {!! Form::label('absconding_time', '* Absconding Date: ', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="col-sm-8">
                        <div data-min-view="2" id="date_of_absconding" data-date-format="yyyy-mm-dd" class="input-group date ">
                        <input size="16" id="absconding_time" name="absconding_time" type="text" value="" class="form-control input-sm input-height-39">
                        <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                        <p style="color: red;" class="absconding_time error hide"></p>
                        </div>
                    </div>
                </div>

                <div class="table-responsive002">
                <table id="abs-table" class="table table-bordered table-hover table-fw-widget width-table-full" width="70%;">
                    <tbody>
                        <tr>
                            <th width="20%" class="width-20">Employee Code</th>
                            <td width="30%" class="emp_code width-30"></td>
                            <th width="20%" class="width-20">Store IO Code</th>
                            <td width="30%" class="io_code width-30"></td>
                        </tr>
                        <tr>
                            <th>Emp Name</th>
                            <td class="emp_name"></td>
                            <th>Store Name</th>
                            <td class="store_name"></td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td class="designation"></td>
                            <th>Mobile</th>
                            <td class="mobile"></td>
                        </tr>
                        <tr>
                            <th>Date Of joining</th>
                            <td class="doj"></td>
                            <th>Email</th>
                            <td class="email"></td>
                        </tr>
                        <tr>
                            <th>Name as per Bank</th>
                            <td class="name"></td>
                            <th>Bank A/c No</th>
                            <td class="ac_no"></td>
                        </tr>
                        <tr>
                            <th>Bank Name</th>
                            <td class="bank_name"></td>
                            <th>IFSC Code</th>
                            <td class="ifsc_code"></td>
                        </tr>
                        <tr>
                            <th>House Address for communication</th>
                            <td colspan="3" class="address"></td>
                        </tr>
                    </tbody>
                </table></div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
            <button type="submit" data-dismiss="modal" class="btn btn-success submit-absconding">Proceed</button>
        </div>
    </div>
</div>
