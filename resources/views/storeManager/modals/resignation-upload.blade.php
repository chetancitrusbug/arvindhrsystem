<div id="resignation-upload" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_forword_title">Resignation Request Process</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>

					<form action="{{url('store/resignation')}}/update" enctype="multipart/form-data" method="POST" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}
					<input type="hidden" name="rid" value="" id="hidden_res_id">


					<div class="form-group">
                      <label class="col-sm-3 control-label">Complete or Decline Request</label>
                      <div class="col-sm-6">
                        <div class="be-radio inline">
                          <input type="radio" checked name="resignation_status" value="documentation-reg" id="rad6">
                          <label for="rad6">Complete</label>
                        </div>
                        <div class="be-radio inline">
                          <input type="radio"  name="resignation_status" value="decline" id="rad7">
                          <label for="rad7">Decline</label>
                        </div>

                      </div>
                    </div>

					<div class="form-group ">
                      <label class="col-sm-3 control-label">Document</label>
					   <div class="col-sm-6">
                       <input type="file" name="document" placeholder="Document" class="form-control">
					  </div>
                    </div>
					<div class="form-group">
                      <label class="col-sm-3 control-label">Note</label>
                      <div class="col-sm-6">
                        <textarea class="form-control" name="store_note" style="min-width: 100%"></textarea>
                      </div>
                    </div>
					<div class="text-center submit_btn_block">
						<div class="xs-mt-50">
								<button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
								<button type="submit" data-dismiss="modal" class="btn btn-success submit-resignation">Submit</button>
						</div>
					</div>
					<div class="text-center submit_notification_block">
						<div class="xs-mt-50">
								<p>Please complete the documentation before upload signature form </p>
						</div>
					</div>

					</form>
					<hr/>

			<div class="form-group">

			</div>
        </div>

        <div class="modal-footer text-center">

        </div>
    </div>
</div>


