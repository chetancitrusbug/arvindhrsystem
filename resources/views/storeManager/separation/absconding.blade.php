@extends('storeManager.layouts.backend')
@section('title',"Absconding")
@section('css')
    <style>
        .paging { padding-right: 20px; padding: 0px 10px !important; }
        #attendance-table { width:3000px;}
        .select{
            padding: 6px 2px;
            height: 30px;
            font-size: 12px;
            width: 55px;
        }
        .table-responsive tbody > tr > td {
            width: 100px;
        }
        .table-responsive tbody > tr > td:first-child{
            width: 200px;
        }
        .request-change{
            cursor: pointer;
        }
        #abs-table{
            width: 95%; margin: 0 auto;
            font-size: 10px;
        }
        .error{
            color: red;
        }
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.absconding')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table table-response1">
                    <div class="panel-heading">
                        <p class="panel-head1">Absconding List
                            <span class="panel-subtitle panel-sheading1">
                                <a href="#" title="Add">
                                    <button class="btn btn-space btn-success add_absconding btn-addfixed">Add</button>
                                </a>
                            </span>
                        </p>

                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table id="absconding-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Employee Name</th>
                                                <th>Employee Code</th>
                                                <th>Date of absconding</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script>
        var url ="{{ url('/store/absconding-data') }}";
        var remove_url = "{{ url('/store/remove-absconding') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#absconding-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                // statusChange();
            },
            columns: [
                { 	data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { 	data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
				{
					data: null,
					"searchable": false,
					"orderable": false,
					"render":function (o){
						var date =  new Date(o.absconding_time)
						return   date.getDate() + '-'+(date.getMonth() + 1) + '-' + date.getFullYear();
					},

				},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d=r=l=u= "";
						if(o.rm_absconding_status == 0)
						{
							v = "<a data-url='"+remove_url+"/"+o.id+"' data-id="+o.id+" class='remove-absconding btn btn-warning btn-sm' title='Remove from absconding'>x</a>&nbsp;";
						}else{
							v= '-'
						}


                        return v+e+u;
                    }

                }
            ]
        });

        $('.add_absconding').on('click',function(e){
            $('select[name="employee_id"]').select2();
            $('#absconding').niftyModal();
        });
        $('select[name="employee_id"]').on('change',function(e){
            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: "{{url('store/getEmployeeDetail')}}",
                data: {id:$(this).val()},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data) {
                if(data.status == '200'){
                    data = data.data;
                    $('.emp_code').html(data.employee_code);
                    $('.io_code').html(data.store_location.io_code);
                    $('.emp_name').html(data.full_name);
                    $('.store_name').html(data.store_location.store_name);
                    $('.designation').html(data.designation.name);
                    $('.mobile').html(data.mobile);
                    $('.doj').html(data.joining_date);
                    $('.email').html(data.email);
                    $('.name').html(data.full_name);
                    $('.ac_no').html(data.account_no);
                    $('.bank_name').html(data.bank_name);
                    $('.ifsc_code').html(data.ifsc);
                    $('.address').html(data.permanent_address_line_1+', '+data.permanent_address_line_2+', '+data.permanent_address_line_3);
                    $("#date_of_absconding_div").show();
                }else if(data.status == '400'){
                    $('.error').html(data.msg);
                }
                /*$('button[type="submit"]').removeAttr('disabled');
                $('.success-text').html(data);
                $('#reject').niftyModal('hide');
                $('#success').niftyModal();

                setTimeout(function () {
                    $('#success').niftyModal('hide');
                    location.reload();
                }, 2000);*/
            })
            .fail(function(data) {
                $('button[type="submit"]').removeAttr('disabled');
                /*jQuery.each(data.responseJSON.errors,function(index,val){
                    $('.'+index+'_error').html(val);
                });*/
            });
        });

        $('.submit-absconding').on('click',function(){
            url = "{{url('/store/add-to-absconding-separation')}}";

            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: url,
                data: {employee_id:$('select[name="employee_id"]').val(),absconding_time:$("#absconding_time").val()},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data) {
                if(data.status == '200'){
                    location.reload();
                }else if(data.status == '400'){
                    $('.error').html(data.msg);
                    $("#date_of_absconding_div").show();
                }
            })
            .fail(function(data) {
                $('button[type="submit"]').removeAttr('disabled');
                /*jQuery.each(data.responseJSON.errors,function(index,val){
                    $('.'+index+'_error').html(val);
                });*/
            });
        });

        $(document).on('click','.remove-absconding',function(){
            url = $(this).data('url');

            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: url,
                data: {employee_id:$('select[name="employee_id"]').val()},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data) {
                if(data.status == '200'){
                    location.reload();
                }else if(data.status == '400'){
                    $('.error').html(data.msg);
                }
            })
            .fail(function(data) {
                $('button[type="submit"]').removeAttr('disabled');
                /*jQuery.each(data.responseJSON.errors,function(index,val){
                    $('.'+index+'_error').html(val);
                });*/
            });
        });
        $("#date_of_absconding").datetimepicker({
            format: 'dd-mm-yyyy',
			endDate: '+0d',
			autoclose: true,
            componentIcon: '.mdi.mdi-calendar'
        });
    </script>
@endsection
