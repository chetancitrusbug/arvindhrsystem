<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Dashboard</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements" role="tablist">
                        <li><a href="{{ url('store') }}"><span>Dashboard</span></a></li>
                        <li class="{{(isset($route) && $route == 'hiring')?'active':''}}"><a href="{{ url('store/hiring') }}"><span>Hiring</span></a></li>
                        <li class="{{(isset($route) && $route == 'employee')?'active':''}}"><a href="{{ url('store/employee') }}"><span>Employee</span></a></li>
                        <li class="{{(isset($route) && $route == 'attendance')?'active':''}}"><a href="{{ url('store/attendance') }}"><span>Attendance</span></a></li>
                       {{-- <li class="{{(isset($route) && $route == 'separation')?'active':''}}"><a href="{{ url('store/absconding') }}"><span>Absconding</span></a></li>
						<li class="{{ Request::is('store/resignation') ? 'active' : '' }}"><a href="{{ url('store/resignation') }}"><span>Resignation</span></a></li>
                         <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>UI Elements</span></a>
                            <ul class="sub-menu">
                                <li><a href="ui-alerts.html">Alerts</a>
                                </li>
                            </ul>
                        </li> --}}
						<li class="parent {{(isset($route) && ($route == 'separation' || $route == 'resignation' ))?'active open':''}}"><a href="#"><span>Separation</span></a>
							<ul class="sub-menu">
								<li class="{{(isset($route) && $route == 'separation')?'active':''}}"><a href="{{ url('store/absconding') }}"><span>Absconding</span></a></li>
								<li class="{{ Request::is('store/resignation') ? 'active' : '' }}"><a href="{{ url('store/resignation') }}"><span>Resignation</span></a></li>
							</ul>
						</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
