
<div class="form-group col-sm-12{{ $errors->has('is_disable') ? ' has-error' : ''}}">
    {!! Form::label('is_disable', 'Is PANKH: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('is_disable', [''=>'-- Select is PANKH --']+array('YES'=>'YES','NO'=>'NO'), (isset($otherInfo['is_disabled']))?$otherInfo['is_disabled']:null ,['class' => 'form-control select2
        input-sm']) !!} {!! $errors->first('bv_report', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div id="disability_desc" class="form-group{{ $errors->has('disability_description') ? ' has-error' : ''}} col-sm-12">
    {!! Form::label('disability_description', '* PANKH Description : ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('disability_description', (isset($otherInfo))?$otherInfo['disability_desc']:null, ['class' => 'form-control input-sm disable']) !!} {!! $errors->first('disability_description',
        '
        <p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group{{ $errors->has('salary') ? ' has-error' : ''}} col-sm-12">
    {!! Form::label('salary', '* Salary(CTC): ', ['class' => 'col-sm-3 control-label']) !!}
    <small class="text-danger">* Amount in actual figures</small>
    <div class="col-sm-6">
        {!! Form::number('salary', (isset($otherInfo['salary']))?$otherInfo['salary']:null, ['class' => 'form-control input-sm disable']) !!} {!! $errors->first('salary', ' <p class="help-block">:message</p>') !!}
    </div>
</div>

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"
/>
@endsection
@section('scripts')
<script>
    datePicker();
        function datePicker(){
            $(".datetimepicker1").datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                componentIcon: '.mdi.mdi-calendar',
            /*    pickerPosition: "bottom-left", */
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });
        }
        if($("#is_disable").val() == 'YES'){
            $("#disability_desc").show();
        } else {
            $("#disability_desc").hide();
        }

        //model dependency on department
        $('select[name="is_disable"]').on('change', function(event) {
            if($(this).val() == 'YES' ){
                $("#disability_desc").show();
            } else {
                $("#disability_desc").hide();
            }
        });
</script>
@endsection