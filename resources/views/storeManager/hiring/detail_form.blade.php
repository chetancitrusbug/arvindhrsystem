<div class="form-group col-sm-12{{ $errors->has('designation_id') ? ' has-error' : ''}}">
    {!! Form::label('designation_id', 'Designation: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('designation_id', [''=>'-- Select Designation --']+$designation, null ,['class' => 'form-control select2
        input-sm','id'=>'designation']) !!} {!! $errors->first('designation_id', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('full_name', 'Personal Information ', ['class' => 'col-sm-12 lable label-default form_label']) !!}
</div>
<div class="form-group col-sm-12{{ $errors->has('full_name') ? ' has-error' : ''}}">
    {!! Form::label('full_name', '* Full Name (as per Aadhar): ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('full_name', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('full_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('position') ? ' has-error' : ''}}">
    {!! Form::label('position', '* Position applied for: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('position_name', null, ['disabled' => true,'class' => 'form-control input-sm disable position']) !!}
        {!! $errors->first('position', '<p class="help-block">:message</p>') !!}
    </div>
	<input type="hidden" class="position" name="position" value="" />
</div>
<div class="form-group col-sm-12{{ ($errors->has('blood_group') || $errors->has('rh_factor')) ? ' has-error' : ''}}">
    {!! Form::label('blood_group', '* Blood Group: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-2">
        {!! Form::select('blood_group', [''=>'-- Select Blood Group --']+config('constants.blood_group'), null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('blood_group', '<p class="help-block">:message</p>') !!}
    </div>
    {!! Form::label('rh_factor', '* RH Factor: ', ['class' => 'col-sm-1 control-label']) !!}
    <div class="col-sm-3">
        {!! Form::select('rh_factor', [''=>'-- Select RH Factor --']+config('constants.rh_factor'), null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('rh_factor', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('date_of_birth') ? ' has-error' : ''}}">
    {!! Form::label('date_of_birth', '* Date of Birth (as per Aadhar): ', ['class' => 'col-sm-3 control-label text-right','data-date-format'=>"dd-mm-yyyy"]) !!}
    <div class="col-sm-6 col-xs-12">
        <div data-min-view="2"  class="input-group {{ !(isset($is_employee) && $is_employee ==1)?'date datetimepicker1':'' }}">
			<input size="16" type="text" value="{{old('date_of_birth',isset($employee)?$employee->date_of_birth:null)}}" class="form-control input-sm disable" data-date-format="dd-mm-yyyy H:i" name="date_of_birth" id="date_of_birth"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span> 
        </div>
        {!! $errors->first('date_of_birth', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('marital_status') ? ' has-error' : ''}}">
    {!! Form::label('marital_status', '* Marital Status: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('marital_status', [''=>'-- Select Marital Status --']+config('constants.marital_status'), null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('marital_status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ ($errors->has('aadhar_status') || $errors->has('aadhar_number')) ? ' has-error' : ''}}">
    {!! Form::label('aadhar_status', '* Availability of Aadhar: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-2">
        {!! Form::select('aadhar_status', [''=>'-- Availability of Aadhar --']+config('constants.aadhar_status'), null ,['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('aadhar_status', '<p class="help-block">:message</p>') !!}
    </div>
    {!! Form::label('aadhar_number', 'Aadhar Number: ', ['class' => 'col-sm-2 control-label aadhar_number']) !!}
    <div class="col-sm-2">
        {!! Form::text('aadhar_number', null, ['class' => 'form-control input-sm aadhar_number disable']) !!}
        {!! $errors->first('aadhar_number', '<p class="help-block aadhar_number">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12 aadhar_enrolment_number {{ $errors->has('aadhar_enrolment_number') ? ' has-error' : ''}}">
    {!! Form::label('aadhar_enrolment_number', 'Aadhar Enrolment Number: ', ['class' => 'col-sm-3 control-label text-right aadhar_enrolment_number']) !!}
    <div class="col-sm-6">
        {!! Form::text('aadhar_enrolment_number', null, ['class' => 'form-control input-sm aadhar_enrolment_number disable']) !!}
        {!! $errors->first('aadhar_enrolment_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('nationality') ? ' has-error' : ''}}">
    {!! Form::label('nationality', '*Nationality: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('nationality', 'Indian', ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('nationality', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('gender') ? ' has-error' : ''}}">
    {!! Form::label('gender', '* Gender (as per Aadhar): ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('gender', [''=>'-- Select Gender --']+config('constants.gender'), null ,['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ ($errors->has('pan_status') || $errors->has('pan_number')) ? ' has-error' : ''}}">
    {!! Form::label('pan_status', '* Availability of PAN: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-2">
        {!! Form::select('pan_status', [''=>'-- Availability of PAN --']+config('constants.pan_status'), null ,['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('pan_status', '<p class="help-block">:message</p>') !!}
    </div>
    {!! Form::label('pan_number', 'PAN:', ['class' => 'col-sm-2 control-label pan_number']) !!}
    <div class="col-sm-2">
        {!! Form::text('pan_number', null, ['class' => 'form-control input-sm pan_number disable']) !!}
        {!! $errors->first('pan_number', '<p class="help-block pan_number">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('mobile') ? ' has-error' : ''}}">
    {!! Form::label('mobile', '* Mobile Number: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('mobile', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', 'Personal Email Id: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('email', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('esic_number') ? ' has-error' : ''}}">
    {!! Form::label('esic_number', 'ESIC Number: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('esic_number', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('esic_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('emergency_person') ? ' has-error' : ''}}">
    {!! Form::label('emergency_person', '* Emergency Contact person name: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('emergency_person', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('emergency_person', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('emergency_number') ? ' has-error' : ''}}">
    {!! Form::label('emergency_number', '* Emergency Contact number: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('emergency_number', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('emergency_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('emergency_relationship') ? ' has-error' : ''}}">
    {!! Form::label('emergency_relationship', '* Emergency Contact Relationship: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('emergency_relationship', [''=>'-- Select Relation --']+config('constants.relationship'), null ,['class' => 'form-control input-sm']) !!}
        {{-- {!! Form::text('emergency_relationship', null, ['class' => 'form-control input-sm']) !!} --}}
        {!! $errors->first('emergency_relationship', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('anniversary_date') ? ' has-error' : ''}}">
    {!! Form::label('anniversary_date', 'Wedding anniversary date: ', ['class' => 'col-sm-3 control-label text-right','data-date-format'=>"dd-mm-yyyy"]) !!}
    <div class="col-sm-6 col-xs-12">
        <div data-min-view="2"  class="input-group date datetimepicker1">
            <input size="16" type="text" value="{{old('anniversary_date',isset($employee)?$employee->anniversary_date:null)}}" class="form-control input-sm" data-date-format="dd-mm-yyyy H:i" name="anniversary_date"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
        </div>
        {!! $errors->first('anniversary_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('first_name') ? ' has-error' : ''}}">
    {!! Form::label('first_name', ' First Name (for email id purpose): ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('first_name', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', ' Last Name (for email id purpose): ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('last_name', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('ua_number') ? ' has-error' : ''}}">
    {!! Form::label('ua_number', 'Universal Account Number (UAN): ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('ua_number', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('ua_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('Current Address', 'Current Address', ['class' => 'col-sm-12 lable label-default form_label']) !!}
</div>
<div class="form-group col-sm-12{{ $errors->has('current_address_line_1') ? ' has-error' : ''}}">
    {!! Form::label('current_address_line_1', '* Address Line 1: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('current_address_line_1', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('current_address_line_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('current_address_line_2') ? ' has-error' : ''}}">
    {!! Form::label('current_address_line_2', 'Address Line 2: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('current_address_line_2', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('current_address_line_2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('current_address_line_3') ? ' has-error' : ''}}">
    {!! Form::label('current_address_line_3', 'Address Line 3: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('current_address_line_3', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('current_address_line_3', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('current_state') ? ' has-error' : ''}}">
    {!! Form::label('current_state', '* State: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('current_state', [''=>'-- Select State --']+$states, null ,['class' => 'form-control select2 input-sm']) !!}
        {!! $errors->first('current_state', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('current_city') ? ' has-error' : ''}}">
    {!! Form::label('current_city', '* City: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('current_city', [''=>'-- Select city --']+$currentCity, null ,['class' => 'form-control select2 input-sm disable']) !!}
        {!! $errors->first('current_city', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('current_pincode') ? ' has-error' : ''}}">
    {!! Form::label('current_pincode', '* Pincode: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('current_pincode', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('current_pincode', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('Permanent Address', 'Permanent Address', ['class' => 'col-sm-12 lable label-default form_label']) !!}
</div>
<div class="form-group col-sm-12">
    {!! Form::label('', '', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::checkbox('same_as_current', 0, false, ['class' => 'control-label pull-left same_as_current']) !!}
        &nbsp;&nbsp;Same As Current Address
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('permanent_address_line_1') ? ' has-error' : ''}}">
    {!! Form::label('permanent_address_line_1', '* Address Line 1: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('permanent_address_line_1', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('permanent_address_line_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('permanent_address_line_2') ? ' has-error' : ''}}">
    {!! Form::label('permanent_address_line_2', 'Address Line 2: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('permanent_address_line_2', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('permanent_address_line_2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('permanent_address_line_3') ? ' has-error' : ''}}">
    {!! Form::label('permanent_address_line_3', 'Address Line 3: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('permanent_address_line_3', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('permanent_address_line_3', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('permanent_state') ? ' has-error' : ''}}">
    {!! Form::label('permanent_state', '* State: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('permanent_state', [''=>'-- Select State --']+$states, null ,['class' => 'form-control select2 input-sm disable']) !!}
        {!! $errors->first('permanent_state', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('permanent_city') ? ' has-error' : ''}}">
    {!! Form::label('permanent_city', '* City: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('permanent_city', [''=>'-- Select city --']+$permanentCity, null ,['class' => 'form-control select2 input-sm disable']) !!}
        {!! $errors->first('permanent_city', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group col-sm-12{{ $errors->has('permanent_pincode') ? ' has-error' : ''}}">
    {!! Form::label('permanent_pincode', '* Pincode: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::text('permanent_pincode', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('permanent_pincode', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@section('scripts')
    <script>
        datePicker();
        function datePicker(){
            $(".datetimepicker1").datetimepicker({
                /*autoclose: true,
                format: 'dd-mm-yyyy',
                componentIcon: '.mdi.mdi-calendar',
				
            /*    pickerPosition: "bottom-left", */
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                },
				autoclose: true,
				format: 'dd/mm/yyyy',
				componentIcon: '.mdi.mdi-calendar',
				navIcons:{
					rightIcon: 'mdi mdi-chevron-right',
					leftIcon: 'mdi mdi-chevron-left'
				}
            });
        }
		
		$(function () {
			$("#designation").change();
		});
		
		$('#designation').on('change', function(event) {
			$(".position").val($("#designation option:selected").text());
		})
		
        //model dependency on department
        $('select[name="department_id"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/store/sub-department-list') }}",
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="sub_department_id"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="sub_department_id"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });

        $('select[name="current_state"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/store/city-list') }}",
                data: {state_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="current_city"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="current_city"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });

        $('select[name="permanent_state"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/store/city-list') }}",
                data: {state_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="permanent_city"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="permanent_city"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });

        panDisplay();
        aadharDisplay();
        $('select[name="pan_status"]').on('change',function(){
            panDisplay();
        });

        $('select[name="aadhar_status"]').on('change',function(){
            aadharDisplay();
        });

        function panDisplay(){
            $('.pan_number').hide();
            if($('select[name="pan_status"]').val() == 'available'){
                $('.pan_number').show();
            }
        }

        function aadharDisplay(){
            $('.aadhar_number').hide();
            $('.aadhar_enrolment_number').hide();
            if($('select[name="aadhar_status"]').val() == 'available'){
                $('.aadhar_number').show();
                $('.aadhar_enrolment_number').hide();
            }

            if($('select[name="aadhar_status"]').val() == 'not_available'){
                $('.aadhar_number').hide();
                $('.aadhar_enrolment_number').show();
            }
        }

        jQuery(document).ready(function($) {
            @if(!isset($employee))
                $('#loading').show();
                selectValue('business_unit_id','Unlimited');
                selectValue('legal_entity_id','Arvind Lifestyle Brands Limited');
                selectValue('employee_classification_id','Store');
                selectValue('variable_pay_type_id','Incentive');
                $('#loading').hide();
            @endif

            @if(isset($is_employee) && $is_employee ==1)
                $('.disable').attr('disabled','disabled');
            @endif
        });

        function selectValue(selector,val){
            $("#"+selector+" option:contains("+val+")").attr('selected', 'selected');
            $('#'+selector).trigger('change');
        }

        $('.same_as_current').on('change',function(e){
            if($(this).prop('checked')){
                $('input[name="permanent_address_line_1"]').val($('input[name="current_address_line_1"]').val());
                $('input[name="permanent_address_line_2"]').val($('input[name="current_address_line_2"]').val());
                $('input[name="permanent_address_line_3"]').val($('input[name="current_address_line_3"]').val());
                $('select[name="permanent_state"]').val($('select[name="current_state"]').val()).trigger('change');
                setTimeout(function(){
                    $('select[name="permanent_city"]').val($('select[name="current_city"]').val()).trigger('change');
                },500);
                $('input[name="permanent_pincode"]').val($('input[name="current_pincode"]').val());
            }else{
                $('input[name="permanent_address_line_1"]').val('');
                $('input[name="permanent_address_line_2"]').val('');
                $('input[name="permanent_address_line_3"]').val('');
                $('select[name="permanent_state"]').val('').trigger('change');
                $('select[name="permanent_city"]').val('').trigger('change');
                $('input[name="permanent_pincode"]').val('');
            }
        });

		$("#date_of_birth").on("change",function(){
			var today = new Date();
			var nowyear = today.getFullYear();
			var nowmonth = today.getMonth();
			var nowday = today.getDate();
			var b = $(this).val();



			var birth = new Date(b);

			var birthyear = birth.getFullYear();
			var birthmonth = birth.getMonth();
			var birthday = birth.getDate();

			var age = nowyear - birthyear;
			var age_month = nowmonth - birthmonth;
			var age_day = nowday - birthday;

			if (age_month < 0 || (age_month == 0 && age_day < 0)) {
				age = parseInt(age) - 1;
			}
			console.log(age);
			if ((age == 18 && age_month <= 0 && age_day <= 0) || age < 18) {
				alert("Employee Age is less then 18 years");
				return false;
			}
		})
    </script>
@endsection