@extends('storeManager.layouts.backend')
@section('title',"View Employee")
@section('css')
    <style type="text/css">
        i.mdi.mdi-case-download{
            font-size: 30px;
        }
        .case{
            /*text-align: right;*/
        }
        td,th {
            vertical-align: top !important;
            padding: 10px !important;
            height: 40px;
        }
        .form_label{ border: none; padding: 10px; text-align: center; font-size: 16px; background: #eee; }
    </style>
@endsection
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">

        <div class="row">
            <div class="user-info-list panel panel-default panel-custom-a1 panel-custom-view view-table2">
                <div class="panel-heading panel-heading-divider">Employee # <strong>{{ $employee->full_name }}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/store/hiring') }}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="tab-container">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#detail" data-toggle="tab"><strong>Personal Information</strong></a></li>
                                    <li><a href="#education" data-toggle="tab"><strong>Education Detail</strong></a></li>
                                    <li><a href="#experience" data-toggle="tab"><strong>Previous Work Experience</strong></a></li>
                                    <li><a href="#family" data-toggle="tab"><strong>Family Information</strong></a></li>
                                    <li><a href="#attachment" data-toggle="tab"><strong>Attachments</strong></a></li>
                                    <li><a href="#otherinfo" data-toggle="tab"><strong>Other Info</strong></a></li>
                                </ul>
                                <div class="tab-content clearfix">
                                    <div id="detail" class="tab-pane active cont">
                                        <div class="table-responsive t_responsive2">
                                            <table class="no-border no-strip skills">
                                                <tbody class="no-border-x no-border-y">
                                                    <tr>
                                                        <th class="item">Full Name</th>
                                                        <td>{{ $employee->full_name }}</td>
                                                        <th class="item">Position</th>
                                                        <td>{{ $employee->position }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Blood Group</th>
                                                        <td>{{ $employee->blood_group }}</td>
                                                        <th class="item">Rh Factor</th>
                                                        <td>{{ $employee->rh_factor }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Date Of Birth</th>
                                                        <td>{{ $employee->date_of_birth }}</td>
                                                        <th class="item">Marital Status</th>
                                                        <td>{{ ucfirst($employee->marital_status) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Availability Of Aadhar</th>
                                                        <td>{{ config('constants.aadhar_status')[$employee->aadhar_status] }}</td>
                                                        <th class="item">Aadhar Number</th>
                                                        <td>{{ $employee->aadhar_number }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Aadhar Enrolment Number</th>
                                                        <td>{{ $employee->aadhar_enrolment_number }}</td>
                                                        <th class="item">Nationality</th>
                                                        <td>{{ $employee->nationality }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Gender</th>
                                                        <td>{{ config('constants.gender')[$employee->gender] }}</td>
                                                        <th class="item">Availability of PAN</th>
                                                        <td>{{ config('constants.pan_status')[$employee->pan_status] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">PAN</th>
                                                        <td>{{ $employee->pan_number }}</td>
                                                        <th class="item">Mobile</th>
                                                        <td>{{ $employee->mobile }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Email</th>
                                                        <td>{{ $employee->email }}</td>
                                                        <th class="item">ESIC Number</th>
                                                        <td>{{ $employee->esic_number }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Emergency Person</th>
                                                        <td>{{ $employee->emergency_person }}</td>
                                                        <th class="item">Emergency Number</th>
                                                        <td>{{ $employee->emergency_number }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Emergency Relationship</th>
                                                        <td>{{ $employee->emergency_relationship }}</td>
                                                        <th class="item">Anniversary Date</th>
                                                        <td>{{ $employee->anniversary_date }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">First Name</th>
                                                        <td>{{ $employee->first_name }}</td>
                                                        <th class="item">Last Name</th>
                                                        <td>{{ $employee->last_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Universal Account Number</th>
                                                        <td>{{ $employee->ua_number }}</td>
                                                        <th class="item">POD No</th>
                                                        <td>{{ $employee->pod_no }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('Current Address', 'Current Address', ['class' => 'col-sm-12 lable label-default form_label']) !!}
                                        </div>
                                        <div class="t_responsive2">
                                        <table class="no-border no-strip skills">
                                            <tbody class="no-border-x no-border-y">
                                                <tr>
                                                    <th class="item">Address Line 1</th>
                                                    <td>{{ $employee->current_address_line_1 }}</td>
                                                    <th class="item">Address Line 2</th>
                                                    <td>{{ $employee->current_address_line_2 }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Address Line 3</th>
                                                    <td>{{ $employee->current_address_line_3 }}</td>
                                                    <th class="item">State</th>
                                                    <td>{{ $employee->currentState->name or '' }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">City</th>
                                                    <td>{{ $employee->currentCity->name or '' }}</td>
                                                    <th class="item">Pincode</th>
                                                    <td>{{ $employee->current_pincode }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('Permanent Address', 'Permanent Address', ['class' => 'col-sm-12 lable label-default form_label']) !!}
                                        </div>
                                        <div class="t_responsive2">
                                        <table class="no-border no-strip skills">
                                            <tbody class="no-border-x no-border-y">
                                                <tr>
                                                    <th class="item">Address Line 1</th>
                                                    <td>{{ $employee->permanent_address_line_1 }}</td>
                                                    <th class="item">Address Line 2</th>
                                                    <td>{{ $employee->permanent_address_line_2 }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Address Line 3</th>
                                                    <td>{{ $employee->permanent_address_line_3 }}</td>
                                                    <th class="item">State</th>
                                                    <td>{{ $employee->permanentState->name or '' }}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">City</th>
                                                    <td>{{ $employee->permanentCity->name or '' }}</td>
                                                    <th class="item">Pincode</th>
                                                    <td>{{ $employee->permanent_pincode or '' }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                        <div class="form-group{{ $errors->has('full_name') ? ' has-error' : ''}}">
                                            {!! Form::label('full_name', 'Personal Information ', ['class' => 'col-sm-12 lable label-default form_label']) !!}
                                        </div>
                                        <div class="t_responsive2">
                                            <table class="no-border no-strip skills">
                                                <tbody class="no-border-x no-border-y">
                                                    <tr>
                                                        <th class="item">Joining Date</th>
                                                        <td>{{ $employee->joining_date }}</td>
                                                        <th class="item">Designation</th>
                                                        <td>{{ $employee->designation->name or '' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Department</th>
                                                        <td>{{ $employee->subDepartment->department->name or '' }}</td>
                                                        <th class="item">Sub Department</th>
                                                        <td>{{ $employee->subDepartment->name or '' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Grade</th>
                                                        <td>{{ $employee->grade->name or '' }}</td>
                                                        <th class="item">Business Unit</th>
                                                        <td>{{ $employee->businessUnit->name or '' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Reporting Manager Name</th>
                                                        <td>{{ $employee->reporting_manager_name }}</td>
                                                        <th class="item">Reporting Manager Employee Code</th>
                                                        <td>{{ $employee->reporting_manager_employee_code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Legal Entity</th>
                                                        <td>{{ $employee->legalEntity->name or '' }}</td>
                                                        <th class="item">Store Name</th>
                                                        <td>{{ $employee->storeLocation->store_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Location Code</th>
                                                        <td>{{ $employee->storeLocation->location_code }}</td>
                                                        <th class="item">IO Code</th>
                                                        <td>{{ $employee->storeLocation->io_code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Employee Classification</th>
                                                        <td>{{ $employee->employeeClassification->name or '' }}</td>
                                                        <th class="item">Source Code</th>
                                                        <td>{{ $employee->sourceDetail->source_code or '' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Source Category</th>
                                                        <td>{{ $employee->sourceCategory->category or '' }}</td>
                                                        <th class="item">Source Name</th>
                                                        <td>{{ $employee->sourceDetail->source_name or '' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Employee refferal amount to be paid</th>
                                                        <td>{{ $employee->employee_refferal_amount }}</td>
                                                        <th class="item">Employee referral payout month</th>
                                                        <td>{{ $employee->employee_refferal_payment_month }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th class="item">Variable Pay Type</th>
                                                        <td>{{ $employee->variablePayType->name or '' }}</td>
                                                        <th class="item">Employee Code</th>
                                                        <td>{{ $employee->employee_code }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="education" class="tab-pane cont">
                                        <div class="table-responsive table03">
                                            <table class="table table-condensed table-bordered">
                                                <tr>
                                                    <th >Category</th>
                                                    <th>Board / University</th>
                                                    <th>Name of the Institution</th>
                                                    <th>Qualification Name</th>
                                                    <th>Specialization</th>
                                                    <th colspan="2" class="w-300" style="width: 15%;">Month & Year of passing</th>
                                                </tr>
                                                <tbody>
                                                    @foreach($employee->education as $key=>$value)
                                                        <tr>
                                                            <td>
                                                                {{ isset($educationCategory[$value['education_category_id']])?$educationCategory[$value['education_category_id']]:'' }}
                                                            </td>
                                                            <td>
                                                                {{ $value['board_uni'] }}
                                                            </td>
                                                            <td>
                                                                {{ $value['institute'] }}
                                                            </td>
                                                            <td>
                                                                {{$value['qualification']}}
                                                            </td>
                                                            <td>
                                                                {{ $value['specialization'] }}
                                                            </td>
                                                            <td>
                                                                {{ isset(config('constants.month_list')[$value['month']])?config('constants.month_list')[$value['month']]:null }}
                                                            </td>
                                                            <td>
                                                                {{ $value['year'] }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="experience" class="tab-pane cont">
                                        <div class="table-responsive table03">
                                        <table class="table table-condensed table-bordered">
                                            <tr>
                                                <th>Company Name</th>
                                                <th>Date Of Joining (DD-MM-YYYY)</th>
                                                <th>Date Of Leaving (DD-MM-YYYY)</th>
                                                <th>Nature of Employment</th>
                                                <th>Last held Designation</th>
                                                <th>Reason for leaving</th>
                                            </tr>
                                            <tbody id="experience_field">
                                                @foreach($employee->previous as $key=>$value)
                                                    <tr>
                                                        <td>
                                                            {{ $value['company_name'] }}
                                                        </td>
                                                        <td>
                                                            {{ $value['joining_date'] }}
                                                        </td>
                                                        <td>
                                                            {{ $value['leaving_date'] }}
                                                        </td>
                                                        <td>
                                                            {{ isset($natureOfEmployment[$value['nature_of_employment_id']])?$natureOfEmployment[$value['nature_of_employment_id']]:'' }}
                                                        </td>
                                                        <td>
                                                            {{ $value['designation'] }}
                                                        </td>
                                                        <td>
                                                            {{ $value['reason_of_leaving'] }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                    <div id="family" class="tab-pane cont">
                                        <div class="table-responsive table03">
                                            <table class="table table-condensed table-bordered">
                                                <tr class="text-center">
                                                    <th>Relationship</th>
                                                    <th>Gender</th>
                                                    <th>Aadhar Number</th>
                                                    <th>Name</th>
                                                    <th>Date of birth as per Aadhar</th>
                                                </tr>
                                                <tbody>
                                                    @foreach($employee->family as $key=>$value)
                                                        <tr>
                                                            <td>
                                                                {{ $value['relation'] }}
                                                            </td>
                                                            <td>
                                                                {{ ($value['gender']!='')?config('constants.gender')[$value['gender']]:'' }}
                                                            </td>
                                                            <td>
                                                                {{ $value['aadhar_number'] }}
                                                            </td>
                                                            <td>
                                                                {{ $value['name'] }}
                                                            </td>
                                                            <td>
                                                                {{ ($value['date'] != '--' && $value['date'] != '//')?$value['date']:'' }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="attachment" class="tab-pane cont">
                                        {{-- <a href="{{ url('/uploads/attachment.zip') }}" title="Back" download="attachments" target="_blank">
                                            <button class="btn btn-space btn-default">Download attachment ZIP</button>
                                        </a> --}}
                                        <div class="table-responsive table03">
                                            <table class="no-border no-strip skills">
                                                <tbody class="no-border-x no-border-y">
                                                    @php
                                                        $title = [
                                                            'scanned_photo'=>'Scanned Photo',
                                                            'employment_form'=>'Employement form attachment',
                                                            'form_11'=>'Form-11',
                                                            'form_1'=>'Form-1',
                                                            'form_02'=>'Form-02',
                                                            'form_f'=>'Form-F',
                                                            'hiring_checklist'=>'Hiring Checklist',
                                                            'inteview_assessment'=>'Interview Assessment',
                                                            'resume'=>'Resume',
                                                            'education_documents'=>'Education Documents',
                                                            'aadhar_card'=>'Aadhar Card',
                                                            'pan_card'=>'Pan Card',
                                                            'bank_passbook'=>'Bank Passbook/Cheque',
                                                            'offer_accept'=>'offer letter acceptancey mail',
                                                            'previous_company_docs'=>'Previouse company offer letter / payslips / releiving letter',
                                                        ];
                                                    @endphp

                                                    @foreach($employee->attachment as $value)
                                                        <tr>
                                                            <th class="item">{{ $title[$value->key] }}</th>
                                                            <td>
                                                                <div class="previous_company_docs-attach case"><a href="{{ url('/').'/'.$value->file }}" target="_blank"><i class="mdi mdi-case-download" title="{{ $title[$value->key] }}"></i></a></div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>



                                    <div id="otherinfo" class="tab-pane cont">
                                        
                                        <table class="table table-condensed table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th class="item">Date Of Leaving</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->dol : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Process Status</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->process_status : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Appointment Status</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->appointment_status : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">ID Card Available</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->id_card_status: ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Uniform Available</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->uniform_status : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">l0l1 Attempt</th>
                                                    <td>{{(isset($employee->otherinfo)) ?$employee->otherinfo->l0l1_status : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">l0l1 Result</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->l0l1_result : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">l0l1 Certificate</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->l0l1_certificate_status : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Background Verified</th>
                                                    <td>{{(isset($employee->otherinfo)) ?$employee->otherinfo->background_verification : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Background Verified Report</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->bv_report_status : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Is Pankh</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->is_disabled : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Pankh Detail</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->disability_desc : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Allocated Leave</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->allocated_leave : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Pending Leave</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->pending_leave : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Availed Leave</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->availed_leave : ''}}</td>
                                                </tr>
                                                <tr>
                                                    <th class="item">Salary(CTC)</th>
                                                    <td>{{(isset($employee->otherinfo)) ? $employee->otherinfo->salary : $employee->ctc}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection