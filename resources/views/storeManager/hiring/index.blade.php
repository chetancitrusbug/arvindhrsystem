@extends('storeManager.layouts.backend')
@section('title',"Hiring")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.confirmation_index')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
                        <p class="panel-head1">Hiring List</p>
                        <span class="panel-subtitle">
                            <a href="{{ url('/store/hiring/create') }}" title="New Joinee Request">
                                <button class="btn btn-space btn-success">New Joinee Request</button>
                            </a>
                            <a href="{{ url('/uploads/attachment.zip') }}" title="Back" download="attachments" target="_blank">
                                <button class="btn btn-space btn-default">Download Joinning Kit</button>
                            </a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <div class="panel-body table-responsive responsive-table1">
                            <table style="width:100%;" id="employee-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Employee Code</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Store Name</th>
                                        <th>Location</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/store/hiring-data') }}";
        var edit_url = "{{ url('/store/hiring') }}";
        var upload_url = "{{ url('/store/upload-documents') }}";
        var hold_url = "{{ url('/store/hold') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                highlight();
            },
            columns: [
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { data: 'designation',name : 'designation',"searchable": true, "orderable": false},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},
                { data: 'location_code',name : 'location_code',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
                        if(o.status == 'approve' || o.status == 'pending'){
                            return '<label class="label label-danger">Pending<label>';
                        }else if(o.status == 'confirm'){
                            return '<label class="label label-success">Approve<label>';
                        }
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":220,
                    "render": function (o) {
                        var e = v = d = r = l = h = u = "";

                        if(o.upload_documents){
                            if(o.store_hold == 0){
                                if(o.store_status == 0 || o.store_status == 3){
                                    r = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/request-region' value="+o.id+" data-id="+o.id+" class='btn btn-warning btn-sm request_from_index' title='Request to Regional HR'><i class='mdi mdi-check-all' ></i></a>&nbsp;";
                                }

                                v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                                if(o.store_status != 1){
                                    e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";
                                }

                                d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('store/hiring')}} data-msg='joinee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";

                                l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-success btn-sm' title='View Log' ><i class='mdi mdi-menu' aria-hidden='true'></i></button></a>&nbsp;";

                                if(o.regional_status != 2){
                                    u = "<a href='"+upload_url+"/"+o.id+"/1' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm' title='Update Document'><i class='mdi mdi-cloud-upload' ></i></a>&nbsp;";
                                }
                            }

                            if(o.store_hold == 0 && o.regional_status != 2){
                                h = "<a href='"+hold_url+"/1/"+o.id+"' value="+o.id+" data-id="+o.id+" class='btn btn-danger btn-sm' title='Hold'><i class='mdi mdi-lock-outline' ></i></a>&nbsp;";
                            }

                            if(o.store_hold == 1){
                                h = "<a href='"+hold_url+"/0/"+o.id+"' value="+o.id+" data-id="+o.id+" class='btn btn-success btn-sm' title='Unhold'><i class='mdi mdi-lock-open' ></i></a>&nbsp;";
                            }
                        }
                        else if(o.add_detail == 0 || o.add_education == 0 || o.add_experience == 0 || o.add_family_info == 0){
                            e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm'><i class='mdi mdi-edit highlight' ></i></a>&nbsp;";

                            u = "<a href='"+upload_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm' title='Upload Document'><i class='mdi mdi-cloud-upload' ></i></a>&nbsp;";
                        }
                        else{
                            u = "<a href='"+upload_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm' title='Upload Document'><i class='mdi mdi-cloud-upload' ></i></a>&nbsp;";
                        }
                        return v+e+u+r+l;
                    }

                }
            ]
        });
</script>
@endsection