@extends('storeManager.layouts.backend')
@section('title',"Edit Joinee")
@section('css')
<style type="text/css">
    .scanned_photo {
        position: relative;
    }

    .scanned_photo-attach {
        position: absolute;
        left: 65px;
        top: 0px;
    }

    img {
        height: 50px;
        width: 50px;
    }

    .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
            color: #fff !important;
            background-color: #2572f2 !important;
            border-color: transparent;
            border-top-color: #0c57d3 !important;
            box-shadow: inset 0 2px 0 #1266f1 !important;
        }
    input[type="file"]{
        border: none;
    }
    i.mdi.mdi-case-download{
        font-size: 30px;
    }
    .case{
        text-align: right;
    }
    .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
                color: #fff !important;
                background-color: #2572f2 !important;
                border-color: transparent;
                border-top-color: #0c57d3 !important;
                box-shadow: inset 0 2px 0 #1266f1 !important;
            }
    .form_label{
        border: none;
        padding: 10px;
        text-align: center;
        font-size: 16px;
        background: #eee;
    }
    .family_label{
        border: none;
        padding: 5px 5px 0px;
    }
    .text-center{
        text-align: center;
    }
    td {
        vertical-align: top !important;
    }
</style>

@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary panel-custom-a1">
                <div class="panel-heading panel-heading-divider">Edit Joinee # <strong>{{$employee->full_name}}</strong>
                    <span class="panel-subtitle">
                        <a href="{{ url('/store/hiring') }}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="tab-container">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#" data-toggle="tab"><strong>Personal Information</strong></a></li>
                                    <li><a href="{{url('store/hiring/createEducationDetail/'.$employee->id)}}"><strong>Education Detail</strong></a></li>
                                    <li><a href="{{url('store/hiring/createExperience/'.$employee->id)}}"><strong>Previous Work Experience</strong></a></li>
                                    <li><a href="{{url('store/hiring/createFamilyInfo/'.$employee->id)}}"><strong>Family Information</strong></a></li>
                                    <li><a href="{{url('store/hiring/createOtherInfo/'.$employee->id)}}"><strong>Other Information</strong></a></li>
                                    {{--
                                    <li><a href="#attachment" data-toggle="tab"><strong>Attachments</strong></a></li> --}}
                                </ul>
                                <div class="tab-content clearfix">
                                    <div id="detail" class="tab-pane active cont">
                                        {!! Form::model($employee, ['method' => 'PATCH','url' => ['/store/hiring', $employee->id],'class' => 'form-horizontal','id'
                                        => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                                            @include('storeManager.hiring.detail_form')
                                            @include('storeManager.modals.confirmation')

                                        <div class="form-group col-sm-9">
                                            {!! Form::submit('Next', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   {{-- {!! Form::model($employee, ['method' => 'PATCH','url' => ['/store/hiring', $employee->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        @include ('storeManager.hiring.form')

                        @include('storeManager.modals.confirmation')
                        <input type="hidden" name="button" value="submit">
                        <div class="form-group col-sm-6">
                            {!! Form::submit('Update & Send to RM', ['class' => 'btn btn-space btn-success pull-right send_to_rm']) !!}
                            {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!} --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection