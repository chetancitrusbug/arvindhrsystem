<div class="table-responsive table-responsive2">
    <table class="table table-condensed table-bordered">
        <tr>
            <th style="width: 15%;">Category</th>
            <th>Board / University</th>
            <th>Name of the Institution</th>
            <th>Qualification Name</th>
            <th>Specialization</th>
            <th colspan="2">Month & Year of passing</th>
            {{-- <th class="actions" align="center"><button class="btn btn-success disable" id="add"><i class="mdi mdi-plus"></i></button></th> --}}
        </tr>
        <tbody id="dynamic_field">
            @if($errors->has('education'))
                <tr>
                    <td colspan="8" class="has-error">
                        {!! $errors->first('education', '<p class="help-block">:message</p>') !!}
                    </td>
                </tr>
            @endif
            @php
                $education = old('education',isset($education)?$education:[]);
            @endphp
            @if(count($education))
                @foreach($education as $key=>$value)
                    <tr class="row{{ $key }}" data-row="{{ $key }}">
                        <td class='{{ $errors->has("education.$key.education_category_id") ? "has-error" : ""}}'>
                            {!! Form::select("education[$key][education_category_id]", [''=>'-- education category --']+$educationCategory, $value['education_category_id'] ,['class' => 'form-control input-sm disable']) !!}
                            @if(isset($value['id']) && !empty($value['id']))
                                {!! Form::hidden("education[$key][id]", $value['id'], ['class' => 'form-control input-sm']) !!}
                            @endif
                            {!! $errors->first("education.$key.education_category_id", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.board_uni") ? "has-error" : ""}}'>
                            {!! Form::text("education[$key][board_uni]", $value['board_uni'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.board_uni", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.institute") ? "has-error" : ""}}'>
                            {!! Form::text("education[$key][institute]", $value['institute'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.institute", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.qualification") ? "has-error" : ""}}'>
                            {!! Form::text("education[$key][qualification]", $value['qualification'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.qualification", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.specialization") ? "has-error" : ""}}'>
                            {!! Form::text("education[$key][specialization]", $value['specialization'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.specialization", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.month") ? "has-error" : ""}}'>
                            {!! Form::select("education[$key][month]", [''=>'-- Month --']+config('constants.month_list'), $value['month'] ,['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.month", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.year") ? "has-error" : ""}}'>
                            {!! Form::select("education[$key][year]", [''=>'-- Year --']+config('constants.year_list'), $value['year'] ,['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.year", '<p class="help-block">:message</p>') !!}
                        </td>
                        {{-- <td align="center"><button class="btn btn-danger remove disable" data-row='{{ $key }}'><i class="mdi mdi-minus"></i></button></td> --}}
                    </tr>
                @endforeach
            @else
                @for ($i = 0; $i < 5; $i++)
                    <tr class="row1" data-row="1">
                        <td>
                        
                            <?php 
                                if($i == 0){
                                    echo '<input type="hidden" name="education[0][education_category_id]" value="1" />
                                    <label class="form-control input-sm">10th</label>';
                                    
                                }else{
                            ?>
                            {!! Form::select("education[$i][education_category_id]", [''=>'-- education category --']+$educationCategory, null ,['class' => 'form-control input-sm']) !!}
                            <?php 
                                }
                            ?>
                        </td>
                        <td>
                            {!! Form::text("education[$i][board_uni]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("education[$i][institute]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("education[$i][qualification]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("education[$i][specialization]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::select("education[$i][month]", [''=>'-- Month --']+config('constants.month_list'), null ,['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::select("education[$i][year]", [''=>'-- Year --']+config('constants.year_list'), null ,['class' => 'form-control input-sm']) !!}
                        </td>
                        {{-- <td align="center"><button class="btn btn-danger remove" data-row='1'><i class="mdi mdi-minus"></i></button></td> --}}
                    </tr>
                @endfor
            @endif
        </tbody>
    </table>
</div>

@section('scripts')
    <script>
        datePicker();
        function datePicker(){
            $(".datetimepicker1").datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                componentIcon: '.mdi.mdi-calendar',
            /*    pickerPosition: "bottom-left", */
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });
        }
    </script>
@endsection