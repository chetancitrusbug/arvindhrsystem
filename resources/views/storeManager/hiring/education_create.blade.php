@extends('storeManager.layouts.backend')
@section('title',"Create Joinee")
@section('css')
    <style type="text/css">
        input[type="file"]{
            border: none;
        }
        .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
                color: #fff !important;
                background-color: #2572f2 !important;
                border-color: transparent;
                border-top-color: #0c57d3 !important;
                box-shadow: inset 0 2px 0 #1266f1 !important;
            }
            .form_label{
            border: none;
            padding: 10px;
            text-align: center;
            font-size: 16px;
            background: #eee;
        }
        .family_label{
            border: none;
            padding: 5px 5px 0px;
        }
        .text-center{
            text-align: center;
        }
        td {
            vertical-align: top !important;
        }
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary panel-custom-a1">
                <div class="panel-heading panel-heading-divider">
                    <p>New Joinee Request</p>
                    <span class="panel-subtitle">
                        <a href="{{ url('store/hiring/'.$id.'/edit') }}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="tab-container">
                                <ul class="nav nav-tabs">
                                    {{--<li><a href="{{url('store/hiring/'.$id.'/edit')}}"><strong>Personal Information</strong></a></li>
                                     <li><a href="{{url('store/hiring/create/'.$id)}}"><strong>Personal Information</strong></a></li>
                                    <li class="active"><a href="#"><strong>Education Detail</strong></a></li>
                                    <li><a href="{{url('store/hiring/createExperience/'.$id)}}"><strong>Previous Work Experience</strong></a></li>
                                    <li><a href="{{url('store/hiring/createFamilyInfo/'.$id)}}"><strong>Family Information</strong></a></li>
                                    <li><a href="{{url('store/hiring/createOtherInfo/'.$id)}}"><strong>Other Information</strong></a></li>
                                     <li><a href="#attachment"><strong>Attachments</strong></a></li> --}}
									<li><a href="#"><strong>Personal Information</strong></a></li>
									<li class="active"><a href="#"><strong>Education Detail</strong></a></li>
                                    <li><a href="#"><strong>Previous Work Experience</strong></a></li>
                                    <li><a href="#"><strong>Family Information</strong></a></li>
                                    <li><a href="#"><strong>Other Information</strong></a></li>
                                </ul>
                                <div class="tab-content clearfix">
                                    <div id="detail" class="tab-pane active cont">
                                        {!! Form::open(['url' => '/store/hiring/storeEducationDetail/'.$id, 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true])
                                        !!}
                                            @include ('storeManager.hiring.education_form')

                                            <div class="form-group col-sm-12">
                                                <div class="mr-15">
                                                    {!! Form::submit('Next', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                                                    <a href="{{url('store/hiring/'.$id.'/edit')}}" class="btn btn-space btn-warning pull-right">Previous</a>
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- {!! Form::open(['url' => '/store/hiring', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                    @include ('storeManager.hiring.form')

                    <div class="form-group col-sm-4">
                        {!! Form::submit('Create', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                    </div>
                {!! Form::close() !!} --}}
            </div>
        </div>
    </div>
</div>
@endsection