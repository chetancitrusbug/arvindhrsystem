@extends('storeManager.layouts.backend')
@section('title',"Employee")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.confirmation_index')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Employee List
                        <span class="panel-subtitle">
                            <a href="{{ url('/store/employee/create') }}" title="Create">
                                <button class="btn btn-space btn-success">Create</button>
                            </a>
                            <a href="{{ url('store/show-employee-upload') }}" title="Create" data-url="" class="btn btn-space btn-primary">
                                Excel Upload
                            </a>
                            <a href="{{ url('/uploads/attachment.zip') }}" title="Back" download="attachments" target="_blank">
                                <button class="btn btn-space btn-default">Download attachment ZIP</button>
                            </a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <table id="employee-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                   <!-- <th>Location Code</th>
                                    <th>Store Name</th> -->
                                    <th>DOJ</th>
                                    <th>CTC</th>
                                    <th>Regional Status</th>
                                    <th>HRMS Status</th>
									<th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/store/employee-data') }}";
        var edit_url = "{{ url('/store/employee') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                // statusChange();
            },
            columns: [
                { data: 'name',name : 'name',"searchable": true, "orderable": false},
                /* { data: 'location_code',name : 'location_code',"searchable": true, "orderable": false},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false}, */
                { data: 'joining_date',name : 'joining_date',"searchable": true, "orderable": false},
                { data: 'ctc',name : 'ctc',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "name" : 'regional_status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
						if(o.store_status == 1 && o.regional_status == 0){
							return '<label class="label label-primary">Request Sent<label>';
						}else if(o.regional_status == 1 || o.regional_status == 0){
                            return '<label class="label label-danger">Pending<label>';
                        }else if(o.regional_status == 2){
                            return '<label class="label label-success">Approve<label>';
                        }else if(o.regional_status == 3){
                            return '<label class="label label-warning">Send Back<label>';
                        }
                    }
                },
                {
                    "data": null,
                    "name" : 'hrms_status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
                        if(o.hrms_status == 1 || o.hrms_status == 0){
                            return '<label class="label label-danger">Pending<label>';
                        }else if(o.hrms_status == 2){
                            return '<label class="label label-success">Approve<label>';
                        }else if(o.hrms_status == 3){
                            return '<label class="label label-warning">send back<label>';
                        }
                    }
                },
				{
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
                        if(o.status == 'approve' || o.status == 'pending'){
                            return '<label class="label label-danger">Pending<label>';
                        }else if(o.status == 'confirm'){
                            return '<label class="label label-success">Approve<label>';
                        }
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d=r=l= "";

                        if(o.store_status == 0 || o.store_status == 3){
                            r = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/request-region' value="+o.id+" data-id="+o.id+" class='btn btn-warning btn-sm request_from_index' title='Request to Regional HR'><i class='mdi mdi-check-all' ></i></a>&nbsp;";
                            // a = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/approve' value="+o.id+" data-id="+o.id+" data-title='hrms' class='btn btn-success btn-sm approve_from_index' title='Approve'><i class='mdi mdi-redo' ></i></a>&nbsp;";
                        }

                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        if(o.store_status != 1){
                            e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='employee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";
                        }

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('store/employee')}} data-msg='employee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";

                        l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-success btn-sm' title='View' ><i class='mdi mdi-menu' aria-hidden='true'></i></button></a>&nbsp;";
                        return v+e+r+l;
                    }

                }
            ]
        });
</script>


@endsection