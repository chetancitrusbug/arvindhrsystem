@extends('storeManager.layouts.backend')
@section('title',"Employee Log")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Employee Log
                        <span class="panel-subtitle">
                            <a href="{{ url('/store/employee') }}" title="Create">
                                <button class="btn btn-space btn-primary  btn-back">Back</button>
                            </a>
                        </span>
                    </div>
                    <div class="panel-body">
                        <table id="employee-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>User Name</th>
                                    <th>User Role</th>
                                    <th>Note</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    {{-- <th>Action</th> --}}
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/store/employee-log/'.$id) }}";
        var edit_url = "{{ url('/store/employee') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                // statusChange();
            },
            columns: [
                { data: 'title',name : 'title',"searchable": true, "orderable": false, width:200},
                { data: 'user_name',name : 'user_name',"searchable": true, "orderable": false, width:200},
                { data: 'user_role',name : 'user_role',"searchable": true, "orderable": false, width:200},
                { data: 'note',name : 'note',"searchable": true, "orderable": false},
                { data: 'created_at',name : 'created_at',"searchable": false, "orderable": false, width:150},
                { data: 'updated_at',name : 'updated_at',"searchable": false, "orderable": false, width:150},
                /*{
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d=r=l= "";

                        if(o.store_status == 0 || o.store_status == 3){
                            r = "<a href='"+edit_url+"/"+o.id+"/request-region' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='Request to Regional HR' ><i class='mdi mdi-check-all' ></i></button></a>&nbsp;";
                        }

                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        if(o.store_status != 1){
                            e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='employee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";
                        }

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('store/employee')}} data-msg='employee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";

                        l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' >l</button></a>&nbsp;";
                        return v+e+r+l;
                    }

                }*/
            ]
        });
</script>


@endsection