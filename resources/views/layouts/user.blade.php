<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Arvind HR System') }}</title>

    <!-- Styles -->
    <style>
        .has-error {
            color: red;
        }
    </style>
    <script src="{{asset('backend/assets/lib/jquery/jquery.min.js')}}" type="text/javascript"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div id="app">
        <nav class=" navbar-expand-md navbar-light navbar-laravel">
            <div style="text-align:center" class="container text-center">
                <center>
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{url('/uploads/logo.png')}}" />
                </a>
                 </center>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('scripts')
</body>

</html>