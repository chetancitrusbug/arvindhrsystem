<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="assets/img/logo-fav.png">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- title --}}
        <title>@yield('title') {{ config('app.name') }}</title>

        <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/material-design-icons/css/material-design-iconic-font.min.css')}}"/><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/jqvmap/jqvmap.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
        <link rel="stylesheet" href="{{asset('backend/assets/css/style.css')}}" type="text/css"/>
        <link rel="stylesheet" href="{{asset('backend/assets/css/your-style.css')}}" type="text/css"/>

        <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/datatables/css/dataTables.bootstrap.min.css')}}" />
        @stack('css')
    </head>
    <body class="be-splash-screen">
        <div class="be-wrapper be-login">
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="splash-container">
                @include('includes.alerts')
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        <script src="{{asset('backend/assets/lib/jquery/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/js/main.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/datatables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/datatables/js/dataTables.bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/buttons.html5.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/buttons.flash.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/buttons.print.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/buttons.colVis.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/js/app-tables-datatables.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/js/bootstrap-checkbox.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/js/app-form-elements.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js')}}" type="text/javascript"></script>
        <script src="{{asset('backend/assets/js/custom.js')}}" type="text/javascript"></script>

        <script type="text/javascript">
        $(document).ready(function(){
            //initialize the javascript
            // App.init();
            // App.dashboard();
            setTimeout(function(){
                jQuery('.alert-dismissible').hide();
            }, 3000);

            $(".datetimepicker").datetimepicker({
                autoclose: true,
                format: 'dd-mm-yyyy',
                componentIcon: '.mdi.mdi-calendar',
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });

            $("#loading").hide();

        });


        </script>
        @stack('js')
    </body>
</html>