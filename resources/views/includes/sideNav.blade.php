<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Dashboard</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content" style="overflow-y:scroll;">
                    <ul class="sidebar-elements">
						<li class=""><a href="{{ url('admin/') }}">Dashboard</a></li>
                        <!--<li class="divider">Menu</li> -->

                       {{-- <li>
                            <a href="{{route('admin_suggestion')}}"><span>Suggestion</span></a>
                        </li> --}}

						<li class="parent">
							<a href="#"><span>Users Modules</span></a>
							<ul class="sub-menu">
                            <!--    <li class="{{(isset($route) && $route == 'users')?'active':''}}"><a href="{{ url('admin/users') }}"><span>Users</span></a></li>
                            -->
                            <li class="{{ request()->is('admin/user/hrms') ? 'active' : '' }}"><a href="{{ url('admin/user/hrms') }}"><span>Human Resource</span></a></li>


                                <li class="{{ request()->is('admin/user/regionalhr') ? 'active' : '' }}"><a href="{{ url('admin/user/regionalhr') }}"><span>Regional HR</span></a></li>
                                <li class="{{ request()->is('admin/user/storemanager') ? 'active' : '' }}"><a href="{{ url('admin/user/storemanager') }}"><span>Store Login</span></a></li>
								{{--<li class="{{(isset($route) && $route == 'role')?'active':''}}"><a href="{{ url('admin/roles') }}"> <span>Roles</span></a></li>
                                    <li class="{{(isset($route) && $route == 'permission')?'active':''}}"><a href="{{ url('admin/permissions') }}"><span>Permissions</span></a></li>
                                    {{(isset($route) && $route == 'user/hrms')?'active':''}}
                                    --}}
							</ul>
						</li>
						<li class="parent">
							<a href="#"><span>Employee Details</span></a>
							<ul class="sub-menu">
								<li class="{{(isset($route) && $route == 'employee')?'active':''}}"><a href="{{ url('admin/regionallist') }}"><span>Employee</span></a></li>
                                <li class="{{(isset($route) && $route == 'employee_classification')?'active':''}}"><a href="{{ url('admin/employee-classification') }}"><span>Employee Classification</span></a></li>

									{{--
									<li class="{{(isset($route) && $route == 'employee')?'active':''}}"><a target="_blank" href="{{ url('admin/export-employee-details') }}"><span>Export All Employee</span></a></li>
									<li class="{{(isset($route) && $route == 'hiring-report')?'active':''}}"><a href="{{ url('admin/hiring-report') }}"><span>Hiring Report</span></a></li>
									<li class="{{(isset($route) && $route == 'attendance_report')?'active':''}}"><a href="{{ url('admin/attendance-report') }}"><span>Attendance Report</span></a></li>--}}
							</ul>
						</li>
						<li class="{{(isset($route) && $route == 'store_collection')?'active':''}}"><a href="{{ url('admin/storecollection') }}"><span>Store Sales </span></a></li>
						<li class="parent">
							<a href="#"><span>Store Details</span></a>
							<ul class="sub-menu">

                                <li class="{{(isset($route) && $route == 'store_location')?'active':''}}"><a href="{{ url('admin/store-location') }}"><span>Store Location</span></a></li>
                                <li class="{{(isset($route) && $route == 'store-budget')?'active':''}}"><a href="{{ url('admin/store-budget-national') }}"><span>Store Budget Man Power</span></a></li>
								{{--<li class="{{(isset($route) && $route == 'man_power')?'active':''}}"><a href="{{ url('admin/man-power') }}"><span>Man Power</span></a></li>--}}
							</ul>
						</li>
                        <li class="parent">
							<a href="#"><span>Master</span></a>
							<ul class="sub-menu">

                                <li class="{{(isset($route) && $route == 'designation')?'active':''}}"><a href="{{ url('admin/designation') }}"><span>Designation</span></a></li>
                                <li class="{{(isset($route) && $route == 'grade')?'active':''}}"><a href="{{ url('admin/grade') }}"><span>Grade</span></a></li>
                                <li class="{{(isset($route) && $route == 'department')?'active':''}}"><a href="{{ url('admin/department') }}"><span>Department</span></a></li>
                                <li class="{{(isset($route) && $route == 'sub_department')?'active':''}}"><a href="{{ url('admin/sub-department') }}"><span>Sub Department</span></a></li>
                                <li class="{{(isset($route) && $route == 'business_unit')?'active':''}}"><a href="{{ url('admin/business-unit') }}"><span>Business Unit</span></a></li>
                                <li class="{{(isset($route) && $route == 'brand')?'active':''}}"><a href="{{ url('admin/brand') }}"><span>Brand</span></a></li>
                                <!--<li class="{{(isset($route) && $route == 'legal_entity')?'active':''}}"><a href="{{ url('admin/legal-entity') }}"><span>Legal Entity</span></a></li> -->
                                <li class="{{(isset($route) && $route == 'source_category')?'active':''}}"><a href="{{ url('admin/source-category') }}"><span>Source Category</span></a></li>
                                <li class="{{(isset($route) && $route == 'variable_pay_type')?'active':''}}"><a href="{{ url('admin/variable-pay-type') }}"><span>Variable Pay Type</span></a></li>
								<li class="{{(isset($route) && $route == 'state')?'active':''}}"><a href="{{ url('admin/state') }}"><span>State</span></a></li>
								<li class="{{(isset($route) && $route == 'city')?'active':''}}"><a href="{{ url('admin/city') }}"><span>City</span></a></li>
								<li class="{{(isset($route) && $route == 'region')?'active':''}}"><a href="{{ url('admin/region') }}"><span>Region</span></a></li>
							</ul>
                        </li>
                        <li class="parent">
                            <a href="#"><span>Reports</span></a>
                            <ul class="sub-menu">
                                <li class="{{(isset($route) && $route == 'manpowerreports')?'active':''}}"><a href="{{ url('admin/manpower-report/region') }}"><span>Manpower Report </span></a></li>
                                <li class="{{(isset($route) && $route == 'manpower-cost')?'active':''}}"><a href="{{ url('admin/manpower-cost-report/region') }}"><span>Manpower Cost Report</span></a></li>
                                <li class="{{(isset($route) && $route == 'hygiene-report')?'active':''}}"><a href="{{ url('admin/hygiene-report/region') }}"><span>Hygiene Report</span></a></li>
                                <li class="{{(isset($route) && $route == 'hiringreport')?'active':''}}"><a href="{{ url('admin/hiring-report/region') }}"><span>Hiring Report</span></a></li>
                                <li class="{{(isset($route) && $route == 'hiring-inprocess')?'active':''}}"><a href="{{ url('admin/hiring-inprocess') }}"><span>Hiring inprocess Report</span></a></li>
                                <li class="{{(isset($route) && $route == 'hiring-source-report')?'active':''}}"><a href="{{ url('admin/hiring-source-report') }}"><span>Hiring Source Report</span></a></li>
                                <li class="{{(isset($route) && $route == 'leave-report')?'active':''}}"><a href="{{ url('admin/leave-report') }}"><span>Leave Report</span></a></li>
                                <!--<li class="{{(isset($route) && $route == 'legal_entity')?'active':''}}"><a href="{{ url('admin/legal-entity') }}"><span>Legal Entity</span></a></li> -->
                                <li class="{{(isset($route) && $route == 'attendance-report')?'active':''}}"><a href="{{ url('admin/attendance-report/region') }}"><span>Attendance Report</span></a></li>
                                <li class="{{(isset($route) && $route == 'attrition-report')?'active':''}}"><a href="{{ url('admin/attrition-report/region') }}"><span>Attrition Report</span></a></li>
                                <li class="{{(isset($route) && $route == 'attrition-reson')?'active':''}}"><a href="{{ url('admin/attrition-report-reason/region') }}"><span>Attrition Report Reason Wise</span></a></li>

                                <li class="{{(isset($route) && $route == 'attrition-promise')?'active':''}}"><a href="{{ url('attrition-report-promise/region') }}"><span>Promise Attrition Report </span></a></li>

                                <li class="{{(isset($route) && $route == 'productivity-report')?'active':''}}"><a href="{{ url('admin/productivity-report') }}"><span>Productivity Report</span></a></li>

                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>