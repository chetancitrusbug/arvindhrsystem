<!DOCTYPE html>
<html style="height: 100%;margin-top: 0;margin-bottom: 0;margin-right: 0;margin-left: 0;margin: 0;">

<body style="height: 100%;margin-top: 0;margin-bottom: 0;margin-right: 0;margin-left: 0;margin: 0;">

    <div class="bgimg">
        <div class="topleft" style="position: absolute;top: 0;left: 16px;">
            <p>ArvindHR</p>
        </div>
        <div class="middle" style="position:absolute;top:50%;left:50%;transform:translate(-50%, -50%);text-align:center;">
        <h3 style="color:darkcyan">Store Collection Updated  For Month {{$store_collection->month}} - {{$store_collection->year}}</h3>
            <hr style="margin-top: auto;margin-bottom: auto;margin-right: auto;margin-left: auto;width: 40%;margin: auto;">
            Store Collection amount Updated {{$store_collection->amount}} To {{$updated_value['amount']}} for {{$store_collection->store_name}} Store. with io code {{$store_collection->io_code}}
        </div>
        <div class="bottomleft" style="position: absolute;bottom: 0;left: 16px;">
            <p>*</p>
        </div>
    </div>
</body>

</html>