@extends('regional.layouts.backend')
@section('title',"Hiring Report")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">
                        Hiring Report - Region
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-warning">Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="hiring-reports" class="table table-striped">
                                        <thead class="primary">
                                            <tr>
                                                <th>Store IO Code</th>
                                                <th>Store Name</th>
                                                <th>GAP</th>
                                                <th>In Process</th>
                                                <th>Hired</th>
                                            </tr>
                                        </thead>
                                        <tbody class="no-border-x">
                                            @if ($hiringReports)
                                                @foreach ($hiringReports as $hiring)
                                                    <tr class="clickable-row" data-href="{{url('regional/store-hiring-report/')}}/{{$hiring['store_id']}}">
                                                        <td>{{$hiring['io_code']}}</td>
                                                        <td>{{$hiring['store_name']}}</td>
                                                        <td>{{($hiring['gap'] > 0) ? $hiring['gap'] : 0 }}</td>
                                                        <td>{{($hiring['in_process'] > 0) ? $hiring['in_process'] : 0 }}</td>
                                                        <td>{{($hiring['hired'] > 0) ? $hiring['hired'] : 0 }}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td>No Data Found</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                        <tfoot class="primary">
                                            <tr>
                                                <th></th>
                                                <th>Total</th>
                                                <th>GAP</th>
                                                <th>In Process</th>
                                                <th>Hired</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(".clickable-row").click(function() { window.location = $(this).data("href"); });
$("#hiring-reports").DataTable({
		dom: 'Blfrtip',
		buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_gap = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_inprocess = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_hired = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total_gap );
            $( api.column( 3 ).footer() ).html( total_inprocess );
            $( api.column( 4 ).footer() ).html( total_hired );

        }
    });
</script>
@endsection