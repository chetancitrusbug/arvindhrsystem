@extends('regional.layouts.backend')
@section('title',"Attrition Report")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Attrition Report
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-warning">Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="employee-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Region</th>
                                                <th>Store Code</th>
                                                <th>Store Name</th>
                                                <th>Total</th>
                                                <th>Resignation</th>
                                                <th>Absconding</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Total</th>
                                                <th>Resignation</th>
                                                <th>Absconding</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('/regional/attrition-report/store-data/') }}/{{Auth::user()->region}}";
        var view_url = "{{ url('/regional/attrition-report/store/') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'name',name : 'name',"searchable": true, "orderable": false, "width":200},
                { data: 'io_code',name : 'io_code',"searchable": true, "orderable": false, "width":200},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false, "width":200},
                { data: 'Total',name : 'Total',"searchable": true, "orderable": false, "width":150},
                { data: 'absconding',name : 'absconding',"searchable": true, "orderable": false, "width":150},
                { data: 'resignation',name : 'resignation',"searchable": true, "orderable": false, "width":100},

                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var v="";
                        v = "<a href='"+view_url+"/"+o.store_id+"' value="+o.store_id+" data-id="+o.store_id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        return v;
                    }
                }
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_resignation = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_absconding = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 3 ).footer() ).html( total );
            $( api.column( 4 ).footer() ).html( total_resignation );
            $( api.column( 5 ).footer() ).html( total_absconding );

        }
        });

</script>
@endsection