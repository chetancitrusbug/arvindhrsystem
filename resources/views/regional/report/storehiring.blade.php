@extends('regional.layouts.backend')
@section('title',"Hygiene")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Hiring Reports Store(YTD) (<small>{{$store_name}}</small>)
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-primary ">Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <table style="width:100%;" id='hygiene-report' class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th class="hide">ID</th>
                                            <th>Month Year</th>
                                            <th>GAP</th>
                                            <th>Hired</th>
                                            <th>Pending</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">

                                        @foreach ($hiringReport as $hiring)
                                        <tr>
                                            <td class="hide">
                                                {{$hiring->id}}
                                            </td>
                                            <td data-orderable="false">
                                                @php
                                                    echo date('M-Y',strtotime('01-'.$hiring->monthyear));
                                                @endphp
                                            </td>
                                            <td>{{($hiring->gap > 0) ? $hiring->gap : 0 }}</td>
                                            <td>{{($hiring->hired > 0) ? $hiring->hired : 0 }}</td>
                                            <td>{{($hiring->pending > 0) ? $hiring->pending : 0 }}</td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#hygiene-report").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
    });

</script>
@endsection