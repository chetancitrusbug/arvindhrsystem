@extends('regional.layouts.backend')
@section('title',"Hiring - Inprocess Report- Store")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Hiring - Inprocess Report- Store
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-warning">Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="hiring-inprocess-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Store IO Code</th>
                                                <th>Store Name</th>
                                                <th>Pending at Store</th>
                                                <th>Pending at RHR</th>
                                                <th>Peding at HRMS</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Pending at Store</th>
                                                <th>Pending at RHR</th>
                                                <th>Peding at HRMS</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('/regional/hiring-inprocess/store-data/')}}/{{Auth::user()->region}}";
        var auth_check = "{{ Auth::check() }}";
        datatable = $('#hiring-inprocess-table').DataTable({
            dom: 'Blfrtip',
            buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'io_code',name : 'io_code',"searchable": true, "orderable": false, "width":200},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false, "width":200},
                { data: 'store_pending',name : 'store_pending',"searchable": true, "orderable": false, "width":200},
                { data: 'region_pending',name : 'region_pending',"searchable": true, "orderable": false, "width":150},
                { data: 'hrms_pending',name : 'hrms_pending',"searchable": true, "orderable": false, "width":150},
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_sp = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_rp = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_hp = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total_sp );
            $( api.column( 3 ).footer() ).html( total_rp );
            $( api.column( 4 ).footer() ).html( total_hp )
        }
        });

</script>
@endsection