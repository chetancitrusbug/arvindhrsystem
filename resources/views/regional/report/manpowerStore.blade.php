@extends('regional.layouts.backend')
@section('title',"Man power")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Man Power List For Region (<small>{{$region->name}}</small>)
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-warning">Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table id="man-power-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Store Name</th>
                                                <th>Store Code</th>
                                                <th>Location</th>
                                                <th>Region</th>
                                                <th>Budget Man Power</th>
                                                <th>Actual Man Power</th>
                                                <th>Gap</th>
                                            </tr>
                                        </thead>
                                        {{-- <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Budget Man Power</th>
                                                <th>Actual Man Power</th>
                                                <th>Gap</th>
                                            </tr>
                                        </tfoot> --}}
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr/>
                    <div class="panel-heading panel-heading-divider clearfix">Store Budget Manpower Report
                        <a href="{{url('regional/store-budget-export')}}" class="pull-right"><button class="btn btn-success"> Excel </button> </a>
                    </div>
                    <br/>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="employee-budget-table" class="table table-striped responsive table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Store IO Code</th>
                                                <th>Store Name</th>
                                                @foreach ($designations as $designation)
                                                <th>{{$designation}}</th>
                                                @endforeach
                                                <th>Total</th>

                                            </tr>
                                        </thead>
                                        @if (count($store_reports) > 0)
                                        <tbody>

                                            @foreach ($store_reports as $store_report)
                                            <tr>
                                                <td>{{$store_report['io_code']}}</td>
                                                <td>{{$store_report['store_name']}}</td>
                                                @foreach ($store_report['designation'] as $key => $value)
                                                <td>{{$value}}</td>
                                                @endforeach
                                            </tr>

                                            @endforeach
                                        </tbody>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/regional/manpower-report/storedata') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#man-power-table').DataTable({
            dom: 'Blfrtip',
            buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false, "width":200},
                { data: 'store_code',name : 'store_code',"searchable": true, "orderable": false, "width":150},
                { data: 'city',name : 'city',"searchable": true, "orderable": false, "width":150},
                { data: 'name',name : 'name',"searchable": false, "orderable": false, "width":150},
                { data: 'budget_man_power',name : 'budget_man_power',"searchable": true, "orderable": false, "width":100},
                { data: 'actual_man_power',name : 'actual_man_power',"searchable": true, "orderable": false, "width":100},
                { data: 'gap',name : 'gap',"searchable": true, "orderable": false, "width":100},

            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_budget = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_actual = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_gap = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 4 ).footer() ).html( total_budget );
            $( api.column( 5 ).footer() ).html( total_actual );
            $( api.column( 6 ).footer() ).html( total_gap );



        }

        });
        datatable = $('#employee-budget-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            responsive: true,
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
         });
</script>
@endsection
