@extends('regional.layouts.backend')
@section('title',"Productivity Reports Region")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">

                    <div class="panel-heading">Productivity report by manpower(MTD)
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-primary ">Back</button>
                        </a>
                    </div>
                        @php
                            $current_year = date('Y');
                            $current_month = date('m');
                            $continue_month = date('m');
                            $current_month = $current_month-1;
                            $months = array();
                            $years = array();
                            if($continue_month == 1){
                                $current_month = 12;
                                $current_year--;
                            }
                            for ($j = $current_year; $j >= $current_year-5; $j--) {
                                $years[$j] = $j;
                            }
                            for($i=1; $i <= $current_month ;$i++){
                                $months[$i]=date( 'M',strtotime( "$current_year-$i-01"));
                            }
                            $current_month_name = date('M',strtotime("$current_year-$current_month-01"));

                            if(app('request')->input('year') && app('request')->input('month') ){
                                $current_month = app('request')->input('month');
                                $current_year = app('request')->input('year');
                            }
                        @endphp

                    <div class="panel-body">
                        <div class="form col-md-offset-1">
                            {!! Form::open(['method'=>'GET', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off',
                            'files'=>false]) !!}
                            <div class="form-group col-sm-5 {{ $errors->has('month') ? ' has-error' : ''}}">
                                {!! Form::label('', '* Select Month: ', ['class' => '']) !!}
                                <div data-min-view="2" id="" class="input-group date">
                                    {!! Form::select('month', $months,$current_month, ['class' => 'col-sm-12 form-control input-sm month','id'=>'month']) !!}
                                    {!! $errors->first('month','
                                    <p class="help-block">:message</p>') !!}
                                </div>

                            </div>
                            <div class="form-group col-sm-5 {{ $errors->has('year') ? ' has-error' : ''}}">
                                {!! Form::label('', '* Select year: ', ['class' => '']) !!}
                                <div data-min-view="2" id="" class="input-group date">
                                    {!! Form::select('year', $years,$current_year, ['class' => 'col-sm-12 form-control input-sm month','id'=>'year']) !!} {!!
                                    $errors->first('year','
                                    <p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group col-sm-2">
                                {!! Form::label('', '&nbsp; ', ['class' => '']) !!}
                                <div class="col-sm-12">
                                    {{Form::submit('Filter',['class'=>'btn btn-primary'])}}
                                </div>
                            </div>
                            {{Form::close()}}
                        </div>
                        <table style="width:100%;" id="store-productivity-report" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Store Code</th>
                                    <th>Store Name</th>
                                    <th>Sales</th>
                                    <th>No. of employee</th>
                                    <th>Sales per employee</th>
                                    <th>Avg cost per emp</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Total</th>
                                    <th>Sales</th>
                                    <th>No. of employee</th>
                                    <th>Sales per employee</th>
                                    <th>Avg cost per emp</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('regional/productivity-report-store-data') }}/{{ Auth::user()->region }}";
    var extraurl = "?year={{app('request')->input('year')}}&month={{app('request')->input('month')}}";
    url = url+extraurl;
        datatable = $('#store-productivity-report').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
			searchable:false,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'io_code',name : 'io_code',"searchable": false, "orderable": false, "width":200},
                { data: 'store_name',name : 'store_name',"searchable": false, "orderable": false, "width":200},
                { data: 'total_amount',name : 'total_amount',"searchable": false, "orderable": true, "width":150},
                { data: 'total_employee',name : 'total_employee',"searchable": false, "orderable": true, "width":150},
                { data: 'per_person_income',name : 'per_person_income',"searchable": false, "orderable": true, "width":150},
                { data: 'avg_ctc',name : 'avg_ctc',"searchable": false, "orderable": true, "width":150},
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_sales = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_employee = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_sales_per_employee = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_average = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total_sales );
            $( api.column( 3 ).footer() ).html( total_employee );
            $( api.column( 4 ).footer() ).html( '-' );
            $( api.column( 5 ).footer() ).html( '-' );




        }

        });

        $("#start_date_div").datetimepicker({
            format:'dd-mm-yyyy',
            endDate: new Date(),
            maxDate: 'today',
            autoclose: true,
            componentIcon: '.mdi.mdi-calendar'
        });
        $("#end_date_div").datetimepicker({
            endDate: new Date(),
            maxDate: 'today',
            format:'dd-mm-yyyy',
            autoclose: true,
            componentIcon: '.mdi.mdi-calendar'
        });

        $("#year").change(function() {

            var current_year = (new Date()).getFullYear();
            var current_month = (new Date()).getMonth();
            var selected_year =  $(this).val()

            var monthArray = new Array();
            monthArray[1] = "Jan";
            monthArray[2] = "Feb";
            monthArray[3] = "Mar";
            monthArray[4] = "Apr";
            monthArray[5] = "May";
            monthArray[6] = "Jun";
            monthArray[7] = "Jul";
            monthArray[8] = "Aug";
            monthArray[9] = "Sep";
            monthArray[10] = "Oct";
            monthArray[11] = "Nov";
            monthArray[12] = "Dec";
            if(current_year != selected_year){
                    $("#month").html('');
                    for(m = 1; m <=12; m++) {
                        var monthElem = document.createElement("option");
                        monthElem.value=m ;
                        monthElem.textContent=monthArray[m];
                        $("#month").append(monthElem);
                    }


            } else {
               $("#month").html('');
               for(m = 1; m <=current_month; m++) {
                   var monthElem=document.createElement( "option");
                   monthElem.value=m ;
                   monthElem.textContent=monthArray[m];
                    $( "#month").append(monthElem);
                }
            }

        });


</script>
@endsection