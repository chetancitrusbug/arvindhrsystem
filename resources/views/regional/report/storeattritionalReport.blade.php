@extends('regional.layouts.backend')
@section('title',"Attrition Report")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Attrition Report
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-warning ">Back</button>
                        </a>
                        <!--  <span class="panel-subtitle">
                            <a href="{{ url('admin/man-power-upload') }}" title="Create" data-url="" class="btn btn-space btn-primary">
                                Excel Upload
                            </a>
                        </span> -->
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="attrition-reports" class="table table-striped">
                                        <thead class="primary">
                                            <tr>
                                                <th>Store Code</th>
                                                <th>Store</th>
                                                <th>Total</th>
                                                <th>Resignation</th>
                                                <th>Absconding</th>
                                                <th>Leaving in % (Percentages)</th>
                                            </tr>
                                        </thead>
                                        @if($store)

                                        <tbody class="no-border-x">
                                            <tr>
                                                <td>{{$store->io_code}}</td>
                                                <td>{{$store->store_name}}</td>
                                                <td>{{($totalattritionReport)?($totalattritionReport->Total > 0) ? $totalattritionReport->Total : 0:'-' }}</td>
                                                <td>{{($totalattritionReport)?($totalattritionReport->resignation > 0) ? $totalattritionReport->resignation : 0:'-' }}</td>
                                                <td>{{($totalattritionReport)?($totalattritionReport->absconding > 0) ? $totalattritionReport->absconding : 0:'-' }}</td>
                                                <td>
                                                    @if ($totalattritionReport)
                                                        @if ($totalattritionReport->actual_man_power > 0 && $totalattritionReport->Total > 0)
                                                            @php
                                                                echo ($totalattritionReport->actual_man_power/$totalattritionReport->Total)*100;
                                                            @endphp
                                                            %
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="primary">
                                            <tr>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Total</th>
                                                <th>Resignation</th>
                                                <th>Absconding</th>
                                                <th>Leaving in % (Percentages)</th>
                                            </tr>
                                        </tfoot>
                                        @else NO Data Found @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>

        datatable = $('#attrition-reports').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            "caseInsensitive": false,
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_resignation = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_absconding = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total );
            $( api.column( 3 ).footer() ).html( total_resignation );
            $( api.column( 4 ).footer() ).html( total_absconding );
            $( api.column( 5 ).footer() ).html( '-' );

        }
        });

</script>
@endsection