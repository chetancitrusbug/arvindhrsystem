@extends('regional.layouts.backend')
@section('title',"Leave Reports")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Leave Reports Employee
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-primary ">Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table style="width:100%;" id="leave-report" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Employee Code</th>
                                        <th>Employee Name</th>
                                        <th>Allocated Leave</th>
                                        <th>Pending Leave</th>
                                        <th>Availed Leaves</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Total</th>
                                        <th>Allocated Leave</th>
                                        <th>Pending Leave</th>
                                        <th>Availed Leaves</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('/regional/leave-report-employee-data/') }}/{{request()->route('store_id')}}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#leave-report').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": true, "width":200},
                { data: 'employee_name',name : 'employee_name',"searchable": true, "orderable": true, "width":200},
                { data: 'allocated_leave',name : 'allocated_leave',"searchable": false, "orderable": false, "width":150},
                { data: 'pending_leave',name : 'pending_leave',"searchable": false, "orderable": false, "width":150},
                { data: 'availed_leave',name : 'availed_leave',"searchable": false, "orderable": false, "width":150},

            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_allocated = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_pending = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_availed = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total_allocated );
            $( api.column( 3 ).footer() ).html( total_pending );
            $( api.column( 4 ).footer() ).html( total_availed );

        }
        });

</script>
@endsection