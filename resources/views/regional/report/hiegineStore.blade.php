@extends('regional.layouts.backend')
@section('title',"Hygiene")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Hygiene For Region (<small>{{$region->name}}</small>)
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-warning">Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="man-power-table" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Store Name</th>
                                                <th>Store Code</th>
                                                <th>Region</th>
                                                <th>Id Card Not Available</th>
                                                <th >Uniform not available</th>
                                                <th>L0L1 untrained</th>
                                                <th>L0L1 non certified</th>
                                                <th>Certificate pending</th>
                                                <th>Appointment Letter</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Id Card Not Available</th>
                                                <th>Uniform not available</th>
                                                <th>L0L1 untrained</th>
                                                <th>L0L1 non certified</th>
                                                <th>Certificate pending</th>
                                                <th>Appointment Letter</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/regional/hygiene-report/storedata') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#man-power-table').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
			searchable:false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'store_name',name : 'store_name',"searchable": false, "orderable": false, "width":200},
                { data: 'store_code',name : 'store_code',"searchable": false, "orderable": false, "width":150},
                { data: 'region_name',name : 'region_name',"searchable": false, "orderable": false, "width":150},
                { data: 'id_card',name : 'id_card',"searchable": false, "orderable": false, "width":150},
                { data: 'uniform',name : 'uniform',"searchable": false, "orderable": false, "width":150},
                { data: 'l0l1_untrained',name : 'l0l1_untrained',"searchable": false, "orderable": false, "width":150},
                { data: 'l0l1_certified',name : 'l0l1_certified',"searchable": false, "orderable": false, "width":150},
                { data: 'l0l1_certified',name : 'l0l1_certified',"searchable": false, "orderable": false, "width":100},
                { data: 'appointment',name : 'appointment',"searchable": false, "orderable": false, "width":100},
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_id_card = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_uniform = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_untrain = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_non_certificate = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_certificate_pending = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_appointment = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 3 ).footer() ).html( total_id_card );
            $( api.column( 4 ).footer() ).html( total_uniform );
            $( api.column( 5 ).footer() ).html( total_untrain );
            $( api.column( 6 ).footer() ).html( total_non_certificate );
            $( api.column( 7 ).footer() ).html( total_certificate_pending );
            $( api.column( 8 ).footer() ).html( total_appointment );

        }
        });
</script>
@endsection
