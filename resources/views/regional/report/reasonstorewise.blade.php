@extends('regional.layouts.backend')
@section('title',"Attrition Report")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading panel-heading-divider clearfix">Attrition Report - By Reason of Attrition
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-warning">Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="attrition-region-report" class="table table-striped table-hover table-fw-widget">
                                        <thead>
                                            <tr>
                                                <th>Store Name</th>
                                                <th>Store Code</th>
                                                @php
                                                    $footer_total = array();
                                                @endphp
                                                @if ($reasons)

                                                    @foreach ($reasons as $reason)
                                                        <th>{{$reason->reason}}</th>
                                                        @php
                                                            $footer_total[$reason->id] = 0;
                                                        @endphp
                                                    @endforeach
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($report_data) > 0) @foreach ($report_data as $report)
                                            <tr>
                                                <td>{{$report['store_name']}}</td>
                                                <td>{{$report['io_code']}}</td>
                                                @foreach ($reasons as $reason)
                                                <td>{{$report[$reason->id]}}</td>
                                                    @php
                                                        $footer_total[$reason->id] = ($footer_total[$reason->id] + $report[$reason->id]);
                                                    @endphp
                                                @endforeach
                                            </tr>
                                            @endforeach @else
                                            <tr>
                                                <td>
                                                    No Data Found
                                                </td>
                                            </tr>
                                            @endif

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Total</th>
                                                @foreach ($footer_total as $key =>$value)
                                                    <th>{{$value}}</th>
                                                @endforeach
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#attrition-region-report").DataTable({
        dom: 'Blfrtip',
        buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 480, "scrollX": true,
    });
</script>
@endsection