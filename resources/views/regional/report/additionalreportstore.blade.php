@extends('regional.layouts.backend')
@section('title',"Additional Report")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider clearfix">
                        <div class="tools"></div><span class="title">Additional Report - Store</span>
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-warning">Back</button>
                        </a>
                        <span class="panel-subtitle"></span>
                    </div>
                    <div class="blk-div margin-top-20 clearfix">
                        {!! Form::open([ 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true,'method' => 'get']) !!}
                        {!! Form::hidden("start_date", null ,['class'=>'input_start_date']) !!}
                        {!! Form::hidden("end_date", null ,['class'=>'input_end_date']) !!}

                        <div class="col-sm-4">
                            <div style="width: 100%">
                                <div id="reportrange" class="form-control input-sm"  style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;height: 32px;">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    <span></span> <b class="caret"></b>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-3 pull-left">
                            <div class="">
                                {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                            </div>
                            <br/>
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="panel-body">

                        <div class="panel-heading panel-heading-divider clearfix">
                            <div class="tools"></div><span class="title">Total Gender Wise Report - Store</span><span class="panel-subtitle"></span>
                        </div>
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="total-gender" class="table table-striped">
                                        <thead class="primary">
                                            <tr>
                                                <th>Store Code</th>
                                                <th>Store Name</th>
                                                <th>Male</th>
                                                <th>Female</th>
                                            </tr>
                                        </thead>
                                        <tbody class="no-border-x">
                                            @if(count($genderwisetotalemployee) > 0 ) @foreach ($genderwisetotalemployee as $gender)
                                            <tr>
                                                <td>{{$gender['io_code']}}</td>
                                                <td>{{$gender['store_name']}}</td>
                                                <td>{!!($gender['male'] > 0)? $gender['male'] : 0 !!}</td>
                                                <td>{!!($gender['female'] > 0)? $gender['female'] : 0 !!}</td>
                                            </tr>
                                            @endforeach @endif
                                        </tbody>
                                        <tfoot class="primary">
                                            <tr>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Male</th>
                                                <th>Female</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading panel-heading-divider clearfix">
                            <div class="tools"></div><span class="title">Gender Wise Attrition Report - Store</span><span class="panel-subtitle"></span>
                        </div>
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="attrition-gender" class="table table-striped">
                                        <thead class="primary">
                                            <tr>
                                                <th>Store Code</th>
                                                <th>Store Name</th>
                                                <th>Male</th>
                                                <th>Female</th>
                                            </tr>
                                        </thead>
                                        <tbody class="no-border-x">
                                            @if(count($genderwiseattrition) > 0 ) @foreach ($genderwiseattrition as $gender)
                                            <tr>
                                                <td>{{$gender['io_code']}}</td>
                                                <td>{{$gender['store_name']}}</td>
                                                <td>{!!($gender['male'] > 0)? $gender['male'] : 0 !!}</td>
                                                <td>{!!($gender['female'] > 0)? $gender['female'] : 0 !!}</td>
                                            </tr>
                                            @endforeach @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Male</th>
                                                <th>Female</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading panel-heading-divider clearfix">
                            <div class="tools"></div><span class="title">Gender Wise Hiring Report - Store</span><span class="panel-subtitle"></span>
                        </div>
                        <div class="col-md-12">
                            <div class="chart-table xs-pt-15">
                                <div class="table-responsive table-minw-fixed table-hiring1">
                                    <table style="width:100%;" id="hiring-gender" class="table table-striped">
                                        <thead class="primary">
                                            <tr>
                                                <th>Store Code</th>
                                                <th>Store Name</th>
                                                <th>Male</th>
                                                <th>Female</th>
                                            </tr>
                                        </thead>
                                        <tbody class="no-border-x">
                                            @if(count($genderwisehiring) > 0 ) @foreach ($genderwisehiring as $gender)
                                            <tr>
                                                <td>{{$gender['io_code']}}</td>
                                                <td>{{$gender['store_name']}}</td>
                                                <td>{!!($gender['male'] > 0)? $gender['male'] : 0 !!}</td>
                                                <td>{!!($gender['female'] > 0)? $gender['female'] : 0 !!}</td>
                                            </tr>
                                            @endforeach @endif
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Total</th>
                                                <th>Male</th>
                                                <th>Female</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var start =moment().startOf('year').add(3,'month');//moment.utc('2015-01-01','YYYY-MM-DD');
    var end = moment().endOf('year').add(3,'month');
    @if(\Request::has('start_date') && \Request::has('end_date'))
     start = moment.utc("{{\Request::get('start_date')}}",'YYYY-MM-DD');
     end = moment.utc("{{\Request::get('end_date')}}",'YYYY-MM-DD');
    @endif
     var range_start ="";
    var range_end ="";
    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if(range_start==""){
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');


        }else{
            range_start = start.format('YYYY-MM-DD');
            range_end = end.format('YYYY-MM-DD');

           
        }

		$(".input_start_date").val(range_start);
		$(".input_end_date").val(range_end);
        

    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            "Current":[start,end],
            "@lang('comman.daterange.lastyear')": [moment().subtract(1, 'year').startOf('year').add(3,'month'), moment().subtract(1, 'year').endOf('year').add(3,'month')]
        }
    }, cb);

     cb(start, end);
    $("#total-gender").DataTable({
		dom: 'Blfrtip',
		buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 250, "scrollX": true,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_male = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_female = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total_male );
            $( api.column( 3 ).footer() ).html( total_female );

        }

    });

	$("#attrition-gender").DataTable({
		dom: 'Blfrtip',
		buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 250, "scrollX": true,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_male = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_female = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total_male );
            $( api.column( 3 ).footer() ).html( total_female );

        }

    });

	$("#hiring-gender").DataTable({
		dom: 'Blfrtip',
		buttons: ['excel'],
        "pageLength": 25,
        "scrollY": 250, "scrollX": true,
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_male = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_female = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total_male );
            $( api.column( 3 ).footer() ).html( total_female );

        }

    });
</script>
@endsection