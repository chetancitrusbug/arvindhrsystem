@extends('regional.layouts.backend')
@section('title',"Attandence Report")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Attandence Report </span>

                        <span class="description"></span>
                        @php
                            $start_date = request()->get('start_date',isset($month)?$month:null);
                            $end_date = request()->get('end_date',isset($year)?$year:null);
                        @endphp
                        <div class="widget-chart-container">
                            <div class="chart-table xs-pt-15 form">
                                {!! Form::open(['class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off',
                                'files'=>false]) !!}
                                <div class="form-group col-sm-5 {{ $errors->has('start_date') ? ' has-error' : ''}}">
                                    {!! Form::label('', '* Start Date: ', ['class' => '']) !!}
                                    <div data-min-view="2" id="start_date_div" data-date-format="dd/mm/yyyy" class="input-group date col-sm-11">
                                        <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>                            {!! Form::text('start_date', old('start_date',$start_date), ['id'=>'start_date','class' => 'form-control input-sm start_date'])
                                        !!} {!! $errors->first('start_date','
                                        <p class="help-block">:message</p>') !!}
                                    </div>

                                </div>
                                <div class="form-group col-sm-5 {{ $errors->has('end_date') ? ' has-error' : ''}}">

                                    {!! Form::label('', '* End Date: ', ['class' => '']) !!}
                                    <div data-min-view="2" id="end_date_div" data-date-format="dd/mm/yyyy" class="input-group date col-sm-11 ">
                                        {!! Form::text('end_date', old('end_date',$end_date), ['id'=>'end_date','class' => 'form-control input-sm end_date']) !!} {!! $errors->first('end_date','
                                        <p class="help-block">:message</p>') !!}
                                        <!-- <input size="16" id="end_date" name="end_date" type="text" value="" class="form-control input-sm input-height-39"> -->
                                        <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>

                                    </div>

                                </div>
                                <div class="form-group col-sm-2">
                                    {!! Form::label('', '&nbsp; ', ['class' => '']) !!}
                                    <div class="col-sm-12">

                                        {{Form::submit('Show Attendance',['class'=>'btn btn-primary'])}}
                                    </div>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($attandance_data)
                <div class="col-md-12">
                    <div class="widget widget-fullwidth">
                        <div class="widget-head">
                            <div class="tools">
                                {!! Form::open(['url'=>url('regional/attendence-csv-export'),'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off', 'files'=>false])
                                !!}
                                         {!! Form::hidden('start_date',old('start_date',$start_date), ['id'=>'start_date','class' => 'form-control input-sm start_date']) !!}
                                        {!! Form::hidden('end_date', old('end_date',$end_date), ['id'=>'end_date','class' => 'form-control input-sm end_date']) !!}
                                        {{Form::submit('Export To Excel',['class'=>'btn btn-primary'])}}

                                {{Form::close()}}

                            </div>
                            <div class="tools"></div>
                            <span class="title">Attandence List </span><span class="description"></span>
                        </div>
                        <div class="widget-chart-container">
                            <div class="chart-table xs-pt-15 table-responsive">
                                <div class="table-responsive tablescroll-xy">
                                    <table class="table table-striped table-hover table-fw-widget">
                                        @php
                                            $j = 0;
                                        @endphp
                                        @foreach ($attandance_data as $attandance)
                                            <tr>
                                                @php
                                                    $i = 1;
                                                @endphp
                                                @foreach ($attandance as $item)
                                                    @if ($i ==  1 && $j < 3)
                                                        <th colspan="7">{{$item}}</th>
                                                    @else
                                                        <td>{{$item}}</td>
                                                    @endif
                                                    @php
                                                        $i++;
                                                    @endphp
                                                @endforeach
                                            </tr>
                                            @php
                                                $j++;
                                            @endphp
                                        @endforeach
                                    </table>
                                </div>     
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$("#start_date_div").datetimepicker({
     format:'dd/mm/yyyy',
     endDate: new Date(),
     maxDate: 'today',
     autoclose: true,
     componentIcon: '.mdi.mdi-calendar'
});
$("#end_date_div").datetimepicker({
    endDate: new Date(),
    maxDate: 'today',
    format:'dd/mm/yyyy',
    autoclose:true,
    componentIcon: '.mdi.mdi-calendar'
});
/*
$("#export_to_excel").click(function name(params) {

})

function download_excel() {
    var start_date = $("#start_date").val();
    var end_date = $("#end_date").val();
    var url = "{{url('regional/attendence-csv-export')}}";
    if(start_date != '' && end_date != ''){
        $.ajax({
            url: url,
            type: 'POST',
            // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
            data:{ _method: 'POST', start_date : start_date, end_date : end_date, _token: '{{ csrf_token()}}'},
            success: function(data){

             },
            error: function(errors){
                alert("Something Went Wrong");
                console.log(errors);
                $.each(errors, function(index, value) {

                });
            }
        });
    } else {
        alert("Start date and end date required");
    }
} */
</script>
@endsection