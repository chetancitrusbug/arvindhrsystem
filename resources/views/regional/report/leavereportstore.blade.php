@extends('regional.layouts.backend')
@section('title',"Leave Reports")
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Leave Reports Store
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-primary">Back</button>
                        </a>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table style="width:100%;" id="leave-report" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Store Code</th>
                                        <th>Store Name</th>
                                        <th>Allocated Leave</th>
                                        <th>Pending Leave</th>
                                        <th>Availed Leaves</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Total</th>
                                        <th>Allocated Leave</th>
                                        <th>Pending Leave</th>
                                        <th>Availed Leaves</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('regional/leave-report-store-data/') }}/{{Auth::user()->region}}";
        var auth_check = "{{ Auth::check() }}";
        var edit_url = "{{ url('regional/leave-report-employee') }}"

        datatable = $('#leave-report').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'io_code',name : 'io_code',"searchable": true, "orderable": false, "width":200},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false, "width":150},
                { data: 'total_allocated_leave',name : 'total_allocated_leave',"searchable": true, "orderable": false, "width":150},
                { data: 'total_pending_leave',name : 'total_pending_leave',"searchable": false, "orderable": false, "width":150},
                { data: 'availed_leave',name : 'availed_leave',"searchable": true, "orderable": false, "width":100},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v="";  var d="";
                            v = "<a href='"+edit_url+"/"+o.store_id+"' value="+o.store_id+" data-id="+o.store_id+"><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";
                return v; } }

            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total_allocated = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_pending = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
            total_availed = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total_allocated );
            $( api.column( 3 ).footer() ).html( total_pending );
            $( api.column( 4 ).footer() ).html( total_availed );



        }
        });

</script>
@endsection