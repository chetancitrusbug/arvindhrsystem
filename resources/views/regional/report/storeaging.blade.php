@extends('regional.layouts.backend')
@section('title',"Attrition Report")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Attrition Report
                        <a href="{!! URL::previous() !!}" class="pull-right" title="Back">
                            <button class="btn btn-space btn-primary">Back</button>
                       </a>
                    </div>
                    <div class="panel-body">
                        <table style="width:100%;" id="store_aging" class="table table-striped">
                            <thead class="primary">
                                <tr>
                                    <th>Store Code</th>
                                    <th>Store Name</th>
                                    <th>Total</th>
                                    <th>Infant attrition Within month</th>
                                    <th>Baby attrition Within a quarter</th>
                                    <th>Attrition</th>
                                </tr>
                            </thead>
                            <tfoot class="primary">
                                <tr>
                                    <th></th>
                                    <th>Total</th>
                                    <th>Total</th>
                                    <th>Infant attrition Within month</th>
                                    <th>Baby attrition Within a quarter</th>
                                    <th>Attrition</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('/regional/store-aging-report-data') }}/{{Auth::user()->region}}";
        var view_url = "{{ url('/regional/attrition-report/store/') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#store_aging').DataTable({
            dom: 'Blfrtip',  buttons: ['excel'],
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'io_code',name : 'io_code',"searchable": true, "orderable": false, "width":200},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false, "width":200},
                { data: 'total',name : 'total',"searchable": true, "orderable": false, "width":200},
                { data: 'infant_attrition',name : 'infant_attrition',"searchable": true, "orderable": false, "width":150},
                { data: 'baby_attrition',name : 'baby_attrition',"searchable": true, "orderable": false, "width":150},
                { data: 'attrition',name : 'attrition',"searchable": true, "orderable": false, "width":100},

            /*    {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var v="";
                     /*   v = "<a href='"+view_url+"/"+o.region_id+"' value="+o.region_id+" data-id="+o.region_id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        return v;
                    }

                } */
            ],
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_infant = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_baby = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            total_attrition = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );


            $( api.column( 2 ).footer() ).html( total );
            $( api.column( 3 ).footer() ).html( total_infant );
            $( api.column( 4 ).footer() ).html( total_baby );
            $( api.column( 5 ).footer() ).html( total_attrition );




        }
        });

</script>
@endsection