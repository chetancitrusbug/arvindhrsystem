@extends('regional.layouts.backend')

@section('content')
    <style>.clickable-row{cursor:pointer}</style>
	<div class="be-content">
	    <div class="main-content container-fluid">
          <div class="row">

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Attandence Report </span><span class="description"></span>

                        <div class="widget-chart-container clearfix">
                            <div class="chart-table xs-pt-15 form">
                                {!! Form::open(['url'=>url('regional/attendence-list'), 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off', 'files'=>false])
                                !!}
                                <div class="form-group col-sm-5 {{ $errors->has('start_date') ? ' has-error' : ''}}">
                                    {!! Form::label('', '* Start Date: ', ['class' => '']) !!}
                                    <div data-min-view="2" id="start_date_div" data-date-format="dd/mm/yyyy" class="input-group date col-sm-11">
                                        <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                        {!! Form::text('start_date', old('start_date',''), ['class' => 'form-control input-sm start_date']) !!}
                                        {!! $errors->first('start_date','<p class="help-block">:message</p>') !!}
                                    </div>

                                </div>
                                <div class="form-group col-sm-5 {{ $errors->has('end_date') ? ' has-error' : ''}}">

                                    {!! Form::label('', '* End Date: ', ['class' => '']) !!}
                                    <div data-min-view="2" id="end_date_div" data-date-format="dd/mm/yyyy" class="input-group date col-sm-11 ">
                                        {!! Form::text('end_date', old('end_date',''), ['class' => 'form-control input-sm end_date']) !!}
                                        {!! $errors->first('end_date','<p class="help-block">:message</p>') !!}
                                       <!-- <input size="16" id="end_date" name="end_date" type="text" value="" class="form-control input-sm input-height-39"> -->
                                        <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>

                                    </div>

                                </div>
                                <div class="form-group col-sm-2">
                                    {!! Form::label('', '&nbsp; ', ['class' => '']) !!}
                                    <div class="col-sm-12">

                                        {{Form::submit('Show Attendance',['class'=>'btn btn-primary'])}}
                                    </div>
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
				<div class="widget widget-fullwidth">
				  <div class="widget-head">
					<div class="tools"></div><span class="title">Manpower Report - Region <small>({{$region->name}})</small></span><span class="description"></span>
				  </div>
				  <div class="widget-chart-container">
					<div class="chart-table xs-pt-15">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="primary">
                                <tr>
                                    <th>Region</th>
                                    <th>Budgeted</th>
                                    <th class="number">Available</th>
                                    <th class="number">GAP</th>
                                    <th class="number">Avg GAP Aging</th>
                                    <th class="number">% GAP</th>
                                </tr>
                                </thead>
                                <tbody class="no-border-x">
                                @php
                                if(count($manPowerGet) > 0){
                                $manPower = array();
                                    foreach($manPowerGet as $key => $value){
                                        $manPower['region_name'][$key] =$value->name;
                                        $manPower['total_actual_man_power'][$key] =$value->total_actual_man_power;
                                        $manPower['gap'][$key] =$value->total_budget_man_power - $value->total_actual_man_power;

                                @endphp
                                <tr class="clickable-row"  data-href="{{ url('regional/manpower-report/store/') }}">
                                    <td>{{$value->name}}</td>
                                    <td>{{($value->total_budget_man_power > 0)? $value->total_budget_man_power : 0 }}</td>
                                    <td class="number">{{($value->total_actual_man_power > 0)? $value->total_actual_man_power : 0 }}</td>
                                    <td class="number">{{($value->total_budget_man_power > 0 && $value->total_actual_man_power > 0)? $value->total_budget_man_power - $value->total_actual_man_power : 0 }}</td>
                                    <td class="number">0</td>
                                    <td class="number">{{($value->total_budget_man_power > 0 && $value->total_actual_man_power > 0)? round(($value->total_budget_man_power - $value->total_actual_man_power)/$value->total_budget_man_power,2) : 0 }}</td>
                                </tr>
                                @php
                                }
                                }else{
                                    echo '<tr><td colspan=7>Data not Available</td></tr>';
                                }
                                @endphp
                                </tbody>
                            </table>
                        </div>
					</div>
				  </div>
				</div>
            </div>
			<div class="col-md-12">
				<div class="widget widget-fullwidth">
				  <div class="widget-head">
					<div class="tools"></div><span class="title">Hygiene Report - Region<small>({{$region->name}})</small></span><span class="description"></span>
				  </div>
				  <div class="widget-chart-container">
					<div class="chart-table xs-pt-15">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="primary">
                                <tr>
                                    <th>Region</th>
                                    <th>Id Card Not Available</th>
                                    <th>Uniform not available</th>
                                    <th>L0L1 untrained</th>
                                    <th>L0L1 non certified</th>
                                    <th>Certificate pending</th>
                                    <th>Appointment Letter</th>
                                </tr>
                                </thead>
                                <tbody class="no-border-x">
                                <tr class="clickable-row"  data-href="{{ url('regional/hygiene-report/store/') }}">
                                    <td>{{$region->name}}</td>
                                    <td>{{($hygiene['id_card'] > 0 )? $hygiene['id_card'] : 0 }}</td>
                                    <td>{{($hygiene['uniform'] > 0 )? $hygiene['uniform'] : 0 }}</td>
                                    <td>{{($hygiene['l0l1_untrained'] > 0 )? $hygiene['l0l1_untrained'] : 0 }}</td>
                                    <td>{{($hygiene['l0l1_certified'] > 0 )? $hygiene['l0l1_certified'] : 0 }}</td>
                                    <td>{{($hygiene['certificate_pending'] > 0 )? $hygiene['certificate_pending'] : 0 }}</td>
                                    <td>{{($hygiene['appoint_letter'] > 0 )? $hygiene['appoint_letter'] : 0 }}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
					</div>
				  </div>
				</div>
            </div>
			<div class="col-md-12">
				<div class="widget widget-fullwidth">
				  <div class="widget-head">
					<div class="tools"></div><span class="title">Man Power Cost Report - Region<small>({{$region->name}})</small></span><span class="description"></span>
				  </div>
				  <div class="widget-chart-container">
					<div class="chart-table xs-pt-15">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="primary">
                                <tr>
                                    <th>Region</th>
                                    <th>Employee</th>
                                    <th>Avg Cost</th>
                                </tr>
                                </thead>
                                <tbody class="no-border-x">
                                <tr class="clickable-row"  data-href="{{ url('regional/manpowercost-report/store/') }}">
                                    <td>{{$region->name}}</td>
                                    <td>{{($averageCost['total_employee'] > 0 )? $averageCost['total_employee'] : 0 }}</td>
                                    <td>{{($averageCost['avg_cost'] > 0 )? round($averageCost['avg_cost'],2) : 0 }}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
					</div>
				  </div>
				</div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div>
                        <span class="title">Hiring Report - Region</span>
                        <span
                            class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <div class="table-responsive">
                                <table data-href="{{ url('regional/hiring-report-store') }}" class="table table-striped clickable-row">
                                    <thead class="primary">
                                        <tr>
                                            <th>GAP</th>
                                            <th>Inprocess</th>
                                            <th>Hired</th>
                                            <th>Pending</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if ($hiringReport)
                                            <tr>
                                                <td>{{($hiringReport->gap_total > 0) ? $hiringReport->gap_total : 0 }}</td>
                                                <td>{{($total_pending_employee->total_pending > 0) ? $total_pending_employee->total_pending : 0 }}</td>
                                                <td>{{($hiringReport->hired_total > 0) ? $hiringReport->hired_total : 0 }}</td>
                                                <td>{{($hiringReport->pending_total > 0) ? $hiringReport->pending_total : 0 }}</td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td>No Data Found</td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div>
                        <span class="title" >Additional Reports - Region</span>
                        <span class="description"></span>
                    </div>
                    <div class="widget-chart-container clickable-row" data-href="{{ url('regional/additional-report/store') }}">
                        <div class="chart-table xs-pt-15 row">
                            <div class="col-sm-6">
                                <div class="widget-head">
                                    <div class="tools"></div>
                                    <span class="title">Gender wise manpower</span>
                                    <span class="description"></span>
                                </div>
                                <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Total</th>
                                            <th>Male</th>
                                            <th>Female</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if (count($genderwisetotalemployee) > 0)
                                        <tr>
                                            <td>{{($genderwisetotalemployee['total'] > 0) ? $genderwisetotalemployee['total']  : 0 }}</td>
                                            <td>{{($genderwisetotalemployee['male']  > 0) ? $genderwisetotalemployee['male']  : 0 }}</td>
                                            <td>{{($genderwisetotalemployee['female']  > 0) ? $genderwisetotalemployee['female']  : 0 }}</td>
                                        </tr>
                                        @else
                                        <tr>
                                            <td>No Data Found</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="widget-head">
                                    <div class="tools"></div>
                                    <span class="title">Gender wise hiring</span>
                                    <span class="description"></span>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead class="primary">
                                            <tr>
                                                <th>Total</th>
                                                <th>Male</th>
                                                <th>Female</th>
                                            </tr>
                                        </thead>
                                        <tbody class="no-border-x">
                                            @if (count($hiring_employee) > 0)
                                                <tr>
                                                    <td>{{($hiring_employee['total'] > 0) ? $hiring_employee['total'] : 0 }}</td>
                                                    <td>{{($hiring_employee['male'] > 0) ? $hiring_employee['male'] : 0 }}</td>
                                                    <td>{{($hiring_employee['female'] > 0) ? $hiring_employee['female'] : 0 }}</td>
                                                </tr>
                                            @else
                                            <tr>
                                                <td>No Data Found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="widget-head">
                                    <div class="tools"></div>
                                    <span class="title">Gender wise attrition</span>
                                    <span class="description"></span>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead class="primary">
                                            <tr>
                                                <th rowspan="2">Gender</th>
                                                <th>Male</th>
                                                <th>Female</th>
                                            </tr>
                                        </thead>
                                        <tbody class="no-border-x">
                                            @if (count($genderwiseattrition) > 0)
                                            <tr>
                                                <tr>
                                                    <td></td>
                                                    <td>{{($genderwiseattrition['male'] > 0) ? $genderwiseattrition['male'] : 0 }}</td>
                                                    <td>{{($genderwiseattrition['female'] > 0) ? $genderwiseattrition['female'] : 0 }}</td>
                                                </tr>
                                            </tr>
                                            @else
                                            <tr>
                                                <td>No Data Found</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools">
                        </div>
                        <span class="title">Attrition Report - Region (MTD)</span>
                        <span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Total</th>
                                            <th>Resigned</th>
                                            <th>Absconding</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if ($totalattritionReport)
                                            @foreach ($totalattritionReport as $attritionReport)
                                                <tr class="clickable-row" data-href="{{url('regional/store-attrition-report/')}}">
                                                    <td>{{($attritionReport->total > 0) ? $attritionReport->total : 0 }}</td>
                                                    <td>{{($attritionReport->resignation_total > 0) ? $attritionReport->resignation_total : 0 }}</td>
                                                    <td>{{($attritionReport->absconding_total > 0) ? $attritionReport->absconding_total : 0 }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools">

                        </div><span class="title">Attrition Aging Report</span>
                        <span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead class="primary">
                                        <tr>
                                            <th>Total</th>
                                            <th>Infant attrition Within month</th>
                                            <th>Baby attrition Within a quarter</th>
                                            <th>Attrition</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        @if ($aging_report)
                                        <tr class="clickable-row" data-href="{{url('regional/store-aging-report/')}}">
                                            <td>{{$aging_report['total']}}</td>
                                            <td>{{($aging_report['infant_attrition']) ? $aging_report['infant_attrition']: 0 }}</td>
                                            <td>{{($aging_report['baby_attrition'] > 0) ? $aging_report['baby_attrition'] : 0 }}</td>
                                            <td>{{($aging_report['attrition'] > 0) ? $aging_report['attrition'] : 0 }}</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools">
                        </div>
                        <span class="title">Attrition report - By percetage of live manpower</span>
                        <span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <div class="table-responsive">
                                <table id="man-powerpercentage-report-table" class="table table-striped table-hover table-fw-widget">
                                    <thead class="primary">
                                        <tr>
                                            <th>Total Man Power</th>
                                            <th>Total Attrition</th>
                                            <th>Percentage</th>
                                        </tr>
                                    </thead>
                                    @if ($manpowerPercentageReport->total_actual_man_power)
                                        <tbody>
                                        <tr class="clickable-row" data-href="{{url('regional/store-manpower-report/')}}">
                                                <td>{{($manpowerPercentageReport->total_actual_man_power)?$manpowerPercentageReport->total_actual_man_power:0}}</td>
                                                <td>{{($manpowerPercentageReport->Total)?$manpowerPercentageReport->Total:0}}</td>
                                                <td>
                                                    @if ($manpowerPercentageReport->total_actual_man_power > 0 && $manpowerPercentageReport->Total > 0)
                                                        @php
                                                            echo round(($manpowerPercentageReport->total_actual_man_power/$manpowerPercentageReport->Total)*100,2);
                                                        @endphp %
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    @else
                                    <tr>
                                        <td>
                                            No Data Found
                                        </td>
                                    </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Attrition Report - By Reason of Attrition</span>
                        <span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            @if ($reasonreportdata)
                                @php
                                    $reasons = $reasonreportdata['reasons'];
                                    $report_data = $reasonreportdata['report_data'];
                                @endphp
                            @endif
                            <div class="table-responsive">
                            <table id="attrition-region-report" class="table table-striped table-hover table-fw-widget">
                                <thead class="primary">
                                    <tr>
                                        @if ($reasons) @foreach ($reasons as $reason)
                                        <th>{{$reason->reason}}</th>
                                        @endforeach @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (count($report_data) > 0)
                                        <tr class="clickable-row" data-href="{{ url('regional/store-reason-report') }}">
                                            @foreach ($reasons as $reason)
                                            <td>{{$report_data[$reason->id]}}</td>
                                            @endforeach
                                        </tr>
                                    @else
                                    <tr>
                                        <td>
                                            No Data Found
                                        </td>
                                    </tr>
                                    @endif

                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Hiring - Inprocess Report</span>
                        <span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            <div class="table-responsive">
                            <table id="attrition-region-report" class="table table-striped table-hover table-fw-widget">
                                <thead class="primary">
                                    <tr>
                                        <th>Pending at Store</th>
                                        <th>Pending at RHR</th>
                                        <th>Peding at HRMS</th>
                                    </tr>
                                </thead>
                                @if (count($hiringinprocessreport) > 0)
                                    <tbody>
                                        <tr class="clickable-row" data-href="{{ url('regional/hiring-inprocess/store/') }}">
                                            <td>{{$hiringinprocessreport['store_pending']}}</td>
                                            <td>{{$hiringinprocessreport['region_pending']}}</td>
                                            <td>{{$hiringinprocessreport['hrms_pending']}}</td>
                                        </tr>
                                    </tbody>
                                @else
                                    <tr>
                                        <td>
                                            No Data Found
                                        </td>
                                    </tr>
                                @endif
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">

                        <div class="tools"></div><span class="title">Productivity report by manpower</span>
                        <span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            @if ($reasonreportdata) @endif
                            <div class="table-responsive">
                            <table id="attrition-region-report" class="table table-striped table-hover table-fw-widget">
                                <thead class="primary">
                                    <tr>
                                        <th>Sales</th>
                                        <th>No. of employee</th>
                                        <th>Sales per employee</th>
                                    </tr>
                                </thead>
                                @if (count($productivity_report) > 0)
                                <tbody>
                                    <tr class="clickable-row" data-href="{{ url('regional/productivity-report-store/') }}">
                                        <td>{{$productivity_report['total_collection']}}</td>
                                        <td>{{$productivity_report['total_employee']}}</td>
                                        <td>{{$productivity_report['per_person_income']}}</td>
                                    </tr>
                                </tbody>
                                @else
                                <tr>
                                    <td>
                                        No Data Found
                                    </td>
                                </tr>
                                @endif
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Leave Report</span>
                        <span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            @if ($reasonreportdata) @endif
                            <div class="table-responsive">
                            <table id="attrition-region-report" class="table table-striped table-hover table-fw-widget">
                                <thead class="primary">
                                    <tr>
                                        <th>Allocated Leave</th>
                                        <th>Pending Leave</th>
                                        <th>Alived Leave</th>
                                    </tr>
                                </thead>
                                @if($leaveReport)
                                <tbody>
                                    <tr class="clickable-row" data-href="{{ url('regional/leave-report-store/') }}">
                                        <td>{{$leaveReport->total_allocated_leave}}</td>
                                        <td>{{$leaveReport->total_pending_leave}}</td>
                                        <td>{{$leaveReport->availed_leave}}</td>
                                    </tr>
                                </tbody>
                                @else
                                    <tr>
                                        <td>
                                            No Data Found
                                        </td>
                                    </tr>
                                @endif
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="widget widget-fullwidth">
                    <div class="widget-head">
                        <div class="tools"></div><span class="title">Attendance Report ({{\Carbon\Carbon::now()->startOfDay()->subDays(1)->format("d/m/Y")}})</span>
                        <span class="description"></span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="chart-table xs-pt-15">
                            @if ($getAttendanceReportData) @endif
                            <div class="table-responsive">
                            <table id="attrition-region-report" class="table table-striped table-hover table-fw-widget">
                                <thead class="primary">
                                    <tr>
                                        <th>Live Manpower</th>
                                        <th>Present Manpower</th>
                                        <th>Present Manpower (%)</th>
                                    </tr>
                                </thead>
                                @if(count($getAttendanceReportData) > 0)
                                <tbody>
                                    <tr class="clickable-row" data-href="{{url('regional/attendance-report/store')}}">
                                        <td>{{$getAttendanceReportData['actual_man_power']}}</td>
                                        <td>{{$getAttendanceReportData['present_man_power']}}</td>
                                        <td>{{$getAttendanceReportData['present_per']}}</td>
                                    </tr>
                                </tbody>
                                @else
                                <tr>
                                    <td>
                                        No Data Found
                                    </td>
                                </tr>
                                @endif
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



          </div>
        </div>
	</div>

@endsection

@section('scripts')
      <?php if(Session::has('flash_success') &&  Session::get('flash_success') !='' ){
      ?>
      <script>

        $(document).ready(function(){
          $('document').find('#success').trigger('click');
          $('#success').click(function(){
            alert('here');
          })
        });
      </script>
    <?php } ?>
	<script>
		$(".clickable-row").click(function() {
			window.location = $(this).data("href");
		});
        $("#start_date_div").datetimepicker({
            format:'dd/mm/yyyy',
            endDate: new Date(),
            maxDate: 'today',
            autoclose: true,
            componentIcon: '.mdi.mdi-calendar'
        });
        $("#end_date_div").datetimepicker({
            endDate: new Date(),
            maxDate: 'today',
            format:'dd/mm/yyyy',
            autoclose: true,
            componentIcon: '.mdi.mdi-calendar'
        });
	</script>
@endsection

@push('modal')
  @include('regional.modals.backendSucess')
@endpush
