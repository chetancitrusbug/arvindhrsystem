@extends('regional.layouts.backend')
@section('title',"Attendance")
@section('css')
    <style>
        .paging { padding-right: 20px; padding: 0px 10px !important; }
        /* #attendance-table { width:3000px;} */
        .select{
            padding: 6px 2px;
            height: 30px;
            font-size: 12px;
            width: 70px;
        }
        .table-responsive tbody > tr > td {
            width: 100px;
        }
        .table-responsive tbody > tr > td:first-child{
            width: 200px;
        }
        .request-change{
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.attendance_request')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Attendance Change Request
                        <span class="panel-subtitle">
                        </span>
                    </div>

                    <div class="panel-body clearfix">
                        <div class="table-responsive" >
                            <table id="attendance-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th width="15%">Full Name</th>
                                        <th width="15%">Date</th>
                                        <th width="15%">Store Name</th>
                                        <th width="10%">Attendance</th>
                                        <th>Note</th>
                                        <th width="10%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($attendance))
                                        {!! Form::open(['url' => '/store/attendance', 'class' => '','id' => 'module_form','autocomplete'=>'off','files'=>true])
                                        !!}
                                            @foreach ($attendance as $key => $value)
                                                <tr>
                                                    <td>{!! $value->full_name !!}</td>
                                                    <td>{!! date('d/m/Y',strtotime($value->date)) !!}</td>
                                                    <td>{!! $value->store_name !!}</td>
                                                    <td>
                                                        {!! Form::select("attendance", config('constants.attendance'), $value->attendance ,['class' => 'form-control select att'.$key]) !!}
                                                    </td>
                                                    <td>{!! $value->note !!}</td>
                                                    <td>
                                                        <a href='javascript:void(0);' data-id="{{$value->id}}" data-key="{{$key}}" data-empid="{{$value->employee_id}}" data-attid="{{$value->attendance_id}}" data-attendance="{{$value->attendance}}" data-status="approve" class='btn btn-success btn-sm approve' title="Approve"><i class='mdi mdi-check' ></i></a>

                                                        <a href='javascript:void(0);' data-id="{{$value->id}}" data-key="{{$key}}" data-empid="{{$value->employee_id}}" data-attid="{{$value->attendance_id}}" data-attendance="{{$value->attendance}}" data-status="reject" class='btn btn-danger btn-sm approve' title="Reject">x</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        {!! Form::close() !!}
                                    @else
                                        <tr>
                                            <td colspan="6">There are no data</td>
                                        </tr>
                                    @endif
                                    <tr align="right">
                                        {{-- <td colspan="2" class="paging">{{ $attendance->links() }}</td> --}}
                                    </tr>
                                <tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var att = status = id = attendance = attendance_id = employee_id = key = null;

        $('.approve').on('click',function(){
            $("#loading").show();
            id = $(this).data('id');
            attendance_id = $(this).data('attid');
            employee_id = $(this).data('empid');
            key = $(this).data('key');
            attendance = $(this).data('attendance');
            status = $(this).data('status');
            att = $(".att"+key).val();

            if(status == 'reject'){
                $('.confirmation-title').html('Are sure to reject request?');
                $('.att-confirm').html('Reject');
            }
            $('#attendanceConfirmation').niftyModal();
        });

        $('.att-confirm').on('click',function(){

            $('button[type="submit"]').attr('disabled','disabled');

            $.ajax({
                type: 'POST',
                url: "{{url('/regional/attendance-action')}}",
                data: {
                    id:id,
                    attendance_id:attendance_id,
                    employee_id:employee_id,
                    attendance:att,
                    status:status,
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data) {
                $('button[type="submit"]').removeAttr('disabled');
                $('.success-text').html(data);
                $('#attendanceConfirmation').niftyModal('hide');
                $('#success').niftyModal();

                setTimeout(function () {
                    location.reload();
                }, 2000);
            })
            .fail(function(data) {
                $('button[type="submit"]').removeAttr('disabled');
                jQuery.each(data.responseJSON.errors,function(index,val){
                    $('.'+index+'_error').html(val);
                });
                $('.error').removeClass('hide');
            });
        });

        $('.att-confirm-cancel, .change-close').on('click',function(){
            $('#attendanceConfirmation').niftyModal('hide');
            $('#change-att-model').niftyModal('hide');
            $("#loading").hide();
        });
    </script>
@endsection