@extends('regional.layouts.backend')
@section('title',"Employee")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.confirmation_index')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Employee List</div>
                    <div class="panel-body">
                        <table id="employee-table" class="table table-striped table-hover table-fw-widget">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <!-- <th>Email</th>
                                    <th>Birth Date</th> -->
                                    <th>HRMS Status</th>
									<th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        var url ="{{ url('/regional/employee-data') }}";
        var edit_url = "{{ url('/regional/employee') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                /*{ data: 'email',name : 'email',"searchable": true, "orderable": false},
                { data: 'date_of_birth',name : 'date_of_birth',"searchable": true, "orderable": false},*/
                {
                    "data": null,
                    "name" : 'hrms_status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
						if(o.regional_status == 2 && o.hrms_status == 1){
							return '<label class="label label-primary">Request Sent<label>';
						}else if(o.hrms_status == 1 || o.hrms_status == 0){
                            return '<label class="label label-danger">Pending<label>';
                        }else if(o.hrms_status == 2){
                            return '<label class="label label-success">Approve<label>';
                        }else if(o.hrms_status == 3){
                            return '<label class="label label-warning">send back<label>';
                        }
                    }
                },
				{
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
                        if(o.status == 'pending' || o.status == 'approve'){
                            return '<label class="label label-danger">Pending<label>';
                        }else if( o.status == 'confirm'){
                            return '<label class="label label-success">Approve<label>';
                        }
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":160,
                    "render": function (o) {
                        var e = v = d = a = b ="";

                        if((o.store_status == 1 && o.regional_status != 2) || o.hrms_status == 3){
                            a = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/approve' value="+o.id+" data-id="+o.id+" data-title='regional' class='btn btn-success btn-sm send_to_hr_from_index' title='Approve'><i class='mdi mdi-redo'></i></a>&nbsp;";

                            b = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/send-back' value="+o.id+" data-note='"+o.regional_note+"' data-title='Store Manager' data-id="+o.id+" class='btn btn-danger btn-sm regional_send_back' title='Send back'><i class='mdi mdi-undo' ></i></a>&nbsp;";
                        }

                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='employee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('regional/employee')}} data-msg='employee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";

                        l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-success btn-sm' title='View' ><i class='mdi mdi-menu' aria-hidden='true'></i></button></a>&nbsp;";
                        return v+e+b+a+l;
                    }

                }
            ]
        });
</script>


@endsection