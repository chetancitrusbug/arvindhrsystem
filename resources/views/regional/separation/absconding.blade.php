@extends('regional.layouts.backend')
@section('title',"Absconding")
@section('css')
    <style>
        .paging { padding-right: 20px; padding: 0px 10px !important; }
        #attendance-table { width:3000px;}
        .select{
            padding: 6px 2px;
            height: 30px;
            font-size: 12px;
            width: 55px;
        }
        .table-responsive tbody > tr > td {
            width: 100px;
        }
        .table-responsive tbody > tr > td:first-child{
            width: 200px;
        }
        .request-change{
            cursor: pointer;
        }
        #abs-table{
            width: 95%; margin: 0 auto;
            font-size: 10px;
        }
        .error{
            color: red;
        }
    </style>
@endsection
@section('content')
<div class="be-content">
{{--@include('regional.includes.alerts')
@include('regional.modals.absconding')--}}
@include('regional.modals.absconding')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Absconding List
                        <!--<span class="panel-subtitle">
                            <a href="#" title="Add">
                                <button class="btn btn-space btn-success add_absconding">Add</button>
                            </a>
                        </span> -->
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table style="width:100%;" id="absconding-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Employee Name</th>
                                        <th>Employee Code</th>
                                        <th>Store code</th>
                                        <th>Store name</th>
                                        <th>Date of absconding</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script>

        $("#pdo_date").datetimepicker({
            autoclose: true,
            minView: 2,
            format: 'yyyy-mm-dd',
            maxDate: new Date(),
            endDate: new Date(),
        });

        var url ="{{ url('/regional/absconding-data') }}";
        var approve_url = "{{ url('/regional/approve-absconding') }}";
        var reject_url = "{{ url('/regional/reject-absconding') }}";
		var download_url = "{{url('/regional/download-letter')}}"
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#absconding-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                // statusChange();
            },
            columns: [
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
                { data: 'location_code',name : 'location_code',"searchable": true, "orderable": false},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},
				{
					data: null,
					"searchable": false,
					"orderable": false,
					"render":function (o){
						var date =  new Date(o.absconding_time)
						return   date.getDate() + '-'+(date.getMonth() + 1) + '-' + date.getFullYear();
					},

				},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var a=""; var r=""; var d=r=l=u=pdo= "";
						if(o.rm_absconding_status == 0){
							a = "<a data-url='"+approve_url+"/"+o.id+"' data-id="+o.id+" class='approve-absconding btn btn-success btn-sm' title='Approve absconding request'>Approve</a>&nbsp;";

							r = "<a data-url='"+reject_url+"/"+o.id+"' data-id="+o.id+" class='reject-absconding btn btn-danger btn-sm' title='Reject absconding request'>Reject</a>&nbsp;";
						}else{
							a = "<a href='"+download_url+"/"+o.id+"' target='_blank' data-id="+o.id+" class='btn btn-success btn-sm' title='Download Letter '>Download Letter </a>&nbsp;";
                            if(o.pdo_id == '' || o.pdo_id == null){
                                pdo = "<a data-id="+o.id+" class='add_pdo btn btn-primary btn-sm' title='Add PDO Detail'>Add PDO</a>&nbsp;";
                            } else {
                                pdo = "<a  data-id="+o.pdo_id+" class='view_pdo btn btn-primary btn-sm' title='View PDO Detail'>View PDO</a>&nbsp;";
                            }

						}
                        return a+r+pdo;
                    }

                }
            ]
        });


        $(document).on('click','.view_pdo',function(){

           var pdo_id = $(this).data('id');
           // $("#loading").show();
            $.ajax({
                type: 'GET',
                url: "{{url('regional/getpdodetail')}}/"+pdo_id,
            })
            .done(function(data) {

                if(data.status){
                    var pdo_data = data.data.pdo;
                    data = data.data.employee;

                    $('.modal_pdo_date').html(pdo_data.pdo_date);
                    $('.modal_pdo_number').html(pdo_data.pdo_number);
                    $('.modal_courier_name').html(pdo_data.courier_name);

                    $('#abscounding_employee_id').val(id);
                    $('.emp_code').html(data.employee_code);
                    $('.io_code').html(data.store_location.io_code);
                    $('.emp_name').html(data.full_name);
                    $('.store_name').html(data.store_location.store_name);
                    $('.designation').html(data.designation.name);
                    $('.mobile').html(data.mobile);
                    $('.doj').html(data.joining_date);
                    $('.email').html(data.email);
                    $('.name').html(data.full_name);
                    $('.ac_no').html(data.account_no);
                    $('.bank_name').html(data.bank_name);
                    $('.ifsc_code').html(data.ifsc);
                    $('.address').html(data.permanent_address_line_1+', '+data.permanent_address_line_2+', '+data.permanent_address_line_3);
                    $('.submit_pdo_form').show();
					$("#loading").hide();
					$('#view_pdo_modal').niftyModal();

                }else if(data.status == '400'){
                    $('.error').html(data.msg);
                    $('.submit_pdo_form').hide();
                    $('#view_pdo_modal').niftyModal();
                }

            })
            .fail(function(data) {
                $('.submit_pdo_form').hide();
            });

        });
        $(document).on('click','.add_pdo',function(){

           var id = $(this).data('id');
            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: "{{url('regional/getemployeedetail')}}",
                data: {id:id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data) {
                if(data.status == '200'){
                    data = data.data;
                    $('#abscounding_employee_id').val(id);
                    $('.emp_code').html(data.employee_code);
                    $('.io_code').html(data.store_location.io_code);
                    $('.emp_name').html(data.full_name);
                    $('.store_name').html(data.store_location.store_name);
                    $('.designation').html(data.designation.name);
                    $('.mobile').html(data.mobile);
                    $('.doj').html(data.joining_date);
                    $('.email').html(data.email);
                    $('.name').html(data.full_name);
                    $('.ac_no').html(data.account_no);
                    $('.bank_name').html(data.bank_name);
                    $('.ifsc_code').html(data.ifsc);
                    $('.address').html(data.permanent_address_line_1+', '+data.permanent_address_line_2+', '+data.permanent_address_line_3);
                    $('.submit_pdo_form').show();
					$("#loading").hide();


                }else if(data.status == '400'){
                    $('.error').html(data.msg);
                    $('.submit_pdo_form').hide();
                }

            })
            .fail(function(data) {
                $('.submit_pdo_form').hide();

            });
            $('#add_pdo_modal').niftyModal();
        });
        $(document).on('click','.approve-absconding',function(){
            url = $(this).data('url');

            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: url,
                data: {employee_id:$('select[name="employee_id"]').val()},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data) {
                if(data.status == '200'){
                    location.reload();
                }else if(data.status == '400'){
                    $('.error').html(data.msg);
                }
            })
            .fail(function(data) {
                $('button[type="submit"]').removeAttr('disabled');
                /*jQuery.each(data.responseJSON.errors,function(index,val){
                    $('.'+index+'_error').html(val);
                });*/
            });
        });

        $(document).on('click','.reject-absconding',function(){
            url = $(this).data('url');

            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: url,
                data: {employee_id:$('select[name="employee_id"]').val()},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            .done(function(data) {
                if(data.status == '200'){
                    location.reload();
                }else if(data.status == '400'){
                    $('.error').html(data.msg);
                }
            })
            .fail(function(data) {
                $('button[type="submit"]').removeAttr('disabled');
                /*jQuery.each(data.responseJSON.errors,function(index,val){
                    $('.'+index+'_error').html(val);
                });*/
            });
        });
    </script>
@endsection