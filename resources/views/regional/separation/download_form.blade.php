<?php /* echo date('d.m.y')?>



<br/>
{{$employee->employee_code}} <br/>

{{$employee->full_name}}<br/>

{{$employee->permanent_address_line_1}},{{$employee->permanent_address_line_2}},{{$employee->permanent_address_line_3}},{{$employee->permanent_city}},{{$employee->permanent_state }},{{$employee->permanent_pincode}}.
	<br/>
                                                           Sub: Voluntary Abandonment of Services
<br/>
Dear {{$employee->name}}
<br/>
It has been noted that you have been on unauthorized absence from duty since <date of absenteeism>, without any prior intimation to the Company.
<br/>
We wish to bring to your notice that such unauthorized absence is not in compliance to the policies of the Company. You are hence called upon to report for duty immediately, and further called upon to respond / show cause to this notice, within 3 days of receipt of this letter, explaining the reasons for your unauthorized absence, and to substantiate why disciplinary action including termination of employment should not be initiated against you.
<br/>
In the event that no response has been received by the Company from you within the above-mentioned timeframe, it will be understood that you are no longer interested to continue your services with the Company, and that your services will be liable for termination.
<br/>
For {{$employee->legal_entity_name}},
<br/>
<br/>
{{Auth()->user()->emp_name}}

*/?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        *{ margin: 0;padding: 0;margin: 0 auto; }
    </style>
</head>
<body style="font-family:Arial, Helvetica, sans-serif; text-align: center; margin: 0 auto;" >

        <table style="width:750px;padding:10px;font-family:Arial, Helvetica, sans-serif;">
            <tr>
                <td align="center" style="height:20px;">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" style="font-size: 18px; font-weight:bold; font-family: sans-serif; color:000; text-transform:uppercase; text-decoration:underline; text-align:center">ARVIND LIFESTYLE BRANDS LIMITED</td>
            </tr>
            <tr>
                <td align="center" style="font-size: 14px;font-weight:bold; font-family: sans-serif; color:000; text-transform:uppercase;text-align:center">A MEMBER OF THE LALBHAI GROUp</td>
            </tr>
            <tr>
                <td align="center" style="height:20px;">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" style="font-size: 14px;font-weight:normal; font-family: sans-serif; color:000;text-align:center">Du Parc Trinity, 5th Floor, 17, M. G. Road, Bangalore - 560 001</td>
            </tr>
            <tr>
                <td align="center" style="font-size: 14px;font-weight:normal; font-family: sans-serif; color:000;text-align:center">Tel : 91-80-40488775, Fax : 91-80-40488751</td>
            </tr>
            <tr>
                <td align="center" style="font-size: 14px;font-weight:normal; font-family: sans-serif; color:000;text-align:center">Website : http://www.arvindmills.com</td>
            </tr>
            <tr>
                <td align="center" style="font-size: 14px;font-weight:normal; font-family: sans-serif; color:000;text-align:center">CIN/LLPIN No. U64201GJ1995PLC024598</td>
            </tr>
        </table>


        <table style="width:750px;padding:20px 10px 0 10px;font-family:Arial, Helvetica, sans-serif;">
            <tr><td align="right" style="text-align:right; font-weight:bold;"><?php echo date('d.m.y')?> </td></tr>
            <tr><td style="padding:3px 0;"><strong>{{$employee->employee_code}}</strong> <br/></td></tr>
            <tr><td style="padding:3px 0;"><strong>{{$employee->full_name}}</strong> <br/></td></tr>
            <tr><td style="padding:3px 0;"><strong>{{$employee->permanent_address_line_1}}<br />{{$employee->permanent_address_line_2}}<br />{{$employee->permanent_address_line_3}}<br />{{$employee->city_name}}<br />{{$employee->state_name }}<br />{{$employee->permanent_pincode}}</strong> </td></tr>
            <tr><td style="padding:25px 0 10px 0;text-align: center;text-decoration: underline;">Sub: Voluntary Abandonment of Services <br/></td></tr>
        </table>

        <table style="width:750px;padding:10px;font-family:Arial, Helvetica, sans-serif;">
            <tr><td style="padding:0px 0 15px 0;"><strong>Dear {{$employee->full_name}},</strong></td></tr>
            <tr><td  style="padding:3px 0;line-height: 21px;">It has been noted that you have been on unauthorized absence from duty since, {!!  date('d.m.Y',strtotime($employee->absconding_time)) !!} without any prior intimation to the Company. </td></tr>
            <tr><td  style="padding:3px 0;line-height: 21px;">We wish to bring to your notice that such unauthorized absence is not in compliance to the policies of the Company. You are hence called upon to report for duty immediately, and further called upon to respond / show cause to this notice, within 3 days of receipt of this letter, explaining the reasons for your unauthorized absence, and to substantiate why disciplinary action including termination of employment should not be initiated against you.  </td></tr>
            <tr><td  style="padding:3px 0;line-height: 21px;">In the event that no response has been received by the Company from you within the above-mentioned timeframe, it will be understood that you are no longer interested to continue your services with the Company, and that your services will be liable for termination.</td> </tr>
        </table>

        <table style="width:750px;padding:10px 10px 100px 10px;font-family:Arial, Helvetica, sans-serif;">
            <tr><td style="padding:0px 0 15px 0;"><strong>Arvind Lifestyle Brands Ltd,</strong></td></tr>
            <tr><td style="padding:0px 0 5px 0;"><strong>Mentireddy {{Auth()->user()->emp_name}}</strong> <br/></td></tr>
            <tr><td style="padding:0px 0 5px 0;"><strong>Regional HR</strong> <br/></td></tr>
        </table>


</body>
</html>

