@extends('regional.layouts.backend')
@section('title',"Resignation")
@section('css')
    <style>
        .paging { padding-right: 20px; padding: 0px 10px !important; }
        #attendance-table { width:3000px;}
        .select{
            padding: 6px 2px;
            height: 30px;
            font-size: 12px;
            width: 55px;
        }
        .table-responsive tbody > tr > td {
            width: 100px;
        }
        .table-responsive tbody > tr > td:first-child{
            width: 200px;
        }
        .request-change{
            cursor: pointer;
        }
        #abs-table{
            width: 95%; margin: 0 auto;
            font-size: 10px;
        }
        .error{
            color: red;
        }
		#last_working_date{
			padding: 0px !important;
		}
		.employee_id{
			width: 85%; !important;
		}
    </style>
@endsection
@section('content')
<div class="be-content">
{{--@include('storeManager.includes.alerts')--}}
    @include('regional.modals.resignation')
    @include('regional.modals.resignation-upload')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Resignation Requests

                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table style="width:100%;" id="resignation-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Employee Name</th>
                                        <th>Employee Code</th>
                                        <th>Last Working Date</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')

    <script>
		 $("#last_working_date").datetimepicker({
			 minDate: new Date(),
			 maxDate: 'today',
			 autoclose: true,
			 componentIcon: '.mdi.mdi-calendar'
		});
        $("#date_of_resi").datetimepicker({
            minDate: new Date(),
            maxDate: 'today',
            autoclose: true,
            componentIcon: '.mdi.mdi-calendar'
        });

        var url ="{{ url('/regional/resignation-data') }}";
        var remove_url = "{{ url('/regional/remove-resignation') }}";
        var auth_check = "{{ Auth::check() }}";
        var letter_url ="{{url('regional/resignation-letter')}}";
        var download_url = "{{url('/uploads/resignation')}}";
        datatable = $('#resignation-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                // statusChange();
            },
            columns: [
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
				{ data: 'last_working_date',name : 'last_working_date',"searchable": true, "orderable": true},
				{
					data: null,
					"searchable": true,
					"orderable": true,
					"render" : function (o){
						var status = o.resignation_status
						if(status == 'approved') return 'Approve';
						if(status == 'cancelled') return 'Cancel';
						if(status == 'complete') return 'Complete';
						if(status == 'decline') return 'Decline';
						if(status == 'documentation') return 'Documentation Required';
                        if(status == 'documentation-reg') return 'Documentation Processing To Regional';
						if(status == 'pending') return 'Pending';
						if(status == 'processing-hr') return 'Processing To Hrms';
					}
				},
				{ data: 'created_at',name : 'created_at',"searchable": true, "orderable": true},
         /*       {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d=r=l=u= "";

                        v = "<a data-url='"+remove_url+"/"+o.id+"' lwd="+o.last_working_date+" data-id="+o.id+" class='process-itema btn btn-warning btn-sm' title='Remove from resignation'>Approve / Decline</a>&nbsp;";

                        return v+e+u;
                    }

                }, */
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var doc="";
                        li1="";
                        li2="";
                        li3="";
                        li4="";


                    if(o.resignation_status == "pending" ){
                        li4 = "<li><a data-url='"+remove_url+"/"+o.id+"' lwd="+o.last_working_date+" data-id="+o.id+" class='process-itema btn btn-warning btn-sm' title='Remove from resignation'>Approve / Decline</a>&nbsp;</a></li>";
                    }
                    if(o.resignation_status == "documentation-reg" || o.resignation_status == "approved" || o.resignation_status == "processing-hr")
                    {
                        li1 = "<li><a href='"+download_url+"/"+o.agreement_copy+"' target='_blank' data-id="+o.id+" class='download-itema' title='Signature Form'>Download Latter <i class='icon mdi mdi-download'></i> </a></li>";
                     /*   li2 = "<li><a href='"+letter_url+"/"+o.id+"' target='_blank' data-id="+o.id+" class='download-itema' title='Signature Form'>Print Letter <i class='icon mdi mdi-print'></i> </a></li>"; */
                        li3 = "<li><a href='#' data-status="+o.resignation_status+" data-id="+o.id+" class='upload-itema' title='Signature Upload'>Signature Upload <i class='icon mdi mdi-upload'></i> </a></li>";
                    }
                    doc = '<div class="btn-group btn-hspace"><button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Open <span class="icon-dropdown mdi mdi-chevron-down"></span></button><ul role="menu" class="dropdown-menu pull-right"> '+li4+li1+li2+li3+'</ul></div>';
                    return doc;
                } }
            ]
        });

		$(document).on('click', '.process-itema', function (e) {
        	var id = $(this).attr('data-id');
			var lwd = $(this).attr('lwd');

			$("#hidden_res_id").val(id);
			$("#lw_date").val(lwd);

            $("#loading").show();
            $.ajax({
				type: "get",
				url: "{{url('store/resignation')}}/"+id,
				success: function (result) {
					//console.log(data);
					data = result.data.employee;
                    console.log(data);

                    $('.emp_code').html(data.employee_code);
                    $('.io_code').html(data.store_location.io_code);
                    $('.emp_name').html(data.full_name);
                    $('.store_name').html(data.store_location.store_name);
                    $('.designation').html(data.designation.name);
                    $('.mobile').html(data.mobile);
                    $('.doj').html(data.joining_date);
                    $('.email').html(data.email);
                    $('.name').html(data.full_name);
                    $('.ac_no').html(data.account_no);
                    $('.bank_name').html(data.bank_name);
                    $('.ifsc_code').html(data.ifsc);
                    $('.address').html(data.permanent_address_line_1+', '+data.permanent_address_line_2+', '+data.permanent_address_line_3);

					$('#resignation').niftyModal();

					item = result.data.item;

					if(item){
						if(item.application_url && item.application_url != ""){
							var application_url = "<a href='"+item.application_url+"' target='_blank'>Click to view employee application </a>";
							$('.application_url').html(application_url);
						}
                         $('#date_of_resignation').val(item.date_of_resignation);
                         $('#regional_note').html(item.regional_note);
                         $('#reason_id').val(item.reason_id);
                         if(item.resignation_status == 'documentation-reg'){
                             $('#rad6').val('processing-hr');
                             $('#clearance_certificate').show();
                             var url = '{{url('regional/resignation-letter')}}/' +item.id
                             var download_link = "{{url('/uploads/resignation')}}/" +item.agreement_copy;
                             $('#clearance_certificate').html('<a href="'+url+'" target="_blank">View Clearance Certificate</a>');
                             $('#clearance_certificate').append('<a href="'+download_link+'" target="_blank">Download Clearance Certificate</a>');
                         }


                         var _token = $('meta[name="csrf-token"]').attr('content');
                        $.ajax({
                            url:"{{route('store_get_subcategory','reason_id')}}",
                            method:'POST',
                            data: {reason_id:item.reason_id,_token:_token},
                            dataType: "json",
                            success:function(result) {
                                if(result){
                                    $("#sub_reason_id").empty();
                                    console.log(result);
                                    $("#sub_reason_id").append('<option value=""> == Select Sub Category ==</option>');
                                    $.each(result, function(index, stateObj){
                                        var selected = '';
                                        if(item.sub_reason_id == stateObj.id){
                                            selected = 'selected';
                                        }
                                        $("#sub_reason_id").append('<option value="'+stateObj.id+'" '+selected+'>'+stateObj.reason+'</option>');
                                    });
                                }
                            }
                        });
                    //    $('#sub_reason_id').val(item.sub_reason_id);
					/*	$('.application_status').html(item.resignation_status);
						$('.last_working_date_show').html(item.last_working_date);
						$('.note_from_store_manager').html(item.store_note);
						$('.note_from_regional_manager').html(item.regional_note);*/
						//$('.application_status').html(item.last_working_date);
					}

				},
				error: function (xhr, status, error) {

				}
			});

        });

        $(document).on('click', '.upload-itema', function (e) {
            $('#resignation-upload').niftyModal();
        });

        $(document).on('click', '.upload-itema', function (e) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "get",
                url: "{{url('store/resignation')}}/"+id,
                success: function (result) {
                    item = result.data.item;
                    console.log(item);
                    if(item){
                        if(item.resignation_status == "documentation-reg"){
                            $(".submit_btn_block").show();
                            $(".submit_notification_block").hide();
                        }else{
                            $(".submit_btn_block").hide();
                            $(".submit_notification_block").show();
                        }
                        $('#resignation-upload').niftyModal();
                        $("#hidden_file_res_id").val(id);
                    }
                },
                error: function (xhr, status, error) {}
            });
        });


        $(function() {
            $("#reason_id").change(function() {
                var reason_id = $('option:selected', this).val();
                var _token = $('meta[name="csrf-token"]').attr('content');
                if(reason_id != ''){
                    $.ajax({
                        url:"{{route('store_get_subcategory','reason_id')}}",
                        method:'POST',
                        data: {reason_id:reason_id,_token:_token},
                        dataType: "json",
                        success:function(result) {
                            if(result){
                                $("#sub_reason_id").empty();
                             //   console.log(result);
                                $("#sub_reason_id").append('<option value=""> == Select Sub Category ==</option>');
                                $.each(result, function(index, stateObj){
                                    $("#sub_reason_id").append('<option value="'+stateObj.id+'">'+stateObj.reason+'</option>');
                                });
                            }
                        }
                    });
                }
            });
        });

        $('.submit-resignation').on('click',function(){
            url = "{{url('/regional/resignation')}}";

            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: url,
                data: {employee_id:$('select[name="employee_id"]').val()},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
				success: function (data) {
                    location.reload();
                },
                error: function (xhr, status, error) {
                    $('button[type="submit"]').removeAttr('disabled');
                }
            })


        });

        $("#rad7, #rad6").change(function (){
            if ($("#rad7").prop("checked")) {
                $(".reason_of_leaving").hide();
                $(".working_days").hide();
            } else {
                $(".reason_of_leaving").show();
                $(".working_days").show();
            }
        });
           /* $("#rad7").change(function () {
            } */

    </script>
@endsection
