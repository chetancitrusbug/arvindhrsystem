<nav class="navbar navbar-default navbar-fixed-top be-top-header">
    <div class="container-fluid">
        <div class="navbar-header"><a href="index.html" class="navbar-brand"><h3>Arvind Hr System</h3></a></div>
        <div class="be-right-navbar">
        <ul class="nav navbar-nav navbar-right be-user-nav">
            <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="{{ asset('backend/assets/img/avatar.png')}}" alt="Avatar"><span class="user-name">{{auth()->user()->emp_name}}</span></a>
            <ul role="menu" class="dropdown-menu">
                <li>
                <div class="user-info">
                    <div class="user-name">{{auth()->user()->emp_name}}</div>
                    <div class="user-position online">Available</div>
                </div>
                </li>
                <!-- <li><a href="#"><span class="icon mdi mdi-face"></span> Account</a></li>
                <li><a href="#"><span class="icon mdi mdi-settings"></span> Settings</a></li> -->
                <li><a href="{{route('store_rpassword_change_form')}}"><span class="icon mdi mdi-key"></span> Change Password</a></li>
                <li>
                    <a class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <span class="icon mdi mdi-power"></span> Logout
                    </a>
                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
            </li>
        </ul>
        <div class="page-title"><span>{{ $module or 'Dashboard' }}</span></div>
        <ul class="nav navbar-nav navbar-right be-icons-nav">
            <!--<li class="dropdown"><a href="#" role="button" aria-expanded="false" class="be-toggle-right-sidebar"><span class="icon mdi mdi-settings"></span></a></li>  -->
            @if (Auth::check())

            @php

            $resignation_notifications = \App\Resignation::select(['employee.*','resignation.id','resignation.last_working_date','resignation.date_of_resignation','resignation.resignation_status','resignation.agreement_copy',
				'resignation.created_at'])
                ->join('employee','employee.id','=','resignation.employee_id')
                ->leftJoin('store_location','store_location.id','employee.store_id')
                ->where('store_location.region_id',Auth::user()->region)
                ->whereNULL('store_location.deleted_at')
                ->where('store_location.status',1)
                ->orderBy('resignation.updated_at','DESC');

            $resignation_notifications->where('resignation.resignation_status',"pending");
            $resignation_notifications->orwhere('resignation.resignation_status',"documentation-reg");

            $resignation_notifications = $resignation_notifications->get();

            $absconding_notifications = \App\Employee::select([
                    'employee.*','store_location.location_code','store_location.store_name'
                ])
				->leftJoin('store_location','store_location.id','employee.store_id')
                // ->where('user_id',auth()->id())
                ->whereIn('employee.rm_absconding_status',["0"])
                ->whereIn('employee.absconding_status',["1","2"])
                ->where('store_location.region_id',Auth::user()->region)
                ->whereNULL('store_location.deleted_at')
                ->where('store_location.status',1)
                ->orderBy('employee.absconding_time','DESC')
                ->get();


            @endphp
            <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-notifications"></span>
            @if($resignation_notifications || $absconding_notifications)
                @if (count($resignation_notifications) > 0 || count($absconding_notifications) > 0)
                    <span class="indicator"></span>
                @endif

                </a>
                @if (count($resignation_notifications) > 0 || count($absconding_notifications) > 0)
                @php
                $total_notifications = count($resignation_notifications) + count($absconding_notifications);
                @endphp

                <ul class="dropdown-menu be-notifications">
                    <li>
                    <div class="title">Notifications<span class="badge">{{$total_notifications}}</span></div>
                    <div class="list">
                        <div class="be-scroller">
                            <div class="content">
                                @if (count($resignation_notifications) > 0)


                                <ul>
                                    @foreach ($resignation_notifications as $resignation_notification)
                                        <?php
                                            $datetime1 = new DateTime();
                                            $datetime2 = new DateTime($resignation_notification->updated_at);
                                            $interval = $datetime1->diff($datetime2);
                                            $elapsed = ($interval->format('%a') > 0)?$interval->format('%a days'):'';
                                            $elapsed .= ($interval->format('%h') > 0)?$interval->format(' %h hours '):'';
                                            $elapsed .= ($interval->format('%i') > 0)?$interval->format(' %i minutes'):'';
                                            $elapsed .= $interval->format(' %s seconds');
                                        ?>
                                        <li class="notification notification-unread">
                                        <a href="{{url('regional/resignation')}}">
                                            <div class="image"><img src="{{config('app.url')}}backend/assets/img/avatar2.png" alt="Avatar"></div>
                                                <div class="notification-info">
                                                    <div class="text"><span class="user-name">{{$resignation_notification->full_name}}</span> give a resignation <!--from the {{$resignation_notification->store_name}} --></div><span class="date">{{$elapsed}} ago</span>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                                @endif
                                @if (count($absconding_notifications) > 0)
                                <ul>
                                    @foreach ($absconding_notifications as $absconding_notification)
                                    <?php
                                                                        $datetime1 = new DateTime();
                                                                        $datetime2 = new DateTime($absconding_notification->updated_at);
                                                                        $interval = $datetime1->diff($datetime2);
                                                                        $elapsed = ($interval->format('%a') > 0)?$interval->format('%a days'):'';
                                                                        $elapsed .= ($interval->format('%h') > 0)?$interval->format(' %h hours '):'';
                                                                        $elapsed .= ($interval->format('%i') > 0)?$interval->format(' %i minutes'):'';
                                                                        $elapsed .= $interval->format(' %s seconds');
                                                                    ?>
                                        <li class="notification notification-unread">
                                            <a href="{{url('regional/absconding')}}">
                                                <div class="image"><img src="{{config('app.url')}}backend/assets/img/avatar2.png" alt="Avatar"></div>
                                                <div class="notification-info">
                                                    <div class="text"><span class="user-name">{{$absconding_notification->full_name}}</span> was absconding from store
                                                        <!--from the {{$absconding_notification->store_name}} -->
                                                    </div><span class="date">{{$elapsed}} ago</span>
                                                </div>
                                            </a>
                                        </li>
                                        @endforeach
                                </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                <!-- <div class="footer"> <a href="#">View all notifications</a></div> -->
                    </li>
                    @endif
                </ul>
            @endif
            @endif
            </li>
           <!-- <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-apps"></span></a>
            <ul class="dropdown-menu be-connections">
                <li>
                <div class="list">
                    <div class="content">
                    <div class="row">
                        <div class="col-xs-4"><a href="#" class="connection-item"><img src="assets/img/github.png" alt="Github"><span>GitHub</span></a></div>
                        <div class="col-xs-4"><a href="#" class="connection-item"><img src="assets/img/bitbucket.png" alt="Bitbucket"><span>Bitbucket</span></a></div>
                        <div class="col-xs-4"><a href="#" class="connection-item"><img src="assets/img/slack.png" alt="Slack"><span>Slack</span></a></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4"><a href="#" class="connection-item"><img src="assets/img/dribbble.png" alt="Dribbble"><span>Dribbble</span></a></div>
                        <div class="col-xs-4"><a href="#" class="connection-item"><img src="assets/img/mail_chimp.png" alt="Mail Chimp"><span>Mail Chimp</span></a></div>
                        <div class="col-xs-4"><a href="#" class="connection-item"><img src="assets/img/dropbox.png" alt="Dropbox"><span>Dropbox</span></a></div>
                    </div>
                    </div>
                </div>
                <div class="footer"> <a href="#">More</a></div>
                </li>
            </ul>
            </li> -->
        </ul>
        </div>
    </div>
</nav>