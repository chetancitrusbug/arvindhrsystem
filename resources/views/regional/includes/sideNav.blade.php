<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Dashboard</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements" role="tablist">
						<li ><a href="{{ url('regional/') }}"><span>Dashboard</span></a></li>
                        <li class="{{(isset($route) && $route == 'attendance-list')?'active':''}}">
                            <a href="{{url('regional/attendence-list') }}"><span>Attendance Report</span></a>
                        </li>
                        <li class="{{(isset($route) && $route == 'hiring')?'active':''}}"><a href="{{ url('regional/hiring') }}"><span>Hiring Request</span></a></li>
                        <li class="{{(isset($route) && $route == 'employee')?'active':''}}"><a href="{{ url('regional/storelist') }}"><span>Store </span></a></li>
                        <li class="{{(isset($route) && $route == 'attendance')?'active':''}}"><a href="{{ url('regional/attendance') }}"><span>Attendance Request</span></a></li>
                        <!--<li class="{{(isset($route) && $route == 'separation')?'active':''}}"><a href="{{ url('regional/absconding') }}"><span>Absconding</span></a></li>
						<li class="{{ Request::is('regional/resignation') ? 'active' : '' }}"><a href="{{ url('regional/resignation') }}"><span>Resignation</span></a></li> -->

						<li class="parent {{(isset($route) && ($route == 'separation' || $route == 'resignation' ))?'active open':''}}"><a href="#"><span>Separation Request</span></a>
							<ul class="sub-menu">
								<li class="{{(isset($route) && $route == 'separation')?'active':''}}"><a href="{{ url('regional/absconding') }}"><span>Absconding </span></a></li>
								<li class="{{ Request::is('regional/resignation') ? 'active' : '' }}"><a href="{{ url('regional/resignation') }}"><span>Resignation</span></a></li>
							</ul>
						</li>
						<li class="parent {{(isset($route) && ($route == 'man-power' || $route == 'hygiene' || $route == 'manpowercost' || $route == 'hiringReport' || $route == 'hiring-inprocess' || $route == 'additional' || $route == 'attrition'))?' active open':''}}"><a href="#"><span>Reports</span></a>
							<ul class="sub-menu">
								<li class="{{(isset($route) && $route == 'man-power')?'active':''}}"><a href="{{ url('regional/manpower-report/store') }}"><span> Man Power Reports</span></a></li>
								<li class="{{(isset($route) && $route == 'manpowercost')?'active':''}}"><a href="{{ url('regional/manpowercost-report/store') }}"><span> Man Power Cost Reports</span></a></li>
								<li class="{{(isset($route) && $route == 'hygiene')?'active':''}}"><a href="{{ url('regional/hygiene-report/store') }}"><span> Hygiene Reports</span></a></li>
								<li class=" {{(isset($route) && ($route == 'hiringReport' ))?'active ':''}}"><a href="{{url('regional/hiring-report-store') }}"><span>Hiring Reports</span></a></li>
								<li class="{{(isset($route) && $route == 'hiring-inprocess')?'active':''}}"><a href="{{ url('regional/hiring-inprocess/store') }}"><span> Hiring - Inprocess Report</span></a></li>
								<li class="{{(isset($route) && $route == 'additional')?'active':''}}"><a href="{{ url('regional/additional-report/store') }}"><span> Additional Report</span></a></li>
                                <li class="{{(isset($route) && $route == 'attrition')?'active':''}}"><a href="{{ url('regional/store-attrition-report') }}"><span> Attrition Report (MTD)</span></a></li>
                                <li class="{{(isset($route) && $route == 'attrition-reason')?'active':''}}"><a href="{{ url('regional/store-reason-report') }}"><span> Attrition by Reason Report</span></a></li>
                                <li class="{{(isset($route) && $route == 'sales-report')?'active':''}}"><a href="{{ url('regional/productivity-report-store') }}"><span> Productivity Report</span></a></li>
                                <li class="{{(isset($route) && $route == 'leave-report')?'active':''}}"><a href="{{ url('regional/leave-report-store') }}"><span> Leave Report</span></a></li>

							</ul>
						</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>