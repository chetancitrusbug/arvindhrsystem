<div id="resignation" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_forword_title">Resignation Request Process</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>
                    <!-- form-horizontal group-border-dashed // Removed class from form -->
					<form action="{{url('regional/resignation')}}/update" method="POST" style="border-radius: 0px;" class="">
					{{ csrf_field() }}
					{{ method_field('PATCH') }}
                    <div class="form-group col-sm-12" style="display:none" id="clearance_certificate">
                        <a href="#" target="_blank">View Clearance Certificate</a>
                    </div>
					<input type="hidden" name="rid" value="" id="hidden_res_id">
					<div class="form-group col-sm-12">
                      <label class="col-sm-3 control-label">Accept or Decline Request</label>
                      <div class="col-sm-9">
                        <div class="be-radio inline">
                          <input type="radio" checked name="resignation_status" value="approved" id="rad6">
                          <label for="rad6">Accept</label>
                        </div>
                        <div class="be-radio inline">
                          <input type="radio"  name="resignation_status" value="decline" id="rad7">
                          <label for="rad7">Decline</label>
                        </div>
                        <!--<div class="be-radio inline">
                            <input type="radio" name="resignation_status" value="processing-hr" id="rad8">
                            <label for="rad8">Appoval For HRMS</label>
                        </div> -->

                      </div>
                    </div>

					<div class="form-group col-sm-6 working_days">
						{!! Form::label('', 'Last Working Date: ', ['class' => '']) !!}
							<div data-min-view="2" id="last_working_date" data-date-format="dd/mm/yyyy" class="input-group date ">
							  <input size="16" name="last_working_date" id="lw_date" type="text" value="" class="form-control input-sm input-height-39"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
							</div>

                    </div>
                    <div class="form-group col-sm-6 working_days">
                        {!! Form::label('', '* Date Of Resignation: ', ['class' => '']) !!}
                        <div data-min-view="2" id="date_of_resi" data-date-format="dd/mm/yyyy" class="input-group date-reg ">
                            <input size="16" id="date_of_resignation" name="date_of_resignation" type="text" value="" class="form-control input-sm input-height-39">
                            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                        </div>
                    </div>


                    <div class="form-group col-sm-6 reason_of_leaving">
                        {!! Form::label('reason_id', '* Reason: ', ['class' => '']) !!}
                        {!! Form::select('reason_id', [''=>'--select Reason--']+$reason_parent,null,
                        ['class' => 'col-sm-12 form-control input-sm reason_id','id'=>'reason_id']) !!}
                        <p style="color: red;" class="employee_id_error error hide"></p>
                    </div>
                    <div class="form-group col-sm-6 reason_of_leaving">
                        {!! Form::label('sub_reason_id', '* Sub Reason: ', ['class' => '']) !!} {!! Form::select('sub_reason_id', [''=>'--select
                        Sub Reasion--'],null, ['class' => 'col-sm-12 form-control input-sm reason_id','id'=>'sub_reason_id']) !!}
                        <p style="color: red;" class="employee_id_error error hide"></p>

                    </div>
                    <div class="form-group col-sm-12">
                        <!--<label class="col-sm-3 control-label">Note</label> -->
                        {!! Form::label('regional_note', 'Note: ', ['class' => '']) !!} {!! Form::textarea('regional_note', '', ['class' => '','style'=>"min-width:
                        100%;max-height: 100px;"]) !!}

                        <!--  <div class="col-sm-6">
                                            <textarea class="form-control" id="regional_note" name="regional_note" style="min-width: 100%"></textarea>
                                          </div> -->
                    </div>
					<div class="text-center col-sm-12">
                       <!-- <div class="xs-mt-20"> -->
                            <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
                            <button type="submit" data-dismiss="modal" class="btn btn-success submit-resignation">Proceed</button>
                        <!--</div> -->
                     </div>
                     <div class="text-center col-sm-12">
                        <div class="xs-mt-10">
                        </div>
                    </div>
					</form>
					<hr/>


            <div class="form-group">


                <table id="abs-table" class="table table-bordered table-hover table-fw-widget" width="70%;">
                    <tbody>

						<tr>
                            <th>Application</th>
                            <td colspan="3" class="application_url"></td>
                        </tr>
                        <tr>
                            <th width="20%">Employee Code</th>
                            <td width="30%" class="emp_code"></td>
                            <th width="20%">Store IO Code</th>
                            <td width="30%" class="io_code"></td>
                        </tr>
                        <tr>
                            <th>Emp Name</th>
                            <td class="emp_name"></td>
                            <th>Store Name</th>
                            <td class="store_name"></td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td class="designation"></td>
                            <th>Mobile</th>
                            <td class="mobile"></td>
                        </tr>
                        <tr>
                            <th>Date Of joining</th>
                            <td class="doj"></td>
                            <th>Email</th>
                            <td class="email"></td>
                        </tr>
                        <tr>
                            <th>Name as per Bank</th>
                            <td class="name"></td>
                            <th>Bank A/c No</th>
                            <td class="ac_no"></td>
                        </tr>
                        <tr>
                            <th>Bank Name</th>
                            <td class="bank_name"></td>
                            <th>IFSC Code</th>
                            <td class="ifsc_code"></td>
                        </tr>
                        <tr>
                            <th>House Address for communication</th>
                            <td colspan="3" class="address"></td>
                        </tr>
                    </tbody>
                </table>

            </div>



			<div class="form-group">

			</div>
        </div>

        <div class="modal-footer text-center">

        </div>
    </div>
</div>


