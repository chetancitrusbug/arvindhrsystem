<div id="add_pdo_modal" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_forword_title">Add PDO Detail</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>
            <!-- form-horizontal group-border-dashed // Removed class from form -->
            <form action="{{url('regional/store/pdo')}}" method="POST" style="border-radius: 0px;" class="">
                {{ csrf_field() }}
                <input type="hidden" name="employee_id" value="" id="abscounding_employee_id">

                <div class="form-group col-sm-6 working_days">
                    {!! Form::label('pdo_date', 'PDO Date: ', ['class' => '']) !!}
                    <div data-min-view="2" id="pdo_date_div" data-date-format="dd/mm/yyyy" class="input-group date ">
                        <input size="16" name="pdo_date" id="pdo_date" type="text" value="" class="form-control input-sm input-height-39">
                        <span
                            class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                    </div>

                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('pdo_number', '* PDO Number: ', ['class' => '']) !!}
                        <input type="text" name="pdo_number" id="pdo_number" class="form-control input-sm input-height-39" />
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::label('courier_name', '* Courier Name: ', ['class' => '']) !!}
                        <input type="text" name="courier_name" id="courier_name" class="form-control input-sm input-height-39" />
                </div>

                <div class="text-center col-sm-12">
                    <!-- <div class="xs-mt-20"> -->
                    <button type="button" data-dismiss="modal" class="btn btn-default modal-close ">Cancel</button>
                    <button type="submit" data-dismiss="modal" class="btn btn-success submit_pdo_form">Proceed</button>
                    <!--</div> -->
                </div>
                <div class="text-center col-sm-12">
                    <div class="xs-mt-10">
                    </div>
                </div>
            </form>
            <hr/>


            <div class="form-group">


                <table id="abs-table" class="table table-bordered table-hover table-fw-widget" width="70%;">
                    <tbody>
                        <tr>
                            <th width="20%">Employee Code</th>
                            <td width="30%" class="emp_code"></td>
                            <th width="20%">Store IO Code</th>
                            <td width="30%" class="io_code"></td>
                        </tr>
                        <tr>
                            <th>Emp Name</th>
                            <td class="emp_name"></td>
                            <th>Store Name</th>
                            <td class="store_name"></td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td class="designation"></td>
                            <th>Mobile</th>
                            <td class="mobile"></td>
                        </tr>
                        <tr>
                            <th>Date Of joining</th>
                            <td class="doj"></td>
                            <th>Email</th>
                            <td class="email"></td>
                        </tr>
                        <tr>
                            <th>Name as per Bank</th>
                            <td class="name"></td>
                            <th>Bank A/c No</th>
                            <td class="ac_no"></td>
                        </tr>
                        <tr>
                            <th>Bank Name</th>
                            <td class="bank_name"></td>
                            <th>IFSC Code</th>
                            <td class="ifsc_code"></td>
                        </tr>
                        <tr>
                            <th>House Address for communication</th>
                            <td colspan="3" class="address"></td>
                        </tr>
                    </tbody>
                </table>

            </div>



            <div class="form-group">

            </div>
        </div>

        <div class="modal-footer text-center">

        </div>
    </div>
</div>


<div id="view_pdo_modal" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_forword_title">View PDO Detail</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>
            <!-- form-horizontal group-border-dashed // Removed class from form -->
            <div class="form-group">


                <table id="abs-table" class="table table-bordered table-hover table-fw-widget" width="70%;">
                    <tbody>
                        <tr>
                            <th width="20%">PDO Date</th>
                            <td width="30%" class="modal_pdo_date"></td>
                            <th width="20%">PDO Number</th>
                            <td width="30%" class="modal_pdo_number"></td>
                        </tr>
                        <tr>
                            <th >Courier Name</th>
                            <td  colspan="3" class="modal_courier_name"></td>

                        </tr>
                        <tr>
                            <th width="20%">Employee Code</th>
                            <td width="30%" class="emp_code"></td>
                            <th width="20%">Store IO Code</th>
                            <td width="30%" class="io_code"></td>
                        </tr>
                        <tr>
                            <th>Emp Name</th>
                            <td class="emp_name"></td>
                            <th>Store Name</th>
                            <td class="store_name"></td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td class="designation"></td>
                            <th>Mobile</th>
                            <td class="mobile"></td>
                        </tr>
                        <tr>
                            <th>Date Of joining</th>
                            <td class="doj"></td>
                            <th>Email</th>
                            <td class="email"></td>
                        </tr>
                        <tr>
                            <th>Name as per Bank</th>
                            <td class="name"></td>
                            <th>Bank A/c No</th>
                            <td class="ac_no"></td>
                        </tr>
                        <tr>
                            <th>Bank Name</th>
                            <td class="bank_name"></td>
                            <th>IFSC Code</th>
                            <td class="ifsc_code"></td>
                        </tr>
                        <tr>
                            <th>House Address for communication</th>
                            <td colspan="3" class="address"></td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <div class="form-group">
            </div>
        </div>

        <div class="modal-footer text-center">

        </div>
    </div>
</div>