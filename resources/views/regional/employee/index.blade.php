@extends('regional.layouts.backend')
@section('title',"Employee")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.confirmation_index')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Employee List
						<a class="btn btn-space btn-warning pull-right" href="{{ URL::previous() }}">Back</a>
					</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table style="width:100%;" id="employee-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Employee Code</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Store Name</th>
                                        <th>Location</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>

        var store_id = "{{request()->route('store_id')}}";
        var url ="{{ url('/regional/employee-data') }}/"+store_id;
        var edit_url = "{{ url('regional/employee') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
			createdRow: function ( row, data, index ) {

				if ( data['rm_absconding_status'] == 2 || data['resignation_status'] == 'complete' ) {
                    $(row).addClass("danger");
					$('tr').addClass('success');

				}

			}
			,
            columns: [
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { data: 'designation',name : 'designation',"searchable": true, "orderable": false},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},
                { data: 'location_code',name : 'location_code',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":160,
                    "render": function (o) {
                        var e = v = d = a = b ="";

                        if((o.store_status == 1 && o.regional_status != 2) || o.hrms_status == 3){
                            a = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/approve' value="+o.id+" data-id="+o.id+" data-title='regional' class='btn btn-success btn-sm send_to_hr_from_index' title='Approve'><i class='mdi mdi-redo'></i></a>&nbsp;";

                            b = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/send-back' value="+o.id+" data-note='"+o.regional_note+"' data-title='Store Manager' data-id="+o.id+" class='btn btn-danger btn-sm regional_send_back' title='Send back'><i class='mdi mdi-undo' ></i></a>&nbsp;";
                        }

                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                        e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='employee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";

                        d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('regional/employee')}} data-msg='employee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";

                        l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-success btn-sm' title='View Log' ><i class='mdi mdi-menu' aria-hidden='true'></i></button></a>&nbsp;";
                        return v+e;
                    }

                }
            ]
        });
</script>


@endsection