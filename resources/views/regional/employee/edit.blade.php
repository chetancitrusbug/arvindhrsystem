@extends('regional.layouts.backend')
@section('title',"Edit employee")
@section('css')
<style type="text/css">
    .scanned_photo {
        position: relative;
    }

    .scanned_photo-attach {
        position: absolute;
        left: 65px;
        top: 0px;
    }

    img {
        height: 50px;
        width: 50px;
    }

    .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
            color: #fff !important;
            background-color: #2572f2 !important;
            border-color: transparent;
            border-top-color: #0c57d3 !important;
            box-shadow: inset 0 2px 0 #1266f1 !important;
        }
    input[type="file"]{
        border: none;
    }
    i.mdi.mdi-case-download{
        font-size: 30px;
    }
    .case{
        text-align: right;
    }
    .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
            color: #fff !important;
            background-color: #2572f2 !important;
            border-color: transparent;
            border-top-color: #0c57d3 !important;
            box-shadow: inset 0 2px 0 #1266f1 !important;
        }
    .form_label{
        border: none;
        padding: 10px;
        text-align: center;
        font-size: 16px;
        background: #eee;
    }
    .family_label{
        border: none;
        padding: 5px 5px 0px;
    }
    .text-center{
        text-align: center;
    }
    td {
        vertical-align: top !important;
    }
</style>

@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary panel-custom-a1">
			{!! Form::model($employee, ['method' => 'PATCH','url' => ['/regional/employee', $employee->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                <div class="panel-heading panel-heading-divider">Edit Employee # <strong>{{$employee->full_name}}</strong>
                    <span class="panel-subtitle clearfix">
                    
                        <div class="abs-right20">
                            <a href="{{ url('/regional/employeelist') }}/{{$employee->store_id}}" title="Back">
                                <label class="btn btn-space btn-warning">Back</label>
                            </a>
                            {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary']) !!}
                        </div>    

						<div class="col-sm-6 pull-right{{ $errors->has('store_id') ? ' has-error' : ''}}">
						{!! Form::label('transfer_employee', 'Transfer Employee : ', ['class' => 'col-sm-3 control-label']) !!}
							<div class="col-sm-6">
								{!! Form::select('store_id', $StoreLocation, $employee->store_id ,['class' => 'form-control select2 input-sm ']) !!}
								{!! $errors->first('store_id', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
					</span>


                </div>
                <div class="panel-body">
					@include ('regional.employee.form')

					@include('storeManager.modals.confirmation')
					<input type="hidden" name="button" value="submit">
					<div class="form-group col-sm-12">
                        <div class="pr-10">
                            {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
					</div>

                </div>
				{!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection