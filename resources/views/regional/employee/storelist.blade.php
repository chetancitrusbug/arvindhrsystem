@extends('regional.layouts.backend')
@section('title',"Employee")
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.confirmation_index')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
						Store List
						<a class="btn btn-primary btn-employee1" href="{{url('regional/employee')}}">All Employee</a>
					</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table style="width:100%;" id="employee-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Store Io Code</th>
                                        <!--<th>Name</th> -->
                                        <th>Store Code</th>
										<th>Store Name</th>
                                        <!--<th>Location</th> -->
                                        <th>View Employee</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var url ="{{ url('/regional/store-data') }}";
        var edit_url = "{{ url('/regional/employeelist') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#employee-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,

            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'io_code',name : 'io_code',"searchable": true, "orderable": false},
               /* { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false}, */
                /*{ data: 'address',name : 'address',"searchable": true, "orderable": false}, */
                { data: 'sap_code',name : 'sap_code',"searchable": true, "orderable": false},
				{ data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},
            /*    { data: 'location_code',name : 'location_code',"searchable": true, "orderable": false}, */
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":160,
                    "render": function (o) {
                        var e = v = d = a = b ="";

                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View Store Employee' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";



                        return v+e;
                    }

                }
            ]
        });

</script>
@endsection