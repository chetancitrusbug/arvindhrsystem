<div class="form-group">
    {!! Form::label('Full Name', 'Full Name: ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6" style="vertical-align:middle;">
        {!! Form::label('', isset($employee->full_name)?$employee->full_name:'', ['class' => 'control-label bold']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Designation', 'Designation: ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6" style="vertical-align:middle;">
        {!! Form::label('', isset($employee->designation->name)?$employee->designation->name:'', ['class' => 'control-label bold']) !!}
    </div>
</div>
<div class="form-group{{ $errors->has('business_unit_id') ? ' has-error' : ''}}">
    {!! Form::label('business_unit_id', 'Business Unit: ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6" style="vertical-align:middle;">
        {!! Form::label('', isset($businessUnits->name)?$businessUnits->name:'', ['class' => 'control-label bold']) !!}
        {!! Form::hidden('business_unit_id', isset($businessUnits->id)?$businessUnits->id:'', null) !!}
        {!! $errors->first('business_unit_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('legal_entity_id') ? ' has-error' : ''}}">
    {!! Form::label('legal_entity_id', 'Legal Entity: ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::label('', isset($legalEntity->name)?$legalEntity->name:'', ['class' => 'control-label bold']) !!}
        {!! Form::hidden('legal_entity_id', isset($legalEntity->id)?$legalEntity->id:'', null) !!}
        {!! $errors->first('legal_entity_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('employee_classification_id') ? ' has-error' : ''}}">
    {!! Form::label('employee_classification_id', 'Employee Classification: ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::label('', isset($employeeClassification->name)?$employeeClassification->name:'', ['class' => 'control-label bold']) !!}
        {!! Form::hidden('employee_classification_id', isset($employeeClassification->id)?$employeeClassification->id:'', null) !!}
        {!! $errors->first('employee_classification_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('variable_pay_type_id') ? ' has-error' : ''}}">
    {!! Form::label('variable_pay_type_id', 'Variable Pay Type: ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::label('', isset($variablePayType->name)?$variablePayType->name:'', ['class' => 'control-label bold']) !!}
        {!! Form::hidden('variable_pay_type_id', isset($variablePayType->id)?$variablePayType->id:'', null) !!}
        {!! $errors->first('variable_pay_type_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<input type="hidden" name="employee_id" value="{{$id}}">
<input type="hidden" name="upload" value="{{$upload}}">
<div class="form-group{{ $errors->has('joining_date') ? ' has-error' : ''}}">
    {!! Form::label('joining_date', 'Joining Date: ', ['class' => 'col-sm-4 control-label','data-date-format'=>"dd-mm-yyyy"]) !!}
    <div class="col-sm-6 col-xs-12">
        <div data-min-view="2"  class="input-group date datetimepicker1">
            {!! Form::text('joining_date', old('joining_date',$employee->joining_date), ['class' => 'form-control input-sm']) !!}
            <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
        </div>
        {!! $errors->first('joining_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group hide{{ $errors->has('pod_no') ? ' has-error' : ''}}">
    {!! Form::label('pod_no', 'POD No: ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('pod_no', $employee->pod_no, ['class' => 'form-control input-sm']) !!} {!! $errors->first('pod_no', '
        <p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group scanned_photo {{ $errors->has('attach.scanned_photo') ? ' has-error' : ''}}">
    {!! Form::label('attach[scanned_photo]', '* Scanned Photo (jpg file Only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[scanned_photo]" class="form-control input-sm logo" data-ext="jpg">
        {!! $errors->first('attach.scanned_photo', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['scanned_photo']['file']) && !empty($employee->attach['scanned_photo']['file']))
            <div class="scanned_photo-attach"><img src="{{ asset($employee->attach['scanned_photo']['file']) }}" class="changeImage"></div>
        @endif
    </div>
</div>
<div class="form-group employment_form {{ $errors->has('attach.employment_form') ? ' has-error' : ''}}">
    {!! Form::label('attach[employment_form]', '* Employement form attachment (pdf file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[employment_form]" class="form-control input-sm logo" data-ext="pdf">
        {!! $errors->first('attach.employment_form', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['employment_form']['file']) && !empty($employee->attach['employment_form']['file']))
            <div class="employment_form-attach case"><a href="{{ url('/').'/'.$employee->attach['employment_form']['file'] }}" target="_blank"}" title="download Employement form attachment"><i class="mdi mdi-case-download"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group form_1 {{ $errors->has('attach.form_1') ? ' has-error' : ''}}">
    {!! Form::label('attach[form_1]', '* Form - 1 (pdf file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-5">
        <input type="file" name="attach[form_1]" accept="application/pdf" class="form-control input-sm logo" data-ext="pdf"> {!! $errors->first('attach.form_1',
        '
        <p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['form_1']['file']) && !empty($employee->attach['form_1']['file']))
        <div class="form_1-attach case"><a href="{{ url('/').'/'.$employee->attach['form_1']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Form-1"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group form_11 {{ $errors->has('attach.form_11') ? ' has-error' : ''}}">
    {!! Form::label('attach[form_11]', '* Form-11 (pdf file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[form_11]" class="form-control input-sm logo" data-ext="pdf">
        {!! $errors->first('attach.form_11', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['form_11']['file']) && !empty($employee->attach['form_11']['file']))
            <div class="form_11-attach case"><a href="{{ url('/').'/'.$employee->attach['form_11']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Form-11"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group form_02 {{ $errors->has('attach.form_02') ? ' has-error' : ''}}">
    {!! Form::label('attach[form_02]', '* Form-02 (pdf file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[form_02]" class="form-control input-sm logo" data-ext="pdf">
        {!! $errors->first('attach.form_02', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['form_02']['file']) && !empty($employee->attach['form_02']['file']))
            <div class="form_02-attach case"><a href="{{ url('/').'/'.$employee->attach['form_02']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Form-02"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group form_f {{ $errors->has('attach.form_f') ? ' has-error' : ''}}">
    {!! Form::label('attach[form_f]', '* Form-F (pdf file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[form_f]" class="form-control input-sm logo" data-ext="pdf">
        {!! $errors->first('attach.form_f', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['form_f']['file']) && !empty($employee->attach['form_f']['file']))
            <div class="form_f-attach case"><a href="{{ url('/').'/'.$employee->attach['form_f']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Form-F"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group hiring_checklist {{ $errors->has('attach.hiring_checklist') ? ' has-error' : ''}}">
    {!! Form::label('attach[hiring_checklist]', '* Hiring Checklist (pdf file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[hiring_checklist]" class="form-control input-sm logo" data-ext="pdf">
        {!! $errors->first('attach.hiring_checklist', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['hiring_checklist']['file']) && !empty($employee->attach['hiring_checklist']['file']))
            <div class="hiring_checklist-attach case"><a href="{{ url('/').'/'.$employee->attach['hiring_checklist']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Hiring Checklist"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group inteview_assessment {{ $errors->has('attach.inteview_assessment') ? ' has-error' : ''}}">
    {!! Form::label('attach[inteview_assessment]', '* Interview Assessment (pdf file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[inteview_assessment]" class="form-control input-sm logo" data-ext="pdf">
        {!! $errors->first('attach.inteview_assessment', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['inteview_assessment']['file']) && !empty($employee->attach['inteview_assessment']['file']))
            <div class="inteview_assessment-attach case"><a href="{{ url('/').'/'.$employee->attach['inteview_assessment']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Interview Assessment"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group resume {{ $errors->has('attach.resume') ? ' has-error' : ''}}">
    {!! Form::label('attach[resume]', '* Resume (pdf file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[resume]" class="form-control input-sm logo" data-ext="pdf">
        {!! $errors->first('attach.resume', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['resume']['file']) && !empty($employee->attach['resume']['file']))
            <div class="resume-attach case"><a href="{{ url('/').'/'.$employee->attach['resume']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Resume"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group education_documents {{ $errors->has('attach.education_documents') ? ' has-error' : ''}}">
    {!! Form::label('attach[education_documents]', '* Education Documents (pdf, jpg file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[education_documents][]" multiple="multiple" class="form-control input-sm logo education_docs">
        {!! $errors->first('attach.education_documents', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['education_documents']) && !empty($employee->attach['education_documents']))
            @foreach($employee->attach['education_documents'] as $key=>$value)
                <div class="education_documents-attach case"><a href="{{ url('/').'/'.$value['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Education Documents"></i></a></div>
            @endforeach
        @endif
    </div>
</div>
<div class="form-group aadhar_card {{ $errors->has('attach.aadhar_card') ? ' has-error' : ''}}">
    {!! Form::label('attach[aadhar_card]', '* Aadhar Card (pdf, jpg file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[aadhar_card]" class="form-control input-sm logo" data-ext="pdf|jpg">
        {!! $errors->first('attach.aadhar_card', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['aadhar_card']['file']) && !empty($employee->attach['aadhar_card']['file']))
            <div class="aadhar_card-attach case"><a href="{{ url('/').'/'.$employee->attach['aadhar_card']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Aadhar Card"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group pan_card {{ $errors->has('attach.pan_card') ? ' has-error' : ''}}">
    {!! Form::label('attach[pan_card]', '* Pan Card (pdf, jpg file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[pan_card]" class="form-control input-sm logo" data-ext="pdf|jpg">
        {!! $errors->first('attach.pan_card', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['pan_card']['file']) && !empty($employee->attach['pan_card']['file']))
            <div class="pan_card-attach case"><a href="{{ url('/').'/'.$employee->attach['pan_card']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Pan Card"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group bank_passbook {{ $errors->has('attach.bank_passbook') ? ' has-error' : ''}}">
    {!! Form::label('attach[bank_passbook]', '* Bank Passbook/Cheque (pdf, jpg file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[bank_passbook]" class="form-control input-sm logo" data-ext="pdf|jpg">
        {!! $errors->first('attach.bank_passbook', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['bank_passbook']['file']) && !empty($employee->attach['bank_passbook']['file']))
            <div class="bank_passbook-attach case"><a href="{{ url('/').'/'.$employee->attach['bank_passbook']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Bank Passbook/Cheque"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group offer_accept {{ $errors->has('attach.offer_accept') ? ' has-error' : ''}}">
    {!! Form::label('attach[offer_accept]', '* offer letter acceptancey mail (pdf file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[offer_accept]" class="form-control input-sm logo" data-ext="pdf">
        {!! $errors->first('attach.offer_accept', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['offer_accept']['file']) && !empty($employee->attach['offer_accept']['file']))
            <div class="offer_accept-attach case"><a href="{{ url('/').'/'.$employee->attach['offer_accept']['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download offer letter acceptancey mail"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group previous_company_docs {{ $errors->has('attach.previous_company_docs') ? ' has-error' : ''}}">
    {!! Form::label('attach[previous_company_docs]', '* Previouse company offer letter / payslips / releiving letter (pdf, jpg file only): ', ['class' => 'col-sm-4 control-label']) !!}
    <div class="col-sm-6">
        <input type="file" name="attach[previous_company_docs][]" multiple="multiple" class="form-control input-sm logo previous_company_docs">
        {!! $errors->first('attach.previous_company_docs', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['previous_company_docs']) && !empty($employee->attach['previous_company_docs']))
            @foreach($employee->attach['previous_company_docs'] as $key=>$value)
                <div class="previous_company_docs-attach case"><a href="{{ url('/').'/'.$value['file'] }}" target="_blank"><i class="mdi mdi-case-download" title="download Previouse company offer letter / payslips / releiving letter"></i></a></div>
            @endforeach
        @endif
    </div>
</div>

@section('scripts')
    <script>
        $(".datetimepicker1").datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                componentIcon: '.mdi.mdi-calendar',
            /*    pickerPosition: "bottom-left", */
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });

        $('input[name="attach[scanned_photo]"]').on('change',function(){
            $('.scanned_photo-attach').hide();
        });
    </script>
@endsection