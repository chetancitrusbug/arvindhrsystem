@extends('regional.layouts.backend')
@section('title',"Hiring")
@section('css')
    <style type="text/css">
        .hide{
            display: none;
        }
        .panel-table tr td:last-child {
            padding-right: 0px;
        }
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.confirmation_index')
    @include('storeManager.modals.forward')
    @include('storeManager.modals.reject')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Hiring List</div>
                    <div class="panel-body">
                        <div class="table-responsive table-2">
                            <table style="width:100%;" id="employee-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        {{-- <th>Employee Code</th> --}}
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Store Name</th>
                                        <th>Location</th>
                                        <th>Status</th>
                                        <th class="min-width-xs">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('.source_category').on('change', function(event) {
            $('.employee_name, .employee_code, .old_employee_code, .employee_refferal_payment_month,.employee_refferal_amount').hide();
            $('input[name="employee_name"], input[name="employee_code"], input[name="old_employee_code"]').hide();
            $('.source_code, .source_name').show();
            $('input[name="source_code"], select[name="source_name"]').show();

            var category = $('select[name="source_category"] option:selected').html();
            $('input[name="category_name"]').val(category);

            if(category == 'Employee Referral'){
                $('.source_code, .source_name').hide();
                $('input[name="source_code"], select[name="source_name"]').hide();

                $('.employee_name, .employee_code,.employee_refferal_payment_month,.employee_refferal_amount').show();
                $('input[name="employee_name"], input[name="employee_code"]').show();
            }else if(category == 'Rehire'){
                $.ajax({
                    url: "{{ url('/regional/sorce-detail') }}",
                    data: {category: $(this).val()},
                })
                .done(function(data) {
                    $('select[name="source_name"] option:not(:first)').remove();
                    if(data != ''){
                        $.each(data, function(index, val) {
                            $('select[name="source_name"]').append("<option value='"+index+"' selected>"+val+"</option>");
                        });
                        $('.source_name, select[name="source_name"]').show();
                        $('.employee_name, .employee_code, input[name="employee_name"], input[name="employee_code"]').hide();
                    }
                });
                $('.source_code').hide();
                $('input[name="source_code"]').hide();
                // selectValue('source_name','Rehire');
                $('.old_employee_code, input[name="old_employee_code"]').show();
            }else{
                categoryChage($(this).val(),null);
            }
        });

        $('select[name="source_name"]').on('change', function(event) {
            sourceNameChange($(this).val(), null);
        });

        function categoryChage(val, selected){
            $.ajax({
                url: "{{ url('/regional/sorce-detail') }}",
                data: {category: val},
            })
            .done(function(data) {
                $('select[name="source_name"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="source_name"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
                if(selected){
                    $('select[name="source_name"]').val(selected);
                }
            });
        }

        function sourceNameChange(val, selected){
            $.ajax({
                url: "{{ url('/regional/sorce-code') }}",
                data: {name: val},
            })
            .done(function(data) {
                $('input[name="source_code"]').val('');
                if(data != ''){
                    $('input[name="source_code"]').val(data.source_code);
                }
            });
        }

        var url ="{{ url('/regional/hiring-data') }}";
        var edit_url = "{{ url('/regional/hiring') }}";
        var auth_check = "{{ Auth::check() }}";
        var upload_url = "{{ url('/regional/upload-documents') }}";
        var hold_url = "{{ url('/regional/hold') }}";
        var employee_url = "{{ url('/regional/employee/detail') }}";

        datatable = $('#employee-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                // statusChange();
            },
            columns: [
            /*    { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false}, */
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { data: 'designation',name : 'designation',"searchable": true, "orderable": false},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},
                { data: 'location_code',name : 'location_code',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
                        if(o.status == 'approve' || o.status == 'pending'){
                            return '<label class="label label-danger">Pending<label>';
                        }else if(o.status == 'confirm'){
                            return '<label class="label label-success">Approve<label>';
                        }
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":250,
                    "render": function (o) {
                        var e = v = d = a = b = r = h = u = l = "";

                        if(o.regional_hold == 0){
                            if(((o.store_status == 1 && o.regional_status != 2) || o.hrms_status == 3) && o.regional_status != 4){
                                //approve
                                a = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/approve' data-employee='"+employee_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" data-title='regional' data-date='"+o.joining_date+"' class='btn btn-success btn-sm send_to_hr_from_index' title='Approve'><i class='mdi mdi-redo'></i></a>&nbsp;";

                                //sendback
                                b = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/send-back' value="+o.id+" data-note='"+o.regional_note+"' data-title='Store Manager' data-id="+o.id+" class='btn btn-danger btn-sm regional_send_back' title='Send back'><i class='mdi mdi-undo' ></i></a>&nbsp;";

                                if(o.regional_hold == 0 && o.hrms_status != 2){
                                    h = "<a href='javascript:void(0);' data-url='"+hold_url+"/1/"+o.id+"' value="+o.id+" data-id="+o.id+" data-msg='Hold' class='btn btn-danger btn-sm hold-hiring' title='Hold'><i class='mdi mdi-lock-outline' ></i></a>&nbsp";
                                }

                                if(o.regional_hold == 1){
                                    h = "<a href='javascript:void(0);' data-url='"+hold_url+"/0/"+o.id+"' value="+o.id+" data-id="+o.id+" data-msg='Unhold' class='btn btn-success btn-sm hold-hiring' title='Unhold'><i class='mdi mdi-lock-outline' ></i></a>&nbsp";
                                }
                            }

                            //reject
                            if(o.regional_status != 4){
                                r = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/reject' value="+o.id+" data-title='reject' data-id="+o.id+" class='btn btn-danger btn-sm regional_reject' title='Reject'>x</a>&nbsp;";

                                //edit
                                e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";
								if(o.regional_hold == 0 && o.hrms_status != 2){
                                    h = "<a href='javascript:void(0);' data-url='"+hold_url+"/1/"+o.id+"' value="+o.id+" data-id="+o.id+" data-msg='Hold' class='btn btn-danger btn-sm hold-hiring' title='Hold'><i class='mdi mdi-lock-outline' ></i></a>&nbsp";

									//h = "<a href='"+hold_url+"/1/"+o.id+"' value="+o.id+" data-id="+o.id+" class='btn btn-danger btn-sm' title='Hold'><i class='mdi mdi-lock-outline' ></i></a>&nbsp;";
                                }

                                if(o.regional_hold == 1){
									h = "<a href='javascript:void(0);' data-url='"+hold_url+"/0/"+o.id+"' value="+o.id+" data-id="+o.id+" data-msg='Unhold' class='btn btn-success btn-sm hold-hiring' title='Unhold'><i class='mdi mdi-lock-outline' ></i></a>&nbsp";
                                    //h = "<a href='"+hold_url+"/0/"+o.id+"' value="+o.id+" data-id="+o.id+" class='btn btn-success btn-sm' title='Unhold'><i class='mdi mdi-lock-open' ></i></a>&nbsp;";
                                }
                            }

                            //upload document
                            if(o.hrms_status != 2 && o.regional_status != 4){
                                u = "<a href='"+upload_url+"/"+o.id+"/1' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm' title='Update Document'><i class='mdi mdi-cloud-upload' ></i></a>&nbsp;";
                            }

                            //view
                            v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";

                            // delete
                            d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('regional/hiring')}} data-msg='joinee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";

                            //log
                            l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-success btn-sm' title='View Log' ><i class='mdi mdi-menu' aria-hidden='true'></i></button></a>&nbsp;";
                        }else{
                            h = "<a href='javascript:void(0);' data-url='"+hold_url+"/0/"+o.id+"' value="+o.id+" data-id="+o.id+" data-msg='Unhold' class='btn btn-success btn-sm hold-hiring' title='Unhold'><i class='mdi mdi-lock-outline' ></i></a>&nbsp";
                        }

                        return v+e+u+b+a+r+h+l;
                    }

                }
            ]
        });
		var date = new Date();
		date.setDate(date.getDate());
        $(".datetimepicker1").datetimepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            componentIcon: '.mdi.mdi-calendar',
			startDate: date,
            //pickerPosition: "bottom-left",
            navIcons:{
                rightIcon: 'mdi mdi-chevron-right',
                leftIcon: 'mdi mdi-chevron-left'
            }
        });
</script>


@endsection