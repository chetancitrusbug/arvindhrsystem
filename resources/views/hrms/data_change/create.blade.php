@extends('hrms.layouts.backend')
@section('title',"Search Employee")
@section('css')
    <style type="text/css">
        input[type="file"]{
            border: none;
        }
        .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
                color: #fff !important;
                background-color: #2572f2 !important;
                border-color: transparent;
                border-top-color: #0c57d3 !important;
                box-shadow: inset 0 2px 0 #1266f1 !important;
            }
        .form_label{
            border: none;
            padding: 10px;
            text-align: center;
            font-size: 16px;
            background: #eee;
        }
        .family_label{
            border: none;
            padding: 5px 5px 0px;
        }
        .text-center{
            text-align: center;
        }
        td {
            vertical-align: top !important;
        }
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider clearfix">
                    <div class="tools"></div><span class="title">Search Employee
                        <a href="{!! URL::previous() !!}" title="Back">
                            <button class="btn btn-space btn-warning pull-right">Back</button>
                        </a>
                    </span><span class="panel-subtitle"></span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/hrms/data-change', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                        <div class="form-group col-sm-12{{ $errors->has('employee_id') ? ' has-error' : ''}}">
                            {!! Form::label('employee_id', 'Employee: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::select('employee_id', [''=>'-- Select Employee --']+$employee, null ,['class' => 'form-control input-sm select2']) !!}
                                {!! $errors->first('employee_id', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group col-sm-10">
                            {!! Form::submit('Search', ['class' => 'btn btn-space btn-primary pull-right','style'=>'margin-right:11.3%;']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection