<div id="resignation" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title send_forword_title">Resignation Request Process</h3>
        </div>
        <div class="modal-body form">
            <div class="error"></div>

					<form action="{{url('hrms/resignation')}}/update" method="POST" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                        <input type="hidden" name="rid" value="" id="hidden_res_id">
                        <div class="form-group">
                        <label class="col-sm-4 control-label">Accept or Decline Request</label>
                        <div class="col-sm-6">
                            <div class="be-radio inline">
                            <input type="radio" checked name="resignation_status" value="complete" id="rad6">
                            <label for="rad6">Accept</label>
                            </div>
                        <!--   <div class="be-radio inline">
                            <input type="radio"  name="resignation_status" value="decline" id="rad7">
                            <label for="rad7">Decline</label>
                            </div> -->
                        </div>
                        </div>
                        <!--
                        <div class="form-group">
                            <label class="col-sm-3 control-label">{!! Form::label('', 'Last Working Date: ', ['class' => '']) !!}</label>
                            <div class="col-sm-6">
                                <div data-min-view="2" id="last_working_date" data-date-format="yyyy-mm-dd" class="input-group date ">
                                <input size="16" disabled name="last_working_date" id="lw_date" type="text" value="" class="form-control input-sm input-height-39"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                                </div>
                            </div>
                        </div> -->
                        <!--
                        <div class="form-group">
                        <label class="col-sm-3 control-label">Note</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" name="hrms_note" style="min-width: 100%"></textarea>
                        </div>
                        </div> -->
                        <div class="text-center">
                            <div class="xs-mt-50">
                                <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
                                <button type="submit" data-dismiss="modal" class="btn btn-success submit-resignation">Proceed</button>
                            </div>
                        </div>
					</form>
					<hr/>


            <div class="form-group">


                <table id="abs-table" class="table table-bordered table-hover table-fw-widget" width="70%;">
                    <tbody>

						<tr>
                            <th>Application</th>
                            <td class="application_url"></td>
                            <th>Download Resignation</th>
                            <td><a id="download_resignation" href="#" target='_blank'> Download Resignation </a></td>
                        </tr>
                        <tr>
                            <th width="20%">Employee Code</th>
                            <td width="30%" class="emp_code"></td>
                            <th width="20%">Store IO Code</th>
                            <td width="30%" class="io_code"></td>
                        </tr>
                        <tr>
                            <th>Emp Name</th>
                            <td class="emp_name"></td>
                            <th>Store Name</th>
                            <td class="store_name"></td>
                        </tr>
                        <tr>
                            <th>Designation</th>
                            <td class="designation"></td>
                            <th>Mobile</th>
                            <td class="mobile"></td>
                        </tr>
                        <tr>
                            <th>Date Of joining</th>
                            <td class="doj"></td>
                            <th>Email</th>
                            <td class="email"></td>
                        </tr>
                        <tr>
                            <th>Date Of Resignation</th>
                            <td class="dor"></td>
                            <th>Last Working Day</th>
                            <td class="lwd"></td>
                        </tr>
                        <tr>
                            <th>Name as per Bank</th>
                            <td class="name"></td>
                            <th>Bank A/c No</th>
                            <td class="ac_no"></td>
                        </tr>
                        <tr>
                            <th>Bank Name</th>
                            <td class="bank_name"></td>
                            <th>IFSC Code</th>
                            <td class="ifsc_code"></td>
                        </tr>
                        <tr>
                            <th>House Address for communication</th>
                            <td colspan="3" class="address"></td>
                        </tr>
                    </tbody>
                </table>

            </div>



			<div class="form-group">

			</div>
        </div>

        <div class="modal-footer text-center">

        </div>
    </div>
</div>


