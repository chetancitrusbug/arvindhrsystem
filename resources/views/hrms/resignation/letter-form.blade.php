@extends('storeManager.layouts.backend')
@section('title',"Resignation")
@section('css')
    <style>
        .paging { padding-right: 20px; padding: 0px 10px !important; }
        #attendance-table { width:3000px;}
        .select{
            padding: 6px 2px;
            height: 30px;
            font-size: 12px;
            width: 55px;
        }
        .table-responsive tbody > tr > td {
            width: 100px;
        }
        .table-responsive tbody > tr > td:first-child{
            width: 200px;
        }
        .request-change{
            cursor: pointer;
        }
        #abs-table{
            width: 95%; margin: 0 auto;
            font-size: 10px;
        }
        .error{
            color: red;
        }
		#last_working_date{
			padding: 0px !important;
		}
		.employee_id{
			width: 85%; !important;
		}
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('regional.modals.resignation')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Resignation Requests
                        
                    </div>
                    <div class="panel-body">
                        <table style="background-color:#fff;width:810px;height:100%;padding:25px 25px;font-family:Arial, Helvetica, sans-serif;vertical-align:top;border:1px solid #000;">
        <tr>
            <td>    

			
				<br/>
				<br/>
                <table style="width:100%;padding:0px 0px;border-spacing:0px;">
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;"><p>CLEARANCE LETTER (EMP.CODE - <span style="height: 25px;width: 100px;display: inline-block;line-height: 24px;margin: 0px 0 0 0;border-bottom: 1px solid #222;padding:0;">
						{{$employee->employee_code}}
						</span> )</p></td>
                    </tr>
                </table>

				<br/>
				<br/>
				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;">
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;" colspan="2"><p>PERSONAL BANK A/C DETAILS</p></td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:11px;width:50%;" colspan="1">UNLIMITED</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 10px;font-size:12px;width:50%;" colspan="1"><span style="background-color:#ffff00;">Store IO Code:</span><span> {{$employee->storeLocation->io_code}} </span></td>
                    </tr>
                </table>

                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;border-bottom:1px solid #000;" colspan="8">Personal Information</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Emp Code</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->employee_code}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Store IO Code:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">{{$employee->storeLocation->io_code}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Emp Name:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->full_name}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Store Name:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">{{$employee->storeLocation->store_name}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Designation:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->designation->name}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Reporting to:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">
						@if($resignation->storemanager)
						{{ $resignation->storemanager->emp_name}}
					
						@elseif($resignation->regionmanager)
						{{ $resignation->regionmanager->emp_name}}
						@endif
						</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Date of Joining:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->joining_date}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Mobile:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">{{$employee->mobile}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Date of Resignation:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3"> {{$resignation->created_at->format('Y-m-d') }} </td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">E-mail ID:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3"> {{$employee->email}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Relieving Date:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3"> {{$resignation->last_working_date}} </td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"></td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3"></td>
                    </tr>
                </table><!-- end of Personal info.  -->

				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-top:0px;border-bottom:0px;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;border-bottom:1px solid #000;" colspan="8">Personal Bank Account Details</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Name as per Bank:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->full_name}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Bank A/c No:</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">{{$employee->account_no}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">Bank Name:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="3">{{$employee->bank_name}}</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:20%;border-left:1px solid #222;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">IFSC Code</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:30%;border-bottom:1px solid #000;" colspan="3">{{$employee->ifsc}}</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:23%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">House Address for communication:</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:27%;border-bottom:1px solid #000;" colspan="7">{{$employee->permanent_address_line_1}}, {{$employee->permanent_address_line_2}}, {{$employee->permanent_address_line_3}}</td>
                    </tr>
                </table><!-- end of Personal Details  -->

<br/>
				<br/>
				<br/>
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;margin:40px 0 0 0;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">SL NO.</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">ITEM</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">NO DUE/ DUE</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">REMARKS</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-bottom:1px solid #000;" colspan="1">SIGN</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">I</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">ADMIN</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">1</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Drawer key</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">2</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Personal Courier – Charges</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">3</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Mobile Charges</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">4</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Calculator</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">5</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Cell Phone</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">6</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Car</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">7</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Outstanding Bills</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">8</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Library Books Clearance</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                   

                </table><!-- end of list1 -->
				
				<br/>
				<br/>
				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;margin:40px 0 0 0;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">SL NO.</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">ITEM</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">NO DUE/ DUE</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">REMARKS</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-bottom:1px solid #000;" colspan="1">SIGN</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">II</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">SYSTEMS</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">1</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Personal Computer / Laptop</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">2</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Desktop Passwords</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">3</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">CD Writer/ Multimedia Kit</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">4</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Email ID Disabled</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    
                </table><!-- end of list2 -->

				<br/>
				<br/>
				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;margin:10px 0 0 0;">
                 
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">III</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">FINANCE</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">1</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Advances or Dues</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                </table><!-- end of list3 -->

				<br/>
				<br/>
				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;margin:40px 0 0 0;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">SL NO.</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">ITEM</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">NO DUE/ DUE</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">REMARKS</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-bottom:1px solid #000;" colspan="1">SIGN</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">IV</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">HUMAN RESOURCE</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">1</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">HR Manual/ Induction Manual</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">2</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">House Deposit</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">3</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Custody of Furniture & Fixtures</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">4</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Loans – V.Loan / Hsg Loan / GP Loan</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">5</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">ID Card/ Access Cards</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">6</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Exit Interview Conducted (Organisation Effectiveness)</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    
                </table><!-- end of list4 -->

				<br/>
				<br/>
				
                <table style="width:100%;padding:0px 0px;border-spacing:0px;border:1px solid #222;border-bottom:0px;margin:10px 0 0 0;">
            
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">V</td>
                        <td style="color:#222;font-weight:600;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">RESPECTIVE DEPT/ FUNCTION</td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">1</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Samples / Designs</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">2</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Computer Floppies / CD’s</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">3</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Data Files / Correspondence documents</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">4</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Registers / Manual</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">5</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">SO@AR Form <span style="font-weight:600;">(Signed by Dept Head  & Line HR)</span></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:10%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1">6</td>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 2px;font-size:12px;width:35%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="2">Proper Handing Over has been done and the Immediate Superior & Department/Function is satisfied with the same. (Please attach Copy of the Handing Over Note).</td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:10px 2px;font-size:12px;width:20%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="checkbox"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:1px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                        <td style="color:#222;font-weight:400;text-align:center;padding:0px 0px;font-size:12px;width:15%;border-right:0px solid #222;border-bottom:1px solid #000;" colspan="1"><input type="text" style="height:30px;border:none;padding:2px 5px;"></td>
                    </tr>
                    
                </table><!-- end of list5 -->

				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
                <table style="width:100%;padding:0px 0px;border-spacing:0px;margin:50px 0 10px 0;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:33.3%;" colspan="1"><span style="border-bottom:1px solid #222;">Signature of Immediate Superior</span></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:33.3%;" colspan="1"><span style="border-bottom:1px solid #222;">Signature of Line HR</span></td>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:33.3%;" colspan="1"><span style="border-bottom:1px solid #222;">Signature of Functional Head.</span></td>
                    </tr>
                </table>

                <table style="width:100%;padding:0px 0px;border-spacing:0px;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:100%;" colspan="1"><span style="border-bottom:1px solid #222;font-style:italic;">Please forward this form duly signed to Human resource department one day before relieving the employee.</span></td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:100%;" colspan="1"><span style="border-bottom:1px solid #222;font-style:italic;">Store Address & Seal</span></td>
                    </tr>
                </table>

                <table style="width:100%;padding:20px 0px 0 0;border-spacing:0px;border-top:1px solid #222;">
                    <tr>
                        <td style="color:#222;font-weight:600;text-align:center;padding:10px 10px;font-size:12px;width:100%;" colspan="1"><span style="border-bottom:1px solid #222;background-color:#ffff00;">Handing Over Note</span></td>
                    </tr>
                    
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;" colspan="1">1.	All Files & Formats have been handed over to the New Role Holder/ Immediate Superior.
                            <label style="padding-left: 20px;font-weight:600;"><input type="radio" name="optradio"> <span>Yes</span></label>
                            <label style="padding-left: 20px;font-weight:600;"><input type="radio" name="optradio"> <span>No</span></label>
                        </td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;padding-left:50px;" colspan="1">a.	The following Files/Formats in Hard Copy have been handed over:</td>
                    </tr>
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;padding-left:50px;" colspan="1">b.	The following Files/Formats in Soft Copy have been forwarded/location clearly specified:</td>
                    </tr>
                    
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;line-height:18px;" colspan="1">2.	The status on the following Work in Progress <span style="font-weight:600;">(WIP)</span> has been clearly communicated to the New Role Holder/ Immediate Superior/ Internal Customer.</td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;" colspan="1">3.	The Original SO@AR Form has been handed over to New Role Holder/ Immediate Superior. 
                            <label style="padding-left: 20px;font-weight:600;"><input type="radio" name="optradio"> <span>Yes</span></label>
                            <label style="padding-left: 20px;font-weight:600;"><input type="radio" name="optradio"> <span>No</span></label>
                            <p style="padding:10px 0;font-weight:600;">Please explain in case the same has not been handed over.</p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;line-height:18px;" colspan="1">4.	Remarks from the immediate superior on how the deliverables of this Role will be managed in the immediate days following the exit.</td>
                    </tr>

                    <tr>
                        <td style="color:#222;font-weight:400;text-align:left;padding:10px 10px;font-size:12px;width:100%;line-height:24px;" colspan="1">
                            5.	Comments from the Immediate Superior & Function Head (Are you satisfied with the handing over 
                            <label style="padding-left: 20px;font-weight:600;"><input type="radio" name="optradio"> <span>Yes</span></label>
                            <label style="padding-left: 20px;padding-right: 20px;font-weight:600;"><input type="radio" name="optradio"> <span>No</span></label> 
                            and convinced that the work will go on to everyone’s satisfaction 
                            <label style="padding-left: 20px;font-weight:600;"><input type="radio" name="optradio"> <span>Yes</span></label>
                            <label style="padding-left: 20px;padding-right: 20px;font-weight:600;"><input type="radio" name="optradio"> <span>No</span></label> 
                            Please elaborate).
                            
                        </td>
                    </tr>

                </table>



                
            </td>
        </tr>
    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

