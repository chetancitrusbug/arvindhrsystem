@extends('hrms.layouts.backend')
@section('title',"Resignation")
@section('css')
    <style>
        .paging { padding-right: 20px; padding: 0px 10px !important; }
        #attendance-table { width:3000px;}
        .select{
            padding: 6px 2px;
            height: 30px;
            font-size: 12px;
            width: 55px;
        }
        .table-responsive tbody > tr > td {
            width: 100px;
        }
        .table-responsive tbody > tr > td:first-child{
            width: 200px;
        }
        .request-change{
            cursor: pointer;
        }
        #abs-table{
            width: 95%; margin: 0 auto;
            font-size: 10px;
        }
        .error{
            color: red;
        }
		#last_working_date{
			padding: 0px !important;
		}
		.employee_id{
			width: 85%; !important;
		}
    </style>
@endsection
@section('content')
<div class="be-content">
{{--@include('storeManager.includes.alerts')--}}
    @include('hrms.modals.resignation')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Resignation Requests</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table style="width:100%;" id="resignation-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        <th>Employee Name</th>
                                        <th>Employee Code</th>
                                        <th>Last Working Date</th>
                                        <th>Status</th>
                                        <th>Created</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')

    <script>
		 $("#last_working_date").datetimepicker({
			 minDate: new Date(),
			 maxDate: 'today',
			 autoclose: true,
			 componentIcon: '.mdi.mdi-calendar'
		});

        var url ="{{ url('/hrms/resignation-data') }}";
        var remove_url = "{{ url('/hrms/remove-resignation') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#resignation-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "order": [[0,"desc"]],
            "pageLength": 25,
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                // statusChange();
            },
            columns: [
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
				{ data: 'last_working_date',name : 'last_working_date',"searchable": true, "orderable": true},
				{
					data: null,
					"searchable": true,
					"orderable": true,
					"render" : function (o){
						var status = o.resignation_status
						if(status == 'approved') return 'Approve';
						if(status == 'cancelled') return 'Cancel';
						if(status == 'complete') return 'Complete';
						if(status == 'decline') return 'Decline';
						if(status == 'documentation') return 'Documentation Required';
                        if(status == 'documentation-reg') return 'Documentation Processing To Regional';
						if(status == 'pending') return 'Pending';
						if(status == 'processing-hr') return 'Processing To Hrms';
					}
				},
				{ data: 'created_at',name : 'created_at',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d=r=l=u= "";

                        v = "<a data-url='"+remove_url+"/"+o.id+"' lwd="+o.last_working_date+" data-id="+o.id+" class='process-itema btn btn-warning btn-sm' title='Remove from resignation'>Approve / Decline</a>&nbsp;";

                        return v+e+u;
                    }

                }
            ]
        });

		$(document).on('click', '.process-itema', function (e) {
        	var id = $(this).attr('data-id');
			var lwd = $(this).attr('lwd');
            var download_link = '';

			$("#hidden_res_id").val(id);
			$("#lw_date").val(lwd);

            $("#loading").show();
            $.ajax({
				type: "get",
				url: "{{url('store/resignation')}}/"+id,
				success: function (result) {
					//console.log(data);
					data = result.data.employee;
                    $('.emp_code').html(data.employee_code);
                    $('.io_code').html(data.store_location.io_code);
                    $('.emp_name').html(data.full_name);
                    $('.store_name').html(data.store_location.store_name);
                    $('.designation').html(data.designation.name);
                    $('.mobile').html(data.mobile);
                    $('.doj').html(data.joining_date);
                    $('.email').html(data.email);
                    $('.name').html(data.full_name);
                    $('.ac_no').html(data.account_no);
                    $('.bank_name').html(data.bank_name);
                    $('.ifsc_code').html(data.ifsc);
                    $('.address').html(data.permanent_address_line_1+', '+data.permanent_address_line_2+', '+data.permanent_address_line_3);

					$('#resignation').niftyModal();

					item = result.data.item;

					if(item){
						if(item.application_url && item.application_url != ""){
							var application_url = "<a href='"+item.application_url+"' target='_blank'>Click to view employee application </a>";
							$('.application_url').html(application_url);
						}

                        if(item.reg_agreement_copy && item.reg_agreement_copy != ""){
                            download_link = "{{url('/uploads/resignation')}}" + '/'+item.reg_agreement_copy;
                            $('#download_resignation').attr("href", download_link)
                        }
                        $('.dor').html(item.date_of_resignation);
                        $('.lwd').html(item.last_working_date);
                     //   console.log(item);

					/*	$('.application_status').html(item.resignation_status);
						$('.last_working_date_show').html(item.last_working_date);
						$('.note_from_store_manager').html(item.store_note);
						$('.note_from_regional_manager').html(item.regional_note);*/
						//$('.application_status').html(item.last_working_date);
					}

				},
				error: function (xhr, status, error) {

				}
			});

        });


        $('.submit-resignation').on('click',function(){
           /* url = "{{url('/hrms/resignation')}}";

            $("#loading").show();
            $.ajax({
                type: 'POST',
                url: url,
                data: {employee_id:$('select[name="employee_id"]').val(),last_working_date:$('#lw_date').val()},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
				success: function (data) {
                   // location.reload();
                },
                error: function (xhr, status, error) {
                    $('button[type="submit"]').removeAttr('disabled');
                }
            })
				*/

        });


    </script>
@endsection
