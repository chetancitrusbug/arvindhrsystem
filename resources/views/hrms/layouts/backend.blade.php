<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Arvind HR System') }}</title>

    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/material-design-icons/css/material-design-iconic-font.min.css')}}"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/jqvmap/jqvmap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>

    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/datatables/css/dataTables.bootstrap.min.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/select2/css/select2.min.css')}}" />

    <link rel="stylesheet" href="{{asset('backend/assets/css/style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('backend/assets/css/your-style.css')}}" type="text/css"/>
    @yield('css')
  </head>
  <body>
    <div id="loading">
        <img id="loading-image" src="{{asset('backend/loading.gif')}}" alt="Loading..." />
    </div>
    <div class="be-wrapper be-fixed-sidebar">
        @include('hrms/includes/topNav')
        @include('hrms/includes/sideNav')

        @yield('content')
        @include('hrms/includes/rightSideBar')
        @include('storeManager.modals.backendSucess')
    </div>
    <script src="{{asset('backend/assets/lib/jquery/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/js/main.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
    {{-- <script src="{{asset('backend/assets/lib/datatables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/datatables/js/dataTables.bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/dataTables.buttons.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/buttons.html5.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/buttons.flash.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/buttons.print.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/buttons.colVis.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/datatables/plugins/buttons/js/buttons.bootstrap.js')}}" type="text/javascript"></script>  --}}




    <script src="{{asset('backend/assets/js/app-tables-datatables.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/js/bootstrap-checkbox.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/js/app-form-elements.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/jquery.niftymodals/dist/jquery.niftymodals.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/js/custom.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/js/bootstrap-checkbox.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/assets/lib/select2/js/select2.min.js')}}" type="text/javascript"></script>


    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="//cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js" type="text/javascript"></script>


    <script type="text/javascript">

        $(document).ready(function(){

            setTimeout(function(){
                jQuery('.alert-dismissible').hide();
            }, 3000);

            $("#loading").hide();
            $('.select2').select2();
			var config = {
				assetsPath: 'assets',
				imgPath: 'img',
				jsPath: 'js',
				libsPath: 'lib',
				leftSidebarSlideSpeed: 200,
				leftSidebarToggleSpeed: 300,
				enableSwipe: true,
				swipeTreshold: 100,
				scrollTop: true,
				openRightSidebarClass: 'open-right-sidebar',
				closeRsOnClickOutside: true,
				removeLeftSidebarClass: 'be-nosidebar-left',
				transitionClass: 'be-animate',
				openSidebarDelay: 400
		    };

		    var colors = {};
		    var body = $("body");
		    var wrapper = $(".be-wrapper");
		    var leftSidebar = $(".be-left-sidebar");
		    var rightSidebar = $(".be-right-sidebar");
		    var openSidebar = false;
			var leftSidebar = $(".be-left-sidebar");
			var firstAnchor = $(".sidebar-elements > li > a", leftSidebar);
			var lsc = $(".left-sidebar-scroll", leftSidebar);
			var lsToggle = $(".left-sidebar-toggle", leftSidebar);

			function updateScroller(){
			  if( wrapper.hasClass("be-fixed-sidebar") ){
				lsc.perfectScrollbar('update');
			  }
			}

			/*Open sub-menu functionality*/
			firstAnchor.on("click",function( e ){

				var $el = $(this), $open, $speed = config.leftSidebarSlideSpeed;
				var $li = $el.parent();
				var $subMenu = $el.next();

				$open = $li.siblings(".open");

				if( $open ){

				  $open.find('> ul:visible').slideUp({ duration: $speed, complete: function(){
					$open.toggleClass('open');
					$(this).removeAttr('style');
					updateScroller();
				  }});
				}

				if( $li.hasClass('open') ){

				  $subMenu.slideUp({ duration: $speed, complete: function(){
					$li.toggleClass('open');
					$(this).removeAttr('style');
					updateScroller();
				  }});
				}else{

				  $subMenu.slideDown({ duration: $speed, complete: function(){
					$li.toggleClass('open');
					$(this).removeAttr('style');
					updateScroller();
				  }});
				}

				//If current element has children stop link action
				if( $el.next().is('ul') ){
				  e.preventDefault();
				}

			  });

			/*Calculate sidebar tree active & open classes*/
			  $("li.active", leftSidebar).parents(".parent").addClass("active open");

			/*Scrollbar plugin init when left sidebar is fixed*/
			  if( wrapper.hasClass("be-fixed-sidebar") ){
				lsc.perfectScrollbar();

				/*Update scrollbar height on window resize*/
				$(window).resize(function () {
				  waitForFinalEvent(function(){
					lsc.perfectScrollbar('update');
				  }, 500, "be_update_scroller");
				});
			  }

			/*Toggle sidebar on small devices*/
			  lsToggle.on('click',function( e ){
				var spacer = $(this).next('.left-sidebar-spacer'), toggleBtn = $(this);
				toggleBtn.toggleClass('open');
				spacer.slideToggle(config.leftSidebarToggleSpeed, function(){
				  $(this).removeAttr('style').toggleClass('open');
				});
			  });
        });
    </script>
    @yield('scripts')
    @yield('modal')
  </body>
</html>