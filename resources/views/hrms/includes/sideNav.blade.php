<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Dashboard</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements" role="tablist">
                        <li class="divider">Menu</li>
                        <li class="{{(isset($route) && $route == 'hiring')?'active':''}}"><a href="{{ url('hrms/hiring') }}"><span>Hiring Request</span></a></li>
						<!--<li class="{{(isset($route) && $route == 'separation')?'active':''}}"><a href="{{ url('hrms/separation') }}"><span>Separation</span></a></li> -->
                        <li class="{{(isset($route) && $route == 'data_change')?'active':''}}"><a href="{{ url('hrms/data-change') }}"><span>Data Change</span></a></li>
						
						<li class="parent {{(isset($route) && ($route == 'absconding' || $route == 'resignation' ))?'active open':''}}"><a href="#"><span>Separation Request</span></a>
							<ul class="sub-menu">
								<li class="{{ Request::is('hrms/resignation') ? 'active' : '' }}"><a href="{{ url('hrms/resignation') }}"><span>Resignation Request</span></a></li> 
							</ul>
						</li>
						
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>