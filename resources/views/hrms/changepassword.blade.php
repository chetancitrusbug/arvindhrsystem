@extends('hrms.layouts.backend')
@section('title',"Change Password")
@section('content')
<div class="be-content">

    <div class="form-group col-sm-12{{ Session::has('message') ? ' has-error' : ''}}">
        @if(Session::has('message'))
            <div class ='col-sm-3' ></div>
            <div class="form-group col-sm-6">
                <p class="alert alert-success">{{ Session::get('message') }}</p>
            </div>
            <div class='col-sm-3'></div>
        @endif
        @if(Session::has('error_message'))
            <div class='col-sm-3'></div>
            <div class="form-group col-sm-6 has-error">
                <p class="alert alert-danger">{{ Session::get('error_message') }}</p>
            </div>
            <div class='col-sm-3'></div>
        @endif
    </div>

    <div class="form-group col-sm-12{{ $errors->has('message_error') ? ' has-error' : ''}}">
        {!! $errors->first('message_error', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="main-content container-fluid">

        <div class="col-sm-6">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">HRMS Change Password</div>
                <div class="panel-body">

                {!! Form::open(['url' => route('store_hrpassword_change'), 'class' => 'form-horizontal group-border-dashed','id' => 'change_password_form','autocomplete'=>'off'])
        !!}
                    <div class="form-group xs-pt-10 {{ $errors->has('old_password') ? ' has-error' : ''}}">
                        {!! Form::label('old_password', '* Old Password: ') !!}
                        {!! Form::password('old_password', ['class' => 'form-control input-sm disable']) !!}
                        {!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group xs-pt-10 {{ $errors->has('password') ? ' has-error' : ''}}">
                        {!! Form::label('password', '* New Password: ') !!}
                        {!! Form::password('password', ['class' => 'form-control input-sm disable']) !!}
                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group xs-pt-10 {{ $errors->has('verify_password') ? ' has-error' : ''}}">
                        {!! Form::label('verify_password', '* Confirm Password: ') !!}
                        {!! Form::password('verify_password', ['class' => 'form-control input-sm disable']) !!}
                        {!! $errors->first('verify_password', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="row xs-pt-15">
                        <div class="col-xs-12">
                            <p class="text-right">
                                {!! Form::label('', '', ['class' => 'col-sm-3 control-label text-right']) !!}
                                {!! Form::submit('Change Password', ['class' => 'btn btn-space btn-primary']) !!}
                            </p>
                        </div>
                    </div>

                </form></div>
            </div>
        </div>

    </div>
</div>

@endsection
 @push('scripts')
@endpush @push('modal')
    @include('storeManager.modals.backendSucess')
@endpush