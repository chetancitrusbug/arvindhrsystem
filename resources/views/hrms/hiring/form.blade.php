<div class="form-group {{ $errors->has('designation_id') ? ' has-error' : ''}}">
    {!! Form::label('designation_id', 'Designation: ', ['class' => 'col-sm-3 control-label text-right']) !!}
    <div class="col-sm-6">
        {!! Form::select('designation_id', [''=>'-- Select Designation --']+$designation, null ,['class' => 'form-control select2
        input-sm']) !!}
        {!! $errors->first('designation_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('full_name') ? ' has-error' : ''}}">
    {!! Form::label('full_name', 'Personal Information ', ['class' => 'col-sm-12 lable label-default form_label']) !!}
</div>
<div class="form-group{{ $errors->has('full_name') ? ' has-error' : ''}}">
    {!! Form::label('full_name', '* Full Name (as per Aadhar): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('full_name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('full_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('position') ? ' has-error' : ''}}">
    {!! Form::label('position', '* Position applied for: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('position', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('position', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ ($errors->has('blood_group') || $errors->has('rh_factor')) ? ' has-error' : ''}}">
    {!! Form::label('blood_group', '* Blood Group: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::select('blood_group', [''=>'-- Select Blood Group --']+config('constants.blood_group'), null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('blood_group', '<p class="help-block">:message</p>') !!}
    </div>
    {!! Form::label('rh_factor', '* RH Factor: ', ['class' => 'col-sm-1 control-label']) !!}
    <div class="col-sm-3">
        {!! Form::select('rh_factor', [''=>'-- Select RH Factor --']+config('constants.rh_factor'), null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('rh_factor', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('date_of_birth') ? ' has-error' : ''}}">
    {!! Form::label('date_of_birth', '* Date of Birth (as per Aadhar): ', ['class' => 'col-sm-3 control-label','data-date-format'=>"dd/mm/yyyy"]) !!}
    <div class="col-sm-6 col-xs-12">
        <div data-min-view="2"  class="input-group input-append date datetimepicker1">
            <input size="16" type="text" value="{{old('date_of_birth',isset($employee)?$employee->date_of_birth:null)}}" class="form-control input-sm" data-date-format="dd/mm/yyyy H:i" name="date_of_birth"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
        </div>
        {!! $errors->first('date_of_birth', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('marital_status') ? ' has-error' : ''}}">
    {!! Form::label('marital_status', '* Marital Status: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('marital_status', [''=>'-- Select Marital Status --']+config('constants.marital_status'), null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('marital_status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ ($errors->has('aadhar_status') || $errors->has('aadhar_number')) ? ' has-error' : ''}}">
    {!! Form::label('aadhar_status', '* Availability of Aadhar: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::select('aadhar_status', [''=>'-- Availability of Aadhar --']+config('constants.aadhar_status'), null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('aadhar_status', '<p class="help-block">:message</p>') !!}
    </div>
    {!! Form::label('aadhar_number', 'Aadhar Number: ', ['class' => 'col-sm-2 control-label aadhar_number']) !!}
    <div class="col-sm-2">
        {!! Form::text('aadhar_number', null, ['class' => 'form-control input-sm aadhar_number']) !!}
        {!! $errors->first('aadhar_number', '<p class="help-block aadhar_number">:message</p>') !!}
    </div>
</div>
<div class="form-group aadhar_enrolment_number {{ $errors->has('aadhar_enrolment_number') ? ' has-error' : ''}}">
    {!! Form::label('aadhar_enrolment_number', 'Aadhar Enrolment Number: ', ['class' => 'col-sm-3 control-label aadhar_enrolment_number']) !!}
    <div class="col-sm-6">
        {!! Form::text('aadhar_enrolment_number', null, ['class' => 'form-control input-sm aadhar_enrolment_number']) !!}
        {!! $errors->first('aadhar_enrolment_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('nationality') ? ' has-error' : ''}}">
    {!! Form::label('nationality', '*Nationality: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('nationality', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('nationality', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('gender') ? ' has-error' : ''}}">
    {!! Form::label('gender', '* Gender (as per Aadhar): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('gender', [''=>'-- Select Gender --']+config('constants.gender'), null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ ($errors->has('pan_status') || $errors->has('pan_number')) ? ' has-error' : ''}}">
    {!! Form::label('pan_status', '* Availability of PAN: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-2">
        {!! Form::select('pan_status', [''=>'-- Availability of PAN --']+config('constants.pan_status'), null ,['class' => 'form-control input-sm']) !!}
        {!! $errors->first('pan_status', '<p class="help-block">:message</p>') !!}
    </div>
    {!! Form::label('pan_number', 'PAN:', ['class' => 'col-sm-2 control-label pan_number']) !!}
    <div class="col-sm-2">
        {!! Form::text('pan_number', null, ['class' => 'form-control input-sm pan_number']) !!}
        {!! $errors->first('pan_number', '<p class="help-block pan_number">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('mobile') ? ' has-error' : ''}}">
    {!! Form::label('mobile', '* Mobile Number: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('mobile', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', 'Personal Email Id: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('email', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('esic_number') ? ' has-error' : ''}}">
    {!! Form::label('esic_number', 'ESIC Number: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('esic_number', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('esic_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('emergency_person') ? ' has-error' : ''}}">
    {!! Form::label('emergency_person', '* Emergency Contact person name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('emergency_person', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('emergency_person', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('emergency_number') ? ' has-error' : ''}}">
    {!! Form::label('emergency_number', '* Emergency Contact number: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('emergency_number', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('emergency_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('emergency_relationship') ? ' has-error' : ''}}">
    {!! Form::label('emergency_relationship', '* Emergency Contact Relationship: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('emergency_relationship', [''=>'-- Select Relation --']+config('constants.relationship'), null ,['class' => 'form-control input-sm']) !!}
        {{-- {!! Form::text('emergency_relationship', null, ['class' => 'form-control input-sm']) !!} --}}
        {!! $errors->first('emergency_relationship', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('anniversary_date') ? ' has-error' : ''}}">
    {!! Form::label('anniversary_date', 'Wedding anniversary date: ', ['class' => 'col-sm-3 control-label','data-date-format'=>"dd/mm/yyyy"]) !!}
    <div class="col-sm-6 col-xs-12">
        <div data-min-view="2"  class="input-group date datetimepicker1">
            <input size="16" type="text" value="{{old('anniversary_date',isset($employee)?$employee->anniversary_date:null)}}" class="form-control input-sm" data-date-format="dd/mm/yyyy H:i" name="anniversary_date"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
        </div>
        {!! $errors->first('anniversary_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('first_name') ? ' has-error' : ''}}">
    {!! Form::label('first_name', '* First Name (for email id purpose): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('first_name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', '* Last Name (for email id purpose): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('last_name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('ua_number') ? ' has-error' : ''}}">
    {!! Form::label('ua_number', 'Universal Account Number (UAN): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('ua_number', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('ua_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('first_time_employment') ? ' has-error' : ''}}">
    {!! Form::label('first_time_employment', '* FIRST TIME EMPLOYMENT (✔ as Applicable): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::checkbox('first_time_employment',1, isset($employee->first_time_employment)?$employee->first_time_employment:1, ['class'=>['first_time_employment']]) !!}
        {!! $errors->first('first_time_employment', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Current Address', 'Current Address', ['class' => 'col-sm-12 lable label-default form_label']) !!}
</div>
<div class="form-group{{ $errors->has('current_address_line_1') ? ' has-error' : ''}}">
    {!! Form::label('current_address_line_1', '* Address Line 1: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('current_address_line_1', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('current_address_line_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('current_address_line_2') ? ' has-error' : ''}}">
    {!! Form::label('current_address_line_2', 'Address Line 2: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('current_address_line_2', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('current_address_line_2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('current_address_line_3') ? ' has-error' : ''}}">
    {!! Form::label('current_address_line_3', 'Address Line 3: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('current_address_line_3', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('current_address_line_3', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('current_state') ? ' has-error' : ''}}">
    {!! Form::label('current_state', '* State: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('current_state', [''=>'-- Select State --']+$states, null ,['class' => 'form-control select2 input-sm']) !!}
        {!! $errors->first('current_state', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('current_city') ? ' has-error' : ''}}">
    {!! Form::label('current_city', '* City: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('current_city', [''=>'-- Select city --']+$currentCity, null ,['class' => 'form-control select2 input-sm disable']) !!}
        {!! $errors->first('current_city', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('current_pincode') ? ' has-error' : ''}}">
    {!! Form::label('current_pincode', '* Pincode: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('current_pincode', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('current_pincode', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('Permanent Address', 'Permanent Address', ['class' => 'col-sm-12 lable label-default form_label']) !!}
</div>
<div class="form-group{{ $errors->has('permanent_address_line_1') ? ' has-error' : ''}}">
    {!! Form::label('permanent_address_line_1', '* Address Line 1: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('permanent_address_line_1', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('permanent_address_line_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('permanent_address_line_2') ? ' has-error' : ''}}">
    {!! Form::label('permanent_address_line_2', 'Address Line 2: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('permanent_address_line_2', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('permanent_address_line_2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('permanent_address_line_3') ? ' has-error' : ''}}">
    {!! Form::label('permanent_address_line_3', 'Address Line 3: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('permanent_address_line_3', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('permanent_address_line_3', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('permanent_state') ? ' has-error' : ''}}">
    {!! Form::label('permanent_state', '* State: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('permanent_state', [''=>'-- Select State --']+$states, null ,['class' => 'form-control select2 input-sm disable']) !!}
        {!! $errors->first('permanent_state', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('permanent_city') ? ' has-error' : ''}}">
    {!! Form::label('permanent_city', '* City: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('permanent_city', [''=>'-- Select city --']+$permanentCity, null ,['class' => 'form-control select2 input-sm disable']) !!}
        {!! $errors->first('permanent_city', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('permanent_pincode') ? ' has-error' : ''}}">
    {!! Form::label('permanent_pincode', '* Pincode: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('permanent_pincode', null, ['class' => 'form-control input-sm disable']) !!}
        {!! $errors->first('permanent_pincode', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('education Details', 'Education Details ', ['class' => 'col-sm-12 lable label-default form_label']) !!}
</div>

<div class="table-responsive table-responsive2">
    <table class="table table-condensed table-bordered">
        <tr>
            <th style="width: 15%;">Category</th>
            <th>Board / University</th>
            <th>Name of the Institution</th>
            <th>Qualification Name</th>
            <th>Specialization</th>
            <th colspan="2">Month & Year of passing</th>
            {{-- <th class="actions" align="center"><button class="btn btn-success" id="add"><i class="mdi mdi-plus"></i></button></th> --}}
        </tr>
        <tbody id="dynamic_field">
            @if($errors->has('education'))
                <tr>
                    <td colspan="8" class="has-error">
                        {!! $errors->first('education', '<p class="help-block">:message</p>') !!}
                    </td>
                </tr>
            @endif
            @php
                $education = old('education',isset($employee->education)?$employee->education:[]);
            @endphp
            @if(count($education))
                @foreach($education as $key=>$value)
                    <tr class="row{{ $key }}" data-row="{{ $key }}">
                        <td class='{{ $errors->has("education.$key.education_category_id") ? "has-error" : ""}}'>
                            {!! Form::select("education[$key][education_category_id]", [''=>'-- education category --']+$educationCategory, $value['education_category_id'] ,['class' => 'form-control input-sm disable']) !!}
                            @if(isset($value['id']) && !empty($value['id']))
                                {!! Form::hidden("education[$key][id]", $value['id'], ['class' => 'form-control input-sm']) !!}
                            @endif
                            {!! $errors->first("education.$key.education_category_id", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.board_uni") ? "has-error" : ""}}'>
                            {!! Form::text("education[$key][board_uni]", $value['board_uni'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.board_uni", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.institute") ? "has-error" : ""}}'>
                            {!! Form::text("education[$key][institute]", $value['institute'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.institute", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.qualification") ? "has-error" : ""}}'>
                            {!! Form::text("education[$key][qualification]", $value['qualification'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.qualification", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.specialization") ? "has-error" : ""}}'>
                            {!! Form::text("education[$key][specialization]", $value['specialization'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.specialization", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.month") ? "has-error" : ""}}'>
                            {!! Form::select("education[$key][month]", [''=>'-- Month --']+config('constants.month_list'), $value['month'] ,['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.month", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("education.$key.year") ? "has-error" : ""}}'>
                            {!! Form::select("education[$key][year]", [''=>'-- Year --']+config('constants.year_list'), $value['year'] ,['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("education.$key.year", '<p class="help-block">:message</p>') !!}
                        </td>
                        {{-- <td align="center"><button class="btn btn-danger remove disable" data-row='{{ $key }}'><i class="mdi mdi-minus"></i></button></td> --}}
                    </tr>
                @endforeach
            @else
                @for ($i = 0; $i < 5; $i++)
                    <tr class="row1" data-row="1">
                        <td>
                            {!! Form::select("education[$i][education_category_id]", [''=>'-- education category --']+$educationCategory, null ,['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("education[$i][board_uni]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("education[$i][institute]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("education[$i][qualification]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("education[$i][specialization]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::select("education[$i][month]", [''=>'-- Month --']+config('constants.month_list'), null ,['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::select("education[$i][year]", [''=>'-- Year --']+config('constants.year_list'), null ,['class' => 'form-control input-sm']) !!}
                        </td>
                        {{-- <td align="center"><button class="btn btn-danger remove" data-row='1'><i class="mdi mdi-minus"></i></button></td> --}}
                    </tr>
                @endfor
            @endif
        </tbody>
    </table>
</div>
<div class="form-group">
    {!! Form::label('Previous Work Experience(s)', 'Previous Work Experience(s) ', ['class' => 'col-sm-12 lable label-default form_label']) !!}
</div>
<div class="table-responsive table-responsive2">
    <table class="table table-condensed table-bordered">
        <tr>
            <th style="width: 15%;">Company Name</th>
            <th style="width: 10%;">Date Of Joining (DD/MM/YYYY)</th>
            <th style="width: 10%;">Date Of Leaving (DD/MM/YYYY)</th>
            <th style="width: 15%;">Nature of Employment</th>
            <th>Last held Designation</th>
            <th>Reason for leaving</th>
            {{-- <th class="actions" align="center"><button class="btn btn-success disable" id="add_2"><i class="mdi mdi-plus"></i></button></th> --}}
        </tr>
        <tbody id="experience_field">
            @php
                $previous = old('previous',isset($employee->previous)?$employee->previous:[]);
            @endphp
            @if(count($previous))
                @foreach($previous as $key=>$value)
                    <tr class="experience{{ $key }}" data-row="{{ $key }}">
                        <td class='{{ $errors->has("previous.$key.company_name") ? "has-error" : ""}}'>
                            {!! Form::text("previous[$key][company_name]", $value['company_name'], ['class' => 'form-control input-sm disable']) !!}
                            @if(isset($value['id']) && !empty($value['id']))
                                {!! Form::hidden("previous[$key][id]", $value['id'], ['class' => 'form-control input-sm disable']) !!}
                            @endif
                            {!! $errors->first("previous.$key.company_name", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("previous.$key.joining_date") ? "has-error" : ""}}'>
                            <div data-min-view="2"  class="input-group {{ !(isset($is_employee) && $is_employee ==1)?'date datetimepicker1':'' }}">
                                {!! Form::text("previous[$key][joining_date]", $value['joining_date'], ['class' => 'form-control input-sm disable']) !!}
                                <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            </div>
                            {!! $errors->first("previous.$key.joining_date", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("previous.$key.leaving_date") ? "has-error" : ""}}'>
                            <div data-min-view="2"  class="input-group {{ !(isset($is_employee) && $is_employee ==1)?'date datetimepicker1':'' }}">
                                {!! Form::text("previous[$key][leaving_date]", $value['leaving_date'], ['class' => 'form-control input-sm disable']) !!}
                                <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            </div>
                            {!! $errors->first("previous.$key.leaving_date", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("previous.$key.nature_of_employment_id") ? "has-error" : ""}}'>
                            {!! Form::select("previous[$key][nature_of_employment_id]", [''=>'-- Nature Of Employment --']+$natureOfEmployment, $value['nature_of_employment_id'] ,['class' => 'form-control input-sm disable']) !!}
                            {!! $errors->first("previous.$key.nature_of_employment_id", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("previous.$key.designation") ? "has-error" : ""}}'>
                            {!! Form::text("previous[$key][designation]", $value['designation'], ['class' => 'form-control input-sm disable']) !!}
                            {!! $errors->first("previous.$key.designation", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("previous.$key.reason_of_leaving") ? "has-error" : ""}}'>
                            {!! Form::text("previous[$key][reason_of_leaving]", $value['reason_of_leaving'], ['class' => 'form-control input-sm disable']) !!}
                            {!! $errors->first("previous.$key.reason_of_leaving", '<p class="help-block">:message</p>') !!}
                        </td>
                        {{-- <td align="center"><button class="btn btn-danger experience_remove disable" data-row='{{ $key }}'><i class="mdi mdi-minus"></i></button></td> --}}
                    </tr>
                @endforeach
            @else
                @for ($i = 0; $i < 5; $i++)
                    <tr class="experience1" data-row="1">
                        <td>
                            {!! Form::text("previous[$i][company_name]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            <div data-min-view="2"  class="input-group date datetimepicker1">
                                {!! Form::text("previous[$i][joining_date]", null, ['class' => 'form-control input-sm']) !!}
                                <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            </div>
                        </td>
                        <td>
                            <div data-min-view="2"  class="input-group date datetimepicker1">
                                {!! Form::text("previous[$i][leaving_date]", null, ['class' => 'form-control input-sm']) !!}
                                <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            </div>
                        </td>
                        <td>
                            {!! Form::select("previous[$i][nature_of_employment_id]", [''=>'-- Nature Of Employment --']+$natureOfEmployment, null ,['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("previous[$i][designation]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("previous[$i][reason_of_leaving]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        {{-- <td align="center"><button class="btn btn-danger experience_remove" data-row='1'><i class="mdi mdi-minus"></i></button></td> --}}
                    </tr>
                @endfor
            @endif
        </tbody>
    </table>
</div>
<div class="form-group">
    {!! Form::label('Family Information - (For Mediclaim / ESIC / PF)', 'Family Information - (For Mediclaim / ESIC / PF) ', ['class' => 'col-sm-12 lable label-default form_label']) !!}
</div>

<div class="table-responsive table-responsive2">
    <table class="table table-condensed table-bordered">
        <tr class="text-center">
            <th style="width: 15%;">Relationship</th>
            <th>Gender</th>
            <th>Aadhar Number</th>
            <th>Name</th>
            <th style="width: 15%;">Date of birth as per Aadhar</th>
        </tr>
        <tbody>
            @php
                $family = old('family',isset($employee->family)?$employee->family:[]);
            @endphp
            @if(count($family))
                @foreach($family as $key=>$value)
                    <tr>
                        <td>
                            {!! Form::label($value['relation'], $value['relation'], ['class' => 'col-sm-12 lable label-default family_label']) !!}
                            {!! Form::hidden("family[$key][relation]", $value['relation'], ['class' => 'form-control input-sm']) !!}
                            @if(isset($value['id']) && !empty($value['id']))
                                {!! Form::hidden("family[$key][id]", $value['id'], ['class' => 'form-control input-sm']) !!}
                            @endif
                        </td>
                        <td>
                            {!! Form::select("family[$key][gender]", [''=>'-- gender --']+config('constants.gender'), $value['gender'] ,['class' => 'form-control input-sm']) !!}
                        </td>
                        <td class='{{ $errors->has("family.$key.aadhar_number") ? "has-error" : ""}}'>
                            {!! Form::text("family[$key][aadhar_number]", $value['aadhar_number'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("family.$key.aadhar_number", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("family.$key.name") ? "has-error" : ""}}'>
                            {!! Form::text("family[$key][name]", $value['name'], ['class' => 'form-control input-sm']) !!}
                            {!! $errors->first("family.$key.name", '<p class="help-block">:message</p>') !!}
                        </td>
                        <td class='{{ $errors->has("family.$key.date") ? "has-error" : ""}}'>
                            <div data-min-view="2"  class="input-group date datetimepicker1">
                                {!! Form::text("family[$key][date]", ($value['date'] != '--' && $value['date'] != '//')?$value['date']:'', ['class' => 'form-control input-sm']) !!}
                                <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            </div>
                            {!! $errors->first("family.$key.date", '<p class="help-block">:message</p>') !!}
                        </td>
                    </tr>
                @endforeach
            @else
                @foreach(config('constants.relation_ship') as $key=>$value)
                    <tr>
                        <td>
                            {!! Form::label($value, $value, ['class' => 'col-sm-12 lable label-default family_label']) !!}
                            {!! Form::hidden("family[$key][relation]", $value, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::select("family[$key][gender]", [''=>'-- gender --']+config('constants.gender'), null ,['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("family[$key][aadhar_number]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            {!! Form::text("family[$key][name]", null, ['class' => 'form-control input-sm']) !!}
                        </td>
                        <td>
                            <div data-min-view="2"  class="input-group date datetimepicker1">
                                {!! Form::text("family[$key][date]", null, ['class' => 'form-control input-sm']) !!}
                                <span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>

<script type="text/template" id="add_more">
    <tr class="rowkey" data-row="key">
        <td>
            {!! Form::select('education[key][education_category_id]', [''=>'-- education category --']+$educationCategory, null ,['class' => 'form-control input-sm']) !!}
        </td>
        <td>
            {!! Form::text('education[key][board_uni]', null, ['class' => 'form-control input-sm']) !!}
        </td>
        <td>
            {!! Form::text('education[key][institute]', null, ['class' => 'form-control input-sm']) !!}
        </td>
        <td>
            {!! Form::text('education[key][qualification]', null, ['class' => 'form-control input-sm']) !!}
        </td>
        <td>
            {!! Form::text('education[key][specialization]', null, ['class' => 'form-control input-sm']) !!}
        </td>
        <td>
            {!! Form::select("education[key][month]", [''=>'-- Month --']+config('constants.month_list'), null ,['class' => 'form-control input-sm']) !!}
        </td>
        <td>
            {!! Form::select("education[key][year]", [''=>'-- Year --']+config('constants.year_list'), null ,['class' => 'form-control input-sm']) !!}
        </td>
        <td align="center"><button class="btn btn-danger remove" data-row='key'><i class="mdi mdi-minus"></i></button></td>
    </tr>
</script>

<script type="text/template" id="add_more2">
    <tr class="experiencekey" data-row="key">
        <td>
            {!! Form::text('previous[key][company_name]', null, ['class' => 'form-control input-sm']) !!}
        </td>
        <td>
            <div data-min-view="2"  class="input-group date datetimepicker1">
                <input size="16" type="text" value="{{old('date_of_birth',isset($employee)?$employee->date_of_birth:null)}}" class="form-control input-sm" data-date-format="dd-mm-yyyy H:i" name="previous[key][joining_date]"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
            </div>
        </td>
        <td>
            <div data-min-view="2"  class="input-group date datetimepicker1">
                <input size="16" type="text" value="{{old('date_of_birth',isset($employee)?$employee->date_of_birth:null)}}" class="form-control input-sm" data-date-format="dd-mm-yyyy H:i" name="previous[key][leaving_date]"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
            </div>
        </td>
        <td>
            {!! Form::select('previous[key][nature_of_employment_id]', [''=>'-- Nature Of Employment --']+$natureOfEmployment, null ,['class' => 'form-control input-sm']) !!}
        </td>
        <td>
            {!! Form::text('previous[key][designation]', null, ['class' => 'form-control input-sm']) !!}
        </td>
        <td>
            {!! Form::text('previous[key][reason_of_leaving]', null, ['class' => 'form-control input-sm']) !!}
        </td>
        <td align="center"><button class="btn btn-danger experience_remove" data-row='key'><i class="mdi mdi-minus"></i></button></td>
    </tr>
</script>
<div class="form-group{{ $errors->has('hrms_note') ? ' has-error' : ''}}">
    {!! Form::label('hrms_note', '* Note: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('hrms_note', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('hrms_note', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if($employee->regional_status == 4)
    <div class="form-group{{ $errors->has('reject_reason') ? ' has-error' : ''}}">
        {!! Form::label('reject_reason', 'Reject Reason: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('reject_reason', null, ['class' => 'form-control input-sm','disabled']) !!}
            {!! $errors->first('reject_reason', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif
<div class="form-group{{ $errors->has('remarks') ? ' has-error' : ''}}">
    {!! Form::label('remarks', '* Remarks: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('remarks', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('remarks', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@section('scripts')
    <script>
        $('#add').on('click',function(e){
            e.preventDefault();
            var last;
            $.each($('#dynamic_field tr'), function(index, val) {
                last = $(this).data('row');
            });
            content = $('#add_more').html();
            content = content.replace(/key/g, parseInt(last)+1);
            $('#dynamic_field').append(content);
            $('.select2').select2();
        });

        $(document).on('click','.remove',function(e){
            e.preventDefault();
            row = $(this).data('row');
            if($('#dynamic_field tr').length > 1){
                $('.row'+row).remove();
            }
        });

        $('#add_2').on('click',function(e){
            e.preventDefault();
            var last;
            $.each($('#experience_field tr'), function(index, val) {
                last = $(this).data('row');
            });
            content = $('#add_more2').html();
            content = content.replace(/key/g, parseInt(last)+1);
            $('#experience_field').append(content);
            $('.select2').select2();
            datePicker();
        });

        $(document).on('click','.experience_remove',function(e){
            e.preventDefault();
            row = $(this).data('row');
            if($('#experience_field tr').length > 1){
                $('.experience'+row).remove();
            }
        });

        datePicker();
        function datePicker(){
            $(".datetimepicker1").datetimepicker({
                autoclose: true,
                format: 'dd/mm/yyyy',
                componentIcon: '.mdi.mdi-calendar',
            /*    pickerPosition: "bottom-left", */
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });
        }

        $('select[name="current_state"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/store/city-list') }}",
                data: {state_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="current_city"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="current_city"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });

        $('select[name="permanent_state"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/store/city-list') }}",
                data: {state_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="permanent_city"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="permanent_city"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });

        panDisplay();
        aadharDisplay();
        $('select[name="pan_status"]').on('change',function(){
            panDisplay();
        });

        $('select[name="aadhar_status"]').on('change',function(){
            aadharDisplay();
        });

        function panDisplay(){
            $('.pan_number').hide();
            if($('select[name="pan_status"]').val() == 'available'){
                $('.pan_number').show();
            }
        }

        function aadharDisplay(){
            $('.aadhar_number').hide();
            $('.aadhar_enrolment_number').hide();
            if($('select[name="aadhar_status"]').val() == 'available'){
                $('.aadhar_number').show();
                $('.aadhar_enrolment_number').hide();
            }

            if($('select[name="aadhar_status"]').val() == 'not_available'){
                $('.aadhar_number').hide();
                $('.aadhar_enrolment_number').show();
            }
        }
    </script>
@endsection