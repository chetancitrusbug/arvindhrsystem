@extends('hrms.layouts.backend')
@section('title',"Create Joinee")
@section('css')
    <style type="text/css">
        input[type="file"]{
            border: none;
        }
        .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
                color: #fff !important;
                background-color: #2572f2 !important;
                border-color: transparent;
                border-top-color: #0c57d3 !important;
                box-shadow: inset 0 2px 0 #1266f1 !important;
            }
        .form_label{
            border: none;
            padding: 10px;
            text-align: center;
            font-size: 16px;
            background: #eee;
        }
        .family_label{
            border: none;
            padding: 5px 5px 0px;
        }
        .text-center{
            text-align: center;
        }
        td {
            vertical-align: top !important;
        }
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Create New Joinee
                    <span class="panel-subtitle">
                        <a href="{{ url('/hrms/hiring') }}" title="Back">
                            <button class="btn btn-space btn-primary  btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => '/hrms/hiring', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        @include ('hrms.hiring.form')

                        <div class="form-group col-sm-4">
                            {!! Form::submit('Create', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection