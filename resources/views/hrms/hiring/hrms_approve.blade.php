@extends('hrms.layouts.backend')
@section('title',"Approve Application")
@section('css')
    <style type="text/css">
        input[type="file"]{
            border: none;
        }
        .btn-primary,.btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus {
                color: #fff !important;
                background-color: #2572f2 !important;
                border-color: transparent;
                border-top-color: #0c57d3 !important;
                box-shadow: inset 0 2px 0 #1266f1 !important;
            }
        .form_label{
            border: none;
            padding: 10px;
            text-align: center;
            font-size: 16px;
            background: #eee;
        }
        .family_label{
            border: none;
            padding: 5px 5px 0px;
        }
        .text-center{
            text-align: center;
        }
        td {
            vertical-align: top !important;
        }
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    <div class="main-content container-fluid">
        <div class="col-md-12">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Approve Application
                    <span class="panel-subtitle">
                        <a href="{{ url('/hrms/hiring') }}" title="Back">
                            <button class="btn btn-space btn-primary btn-back">Back</button>
                        </a>
                    </span>
                </div>
                <div class="panel-body">
                    {!! Form::open(['url' => 'hrms/hiring/'.$id.'/approve', 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                        @csrf

                        <div class="form-group{{ $errors->has('employee_code') ? ' has-error' : ''}}">
							@php
								$employee_code = $employee->employee_code;
								if($employee_code == ''){
									$employee_code = $employee->aadhar_number;
								}
							@endphp
                            {!! Form::label('employee_code', '* Employee Code: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('employee_code',old('employee_code',$employee_code) , ['class' => 'form-control input-sm employee_code']) !!}
                                {!! $errors->first('employee_code', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('appointment_letter') ? ' has-error' : ''}}">
                            {!! Form::label('appointment_letter', 'Appointment Letter: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-5">
                                {!! Form::file('appointment_letter', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
                                {!! $errors->first('appointment_letter', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('designation_id') ? ' has-error' : ''}}">
                            {!! Form::label('designation_id', '* Designation: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::select('designation_id', [''=>'-- Select Designation --']+$designation, old('designation_id',$employee->designation_id) ,['class' => 'form-control input-sm'])
                                !!}
                                {!! $errors->first('designation_id', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('ctc') ? ' has-error' : ''}}">
                            {!! Form::label('ctc', '* CTC: ', ['class' => 'col-sm-3 control-label']) !!}
                            <small class="text-danger">* Amount in actual figures</small>
                            <div class="col-sm-6">
                                {!! Form::number('ctc', $employee->ctc, ['class' => 'form-control input-sm employee_code']) !!}
                                {!! $errors->first('ctc', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('esic_number') ? ' has-error' : ''}}">
                            {!! Form::label('esic_number', 'ESIC Team No: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('esic_number', $employee->esic_number, ['class' => 'form-control input-sm employee_code']) !!}
                                {!! $errors->first('esic_number', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('id_card_issue') ? ' has-error' : ''}}">
                            {!! Form::label('id_card_issue', 'ID card issue: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('id_card_issue', null, ['class' => 'form-control input-sm employee_code']) !!}
                                {!! $errors->first('id_card_issue', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('bank_name') ? ' has-error' : ''}} {{ $errors->has('new_bank_name') ? ' has-error' : ''}}">
                            {!! Form::label('bank_name', '* Bank Name: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::select('bank_name', [''=>'-- Select Bank --']+$banks+['other'=>'other'], null,['class' => 'form-control input-sm','id'=>'sel_bank_name']) !!}
                                {!! $errors->first('bank_name', '<p class="help-block">:message</p>') !!}

                                {{-- {!! Form::text('bank_name', null, ['class' => 'form-control input-sm employee_code']) !!} --}}
                                {!! Form::text('new_bank_name', null, ['class' => 'form-control input-sm employee_code hide','Placeholder'=>"Enter Other bank name",'id'=>"new_bank_name"]) !!}
                                {!! $errors->first('new_bank_name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('ifsc') ? ' has-error' : ''}}">
                            {!! Form::label('ifsc', '* IFSC Code: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('ifsc', null, ['class' => 'form-control input-sm employee_code']) !!}
                                {!! $errors->first('ifsc', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('account_no') ? ' has-error' : ''}}">
                            {!! Form::label('account_no', '* Account No: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('account_no', null, ['class' => 'form-control input-sm employee_code']) !!}
                                {!! $errors->first('account_no', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('branch_name') ? ' has-error' : ''}}">
                            {!! Form::label('branch_name', '* Branch Name: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('branch_name', null, ['class' => 'form-control input-sm employee_code']) !!}
                                {!! $errors->first('branch_name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="form-group col-sm-9">
                            {!! Form::submit('Approve', ['class' => 'btn btn-space btn-primary pull-right']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    if($("#sel_bank_name").val() == 'other'){
        $("#new_bank_name").removeClass("hide");
    } else {
        $("#new_bank_name").addClass("hide");
    }
    $("#sel_bank_name").change(function(e) {
        if($(this).val() == 'other'){
            $("#new_bank_name").removeClass("hide");
        }else {
            $("#new_bank_name").addClass("hide");
        }
    });
</script>

@endsection