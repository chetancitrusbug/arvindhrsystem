@extends('hrms.layouts.backend')
@section('title',"Hiring")
@section('css')
    <style type="text/css">
        .hide{
            display: none;
        }
        .panel-table tr td:last-child {
            padding-right: 0px;
        }
    </style>
@endsection
@section('content')
<div class="be-content">
    @include('storeManager.includes.alerts')
    @include('storeManager.modals.confirmation_index')
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">Hiring List</div>
                    <div class="panel-body" style="padding:20px;">

                        <div class="form-group col-md-12">
                            <div class="col-md-11">
                                <form method="GET">
                                    <label for="datepicker_from">Start Date</label>
                                    <input type="text" id="datepicker_from" value="{{$start->format('d/m/Y')}}" name="start_date" />
                                    <label for="datepicker_to">End Date</label>
                                    <input type="text" id="datepicker_to" value="{{$end->format('d/m/Y')}}" name="end_date" />
                                    <button id="btn_filter" class="btn btn-primary">Filter</button>
                                </form>
                            </div>
							<!--
                            <div class="col-md-1">
                                <a href="{{ url('hrms/hiring/export-excle').'?start_date='.$start->format('d/m/Y').'&end_date='.$end->format('d/m/Y') }}" title="Export Hiring Data" class="btn btn-primary">Export Excle</a>
                            </div> -->
                        </div>
                        <div class="table-responsive responsive-table1 col-md-12">
                            <table style="width:100%;" id="employee-table" class="table table-striped table-hover table-fw-widget">
                                <thead>
                                    <tr>
                                        {{-- <th>Employee Code</th>  --}}
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Store Name</th>
                                        <th>Location</th>
                                        <th>Status</th>
                                        {{-- <th>Request Date</th> --}}
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $('.source_name').on('change', function(event) {
            $.ajax({
                url: "{{ url('/hrms/sorce-detail') }}",
                data: {name: $(this).val()},
            })
            .done(function(data) {
                $('.source_code, .source_category').val('');
                if(data != ''){
                    $('.source_code').val(data.source_code);
                    $('.source_category').val(data.category);
                }
            });
        });
        $("#datepicker_from").datetimepicker({
            autoclose: true,
            minView: 2,
            format: 'dd/mm/yyyy',
            maxDate: new Date(),
            endDate: new Date(),
        });
        $("#datepicker_to").datetimepicker({
            autoclose: true,
            minView: 2,
            format: 'dd/mm/yyyy',
            maxDate: new Date(),
            endDate: new Date(),
        });
        $('#btn_filter').on('click', function(event) {
            $start_date = $("#datepicker_from").val();
            $end_date = $("#datepicker_to").val();
            if($start_date != '' && $end_date != ''){
                var redirect = "{{url('hrms/hiring')}}?start_date="+$start_date+"&end_date="+$end_date;
                window.location.href = redirect;
            }else{
                if($start_date == ''){
                    $("#datepicker_from").focus();
                } else {
                    $("#datepicker_to").focus();
                }

            }

        });


        var url ="{{ url('/hrms/hiring-data') }}";
        var edit_url = "{{ url('/hrms/hiring') }}";
        var auth_check = "{{ Auth::check() }}";
        var upload_url = "{{ url('/hrms/upload-documents') }}";
        var filter_date = '';
        if($("#datepicker_from").val() != '' && $("#datepicker_to").val() != ''){
            filter_date = "?start_date="+$("#datepicker_from").val()+"&end_date="+$("#datepicker_to").val();
            url = url+filter_date
        }

        datatable = $('#employee-table').DataTable({
            //dom: 'Blfrtip',
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
			 "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
            "order": [[0,"desc"]],
            "pageLength": 25,
            //buttons: ['excel'],
            "scrollY": 480, "scrollX": true,
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
           /*     { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false}, */
                { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                { data: 'designation',name : 'designation',"searchable": true, "orderable": false},
                { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},
                { data: 'location_code',name : 'location_code',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": false,
                    "class":'capitalize',
                    "render": function (o) {
                        if(o.status == 'approve' || o.status == 'pending'){
                            return '<label class="label label-danger">Pending<label>';
                        }else if(o.status == 'confirm'){
                            return '<label class="label label-success">Approve<label>';
                        }
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":180,
                    "render": function (o) {
                        var e = v = d = a = b = u = "";
						//view
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";
						//log
						l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-success btn-sm' title='View Log' ><i class='mdi mdi-menu' aria-hidden='true'></i></button></a>&nbsp;";

						if(o.regional_hold == 0){
							if(o.regional_status == 2 && o.hrms_status != 2){
								//approve
								a = "<a href='"+edit_url+"/"+o.id+"/approve_view' value="+o.id+" data-id="+o.id+" data-title='hrms' data-date='"+o.joining_date+"' class='btn btn-success btn-sm' title='Approve'><i class='mdi mdi-redo' ></i></a>&nbsp;";

								//sendback
								b = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/send-back' value="+o.id+" data-note='"+o.hrms_note+"' data-title='Regional HR' data-id="+o.id+" class='btn btn-danger btn-sm regional_send_back' title='Send back'><i class='mdi mdi-undo' ></i></a>&nbsp;";
							}

							//upload document
							u = "<a href='"+upload_url+"/"+o.id+"/1' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm' title='Update Document'><i class='mdi mdi-cloud-upload' ></i></a>&nbsp;";



							//edit
							e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";

							//delete
							d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('hrms/hiring')}} data-msg='joinee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";


						}

                        return v+e+u+b+a+l;
                    }

                }
            ]
        });
        /* Filter Date Datatable */
        /*

        $(function() {
            var url ="{{ url('/hrms/hiring-data') }}";
            var edit_url = "{{ url('/hrms/hiring') }}";
            var auth_check = "{{ Auth::check() }}";
            var upload_url = "{{ url('/hrms/upload-documents') }}";
    var oTable = $('#employee-table').DataTable({
        "oLanguage": {
        "sSearch": "Filter Data"
        },
        "iDisplayLength": -1,
        "sPaginationType": "full_numbers",
        dom:
                "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
                processing: false,
                serverSide: true,
                "caseInsensitive": false,
                "lengthMenu": [[6, 10, 25, 50, -1], [6, 10, 25, 50, "All"]],
                "order": [[0,"desc"]],
                "pageLength": 25,
                "scrollY": 480, "scrollX": true,
                ajax: {
                    url:url,
                    type:"get",
                },
                "drawCallback": function( settings ) {
                    statusChange();
                },
                columns: [
                  { data: 'employee_code',name : 'employee_code',"searchable": true, "orderable": false},
                    { data: 'full_name',name : 'full_name',"searchable": true, "orderable": false},
                    { data: 'designation',name : 'designation',"searchable": true, "orderable": false},
                    { data: 'store_name',name : 'store_name',"searchable": true, "orderable": false},
                    { data: 'location_code',name : 'location_code',"searchable": true, "orderable": false},
                    {
                        "data": null,
                        "name" : 'status',
                        "searchable": false,
                        "orderable": false,
                        "class":'capitalize',
                        "render": function (o) {
                            if(o.status == 'approve' || o.status == 'pending'){
                                return '<label class="label label-danger">Pending<label>';
                            }else if(o.status == 'confirm'){
                                return '<label class="label label-success">Approve<label>';
                            }
                        }
                    },
                    { data: 'updated_at',name : 'updated_at',"searchable": true, "orderable": true},
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "width":180,
                        "render": function (o) {
                            var e = v = d = a = b = u = "";
                            //view
                            v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-warning btn-sm' title='View' ><i class='mdi mdi-eye' ></i></button></a>&nbsp;";
                            //log
                            l = "<a href='"+edit_url+"/logs/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-success btn-sm' title='View Log' ><i class='mdi mdi-menu' aria-hidden='true'></i></button></a>&nbsp;";

                            if(o.regional_hold == 0){
                                if(o.regional_status == 2 && o.hrms_status != 2){
                                    //approve
                                    a = "<a href='"+edit_url+"/"+o.id+"/approve_view' value="+o.id+" data-id="+o.id+" data-title='hrms' data-date='"+o.joining_date+"' class='btn btn-success btn-sm' title='Approve'><i class='mdi mdi-redo' ></i></a>&nbsp;";

                                    //sendback
                                    b = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/send-back' value="+o.id+" data-note='"+o.hrms_note+"' data-title='Regional HR' data-id="+o.id+" class='btn btn-danger btn-sm regional_send_back' title='Send back'><i class='mdi mdi-undo' ></i></a>&nbsp;";
                                }

                                //upload document
                                u = "<a href='"+upload_url+"/"+o.id+"/1' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm' title='Update Document'><i class='mdi mdi-cloud-upload' ></i></a>&nbsp;";



                                //edit
                                e = "<a href='javascript:void(0);' data-url='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" data-msg='joinee' class='btn btn-info btn-sm edit-employee'><i class='mdi mdi-edit' ></i></a>&nbsp;";

                                //delete
                                d = "<a href='javascript:void(0);' class='btn btn-danger btn-sm del-item' data-id="+o.id+" data-url={{url('hrms/hiring')}} data-msg='joinee' data-backdrop='static' data-keyboard='false'><i class='mdi mdi-delete' aria-hidden='true'></i></a>&nbsp;";


                            }

                            return v+e+u+b+a+l;
                        }

                    }
                ]
    });




  $("#datepicker_from").datetimepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      minDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    minDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

  $("#datepicker_to").datetimepicker({
    showOn: "button",
    buttonImage: "images/calendar.gif",
    buttonImageOnly: false,
    "onSelect": function(date) {
      maxDateFilter = new Date(date).getTime();
      oTable.fnDraw();
    }
  }).keyup(function() {
    maxDateFilter = new Date(this.value).getTime();
    oTable.fnDraw();
  });

});

// Date range filter
minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
  function(oSettings, aData, iDataIndex) {
    if (typeof aData._date == 'undefined') {
      aData._date = new Date(aData[0]).getTime();
    }

    if (minDateFilter && !isNaN(minDateFilter)) {
      if (aData._date < minDateFilter) {
        return false;
      }
    }

    if (maxDateFilter && !isNaN(maxDateFilter)) {
      if (aData._date > maxDateFilter) {
        return false;
      }
    }

    return true;
  }
); */



</script>


@endsection