@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('backend/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"
    />
@endsection
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Employee Full Name (As Per Aadhar card): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('joining_date') ? ' has-error' : ''}}">
    {!! Form::label('joining_date', '* Date of joining: ', ['class' => 'col-sm-3 control-label','data-date-format'=>"dd/mm/yyyy"]) !!}
    <div class="col-sm-6 col-xs-12">
        <div data-min-view="2"  class="input-group date datetimepicker1">
            <input size="16" type="text" value="{{old('joining_date',isset($employee)?$employee->joining_date:null)}}" class="form-control input-sm" data-date-format="dd/mm/yyyy H:i" name="joining_date"><span class="input-group-addon btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></span>
        </div>
        {!! $errors->first('joining_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('grade_id') ? ' has-error' : ''}}">
    {!! Form::label('grade_id', '* Grade: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('grade_id', [''=>'-- Select Grade --']+$grades, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('grade_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('designation_id') ? ' has-error' : ''}}">
    {!! Form::label('designation_id', '* Designation: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('designation_id', [''=>'-- Select Designation --']+$designation, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('designation_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('department_id') ? ' has-error' : ''}}">
    {!! Form::label('department_id', '* Department: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('department_id', [''=>'-- Select Department --']+$departments, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('department_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('sub_department_id') ? ' has-error' : ''}}">
    {!! Form::label('sub_department_id', '* Sub Department: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('sub_department_id', [''=>'-- Select Sub Department --']+$subDepartments, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('sub_department_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('business_unit_id') ? ' has-error' : ''}}">
    {!! Form::label('business_unit_id', '* Business Unit: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('business_unit_id', [''=>'-- Select Business Unit --']+$businessUnits, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('business_unit_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('reporting_manager_employee_code') ? ' has-error' : ''}}">
    {!! Form::label('reporting_manager_employee_code', '* Reporting Manager Employee Code: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('reporting_manager_employee_code', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('reporting_manager_employee_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('reporting_manager_name') ? ' has-error' : ''}}">
    {!! Form::label('reporting_manager_name', '* Reporting Manager Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('reporting_manager_name', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('reporting_manager_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('brand_id') ? ' has-error' : ''}}">
    {!! Form::label('brand_id', '* Brand: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('brand_id', [''=>'-- Select Brand --']+$brands, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('brand_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('legal_entity_id') ? ' has-error' : ''}}">
    {!! Form::label('legal_entity_id', '* Legal Entity: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('legal_entity_id', [''=>'-- Select Legal Entity --']+$legalEntity, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('legal_entity_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('location_code') ? ' has-error' : ''}}">
    {!! Form::label('location_code', '* Location Code: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('location_code', isset($locationCode->location_code)?$locationCode->location_code:$employee->location_code, ['class' => 'form-control input-sm','disabled']) !!}
        {!! Form::hidden('location_code', isset($locationCode->location_code)?$locationCode->location_code:$employee->location_code) !!}
        {!! $errors->first('location_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('io_code') ? ' has-error' : ''}}">
    {!! Form::label('io_code', '* Cost Centre / IO Code: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('io_code', isset($locationCode->io_code)?$locationCode->io_code:$employee->io_code, ['class' => 'form-control input-sm','disabled']) !!}
        {!! Form::hidden('io_code', isset($locationCode->io_code)?$locationCode->io_code:$employee->io_code) !!}
        {!! $errors->first('io_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('store_name') ? ' has-error' : ''}}">
    {!! Form::label('store_name', '* Store Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('store_name', isset($locationCode->store_name)?$locationCode->store_name:$employee->store_name, ['class' => 'form-control input-sm','disabled']) !!}
        {!! Form::hidden('store_name', isset($locationCode->store_name)?$locationCode->store_name:$employee->store_name) !!}
        {!! $errors->first('store_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('employee_classification_id') ? ' has-error' : ''}}">
    {!! Form::label('employee_classification_id', '* Employee Classification: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('employee_classification_id', [''=>'-- Select Employee Classification --']+$employeeClassification, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('employee_classification_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('source_name') ? ' has-error' : ''}}">
    {!! Form::label('source_name', '* Source Name: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('source_name', [''=>'-- Select Source Name --']+$sourceName, null ,['class' => 'form-control select2 input-sm select2']) !!}
        {!! $errors->first('source_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('source_category') ? ' has-error' : ''}}">
    {!! Form::label('source_category', '* Source Category: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('source_category', null, ['class' => 'form-control input-sm','disabled']) !!}
        {!! Form::hidden('source_category', null) !!}
        {!! $errors->first('source_category', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('source_code') ? ' has-error' : ''}}">
    {!! Form::label('source_code', '* Source Code: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('source_code', null, ['class' => 'form-control input-sm','disabled']) !!}
        {!! Form::hidden('source_code', null) !!}
        {!! $errors->first('source_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('employee_refferal_amount') ? ' has-error' : ''}}">
    {!! Form::label('employee_refferal_amount', '* Employee refferal amount to be paid: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('employee_refferal_amount', null, ['class' => 'form-control input-sm checkNumber']) !!}
        {!! $errors->first('employee_refferal_amount', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('employee_refferal_payment_month') ? ' has-error' : ''}}">
    {!! Form::label('employee_refferal_payment_month', '* Employee referral payout month: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('employee_refferal_payment_month', [''=>'-- Select Employee referral payout month --']+config('constants.month_list'), null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('employee_refferal_payment_month', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('ctc') ? ' has-error' : ''}}">
    {!! Form::label('ctc', '* CTC: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('ctc', null, ['class' => 'form-control input-sm checkNumber']) !!}
        {!! $errors->first('ctc', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('variable_pay_type_id') ? ' has-error' : ''}}">
    {!! Form::label('variable_pay_type_id', 'Variable Pay Type: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('variable_pay_type_id', [''=>'-- Select Variable Pay Type --']+$variablePayType, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('variable_pay_type_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('soft_copy_attached') ? ' has-error' : ''}}">
    {!! Form::label('soft_copy_attached', '* Completed soft copy of Joining kit attached: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('soft_copy_attached', [''=>'-- Select Soft Copy Attached --']+config('constants.soft_copy_attached'), null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('soft_copy_attached', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('pod_no') ? ' has-error' : ''}}">
    {!! Form::label('pod_no', '* POD No: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::text('pod_no', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('pod_no', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group scanned_photo {{ $errors->has('attach.scanned_photo') ? ' has-error' : ''}}">
    {!! Form::label('attach[scanned_photo]', '* Scanned Photo (jpg format Only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[scanned_photo]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.scanned_photo', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['scanned_photo']['file']) && !empty($employee->attach['scanned_photo']['file']))
            <div class="scanned_photo-attach"><img src="{{ asset($employee->attach['scanned_photo']['file']) }}" class="changeImage"></div>
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('region_id') ? ' has-error' : ''}}">
    {!! Form::label('region_id', '* Region: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('region_id', [''=>'-- Select Region --']+$region, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('region_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('state_id') ? ' has-error' : ''}}">
    {!! Form::label('state_id', '* State: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('state_id', [''=>'-- Select State --']+$states, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('state_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('city_id') ? ' has-error' : ''}}">
    {!! Form::label('city_id', '* City: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('city_id', [''=>'-- Select city --']+$city, null ,['class' => 'form-control input-sm select2']) !!}
        {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group employment_form {{ $errors->has('attach.employment_form') ? ' has-error' : ''}}">
    {!! Form::label('attach[employment_form]', '* Employement form attachment (xls,xlsx,csv format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[employment_form]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.employment_form', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['employment_form']['file']) && !empty($employee->attach['employment_form']['file']))
            <div class="employment_form-attach case"><a href="{{ url('/').'/'.$employee->attach['employment_form']['file'] }}" download="{{ $employee->attach['employment_form']['file'] }}" title="download Employement form attachment"><i class="mdi mdi-case-download"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group form_11 {{ $errors->has('attach.form_11') ? ' has-error' : ''}}">
    {!! Form::label('attach[form_11]', '* Form-11 (pdf format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[form_11]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.form_11', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['form_11']['file']) && !empty($employee->attach['form_11']['file']))
            <div class="form_11-attach case"><a href="{{ url('/').'/'.$employee->attach['form_11']['file'] }}" download="{{ $employee->attach['form_11']['file'] }}"><i class="mdi mdi-case-download" title="download Form-11"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group form_02 {{ $errors->has('attach.form_02') ? ' has-error' : ''}}">
    {!! Form::label('attach[form_02]', '* Form-02 (pdf format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[form_02]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.form_02', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['form_02']['file']) && !empty($employee->attach['form_02']['file']))
            <div class="form_02-attach case"><a href="{{ url('/').'/'.$employee->attach['form_02']['file'] }}" download="{{ $employee->attach['form_02']['file'] }}"><i class="mdi mdi-case-download" title="download Form-02"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group form_f {{ $errors->has('attach.form_f') ? ' has-error' : ''}}">
    {!! Form::label('attach[form_f]', '* Form-F (pdf format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[form_f]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.form_f', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['form_f']['file']) && !empty($employee->attach['form_f']['file']))
            <div class="form_f-attach case"><a href="{{ url('/').'/'.$employee->attach['form_f']['file'] }}" download="{{ $employee->attach['form_f']['file'] }}"><i class="mdi mdi-case-download" title="download Form-F"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group hiring_checklist {{ $errors->has('attach.hiring_checklist') ? ' has-error' : ''}}">
    {!! Form::label('attach[hiring_checklist]', '* Hiring Checklist (pdf format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[hiring_checklist]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.hiring_checklist', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['hiring_checklist']['file']) && !empty($employee->attach['hiring_checklist']['file']))
            <div class="hiring_checklist-attach case"><a href="{{ url('/').'/'.$employee->attach['hiring_checklist']['file'] }}" download="{{ $employee->attach['hiring_checklist']['file'] }}"><i class="mdi mdi-case-download" title="download Hiring Checklist"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group inteview_assessment {{ $errors->has('attach.inteview_assessment') ? ' has-error' : ''}}">
    {!! Form::label('attach[inteview_assessment]', '* Interview Assessment (pdf format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[inteview_assessment]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.inteview_assessment', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['inteview_assessment']['file']) && !empty($employee->attach['inteview_assessment']['file']))
            <div class="inteview_assessment-attach case"><a href="{{ url('/').'/'.$employee->attach['inteview_assessment']['file'] }}" download="{{ $employee->attach['inteview_assessment']['file'] }}"><i class="mdi mdi-case-download" title="download Interview Assessment"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group resume {{ $errors->has('attach.resume') ? ' has-error' : ''}}">
    {!! Form::label('attach[resume]', '* Resume (pdf format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[resume]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.resume', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['resume']['file']) && !empty($employee->attach['resume']['file']))
            <div class="resume-attach case"><a href="{{ url('/').'/'.$employee->attach['resume']['file'] }}" download="{{ $employee->attach['resume']['file'] }}"><i class="mdi mdi-case-download" title="download Resume"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group education_documents {{ $errors->has('attach.education_documents') ? ' has-error' : ''}}">
    {!! Form::label('attach[education_documents]', '* Education Documents (pdf, jpg, jpeg, zip format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        <input type="file" name="attach[education_documents][]" multiple="multiple" class="form-control input-sm logo">
        {!! $errors->first('attach.education_documents', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['education_documents']) && !empty($employee->attach['education_documents']))
            @foreach($employee->attach['education_documents'] as $key=>$value)
                <div class="education_documents-attach case"><a href="{{ url('/').'/'.$value['file'] }}" download="{{ $value['file'] }}"><i class="mdi mdi-case-download" title="download Education Documents"></i></a></div>
            @endforeach
        @endif
    </div>
</div>
<div class="form-group aadhar_card {{ $errors->has('attach.aadhar_card') ? ' has-error' : ''}}">
    {!! Form::label('attach[aadhar_card]', '* Aadhar Card (pdf, jpg, jpeg format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[aadhar_card]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.aadhar_card', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['aadhar_card']['file']) && !empty($employee->attach['aadhar_card']['file']))
            <div class="aadhar_card-attach case"><a href="{{ url('/').'/'.$employee->attach['aadhar_card']['file'] }}" download="{{ $employee->attach['aadhar_card']['file'] }}"><i class="mdi mdi-case-download" title="download Aadhar Card"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group pan_card {{ $errors->has('attach.pan_card') ? ' has-error' : ''}}">
    {!! Form::label('attach[pan_card]', '* Pan Card (pdf, jpg, jpeg format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[pan_card]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.pan_card', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['pan_card']['file']) && !empty($employee->attach['pan_card']['file']))
            <div class="pan_card-attach case"><a href="{{ url('/').'/'.$employee->attach['pan_card']['file'] }}" download="{{ $employee->attach['pan_card']['file'] }}"><i class="mdi mdi-case-download" title="download Pan Card"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group bank_passbook {{ $errors->has('attach.bank_passbook') ? ' has-error' : ''}}">
    {!! Form::label('attach[bank_passbook]', '* Bank Passbook/Cheque (pdf, jpg, jpeg format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[bank_passbook]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.bank_passbook', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['bank_passbook']['file']) && !empty($employee->attach['bank_passbook']['file']))
            <div class="bank_passbook-attach case"><a href="{{ url('/').'/'.$employee->attach['bank_passbook']['file'] }}" download="{{ $employee->attach['bank_passbook']['file'] }}"><i class="mdi mdi-case-download" title="download Bank Passbook/Cheque"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group offer_accept {{ $errors->has('attach.offer_accept') ? ' has-error' : ''}}">
    {!! Form::label('attach[offer_accept]', '* offer letter acceptancey mail (pdf format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        {!! Form::file('attach[offer_accept]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)']) !!}
        {!! $errors->first('attach.offer_accept', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['offer_accept']['file']) && !empty($employee->attach['offer_accept']['file']))
            <div class="offer_accept-attach case"><a href="{{ url('/').'/'.$employee->attach['offer_accept']['file'] }}" download="{{ $employee->attach['offer_accept']['file'] }}"><i class="mdi mdi-case-download" title="download offer letter acceptancey mail"></i></a></div>
        @endif
    </div>
</div>
<div class="form-group previous_company_docs {{ $errors->has('attach.previous_company_docs') ? ' has-error' : ''}}">
    {!! Form::label('attach[previous_company_docs]', '* Previouse company offer letter / payslips / releiving letter (pdf, jpg, jpeg,zip format only): ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-5">
        <input type="file" name="attach[previous_company_docs][]" multiple="multiple" class="form-control input-sm logo">
        {{-- {!! Form::file('attach[previous_company_docs][]', null, ['class' => 'form-control input-sm logo','onchange'=>'changeImage(1)','multiple'=>'multiple']) !!} --}}
        {!! $errors->first('attach.previous_company_docs', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="col-sm-1">
        @if(isset($employee->attach['previous_company_docs']) && !empty($employee->attach['previous_company_docs']))
            @foreach($employee->attach['previous_company_docs'] as $key=>$value)
                <div class="previous_company_docs-attach case"><a href="{{ url('/').'/'.$value['file'] }}" download="{{ $value['file'] }}"><i class="mdi mdi-case-download" title="download Previouse company offer letter / payslips / releiving letter"></i></a></div>
            @endforeach
        @endif
    </div>
</div>
<div class="form-group{{ $errors->has('hrms_note') ? ' has-error' : ''}}">
    {!! Form::label('hrms_note', '* Note: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::textarea('hrms_note', null, ['class' => 'form-control input-sm']) !!}
        {!! $errors->first('hrms_note', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@section('scripts')
    <script src="{{asset('backend/assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script>
        /* $(".form_datetime").datetimepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
        }); */
        $(".datetimepicker1").datetimepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            componentIcon: '.mdi.mdi-calendar',
            navIcons:{
                rightIcon: 'mdi mdi-chevron-right',
                leftIcon: 'mdi mdi-chevron-left'
            }
        });

        //model dependency on department
        $('select[name="department_id"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/hrms/sub-department-list') }}",
                data: {department_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="sub_department_id"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="sub_department_id"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });

        $('select[name="region_id"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/hrms/state-list') }}",
                data: {region_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="state_id"] option:not(:first)').remove();
                $('select[name="city_id"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="state_id"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });

        $('select[name="state_id"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/hrms/city-list') }}",
                data: {state_id: $(this).val()},
            })
            .done(function(data) {
                $('select[name="city_id"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="city_id"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
            });
        });

        //model dependency on source name
        $('select[name="source_name"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/hrms/sorce-detail') }}",
                data: {name: $(this).val()},
            })
            .done(function(data) {
                $('input[name="source_code"], input[name="source_category"]').val('');
                if(data != ''){
                    $('input[name="source_code"]').val(data.source_code);
                    $('input[name="source_category"]').val(data.category);
                }
            });
        });

        //model dependency on department
        $('select[name="location_code"]').on('change', function(event) {
            $.ajax({
                url: "{{ url('/hrms/location-detail') }}",
                data: {locationCode: $(this).val()},
            })
            .done(function(data) {
                $('input[name="io_code"], input[name="store_name"]').val('');
                if(data != ''){
                    $('input[name="io_code"]').val(data.io_code); 
                    $('input[name="store_name"]').val(data.store_name);
                }
            });
        });

        $('input[name="attach[scanned_photo]"]').on('change',function(){
            $('.changeImage').hide();
        });
    </script>
@endsection