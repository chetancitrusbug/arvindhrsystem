@extends('hrms.layouts.backend')

@section('content')
      <div class="be-content">
        <div class="main-content container-fluid">
          <h2 class="text-center"> Hrms Manager Loggin</h2>
          
        </div>
      </div>
      <button data-toggle="modal" id="success" data-target="#md-footer-success" type="button" style="display:hidden" class="btn btn-space">Success</button>
      
@endsection

@push('scripts')
      <?php if(Session::has('flash_success') &&  Session::get('flash_success') !='' ){
      ?>
      <script>
        
        $(document).ready(function(){
          $('document').find('#success').trigger('click');
          $('#success').click(function(){
            alert('here');
          })
        });
      </script>
    <?php } ?>
@endpush

@push('modal')
  @include('hrms.modals.backendSucess')
@endpush
