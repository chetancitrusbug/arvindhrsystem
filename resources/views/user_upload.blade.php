@extends('layouts.user')
@section('content')
<style>
    .has-error{ color:red; }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    <!--@if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif You are logged in! -->
                    {!! Form::open([ 'class' => 'form-horizontal group-border-dashed','id' => 'module_form','autocomplete'=>'off', 'files'=>true]) !!}
                    <div class="form-group{{ $errors->has('employee_code') ? ' has-error' : ''}}">
                        {!! Form::label('employee_code', '* Employee Code: ', ['class' => 'col-sm-12 control-label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('employee_code', old('employee_code',''), ['class' => 'form-control input-sm employee_code'])
                            !!} {!! $errors->first('employee_code', '
                            <p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('employee_name') ? ' has-error' : ''}}">
                        {!! Form::label('employee_name', '* Employee Name: ', ['class' => 'col-sm-12 control-label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('employee_name', old('employee_name',''), ['readonly'=>true,'class' => 'form-control input-sm employee_name']) !!} {!! $errors->first('employee_name','<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('employee_email') ? ' has-error' : ''}}">
                        {!! Form::label('employee_email', '* Email Address: ', ['class' => 'col-sm-12 control-label']) !!}
                        <div class="col-sm-12">
                            {!! Form::email('employee_email', old('employee_email',''), ['readonly'=>true,'class' => 'form-control input-sm employee_email']) !!} {!! $errors->first('employee_email','<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('idea_name') ? ' has-error' : ''}}">
                        {!! Form::label('idea_name', 'Idea Name: ', ['class' => 'col-sm-12 control-label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('idea_name', old('idea_name',''), ['class' => 'form-control input-sm idea_name']) !!} {!! $errors->first('idea_name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('idea_category') ? ' has-error' : ''}}">
                        {!! Form::label('idea_category', '* Idea Category: ', ['class' => 'col-sm-12 control-label']) !!}
                        <div class="col-sm-12">
                            {!! Form::select('idea_category', [''=>'-- Select Idea Category --']+$idea_category, null ,['class' => 'form-control input-sm
                            ']) !!} {!! $errors->first('idea_category', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
                        {!! Form::label('description', '* Description: ', ['class' => 'col-sm-12 control-label']) !!}
                        <div class="col-sm-12">
                            {!! Form::textarea('description', old('description',''), ['class' => 'form-control input-sm description']) !!} {!! $errors->first('description',
                            '
                            <p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('expected_out_come') ? ' has-error' : ''}}">
                        {!! Form::label('expected_out_come', '* Expected Out Come: ', ['class' => 'col-sm-12 control-label']) !!}
                        <div class="col-sm-12">
                            {!! Form::text('expected_out_come', old('expected_out_come',''), ['class' => 'form-control input-sm expected_out_come']) !!} {!! $errors->first('expected_out_come','
                            <p
                                class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('document') ? ' has-error' : ''}}">
                        {!! Form::label('document', 'File Upload: ', ['class' => 'col-sm-12 control-label']) !!}
                        <div class="col-sm-12">
                            {{Form::file('document', ['class' => 'form-control input-sm employee_code'])}}
                            {!! $errors->first('document','<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{Form::submit('Submit',['class'=>'btn btn-primary'])}}
                        </div>
                    </div>

                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $("#employee_code").focusout(function() {
            var employee_code = $("#employee_code").val();

            if(employee_code != ''){
                var url = "{{url('getuserdata')}}/"+employee_code;
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        if(data) {
                            $("#employee_email").val(data.email);
                            $("#employee_name").val(data.full_name);
                        } else {
                            alert("Employee Code not Match");
                            $("#employee_email").val("");
                            $("#employee_name").val("");
                            console.log(data);
                        }
                    },
                    error: function(data) {
                        $("#employee_email").val("");
                        $("#employee_name").val("");
                        alert("Employee Code not Match");
                    }
                });
            }
        });
    </script>
@endsection


