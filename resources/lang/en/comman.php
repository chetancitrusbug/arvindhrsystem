<?php

return [


    'datatable' =>[
        "emptyTable"=> "No data available in table",
        "infoEmpty"=>"Showing 0 to 0 of 0 entries",
        'search' => 'Search',
        'show' => 'Show',
        'entries' => 'Entries',
        'showing' => 'Showing', // Showing _START_ to _END_ of _TOTAL_ entries
        'to' => 'to',
        'of' => 'of',
        'small_entries' => 'entries',
        'paginate' => [
            'next' => 'Next',
            'previous' => 'Previous',
            'first'=>'First',
            'last'=>'Last',
        ],

    ],
    'daterange' =>[
        'all'=>'All',
        'today'=>'Today',
        'yesterday'=>'Yesterday',
        'last7day'=>'Last 7 Days',
        'last30day'=>'Last 30 Days',
        'thismonth'=>'This Month',
        'lastmonth'=>'Last Month',
        'thisyear'=>'This Year',
        'lastyear'=>'Last Year',
        'customeRange'=>'Custome Range',
        'applyBtn'=>'Apply',
        'cancelBtn'=>'Cancle',
    ],
   
    'month_names'=>[
        "January", 
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ],
    'month_sort_names'=>[
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
    ]


];
