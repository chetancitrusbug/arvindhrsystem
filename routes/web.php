<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Auth\LoginController@showLoginForm');
Route::get('/power-of-unlimiting','UserUploadController@showForm');
Route::post('/power-of-unlimiting','UserUploadController@upload_files')->name('upload-user-file');
Route::get('/thankyou','UserUploadController@thankyou')->name('upload-user-file');
Route::get('/getuserdata/{employee_code}','UserUploadController@getuserdata');
Route::get('CronRun', 'Admin\HiringReportController@CronRun');
Route::get('CronRun/absconding', 'Admin\HiringReportController@CronRunAbsconding');
Route::get('CronRun/hiringReportset', 'Admin\HiringReportController@CronHiringReports');

Auth::routes();
Route::post('login', 'Auth\LoginController@authenticate')->name('login');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin' , 'as' => 'admin','middleware' => ['auth','redirectCheck']], function () {
    Route::get('/', 'Admin\AdminController@index');
    Route::get('manpower-report/region', 'Admin\ManPowerController@manPowerRegion');
    Route::get('manpower-report/region-export', 'Admin\ManPowerController@exportmanpower');
    Route::get('manpower-budget/region-export', 'Admin\ManPowerController@manPowerbudgetRegionExport');

    Route::get('manpower-cost-report/region', 'Admin\AdminController@manPowerCostRegion');
    Route::get('hygiene-report/region', 'Admin\AdminController@hygieneRegion');
    Route::get('hygiene-report/region-wise-store/{id}', 'Admin\AdminController@hygieneStore');
    Route::get('/hygiene-data/{id}', 'Admin\AdminController@hygieneDatatable');
    Route::get('manpower-cost-report/region-export', 'Admin\AdminController@avgcostExport');



	Route::get('/manpowercost-report/region-wise-store/{id}','Admin\AdminController@manPowerCostRegionStore');
	Route::get('/manpowercost-report/region-wise-store-data/{id}','Admin\AdminController@manPowerCostRegionStoreData');

    Route::resource('roles', 'Admin\RolesController');
    Route::get('/roles-data', 'Admin\RolesController@datatable');
    Route::get('/employee/logs/{id}', 'Admin\EmployeeController@logView');
    Route::get('/hiring/logs/{id}', 'Admin\HiringController@logView');
    Route::get('/employee-log/{id}', 'Admin\EmployeeController@log');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::get('/permissions-data', 'Admin\PermissionsController@datatable');
    Route::get('users/import', 'Admin\UsersController@viewSroreManager');
    Route::post('users/import', 'Admin\UsersController@importSroreManager');
    Route::resource('users', 'Admin\UsersController');
    Route::resource('pages', 'Admin\PagesController');
    Route::resource('settings', 'Admin\SettingsController');

    //employee route
    Route::resource('hiring', 'Admin\HiringController');
    Route::get('/hiring-data', 'Admin\HiringController@datatable');
    Route::resource('employee', 'Admin\EmployeeController');
    Route::get('/employee-data', 'Admin\EmployeeController@datatable');
    Route::get('sub-department-list', 'Admin\EmployeeController@getSubDepartments');
    Route::get('city-list', 'Admin\EmployeeController@getCityFromState');
    Route::get('sorce-detail', 'Admin\EmployeeController@getSourceDetail');
    Route::get('show-employee-upload', 'Admin\EmployeeController@showUpload');
    Route::post('employeeUpload', 'Admin\EmployeeController@upload');

    //store route
    Route::resource('store-location', 'Admin\StoreLocationController');
    Route::get('/store-location-data', 'Admin\StoreLocationController@datatable');
    Route::get('store-location-upload', 'Admin\StoreLocationController@showUpload');
    Route::post('storeLocationUpload', 'Admin\StoreLocationController@upload');

    //designation route
    Route::resource('designation', 'Admin\DesignationController');
    Route::get('/designation-data', 'Admin\DesignationController@datatable');
    Route::get('designation-upload', 'Admin\DesignationController@showUpload');
    Route::post('designationUpload', 'Admin\DesignationController@upload');

    Route::resource('/users', 'Admin\UsersController');
    Route::get('/users-data', 'Admin\UsersController@datatable');

    //grade route
    Route::resource('grade', 'Admin\GradeController');
    Route::get('/grade-data', 'Admin\GradeController@datatable');
    Route::get('grade-upload', 'Admin\GradeController@showUpload');
    Route::post('gradeUpload', 'Admin\GradeController@upload');

    //department route
    Route::resource('department', 'Admin\DepartmentController');
    Route::get('/department-data', 'Admin\DepartmentController@datatable');
    Route::get('department-upload', 'Admin\DepartmentController@showUpload');
    Route::post('departmentUpload', 'Admin\DepartmentController@upload');

    //department route
    Route::resource('sub-department', 'Admin\SubDepartmentController');
    Route::get('/sub-department-data', 'Admin\SubDepartmentController@datatable');
    Route::get('sub-department-upload', 'Admin\SubDepartmentController@showUpload');
    Route::post('subDepartmentUpload', 'Admin\SubDepartmentController@upload');

    //business unit route
    Route::resource('business-unit', 'Admin\BusinessUnitController');
    Route::get('/business-unit-data', 'Admin\BusinessUnitController@datatable');
    Route::get('business-unit-upload', 'Admin\BusinessUnitController@showUpload');
    Route::post('businessUnitUpload', 'Admin\BusinessUnitController@upload');

    //legal entity route
    Route::resource('legal-entity', 'Admin\LegalEntityController');
    Route::get('/legal-entity-data', 'Admin\LegalEntityController@datatable');
    Route::get('legal-entity-upload', 'Admin\LegalEntityController@showUpload');
    Route::post('legalEntityUpload', 'Admin\LegalEntityController@upload');

    //brand route
    Route::resource('brand', 'Admin\BrandController');
    Route::get('/brand-data', 'Admin\BrandController@datatable');
    Route::get('brand-upload', 'Admin\BrandController@showUpload');
    Route::post('brandUpload', 'Admin\BrandController@upload');

    //employee classification route
    Route::resource('employee-classification', 'Admin\EmployeeClassificationController');
    Route::get('/employee-classification-data', 'Admin\EmployeeClassificationController@datatable');
    Route::get('employee-classification-upload', 'Admin\EmployeeClassificationController@showUpload');
    Route::post('employeeClassificationUpload', 'Admin\EmployeeClassificationController@upload');

    //source category route
    Route::resource('source-category', 'Admin\SourceCategoryController');
    Route::get('/source-category-data', 'Admin\SourceCategoryController@datatable');
    Route::get('source-category-upload', 'Admin\SourceCategoryController@showUpload');
    Route::post('sourceCategoryUpload', 'Admin\SourceCategoryController@upload');

    //variable pay type route
    Route::resource('variable-pay-type', 'Admin\VariablePayTypeController');
    Route::get('/variable-pay-type-data', 'Admin\VariablePayTypeController@datatable');
    Route::get('variable-pay-type-upload', 'Admin\VariablePayTypeController@showUpload');
    Route::post('variablePayTypeUpload', 'Admin\VariablePayTypeController@upload');

    //Man Power
    Route::resource('man-power', 'Admin\ManPowerController');
	Route::get('manpower-report/region-wise-store/{id}', 'Admin\ManPowerController@manPowerRegionWiseStore');
    Route::get('/man-power-data/{id?}', 'Admin\ManPowerController@datatable');
    Route::get('man-power-upload', 'Admin\ManPowerController@showUpload');
    Route::post('manPowerUpload', 'Admin\ManPowerController@upload');
    Route::get('manpower-report/region-wise-store-export/{region_id}', 'Admin\ManPowerController@exportstoremanpower');

    //state route
    Route::resource('state', 'Admin\StateController');
    Route::get('/state-data', 'Admin\StateController@datatable');
    Route::get('state-upload', 'Admin\StateController@showUpload');
    Route::post('stateUpload', 'Admin\StateController@upload');

    //city route
    Route::resource('city', 'Admin\CityController');
    Route::get('/city-data', 'Admin\CityController@datatable');
    Route::get('city-upload', 'Admin\CityController@showUpload');
    Route::post('cityUpload', 'Admin\CityController@upload');

    //region route
    Route::resource('region', 'Admin\RegionController');
    Route::get('/region-data', 'Admin\RegionController@datatable');
    Route::get('region-upload', 'Admin\RegionController@showUpload');
    Route::post('regionUpload', 'Admin\RegionController@upload');

    //reports
    Route::any('hiring-report', 'Admin\ReportController@hiringReport');
    Route::any('attendance-report', 'Admin\ReportController@attendanceReport');

    // Employee Route
    Route::get('storelist/{regional_id}', 'Admin\EmployeeController@storelist');
    Route::get('employeelist/{store_id}', 'Admin\EmployeeController@employee_list');
    Route::get('store-data/{regional_id}', 'Admin\EmployeeController@storedatatable');
    Route::get('store-employee-data/{store_id}', 'Admin\EmployeeController@storeemployeedatatable');
    Route::get('regionallist', 'Admin\EmployeeController@regionallist');
    Route::get('regional-data', 'Admin\EmployeeController@regionaldatatable');

    Route::get('suggestion', 'Admin\UserUploadController@index')->name('_suggestion');
    Route::get('usercodeview/{id}', 'Admin\UserUploadController@show');
    Route::get('user-upload-data', 'Admin\UserUploadController@useruploaddatatable');
    Route::get('export-to-excel', 'Admin\UserUploadController@ExportToExcel')->name('_ExportToExcel');




    // Hiring-Report Routes
    //Man Power
  //  Route::resource('man-power', 'Admin\HiringReportController');
    Route::get('hiring-report/region-wise-store/{id}', 'Admin\HiringReportController@hiringReportRegionWiseStore');
    Route::get('hiring-report', 'Admin\HiringReportController@hiringReport');
    Route::get('hiring-report-data', 'Admin\HiringReportController@datatable');
    Route::get('hiring-report/region', 'Admin\HiringReportController@hiringreportRegion');
    Route::get('hiring-report/store/{region_id}', 'Admin\HiringReportController@hiringreportStore');
    Route::get('hiring-pending-employee/store/{store_id}', 'Admin\HiringReportController@hiringreportStoreemployee');
    Route::get('hiring-pending-employee/store-data/{store_id}', 'Admin\HiringReportController@hiringreportStoreemployeedatatable');
    Route::get('hiring-pending-employee/view/{employee_id}', 'Admin\HiringReportController@hiringreportpendingemployeeview');
    Route::get('hiring-report/store-report/{store_id}', 'Admin\HiringReportController@hiringreportStoreReport');
    Route::get('hiring-report/region-wise-store/{id}', 'Admin\HiringReportController@attrition-reportRegionWiseStore');


    Route::get('attrition-report', 'Admin\HiringReportController@attrition-reportReport');
    Route::get('attrition-report-data', 'Admin\HiringReportController@attritionReportdatatable');
    Route::get('attrition-report/region', 'Admin\HiringReportController@attritionReportRegion');
    Route::get('attrition-report-percentage-data', 'Admin\HiringReportController@regionmanpowerpecentagedatatable');
    Route::get('attrition-report/store/{region_id}', 'Admin\HiringReportController@attritionReportStore');
    Route::get('attrition-report/store-report/{store_id}', 'Admin\HiringReportController@attritionreportreportStoreReport');
    Route::get('attrition-report/region-data', 'Admin\HiringReportController@regionattritiondatatable');
    Route::get('attrition-report/store-data/{region_id}', 'Admin\HiringReportController@attritionReportStoredatatable');
    Route::get('attrition-report-reason/region', 'Admin\HiringReportController@attritionReportReason');
    Route::get('attrition-report-reason/region/{region_id}', 'Admin\HiringReportController@attritionReportReasonRegion');
    Route::get('attrition-report-reason/store/{store_id}', 'Admin\HiringReportController@attritionReportReasonStore');
    Route::get('attrition-report-aging/region-data', 'Admin\HiringReportController@attritionReportAgingdatatable');
    Route::get('attrition-report-promise/region', 'Admin\HiringReportController@attritionReportRegionPromise');
    Route::get('attrition-report-promise/regionwise/{region_id}', 'Admin\HiringReportController@attritionReportRegionPromiseRegionWise');



    /* Aging Routes */

    Route::get('aging-report/region', 'Admin\HiringReportController@agingregion');
    Route::get('aging-report/region/{region_id}', 'Admin\HiringReportController@agingregionstore');
    Route::get('aging-report/region-data/{region_id}', 'Admin\HiringReportController@agingregionstoredatatable');
    Route::get('aging-report/store/{store_id}', 'Admin\HiringReportController@agingstore');
    Route::get('aging-report/region-data/', 'Admin\HiringReportController@agingregiondatatable');


    /* Man Power Percentages Routes */
    Route::get('man-power-percentage/{region_id}', 'Admin\HiringReportController@manpowerpercentageRegion');
    Route::get('man-power-percentage/store/{store_id}', 'Admin\HiringReportController@manpowerpercentageStore');

    /* Hiring - Inprocess report routes  */
    Route::get('hiring-inprocess', 'Admin\HiringReportController@hiringinprocessRegion');
    Route::get('hiring-inprocess-data', 'Admin\HiringReportController@hiringinprocessRegiondatatable');
    Route::get('hiring-inprocess/store/{region_id}', 'Admin\HiringReportController@hiringinprocessStore');
    Route::get('hiring-inprocess/store-data/{region_id}', 'Admin\HiringReportController@hiringinprocessStoredatatable');

    /* Hiring Source Reports */
    Route::get('hiring-source-report', 'Admin\HiringReportController@hiringsourceregion');
    Route::get('hiring-source-report-store/{region_id}', 'Admin\HiringReportController@hiringsourceregionstore');

    /* Gender wise attrition Report Routes */
    Route::get('gender-attrition-report/region', 'Admin\HiringReportController@additionalReports');
    Route::get('gender-attrition-report/store/{region_id}', 'Admin\HiringReportController@storeadditionalReports');

    /* Store income Upload Excel file Routes */
    Route::get('storecollection', 'Admin\StoreLocationController@StoreCollectionForm');
    Route::post('storecollection', 'Admin\StoreLocationController@StoreCollectionUpload');

    /* Productivity Reports Routes */
    Route::get('productivity-report', 'Admin\ReportController@ColletionReportregion');
    Route::get('productivity-report-data', 'Admin\ReportController@ColletionReportregiondatatable');
    Route::get('productivity-report-store/{region_id}', 'Admin\ReportController@ColletionReportstore');
    Route::get('productivity-report-store-data/{region_id}', 'Admin\ReportController@ColletionReportstoredatatable');

    /* Leave Report Route */

    Route::get('leave-report', 'Admin\ReportController@LeaveReportregion');
    Route::get('leave-report-region-data', 'Admin\ReportController@LeaveReportregiondatatable');
    Route::get('leave-report-store/{region_id}', 'Admin\ReportController@LeaveReportstore');
    Route::get('leave-report-store-data/{region_id}', 'Admin\ReportController@LeaveReportstoredatatable');
    Route::get('leave-report-employee/{store_id}', 'Admin\ReportController@LeaveReportemployee');
    Route::get('leave-report-employee-data/{store_id}', 'Admin\ReportController@LeaveReportemployeedatatable');


    /* Attendance Reports */
    Route::get('attendance-report', 'Admin\AttendanceReportController@index');
    Route::get('attendance-report/region', 'Admin\AttendanceReportController@region');
    Route::get('attendance-report/store/{region_id}', 'Admin\AttendanceReportController@storevise');

    Route::any('attendance-report/employee/{store_id}', 'Admin\AttendanceReportController@EmployeeAttandance');
    Route::post('attendence-csv-export-store', 'Admin\AttendanceReportController@attendencecsvExportstore');
    Route::get('setdate', 'Admin\AdminController@resetAttendanceDate');

    /* Employee Exports */
    Route::get('export-employee-details', 'Admin\EmployeeController@ExportEmployeeEducationDetail');


    /* User Routes */

    Route::get('user/userdatatable/{user_type}', 'Admin\UsersController@userdatatable');

    Route::get('/user/hrms', 'Admin\UsersController@hrmslist');
    Route::get('/user/hrms/create', 'Admin\UsersController@hrmscreate');
    Route::post('/user/hrms/create', 'Admin\UsersController@userstore');
    Route::get('/user/hrms/edit/{user_id}', 'Admin\UsersController@hrmsedit');
    Route::patch('/user/hrms/edit/{user_id}', 'Admin\UsersController@userupdate');

    Route::get('/user/regionalhr', 'Admin\UsersController@regionalhrlist');
    Route::get('/user/regionalhr/create', 'Admin\UsersController@regionalhrcreate');
    Route::post('/user/regionalhr/create', 'Admin\UsersController@userstore');
    Route::get('/user/regionalhr/edit/{user_id}', 'Admin\UsersController@regionalhredit');
    Route::patch('/user/regionalhr/edit/{user_id}', 'Admin\UsersController@userupdate');

    Route::get('/user/storemanager', 'Admin\UsersController@storemanagerlist');
    Route::get('/user/storemanager/create', 'Admin\UsersController@storemanagercreate');
    Route::post('/user/storemanager/create', 'Admin\UsersController@userstore');
    Route::get('/user/storemanager/edit/{user_id}', 'Admin\UsersController@storemanageredit');
    Route::patch('/user/storemanager/edit/{user_id}', 'Admin\UsersController@userupdate');


    Route::get('store_details/{store_id}', 'Admin\StoreLocationController@StoreDetails');
    /* store_budget Routes */
    Route::resource('store-budget', 'Admin\StoreBudgetController');
    Route::get('store-budget-national', 'Admin\StoreBudgetController@getnational');
    Route::get('store-budget-region-data', 'Admin\StoreBudgetController@regiondatatable');
    Route::get('store-budget-data', 'Admin\StoreBudgetController@datatable');
    Route::get('store-budget-data/{region_id}', 'Admin\StoreBudgetController@storedatatable');
    Route::get('store-budget-upload', 'Admin\StoreBudgetController@showUpload');
    Route::post('store-budget-excelUpload', 'Admin\StoreBudgetController@ExcelUpload');
    Route::get('store-budget-region/{region_id}', 'Admin\StoreBudgetController@getregional');

    /* Store Budget Lable  */

    Route::get('store-budget-lable', 'Admin\StoreBudgetController@showLableFileUpload');
    Route::post('store-Lablebudget-excelUpload', 'Admin\StoreBudgetController@LableExcelUpload');

    /* Store_budget Reports */
    Route::get('store-budget-region', 'Admin\StoreBudgetController@storebudgetregionwise');
    Route::get('store-budget-store/{region_id}', 'Admin\StoreBudgetController@storebudgetstorewise');
    Route::get('store-budget-store-export/{region_id}', 'Admin\StoreBudgetController@exportstorebudget');


    Route::get('user/delete', 'Admin\UsersController@userdestroy');

    //status change
    Route::get('/change-status/{table}/{status}/{id}', function($table,$status,$id){
        $status = ($status == 0)?1:0;

        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    });

    Route::get('getalluserlogin', 'HomeController@GetAllLogin');
    // Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    // Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
});

Route::group(['prefix' => 'store' , 'as' => 'store','middleware' => ['auth','redirectCheck']], function () {
    Route::get('/', 'Store\StoreController@index');

    //employee route
    Route::get('/hiring-data', 'Store\HiringController@datatable');
    Route::get('/hiring/createEducationDetail/{id?}', 'Store\HiringController@createEducationDetail');
    Route::post('hiring/storeEducationDetail/{id}', 'Store\HiringController@storeEducationDetail');
    Route::get('hiring/createExperience/{id?}', 'Store\HiringController@createExperience');
    Route::post('hiring/storeExperience/{id}', 'Store\HiringController@storeExperience');
    Route::get('hiring/createFamilyInfo/{id?}', 'Store\HiringController@createFamilyInfo');
    Route::post('hiring/storeFamilyInfo/{id}', 'Store\HiringController@storeFamilyInfo');
    Route::get('hiring/createOtherInfo/{id?}', 'Store\HiringController@createOtherInfo');
    Route::post('hiring/storeOtherInfo/{id}', 'Store\HiringController@storeOtherInfo');
    Route::get('/upload-documents/{id}/{upload?}', 'Store\HiringController@uploadDocuments');
    Route::get('/download-appointment-letter/{id}', 'Store\HiringController@downloadAppointmentLetter');
    Route::post('/saveDocuments/{id}', 'Store\HiringController@saveDocuments');
    Route::get('/downloadForm/{id}', 'Store\HiringController@downloadForm');
    Route::resource('hiring', 'Store\HiringController');

    Route::resource('employee', 'Store\EmployeeController');
    Route::get('/employee-data', 'Store\EmployeeController@datatable');
    Route::get('/employee/createEducationDetail/{id?}', 'Store\EmployeeController@createEducationDetail');
    Route::post('employee/storeEducationDetail/{id}', 'Store\EmployeeController@storeEducationDetail');
    Route::get('employee/createExperience/{id?}', 'Store\EmployeeController@createExperience');
    Route::post('employee/storeExperience/{id}', 'Store\EmployeeController@storeExperience');
    Route::get('employee/createFamilyInfo/{id?}', 'Store\EmployeeController@createFamilyInfo');
    Route::post('employee/storeFamilyInfo/{id}', 'Store\EmployeeController@storeFamilyInfo');
    Route::get('employee/upload-documents/{id}/{upload?}', 'Store\EmployeeController@uploadDocuments');
    Route::post('employee/saveDocuments/{id}', 'Store\EmployeeController@saveDocuments');
    Route::get('employee/downloadForm/{id}', 'Store\EmployeeController@downloadForm');
    Route::get('employee/createOtherInfo/{id?}', 'Store\EmployeeController@createOtherInfo');
    Route::post('employee/storeOtherInfo/{id}', 'Store\EmployeeController@storeOtherInfo');

    Route::get('/employee/logs/{id}', 'Store\EmployeeController@logView');
    Route::get('/employee-log/{id}', 'Store\EmployeeController@log');
    Route::get('/hiring/logs/{id}', 'Store\HiringController@logView');
    Route::get('/employee-log/{id}', 'Store\EmployeeController@log');
    Route::get('/hiring/{id}/request-region', 'Store\HiringController@sendRequestToRegionManager');
    Route::get('sub-department-list', 'Store\EmployeeController@getSubDepartments');
    Route::get('city-list', 'Store\EmployeeController@getCityFromState');
    Route::get('state-list', 'Store\EmployeeController@getStateFromRegion');
    Route::get('sorce-detail', 'Store\EmployeeController@getSourceDetail');
    Route::get('location-detail', 'Store\EmployeeController@getLocationDetail');
    /*Route::get('show-employee-upload', 'Store\EmployeeController@showUpload');
    Route::post('employeeUpload', 'Store\EmployeeController@upload');*/

    Route::any('attendance-filter', 'Store\AttendanceController@index');
    Route::post('change-request', 'Store\AttendanceController@changeRequest');
    Route::resource('attendance', 'Store\AttendanceController');

    Route::resource('absconding', 'Store\AbscondingController');
    Route::get('/absconding-data', 'Store\AbscondingController@datatable');
    Route::post('getEmployeeDetail', 'Store\AbscondingController@getEmployeeDetail');
    Route::post('add-to-absconding-separation', 'Store\AbscondingController@addToAbsconding');
    Route::post('remove-absconding/{id}', 'Store\AbscondingController@removeFromAbsconding');
    Route::get('send-notification-to-rm', 'Store\AbscondingController@abscondingNotificationToRM');

	Route::get('resignation-data', 'Store\ResignationController@datatable');
	Route::get('resignation-letter/{id}', 'Store\ResignationController@resignationLetterDownload');
	Route::post('resignation-letter', 'Store\ResignationController@resignationLetterSubmit');
    Route::resource('resignation', 'Store\ResignationController');
    Route::post('resignation_update', 'Store\ResignationController@Resignation_Update');
    //status change

    /* Leave Report */
    Route::get('leave-report-employee', 'Store\EmployeeController@LeaveReportemployee');
    Route::get('leave-report-employee-data/{store_id}', 'Store\EmployeeController@LeaveReportemployeedatatable');


    /* Attendance Reports */
    Route::any('attendance-report/employee', 'Store\AttendanceController@EmployeeAttandance');
    Route::post('attendence-csv-export', 'Store\AttendanceController@attendencecsvExport');

    Route::post('getsubcategory/{id}', 'Store\ResignationController@getsubreason')->name('_get_subcategory');


    Route::get('change-password', 'Store\StorePasswordChange@changepasswordform')->name('_password_change_form');
    Route::post('change-password', 'Store\StorePasswordChange@changepassword')->name('_password_change');
    Route::get('/change-status/{table}/{status}/{id}', function($table,$status,$id){
        $status = ($status == 0)?1:0;

        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    });

    Route::get('/hold/{status}/{id}', function($status,$id){
        $message = ($status == '1')?"Application Hold":"Application Unhold";
        \DB::table('employee')->where('id',$id)->update(['store_hold'=>$status]);

        return redirect('store/hiring')->with('flash_success', $message);
    });
});

Route::group(['prefix' => 'regional' , 'as' => 'store','middleware' => ['auth','redirectCheck']], function () {
    Route::get('/', 'Regional\RegionalController@index');
	Route::get('/manpower-report/store/','Regional\RegionalController@manpowerstore');
	Route::get('/manpower-report/storedata/','Regional\RegionalController@manpowerstoredata');
	Route::get('/hygiene-report/store','Regional\RegionalController@hygienestore');
	Route::get('/hygiene-report/storedata/','Regional\RegionalController@hygienestoredata');

	Route::get('/manpowercost-report/store/','Regional\RegionalController@manpowercoststore');
    Route::get('/manpowercost-report/storedata/','Regional\RegionalController@manpowercoststoredata');
    Route::get('store-budget-export','Regional\RegionalController@exportstorebudget');

    //employee route
    Route::resource('hiring', 'Regional\HiringController');
    Route::get('/hiring-data', 'Regional\HiringController@datatable');
    Route::get('/upload-documents/{id}/{upload?}', 'Regional\HiringController@uploadDocuments');
    Route::post('/saveDocuments/{id}', 'Regional\HiringController@saveDocuments');
    Route::get('/downloadForm/{id}', 'Regional\HiringController@downloadForm');

    Route::get('employee/detail/{id}', function($id){
        $employee = \DB::table('employee')->select(['id','source_category_id','source_detail_id','source_code','employee_code','employee_name','old_employee_code','joining_date','employee_refferal_amount','employee_refferal_payment_month','grade_id'])->where('id',$id)->first();

        return json_encode($employee);
    });
    Route::resource('employee', 'Regional\EmployeeController');
    Route::get('storelist', 'Regional\EmployeeController@storelist');
    Route::get('employeelist/{store_id}', 'Regional\EmployeeController@employee_list');
    Route::get('store-data', 'Regional\EmployeeController@storedatatable');
   /* Route::get('employee/{store_id}/', 'Regional\EmployeeController@index');
    Route::get('employee/{store_id}/{id}', 'Regional\EmployeeController@show');
    Route::get('employee/{store_id}/create', 'Regional\EmployeeController@create');
    Route::post('employee/{store_id}', 'Regional\EmployeeController@store');
    Route::delete('employee/{store_id}/{id}', 'Regional\EmployeeController@destroy');
    Route::put('employee/{store_id}/{id}', 'Regional\EmployeeController'); */
    Route::get('/employee-data/{store_id}', 'Regional\EmployeeController@datatable');
    Route::get('/employee-data', 'Regional\EmployeeController@allEmployeeDatatable');
    Route::get('/employee/logs/{id}', 'Regional\EmployeeController@logView');
    Route::get('/hiring/logs/{id}', 'Regional\HiringController@logView');
    Route::get('/employee-log/{id}', 'Regional\EmployeeController@log');
    Route::any('/hiring/{id}/approve', 'Regional\HiringController@approve');
    Route::post('/hiring/{id}/send-back', 'Regional\HiringController@sendBack');
    Route::any('/hiring/{id}/reject', 'Regional\HiringController@reject');
    Route::any('/hiring/{id}/hold', 'Regional\HiringController@hold');
    Route::get('sub-department-list', 'Regional\EmployeeController@getSubDepartments');
    Route::get('city-list', 'Regional\EmployeeController@getCityFromState');
    Route::get('state-list', 'Regional\EmployeeController@getStateFromRegion');
    Route::get('sorce-detail', 'Regional\EmployeeController@getSourceDetail');
    Route::get('sorce-code', 'Regional\EmployeeController@getSourceCode');
    Route::get('location-detail', 'Regional\EmployeeController@getLocationDetail');

    Route::post('attendance-action', 'Regional\AttendanceController@approveReject');
    Route::resource('attendance', 'Regional\AttendanceController');

    //absconding separation route
    Route::resource('absconding', 'Regional\AbscondingController');
    Route::get('/absconding-data', 'Regional\AbscondingController@datatable');
    Route::post('approve-absconding/{id}', 'Regional\AbscondingController@approveAbsconding');
    Route::get('download-letter/{id}', 'Regional\AbscondingController@downloadLetter');
    Route::post('reject-absconding/{id}', 'Regional\AbscondingController@rejectAbsconding');

	Route::get('resignation-data', 'Regional\ResignationController@datatable');
    Route::resource('resignation', 'Regional\ResignationController');
    Route::post('resignationupdate', 'Regional\ResignationController@updateRegignation');

	Route::get('change-password', 'Regional\RegionalPasswordChange@changepasswordform')->name('_rpassword_change_form');
    Route::post('change-password', 'Regional\RegionalPasswordChange@changepassword')->name('_rpassword_change');
    Route::get('resignation-letter/{id}', 'Regional\ResignationController@resignationLetterDownload');
    //status change

    Route::get('store-hiring-report/{store_id}','Regional\HiringController@storeHiringReport');

    Route::get('store-attrition-report/', 'Regional\HiringController@storewiseAttritionReport');
    Route::get('attrition-report/store-data/{region_id}', 'Regional\HiringController@attritionReportStoredatatable');
    Route::get('attrition-report/store/{store_id}', 'Regional\HiringController@attritionStoreReportStoredatatable');


    Route::get('aging-report/region', 'Regional\HiringController@agingregion');
    Route::get('store-aging-report', 'Regional\HiringController@agingregionstore');
    Route::get('store-aging-report-data/{region_id}', 'Regional\HiringController@agingregionstoredatatable');
    Route::get('aging-report/store/{store_id}', 'Regional\HiringController@agingstore');
    Route::get('aging-report/region-data/', 'Regional\HiringController@agingregiondatatable');
    Route::get('store-reason-report', 'Regional\HiringController@attritionReportReasonRegion');
    Route::get('store-manpower-report', 'Regional\HiringController@manpowerpercentageReportbyStore');

    Route::get('hiring-report-store', 'Regional\HiringController@hiringReportbyStore');

    Route::post('attendence-csv-export', 'Regional\AttendanceController@attendencecsvExport');
    Route::post('attendence-csv-export-store', 'Regional\AttendanceController@attendencecsvExportstore');
    Route::get('attendence-list', 'Regional\AttendanceController@attandanceList');
    Route::post('attendence-list', 'Regional\AttendanceController@attandanceListget');

    /* Hiring - Inprocess report routes  */
    Route::get('hiring-inprocess/store/', 'Regional\HiringController@hiringinprocessStore');
    Route::get('hiring-inprocess/store-data/{region_id}', 'Regional\HiringController@hiringinprocessStoredatatable');


    /* Additional Reports Routes */
    Route::get('additional-report/store/', 'Regional\HiringController@storeadditionalReports');

    /* Productivity Report Routes */
    Route::get('productivity-report-store/', 'Regional\ReportController@ColletionReportstore');
    Route::get('productivity-report-store-data/{region_id}', 'Regional\ReportController@ColletionReportstoredatatable');

    /* Leave Reports Routes */
    Route::get('leave-report-store', 'Regional\ReportController@LeaveReportstore');
    Route::get('leave-report-store-data/{region_id}', 'Regional\ReportController@LeaveReportstoredatatable');
    Route::get('leave-report-employee/{store_id}', 'Regional\ReportController@LeaveReportemployee');
    Route::get('leave-report-employee-data/{store_id}', 'Regional\ReportController@LeaveReportemployeedatatable');

    /* Attendance Reports */
    Route::get('attendance-report/store/', 'Regional\AttendanceController@storevise');
    Route::any('attendance-report/employee/{store_id}', 'Regional\AttendanceController@EmployeeAttandance');

    /* Get Employee Detail */

    Route::post('getemployeedetail', 'Regional\EmployeeController@getEmployeeDetail');
    Route::post('store/pdo', 'Regional\PDOController@store');
    Route::get('getpdodetail/{pdo_id}', 'Regional\PDOController@GetPDODetail');


    Route::get('/change-status/{table}/{status}/{id}', function($table,$status,$id){
        $status = ($status == 0)?1:0;

        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    });

    Route::get('/hold/{status}/{id}', function($status,$id){
        $message = ($status == '1')?"Application Hold":"Application Unhold";
        \DB::table('employee')->where('id',$id)->update(['regional_hold'=>$status]);

        return redirect('regional/hiring')->with('flash_success', $message);
    });
});

Route::group(['prefix' => 'hrms' , 'as' => 'store','middleware' => ['auth','redirectCheck']], function () {
    Route::get('/', 'Hrms\HrmsController@index');

    //employee route
    Route::resource('separation', 'Hrms\SeparationController');
    Route::get('/separation-data', 'Hrms\SeparationController@datatable');

	Route::get('resignation-data', 'Hrms\ResignationController@datatable');
	Route::resource('resignation', 'Hrms\ResignationController');

    Route::resource('data-change', 'Hrms\DataChangeController');
    Route::get('/data-change-data', 'Hrms\DataChangeController@datatable');

    Route::get('/hiring/export-excle', 'Hrms\HiringController@exportHiring');
    Route::resource('hiring', 'Hrms\HiringController');
    Route::get('/hiring-data', 'Hrms\HiringController@datatable');

    Route::get('/upload-documents/{id}/{upload?}', 'Hrms\HiringController@uploadDocuments');
    Route::post('/saveDocuments/{id}', 'Hrms\HiringController@saveDocuments');
    Route::get('/downloadForm/{id}', 'Hrms\HiringController@downloadForm');

    // Route::get('/employee/logs/{id}', 'Hrms\EmployeeController@logView');
    Route::get('/hiring/logs/{id}', 'Hrms\HiringController@logView');
    Route::get('/employee-log/{id}', 'Hrms\HiringController@log');
    Route::any('/hiring/{id}/approve_view', 'Hrms\HiringController@approveView');
    Route::any('/hiring/{id}/approve', 'Hrms\HiringController@approve');
    Route::post('/hiring/{id}/send-back', 'Hrms\HiringController@sendBack');
    // Route::get('sub-department-list', 'Hrms\EmployeeController@getSubDepartments');
    // Route::get('city-list', 'Hrms\EmployeeController@getCityFromState');
    // Route::get('state-list', 'Hrms\EmployeeController@getStateFromRegion');
    // Route::get('sorce-detail', 'Hrms\EmployeeController@getSourceDetail');
    // Route::get('location-detail', 'Hrms\EmployeeController@getLocationDetail');
	Route::get('change-password', 'Hrms\HrmsPasswordChange@changepasswordform')->name('_hrpassword_change_form');
    Route::post('change-password', 'Hrms\HrmsPasswordChange@changepassword')->name('_hrpassword_change');
    //status change
    Route::get('/change-status/{table}/{status}/{id}', function($table,$status,$id){
        $status = ($status == 0)?1:0;

        \DB::table($table)->where('id',$id)->update(['status'=>$status]);

        return response()->json(['message' => 'Status Updated'],200);
    });
});

function caseChange($value){
    return strtolower(trim($value));
}

function logCreate($data){
    $user = auth()->id();
    $role = \DB::table('role_user')->where('user_id',$user)->first();
    $data['user_id'] = $user;
    $data['user_role_id'] = $role->role_id;

    DB::table('logs')->insert($data);

    return true;
}