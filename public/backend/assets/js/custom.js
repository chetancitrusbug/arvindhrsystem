jQuery(document).ready(function ($) {
    statusChange();
    highlight();
});


function highlight(){
    $('.highlight').parent('a').parent('td').parent('tr').removeClass('odd').addClass('highlight-row');
}

function statusChange() {
    $('.status').checkboxpicker({
        offLabel: 'Inactive',
        onLabel: 'Active',
    });
    $('.status1').checkboxpicker();

    $('.status_invoice').checkboxpicker({
        offLabel: 'Inprogress',
        onLabel: 'Confirm',
    });
}
$('.status_invoice').checkboxpicker({
    offLabel: 'Inprogress',
    onLabel: 'Confirm',
});

$('.first_time_employment').checkboxpicker({
    offLabel: 'No',
    onLabel: 'Yes',
});


function changeImage(id) {
    $('.changeImage' + id).hide();
}

function capitalize(status) {
    return "<?php echo ucfirst("+ status +"); ?>";
}

$.fn.niftyModal('setDefaults', {
    overlaySelector: '.modal-overlay',
    closeSelector: '.modal-close',
    classAddAfterOpen: 'modal-show',
});

var id;
var url;

$(document).on('click', '.del-item', function (e) {
    $("#loading").show();
    id = $(this).data('id');
    url = $(this).data('url');
    msg = $(this).data('msg');
    url = url + "/" + id;

    jQuery('.delete-confirm-text').html("Are you sure you want to Delete " + msg +" ?");
    $('#danger').niftyModal();
});

$('#loading').bind('ajaxStart', function () {
    $(this).show();
}).bind('ajaxStop', function () {
    $(this).hide();
});

$('.modal-close').on('click',function(){
    $("#loading").hide();
    location.reload();
});

$(".delete-confirm").on('click',function(){
    $("#loading").show();

    $.ajax({
        type: "delete",
        url: url,
        data: { from_index: true },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $("#loading").hide();
            $('.success-text').html(data.message);
            $('#danger').niftyModal('hide');
            $('#success').niftyModal();

            if(msg != 'roles' && msg != 'permission'){
                datatable.draw();
            }

            id = url = null;

            setTimeout(function () {
                $('#success').niftyModal('hide');
                if(msg == 'roles' || msg == 'permission'){
                    location.reload();
                }
            }, 3000);
        },
        error: function (xhr, status, error) {
            jQuery('.delete-confirm-text').html("");
            jQuery('.delete-title').html("Action not proceed!");
            $('#danger').niftyModal();
        }
    });
});

$(document).on('change', '.status-change', function (e) {
    $("#loading").show();
    var id = $(this).data('id');
    var status = $(this).data('status');
    var table = $(this).data('table');
    var url = $(this).data('url');

    url = url+ "/" + table + '/' + status + '/' + id;

    $.ajax({
        url: url,
        success: function (data) {
            $("#loading").hide();
            $('.success-text').html(data.message);
            $('#danger').niftyModal('hide');
            $('#success').niftyModal();
            datatable.draw();

            setTimeout(function () {
                $('#success').niftyModal('hide');
            }, 1000);
        },
        error: function (xhr, status, error) {
            jQuery('.delete-confirm-text').html("");
            jQuery('.delete-title').html("Action not proceed!");
            $('#danger').niftyModal();
        }
    });
});

$(document).on('click', '.edit-employee', function (e) {
    $("#loading").show();
    url = $(this).data('url');
    msg = $(this).data('msg');
    url = url;

    jQuery('.warning-confirm-text').html("Are you sure you want to Edit " + msg +" ?");
    $('#warning').niftyModal();
});

$(document).on('click', '.hold-hiring', function (e) {
    $("#loading").show();
    url = $(this).data('url');
    msg = $(this).data('msg');
    url = url;
	jQuery('.warning-confirm-text').html("Are you sure you want to "+msg+" hiring  ?");
    $('#warning').niftyModal();
});

$(".warning-confirm").on('click',function(){
    $("#loading").hide();

    window.location.href = url;
});

$(document).on('click', '.excel-upload', function (e) {
    $("#uploadForm").attr('action',$(this).data('url'));
    $('#fileUpload').niftyModal('show');
});

///reject by regional manager
$(document).on('click', '.regional_reject', function (e) {
    $("#loading").show();
    id = $(this).data('id');
    url = $(this).data('url');
    title = $(this).data('title');

    $('#reject').niftyModal();
});

$(document).on('click', '.submit-reject', function (e) {
    $("#loading").show();
    $.ajax({
        type: 'POST',
        url: url,
        data: {reject_reason:$('#reject_reason').val()},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    .done(function(data) {
        $('button[type="submit"]').removeAttr('disabled');
        $('.success-text').html(data);
        $('#reject').niftyModal('hide');
        $('#success').niftyModal();

        setTimeout(function () {
            $('#success').niftyModal('hide');
            location.reload();
        }, 2000);
    })
    .fail(function(data) {
        $('button[type="submit"]').removeAttr('disabled');
        jQuery.each(data.responseJSON.errors,function(index,val){
            $('.'+index+'_error').html(val);
        });
    });
});

///send back functionality
$(document).on('click', '.regional_send_back', function (e) {
    $("#loading").show();
    id = $(this).data('id');
    url = $(this).data('url');
    title = $(this).data('title');

    if($(this).data('note')){
        sendBack($(this).data('note'));
    }else{
        jQuery('.send_back_title').html("Send Back to "+title);
        $('#sendBack').niftyModal();
    }
});


$(document).on('click', '.submit-note', function (e) {
    $("#loading").show();
    $('button[type="submit"]').attr('disabled', 'disabled');

    sendBack($('#add_note').val());
});

function sendBack(note){
    $.ajax({
        type: 'POST',
        url: url,
        data: {note:note},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    .done(function(data) {
        $('button[type="submit"]').removeAttr('disabled');
        $('.success-text').html(data);
        $('#sendBack').niftyModal('hide');
        $('#success').niftyModal();
        datatable.draw();

        setTimeout(function () {
            $('#success').niftyModal('hide');
            $("#loading").hide();
        }, 2000);
    })
    .fail(function(data) {
        $('button[type="submit"]').removeAttr('disabled');
        jQuery.each(data.responseJSON.errors,function(index,val){
            $('.'+index+'_error').html(val);
        });
    });
}
///send back functionality end

///number validation
$('.checkNumber').on('keypress',function(event){
    if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
       event.preventDefault(); //stop character from entering input
   }
});
///number validation end

//store manager confirmation
$('.send_to_rm').on('click',function(e){
    $('input[name="button"]').val($(this).val());
    $("#loading").show();
    e.preventDefault();
    $('#confirmation').niftyModal();
});

$('.confirm-cancel').on('click',function(e){
    $('#confirmation').niftyModal('hide');
    $('#confirmation_index').niftyModal('hide');
    $("#loading").hide();
});

$('.confirmation-close').on('click',function(e){
    e.preventDefault();
    $('input[name="button"]').val();
    $('#confirmation').niftyModal('hide');
    $("#loading").hide();
    $('form').submit();
});

//regional manger confirmation
$('.send_back_to_sm, .send_to_hr').on('click',function(e){
    $("#loading").show();
    e.preventDefault();
    var title = $(this).data('title');

    if(title == 'send_to_hr'){
        $('.confirmation-title').html('Are you sure to approve and approve to HR ?');
    }else{
        $('.confirmation-title').html('Are you sure to send back to SM ?');
    }

    $('input[name="button"]').val($(this).val());
    $('#confirmation').niftyModal();
});

//HR manger confirmation
$('.send_back_to_rm, .update_and_approve').on('click',function(e){
    $("#loading").show();
    e.preventDefault();
    var title = $(this).data('title');

    if(title == 'approve'){
        $('.confirmation-title').html('Are you sure to approve Employee ?');
    }else{
        $('.confirmation-title').html('Are you sure to send back to RM ?');
    }

    $('input[name="button"]').val($(this).val());
    $('#confirmation').niftyModal();
});

$('.confirmation-index-close').on('click',function(){
    $("#loading").hide();
    window.location.href = url;
});

//confirmation from index page on approve
$(document).on('click','.request_from_index',function(e){
    $("#loading").show();
    e.preventDefault();
    url = $(this).data('url');

    $('.confirmation-index-title').html('Are you sure to request Regional HR ?');

    $('#confirmation_index').niftyModal();
});

var title = '';
$(document).on('click', '.send_to_hr_from_index, .approve_from_index', function (e) {
    $("#loading").show();
    id = $(this).data('id');
    url = $(this).data('url');
    title = $(this).data('title');
    date = $(this).data('date');

    $('.joining_date').val(date);

    if(title == 'hrms'){
        jQuery('.send_forword_title').html("Approve by HR");
        jQuery('.employee_code').val("");
        $('#sendForwordHrms').niftyModal();
    }else{
        $.ajax({
            // type: 'POST',
            url: $(this).data('employee'),
            data: {id:id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        .done(function(data) {
            data = JSON.parse(data);
            $('select[name="source_category"]').val(data.source_category_id);
            $('select[name="source_category"]').trigger('change');
            categoryChage(data.source_category_id,data.source_detail_id);
            $('select[name="source_name"]').val(data.source_detail_id);
            $('input[name="source_code"]').val(data.source_code);
            $('select[name="grade_id"]').val(data.grade_id);
            $('select[name="employee_refferal_payment_month"]').val(data.employee_refferal_payment_month);
            $('input[name="employee_refferal_amount"]').val(data.employee_refferal_amount);
            $('select[name="grade_id"]').val(data.grade_id);
            $('input[name="employee_name"]').val(data.employee_name);
            $('input[name="employee_code"]').val(data.employee_code);
            $('input[name="old_employee_code"]').val(data.old_employee_code);

            $('#sendForword').niftyModal();
        });
    }

    $(".datetimepicker1").datetimepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        componentIcon: '.mdi.mdi-calendar',
        navIcons: {
            rightIcon: 'mdi mdi-chevron-right',
            leftIcon: 'mdi mdi-chevron-left'
        }
    });
});

/*//confirmation from index page on approve
$(document).on('click','.approve_from_index',function(e){
    $("#loading").show();
    e.preventDefault();
    url = $(this).data('url');
    title = $(this).data('title');

    if(title == 'regional'){
        $('.confirmation-index-title').html('Are you sure to approve and approve to HR ?');
    }
    if(title == 'hrms'){
        $('.confirmation-index-title').html('Are you sure to approve Employee ?');
    }
    $('#confirmation_index').niftyModal();
});*/

$('.submit-forward-region').on('click',function(){
    $('.error').html('');
    $("#loading").show();
    $.ajax({
        type: 'POST',
        url: url,
        data: {
            source_category:$('select[name="source_category"]').val(),
            source_name:$('select[name="source_name"]').val(),
            category_name:$('input[name="category_name"]').val(),
            source_code:$('input[name="source_code"]').val(),
            employee_name:$('input[name="employee_name"]').val(),
            employee_code:$('input[name="employee_code"]').val(),
            old_employee_code:$('input[name="old_employee_code"]').val(),
            employee_refferal_amount:$('.employee_refferal_amount').val(),
            employee_refferal_payment_month:$('.employee_refferal_payment_month').val(),
            grade_id:$('.grade_id').val(),
            joining_date:$('.joining_date').val(),
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    .done(function(data) {
        $('button[type="submit"]').removeAttr('disabled');
        $('.success-text').html(data);
        $('#sendForword').niftyModal('hide');
        $('#sendForwordHrms').niftyModal('hide');
        $('#success').niftyModal();

        setTimeout(function () {
            $('#success').niftyModal('hide');
            location.reload();
        }, 2000);
    })
    .fail(function(data) {
        $('button[type="submit"]').removeAttr('disabled');
        jQuery.each(data.responseJSON.errors,function(index,val){
            $('.'+index+'_error').html(val);
        });
        $('.error').removeClass('hide');
    });
});

$(document).on('click', '.regional_hold', function (e) {
    $("#loading").show();
    url = $(this).data('url');

    jQuery('.warning-confirm-text').html("Are you sure you want to Hold Application ?");
    $('#warning').niftyModal();
});

$(".warning-confirm").on('click',function(){
    $("#loading").hide();

    window.location.href = url;
});

$(function () {
    $('input[type=file]').change(function () {
        if($(this).data('ext')){
            var exp = "(.*?)\.("+ $(this).data('ext') +")$";

            var val = $(this).val().toLowerCase(),
                regex = new RegExp(exp);

            if (!(regex.test(val))) {
                $(this).val('');
                alert('Please select correct file format');
            }
        }
    });

    $('input[name="attach[previous_company_docs][]"] , input[name="attach[education_documents][]"]').on('change',function(){
        var me = this;
        var status = false;
        for (var i=0; i<this.files.length; i++) {
            var exp = "(.*?)\.(pdf|jpg)$";
            var ext = this.files[i].name.substr(-3).toLowerCase();
            console.log(ext);

            if(!((ext === "jpg") || (ext === "pdf"))){
                status = true;
            }
        }
        if(status){
            $(this).val('');
            alert('Please select correct file format');
        }
    });
});

function selectValue(selector,val){
    $("#"+selector+" option:contains("+val+")").attr('selected', 'selected');
    $('#'+selector).trigger('change');
}