-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2018 at 03:59 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arvind_hr_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` int(11) DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'App\\Permission model has been created', 1, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(2, 'default', 'App\\Permission model has been created', 2, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(3, 'default', 'App\\Permission model has been created', 3, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(4, 'default', 'App\\Permission model has been created', 4, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(5, 'default', 'App\\Permission model has been created', 5, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(6, 'default', 'App\\Permission model has been created', 6, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(7, 'default', 'App\\Permission model has been created', 7, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(8, 'default', 'App\\Permission model has been created', 8, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(9, 'default', 'App\\Permission model has been created', 9, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(10, 'default', 'App\\Permission model has been created', 10, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(11, 'default', 'App\\Permission model has been created', 11, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(12, 'default', 'App\\Permission model has been created', 12, 'App\\Permission', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(13, 'default', 'App\\Role model has been created', 1, 'App\\Role', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(14, 'default', 'App\\Role model has been created', 2, 'App\\Role', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(15, 'default', 'App\\Role model has been created', 3, 'App\\Role', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(16, 'default', 'App\\Role model has been created', 4, 'App\\Role', NULL, NULL, '[]', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(17, 'default', 'App\\Role model has been created', 5, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:46:19', '2018-09-14 06:46:19'),
(18, 'default', 'App\\Role model has been updated', 5, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:47:00', '2018-09-14 06:47:00'),
(19, 'default', 'App\\Role model has been deleted', 5, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:47:26', '2018-09-14 06:47:26'),
(20, 'default', 'App\\Role model has been created', 6, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:49:37', '2018-09-14 06:49:37'),
(21, 'default', 'App\\Role model has been deleted', 6, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:49:41', '2018-09-14 06:49:41'),
(22, 'default', 'App\\Role model has been created', 7, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:50:05', '2018-09-14 06:50:05'),
(23, 'default', 'App\\Role model has been deleted', 7, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:50:08', '2018-09-14 06:50:08'),
(24, 'default', 'App\\Role model has been created', 8, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:52:32', '2018-09-14 06:52:32'),
(25, 'default', 'App\\Role model has been deleted', 8, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:52:36', '2018-09-14 06:52:36'),
(26, 'default', 'App\\Role model has been created', 9, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:53:51', '2018-09-14 06:53:51'),
(27, 'default', 'App\\Role model has been deleted', 9, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 06:57:51', '2018-09-14 06:57:51'),
(28, 'default', 'App\\Permission model has been created', 13, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:15:21', '2018-09-14 08:15:21'),
(29, 'default', 'App\\Permission model has been deleted', 13, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:15:53', '2018-09-14 08:15:53'),
(30, 'default', 'App\\Permission model has been created', 14, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:16:43', '2018-09-14 08:16:43'),
(31, 'default', 'App\\Permission model has been deleted', 14, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:17:46', '2018-09-14 08:17:46'),
(32, 'default', 'App\\Permission model has been created', 15, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:18:39', '2018-09-14 08:18:39'),
(33, 'default', 'App\\Permission model has been deleted', 15, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:18:44', '2018-09-14 08:18:44'),
(34, 'default', 'App\\Role model has been created', 10, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 08:19:40', '2018-09-14 08:19:40'),
(35, 'default', 'App\\Role model has been deleted', 10, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 08:19:44', '2018-09-14 08:19:44'),
(36, 'default', 'App\\Role model has been created', 11, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 08:24:18', '2018-09-14 08:24:18'),
(37, 'default', 'App\\Role model has been deleted', 11, 'App\\Role', 1, 'App\\User', '[]', '2018-09-14 08:24:33', '2018-09-14 08:24:33'),
(38, 'default', 'App\\Permission model has been created', 16, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:25:52', '2018-09-14 08:25:52'),
(39, 'default', 'App\\Permission model has been deleted', 16, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:25:59', '2018-09-14 08:25:59'),
(40, 'default', 'App\\Permission model has been created', 17, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:28:06', '2018-09-14 08:28:06'),
(41, 'default', 'App\\Permission model has been created', 18, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:28:45', '2018-09-14 08:28:45'),
(42, 'default', 'App\\Permission model has been created', 19, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:30:24', '2018-09-14 08:30:24'),
(43, 'default', 'App\\Permission model has been created', 20, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:30:47', '2018-09-14 08:30:47'),
(44, 'default', 'App\\Permission model has been created', 21, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:34:13', '2018-09-14 08:34:13'),
(45, 'default', 'App\\Permission model has been created', 22, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:34:42', '2018-09-14 08:34:42'),
(46, 'default', 'App\\Permission model has been created', 23, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:35:07', '2018-09-14 08:35:07'),
(47, 'default', 'App\\Permission model has been created', 24, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-14 08:35:31', '2018-09-14 08:35:31'),
(48, 'default', 'App\\Permission model has been created', 25, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-16 23:53:08', '2018-09-16 23:53:08'),
(49, 'default', 'created', 5, 'App\\Role', 4, 'App\\User', '[]', '2018-09-17 01:10:26', '2018-09-17 01:10:26'),
(50, 'default', 'created', 6, 'App\\Role', 4, 'App\\User', '[]', '2018-09-17 01:12:41', '2018-09-17 01:12:41'),
(51, 'default', 'deleted', 5, 'App\\Role', 4, 'App\\User', '[]', '2018-09-17 01:14:48', '2018-09-17 01:14:48'),
(52, 'default', 'deleted', 6, 'App\\Role', 4, 'App\\User', '[]', '2018-09-17 01:14:55', '2018-09-17 01:14:55'),
(53, 'default', 'created', 26, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:48:07', '2018-09-17 01:48:07'),
(54, 'default', 'updated', 24, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:48:52', '2018-09-17 01:48:52'),
(55, 'default', 'updated', 23, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:48:57', '2018-09-17 01:48:57'),
(56, 'default', 'updated', 22, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:49:03', '2018-09-17 01:49:03'),
(57, 'default', 'updated', 20, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:49:32', '2018-09-17 01:49:32'),
(58, 'default', 'updated', 19, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:49:37', '2018-09-17 01:49:37'),
(59, 'default', 'updated', 18, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:49:40', '2018-09-17 01:49:40'),
(60, 'default', 'created', 27, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:50:37', '2018-09-17 01:50:37'),
(61, 'default', 'created', 28, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:50:58', '2018-09-17 01:50:58'),
(62, 'default', 'created', 29, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:53:17', '2018-09-17 01:53:17'),
(63, 'default', 'updated', 29, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:53:48', '2018-09-17 01:53:48'),
(64, 'default', 'created', 30, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:54:15', '2018-09-17 01:54:15'),
(65, 'default', 'created', 31, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:54:30', '2018-09-17 01:54:30'),
(66, 'default', 'created', 32, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:54:47', '2018-09-17 01:54:47'),
(67, 'default', 'created', 33, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:55:07', '2018-09-17 01:55:07'),
(68, 'default', 'created', 34, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:55:36', '2018-09-17 01:55:36'),
(69, 'default', 'created', 35, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:55:52', '2018-09-17 01:55:52'),
(70, 'default', 'created', 36, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:56:17', '2018-09-17 01:56:17'),
(71, 'default', 'created', 37, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:57:04', '2018-09-17 01:57:04'),
(72, 'default', 'created', 38, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:57:33', '2018-09-17 01:57:33'),
(73, 'default', 'created', 39, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:57:57', '2018-09-17 01:57:57'),
(74, 'default', 'created', 40, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:58:14', '2018-09-17 01:58:14'),
(75, 'default', 'created', 41, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:58:43', '2018-09-17 01:58:43'),
(76, 'default', 'created', 42, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 01:58:58', '2018-09-17 01:58:58'),
(77, 'default', 'created', 43, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 02:00:54', '2018-09-17 02:00:54'),
(78, 'default', 'created', 44, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 02:01:27', '2018-09-17 02:01:27'),
(79, 'default', 'created', 45, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 02:02:01', '2018-09-17 02:02:01'),
(80, 'default', 'created', 46, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 02:02:25', '2018-09-17 02:02:25'),
(81, 'default', 'updated', 46, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 02:02:42', '2018-09-17 02:02:42'),
(82, 'default', 'updated', 45, 'App\\Permission', 4, 'App\\User', '[]', '2018-09-17 02:02:47', '2018-09-17 02:02:47'),
(83, 'default', 'created', 47, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:32:45', '2018-09-17 03:32:45'),
(84, 'default', 'created', 48, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:33:33', '2018-09-17 03:33:33'),
(85, 'default', 'created', 49, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:34:08', '2018-09-17 03:34:08'),
(86, 'default', 'created', 50, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:41:42', '2018-09-17 03:41:42'),
(87, 'default', 'created', 51, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:41:57', '2018-09-17 03:41:57'),
(88, 'default', 'created', 52, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:42:21', '2018-09-17 03:42:21'),
(89, 'default', 'created', 53, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:43:38', '2018-09-17 03:43:38'),
(90, 'default', 'created', 54, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:43:57', '2018-09-17 03:43:57'),
(91, 'default', 'created', 55, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:44:21', '2018-09-17 03:44:21'),
(92, 'default', 'created', 56, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:44:51', '2018-09-17 03:44:51'),
(93, 'default', 'created', 57, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:45:13', '2018-09-17 03:45:13'),
(94, 'default', 'created', 58, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:45:51', '2018-09-17 03:45:51'),
(95, 'default', 'created', 59, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:46:26', '2018-09-17 03:46:26'),
(96, 'default', 'created', 60, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:46:59', '2018-09-17 03:46:59'),
(97, 'default', 'created', 61, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:47:19', '2018-09-17 03:47:19'),
(98, 'default', 'created', 62, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:48:00', '2018-09-17 03:48:00'),
(99, 'default', 'created', 63, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:48:19', '2018-09-17 03:48:19'),
(100, 'default', 'created', 64, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:48:35', '2018-09-17 03:48:35'),
(101, 'default', 'created', 65, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:49:10', '2018-09-17 03:49:10'),
(102, 'default', 'created', 66, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:49:28', '2018-09-17 03:49:28'),
(103, 'default', 'created', 67, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:49:50', '2018-09-17 03:49:50'),
(104, 'default', 'created', 68, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:50:44', '2018-09-17 03:50:44'),
(105, 'default', 'created', 69, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:51:05', '2018-09-17 03:51:05'),
(106, 'default', 'created', 70, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:51:34', '2018-09-17 03:51:34'),
(107, 'default', 'created', 71, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:52:19', '2018-09-17 03:52:19'),
(108, 'default', 'created', 72, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:52:39', '2018-09-17 03:52:39'),
(109, 'default', 'created', 73, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:52:58', '2018-09-17 03:52:58'),
(110, 'default', 'created', 74, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:53:36', '2018-09-17 03:53:36'),
(111, 'default', 'updated', 74, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:53:47', '2018-09-17 03:53:47'),
(112, 'default', 'created', 75, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:54:16', '2018-09-17 03:54:16'),
(113, 'default', 'created', 76, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:54:34', '2018-09-17 03:54:34'),
(114, 'default', 'created', 77, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:55:43', '2018-09-17 03:55:43'),
(115, 'default', 'created', 78, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:56:02', '2018-09-17 03:56:02'),
(116, 'default', 'created', 79, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:56:23', '2018-09-17 03:56:23'),
(117, 'default', 'created', 80, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:56:59', '2018-09-17 03:56:59'),
(118, 'default', 'created', 81, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:57:48', '2018-09-17 03:57:48'),
(119, 'default', 'created', 82, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:58:01', '2018-09-17 03:58:01'),
(120, 'default', 'created', 83, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:58:23', '2018-09-17 03:58:23'),
(121, 'default', 'created', 84, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:58:37', '2018-09-17 03:58:37'),
(122, 'default', 'created', 85, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:58:55', '2018-09-17 03:58:55'),
(123, 'default', 'created', 86, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:59:27', '2018-09-17 03:59:27'),
(124, 'default', 'updated', 86, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 03:59:50', '2018-09-17 03:59:50'),
(125, 'default', 'created', 87, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 04:00:09', '2018-09-17 04:00:09'),
(126, 'default', 'created', 88, 'App\\Permission', 1, 'App\\User', '[]', '2018-09-17 04:00:23', '2018-09-17 04:00:23');

-- --------------------------------------------------------

--
-- Table structure for table `attachment`
--

CREATE TABLE `attachment` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attachment`
--

INSERT INTO `attachment` (`id`, `employee_id`, `file`, `key`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 1, 'uploads/employee/scanned_photo/_15366592885b978f5855c51.jpg', 'scanned_photo', '2018-09-11 07:24:44', '2018-09-11 04:18:08', '2018-09-11 07:24:44'),
(3, 2, 'uploads/employee/scanned_photo/Mayur_vadher_15366597165b979104d2b91.jpg', 'scanned_photo', '2018-09-11 07:26:59', '2018-09-11 04:25:16', '2018-09-11 07:26:59'),
(4, 2, 'uploads/employee/employment_form/Mayur_vadher_15366597165b979104d416d.xls', 'employment_form', '2018-09-11 07:26:59', '2018-09-11 04:25:16', '2018-09-11 07:26:59'),
(5, 2, 'uploads/employee/form_11/Mayur_vadher_15366597165b979104d57b8.jpg', 'form_11', '2018-09-11 07:26:59', '2018-09-11 04:25:16', '2018-09-11 07:26:59'),
(6, 2, 'uploads/employee/form_02/Mayur_vadher_15366597165b979104d7a75.jpg', 'form_02', '2018-09-11 07:26:59', '2018-09-11 04:25:16', '2018-09-11 07:26:59'),
(7, 2, 'uploads/employee/form_f/Mayur_vadher_15366597165b979104d97ac.jpg', 'form_f', '2018-09-11 07:26:59', '2018-09-11 04:25:16', '2018-09-11 07:26:59'),
(8, 2, 'uploads/employee/hiring_checklist/Mayur_vadher_15366597165b979104dabbb.jpg', 'hiring_checklist', '2018-09-11 07:26:59', '2018-09-11 04:25:16', '2018-09-11 07:26:59'),
(9, 2, 'uploads/employee/inteview_assessment/Mayur_vadher_15366597165b979104dc177.jpg', 'inteview_assessment', '2018-09-11 07:26:59', '2018-09-11 04:25:16', '2018-09-11 07:26:59'),
(10, 2, 'uploads/employee/resume/Mayur_vadher_15366597165b979104dcfdf.jpg', 'resume', '2018-09-11 07:26:59', '2018-09-11 04:25:16', '2018-09-11 07:26:59'),
(11, 4, 'uploads/employee/scanned_photo/Mayur_vadher_15366598385b97917e0c503.jpg', 'scanned_photo', '2018-09-11 06:49:39', '2018-09-11 04:27:18', '2018-09-11 06:49:39'),
(12, 4, 'uploads/employee/employment_form/Mayur_vadher_15366598385b97917e0db47.xls', 'employment_form', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(13, 4, 'uploads/employee/form_11/Mayur_vadher_15366598385b97917e0ea50.jpg', 'form_11', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(14, 4, 'uploads/employee/form_02/Mayur_vadher_15366598385b97917e0ff7a.jpg', 'form_02', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(15, 4, 'uploads/employee/form_f/Mayur_vadher_15366598385b97917e11302.jpg', 'form_f', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(16, 4, 'uploads/employee/hiring_checklist/Mayur_vadher_15366598385b97917e12704.jpg', 'hiring_checklist', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(17, 4, 'uploads/employee/inteview_assessment/Mayur_vadher_15366598385b97917e13c7f.jpg', 'inteview_assessment', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(18, 4, 'uploads/employee/resume/Mayur_vadher_15366598385b97917e14ff6.jpg', 'resume', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(19, 4, 'uploads/employee/education_documents/Mayur_vadher_15366598385b97917e15f13.jpg', 'education_documents', '2018-09-11 06:52:13', '2018-09-11 04:27:18', '2018-09-11 06:52:13'),
(20, 4, 'uploads/employee/education_documents/Mayur_vadher_15366598385b97917e17174.jpg', 'education_documents', '2018-09-11 06:52:13', '2018-09-11 04:27:18', '2018-09-11 06:52:13'),
(21, 4, 'uploads/employee/aadhar_card/Mayur_vadher_15366598385b97917e17f4d.jpg', 'aadhar_card', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(22, 4, 'uploads/employee/pan_card/Mayur_vadher_15366598385b97917e19281.jpg', 'pan_card', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(23, 4, 'uploads/employee/bank_passbook/Mayur_vadher_15366598385b97917e1a15a.jpg', 'bank_passbook', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(24, 4, 'uploads/employee/offer_accept/Mayur_vadher_15366598385b97917e1b53b.jpg', 'offer_accept', '2018-09-11 07:35:03', '2018-09-11 04:27:18', '2018-09-11 07:35:03'),
(25, 4, 'uploads/employee/previous_company_docs/Mayur_vadher_15366598385b97917e1c1fd.jpg', 'previous_company_docs', '2018-09-11 06:52:58', '2018-09-11 04:27:18', '2018-09-11 06:52:58'),
(26, 4, 'uploads/employee/previous_company_docs/Mayur_vadher_15366598385b97917e1d5c8.jpg', 'previous_company_docs', '2018-09-11 06:52:58', '2018-09-11 04:27:18', '2018-09-11 06:52:58'),
(27, 4, 'uploads/employee/previous_company_docs/Mayur_vadher_15366598385b97917e1e6fd.jpg', 'previous_company_docs', '2018-09-11 06:52:58', '2018-09-11 04:27:18', '2018-09-11 06:52:58'),
(28, 4, 'uploads/employee/scanned_photo/Mayur_vadher_15366683795b97b2db52a91.jpg', 'scanned_photo', '2018-09-11 06:51:30', '2018-09-11 06:49:39', '2018-09-11 06:51:30'),
(29, 4, 'uploads/employee/scanned_photo/Mayur_vadher_15366684905b97b34a699ce.jpg', 'scanned_photo', '2018-09-11 07:35:03', '2018-09-11 06:51:30', '2018-09-11 07:35:03'),
(30, 4, 'uploads/employee/education_documents/Mayur_vadher_15366685335b97b375f1979.jpg', 'education_documents', '2018-09-11 07:35:03', '2018-09-11 06:52:13', '2018-09-11 07:35:03'),
(31, 4, 'uploads/employee/previous_company_docs/Mayur_vadher_15366685785b97b3a28fefe.pdf', 'previous_company_docs', '2018-09-11 07:35:03', '2018-09-11 06:52:58', '2018-09-11 07:35:03'),
(32, 4, 'uploads/employee/previous_company_docs/Mayur_vadher_15366685785b97b3a291922.xls', 'previous_company_docs', '2018-09-11 07:35:03', '2018-09-11 06:52:58', '2018-09-11 07:35:03'),
(33, 5, 'uploads/employee/scanned_photo/Mayur_vadher_15366713415b97be6d9d0d2.jpg', 'scanned_photo', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(34, 5, 'uploads/employee/employment_form/Mayur_vadher_15366713415b97be6d9ea9a.xls', 'employment_form', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(35, 5, 'uploads/employee/form_11/Mayur_vadher_15366713415b97be6da0733.jpg', 'form_11', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(36, 5, 'uploads/employee/form_02/Mayur_vadher_15366713415b97be6da2d12.jpg', 'form_02', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(37, 5, 'uploads/employee/form_f/Mayur_vadher_15366713415b97be6da590b.jpg', 'form_f', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(38, 5, 'uploads/employee/hiring_checklist/Mayur_vadher_15366713415b97be6da9378.jpg', 'hiring_checklist', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(39, 5, 'uploads/employee/inteview_assessment/Mayur_vadher_15366713415b97be6dab843.jpg', 'inteview_assessment', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(40, 5, 'uploads/employee/resume/Mayur_vadher_15366713415b97be6dad775.jpg', 'resume', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(41, 5, 'uploads/employee/education_documents/Mayur_vadher_15366713415b97be6daed49.jpg', 'education_documents', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(42, 5, 'uploads/employee/education_documents/Mayur_vadher_15366713415b97be6db03c0.pdf', 'education_documents', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(43, 5, 'uploads/employee/education_documents/Mayur_vadher_15366713415b97be6db37f6.xls', 'education_documents', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(44, 5, 'uploads/employee/aadhar_card/Mayur_vadher_15366713415b97be6db5e9f.jpg', 'aadhar_card', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(45, 5, 'uploads/employee/pan_card/Mayur_vadher_15366713415b97be6db86d5.jpg', 'pan_card', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(46, 5, 'uploads/employee/bank_passbook/Mayur_vadher_15366713415b97be6dba9a0.jpg', 'bank_passbook', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(47, 5, 'uploads/employee/offer_accept/Mayur_vadher_15366713415b97be6dbd131.jpg', 'offer_accept', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(48, 5, 'uploads/employee/previous_company_docs/Mayur_vadher_15366713415b97be6dbf874.jpg', 'previous_company_docs', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(49, 5, 'uploads/employee/previous_company_docs/Mayur_vadher_15366713415b97be6dc1ea7.jpg', 'previous_company_docs', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(50, 5, 'uploads/employee/previous_company_docs/Mayur_vadher_15366713415b97be6dc3d31.pdf', 'previous_company_docs', '2018-09-11 07:54:58', '2018-09-11 07:39:01', '2018-09-11 07:54:58'),
(51, 6, 'uploads/employee/scanned_photo/Mayur_Vadher_15366725975b97c35520d69.jpg', 'scanned_photo', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(52, 6, 'uploads/employee/employment_form/Mayur_Vadher_15366725975b97c355220f0.xls', 'employment_form', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(53, 6, 'uploads/employee/form_11/Mayur_Vadher_15366725975b97c35523398.jpg', 'form_11', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(54, 6, 'uploads/employee/form_02/Mayur_Vadher_15366725975b97c3552463e.jpg', 'form_02', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(55, 6, 'uploads/employee/form_f/Mayur_Vadher_15366725975b97c35525651.jpg', 'form_f', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(56, 6, 'uploads/employee/hiring_checklist/Mayur_Vadher_15366725975b97c35526a40.jpg', 'hiring_checklist', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(57, 6, 'uploads/employee/inteview_assessment/Mayur_Vadher_15366725975b97c35527827.jpg', 'inteview_assessment', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(58, 6, 'uploads/employee/resume/Mayur_Vadher_15366725975b97c35528bb0.jpg', 'resume', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(59, 6, 'uploads/employee/education_documents/Mayur_Vadher_15366725975b97c35529c9e.jpg', 'education_documents', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(60, 6, 'uploads/employee/aadhar_card/Mayur_Vadher_15366725975b97c3552afeb.jpg', 'aadhar_card', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(61, 6, 'uploads/employee/pan_card/Mayur_Vadher_15366725975b97c3552bfb7.jpg', 'pan_card', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(62, 6, 'uploads/employee/bank_passbook/Mayur_Vadher_15366725975b97c3552d3bc.jpg', 'bank_passbook', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(63, 6, 'uploads/employee/offer_accept/Mayur_Vadher_15366725975b97c3552e3fe.png', 'offer_accept', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(64, 6, 'uploads/employee/previous_company_docs/Mayur_Vadher_15366725975b97c3552f714.jpg', 'previous_company_docs', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(65, 6, 'uploads/employee/previous_company_docs/Mayur_Vadher_15366725975b97c35530cec.jpg', 'previous_company_docs', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(66, 6, 'uploads/employee/previous_company_docs/Mayur_Vadher_15366725975b97c355319fb.pdf', 'previous_company_docs', '2018-09-12 04:40:48', '2018-09-11 07:59:57', '2018-09-12 04:40:48'),
(67, 1, 'uploads/employee/scanned_photo/Mayur_Vadher_15367478815b98e9699838f.jpg', 'scanned_photo', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(68, 1, 'uploads/employee/employment_form/Mayur_Vadher_15367478815b98e9699a778.xls', 'employment_form', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(69, 1, 'uploads/employee/form_11/Mayur_Vadher_15367478815b98e9699c735.jpg', 'form_11', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(70, 1, 'uploads/employee/form_02/Mayur_Vadher_15367478815b98e9699e55d.jpg', 'form_02', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(71, 1, 'uploads/employee/form_f/Mayur_Vadher_15367478815b98e9699f91b.jpg', 'form_f', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(72, 1, 'uploads/employee/hiring_checklist/Mayur_Vadher_15367478815b98e969a0b01.jpg', 'hiring_checklist', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(73, 1, 'uploads/employee/inteview_assessment/Mayur_Vadher_15367478815b98e969a1f21.jpg', 'inteview_assessment', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(74, 1, 'uploads/employee/resume/Mayur_Vadher_15367478815b98e969a30e0.jpg', 'resume', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(75, 1, 'uploads/employee/education_documents/Mayur_Vadher_15367478815b98e969a4505.jpg', 'education_documents', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(76, 1, 'uploads/employee/aadhar_card/Mayur_Vadher_15367478815b98e969a532e.jpg', 'aadhar_card', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(77, 1, 'uploads/employee/pan_card/Mayur_Vadher_15367478815b98e969a63a6.jpg', 'pan_card', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(78, 1, 'uploads/employee/bank_passbook/Mayur_Vadher_15367478815b98e969a7295.jpg', 'bank_passbook', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(79, 1, 'uploads/employee/offer_accept/Mayur_Vadher_15367478815b98e969a8478.jpg', 'offer_accept', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(80, 1, 'uploads/employee/previous_company_docs/Mayur_Vadher_15367478815b98e969aa26e.jpg', 'previous_company_docs', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(81, 1, 'uploads/employee/previous_company_docs/Mayur_Vadher_15367478815b98e969abf5a.jpg', 'previous_company_docs', NULL, '2018-09-12 04:54:41', '2018-09-12 04:54:41'),
(82, 2, 'uploads/employee/scanned_photo/Selma_Valentine_15367591215b99155102339.jpg', 'scanned_photo', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(83, 2, 'uploads/employee/employment_form/Selma_Valentine_15367591215b9915510521e.xls', 'employment_form', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(84, 2, 'uploads/employee/form_11/Selma_Valentine_15367591215b9915510716c.jpg', 'form_11', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(85, 2, 'uploads/employee/form_02/Selma_Valentine_15367591215b99155109a18.jpg', 'form_02', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(86, 2, 'uploads/employee/form_f/Selma_Valentine_15367591215b9915510b9b3.jpg', 'form_f', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(87, 2, 'uploads/employee/hiring_checklist/Selma_Valentine_15367591215b9915510dd27.jpg', 'hiring_checklist', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(88, 2, 'uploads/employee/inteview_assessment/Selma_Valentine_15367591215b99155110108.jpg', 'inteview_assessment', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(89, 2, 'uploads/employee/resume/Selma_Valentine_15367591215b99155111ba3.jpg', 'resume', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(90, 2, 'uploads/employee/education_documents/Selma_Valentine_15367591215b9915511353f.jpg', 'education_documents', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(91, 2, 'uploads/employee/education_documents/Selma_Valentine_15367591215b991551159e3.jpg', 'education_documents', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(92, 2, 'uploads/employee/education_documents/Selma_Valentine_15367591215b99155117772.pdf', 'education_documents', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(93, 2, 'uploads/employee/aadhar_card/Selma_Valentine_15367591215b99155119156.jpg', 'aadhar_card', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(94, 2, 'uploads/employee/pan_card/Selma_Valentine_15367591215b9915511bdd6.jpg', 'pan_card', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(95, 2, 'uploads/employee/bank_passbook/Selma_Valentine_15367591215b9915511f8cc.jpg', 'bank_passbook', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(96, 2, 'uploads/employee/offer_accept/Selma_Valentine_15367591215b991551232e4.jpg', 'offer_accept', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(97, 2, 'uploads/employee/previous_company_docs/Selma_Valentine_15367591215b99155125a1b.jpg', 'previous_company_docs', NULL, '2018-09-12 08:02:01', '2018-09-12 08:02:01'),
(98, 3, 'uploads/employee/scanned_photo/Leo_Good_15368253465b9a1802b266d.jpg', 'scanned_photo', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(99, 3, 'uploads/employee/employment_form/Leo_Good_15368253465b9a1802b60c4.xls', 'employment_form', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(100, 3, 'uploads/employee/form_11/Leo_Good_15368253465b9a1802b88df.jpg', 'form_11', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(101, 3, 'uploads/employee/form_02/Leo_Good_15368253465b9a1802ba60a.jpg', 'form_02', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(102, 3, 'uploads/employee/form_f/Leo_Good_15368253465b9a1802bbdd1.pdf', 'form_f', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(103, 3, 'uploads/employee/hiring_checklist/Leo_Good_15368253465b9a1802bd419.jpg', 'hiring_checklist', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(104, 3, 'uploads/employee/inteview_assessment/Leo_Good_15368253465b9a1802be863.jpg', 'inteview_assessment', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(105, 3, 'uploads/employee/resume/Leo_Good_15368253465b9a1802bf997.jpg', 'resume', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(106, 3, 'uploads/employee/education_documents/Leo_Good_15368253465b9a1802c0e6d.jpg', 'education_documents', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(107, 3, 'uploads/employee/education_documents/Leo_Good_15368253465b9a1802c2303.jpg', 'education_documents', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(108, 3, 'uploads/employee/education_documents/Leo_Good_15368253465b9a1802c3093.pdf', 'education_documents', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(109, 3, 'uploads/employee/aadhar_card/Leo_Good_15368253465b9a1802c4356.jpg', 'aadhar_card', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(110, 3, 'uploads/employee/pan_card/Leo_Good_15368253465b9a1802c535b.jpg', 'pan_card', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(111, 3, 'uploads/employee/bank_passbook/Leo_Good_15368253465b9a1802c6b88.jpg', 'bank_passbook', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(112, 3, 'uploads/employee/offer_accept/Leo_Good_15368253465b9a1802c7f4c.jpg', 'offer_accept', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(113, 3, 'uploads/employee/previous_company_docs/Leo_Good_15368253465b9a1802c90a6.jpg', 'previous_company_docs', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(114, 3, 'uploads/employee/previous_company_docs/Leo_Good_15368253465b9a1802ca4f9.jpg', 'previous_company_docs', NULL, '2018-09-13 02:25:46', '2018-09-13 02:25:46'),
(115, 4, 'uploads/employee/scanned_photo/Nehru_Silva_15368346255b9a3c4187012.jpg', 'scanned_photo', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(116, 4, 'uploads/employee/employment_form/Nehru_Silva_15368346255b9a3c418964d.xls', 'employment_form', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(117, 4, 'uploads/employee/form_11/Nehru_Silva_15368346255b9a3c418aee7.jpg', 'form_11', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(118, 4, 'uploads/employee/form_02/Nehru_Silva_15368346255b9a3c418c619.jpg', 'form_02', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(119, 4, 'uploads/employee/form_f/Nehru_Silva_15368346255b9a3c418ded7.jpg', 'form_f', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(120, 4, 'uploads/employee/hiring_checklist/Nehru_Silva_15368346255b9a3c418f782.jpg', 'hiring_checklist', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(121, 4, 'uploads/employee/inteview_assessment/Nehru_Silva_15368346255b9a3c4190b6b.jpg', 'inteview_assessment', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(122, 4, 'uploads/employee/resume/Nehru_Silva_15368346255b9a3c41920bc.jpg', 'resume', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(123, 4, 'uploads/employee/education_documents/Nehru_Silva_15368346255b9a3c41934df.jpg', 'education_documents', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(124, 4, 'uploads/employee/aadhar_card/Nehru_Silva_15368346255b9a3c419486c.jpg', 'aadhar_card', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(125, 4, 'uploads/employee/pan_card/Nehru_Silva_15368346255b9a3c419704e.jpg', 'pan_card', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(126, 4, 'uploads/employee/bank_passbook/Nehru_Silva_15368346255b9a3c41988df.jpg', 'bank_passbook', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(127, 4, 'uploads/employee/offer_accept/Nehru_Silva_15368346255b9a3c419a324.jpg', 'offer_accept', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(128, 4, 'uploads/employee/previous_company_docs/Nehru_Silva_15368346255b9a3c419bc6e.jpg', 'previous_company_docs', NULL, '2018-09-13 05:00:25', '2018-09-13 05:00:25'),
(129, 5, 'uploads/employee/scanned_photo/Edward_Yang_15369165015b9b7c15f1675.jpg', 'scanned_photo', NULL, '2018-09-14 03:45:01', '2018-09-14 03:45:01'),
(130, 5, 'uploads/employee/employment_form/Edward_Yang_15369165015b9b7c15f3e35.xls', 'employment_form', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(131, 5, 'uploads/employee/form_11/Edward_Yang_15369165025b9b7c1601365.jpg', 'form_11', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(132, 5, 'uploads/employee/form_02/Edward_Yang_15369165025b9b7c1602c7f.jpg', 'form_02', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(133, 5, 'uploads/employee/form_f/Edward_Yang_15369165025b9b7c1604562.jpg', 'form_f', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(134, 5, 'uploads/employee/hiring_checklist/Edward_Yang_15369165025b9b7c1607579.jpg', 'hiring_checklist', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(135, 5, 'uploads/employee/inteview_assessment/Edward_Yang_15369165025b9b7c160a05f.jpg', 'inteview_assessment', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(136, 5, 'uploads/employee/resume/Edward_Yang_15369165025b9b7c160c6ae.jpg', 'resume', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(137, 5, 'uploads/employee/education_documents/Edward_Yang_15369165025b9b7c160e485.jpg', 'education_documents', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(138, 5, 'uploads/employee/education_documents/Edward_Yang_15369165025b9b7c160feaf.jpg', 'education_documents', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(139, 5, 'uploads/employee/aadhar_card/Edward_Yang_15369165025b9b7c16114aa.jpg', 'aadhar_card', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(140, 5, 'uploads/employee/pan_card/Edward_Yang_15369165025b9b7c1612d41.jpg', 'pan_card', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(141, 5, 'uploads/employee/bank_passbook/Edward_Yang_15369165025b9b7c1615ff9.jpg', 'bank_passbook', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(142, 5, 'uploads/employee/offer_accept/Edward_Yang_15369165025b9b7c16182bf.jpg', 'offer_accept', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02'),
(143, 5, 'uploads/employee/previous_company_docs/Edward_Yang_15369165025b9b7c161b957.jpg', 'previous_company_docs', NULL, '2018-09-14 03:45:02', '2018-09-14 03:45:02');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'UNLIMITED BRAND', 1, NULL, '2018-09-10 18:30:00', '2018-09-14 01:27:14'),
(2, 'Unlimited test', 0, '2018-09-13 07:38:28', '2018-09-13 07:38:05', '2018-09-13 07:38:28'),
(3, 'test2 brand', 1, NULL, '2018-09-18 01:28:14', '2018-09-18 01:28:14'),
(4, 'test brand', 1, NULL, '2018-09-18 02:34:23', '2018-09-18 02:34:23'),
(5, 'UNLIMITED', 1, NULL, '2018-09-18 03:57:45', '2018-09-18 03:57:45'),
(6, 'IT', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `business_unit`
--

CREATE TABLE `business_unit` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `business_unit`
--

INSERT INTO `business_unit` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Store', 1, NULL, '2018-09-10 18:30:00', '2018-09-14 01:28:15'),
(2, 'test test', 1, '2018-09-13 07:30:47', '2018-09-13 07:30:22', '2018-09-13 07:30:47'),
(3, 'test businessunit', 1, NULL, '2018-09-18 01:24:05', '2018-09-18 01:24:05'),
(4, 'test brand', 1, NULL, '2018-09-18 01:26:54', '2018-09-18 01:26:54'),
(5, 'test2 brand', 1, NULL, '2018-09-18 01:27:28', '2018-09-18 01:27:28'),
(6, 'IT', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(10) UNSIGNED NOT NULL,
  `state_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `state_id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'ahmedabad', 1, NULL, '2018-09-10 18:30:00', '2018-09-14 01:05:57'),
(2, 1, 'ahmedabad', 1, '2018-09-14 01:07:45', '2018-08-10 01:47:55', '2018-09-14 01:07:45'),
(3, 2, 'Mumbai', 1, NULL, '2018-09-14 00:32:38', '2018-09-14 00:33:10'),
(4, 4, 'delhi', 1, '2018-09-14 00:37:03', '2018-09-14 00:35:15', '2018-09-14 00:37:03'),
(5, 4, 'delhi', 1, '2018-09-14 00:37:03', '2018-09-14 00:35:15', '2018-09-14 00:37:03'),
(6, 4, 'delhi', 1, '2018-09-14 00:37:03', '2018-09-14 00:35:35', '2018-09-14 00:37:03'),
(7, 2, 'rajkot', 1, '2018-09-14 00:44:37', '2018-09-14 00:44:30', '2018-09-14 00:44:37'),
(8, 1, 'rajkot', 1, NULL, '2018-09-14 01:03:44', '2018-09-14 01:10:00'),
(9, 1, 'vadodara', 1, NULL, '2018-09-14 01:10:21', '2018-09-14 01:10:27'),
(10, 5, 'amreli', 1, NULL, '2018-09-18 01:42:59', '2018-09-18 01:42:59'),
(11, 6, 'Bangalore', 1, NULL, '2018-09-18 04:59:03', '2018-09-18 04:59:03'),
(12, 2, 'pune', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Operations', 1, NULL, '2018-08-09 23:44:17', '2018-09-17 04:20:19'),
(2, 'test test', 1, '2018-09-13 06:56:02', '2018-09-13 06:55:51', '2018-09-13 06:56:02'),
(3, 'sales', 1, '2018-09-13 07:17:50', '2018-09-13 07:15:46', '2018-09-13 07:17:50'),
(4, 'sales', 1, NULL, '2018-09-17 04:58:51', '2018-09-17 04:58:51'),
(5, 'test department', 1, NULL, '2018-09-18 01:03:41', '2018-09-18 01:03:41'),
(6, 'operation', 1, NULL, '2018-09-18 02:34:23', '2018-09-18 02:34:23'),
(7, 'IT', 1, NULL, NULL, NULL),
(8, 'IT2', 1, NULL, NULL, NULL),
(9, 'mayur', 1, NULL, '2018-09-18 07:40:58', '2018-09-18 07:40:58');

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Area Manager', 1, NULL, '2018-09-10 04:09:25', NULL),
(2, 'Assistant - Admin', 1, NULL, '2018-08-10 01:47:55', '2018-09-14 01:33:47'),
(3, 'Test', 1, '2018-09-13 05:56:32', '2018-09-13 05:56:22', '2018-09-13 05:56:32'),
(4, 'test test', 1, '2018-09-13 05:56:46', '2018-09-13 05:56:38', '2018-09-13 05:56:46'),
(5, 'test designation', 1, NULL, '2018-09-18 01:03:41', '2018-09-18 01:03:41'),
(6, 'developer', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `joining_date` date NOT NULL,
  `grade_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `sub_department_id` int(11) NOT NULL,
  `business_unit_id` int(11) NOT NULL,
  `reporting_manager_employee_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reporting_manager_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_id` int(11) NOT NULL,
  `legal_entity_id` int(11) NOT NULL,
  `location_code` int(11) NOT NULL,
  `io_code` int(11) NOT NULL,
  `employee_classification_id` int(11) NOT NULL,
  `source_category_id` int(11) NOT NULL,
  `employee_refferal_amount` double NOT NULL,
  `employee_refferal_payment_month` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ctc` double NOT NULL,
  `variable_pay_type_id` int(11) DEFAULT NULL,
  `soft_copy_attached` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL,
  `pod_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `store_name` int(11) NOT NULL,
  `regional_note` text COLLATE utf8mb4_unicode_ci,
  `hrms_note` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `regional_status` tinyint(4) NOT NULL DEFAULT '0',
  `hrms_status` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `user_id`, `name`, `joining_date`, `grade_id`, `designation_id`, `department_id`, `sub_department_id`, `business_unit_id`, `reporting_manager_employee_code`, `reporting_manager_name`, `brand_id`, `legal_entity_id`, `location_code`, `io_code`, `employee_classification_id`, `source_category_id`, `employee_refferal_amount`, `employee_refferal_payment_month`, `ctc`, `variable_pay_type_id`, `soft_copy_attached`, `pod_no`, `city_id`, `state_id`, `region_id`, `store_name`, `regional_note`, `hrms_note`, `status`, `regional_status`, `hrms_status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Mayur Vadher', '2011-12-31', 1, 2, 1, 1, 1, '12456', '1245648', 1, 1, 2, 2, 1, 1, 78454, 'February', 10000, 1, 'yes', '784154', 1, 1, 1, 2, 'Not valid', 'check proper', 'confirm', 1, 3, NULL, '2018-09-12 04:54:41', '2018-09-13 04:56:35'),
(2, 2, 'Selma Valentine', '2018-09-12', 1, 1, 1, 1, 1, 'Dolor alias asperiores consequatur consequatur consectetur incididunt sit aliquam', 'Linda Haley', 1, 1, 2, 2, 1, 1, 15000, 'July', 10000, 1, 'no', 'Quia necessitatibus dolores ad velit ut qui aliquip sit voluptatem Sit sit officiis voluptates soluta cum nulla', 1, 1, 1, 2, 'document not proper', 'Please Check Proper', 'confirm', 1, 3, NULL, '2018-09-12 08:02:01', '2018-09-14 01:50:20'),
(3, 4, 'Leo Good', '1998-12-28', 1, 2, 1, 1, 1, '87845SD', 'Elizabeth Jennings', 1, 1, 2, 2, 1, 1, 788, 'Jun', 15000, 1, 'yes', '745dsf', 1, 1, 1, 2, NULL, 'yrdy', 'confirm', 1, 3, NULL, '2018-09-13 02:25:46', '2018-09-13 05:02:48'),
(4, 4, 'Nehru Silva', '1991-12-09', 1, 1, 1, 1, 1, 'Quo et cupiditate et quia qui omnis consequatur Sit vero autem sed ullamco', 'Herman Farley', 1, 1, 2, 2, 1, 1, 1000, 'November', 5000, 1, 'yes', '48745', 1, 1, 1, 2, NULL, 'tyest', 'confirm', 1, 3, NULL, '2018-09-13 05:00:25', '2018-09-13 05:02:10'),
(5, 4, 'Edward Yang', '2000-12-20', 1, 2, 1, 1, 1, 'Et exercitation quia totam deleniti est velit non quo ipsa aliquid ut tempora at ex quia neque quae incididunt', 'Imelda Oneill', 1, 1, 2, 2, 1, 1, 4000, 'October', 10000, 1, 'yes', 'Quasi deserunt quia maxime et officia', 3, 2, 4, 2, 'test once', 'test', 'confirm', 1, 3, NULL, '2018-09-14 03:45:01', '2018-09-14 03:50:05'),
(6, 2, 'Mayur vadher', '2017-05-20', 4, 5, 5, 6, 3, '123456', 'snehal patel', 3, 3, 2, 2, 3, 1, 1000, 'february', 10000, 4, 'yes', '123456', 10, 5, 5, 2, NULL, NULL, '1', 0, 0, '2018-09-18 03:51:42', NULL, '2018-09-18 03:51:42'),
(7, 2, 'Mayur vadher', '2017-05-20', 1, 1, 6, 7, 1, '123456', 'snehal patel', 4, 4, 2, 2, 1, 1, 1000, 'february', 10000, 1, 'yes', '123456', 8, 1, 1, 2, NULL, NULL, '1', 0, 0, '2018-09-18 03:51:38', NULL, '2018-09-18 03:51:38'),
(8, 2, 'Mayur vadher', '2017-05-20', 4, 5, 5, 6, 3, '123456', 'snehal patel', 3, 3, 2, 2, 3, 1, 1000, 'february', 10000, 4, 'yes', '123456', 10, 5, 5, 2, NULL, NULL, 'pending', 0, 0, '2018-09-18 03:52:47', NULL, '2018-09-18 03:52:47'),
(9, 2, 'Mayur vadher', '2017-05-20', 1, 1, 6, 7, 1, '123456', 'snehal patel', 4, 4, 2, 2, 1, 1, 1000, 'february', 10000, 1, 'yes', '123456', 8, 1, 1, 2, NULL, NULL, 'pending', 0, 0, '2018-09-18 03:52:44', NULL, '2018-09-18 03:52:44'),
(10, 2, 'Mayur vadher', '2017-05-20', 4, 5, 5, 8, 3, '123456', 'snehal patel', 3, 3, 2, 2, 3, 1, 1000, 'february', 10000, 4, 'yes', '123456', 10, 5, 5, 2, NULL, NULL, 'pending', 0, 0, NULL, NULL, NULL),
(11, 2, 'Mayur vadher', '2017-05-20', 1, 1, 6, 7, 1, '123456', 'snehal patel', 4, 4, 2, 2, 1, 1, 1000, 'february', 10000, 1, 'yes', '123456', 8, 1, 1, 2, NULL, NULL, 'pending', 0, 0, NULL, NULL, NULL),
(12, 2, 'Leo Good', '2017-05-20', 1, 1, 6, 7, 1, '123456', 'snehal patel', 5, 5, 2, 2, 1, 1, 1000, 'february', 10000, 1, 'yes', '123456', 8, 1, 1, 2, NULL, NULL, 'pending', 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_classification`
--

CREATE TABLE `employee_classification` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_classification`
--

INSERT INTO `employee_classification` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Store', 1, NULL, '2018-09-10 18:30:00', '2018-09-14 01:24:10'),
(2, 'test test', 1, '2018-09-13 08:05:11', '2018-09-13 08:02:50', '2018-09-13 08:05:11'),
(3, 'test emp classs', 1, NULL, '2018-09-18 01:33:53', '2018-09-18 01:33:53'),
(4, 'IT', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Assistant', 1, NULL, NULL, '2018-09-14 01:32:48'),
(2, 'test test', 1, '2018-09-13 06:42:13', '2018-09-13 06:40:42', '2018-09-13 06:42:13'),
(3, 'store', 1, NULL, '2018-09-18 00:51:22', '2018-09-18 00:51:22'),
(4, 'test', 1, NULL, '2018-09-18 01:03:41', '2018-09-18 01:03:41'),
(5, 'IT', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `legal_entity`
--

CREATE TABLE `legal_entity` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `legal_entity`
--

INSERT INTO `legal_entity` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Arvind Lifestyle Brands Limited', 1, NULL, '2018-08-09 23:44:17', '2018-09-14 01:26:03'),
(2, 'test test', 0, '2018-09-13 07:50:39', '2018-09-13 07:50:23', '2018-09-13 07:50:39'),
(3, 'test leg entity', 1, NULL, '2018-09-18 01:33:53', '2018-09-18 01:33:53'),
(4, 'arvind', 1, NULL, '2018-09-18 02:34:23', '2018-09-18 02:34:23'),
(5, 'Arvind Lifestyle', 1, NULL, '2018-09-18 03:57:45', '2018-09-18 03:57:45'),
(6, 'IT', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_193651_create_roles_permissions_tables', 1),
(4, '2018_08_01_183154_create_pages_table', 1),
(5, '2018_08_04_122319_create_settings_table', 1),
(6, '2018_09_06_140942_create_activity_log_table', 1),
(7, '2018_09_07_110553_create_sessions_table', 1),
(10, '2018_09_10_073908_create_grade_table', 2),
(11, '2018_09_10_094227_create_store_location_table', 2),
(12, '2018_09_10_094824_create_department_table', 3),
(13, '2018_09_10_095625_create_sub_department_table', 4),
(14, '2018_09_10_095752_create_business_unit_table', 5),
(15, '2018_09_10_095934_create_brand_table', 6),
(16, '2018_09_10_100003_create_legal_entity_table', 6),
(17, '2018_09_10_100045_create_employee_classification_table', 6),
(18, '2018_09_10_100113_create_source_category_table', 6),
(19, '2018_09_10_100225_create_variable_pay_type_table', 7),
(20, '2018_09_10_100301_create_attachment_table', 7),
(25, '2018_09_10_100756_create_employee_table', 8),
(26, '2018_09_10_122335_create_state_table', 8),
(27, '2018_09_10_122702_create_city_table', 8),
(28, '2018_09_10_122745_create_region_table', 8),
(30, '2018_09_11_093250_change_type_of_store_name_column_in_employee_table', 9),
(33, '2018_09_12_100516_changes_type_in_employee_table', 10),
(34, '2018_09_12_104127_create_designation_table', 11),
(35, '2018_09_12_104753_alter_column_designation', 12),
(38, '2018_09_13_035852_add_request_to_rm_column_in_employee', 13);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `label`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 'access.users', 'Can Access Users', 0, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(2, 'access.user.create', 'Can Create User', 1, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(3, 'access.user.edit', 'Can Edit User', 1, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(4, 'access.user.delete', 'Can Delete User', 1, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(5, 'access.roles', 'Can Access Roles', 0, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(6, 'access.role.create', 'Can Create Role', 5, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(7, 'access.role.edit', 'Can Edit Role', 5, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(8, 'access.role.delete', 'Can Delete Role', 5, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(9, 'access.permissions', 'Can Access Permission', 0, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(10, 'access.permission.create', 'Can Create Permission', 9, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(11, 'access.permission.edit', 'Can Edit Permission', 9, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(12, 'access.permission.delete', 'Can Delete Permission', 9, '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(17, 'access.store.employee', 'can access store employee', 0, '2018-09-14 08:28:06', '2018-09-14 08:28:06'),
(18, 'access.store.employee.create', 'can create store employee', 17, '2018-09-14 08:28:45', '2018-09-17 01:49:40'),
(19, 'access.store.employee.edit', 'can edit store employee', 17, '2018-09-14 08:30:24', '2018-09-17 01:49:37'),
(20, 'access.store.employee.delete', 'can delete store employee', 17, '2018-09-14 08:30:47', '2018-09-17 01:49:32'),
(21, 'access.regional.employee', 'can access regional employee', 0, '2018-09-14 08:34:13', '2018-09-14 08:34:13'),
(22, 'access.regional.employee.create', 'can create regional employee', 21, '2018-09-14 08:34:42', '2018-09-17 01:49:03'),
(23, 'access.regional.employee.edit', 'can edit regional employee', 21, '2018-09-14 08:35:07', '2018-09-17 01:48:57'),
(24, 'access.regional.employee.delete', 'can delete regional employee', 21, '2018-09-14 08:35:31', '2018-09-17 01:48:52'),
(25, 'access.hrms.employee', 'can access hrms employee', 0, '2018-09-16 23:53:08', '2018-09-16 23:53:08'),
(26, 'access.hrms.employee.create', 'can create hrms employee', 25, '2018-09-17 01:48:07', '2018-09-17 01:48:07'),
(27, 'access.hrms.employee.edit', 'can edit hrms employee', 25, '2018-09-17 01:50:37', '2018-09-17 01:50:37'),
(28, 'access.hrms.employee.delete', 'can delete hrms employee', 25, '2018-09-17 01:50:58', '2018-09-17 01:50:58'),
(29, 'access.storelocation', 'can access store location', 0, '2018-09-17 01:53:17', '2018-09-17 01:53:48'),
(30, 'access.designation', 'can access designation', 0, '2018-09-17 01:54:15', '2018-09-17 01:54:15'),
(31, 'access.grade', 'can access grade', 0, '2018-09-17 01:54:30', '2018-09-17 01:54:30'),
(32, 'access.department', 'can access department', 0, '2018-09-17 01:54:47', '2018-09-17 01:54:47'),
(33, 'access.subdepartment', 'can access sub department', 0, '2018-09-17 01:55:07', '2018-09-17 01:55:07'),
(34, 'access.businessunit', 'can access business unit', 0, '2018-09-17 01:55:36', '2018-09-17 01:55:36'),
(35, 'access.brand', 'can access brand', 0, '2018-09-17 01:55:52', '2018-09-17 01:55:52'),
(36, 'access.legalentity', 'can access legal entity', 0, '2018-09-17 01:56:17', '2018-09-17 01:56:17'),
(37, 'access.employeeclassification', 'can access employee classification', 0, '2018-09-17 01:57:04', '2018-09-17 01:57:04'),
(38, 'access.sourcecategory', 'can access source category', 0, '2018-09-17 01:57:33', '2018-09-17 01:57:33'),
(39, 'access.variablepaytype', 'can access variable pay type', 0, '2018-09-17 01:57:57', '2018-09-17 01:57:57'),
(40, 'access.state', 'can access state', 0, '2018-09-17 01:58:14', '2018-09-17 01:58:14'),
(41, 'access.city', 'can access city', 0, '2018-09-17 01:58:43', '2018-09-17 01:58:43'),
(42, 'access.region', 'can access region', 0, '2018-09-17 01:58:58', '2018-09-17 01:58:58'),
(43, 'access.admin.employee', 'can access admin employee', 0, '2018-09-17 02:00:54', '2018-09-17 02:00:54'),
(44, 'access.admin.employee.create', 'can create admin employee', 43, '2018-09-17 02:01:27', '2018-09-17 02:01:27'),
(45, 'access.admin.employee.edit', 'can edit admin employee', 43, '2018-09-17 02:02:01', '2018-09-17 02:02:47'),
(46, 'access.admin.employee.delete', 'can delete admin employee', 43, '2018-09-17 02:02:25', '2018-09-17 02:02:42'),
(47, 'access.department.create', 'can create designation', 32, '2018-09-17 03:32:45', '2018-09-17 03:32:45'),
(48, 'access.department.edit', 'can edit department', 32, '2018-09-17 03:33:33', '2018-09-17 03:33:33'),
(49, 'access.department.delete', 'can delete department', 32, '2018-09-17 03:34:08', '2018-09-17 03:34:08'),
(50, 'access.storelocation.create', 'can create store location', 29, '2018-09-17 03:41:42', '2018-09-17 03:41:42'),
(51, 'access.storelocation.edit', 'can edit store location', 29, '2018-09-17 03:41:57', '2018-09-17 03:41:57'),
(52, 'access.storelocation.delete', 'can delete store location', 29, '2018-09-17 03:42:21', '2018-09-17 03:42:21'),
(53, 'access.designation.create', 'can create designation', 30, '2018-09-17 03:43:38', '2018-09-17 03:43:38'),
(54, 'access.designation.edit', 'can edit designation', 30, '2018-09-17 03:43:57', '2018-09-17 03:43:57'),
(55, 'access.designation.delete', 'can delete designation', 30, '2018-09-17 03:44:21', '2018-09-17 03:44:21'),
(56, 'access.grade.create', 'can create grade', 31, '2018-09-17 03:44:51', '2018-09-17 03:44:51'),
(57, 'access.grade.edit', 'can edit grade', 31, '2018-09-17 03:45:13', '2018-09-17 03:45:13'),
(58, 'access.grade.delete', 'can delete grade', 31, '2018-09-17 03:45:51', '2018-09-17 03:45:51'),
(59, 'access.subdepartment.create', 'can create sub department', 33, '2018-09-17 03:46:26', '2018-09-17 03:46:26'),
(60, 'access.subdepartment.edit', 'can edit sub department', 33, '2018-09-17 03:46:59', '2018-09-17 03:46:59'),
(61, 'access.subdepartment.delete', 'can delete sub department', 33, '2018-09-17 03:47:19', '2018-09-17 03:47:19'),
(62, 'access.businessunit.create', 'can create business unit', 34, '2018-09-17 03:48:00', '2018-09-17 03:48:00'),
(63, 'access.businessunit.edit', 'can edit business unit', 34, '2018-09-17 03:48:19', '2018-09-17 03:48:19'),
(64, 'access.businessunit.delete', 'can delete business unit', 34, '2018-09-17 03:48:35', '2018-09-17 03:48:35'),
(65, 'access.brand.create', 'can create brand', 35, '2018-09-17 03:49:10', '2018-09-17 03:49:10'),
(66, 'access.brand.edit', 'can edit brand', 35, '2018-09-17 03:49:28', '2018-09-17 03:49:28'),
(67, 'access.brand.delete', 'can delete brand', 35, '2018-09-17 03:49:50', '2018-09-17 03:49:50'),
(68, 'access.legalentity.create', 'can create legal entity', 36, '2018-09-17 03:50:44', '2018-09-17 03:50:44'),
(69, 'access.legalentity.edit', 'can edit legal entity', 36, '2018-09-17 03:51:05', '2018-09-17 03:51:05'),
(70, 'access.legalentity.delete', 'can delete legal entity', 36, '2018-09-17 03:51:34', '2018-09-17 03:51:34'),
(71, 'access.employeeclassification.create', 'can create employee classification', 37, '2018-09-17 03:52:19', '2018-09-17 03:52:19'),
(72, 'access.employeeclassification.edit', 'can edit employee classification', 37, '2018-09-17 03:52:39', '2018-09-17 03:52:39'),
(73, 'access.employeeclassification.delete', 'can delete employee classification', 37, '2018-09-17 03:52:58', '2018-09-17 03:52:58'),
(74, 'access.sourcecategory.create', 'can create source category', 38, '2018-09-17 03:53:36', '2018-09-17 03:53:47'),
(75, 'access.sourcecategory.edit', 'can edit source category', 38, '2018-09-17 03:54:16', '2018-09-17 03:54:16'),
(76, 'access.sourcecategory.delete', 'can delete source category', 38, '2018-09-17 03:54:34', '2018-09-17 03:54:34'),
(77, 'access.variablepaytype.create', 'can create variable type', 39, '2018-09-17 03:55:43', '2018-09-17 03:55:43'),
(78, 'access.variablepaytype.edit', 'can edit variable type', 39, '2018-09-17 03:56:02', '2018-09-17 03:56:02'),
(79, 'access.variablepaytype.delete', 'can delete variable type', 39, '2018-09-17 03:56:23', '2018-09-17 03:56:23'),
(80, 'access.state.create', 'can create state', 40, '2018-09-17 03:56:59', '2018-09-17 03:56:59'),
(81, 'access.state.edit', 'can edit state', 40, '2018-09-17 03:57:48', '2018-09-17 03:57:48'),
(82, 'access.state.delete', 'can delete state', 40, '2018-09-17 03:58:01', '2018-09-17 03:58:01'),
(83, 'access.city.create', 'can create city', 41, '2018-09-17 03:58:23', '2018-09-17 03:58:23'),
(84, 'access.city.edit', 'can edit city', 41, '2018-09-17 03:58:37', '2018-09-17 03:58:37'),
(85, 'access.city.delete', 'can delete city', 41, '2018-09-17 03:58:55', '2018-09-17 03:58:55'),
(86, 'access.region.create', 'can create region', 42, '2018-09-17 03:59:27', '2018-09-17 03:59:50'),
(87, 'access.region edit', 'can edit region', 42, '2018-09-17 04:00:09', '2018-09-17 04:00:09'),
(88, 'access.region.delete', 'can delete region', 42, '2018-09-17 04:00:23', '2018-09-17 04:00:23');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(17, 4),
(18, 4),
(19, 4),
(20, 4),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(25, 2),
(27, 2),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1);

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'south', 1, NULL, '2018-08-09 23:44:17', '2018-09-14 01:00:58'),
(2, 'west', 1, '2018-09-14 00:55:07', '2018-08-10 01:47:55', '2018-09-14 00:55:07'),
(3, 'north', 1, NULL, '2018-09-14 00:54:50', '2018-09-14 00:54:50'),
(4, 'East', 1, NULL, '2018-09-14 00:55:01', '2018-09-14 00:55:01'),
(5, 'test', 1, NULL, '2018-09-18 01:52:30', '2018-09-18 01:52:30'),
(6, 'IT', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'SU', 'Admin', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(2, 'HRMS', 'Human Resource', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(3, 'RM', 'Regional Manager', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(4, 'SM', 'Store Manger', '2018-09-10 04:09:25', '2018-09-10 04:09:25');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(1, 1),
(2, 4),
(3, 3),
(4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('5IJRW7P8WMTvOA6uXFVPtFLVUcY82ht7hyYXw9Um', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiVEQ3RzVNMEhPSktDcGZEdjloTmtqV3c5cEZVQkpPWFVUdnBsN1hLUyI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1536905203),
('bCzYeXJz4YqJiABZ7PWaWwYc9osMnIdSj6J4owWE', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoib05LU3UwaG40aUJIOE1hdWxET1V3VDRnRmNydWxzeG5WM0JzZmxGdCI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1536819497),
('kf43px9JyOQxDE5CQlBeoc45EBOHo9aL9RvCjIx1', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiTzJiQ1JmRVp5WEtFTkdZMmxRUGplbGh3MlRzUzdOUmd1VjlQVDZMRiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1537183926),
('M5FnryboL44U0E12VWSZXWhUPue4wj15KyyH9WnW', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiVThaZWtQUHk3TmlYNmZSNDRwTDFZTEFUUHVMZ0xaYUlta1VmTjdSYyI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czozNDoiaHR0cDovL2xvY2FsaG9zdDo4NC9hZG1pbi9lbXBsb3llZSI7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjM0OiJodHRwOi8vbG9jYWxob3N0Ojg0L2FkbWluL2VtcGxveWVlIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1536670750),
('mOgRRr4qPLakob4RSeMxHEzNcvmN2YS5pWgARPU0', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoibXdzRDB5d25PS0NEbTJWZ2huZ3lNSUQ3ZjNsUEg5WVA1eGxtdXh1eiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1536831530);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `source_category`
--

CREATE TABLE `source_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `source_category`
--

INSERT INTO `source_category` (`id`, `category`, `source_name`, `source_code`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Direct', 'source name 1', 'source code 1', 1, NULL, '2018-09-10 18:30:00', '2018-09-14 01:22:35'),
(2, 'test', 'mayur mayur test', 'test', 1, '2018-09-13 08:21:29', '2018-09-13 08:20:52', '2018-09-13 08:21:29'),
(3, 'job portal', 'naukri', 'SC001', 1, NULL, NULL, NULL),
(4, 'job portal', 'IIM jobs', 'SC002', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Gujarat', 1, NULL, '2018-09-10 18:30:00', NULL),
(2, 'Maharashtra', 1, NULL, '2018-08-10 01:47:55', '2018-09-14 01:02:33'),
(3, 'Hariyana', 0, '2018-09-14 00:14:10', '2018-09-14 00:12:47', '2018-09-14 00:14:10'),
(4, 'Hariyana', 1, '2018-09-14 00:37:03', '2018-09-14 00:35:07', '2018-09-14 00:37:03'),
(5, 'up', 1, NULL, '2018-09-18 01:42:59', '2018-09-18 01:42:59'),
(6, 'Karnataka', 1, NULL, '2018-09-18 04:59:03', '2018-09-18 04:59:03'),
(7, 'IT', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `store_location`
--

CREATE TABLE `store_location` (
  `id` int(10) UNSIGNED NOT NULL,
  `location_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `io_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sap_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_id` int(11) NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_location`
--

INSERT INTO `store_location` (`id`, `location_code`, `io_code`, `sap_code`, `store_name`, `brand_id`, `address`, `city_id`, `state_id`, `region_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ALBL-WH_Bannerghatta Road-WH', '501025', 'Not applicable', 'WH_Bannerghatta Road', 1, '', 1, 1, 1, 1, '2018-09-12 01:19:35', '2018-09-09 18:30:00', '2018-09-12 01:19:35'),
(2, 'ALBL-Unlimited-ROMH-RO', '501031', 'Not applicable', 'Unlimited-ROMH', 1, 'Survey.No.178/2, C.K. Palya, Hullahalli, Sakalavara Post, Bannerghatta Road, Bangalore - 560083.', 1, 1, 1, 1, NULL, '2018-09-12 00:53:37', '2018-09-14 01:49:36'),
(3, 'ALBL-WH_Bannerghatta Road-WH', '501025', 'Not applicable', 'WH_Bannerghatta Road', 5, 'Survey.No.178/2, C.K. Palya, Hullahalli, Sakalavara Post, Bannerghatta Road, Bangalore - 560083.', 11, 6, 1, 1, NULL, NULL, NULL),
(4, 'ALBL-WH_Bannerghatta Road-WH22', '501025', 'Not applicable', 'WH_Bannerghatta Road', 5, 'Survey.No.178/2, C.K. Palya, Hullahalli, Sakalavara Post, Bannerghatta Road, Bangalore - 560083.', 11, 6, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_department`
--

CREATE TABLE `sub_department` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_department`
--

INSERT INTO `sub_department` (`id`, `department_id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sales Operations', 1, NULL, '2018-08-09 23:44:17', '2018-09-14 01:30:33'),
(2, 1, 'test', 1, '2018-09-13 07:18:30', '2018-09-13 07:15:36', '2018-09-13 07:18:30'),
(3, 3, 'test operation', 1, '2018-09-13 07:17:50', '2018-09-13 07:16:01', '2018-09-13 07:17:50'),
(4, 1, 'yrdy', 1, '2018-09-13 07:31:26', '2018-09-13 07:31:23', '2018-09-13 07:31:26'),
(5, 4, 'Mobile', 1, NULL, '2018-09-17 04:59:22', '2018-09-17 04:59:22'),
(6, 5, 'test sub department', 1, NULL, '2018-09-18 01:09:10', '2018-09-18 01:09:10'),
(7, 6, 'sales operation', 1, NULL, '2018-09-18 02:34:23', '2018-09-18 02:34:23'),
(8, 5, 'test', 1, NULL, '2018-09-18 03:53:31', '2018-09-18 03:53:31'),
(9, 4, 'IT', 1, NULL, NULL, NULL),
(10, 9, 'mayur', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emp_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_joining` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `emp_id`, `emp_name`, `gender`, `date_of_joining`, `designation`, `email`, `personal_email`, `mobile_no`, `region`, `state`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'super', 'Arvind Lifestyle', 'male', '0000-00-00', NULL, 'arvinduniversity@gmail.com', NULL, NULL, NULL, NULL, '$2y$10$8mktQf/9PUHmoNIZFkERV.notQ0gd4q.q6epqvgv9J1zDMKQSFAJu', 1, 'A9EjkUkZOuUJ405wl1ugCPgYy7ewOPCItEadz98Lu6PzjwmQoI9PyJbYM0oW', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(2, 'super', 'Mayur Store', 'male', '0000-00-00', NULL, 'mayur.citrusbug+store@gmail.com', NULL, '7405568435', NULL, NULL, '$2y$10$8mktQf/9PUHmoNIZFkERV.notQ0gd4q.q6epqvgv9J1zDMKQSFAJu', 1, 'oLZLpmZxWLeuzub6OuWlnd2gzknPv4VzcNdSnm4Sbz4DrXqzOuZzyhK0B2sI', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(3, 'super', 'Mayur RM', 'male', '0000-00-00', NULL, 'mayur.citrusbug+rm@gmail.com', NULL, '7405568435', NULL, NULL, '$2y$10$8mktQf/9PUHmoNIZFkERV.notQ0gd4q.q6epqvgv9J1zDMKQSFAJu', 1, '89YP52hWvtDSUE1EjT8TJipWHuogW20G7DjKcHEYOLVbCIou8PEO7FHPnD1f', '2018-09-10 04:09:25', '2018-09-10 04:09:25'),
(4, 'super', 'Mayur HR', 'male', '0000-00-00', NULL, 'mayur.citrusbug+hr@gmail.com', NULL, '7405568435', NULL, NULL, '$2y$10$8mktQf/9PUHmoNIZFkERV.notQ0gd4q.q6epqvgv9J1zDMKQSFAJu', 1, 'MUSoeCqLV60AL5TGijN7272CqlDzkYE4q8ZGbhbc131ONwmnfdshqgbSmmgf', '2018-09-10 04:09:25', '2018-09-10 04:09:25');

-- --------------------------------------------------------

--
-- Table structure for table `variable_pay_type`
--

CREATE TABLE `variable_pay_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variable_pay_type`
--

INSERT INTO `variable_pay_type` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Incentive', 1, NULL, '2018-09-10 18:30:00', NULL),
(2, 'test tset', 0, '2018-09-13 23:59:12', '2018-09-13 23:58:54', '2018-09-13 23:59:12'),
(3, 'Incentive1', 1, '2018-09-14 01:12:14', '2018-09-14 01:12:07', '2018-09-14 01:12:14'),
(4, 'test pay', 1, NULL, '2018-09-18 01:37:23', '2018-09-18 01:37:23'),
(5, 'IT', 1, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`);

--
-- Indexes for table `attachment`
--
ALTER TABLE `attachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_unit`
--
ALTER TABLE `business_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_classification`
--
ALTER TABLE `employee_classification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `legal_entity`
--
ALTER TABLE `legal_entity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `source_category`
--
ALTER TABLE `source_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_location`
--
ALTER TABLE `store_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_department`
--
ALTER TABLE `sub_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `variable_pay_type`
--
ALTER TABLE `variable_pay_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `attachment`
--
ALTER TABLE `attachment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `business_unit`
--
ALTER TABLE `business_unit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `employee_classification`
--
ALTER TABLE `employee_classification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `legal_entity`
--
ALTER TABLE `legal_entity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `source_category`
--
ALTER TABLE `source_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `store_location`
--
ALTER TABLE `store_location`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sub_department`
--
ALTER TABLE `sub_department`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `variable_pay_type`
--
ALTER TABLE `variable_pay_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
